﻿<p>%[TRAD_LOGIN_INFO]</p>
<table border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td style="color: %[account.SkinColor]; padding-left: 30px;">&bull;<span style="color: %[account.SkinColor]; padding-left: 5px">%[TRAD_LOGIN]</span> %[CustomTag:recipientId]</td>
	</tr>
	<tr>
		<td style="color: %[account.SkinColor]; padding-left: 30px;">&bull;<span style="color: %[account.SkinColor]; padding-left: 5px">%[TRAD_PASSWD]</span> <a href='%[CustomTag:passwordUrl]'>%[TRAD_PASSWD_LINK]</a></td>
	</tr>
</table>