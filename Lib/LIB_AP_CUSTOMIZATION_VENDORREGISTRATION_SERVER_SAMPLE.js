/* eslint no-empty-function: "off", no-unused-vars: "off" */
/**
 * @file Lib.AP.Customization.VendorRegistration_Server library
 * @namespace Lib.AP.Customization.VendorRegistration_Server
 */

// eslint-disable-next-line no-redeclare
var Lib = Lib || {};
Lib.AP = Lib.AP || {};
Lib.AP.Customization = Lib.AP.Customization || {};

(function (parentLib)
{
	/**
	 * @lends Lib.AP.Customization.VendorRegistration_Server
	 */
	parentLib.VendorRegistration_Server =
	{
		/**
		* Function called in Vendor registration when the process is sent by the user
		*
		* @memberof Lib.AP.Customization.VendorRegistration_Server
		* @returns {boolean} If the process is valid and can be sent
		* @example
		* <pre><code>
		* IsProcessValid: function()
		* {
		*	//We require a value for this field
		*	var value = Data.GetValue("CompanyStructure__");
		*	return (value !== null && value !== "");
		* }
		* </code></pre>
		*/
		IsProcessValid: function()
		{
		}
	};
})(Lib.AP.Customization);
