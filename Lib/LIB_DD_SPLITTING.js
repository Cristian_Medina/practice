/**
 * @file Lib.DD.Splitting library: helpers for splitting capabilities
 */
var Lib;
Lib.DD.AddLib("Splitting", function ()
{
	/**
	 * @exports Splitting
	 * @memberof Lib.DD
	 */
	return {
		/** @lends Helpers.prototype */
		nDocs: 0,
		enableHighlight: true,
		enableStoreArea: false,
		nbHighlightedZone: 0,
		nMaxHighlightedZone: 50,
		splitParameters: [],
		splitAreas: [],
		details: "",
		error: "",
		chunkSize: 1000,

	 	translations: {
			applyOffset: Language.Translate("_Apply offset"),
			offset: Language.Translate("_Offset"),
			pagesNumber: Language.Translate("_Number of pages"),
			splitOnString: Language.Translate("_Split when finding a string"),
			stringToSearch: Language.Translate("_String to search"),
			caseSensitive: Language.Translate("_Case sensitive"),
			stringOnFirstPage: Language.Translate("_String on first page"),
			searchArea: Language.Translate("_Search area"),
			useRegex: Language.Translate("_Use reg expression"),
			splitOnAreaChange: Language.Translate("_Split when the content of an area change"),
			regex: Language.Translate("_Reg expression"),
			pageWithEmptySplittingArea: Language.Translate("_Page with empty splitting area"),

			errors: {
				offsetGreaterError: Language.Translate("_Offset is greater than number of pages"),
				noStringSpecified: Language.Translate("_Regexp is disabled but no string is specified"),
				noMatchInArea: Language.Translate("_No match in the document or in the specified area"),
				stringNotFound: Language.Translate("_String not found"),
				noAreaSpecified: Language.Translate("_No area specified"),
				invalidArea: Language.Translate("_Invalid area"),
				firstPageNotMatchRegex: Language.Translate("_1st page of document to handle does not match regexp"),
				splittingAreaEmpty: Language.Translate("_Splitting area is empty on one (or more) page(s)")
			}
		},

		/**
		* Reset splitting lib static variable
		*/
		Reset: function ()
		{
			this.nDocs = 0;
			this.enableHighlight = true;
			this.enableStoreArea = false;
			this.nbHighlightedZone = 0;
			this.nMaxHighlightedZone = 50;
			this.splitParameters = [];
			this.splitAreas = [];
			this.details = "";
			this.error = "";
			this.chunkSize = 1000;
		},

		/**
		* @param {object} options object to configure splitting
		* @param {string} options.mode How the split is done. Required, possible value fixedPageNumber, stringSplit, areaSplit, noDivision
		* @param {number} options.offset Where the split begin in 0base. Optional, default: 0
		* @param {number} options.totalPages Where the split end i 0base. Optional, default: The document total page
		* @param {number} options.numberOfPages The number of page of splitted documents. Required for fixedPageNumber mode, not use for other mode
		* @param {string} options.searchString The string to search to determine if we begin a new document
		* @param {bool} options.caseSensitive the search string or the regx is case sensitive
		* @param {bool} options.searchStringOnFirstPage If true, When we found the seacrh strig or the regex match it's begin a new doc. If false it's end a new doc
		* @param {string} options.area The area to search
		* @param {bool} options.useRegex Activate the regexp mode, in this case options.searchString is the regexp
		* @return {object} object representing split parameter, null if not in success
		*/
		Split: function (options)
		{
			var getTotalPages = function ()
			{
				return options.totalPages || Document.GetPageCount();
			};

			if (options.mode)
			{
				this.Reset();
				this.enableHighlight = typeof (options.enableHighlight) === 'boolean' ? options.enableHighlight : true;
				this.enableStoreArea = typeof (options.enableStoreArea) === 'boolean' ? options.enableStoreArea : false;

				if (options.mode === 'fixedPageNumber')
				{
					return this.SplitEveryNPages(options.offset, getTotalPages(), options.numberOfPages);
				}
				else if (options.mode === 'stringSplit')
				{
					return this.SplitStringPage(options.offset, getTotalPages(), options.searchString, options.caseSensitive, options.searchStringOnFirstPage, options.area, options.useRegex);
				}
				else if (options.mode === 'areaSplit')
				{
					return this.SplitAreaChange(options.offset, getTotalPages(), options.area, options.caseSensitive, options.useRegex, options.searchString);
				}
				else if (options.mode === 'noDivision')
				{
					return this.ApplyOffset(options.offset, getTotalPages());
				}
			}

			return null;
		},

		/**
		* Parse string containing X,Y,W,H
		*
		* @param {string} myCoordinates value to parse
		* @return {object} structure containing rect values {x, y ,width, height}
		*/
		GetAreaFromCoordinates: function (myCoordinates)
		{
			if (!myCoordinates)
			{
				return null;
			}

			var array = myCoordinates.split(",");
			if (array.length !== 4)
			{
				return null;
			}

			var area = {
				x: parseInt(array[0], 10),
				y: parseInt(array[1], 10),
				width: parseInt(array[2], 10),
				height: parseInt(array[3], 10)
			};

			var isWidthNegativeOrZero = area.width == null || area.width <= 0;
			var isHeightNegativeOrZero = area.height == null || area.height <= 0;
			var isSizeError = isWidthNegativeOrZero || isHeightNegativeOrZero;

			if (isSizeError)
			{
				return null;
			}

			return area;
		},

		HandleArea: function (area, store, highlight)
		{
			if (store)
			{
				this.splitAreas.push(area);
			}

			if (highlight)
			{
				this.Highlight(area);
			}
		},

		Highlight: function (area)
		{
			if (!this.enableHighlight)
			{
				return;
			}

			if (this.nbHighlightedZone < this.nMaxHighlightedZone)
			{
				area.Highlight(true, 0xFF0000, 0x000000);

				this.nbHighlightedZone++;
			}
		},

		/**
		* Parse string containing X,Y,W,H
		*
		* @param {string} myCoordinates value to parse
		* @return {object} structure containing rect values {x:,y:,width,height:}
		*/
		TrimString: function (s)
		{
			return s == null ? "" : s.replace(/^\s+|\s+$/, '');
		},

		AddSplitParameter: function (startPage, endPage)
		{
			this.splitParameters.push({ startPage: startPage, endPage: endPage });
			this.nDocs++;
		},

		GetSplitParameters: function ()
		{
			return this.splitParameters;
		},

		SerializeSplitParameters: function (splitParameters, start, size)
		{
			splitParameters = splitParameters || this.splitParameters;
			start = start ? Math.min(start, splitParameters.length) : 0;
			size = size ? Math.min(size, splitParameters.length) : splitParameters.length;

			if (start + size > splitParameters.length)
			{
				size = splitParameters.length - start;
			}

			var result = "";
			var count = start + size;
			for (var i = start; i < count; i++)
			{
				if (i != start)
				{
					result += ';';
				}

				result += splitParameters[i].startPage + '-' + splitParameters[i].endPage;
			}
			return result;
		},

		SerializeSplitParametersByChunk: function (splitParameters, chunkSize)
		{
			splitParameters = splitParameters || this.splitParameters;
			chunkSize = chunkSize || this.chunkSize;
			var chunks = [];
			var i = 0;
			while (i < splitParameters.length)
			{
				chunks.push(this.SerializeSplitParameters(splitParameters, i, chunkSize));
				i = i + chunkSize;
			}

			return chunks;
		},

		DeserializeSplitParameters: function (stringSplitParameters)
		{
			var params = stringSplitParameters.split(';');
			for (var i = 0; i < params.length; i++)
			{
				var param = params[i].split('-');
				if (param.length === 2)
				{
					this.AddSplitParameter(param[0], param[1]);
				}
			}
			return this.splitParameters;
		},

		NormalizePositiveNumber: function (n)
		{
			if (isNaN(n) || n < 0)
			{
				return 0;
			}
			return n;
		},

		/**
		* Truncate the document after an offset of pages
		*
		* @param {number} offset index of page to take the document from
		* @param {number} totalPages total number of pages
		* @return {object} object representing split parameter, null if not in succeess
		*/
		ApplyOffset: function (offset, totalPages)
		{
			offset = this.NormalizePositiveNumber(offset);

			// write settings in details
			this.details = this.translations.applyOffset;
			this.details += "\n" + this.translations.offset + " : " + offset;
			this.details += "\n" + this.translations.pagesNumber + " : " + totalPages;

			// handle invalid params
			if (totalPages <= offset)
			{
				this.error = this.translations.errors.offsetGreaterError;
				return false;
			}

			this.AddSplitParameter(offset + 1, totalPages);
			return this.splitParameters;
		},

		/**
		* Split document each page number defined by a documentlength
		*
		* @param {number} offset index of page to take the document from
		* @param {number} totalPages total number of pages
		* @param {number} documentlength each splitted documents will have this length (in pages) or less for last one
		* @return {object} object representing split parameter, null if not in succeess
		*/
		SplitEveryNPages: function (offset, totalPages, singleDocumentlength)
		{
			if (isNaN(singleDocumentlength))
			{
				return null;
			}

			offset = this.NormalizePositiveNumber(offset);

			// write settings in details
			this.details = Language.Translate("_Split every {0} pages", true, singleDocumentlength);
			this.details += "\n" + this.translations.offset + " : " + offset;
			this.details += "\n" + this.translations.pagesNumber + " : " + totalPages;

			// handle invalid params
			if (totalPages <= offset)
			{
				this.error = this.translations.errors.offsetGreaterError;
				return false;
			}

			if ((totalPages < (offset + singleDocumentlength)) || ((totalPages - offset) % singleDocumentlength !== 0) || (singleDocumentlength === 0))
			{
				this.error = Language.Translate("_The document's number of page has to be a multiple of {0} (after offset).", true, singleDocumentlength);
				return false;
			}

			// effective split
			for (var i = offset + 1; i < totalPages + 1; i = i + singleDocumentlength)
			{
				this.AddSplitParameter(i, i + singleDocumentlength - 1);
			}

			return this.splitParameters;
		},

		/**
		* Split according a string in the whole page or in an area of the page
		*
		* @param {number} offset index of page to take the document from
		* @param {number} totalPages total number of pages
		* @param {string} searchString string to search in page or in area. Can be null if use regex
		* @param {bool} bCaseSensitive search for string or regex is case sensitive or not
		* @param {bool} bSearchStringOnFirstPage determine where split when qe found the specified string or the regex match
		* @param {bool} bWholePage search o the whole doc
		* @param {string} sArea string containing X,Y,W,H
		* @param {bool} bRegExpUse use regex mod if true
		* @param {string} regExp in regex mode the regx string to match
		* * @return {object} object representing split parameter, null if not in succeess
		*/
		SplitStringPage: function (offset, totalPages, searchString, bCaseSensitive, bSearchStringOnFirstPage, sArea, bRegExpUse)
		{
			var that = this;
			var bNewDoc = true;
			var area = this.GetAreaFromCoordinates(sArea);
			var startPage = 1;

			function _SplitStringPage(i)
			{
				var areaSplit = area ? Document.GetArea(i, area.x, area.y, area.width, area.height) : null;
				var paramSplit = {
					"area": areaSplit, // if area is not valid, search on whole page
					"page": i,
					"keepCase": bCaseSensitive,
					"valueToSearch": searchString,
					"type": bRegExpUse ? "regexp" : "string",
					"matchingTextOnly": "true"
				};

				var zones = Document.SearchString(paramSplit);

				if (zones != null && zones.length > 0)
				{
					// String found on the page
					that.HandleArea(zones[0], that.enableStoreArea, that.enableHighlight);

					if (bSearchStringOnFirstPage)
					{
						if (bNewDoc)
						{
							// 1st page of 1st doc - take into account if offset is badly set
							startPage = i + 1;
							bNewDoc = false;
							if (totalPages === 1)
							{
								that.AddSplitParameter(startPage, startPage);
							}
						}
						else
						{
							that.AddSplitParameter(startPage, i);
							startPage = i + 1;

							if (i === totalPages - 1)
							{
								that.AddSplitParameter(startPage, i + 1);
							}
						}
					}
					else
					{
						that.AddSplitParameter(startPage, i + 1);
						startPage = i + 2;
					}
				}
				else if (bSearchStringOnFirstPage && i === totalPages - 1)
				{
					that.AddSplitParameter(startPage, i + 1);
				}
			}

			offset = this.NormalizePositiveNumber(offset);

			this.details = this.translations.splitOnString;
			this.details += "\n" + this.translations.offset + " : " + offset;
			this.details += "\n" + this.translations.pagesNumber + " : " + totalPages;
			this.details += "\n" + this.translations.stringToSearch + " : " + searchString;
			this.details += "\n" + this.translations.caseSensitive + " : " + Language.Translate(bCaseSensitive);
			this.details += "\n" + this.translations.stringOnFirstPage + " : " + Language.Translate(bSearchStringOnFirstPage);
			this.details += "\n" + this.translations.searchArea + " : " + sArea;
			this.details += "\n" + this.translations.useRegex + " : " + Language.Translate(bRegExpUse);

			if (totalPages <= offset)
			{
				this.error = this.translations.errors.offsetGreaterError;
				return null;
			}

			if (searchString === "" && !bRegExpUse)
			{
				this.error = this.translations.errors.noStringSpecified;
				return null;
			}

			startPage = offset + 1;
			for (var i = offset; i < totalPages ; i++)
			{
				_SplitStringPage(i);
			}

			if (this.splitParameters.length === 0)
			{
				this.error = bRegExpUse ?
					this.translations.errors.noMatchInArea :
					this.translations.errors.stringNotFound;

				return null;
			}

			return this.splitParameters;
		},

		/**
		* Split according a the content of an area changed
		*
		* @param {number} offset index of page to take the document from
		* @param {number} totalPages total number of pages
		* @param {string} sArea string containing X,Y,W,H
		* @param {bool} bRegExpUse use regex mod if true
		* @param {string} regExp in regex mode the regx string to match
		* @param {bool} bSplitAreaMustBeFilled specify that splitting area must be filled
		* @return {object} object representing split parameter, null if not in succeess
		*/
		SplitAreaChange: function (offset, totalPages, sArea, bCaseSensitive, bRegExpUse, regExp, bSplitAreaMustBeFilled)
		{
			var that = this;
			var area = this.GetAreaFromCoordinates(sArea);
			var RegExpression = null;
			var bFound = true;
			var cachedString = "";
			var startPage = 1;
			var SplittingConditionNotFound = [];

			function _SplitAreaChange(i)
			{
				var highlightArea = Document.GetArea(i, area.x, area.y, area.width, area.height);
				var newString = highlightArea.toString();
				if (bRegExpUse)
				{
					bFound = RegExpression.exec(newString);
					//see http://stackoverflow.com/questions/4724701/regexp-exec-returns-null-sporadically
					RegExpression.lastIndex = 0;
					if (i === offset && !bFound)
					{
						that.error = that.translations.errors.firstPageNotMatchRegex;
						return false;
					}
					if (bFound && bFound.length > 0 && bFound[1])
					{
						newString = bFound[1];
					}
				}
				if (i === offset && totalPages === 1)
				{
					that.AddSplitParameter(totalPages, totalPages);
				}
				var stringHasChanged = that.TrimString(newString) !== cachedString;
				var isLastPage = i === (totalPages - 1);
				if (i === offset && bFound)
				{
					//1st page of 1st doc
					startPage = i + 1;
					cachedString = that.TrimString(newString);
					that.HandleArea(highlightArea, that.enableStoreArea, that.enableHighlight);
				}
				else if (stringHasChanged && !isLastPage && bFound)
				{
					that.AddSplitParameter(startPage, i);
					startPage = i + 1;
					cachedString = that.TrimString(newString);
					that.HandleArea(highlightArea, that.enableStoreArea, that.enableHighlight);
				}
				else if (!stringHasChanged && isLastPage && bFound)
				{
					that.AddSplitParameter(startPage, totalPages);
				}
				else if (stringHasChanged && isLastPage && bFound)
				{
					that.AddSplitParameter(startPage, i);
					that.AddSplitParameter(totalPages, totalPages);
					that.HandleArea(highlightArea, that.enableStoreArea, that.enableHighlight);
				}
				else if (isLastPage && !bFound)
				{
					that.AddSplitParameter(startPage, i + 1);
				}

				if (!bFound || (!bRegExpUse && !that.TrimString(newString)))
				{
					SplittingConditionNotFound.push((i + 1).toString());
				}
				return true;
			}

			offset = this.NormalizePositiveNumber(offset);

			// write settings in details
			this.details = this.translations.splitOnAreaChange;
			this.details += "\n" + this.translations.offset + " : " + offset;
			this.details += "\n" + this.translations.pagesNumber + " : " + totalPages;
			this.details += "\n" + this.translations.searchArea + " : " + sArea;
			this.details += "\n" + this.translations.caseSensitive + " : " + Language.Translate(bCaseSensitive);
			this.details += "\n" + this.translations.useRegex + " : " + Language.Translate(bRegExpUse);
			if (regExp)
			{
				this.details += "\n" + this.translations.regex + " : " + regExp;
			}

			// handle invalid params
			if (totalPages <= offset)
			{
				this.error = this.translations.errors.offsetGreaterError;
				return null;
			}

			if (!area)
			{
				this.error = sArea === "" ? this.translations.errors.noAreaSpecified : this.translations.errors.invalidArea;
				return null;
			}
			// we will try to match the content of the area to a regular expression, if it match then it means the start of a new doc
			bRegExpUse = bRegExpUse && regExp;
			if (bRegExpUse)
			{
				var regExpFlags = (bCaseSensitive) ? "g" : "gi";
				RegExpression = new RegExp(regExp, regExpFlags);
			}

			// effective search
			startPage = offset;
			for (var i = offset; i < totalPages ; i++)
			{
				var splitSuccess = _SplitAreaChange(i);
				if (!splitSuccess)
				{
					return null;
				}
			}

			if (bSplitAreaMustBeFilled && SplittingConditionNotFound.length > 0)
			{
				this.details += "\n" + this.translations.pageWithEmptySplittingArea + " : " + SplittingConditionNotFound.join(", ");
				that.error = that.translations.errors.splittingAreaEmpty;
				return null;
			}

			return this.splitParameters;
		}
	};
});
