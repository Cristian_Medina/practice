///#GLOBALS Lib Sys
/* LIB_DEFINITION{
  "name": "Lib_Purchasing_ShipTo_Client",
  "libraryType": "LIB",
  "scriptType": "CLIENT",
  "comment": "Purchasing library",
  "require": [
    "Sys/Sys_Helpers",
    "Sys/Sys_Helpers_Promise",
    "Sys/Sys_OnDemand_Users",
    "[Sys/Sys_GenericAPI_Server]",
    "[Sys/Sys_GenericAPI_Client]",
    "Lib_P2P_V12.0.425.0",
    "Lib_Purchasing_PRLineItems_V12.0.425.0",
    "Sys/Sys_TechnicalData",
    "Lib_Purchasing_CheckPO_V12.0.425.0",
    "Lib_Purchasing_Punchout_PO_Client_V12.0.425.0"
  ]
}*/
//CLIENT
var Lib;
(function (Lib) {
    var Purchasing;
    (function (Purchasing) {
        var ShipTo;
        (function (ShipTo) {
            /**
            * PS can add those line in Lib.P2P.Customization.Common
            * if(Lib.Purchasing && Lib.Purchasing.ShipTo)
            * {
            * 		Lib.Purchasing.ShipTo.defaultCountryCode = "US";
            * }
            */
            var defaultShipTo = {};
            function InitControls() {
                Controls.ShipToCompany__.SetAttributes("ID__");
                Controls.ShipToCompany__.OnSelectItem = OnSelectItem;
                Controls.ShipToCompany__.ClearButtons();
                Controls.ShipToCompany__.AddButton("_My Address", "_My Address", { SHIPTOCOMPANY__: "__my address__" });
                Controls.ShipToCompany__.OnChange = SetByUser;
                Controls.ShipToContact__.OnChange = SetByUser;
                Controls.ShipToPhone__.OnChange = SetByUser;
                Controls.ShipToEmail__.OnChange = SetByUser;
                Controls.ShipToAddress__.OnChange = SetByUser;
                if (Sys.Parameters.GetInstance("PAC").GetParameter("MultiShipTo", false)) {
                    // Hide main Ship to address at headers
                    Controls.ShipToCompany__.Hide(true);
                    Controls.ShipToAddress__.Hide(true);
                    // Ship to by item
                    Controls.LineItems__.ItemShipToCompany__.Hide(false);
                    Controls.LineItems__.ItemShipToCompany__.SetRequired(true);
                    Controls.LineItems__.ItemShipToCompany__.SetAttributes(Lib.Purchasing.ShipTo.GetContactAttributes());
                    Controls.LineItems__.ItemShipToCompany__.OnSelectItem = OnSelectItem;
                    Controls.LineItems__.ItemShipToCompany__.ClearButtons();
                    Controls.LineItems__.ItemShipToCompany__.SetAllowTableValuesOnly(true);
                    Controls.LineItems__.ItemShipToAddress__.Hide(false);
                    Controls.LineItems__.ItemShipToAddress__.SetRequired(true);
                    Controls.LineItems__.ItemShipToCompany__.OnBlur = Lib.Purchasing.PRLineItems.RequiredFields.OnBlur;
                    Controls.LineItems__.ItemShipToAddress__.OnBlur = Lib.Purchasing.PRLineItems.RequiredFields.OnBlur;
                }
            }
            ShipTo.InitControls = InitControls;
            function SetReadOnly(readOnly) {
                Controls.ShipToCompany__.SetReadOnly(readOnly);
                Controls.ShipToContact__.SetReadOnly(readOnly);
                Controls.ShipToPhone__.SetReadOnly(readOnly);
                Controls.ShipToEmail__.SetReadOnly(readOnly);
                Controls.ShipToAddress__.SetReadOnly(Lib.P2P.IsPR() || readOnly); // There's not mechanism to transfer a customized address to the PO, so leave it readonly
                if (Sys.Parameters.GetInstance("PAC").GetParameter("MultiShipTo", false)) {
                    Controls.LineItems__.ItemShipToCompany__.SetReadOnly(readOnly);
                }
                else if (!Lib.P2P.IsPR()) {
                    Controls.ShipToCompany__.SetRequired(!readOnly);
                    Controls.ShipToAddress__.SetRequired(!readOnly);
                }
            }
            ShipTo.SetReadOnly = SetReadOnly;
            function IsSetByUser() {
                return Variable.GetValueAsString("ShipTo_SetByUser__") === "true";
            }
            ShipTo.IsSetByUser = IsSetByUser;
            function SetByUser() {
                Variable.SetValueAsString("ShipTo_SetByUser__", "true");
                Variable.SetValueAsString("ShipToAddress", "");
            }
            ShipTo.SetByUser = SetByUser;
            function OnSelectItem(item) {
                var row = this.GetRow();
                if (row) {
                    Lib.Purchasing.PRLineItems.fromUserChange = true;
                }
                else {
                    Variable.SetValueAsString("ShipTo_SetByUser__", "true");
                }
                if (item.GetValue("ShipToCompany__") === "__my address__") {
                    FillFromUser(User.loginId, row).Then(function () {
                        if (row) {
                            Lib.Purchasing.PRLineItems.LeaveEmptyLine();
                        }
                    });
                }
                else {
                    Lib.Purchasing.ShipTo.Fill(function (field) { return item.GetValue(field); }, false, row ? row.GetItem() : null)
                        .Then(function () {
                        if (row) {
                            Lib.Purchasing.PRLineItems.LeaveEmptyLine();
                        }
                    });
                    if (row && row.GetLineNumber(true) === 1) {
                        // Set the header shipto by the first line shipto
                        Lib.Purchasing.ShipTo.Fill(function (field) { return item.GetValue(field); });
                    }
                }
                if (Lib.P2P.IsPO()) {
                    var requiredFields = new Lib.Purchasing.CheckPO.RequiredFields(Data);
                    if (requiredFields.IsMultiShipTo && Lib.Purchasing.Punchout.PO.IsEnabled()) {
                        if (row.GetLineNumber() === 1) {
                            requiredFields.CheckAllSameDeliveryAddress();
                        }
                        else {
                            var deliveryAddressID = (Controls.LineItems__.GetRow(0)).ItemDeliveryAddressID__.GetValue();
                            Lib.Purchasing.Punchout.PO.CheckDeliveryAddressID(row, deliveryAddressID);
                        }
                    }
                }
            }
            ShipTo.OnSelectItem = OnSelectItem;
            function OnAddItem(item, previousItem) {
                if (Sys.Parameters.GetInstance("PAC").GetParameter("MultiShipTo", false) &&
                    Sys.Helpers.IsEmpty(item.GetValue("ItemShipToCompany__")) &&
                    Sys.Helpers.IsEmpty(item.GetValue("ItemShipToAddress__"))) {
                    if (previousItem && previousItem.GetValue("ItemShipToCompany__") && previousItem.GetValue("ItemShipToAddress__")) {
                        item.SetValue("ItemShipToCompany__", previousItem.GetValue("ItemShipToCompany__"));
                        item.SetValue("ItemShipToAddress__", previousItem.GetValue("ItemShipToAddress__"));
                        item.SetValue("ItemDeliveryAddressID__", previousItem.GetValue("ItemDeliveryAddressID__"));
                    }
                    else {
                        item.SetValue("ItemShipToCompany__", defaultShipTo["ShipToCompany__"]);
                        item.SetValue("ItemShipToAddress__", defaultShipTo["formattedpostaladdress"]);
                        item.SetValue("ItemDeliveryAddressID__", defaultShipTo["ID__"]);
                    }
                }
            }
            ShipTo.OnAddItem = OnAddItem;
            function OnRefreshRow(index) {
                var row = Controls.LineItems__.GetRow(index);
                var item = row.GetItem();
                if (item && Lib.Purchasing.IsLineItemEmpty(item)) {
                    row.ItemShipToAddress__.SetError("");
                }
            }
            ShipTo.OnRefreshRow = OnRefreshRow;
            /**
            * return true if all Fields related to ShipTo are empty
            */
            function IsEmpty() {
                return Sys.Helpers.IsEmpty(Data.GetValue("DeliveryAddressID__"))
                    && Sys.Helpers.IsEmpty(Data.GetValue("ShipToCompany__"))
                    && Sys.Helpers.IsEmpty(Data.GetValue("ShipToContact__"))
                    && Sys.Helpers.IsEmpty(Data.GetValue("ShipToPhone__"))
                    && Sys.Helpers.IsEmpty(Data.GetValue("ShipToEmail__"))
                    && Sys.Helpers.IsEmpty(Data.GetValue("ShipToAddress__"));
            }
            ShipTo.IsEmpty = IsEmpty;
            /**
            * @returns {promise}
            */
            function FillFromShipToCompany(item, firstItem) {
                return Sys.Helpers.Promise.Create(function (resolve, reject) {
                    var companyCode = Data.GetValue("CompanyCode__");
                    var itemShipToCompany = item.GetValue("ItemShipToCompany__");
                    Lib.Purchasing.ShipTo.QueryShipToByShipToCompany(itemShipToCompany, companyCode, function (queryResult, error) {
                        if (error) {
                            Log.Error(error);
                            resolve();
                        }
                        else if (queryResult.length > 0) {
                            Sys.Helpers.SilentChange(function () {
                                Lib.Purchasing.ShipTo.Fill(function (field) { return queryResult[0][field]; }, false, item)
                                    .Then(function () {
                                    if (firstItem) {
                                        // Set the header shipto by the first line shipto
                                        Lib.Purchasing.ShipTo.Fill(function (field) { return queryResult[0][field]; });
                                    }
                                    resolve();
                                });
                            });
                        }
                        else {
                            Log.Error("No Record with filter (&(ShipToCompany__=" + itemShipToCompany + ")(CompanyCode__=" + companyCode + "))");
                            resolve();
                        }
                    });
                });
            }
            ShipTo.FillFromShipToCompany = FillFromShipToCompany;
            /**
            * @returns {promise}
            */
            function FillFromCompanyCode() {
                return Sys.Helpers.Promise.Create(function (resolve, reject) {
                    var companyCode = Data.GetValue("CompanyCode__"), deliveryAddessId = null;
                    Lib.P2P.CompanyCodesValue.QueryValues(companyCode).Then(function (CCValues) {
                        Log.Info("Get shipto info for " + companyCode);
                        if (Object.keys(CCValues).length > 0) {
                            deliveryAddessId = CCValues.DeliveryAddressID__;
                        }
                        Lib.Purchasing.ShipTo.QueryShipTo(deliveryAddessId, companyCode, function (queryResult, error) {
                            if (error) {
                                Log.Error(error);
                                resolve();
                            }
                            else if (queryResult.length > 0) {
                                defaultShipTo = queryResult[0];
                                Sys.Helpers.SilentChange(function () {
                                    Lib.Purchasing.ShipTo.Fill(function (field) { return defaultShipTo[field]; })
                                        .Then(function () {
                                        defaultShipTo["formattedpostaladdress"] = Data.GetValue("ShipToAddress__");
                                        resolve();
                                    });
                                });
                            }
                            else {
                                Log.Error("No Record with filter (&(ID__=" + deliveryAddessId + ")(CompanyCode__=" + companyCode + "))");
                                resolve();
                            }
                            Sys.Helpers.TryCallFunction("Lib.PR.Customization.Client.OnFillFromCompanyCode");
                        });
                    });
                });
            }
            ShipTo.FillFromCompanyCode = FillFromCompanyCode;
            function FillFromUser(userLogin, row) {
                return Sys.OnDemand.Users.CacheByLogin.Get(userLogin, Lib.P2P.attributesForUserCache.concat(["formattedpostaladdress"]))
                    .Then(function (result) {
                    Log.Info("Get shipto info for " + userLogin);
                    var user = result[userLogin];
                    if (user.$error) {
                        Log.Error(user.$error);
                        Popup.Alert(user.$error, true, null, "_Error");
                    }
                    else {
                        var tableItem_1 = row ? row.GetItem() : null;
                        var shipToAddress_1 = {
                            "ID__": "",
                            "ShipToCompany__": user.company,
                            "ShipToContact__": user.displayname,
                            "ShipToPhone__": user.phonenumber,
                            "ShipToEmail__": user.emailaddress,
                            "ShipToSub__": user.mailsub,
                            "ShipToStreet__": user.street,
                            "ShipToCity__": user.city,
                            "ShipToZipCode__": user.zipcode,
                            "ShipToRegion__": user.mailstate,
                            "ShipToCountry__": user.country,
                            "ShipToAddress__": user.formattedpostaladdress,
                            "formattedpostaladdress": user.formattedpostaladdress
                        };
                        Lib.Purchasing.ShipTo.Fill(function (field) { return shipToAddress_1[field]; }, false, tableItem_1)
                            .Then(function () {
                            if (!user.formattedpostaladdress) {
                                shipToAddress_1.formattedpostaladdress = (tableItem_1 || Data).GetValue(tableItem_1 ? "ItemShipToAddress__" : "ShipToAddress__");
                                user.formattedpostaladdress = shipToAddress_1.formattedpostaladdress;
                                Sys.OnDemand.Users.CacheByLogin.SetUserData(userLogin, "formattedpostaladdress", user.formattedpostaladdress);
                            }
                            if (row && row.GetLineNumber(true) === 1) {
                                // Set the header shipto by the first line shipto
                                Lib.Purchasing.ShipTo.Fill(function (field) { return shipToAddress_1[field]; });
                            }
                        });
                    }
                });
            }
            ShipTo.FillFromUser = FillFromUser;
            // Declare an user data attribute for User cache.
            Sys.OnDemand.Users.CacheByLogin.RegisterUserDataAttribute("formattedpostaladdress");
        })(ShipTo = Purchasing.ShipTo || (Purchasing.ShipTo = {}));
    })(Purchasing = Lib.Purchasing || (Lib.Purchasing = {}));
})(Lib || (Lib = {}));
