/* LIB_DEFINITION{
  "name": "Lib_Version",
  "libraryType": "LIB",
  "scriptType": "COMMON",
  "comment": "Library describing package versions",
  "require": [
    "Sys/Sys",
    "Lib_V12.0.425.0"
  ]
}*/
///#GLOBALS Lib Sys
var Lib;
(function (Lib) {
    var Version;
    (function (Version) {
        Version.PAC = 2;
    })(Version = Lib.Version || (Lib.Version = {}));
})(Lib || (Lib = {}));
