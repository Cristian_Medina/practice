///#GLOBALS Lib Sys
/* LIB_DEFINITION{
  "name": "Lib_Purchasing_CheckPR",
  "libraryType": "LIB",
  "scriptType": "COMMON",
  "comment": "Purchasing library",
  "require": [
    "Lib_Purchasing_V12.0.425.0",
    "Lib_Purchasing_Items_V12.0.425.0",
    "Lib_Purchasing_ShipTo_V12.0.425.0",
    "Sys/Sys_Helpers",
    "[Lib_PR_Customization_Common]"
  ]
}*/
var Lib;
(function (Lib) {
    var Purchasing;
    (function (Purchasing) {
        var CheckPR;
        (function (CheckPR) {
            // required line items
            CheckPR.requiredItems = ["ItemCostCenterName__",
                "ItemUnitPrice__",
                "ItemNetAmount__",
                "ItemQuantity__",
                "ItemDescription__",
                "SupplyTypeName__",
                "ItemCurrency__",
                "BuyerName__"];
            function SetRequiredItem(itemName, required) {
                if (required) {
                    this.requiredItems.push(itemName);
                }
                else {
                    Sys.Helpers.Array.Remove(this.requiredItems, function (item) {
                        return item === itemName;
                    });
                }
            }
            CheckPR.SetRequiredItem = SetRequiredItem;
            /**
             * PS Can push Controls2Check by Role like this :
             * In Lib_PR_Customisation_Client
             * Lib.Purchasing.CheckPR.controls2CheckByRole[Lib.Purchasing.roleRequester] = ["ShipToAddress__","ShipToContact__"]
             */
            CheckPR.controls2CheckByRole = {};
            /**
            * @description Check required fields values on Purchase Requisition
            *
            * @param dataObject Javascript object describing the data that will be checked.
            *
            * @returns a boolean value indicating that all required values have been set
            *
            * @example let requiredFields = new Lib.Purchasing.CheckPR.RequiredFields(Data);
            *
            **/
            var RequiredFields = /** @class */ (function () {
                function RequiredFields(dataObject) {
                    this.dataObject = null;
                    this.allowRequestedDeliveryDateInPast = Sys.Parameters.GetInstance("PAC").GetParameter("AllowRequestedDeliveryDateInPast", false);
                    this.dataObject = dataObject;
                }
                RequiredFields.prototype.CheckItemsDeliveryDates = function (currentRole, options) {
                    options = options || {};
                    var table = this.dataObject.GetTable("LineItems__");
                    // case requester submiting a QR
                    var isReadOnly = Lib.Purchasing.RequestedDeliveryDate.IsReadOnly(currentRole);
                    var isRequired = Lib.Purchasing.RequestedDeliveryDate.IsRequired(currentRole);
                    var atLeastOneDeliveryDateInPast = false;
                    var ok = true, deliveryDate, deliveryDateInPast;
                    var count = table.GetItemCount();
                    for (var i = 0; i < count; i++) {
                        var rowItem = table.GetItem(i);
                        var isSpecificRow = !Sys.Helpers.IsNumeric(options.specificItemIndex) || options.specificItemIndex === i;
                        if (!Lib.Purchasing.IsLineItemEmpty(rowItem)) {
                            var serviceBased = Lib.Purchasing.Items.IsServiceBasedItem(rowItem);
                            if (serviceBased && options.ignoreRequiredCheck !== true && isRequired && isSpecificRow) {
                                if (!rowItem.GetValue("ItemStartDate__")) {
                                    rowItem.SetError("ItemStartDate__", "This field is required!");
                                    ok = false;
                                }
                                if (!rowItem.GetValue("ItemEndDate__")) {
                                    rowItem.SetError("ItemEndDate__", "This field is required!");
                                    ok = false;
                                }
                            }
                            deliveryDate = rowItem.GetValue("ItemRequestedDeliveryDate__");
                            if (Sys.Helpers.IsEmpty(deliveryDate)) {
                                if (options.ignoreRequiredCheck !== true && isRequired && isSpecificRow) {
                                    rowItem.SetError("ItemRequestedDeliveryDate__", "This field is required!");
                                    ok = false;
                                }
                            }
                            else if (!isReadOnly) {
                                var today = new Date();
                                today.setHours(0, 0, 0, 0);
                                deliveryDate.setHours(0, 0, 0, 0);
                                deliveryDateInPast = Sys.Helpers.Date.CompareDate(deliveryDate, today) < 0;
                                if (deliveryDateInPast && isSpecificRow && !serviceBased) {
                                    if (this.allowRequestedDeliveryDateInPast) {
                                        rowItem.SetWarning("ItemRequestedDeliveryDate__", "_Warning date in the past");
                                    }
                                    else {
                                        rowItem.SetError("ItemRequestedDeliveryDate__", "_Error date in the past");
                                        ok = false;
                                    }
                                }
                                rowItem.SetValue("RequestedDeliveryDateInPast__", deliveryDateInPast);
                                atLeastOneDeliveryDateInPast = atLeastOneDeliveryDateInPast || deliveryDateInPast;
                            }
                        }
                    }
                    if (!isReadOnly) {
                        this.dataObject.SetValue("AtLeastOneRequestedDeliveryDateInPast__", atLeastOneDeliveryDateInPast);
                    }
                    return ok;
                };
                RequiredFields.prototype.CheckTotal = function ( /*currentRole: string*/) {
                    // the buyer (quote) has to set the amount of the purchase requisition, if no quote, the requester has to set the amount
                    var table = this.dataObject.GetTable("LineItems__");
                    var isUnitOfMeasureEnabled = Sys.Parameters.GetInstance("PAC").GetParameter("DisplayUnitOfMeasure");
                    // case requester submiting a QR
                    var ok = true;
                    var count = table.GetItemCount();
                    var _loop_1 = function (i) {
                        var item = table.GetItem(i);
                        var isAmountBased = Lib.Purchasing.Items.IsAmountBasedItem(item);
                        var allRequiredItems = Lib.Purchasing.CheckPR.requiredItems;
                        Sys.Helpers.Array.ForEach(allRequiredItems, function (reqItem) {
                            if (Sys.Helpers.IsEmpty(item.GetValue(reqItem))) {
                                item.SetError(reqItem, "This field is required!");
                                ok = false;
                            }
                        });
                        if (isAmountBased) {
                            if (item.GetValue("ItemNetAmount__") <= 0) {
                                item.SetError("ItemNetAmount__", "Value is not allowed!");
                                ok = false;
                            }
                            item.SetValue("ItemUnit__", "");
                            item.SetValue("ItemUnitDescription__", "");
                        }
                        else {
                            if (isUnitOfMeasureEnabled && Sys.Helpers.IsEmpty(item.GetValue("ItemUnit__"))) {
                                item.SetError("ItemUnit__", "This field is required!");
                                ok = false;
                            }
                            if (item.GetValue("ItemQuantity__") <= 0) {
                                item.SetError("ItemQuantity__", "Value is not allowed!");
                                ok = false;
                            }
                        }
                        if (!Sys.Helpers.IsEmpty(item.GetError("ItemGLAccount__"))) {
                            item.SetValue("ItemGLAccount__", "");
                        }
                    };
                    for (var i = 0; i < count; i++) {
                        _loop_1(i);
                    }
                    return ok;
                };
                RequiredFields.prototype.CheckRequiredFields = function (currentRole) {
                    var ok = true;
                    if (Lib.Purchasing.CheckPR.controls2CheckByRole[currentRole]) {
                        var controls = Lib.Purchasing.CheckPR.controls2CheckByRole[currentRole];
                        for (var i = 0; i < controls.length; i++) {
                            if (Sys.Helpers.IsEmpty(this.dataObject.GetValue(controls[i]))) {
                                this.dataObject.SetError(controls[i], "This field is required!");
                                ok = false;
                            }
                        }
                    }
                    return ok;
                };
                RequiredFields.prototype.CheckUpdatedItems = function (doNotCheckLeadTime) {
                    var table = this.dataObject.GetTable("LineItems__");
                    var QueryItems = function (filter) {
                        var options = {
                            table: "PurchasingOrderedItems__",
                            filter: filter,
                            attributes: ["ItemNumber__", "ValidityDate__", "ExpirationDate__", "LeadTime__"],
                            additionalOptions: {
                                bigQuery: true
                            }
                        };
                        return Sys.GenericAPI.PromisedQuery(options);
                    };
                    var requisitionStatus = Data.GetValue("RequisitionStatus__");
                    if (requisitionStatus !== "To receive" && requisitionStatus !== "Received" && requisitionStatus !== "Rejected" && requisitionStatus !== "Canceled") {
                        var filter = "(|";
                        var items_1 = [];
                        for (var i = 0; i < table.GetItemCount(); i++) {
                            var item = table.GetItem(i);
                            var itemID = item.GetValue("ItemNumber__");
                            items_1[i] = item;
                            filter += "(ItemNumber__=" + itemID + ")";
                        }
                        filter += ")";
                        return QueryItems(filter)
                            .Then(function (results) {
                            if (results) {
                                items_1.forEach(function (item) {
                                    var result = results.filter(function (res) { return res.ItemNumber__ === item.GetValue("ItemNumber__"); });
                                    if (result.length > 0) {
                                        result = result[0];
                                        // Check item after update
                                        var date = new Date();
                                        date.setHours(0, 0, 0, 0);
                                        if ((result.ExpirationDate__ && Sys.Helpers.Date.CompareDate(date, new Date(result.ExpirationDate__)) > 0)
                                            || (result.ValidityDate__ && Sys.Helpers.Date.CompareDate(date, new Date(result.ValidityDate__)) < 0)) {
                                            item.SetError("ItemDescription__", "_Item not available");
                                        }
                                        else {
                                            if (item.GetError("ItemDescription__") === Language.Translate("_Item not available", false)) {
                                                item.SetError("ItemDescription__", "");
                                            }
                                            if (!doNotCheckLeadTime && requisitionStatus !== "To approve" && item.GetValue("ItemRequestedDeliveryDate__")) {
                                                var itemDeliveryDate = new Date(item.GetValue("ItemRequestedDeliveryDate__"));
                                                item.SetValue("LeadTime__", result.LeadTime__);
                                                date.setDate(date.getDate() + parseInt(result.LeadTime__, 10));
                                                if (Sys.Helpers.Date.CompareDate(itemDeliveryDate, date) < 0) {
                                                    item.SetWarning("ItemRequestedDeliveryDate__", "_Warning the requested delivery date is too soon (expected {0} or later)", Language.FormatDate(date));
                                                }
                                                else {
                                                    item.SetWarning("ItemRequestedDeliveryDate__", "");
                                                }
                                            }
                                        }
                                    }
                                });
                            }
                        });
                    }
                };
                RequiredFields.prototype.CheckAll = function (currentRole) {
                    var table = this.dataObject.GetTable("LineItems__");
                    var customIsValid;
                    Lib.Purchasing.RemoveEmptyLineItem(table);
                    // Checks everything, even if one fails
                    var ok = true;
                    ok = this.CheckTotal() && ok;
                    ok = this.CheckItemsDeliveryDates(currentRole) && ok;
                    ok = this.CheckRequiredFields(currentRole) && ok;
                    ok = Lib.Purchasing.ShipTo.CheckDeliveryAddress(currentRole) && ok;
                    if (Sys.Helpers.TryGetFunction("Lib.PR.Customization.Common.OnValidateForm")) {
                        customIsValid = Sys.Helpers.TryCallFunction("Lib.PR.Customization.Common.OnValidateForm", ok);
                    }
                    else {
                        customIsValid = ok;
                    }
                    var promisifiedIsValid;
                    if (typeof customIsValid === "boolean") {
                        promisifiedIsValid = Sys.Helpers.Promise.Resolve(customIsValid);
                    }
                    else if (Sys.Helpers.Promise.IsPromise(customIsValid)) {
                        promisifiedIsValid = customIsValid;
                    }
                    else {
                        promisifiedIsValid = Sys.Helpers.Promise.Resolve(ok);
                    }
                    return promisifiedIsValid;
                };
                return RequiredFields;
            }());
            CheckPR.RequiredFields = RequiredFields;
            function isSingleVendor(dataObject) {
                var table = dataObject.GetTable("LineItems__");
                var count = table.GetItemCount();
                if (count <= 1) {
                    return true;
                }
                var vendorRef = table.GetItem(0).GetValue("VendorName__");
                for (var i = 1; i < count; i++) {
                    var item = table.GetItem(i);
                    if (item.GetValue("VendorName__") !== vendorRef) {
                        return false;
                    }
                }
                return true;
            }
            CheckPR.isSingleVendor = isSingleVendor;
        })(CheckPR = Purchasing.CheckPR || (Purchasing.CheckPR = {}));
    })(Purchasing = Lib.Purchasing || (Lib.Purchasing = {}));
})(Lib || (Lib = {}));
