///#GLOBALS Lib Sys
/* LIB_DEFINITION{
  "name": "Lib_Purchasing_CheckPO",
  "libraryType": "LIB",
  "scriptType": "COMMON",
  "comment": "Purchasing library",
  "require": [
    "Lib_Purchasing_V12.0.425.0",
    "Lib_Purchasing_AdditionalFees_V12.0.425.0",
    "Lib_Purchasing_Items_V12.0.425.0",
    "Lib_Purchasing_ShipTo_V12.0.425.0",
    "Sys/Sys_Decimal",
    "Sys/Sys_Helpers_Object"
  ]
}*/
var Lib;
(function (Lib) {
    var Purchasing;
    (function (Purchasing) {
        var CheckPO;
        (function (CheckPO) {
            /**
                * @exports CheckPO
                * @memberof Lib.Purchasing
                */
            function IsToleratedPrice(requisitionPrice, orderablePrice, orderPrice, toleranceLimit) {
                var toleratedPrice = new Sys.Decimal(orderablePrice);
                var d_orderPrice = new Sys.Decimal(orderPrice);
                if (toleranceLimit) {
                    var v = parseInt(toleranceLimit, 10);
                    if (toleranceLimit[toleranceLimit.length - 1] === "%") {
                        var requestedPrice = new Sys.Decimal(requisitionPrice);
                        toleratedPrice = toleratedPrice.add(requestedPrice.mul(v).div(100));
                    }
                    else {
                        toleratedPrice = toleratedPrice.add(v);
                    }
                }
                return d_orderPrice.lessThanOrEqualTo(toleratedPrice);
            }
            CheckPO.requiredFields = ["VendorName__", "ShipToCompany__"];
            Sys.Parameters.GetInstance("PAC").OnLoad(function () {
                if (Sys.Parameters.GetInstance("PAC").GetParameter("MultiShipTo", false)) {
                    CheckPO.requiredFields = ["VendorName__"];
                }
            });
            /**
            * @description Check required fields values on Purchase Order
            *
            * @param dataObject Javascript object describing the data that will be checked.
            *
            * @returns a boolean value indicating that all required values have been set
            *
            * @example let requiredFields = new Lib.Purchasing.CheckPO.RequiredFields(Data);
            *
            **/
            var RequiredFields = /** @class */ (function () {
                function RequiredFields(dataObject) {
                    this.dataObject = null;
                    this.IsRequestedDeliveryDateInPastAllowed = Sys.Helpers.Object.ConstantGetter(function () {
                        return Sys.Parameters.GetInstance("PAC").GetParameter("AllowRequestedDeliveryDateInPast", false);
                    });
                    this.IsMultiShipTo = Sys.Helpers.Object.ConstantGetter(function () {
                        return Sys.Parameters.GetInstance("PAC").GetParameter("MultiShipTo", false);
                    });
                    this.dataObject = dataObject;
                }
                RequiredFields.prototype.RemoveAllDownPaymentInputError = function () {
                    this.dataObject.SetError("PaymentPercent__", "");
                    this.dataObject.SetError("PaymentAmount__", "");
                };
                RequiredFields.prototype.CheckDownPaymentAmount = function () {
                    var val = this.dataObject.GetValue("PaymentAmount__");
                    if (val !== null && val !== 0 && (val < 0 || this.dataObject.GetValue("TotalNetAmount__") < val)) {
                        this.dataObject.SetError("PaymentAmount__", "_Payment amount must be between 0 and total net amout");
                        return false;
                    }
                    return true;
                };
                RequiredFields.prototype.CheckDownPaymentPercent = function () {
                    var val = this.dataObject.GetValue("PaymentPercent__");
                    if (val !== null && val !== 0 && (val < 0 || 100 < val)) {
                        this.dataObject.SetError("PaymentPercent__", "_Payment percent must be between 0 and 100");
                        return false;
                    }
                    return true;
                };
                RequiredFields.prototype.CheckDownPaymentExtraInfo = function () {
                    var status = this.dataObject.GetValue("OrderStatus__");
                    if (status === "To order") {
                        // no mandatory fields
                    }
                    else if (status === "To pay") {
                        return this.CheckRequiredFields([
                            "PaymentType__",
                            "PaymentDate__"
                        ]);
                    }
                    return true;
                };
                RequiredFields.prototype.CheckRequiredFields = function (controls2Check) {
                    var ok = true;
                    for (var i = 0; i < controls2Check.length; i++) {
                        if (Sys.Helpers.IsEmpty(this.dataObject.GetValue(controls2Check[i]))) {
                            this.dataObject.SetError(controls2Check[i], "This field is required!");
                            ok = false;
                        }
                    }
                    return ok;
                };
                RequiredFields.prototype.CheckItemsDeliveryDates = function (options) {
                    options = options || {};
                    var ok = true;
                    var status = this.dataObject.GetValue("OrderStatus__");
                    if (status === "To order") {
                        var atLeastOneDeliveryDateInPast = false;
                        var table = this.dataObject.GetTable("LineItems__");
                        var count = table.GetItemCount();
                        var today = new Date();
                        today.setHours(0, 0, 0, 0);
                        for (var i = 0; i < count; i++) {
                            var row = table.GetItem(i);
                            var isSpecificRow = !Sys.Helpers.IsNumeric(options.specificItemIndex) || options.specificItemIndex === i;
                            var selectedDateTime = row.GetValue("ItemRequestedDeliveryDate__");
                            if (selectedDateTime) {
                                selectedDateTime.setHours(0, 0, 0, 0);
                                var deliveryDateInPast = selectedDateTime < today;
                                if (deliveryDateInPast && isSpecificRow && !Lib.Purchasing.Items.IsServiceBasedItem(row)) {
                                    if (this.IsRequestedDeliveryDateInPastAllowed()) {
                                        row.SetWarning("ItemRequestedDeliveryDate__", "_Warning date in the past");
                                    }
                                    else {
                                        row.SetError("ItemRequestedDeliveryDate__", "_Error date in the past");
                                        ok = false;
                                    }
                                }
                                row.SetValue("RequestedDeliveryDateInPast__", deliveryDateInPast);
                                atLeastOneDeliveryDateInPast = atLeastOneDeliveryDateInPast || deliveryDateInPast;
                            }
                        }
                        this.dataObject.SetValue("AtLeastOneRequestedDeliveryDateInPast__", atLeastOneDeliveryDateInPast);
                    }
                    return ok;
                };
                RequiredFields.prototype.CheckItemsQuantities = function () {
                    var ok = true;
                    var table = this.dataObject.GetTable("LineItems__");
                    var count = table.GetItemCount();
                    for (var i = 0; i < count; i++) {
                        var item = table.GetItem(i);
                        ok = ok && CheckItemQuantity(item);
                    }
                    return ok;
                };
                RequiredFields.prototype.CheckAdditionalFeesAmounts = function () {
                    var g = Sys.Helpers.Globals;
                    var companyCode = g.Data.GetValue("CompanyCode__");
                    var requiredIds = [];
                    Sys.Helpers.Data.ForEachTableItem("AdditionalFees__", function (item /*, i*/) {
                        var additionalFeeID = item.GetValue("AdditionalFeeID__");
                        if (!Lib.Purchasing.AdditionalFees.GetValues(companyCode, additionalFeeID)) {
                            requiredIds.push(additionalFeeID);
                        }
                    });
                    var table = g.Data.GetTable("LineItems__");
                    var exchangeRate = 1;
                    var currency = "";
                    if (table.GetItemCount() >= 1) {
                        exchangeRate = table.GetItem(0).GetValue("ItemExchangeRate__");
                        currency = table.GetItem(0).GetValue("ItemCurrency__");
                    }
                    function checklines() {
                        var additionalFeesSummary = {};
                        Sys.Helpers.Data.ForEachTableItem("AdditionalFees__", function (item /*, i*/) {
                            var additionalFeeID = item.GetValue("AdditionalFeeID__");
                            var amount = item.GetValue("Price__");
                            if (additionalFeesSummary[additionalFeeID]) {
                                additionalFeesSummary[additionalFeeID].amount = new Sys.Decimal(additionalFeesSummary[additionalFeeID].amount || 0).add(amount || 0).toNumber();
                            }
                            else {
                                var maxAmount = Sys.Helpers.Round(new Sys.Decimal(Lib.Purchasing.AdditionalFees.GetValues(companyCode, additionalFeeID).MaxAmount__).div(exchangeRate), 2).toNumber();
                                additionalFeesSummary[additionalFeeID] = {
                                    amount: amount,
                                    maxAmount: maxAmount
                                };
                            }
                        });
                        Sys.Helpers.Data.ForEachTableItem("AdditionalFees__", function (item /*, i*/) {
                            var additionalFeeID = item.GetValue("AdditionalFeeID__");
                            if (additionalFeesSummary[additionalFeeID].amount > additionalFeesSummary[additionalFeeID].maxAmount) {
                                item.SetError("Price__", Language.Translate("_Price exceeds max amount of {0} {1}", false, additionalFeesSummary[additionalFeeID].maxAmount, currency));
                            }
                            else if (item.GetValue("Price__")) {
                                item.SetError("Price__", "");
                            }
                        });
                    }
                    // Synchronous because server side
                    return Lib.Purchasing.AdditionalFees.QueryValues(companyCode, requiredIds)
                        .Then(checklines);
                };
                RequiredFields.prototype.CheckValidityPeriod = function (ctrlName) {
                    this.dataObject.SetError("ValidityStart__", "");
                    this.dataObject.SetError("ValidityEnd__", "");
                    var start = this.dataObject.GetValue("ValidityStart__");
                    var end = this.dataObject.GetValue("ValidityEnd__");
                    if (Sys.Helpers.Date.CompareDate(start, end) > 0) {
                        this.dataObject.SetError(ctrlName || "ValidityStart__", "_Validity period start date is later than end date");
                        return false;
                    }
                    return true;
                };
                RequiredFields.prototype.CheckAllSameDeliveryAddress = function () {
                    var table = this.dataObject.GetTable("LineItems__");
                    var res = true;
                    if (table.GetItemCount() > 1) {
                        var deliveryAddressID_1 = table.GetItem(0).GetValue("ItemDeliveryAddressID__");
                        Sys.Helpers.Data.ForEachTableItem(table, function (item) {
                            if (item.GetValue("ItemDeliveryAddressID__") !== deliveryAddressID_1) {
                                item.SetError("ItemDescription__", "_Error delivery adress is not the same as the first item");
                                res = false;
                            }
                            else if (item.GetError("ItemDescription__") === Language.Translate("_Error delivery adress is not the same as the first item", false)) {
                                item.SetError("ItemDescription__", "");
                            }
                        });
                    }
                    return res;
                };
                RequiredFields.prototype.CheckAll = function () {
                    // Checks everything, even if one fails
                    var ok = true;
                    ok = this.CheckDownPaymentPercent() && ok;
                    ok = this.CheckDownPaymentAmount() && ok;
                    ok = this.CheckDownPaymentExtraInfo() && ok;
                    ok = this.CheckItemsDeliveryDates() && ok;
                    ok = this.CheckItemsQuantities() && ok;
                    ok = this.CheckRequiredFields(Lib.Purchasing.CheckPO.requiredFields) && ok;
                    ok = this.CheckValidityPeriod() && ok;
                    ok = Lib.Purchasing.ShipTo.CheckDeliveryAddress() && ok;
                    if (this.IsMultiShipTo && this.dataObject.GetValue("EmailNotificationOptions__") === "PunchoutMode") {
                        ok = this.CheckAllSameDeliveryAddress() && ok;
                    }
                    if (Sys.ScriptInfo.IsServer()) {
                        ok = this.CheckAdditionalFeesAmounts() && ok;
                    }
                    return ok;
                };
                return RequiredFields;
            }());
            CheckPO.RequiredFields = RequiredFields;
            function IsItemPriceTolerated(requisitionPrice, orderablePrice, orderPrice) {
                return IsToleratedPrice(requisitionPrice, orderablePrice, orderPrice, Sys.Parameters.GetInstance("PAC").GetParameter("ItemPriceVarianceToleranceLimit"));
            }
            CheckPO.IsItemPriceTolerated = IsItemPriceTolerated;
            function IsTotalPriceTolerated(requisitionPrice, orderablePrice, orderPrice) {
                return IsToleratedPrice(requisitionPrice, orderablePrice, orderPrice, Sys.Parameters.GetInstance("PAC").GetParameter("TotalPriceVarianceToleranceLimit"));
            }
            CheckPO.IsTotalPriceTolerated = IsTotalPriceTolerated;
            function CheckItemQuantity(item) {
                var ok = true;
                if (Lib.Purchasing.Items.IsAmountBasedItem(item)) {
                    item.SetError("ItemQuantity__", "");
                }
                else {
                    if (Sys.Helpers.IsEmpty(item.GetValue("ItemQuantity__"))) {
                        item.SetError("ItemQuantity__", "This field is required!");
                        ok = false;
                    }
                    if (item.GetValue("ItemQuantity__") <= 0) {
                        //if there is already an error msg, do not replace it (overerdered error msg)
                        if (Sys.Helpers.IsEmpty(item.GetError("PRNumber__"))) {
                            item.SetValue("ItemQuantity__", null);
                            item.SetError("ItemQuantity__", "Value is not allowed!");
                        }
                        ok = false;
                    }
                    var customChangeAllowed = Sys.Helpers.TryCallFunction("Lib.PO.Customization.Common.IsItemQuantityChangeAllowed", item);
                    var changeAllowed = customChangeAllowed || (customChangeAllowed === null && item.GetValue("ItemQuantity__") <= item.GetValue("ItemRequestedQuantity__"));
                    if (ok && !changeAllowed) {
                        item.SetError("ItemQuantity__", "_Quantity cannot exceed requested quantity");
                        ok = false;
                    }
                }
                return ok;
            }
            CheckPO.CheckItemQuantity = CheckItemQuantity;
            function CheckLeadTime(item) {
                var leadTime = item.GetValue("LeadTime__");
                if (!Sys.Helpers.IsEmpty(leadTime)) {
                    var defaultDeliveryDate = new Date();
                    defaultDeliveryDate.setHours(0, 0, 0, 0);
                    defaultDeliveryDate.setDate(defaultDeliveryDate.getDate() + leadTime);
                    var deliveryDate = item.GetValue("ItemRequestedDeliveryDate__");
                    if (deliveryDate) {
                        deliveryDate.setHours(0, 0, 0, 0);
                    }
                    if (deliveryDate < defaultDeliveryDate) {
                        item.SetWarning("ItemRequestedDeliveryDate__", "_Warning the requested delivery date is too soon (expected {0} or later)", Language.FormatDate(defaultDeliveryDate));
                    }
                    else {
                        item.SetWarning("ItemRequestedDeliveryDate__", "");
                    }
                }
            }
            CheckPO.CheckLeadTime = CheckLeadTime;
            function CheckAllLeadTimes() {
                Sys.Helpers.Data.ForEachTableItem("LineItems__", function (item /*, i*/) {
                    CheckLeadTime(item);
                });
            }
            CheckPO.CheckAllLeadTimes = CheckAllLeadTimes;
        })(CheckPO = Purchasing.CheckPO || (Purchasing.CheckPO = {}));
    })(Purchasing = Lib.Purchasing || (Lib.Purchasing = {}));
})(Lib || (Lib = {}));
