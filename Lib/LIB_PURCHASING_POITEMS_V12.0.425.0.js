///#GLOBALS Lib Sys
/* LIB_DEFINITION{
  "name": "Lib_Purchasing_POItems",
  "libraryType": "LIB",
  "scriptType": "COMMON",
  "comment": "Purchasing library to manage items in PO",
  "require": [
    "Sys/Sys_Helpers_LdapUtil",
    "Sys/Sys_Helpers_String",
    "Sys/Sys_Decimal",
    "Lib_Purchasing_Items_V12.0.425.0",
    "Lib_Purchasing_Vendor_V12.0.425.0",
    "Lib_Purchasing_V12.0.425.0",
    "[Lib_Purchasing_Punchout_PO_Client_V12.0.425.0]",
    "[Sys/Sys_Helpers_Browse]",
    "[Lib_CommonDialog_V12.0.425.0]"
  ]
}*/
var Lib;
(function (Lib) {
    var Purchasing;
    (function (Purchasing) {
        var POItems;
        (function (POItems) {
            // Members declaration and implementation
            var MAXRECORDS = Lib.Purchasing.Items.MAXRECORDS;
            var PRItemsSynchronizeConfig = Lib.Purchasing.Items.PRItemsSynchronizeConfig;
            var PRItemsDBInfo = Lib.Purchasing.Items.PRItemsDBInfo;
            var POItemsDBInfo = Lib.Purchasing.Items.POItemsDBInfo;
            var PRItemsToPO = Lib.Purchasing.Items.PRItemsToPO;
            POItems.customerInterestedItems = ["ItemNumber__", "ItemDescription__", "ItemRequestedDeliveryDate__", "ItemQuantity__",
                "ItemUnit__", "ItemUnitPrice__", "ItemNetAmount__", "ItemCurrency__", "ItemTaxCode__", "Deleted", "Added"];
            POItems.customerInterestedAdditionalFeesFields = ["AdditionalFeeDescription__", "Price__", "ItemTaxCode__", "Deleted", "Added"];
            /**
             * Fill extra fields on PO items. This function is set as option when calling FillFormItems function.
             * @param {object} dbItem current pr item in database
             * @param {object} item current po item in form
             * @param {object} options options used to fill items
             */
            function CompleteFormItem(dbItem, item, options) {
                var realOrderedQuantity = null;
                if (options && options.checkQtyWith
                    && Object.keys(options.checkQtyWith).length !== 0
                    && options.checkQtyWith[dbItem.GetValue(PRItemsSynchronizeConfig.tableKey)]
                    && options.checkQtyWith[dbItem.GetValue(PRItemsSynchronizeConfig.tableKey)][dbItem.GetValue(PRItemsSynchronizeConfig.lineKey)] != undefined) {
                    realOrderedQuantity = options.checkQtyWith[dbItem.GetValue(PRItemsSynchronizeConfig.tableKey)][dbItem.GetValue(PRItemsSynchronizeConfig.lineKey)];
                }
                var orderedQuantity = realOrderedQuantity !== null ? realOrderedQuantity : dbItem.GetValue("OrderedQuantity__");
                var unitPrice = dbItem.GetValue("UnitPrice__");
                var noGoodsReceipt = dbItem.GetValue("NoGoodsReceipt__");
                noGoodsReceipt = Sys.Helpers.IsString(noGoodsReceipt) ? Sys.Helpers.String.ToBoolean(noGoodsReceipt) : noGoodsReceipt;
                var requestedQuantity = dbItem.GetValue("Quantity__") - (dbItem.GetValue("QuantityTakenFromStock__") || 0);
                var requestedAmount = dbItem.GetValue("NetAmount__") - (dbItem.GetValue("AmountTakenFromStock__") || 0);
                item.SetValue("ItemRequestedQuantity__", requestedQuantity);
                item.SetValue("ItemRequestedAmount__", requestedAmount);
                var toOrderQuantity = new Sys.Decimal(requestedQuantity).minus(dbItem.GetValue("CanceledQuantity__") || 0).minus(orderedQuantity).toNumber();
                var toOrderAmount = new Sys.Decimal(unitPrice).mul(toOrderQuantity).toNumber();
                //used to check overOrder client Side
                item.SetValue("OrderableQuantity__", toOrderQuantity);
                item.SetValue("OrderableAmount__", toOrderAmount);
                item.SetValue("ItemQuantity__", toOrderQuantity);
                item.SetValue("ItemNetAmount__", toOrderAmount);
                item.SetValue("ItemNetAmountLocalCurrency__", new Sys.Decimal(toOrderAmount).mul(item.GetValue("ItemExchangeRate__")).toNumber());
                item.SetValue("ItemUndeliveredQuantity__", noGoodsReceipt ? 0 : toOrderQuantity);
                item.SetValue("ItemOpenAmount__", noGoodsReceipt ? 0 : toOrderAmount);
                if (toOrderQuantity <= 0) {
                    item.SetError("PRNumber__", "_Item over ordered {0}", toOrderQuantity);
                }
                item.SetValue("ItemRequestedUnitPrice__", unitPrice);
                item.SetValue("ItemTaxAmount__", new Sys.Decimal(toOrderAmount).mul(dbItem.GetValue("TaxRate__")).div(100).toNumber());
                item.SetValue("ItemTotalDeliveredQuantity__", 0);
                item.SetValue("ItemReceivedAmount__", 0);
                if (dbItem.GetValue("Status__") === "Canceled") {
                    item.SetError("PRNumber__", "_Item no longer orderable");
                }
                item.SetValue("ItemInitialRequestedDeliveryDate__", dbItem.GetValue("RequestedDeliveryDate__"));
                item.SetValue("RequestedBudgetID__", dbItem.GetValue("BudgetID__"));
                item.SetValue("ItemCurrency__", dbItem.GetValue("Currency__"));
                var requesterLogin = Sys.Helpers.String.ExtractLoginFromDN(dbItem.GetValue("RequesterDN__"));
                Sys.OnDemand.Users.CacheByLogin.Get(requesterLogin, Lib.P2P.attributesForUserCache).Then(function (result) {
                    var user = result[requesterLogin];
                    if (!user.$error) {
                        item.SetValue("ItemRequester__", user.displayname ? user.displayname : user.login);
                    }
                });
                var recipientLogin = Sys.Helpers.String.ExtractLoginFromDN(dbItem.GetValue("RecipientDN__"));
                Sys.OnDemand.Users.CacheByLogin.Get(recipientLogin, Lib.P2P.attributesForUserCache).Then(function (result) {
                    var user = result[recipientLogin];
                    if (!user.$error) {
                        item.SetValue("ItemRecipient__", user.displayname ? user.displayname : user.login);
                    }
                });
                return true;
            }
            POItems.CompleteFormItem = CompleteFormItem;
            function FillTaxSummary() {
                var taxCount = 0;
                var taxSummary = {};
                var addTaxFromLine = function (line) {
                    var taxCode = line.GetValue("ItemTaxCode__");
                    var netAmount = line.GetValue("ItemNetAmount__");
                    netAmount = netAmount ? netAmount : line.GetValue("Price__"); // if it comes from additional fees
                    if (taxSummary[taxCode]) {
                        taxSummary[taxCode].netAmount = new Sys.Decimal(taxSummary[taxCode].netAmount || 0).add(netAmount || 0).toNumber();
                    }
                    else if (netAmount) {
                        taxSummary[taxCode] =
                            {
                                taxRate: line.GetValue("ItemTaxRate__"),
                                netAmount: netAmount
                            };
                        taxCount++;
                    }
                };
                Sys.Helpers.Data.ForEachTableItem("LineItems__", addTaxFromLine);
                Sys.Helpers.Data.ForEachTableItem("AdditionalFees__", addTaxFromLine);
                var filter = "";
                var totalTaxAmount = new Sys.Decimal(0);
                var taxTable = Data.GetTable("TaxSummary__");
                if (taxTable.GetItemCount() != taxCount) {
                    taxTable.SetItemCount(taxCount);
                }
                taxCount = 0;
                Sys.Helpers.Object.ForEach(taxSummary, function (summary, taxCode) {
                    var item = taxTable.GetItem(taxCount++);
                    var taxAmount = new Sys.Decimal(summary.netAmount || 0).mul(summary.taxRate || 0).div(100).toNumber();
                    item.SetValue("TaxCode__", taxCode);
                    item.SetValue("TaxRate__", summary.taxRate);
                    item.SetValue("NetAmount__", summary.netAmount);
                    item.SetValue("TaxAmount__", taxAmount);
                    item.SetValue("TaxDescription__", "");
                    totalTaxAmount = totalTaxAmount.add(taxAmount);
                    filter += "(TaxCode__=" + taxCode + ")";
                });
                if (filter) {
                    filter = "(&(|(CompanyCode__=" + Data.GetValue("CompanyCode__") + ")(CompanyCode__=)(CompanyCode__!=*))(|" + filter + "))";
                    Sys.GenericAPI.Query("AP - Tax codes__", filter, ["TaxCode__", "Description__"], function (result, error) {
                        if (!error && result.length > 0) {
                            Sys.Helpers.Data.ForEachTableItem("TaxSummary__", function (item /*, i: number*/) {
                                var taxCode = item.GetValue("TaxCode__");
                                var foundTax = Sys.Helpers.Array.Find(result, function (tax) {
                                    return tax.TaxCode__ === taxCode;
                                });
                                if (foundTax) {
                                    item.SetValue("TaxDescription__", foundTax.Description__);
                                }
                            });
                        }
                    }, null, 100);
                }
                Data.SetValue("TotalTaxAmount__", totalTaxAmount.toNumber());
                Data.SetValue("TotalAmountIncludingVAT__", new Sys.Decimal(Data.GetValue("TotalNetAmount__")).add(totalTaxAmount).toNumber());
            }
            POItems.FillTaxSummary = FillTaxSummary;
            function QueryPRItems(context, filter) {
                return Sys.Helpers.Promise.Create(function (resolve, reject) {
                    Sys.GenericAPI.Query(PRItemsDBInfo.table, filter, ["*"], function (dbItems, error) {
                        if (error) {
                            Log.TimeEnd("Lib.Purchasing.POItems.FillForm");
                            reject("FillForm: error querying PR items with filter. Details: " + error);
                        }
                        else if (dbItems.length === 0) {
                            Log.TimeEnd("Lib.Purchasing.POItems.FillForm");
                            reject("FillForm: cannot find any PR items with filter: " + filter);
                        }
                        else {
                            // Before S152, client side edd queries do return technical fields ONLY on 1st record, so we disable the ordering
                            var allItemsHaveMsnOrMsnex = Sys.Helpers.Array.Every(dbItems, function (v) {
                                return v.GetValue("Msn") || v.GetValue("MsnEx");
                            });
                            if (Sys.Helpers.IsArray(context.options.orderByMsn) && allItemsHaveMsnOrMsnex) {
                                // by default we look for based on MsnEx
                                // If we don't find item based on this field the first time, we base on Msn
                                var searchOnMsnEx_1 = true;
                                dbItems = context.options.orderByMsn.map(function (msn) {
                                    var foundItem;
                                    if (searchOnMsnEx_1) {
                                        foundItem = Sys.Helpers.Array.Find(dbItems, function (item) {
                                            return item.GetValue("MsnEx") === msn;
                                        });
                                    }
                                    if (!foundItem) {
                                        searchOnMsnEx_1 = false;
                                        foundItem = Sys.Helpers.Array.Find(dbItems, function (item) {
                                            return item.GetValue("Msn") === msn;
                                        });
                                    }
                                    return foundItem;
                                });
                            }
                            context.dbItems = dbItems;
                            resolve(context);
                        }
                    }, context.options.orderByClause || "", null, {
                        recordBuilder: Sys.GenericAPI.BuildQueryResult,
                        fieldToTypeMap: PRItemsDBInfo.fieldsMap,
                        bigQuery: true,
                        queryOptions: "ReturnNullAttributes=1"
                    });
                });
            }
            function QueryAlreadyOrderedQuantity(context) {
                return Sys.Helpers.Promise.Create(function (resolve, reject) {
                    var poItemsFilter = "(|";
                    context.dbItems.forEach(function (dbItem /*, index: number*/) {
                        poItemsFilter += "(&(Status__!=Canceled)(PRNumber__=" + dbItem.GetValue(PRItemsSynchronizeConfig.tableKey) + ")(PRLineNumber__=" + dbItem.GetValue(PRItemsSynchronizeConfig.lineKey) + "))";
                    });
                    poItemsFilter += ")";
                    Log.Info("Fetching ordered information from PO Items: " + poItemsFilter);
                    Sys.GenericAPI.Query(POItemsDBInfo.table, poItemsFilter, ["PRNumber__", "PRLineNumber__", "OrderedQuantity__"], function (dbPOItems, error) {
                        if (error) {
                            Log.TimeEnd("Lib.Purchasing.POItems.FillForm");
                            reject("FillForm: error querying PO items for Quantity__ with filter. Details: " + error);
                        }
                        else {
                            if (dbPOItems.length !== 0) {
                                var poOrderedQty_1 = {};
                                dbPOItems.forEach(function (dbPOItem) {
                                    var prNumber = dbPOItem.GetValue("PRNumber__");
                                    var prItemNumber = dbPOItem.GetValue("PRLineNumber__");
                                    var qty = dbPOItem.GetValue("OrderedQuantity__");
                                    if (!poOrderedQty_1[prNumber]) {
                                        poOrderedQty_1[prNumber] = {};
                                    }
                                    if (!poOrderedQty_1[prNumber][prItemNumber]) {
                                        poOrderedQty_1[prNumber][prItemNumber] = 0;
                                    }
                                    poOrderedQty_1[prNumber][prItemNumber] = new Sys.Decimal(poOrderedQty_1[prNumber][prItemNumber]).add(qty).toNumber();
                                });
                                context.options.checkQtyWith = poOrderedQty_1;
                            }
                            resolve(context);
                        }
                    }, "", MAXRECORDS, {
                        recordBuilder: Sys.GenericAPI.BuildQueryResult,
                        fieldToTypeMap: POItemsDBInfo.fieldsMap
                    });
                });
            }
            function FillPOItems(context) {
                return Lib.Purchasing.Items.LoadForeignData(context.dbItems, PRItemsDBInfo)
                    .Then(function (foreignData) {
                    context.options.foreignData = foreignData;
                    context.fieldsInError = Lib.Purchasing.Items.FillFormItems(context.dbItems, PRItemsToPO, context.options);
                    return context;
                });
            }
            function InitConfiguration(context) {
                return Lib.Purchasing.SetERPByCompanyCode(Data.GetValue("CompanyCode__"))
                    .Then(function () {
                    Lib.P2P.InitSAPConfiguration(Lib.ERP.GetERPName(), "PAC");
                    return context;
                });
            }
            /**
             * Fill the purchase order form (header + items) according to the information stored in items.
             */
            function CompleteForm(context) {
                return Sys.Helpers.Promise.Create(function (resolve) {
                    Lib.Purchasing.Items.ComputeTotalAmount();
                    FillTaxSummary();
                    Data.SetValue("OrderStatus__", "To order");
                    if (Sys.ScriptInfo.IsServer()) {
                        Data.SetValue("BuyerName__", Lib.Purchasing.GetOwner().GetVars().GetValue_String("DisplayName", 0));
                        Data.SetValue("BuyerLogin__", Lib.Purchasing.GetOwner().GetVars().GetValue_String("login", 0));
                    }
                    else {
                        Data.SetValue("BuyerName__", Sys.Helpers.Globals.User.fullName);
                        Data.SetValue("BuyerLogin__", Sys.Helpers.Globals.User.loginId);
                    }
                    var promises = [Lib.Purchasing.Vendor.Init(), Lib.Purchasing.ShipTo.Init(), Lib.Purchasing.POItems.SetLocalCurrency()];
                    Sys.Helpers.Synchronizer.All(promises)
                        .Then(function () {
                        resolve(context);
                    });
                });
            }
            function SetLocalCurrency() {
                return Sys.Helpers.Promise.Create(function (resolve, reject) {
                    var companyCode = Data.GetValue("CompanyCode__");
                    if (companyCode) {
                        Lib.P2P.CompanyCodesValue.QueryValues(companyCode, true).Then(function (CCValues) {
                            if (Object.keys(CCValues).length > 0) {
                                var currency = CCValues.Currency__;
                                Data.SetValue("LocalCurrency__", currency);
                                resolve(currency);
                            }
                            else {
                                reject("This CompanyCode does not exist in the table.");
                            }
                        });
                    }
                    else {
                        reject("Empty company code.");
                    }
                });
            }
            POItems.SetLocalCurrency = SetLocalCurrency;
            function CheckError(context) {
                return Sys.Helpers.Promise.Create(function (resolve, reject) {
                    if (context.fieldsInError.length > 0) {
                        // Reject with error message of the first field
                        reject(PRItemsToPO.errorMessages[context.fieldsInError[0]] || "Some items have different values on the following fields: " + context.fieldsInError.join(", "));
                    }
                    else {
                        resolve(context);
                    }
                });
            }
            /**
             * Fill the purchase order form according to the selected PR items by the specified filter.
             * @param {string} filter selected DB items used to fill form items.
             * @param {object} options
             * @returns {promise}
             */
            function FillForm(filter, options) {
                Log.Time("Lib.Purchasing.POItems.FillForm");
                var context = {
                    options: null
                };
                context.options = options || {};
                context.options.fillItem = options.fillItem || CompleteFormItem;
                return Sys.Helpers.Promise.Create(function (resolve, reject) {
                    QueryPRItems(context, filter) //add context.dbItems
                        .Then(QueryAlreadyOrderedQuantity) //add context.options.checkQtyWith
                        .Then(FillPOItems) //add context.options.foreignData  & context.fieldsInError
                        .Then(InitConfiguration) // Use Setted CompanyCode
                        .Then(CompleteForm) // Use Setted Configuration
                        .Then(CheckError) // check context.fieldsInError
                        .Then(function () {
                        Log.TimeEnd("Lib.Purchasing.POItems.FillForm");
                        resolve();
                    })
                        .Catch(function (rejectMsg) {
                        Log.TimeEnd("Lib.Purchasing.POItems.FillForm");
                        reject(rejectMsg);
                    });
                });
            }
            POItems.FillForm = FillForm;
            function UpdatePOFromGR(callback, computedData) {
                Log.Info("Update PO items according to the GR items (goods) with number '" + Data.GetValue("OrderNumber__") + "'");
                var lastError = null;
                // 1 - retrieve GR items for this purchase order
                var grItems = null;
                var sync = Sys.Helpers.Synchronizer.Create(function () {
                    if (lastError != null) {
                        throw new Error(lastError);
                    }
                    // 2 - update global status and compute some data for lines (use in synchronization) of the PR
                    var openOrderLocalCurrency = new Sys.Decimal(0);
                    var deliveryDate = new Date("01/01/1970");
                    var hasGoodsReceipt = false;
                    var stillWaitingForReception = false;
                    var todayDate = new Date();
                    todayDate.setUTCHours(12, 0, 0, 0);
                    var noGoodsReceiptItems = null;
                    Sys.Helpers.Data.ForEachTableItem("LineItems__", function (line) {
                        var lineNumber = line.GetValue("LineItemNumber__");
                        var LineDeliveryDate = new Date("01/01/1970");
                        var lineStatus = Data.GetValue("OrderStatus__");
                        var lineReceivedQuantity = new Sys.Decimal(0);
                        var lineOpenQuantity = new Sys.Decimal(line.GetValue("NoGoodsReceipt__") ? 0 : line.GetValue("ItemQuantity__"));
                        var lineReceivedAmount = new Sys.Decimal(0);
                        var lineOpenAmount = new Sys.Decimal(line.GetValue("NoGoodsReceipt__") ? 0 : line.GetValue("ItemNetAmount__"));
                        var grItemsForLine = grItems[lineNumber];
                        if (grItemsForLine) {
                            hasGoodsReceipt = true;
                            grItemsForLine.forEach(function (grItem) {
                                lineReceivedAmount = lineReceivedAmount.add(grItem.GetValue("Amount__"));
                                lineReceivedQuantity = lineReceivedQuantity.add(grItem.GetValue("Quantity__"));
                                if (grItem.GetValue("DeliveryCompleted__")) {
                                    lineOpenQuantity = new Sys.Decimal(0);
                                    lineOpenAmount = new Sys.Decimal(0);
                                }
                                if (!lineOpenQuantity.equals(0)) {
                                    lineOpenAmount = lineOpenAmount.minus(grItem.GetValue("Amount__"));
                                    lineOpenQuantity = lineOpenQuantity.minus(grItem.GetValue("Quantity__"));
                                }
                                LineDeliveryDate = Sys.Helpers.Date.CompareDate(grItem.GetValue("DeliveryDate__"), LineDeliveryDate) > 0 ? grItem.GetValue("DeliveryDate__") : LineDeliveryDate;
                            });
                            lineOpenQuantity = Sys.Decimal.max(0, lineOpenQuantity); // Avoid negative values for OpenQuantity
                            if (lineOpenQuantity.greaterThan(0)) {
                                lineStatus = "To receive";
                            }
                            else {
                                lineStatus = "Received";
                            }
                        }
                        else if (line.GetValue("NoGoodsReceipt__") && Sys.Helpers.Date.CompareDate(todayDate, line.GetValue("ItemRequestedDeliveryDate__")) > 0) {
                            Log.Info("PO with line number '" + lineNumber + "' has reached its requested delivery date and does not need goods receipt");
                            hasGoodsReceipt = true;
                            LineDeliveryDate = line.GetValue("ItemRequestedDeliveryDate__");
                            lineStatus = "Received";
                            if (!line.GetValue("ItemDeliveryComplete__")) {
                                if (!noGoodsReceiptItems) {
                                    noGoodsReceiptItems = [];
                                }
                                noGoodsReceiptItems.push(line);
                            }
                        }
                        else {
                            Log.Info("PO line number '" + lineNumber + "' not yet received");
                        }
                        deliveryDate = Sys.Helpers.Date.CompareDate(LineDeliveryDate, deliveryDate) > 0 ? LineDeliveryDate : deliveryDate;
                        openOrderLocalCurrency = openOrderLocalCurrency.add(lineOpenQuantity.mul(line.GetValue("ItemUnitPrice__")).mul(line.GetValue("ItemExchangeRate__")));
                        // byline computed data
                        line.SetValue("ItemReceivedAmount__", lineReceivedAmount.toNumber());
                        line.SetValue("ItemOpenAmount__", lineOpenAmount.toNumber());
                        line.SetValue("ItemTotalDeliveredQuantity__", lineReceivedQuantity.toNumber());
                        line.SetValue("ItemUndeliveredQuantity__", lineOpenQuantity.toNumber());
                        line.SetValue("ItemDeliveryComplete__", lineStatus === "Received" ? 1 : 0);
                        line.SetValue("ItemDeliveryDate__", Sys.Helpers.Date.CompareDate(LineDeliveryDate, new Date("01/01/1970")) == 0 ? null : LineDeliveryDate);
                        if (lineStatus != "Received") {
                            stillWaitingForReception = true;
                        }
                        if (computedData) {
                            Sys.Helpers.Extend(computedData.byLine[lineNumber], {
                                "Status": lineStatus
                            });
                        }
                    });
                    var status = Data.GetValue("OrderStatus__");
                    // global status
                    if (!hasGoodsReceipt) {
                        status = Data.GetValue("OrderStatus__");
                    }
                    else if (stillWaitingForReception) {
                        status = "To receive";
                    }
                    else {
                        status = "Received";
                        Data.SetValue("DeliveredDate__", deliveryDate);
                    }
                    Log.Info("Set Order Status to '" + status + "' with openOrderLocalCurrency : '" + openOrderLocalCurrency.toNumber() + "'");
                    Data.SetValue("OrderStatus__", status);
                    Data.SetValue("OpenOrderLocalCurrency__", openOrderLocalCurrency.toNumber());
                    var hasCanceledGoodsReceipt = !!Variable.GetValueAsString("LastCanceledReceptionData");
                    callback(hasGoodsReceipt || hasCanceledGoodsReceipt, hasGoodsReceipt, noGoodsReceiptItems);
                });
                var grItemsFilter = "(&(!(Status__=Canceled))(OrderNumber__=" + Data.GetValue("OrderNumber__") + "))";
                var promise = Lib.Purchasing.Items.GetItemsForDocument(Lib.Purchasing.Items.GRItemsDBInfo, grItemsFilter, "LineNumber__")
                    .Then(function (items) { grItems = items; })
                    .Catch(function (err) { lastError = err; });
                sync.Register(promise);
                sync.Start();
            }
            POItems.UpdatePOFromGR = UpdatePOFromGR;
            /**
             * Returns all items in PO form by PR number.
             * @returns {object} items map by PR number
             */
            function GetPRItemsInForm() {
                var allPRItems = {};
                Sys.Helpers.Data.ForEachTableItem("LineItems__", function (line) {
                    var prNumber = line.GetValue("PRNumber__");
                    if (!(prNumber in allPRItems)) {
                        allPRItems[prNumber] = [];
                    }
                    allPRItems[prNumber].push(line);
                });
                return allPRItems;
            }
            POItems.GetPRItemsInForm = GetPRItemsInForm;
            function UpdatePOFromPR(editOrder) {
                if (editOrder === void 0) { editOrder = false; }
                var allPRItems = GetPRItemsInForm();
                // Get the PR Items filter
                var prItemsFilter = "(|";
                Sys.Helpers.Object.ForEach(allPRItems, function (prItems, prNumber) {
                    prItems.forEach(function (prItem) {
                        prItemsFilter += "(&(PRNumber__=" + prNumber + ")(LineNumber__=" + prItem.GetValue("PRLineNumber__") + "))";
                    });
                });
                prItemsFilter += ")";
                return Lib.Purchasing.Items.GetItemsForDocument(Lib.Purchasing.Items.PRItemsDBInfo, prItemsFilter, "PRNumber__")
                    .Then(function (prItems) {
                    Sys.Helpers.Data.ForEachTableItem("LineItems__", function (line) {
                        var prNumber = line.GetValue("PRNumber__");
                        var prLineNumber = line.GetValue("PRLineNumber__");
                        var prLine = null;
                        if (prItems && prItems[prNumber]) {
                            prLine = Sys.Helpers.Array.Find(prItems[prNumber], function (prL) {
                                return prL.GetValue("LineNumber__") == prLineNumber;
                            });
                        }
                        var lineIsOrderable = false;
                        if (prLine) {
                            line.SetValue("OrderableQuantity__", prLine.GetValue("CanceledQuantity__"));
                            line.SetValue("OrderableAmount__", prLine.GetValue("CanceledAmount__"));
                            // check status
                            var isToOrder = Purchasing.PRStatus.ForPOWorkflow.indexOf(prLine.GetValue("Status__")) !== -1;
                            var isNotInPRWorkflow = Purchasing.PRStatus.ForPRWorkflow.indexOf(prLine.GetValue("Status__")) === -1;
                            lineIsOrderable = (editOrder && isNotInPRWorkflow) || (!editOrder && isToOrder);
                        }
                        if (!lineIsOrderable) {
                            line.SetError("PRNumber__", "_Item no longer orderable");
                        }
                    });
                });
            }
            function UpdatePOFromPO() {
                var allPRItems = GetPRItemsInForm();
                //Get the PO Items already created for each PR Items present in this Order to retrieve alreadyOrderedQuantity quantity
                var poItemsFilter = "(|";
                Sys.Helpers.Object.ForEach(allPRItems, function (prItems, prNumber) {
                    prItems.forEach(function (prItem) {
                        poItemsFilter += "(&(Status__!=Canceled)(PRNumber__=" + prNumber + ")(PRLineNumber__=" + prItem.GetValue("PRLineNumber__") + "))";
                    });
                });
                poItemsFilter += ")";
                return Lib.Purchasing.Items.GetItemsForDocument(Lib.Purchasing.Items.POItemsDBInfo, poItemsFilter, "PRNumber__")
                    .Then(function (poItems) {
                    var poRuidex = Data.GetValue("RUIDEX");
                    Sys.Helpers.Data.ForEachTableItem("LineItems__", function (line) {
                        var prNumber = line.GetValue("PRNumber__");
                        var prLineNumber = line.GetValue("PRLineNumber__");
                        var requestedQuantity = line.GetValue("ItemRequestedQuantity__");
                        //reset and set in UpdatePOFromPR with CanceledQuantity from pr item
                        var canceledQuantity = line.GetValue("OrderableQuantity__");
                        var orderedQuantity = line.GetValue("ItemQuantity__");
                        var alreadyOrderedQuantity = new Sys.Decimal(canceledQuantity || 0);
                        if (poItems && poItems[prNumber] && poItems[prNumber].length > 0) {
                            poItems[prNumber].forEach(function (poLine) {
                                // Ignore items from this PO
                                if (poLine.GetValue("PRLineNumber__") == prLineNumber && poLine.GetValue("PORUIDEX__") !== poRuidex) {
                                    alreadyOrderedQuantity = alreadyOrderedQuantity.add(poLine.GetValue("OrderedQuantity__"));
                                }
                            });
                        }
                        var orderableQuantity = new Sys.Decimal(requestedQuantity).minus(alreadyOrderedQuantity).toNumber();
                        var orderableAmount = new Sys.Decimal(line.GetValue("ItemRequestedUnitPrice__") || 0).mul(orderableQuantity).toNumber();
                        line.SetValue("OrderableQuantity__", orderableQuantity);
                        line.SetValue("OrderableAmount__", orderableAmount);
                        if (Lib.Purchasing.Items.IsQuantityBasedItem(line)) {
                            var overOrderedQuantity = alreadyOrderedQuantity.add(orderedQuantity).minus(requestedQuantity);
                            if (overOrderedQuantity.greaterThan(0)) {
                                line.SetError("PRNumber__", "_Item over ordered {0}", overOrderedQuantity.toNumber());
                            }
                        }
                        else {
                            var customChangeAllowed = Sys.Helpers.TryCallFunction("Lib.PO.Customization.Common.IsItemUnitPriceChangeAllowed", line);
                            var changeAllowed = customChangeAllowed || (customChangeAllowed === null && Lib.Purchasing.CheckPO.IsTotalPriceTolerated(requestedQuantity, orderableQuantity, orderedQuantity));
                            if (!changeAllowed) {
                                line.SetError("ItemNetAmount__", "_The price variance between purchase order and purchase requisition exceeds the tolerance limit");
                            }
                        }
                    });
                });
            }
            function UpdatePOFromPRAndPO(editOrder) {
                if (editOrder === void 0) { editOrder = false; }
                return UpdatePOFromPR(editOrder)
                    .Then(UpdatePOFromPO);
            }
            POItems.UpdatePOFromPRAndPO = UpdatePOFromPRAndPO;
            function CheckDataCoherency() {
                var currency = Data.GetValue("Currency__");
                var companyCode = Data.GetValue("CompanyCode__");
                var error = false;
                Sys.Helpers.Data.ForEachTableItem("LineItems__", function (item) {
                    item.SetError("ItemCurrency__", currency !== item.GetValue("ItemCurrency__") ? "_Only items with the same currency can be selected." : "");
                    item.SetError("ItemCompanyCode__", companyCode !== item.GetValue("ItemCompanyCode__") ? "_Only items from the same company can be selected." : "");
                    if (currency !== item.GetValue("ItemCurrency__") || companyCode !== item.GetValue("ItemCompanyCode__")) {
                        error = true;
                    }
                });
                return error;
            }
            POItems.CheckDataCoherency = CheckDataCoherency;
            function GoodsReceiptNeeded() {
                var goodsReceiptNeeded = false;
                Sys.Helpers.Data.ForEachTableItem("LineItems__", function (line) {
                    if (!line.GetValue("NoGoodsReceipt__") && line.GetValue("ItemUndeliveredQuantity__") > 0) {
                        goodsReceiptNeeded = true;
                    }
                });
                return goodsReceiptNeeded;
            }
            POItems.GoodsReceiptNeeded = GoodsReceiptNeeded;
            function HasDeliveredItems() {
                var hasDeliveredItems = false;
                Sys.Helpers.Data.ForEachTableItem("LineItems__", function (line) {
                    if (!line.GetValue("NoGoodsReceipt__") && line.GetValue("ItemUndeliveredQuantity__") == 0) {
                        hasDeliveredItems = true;
                    }
                });
                return hasDeliveredItems;
            }
            POItems.HasDeliveredItems = HasDeliveredItems;
            function QueryPOItems(params) {
                return Sys.Helpers.Promise.Create(function (resolve, reject) {
                    var _a;
                    var filters = [];
                    var PONumber;
                    var PORUIDEX = params.ancestorsids;
                    if (PORUIDEX) {
                        filters.push(Sys.Helpers.LdapUtil.FilterEqual("PORUIDEX__", PORUIDEX));
                    }
                    else {
                        PONumber = params.orderNumber || Variable.GetValueAsString("OrderNumber__");
                        if (PONumber) {
                            filters.push(Sys.Helpers.LdapUtil.FilterEqual("PONumber__", PONumber));
                        }
                    }
                    if (PORUIDEX || PONumber) {
                        filters.push(Sys.Helpers.LdapUtil.FilterNotEqual("NoGoodsReceipt__", "true"));
                        if (((_a = params.additionnalFilters) === null || _a === void 0 ? void 0 : _a.length) > 0) {
                            filters = filters.concat(params.additionnalFilters);
                        }
                        var filter_1 = Sys.Helpers.LdapUtil.FilterAnd.apply(null, filters);
                        Sys.GenericAPI.Query(POItemsDBInfo.table, filter_1.toString(), ["*"], function (dbItems, error) {
                            if (error) {
                                reject("FillForm: error querying PO items with filter. Details: " + error);
                            }
                            else if (dbItems.length === 0) {
                                reject("FillForm: cannot find any PO items with filter: " + filter_1);
                            }
                            else {
                                try {
                                    // need to sort by line number casted in integer (PO item LineNumber__ is a string...)
                                    dbItems.sort(function (dbItem1, dbItem2) {
                                        // Here GetValue returns integer because we provide a fieldToTypeMap to the recordBuilder
                                        return dbItem1.GetValue("LineNumber__") - dbItem2.GetValue("LineNumber__");
                                    });
                                    resolve(dbItems);
                                }
                                catch (e) {
                                    reject(e.toString());
                                }
                            }
                        }, params.orderByClause || "", null, {
                            recordBuilder: Sys.GenericAPI.BuildQueryResult,
                            fieldToTypeMap: POItemsDBInfo.fieldsMap,
                            bigQuery: true
                        });
                    }
                    else {
                        reject("FillForm: error, no PO Number");
                    }
                });
            }
            POItems.QueryPOItems = QueryPOItems;
            function CheckContainsQuantityBasedItem(orderNumber) {
                return Sys.Helpers.Promise.Create(function (resolve, reject) {
                    if (orderNumber) {
                        Lib.Purchasing.POItems.QueryPOItems({ orderNumber: orderNumber }).Then(function (dbItems) {
                            resolve(!Sys.Helpers.Array.Every(dbItems, function (dbItem) {
                                return dbItem.GetValue("ItemType__") !== Lib.P2P.ItemType.QUANTITY_BASED;
                            }));
                        }).Catch(function () {
                            resolve(false);
                        });
                    }
                    else {
                        reject("cannot check if PO contains order : missing PO number");
                    }
                });
            }
            POItems.CheckContainsQuantityBasedItem = CheckContainsQuantityBasedItem;
            function FillItems(queryParams, itemType) {
                return Sys.Helpers.Promise.Create(function (resolve) {
                    Lib.Purchasing.POItems.QueryPOItems(queryParams)
                        .Then(function (dbItems) {
                        Lib.Purchasing.Items.FillFormItems(dbItems, itemType);
                        resolve(dbItems);
                    });
                });
            }
            POItems.FillItems = FillItems;
        })(POItems = Purchasing.POItems || (Purchasing.POItems = {}));
    })(Purchasing = Lib.Purchasing || (Lib.Purchasing = {}));
})(Lib || (Lib = {}));
