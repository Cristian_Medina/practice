/* eslint no-empty-function: "off", no-unused-vars: "off" */
/**
 * @file Lib.AP.Customization.VendorPortal.HTMLScripts library
 * @namespace Lib.AP.Customization.VendorPortal.HTMLScripts
 */


// eslint-disable-next-line no-redeclare
var Lib = Lib || {};
Lib.AP = Lib.AP || {};
Lib.AP.Customization = Lib.AP.Customization || {};
Lib.AP.Customization.VendorPortal = Lib.AP.Customization.VendorPortal || {};

(function (parentLib)
{
	/**
	 * @lends Lib.AP.Customization.VendorPortal.HTMLScripts
	 */
	parentLib.VendorPortal.HTMLScripts =
	{
		/**
		* This user exit is called at the end of the HTML page script of the Vendor Portal Customer Invoice.
		* @memberof Lib.AP.Customization.VendorPortal.HTMLScripts
		* @example
		* <pre><code>
		* OnHTMLScriptEnd: function ()
		* {
		*	var options =
		*	{
		*		ignoreIfExists: false,
		*		notifyByEmail: true,
		*		notifyAllUsersInGroup: false,
		*		emailCustomTags: {
		*			"VIMsnEx": Controls.VIRuidEx__.GetValue().split('.')[1],
		*			"URLDisplayText": "Cliquez sur ce lien pour afficher la conversation"
		*		}
		*	};
		*
		*	Controls.ConversationUI__.SetOptions(options);
		* }
		* </code></pre>
		*/
		OnHTMLScriptEnd: function()
		{
		},

		/**
		 * This user exit is called at the end of the HTML page script of the Vendor Portal Customer Order.
		* @memberof Lib.AP.Customization.VendorPortal.HTMLScripts
		* @example
		* <pre><code>
		* OnHTMLScriptEnd: function ()
		* {
		*	// Display a button that creates a new process
		*	Controls.ServiceEntrySheet__.Hide(false);
		*	Controls.ServiceEntrySheet__.SetDisabled(false);
		*	Controls.ServiceEntrySheet__.OnClick = function ()
		*	{
		*		Process.CreateProcessInstance("Service entry sheet");
		*	}
		* }
		* </code></pre>
		 */
		OnCustomerOrderHTMLScriptEnd: function()
		{
		}
	};
})(Lib.AP.Customization);
