///#GLOBALS Lib Sys
/* globals setTimeout*/
/* LIB_DEFINITION{
  "name": "Lib_Purchasing_Vendor",
  "libraryType": "LIB",
  "scriptType": "COMMON",
  "comment": "Purchasing library",
  "require": [
    "Sys/Sys_Helpers",
    "Sys/Sys_Helpers_Array",
    "Sys/Sys_Helpers_String_SAP",
    "Sys/Sys_Parameters",
    "[Sys/Sys_GenericAPI_Server]",
    "[Sys/Sys_GenericAPI_Client]",
    "Lib_Purchasing_V12.0.425.0",
    "Lib_Purchasing_ShipTo_V12.0.425.0",
    "Lib_ERP_V12.0.425.0",
    "Sys/Sys_TechnicalData"
  ]
}*/
// Common interface
var Lib;
(function (Lib) {
    var Purchasing;
    (function (Purchasing) {
        var Vendor;
        (function (Vendor) {
            var g = Sys.Helpers.Globals;
            // Cache.
            var _vendorNumber;
            var _culture;
            var _language;
            /**
             * PS can add those line in Lib.P2P.Customization.Common
             * if(Lib.Purchasing && Lib.Purchasing.Vendor)
             * {
             * 		Lib.Purchasing.Vendor.defaultCountryCode = "US";
             * }
             */
            Vendor.defaultCountryCode = "";
            var lastSelectedVendorName = Data.GetValue("VendorName__");
            var vendorFieldsMapping = {
                "Number__": "VendorNumber__",
                "Name__": "VendorName__",
                "Email__": "VendorEmail__",
                "VATNumber__": "VendorVatNumber__",
                "FaxNumber__": "VendorFaxNumber__",
                "PaymentTermCode__": "PaymentTermCode__",
                "PaymentTermCode__.Description__": "PaymentTermDescription__"
            };
            function QueryVendorTableEmail(companyCode, vendorNumber, callback) {
                var filter = "&(Number__=" + vendorNumber + ")(|(CompanyCode__=" + companyCode + ")(CompanyCode__=)(CompanyCode__!=*))";
                Sys.GenericAPI.Query("AP - Vendors__", filter, ["Email__"], function (result, error) {
                    var email = !error && result && result.length > 0 ? result[0].Email__ : null;
                    callback(email, error);
                }, "", 1);
            }
            // Search firstly in ODUSER via "AP - Vendors links"
            // If not found or non email defined, use "AP - Vendors__"
            function QueryVendorContactEmail(companyCode, vendorNumber, callback) {
                var accountId = Sys.ScriptInfo.IsClient() ? g.User.accountId : Lib.P2P.GetValidatorOrOwner().GetValue("AccountId");
                var filter = "&(Number__=" + vendorNumber + ")(|(CompanyCode__=" + companyCode + ")(CompanyCode__=)(CompanyCode__!=*))";
                Sys.GenericAPI.Query("AP - Vendors links__", filter, ["ShortLoginPAC__", "ShortLogin__"], function (result, error) {
                    if (!error && result && result.length > 0) {
                        var vendorLogin_1 = result[0].ShortLoginPAC__;
                        if (!vendorLogin_1) {
                            vendorLogin_1 = result[0].ShortLogin__;
                        }
                        vendorLogin_1 = accountId + "$" + vendorLogin_1;
                        filter = "&(VENDOR=1)(Login=" + vendorLogin_1 + ")";
                        Sys.GenericAPI.Query("ODUSER", filter, ["EmailAddress", "Language", "Culture"], function (result2, error2) {
                            _language = !error2 && result2 && result2.length > 0 ? result2[0].Language : null;
                            _culture = !error2 && result2 && result2.length > 0 ? result2[0].Culture : null;
                            if (_language != null && _culture != null) {
                                _vendorNumber = vendorNumber;
                            }
                            var email = !error2 && result2 && result2.length > 0 ? result2[0].EmailAddress : null;
                            if (email) {
                                Variable.SetValueAsString("ContactLogin", vendorLogin_1);
                                callback(email);
                            }
                            else {
                                Variable.SetValueAsString("ContactLogin", "");
                                QueryVendorTableEmail(companyCode, vendorNumber, callback);
                            }
                        }, null, 1);
                    }
                    else {
                        QueryVendorTableEmail(companyCode, vendorNumber, callback);
                    }
                }, null, 1);
            }
            function GetVendorInfo(callback) {
                var lastSelectedVendorNumber = Data.GetValue("VendorNumber__");
                if (lastSelectedVendorNumber != _vendorNumber) {
                    _vendorNumber = Data.GetValue("VendorNumber__");
                    _culture = null;
                    _language = null;
                    QueryVendorContactEmail(Data.GetValue("CompanyCode__"), _vendorNumber, function ( /*email: string*/) {
                        callback({ culture: _culture, language: _language });
                    });
                }
                else {
                    callback({ culture: _culture, language: _language });
                }
            }
            Vendor.GetVendorInfo = GetVendorInfo;
            function OnChange() {
                // If we're here, then the user *manually* typed something in the vendor name that doesn't match any vendor in the database.
                // Then:
                //  - reset the new vendor request
                //  - set an error telling the vendor doesn't exists (only if we're the requester or the buyer)
                //
                if (Data.GetValue("VendorName__") !== lastSelectedVendorName) {
                    Log.Info("Vendor name changed to '" + Data.GetValue("VendorName__") + "'");
                    Variable.SetValueAsString("NewVendor__", "");
                    lastSelectedVendorName = Data.GetValue("VendorName__");
                    Reset();
                    Data.SetValue("VendorName__", lastSelectedVendorName);
                    if (!Sys.Helpers.IsEmpty(Data.GetValue("VendorName__"))) {
                        Data.SetError("VendorName__", Language.Translate("Unknown vendor"));
                    }
                }
            }
            Vendor.OnChange = OnChange;
            function OnSelectItem(item) {
                var vendorSelected = function () {
                    Log.Info("Vendor selected");
                    Variable.SetValueAsString("NewVendor__", "");
                    lastSelectedVendorName = Data.GetValue("VendorName__");
                    QueryVendorContactEmail(Data.GetValue("CompanyCode__"), item.GetValue("Number__"), function (email) {
                        Fill(function (fieldName) {
                            return fieldName === "Email__" ? email : item.GetValue(fieldName);
                        });
                    });
                    // In Punchout mode, items must be in error if they don't match the selected vendor
                    if (Lib.Purchasing.Punchout && Lib.Purchasing.Punchout.PO && Lib.Purchasing.Punchout.PO.IsEnabled()) {
                        Lib.Purchasing.Punchout.PO.UpdatePane(item.GetValue("Number__"));
                    }
                };
                if (!Sys.Helpers.IsEmpty(Data.GetValue("PaymentTermCode__"))
                    || !Sys.Helpers.IsEmpty(Data.GetValue("VendorEmail__"))) {
                    var lastLastSelectedVendorName_1 = lastSelectedVendorName;
                    lastSelectedVendorName = Data.GetValue("VendorName__");
                    setTimeout(function () {
                        Lib.CommonDialog.PopupYesCancel(function (action) {
                            if (action === "Yes") {
                                vendorSelected();
                            }
                            else {
                                // Reset previous selected vendor
                                lastSelectedVendorName = lastLastSelectedVendorName_1;
                                Data.SetValue("VendorName__", lastSelectedVendorName);
                            }
                        }, "_Warning", "_Any fields related to the vendor will be reset. Are you sure you want to continue?", "_Yes", "_No");
                    });
                }
                else {
                    vendorSelected();
                }
            }
            Vendor.OnSelectItem = OnSelectItem;
            function IsEmpty() {
                return Sys.Helpers.IsEmpty(Data.GetValue("VendorNumber__"))
                    || Sys.Helpers.IsEmpty(Data.GetValue("VendorName__"))
                    || Sys.Helpers.IsEmpty(Data.GetValue("VendorAddress__"))
                    || Sys.Helpers.IsEmpty(Data.GetValue("VendorEmail__"));
            }
            Vendor.IsEmpty = IsEmpty;
            function FillPostalAddress(VendorAddress) {
                Log.Time("Vendor.FillPostalAddress");
                var fromCountryCode = Lib.Purchasing.Vendor.defaultCountryCode;
                if (VendorAddress.ForceCountry === "false") {
                    fromCountryCode = VendorAddress.ToCountry;
                }
                else if (!fromCountryCode) {
                    fromCountryCode = "US";
                }
                var options = {
                    "isVariablesAddress": true,
                    "address": VendorAddress,
                    "countryCode": fromCountryCode // Get country code from contract ModProvider
                };
                return Sys.Helpers.Promise.Create(function (resolve /*, reject*/) {
                    var OnVendorAddressFormatResult = Sys.Helpers.TryCallFunction("Lib.P2P.Customization.Common.OnVendorAddressFormat", options);
                    if (OnVendorAddressFormatResult === false || OnVendorAddressFormatResult === null) {
                        Sys.GenericAPI.CheckPostalAddress(options, function (address) {
                            Log.Info("CheckPostalAddress input:  " + JSON.stringify(VendorAddress));
                            Log.Info("CheckPostalAddress output: " + JSON.stringify(address));
                            if (!Sys.Helpers.IsEmpty(address.LastErrorMessage)) {
                                Data.SetValue("VendorAddress__", "");
                                Data.SetWarning("VendorAddress__", Language.Translate("_Vendor address error:") + " " + address.LastErrorMessage);
                            }
                            else {
                                Data.SetValue("VendorAddress__", address.FormattedBlockAddress.replace(/^[^\r\n]+(\r|\n)+/, ""));
                            }
                            Log.TimeEnd("Vendor.FillPostalAddress");
                            resolve();
                        });
                    }
                    else {
                        Data.SetValue("VendorAddress__", OnVendorAddressFormatResult);
                        //Formated by user exit
                        resolve();
                    }
                });
            }
            Vendor.FillPostalAddress = FillPostalAddress;
            function Fill(GetValueFct) {
                Log.Time("Vendor.Fill");
                var field;
                for (field in vendorFieldsMapping) {
                    if (Object.prototype.hasOwnProperty.call(vendorFieldsMapping, field)) {
                        Data.SetValue(vendorFieldsMapping[field], GetValueFct(field) || "");
                    }
                }
                Data.SetError("VendorName__", "");
                var shipToAddress = JSON.parse(Variable.GetValueAsString("ShipToAddress") || null);
                var sameCountry = shipToAddress && (shipToAddress.ToCountry == GetValueFct("Country__"));
                var VendorAddress = {
                    "ToName": "ToRemove",
                    "ToSub": GetValueFct("Sub__"),
                    "ToMail": GetValueFct("Street__"),
                    "ToPostal": GetValueFct("PostalCode__"),
                    "ToCountry": GetValueFct("Country__"),
                    "ToCountryCode": GetValueFct("Country__"),
                    "ToState": GetValueFct("Region__"),
                    "ToCity": GetValueFct("City__"),
                    "ToPOBox": GetValueFct("PostOfficeBox__"),
                    "ForceCountry": sameCountry ? "false" : "true"
                };
                Variable.SetValueAsString("VendorAddress", JSON.stringify(VendorAddress));
                var promises = [];
                promises.push(FillPostalAddress(VendorAddress));
                if (shipToAddress) {
                    Log.Info("Filling shipto from vendor");
                    shipToAddress.ForceCountry = VendorAddress.ForceCountry;
                    Variable.SetValueAsString("ShipToAddress", JSON.stringify(shipToAddress));
                    Sys.TechnicalData.SetValue("ShipToAddress", shipToAddress);
                    promises.push(Lib.Purchasing.ShipTo.FillPostalAddress(shipToAddress));
                }
                return Sys.Helpers.Synchronizer.All(promises)
                    .Then(function () {
                    Log.TimeEnd("Vendor.Fill");
                });
            }
            Vendor.Fill = Fill;
            function Reset() {
                var field;
                for (field in vendorFieldsMapping) {
                    if (Object.prototype.hasOwnProperty.call(vendorFieldsMapping, field)) {
                        Data.SetValue(vendorFieldsMapping[field], null);
                        Data.SetError(vendorFieldsMapping[field], "");
                    }
                }
                Data.SetValue("VendorAddress__", null);
                Data.SetError("VendorAddress__", "");
                Variable.SetValueAsString("VendorAddress", "");
            }
            Vendor.Reset = Reset;
            function QueryVendor(companyCode, vendorNumber, callback) {
                function CompletVendorEmail(vendor) {
                    QueryVendorContactEmail(companyCode, vendorNumber, function (email) {
                        vendor.Email__ = email;
                        callback(vendor);
                    });
                }
                if (Lib.ERP.IsSAP() && Sys.Parameters.GetInstance("P2P_" + Lib.ERP.GetERPName()).GetParameter("BrowseMasterDataFromERP")) {
                    var sapField2FFFieldMapping_1 = {
                        BUKRS: "CompanyCode__",
                        NAME1: "Name__",
                        LIFNR: "Number__",
                        STRAS: "Street__",
                        PFACH: "PostOfficeBox__",
                        ORT01: "City__",
                        PSTLZ: "PostalCode__",
                        REGIO: "Region__",
                        LAND1: "Country__",
                        TELFX: "FaxNumber__",
                        STCEG: "VATNumber__",
                        ZTERM: "PaymentTermCode__"
                    };
                    var config_1 = Variable.GetValueAsString("SAPConfiguration");
                    var filter_1 = "BUKRS = '" + companyCode + "' AND LIFNR = '" + Sys.Helpers.String.SAP.NormalizeID(vendorNumber, 10) + "'";
                    var attributes_1 = [];
                    var sapField2TypeMapping = {};
                    for (var sapField in sapField2FFFieldMapping_1) {
                        if (Object.prototype.hasOwnProperty.call(sapField2FFFieldMapping_1, sapField)) {
                            attributes_1.push(sapField);
                            sapField2TypeMapping[sapField] = "string";
                        }
                    }
                    var options_1 = { recordBuilder: Sys.GenericAPI.BuildQueryResult, fieldToTypeMap: sapField2TypeMapping };
                    Sys.GenericAPI.SAPQuery(config_1, "ZESK_VENDORS", filter_1, attributes_1, function (result, error, bapiMgr) {
                        if (error || !result || result.length == 0) {
                            callback(null, error);
                        }
                        else {
                            var record_1 = result[0];
                            result = {};
                            Sys.Helpers.Array.ForEach(attributes_1, function (sapField) {
                                var ffField = sapField2FFFieldMapping_1[sapField];
                                if (ffField) {
                                    result[ffField] = record_1.GetValue(sapField);
                                }
                            });
                            result.Number__ = vendorNumber; // Query.SAPQuery returns the left 0 padded value. ex. 0000000300
                            if (result.PaymentTermCode__) {
                                // !! Description is language dependent
                                filter_1 = "ZTERM = '" + result.PaymentTermCode__ + "' AND SPRAS = '%SAPCONNECTIONLANGUAGE%'";
                                options_1.fieldToTypeMap = [{ TEXT1: "string" }];
                                Sys.GenericAPI.SAPQuery(bapiMgr || config_1, "T052U", filter_1, ["TEXT1"], function (result2, error2) {
                                    result["PaymentTermCode__.Description__"] = !error2 && result2 && result2.length > 0 ? result2[0].GetValue("TEXT1") : "";
                                    CompletVendorEmail(result);
                                }, 1, options_1);
                            }
                            else {
                                CompletVendorEmail(result);
                            }
                        }
                    }, 1, options_1);
                }
                else {
                    var filter = "&(" + "Number__=" + vendorNumber + ")(|(CompanyCode__=" + companyCode + ")(CompanyCode__=)(CompanyCode__!=*))";
                    var fields = ["CompanyCode__", "Number__", "Name__", "Sub__", "Street__", "PostOfficeBox__", "City__", "PostalCode__", "Region__", "Country__", "Email__", "VATNumber__", "FaxNumber__", "PaymentTermCode__", "PaymentTermCode__.Description__"];
                    Sys.GenericAPI.Query("AP - Vendors__", filter, fields, function (result, error) {
                        if (error || !result || result.length == 0) {
                            callback(null, error);
                        }
                        else {
                            CompletVendorEmail(result[0]);
                        }
                    }, null, 1, "EnableJoin=1");
                }
            }
            Vendor.QueryVendor = QueryVendor;
            // Loads vendor informations from VendorNumber__
            function Init() {
                Log.Time("Vendor.Init");
                return Sys.Helpers.Promise.Create(function (resolve /*, reject*/) {
                    if (!Sys.Helpers.IsEmpty(Data.GetValue("VendorNumber__")) && Sys.Helpers.IsEmpty(Data.GetValue("VendorAddress__"))) {
                        QueryVendor(Data.GetValue("CompanyCode__"), Data.GetValue("VendorNumber__"), function (result, error) {
                            if (error) {
                                Log.Error("QueryVendor error: " + error);
                                if (Sys.ScriptInfo.IsClient()) {
                                    Sys.Helpers.Globals.Popup.Alert(["_Error querying vendor message", error], false, null, "_Error querying vendor");
                                }
                                else {
                                    Lib.CommonDialog.NextAlert.Define("_Error querying vendor", "_Error querying vendor message", { isError: false }, error);
                                }
                            }
                            else if (result) {
                                Fill(function (fieldsName) {
                                    return result[fieldsName];
                                })
                                    .Then(function () {
                                    Log.TimeEnd("Vendor.Init");
                                    resolve();
                                });
                                return;
                            }
                            else {
                                Data.SetError("VendorName__", Language.Translate("Unknown vendor"));
                            }
                            Log.TimeEnd("Vendor.Init");
                            resolve();
                        });
                    }
                    else {
                        Log.TimeEnd("Vendor.Init");
                        resolve();
                    }
                });
            }
            Vendor.Init = Init;
            /**
             * CLIENT SIDE API
             */
            function OpenNewVendorRequestForm() {
                function fillNewVendorRequestDialog(dialog /*, tabId, event, control*/) {
                    var vendorNameCtrl = dialog.AddText("name", "_Vendor name", 230);
                    dialog.RequireControl(vendorNameCtrl);
                    var vendorAddressCtrl = dialog.AddMultilineText("address", "_Vendor address", 230);
                    dialog.RequireControl(vendorAddressCtrl);
                    vendorAddressCtrl.SetLineCount(4);
                    var vendorEmailCtrl = dialog.AddText("email", "_Vendor email", 230);
                    dialog.RequireControl(vendorEmailCtrl);
                    // fill with the values already set
                    if (Variable.GetValueAsString("NewVendor__") === "true") {
                        vendorNameCtrl.SetValue(Data.GetValue("VendorName__"));
                        vendorAddressCtrl.SetValue(Data.GetValue("VendorAddress__"));
                        vendorEmailCtrl.SetValue(Data.GetValue("VendorEmail__"));
                    }
                }
                function CommitNewVendorRequestDialog(dialog /*, tabId, event, control*/) {
                    Lib.Purchasing.Vendor.Reset(); // clear previous vendor info
                    Variable.SetValueAsString("NewVendor__", "true");
                    Data.SetValue("VendorName__", dialog.GetControl("name").GetValue());
                    Data.SetValue("VendorAddress__", dialog.GetControl("address").GetValue());
                    Data.SetValue("VendorEmail__", dialog.GetControl("email").GetValue());
                    Variable.SetValueAsString("ContactLogin", "");
                    Variable.SetValueAsString("VendorAddress", "");
                    Data.SetValue("VendorNumber__", "");
                    Data.SetError("VendorName__", "");
                    Log.Info("New vendor requested");
                }
                if (Sys.ScriptInfo.IsClient()) {
                    Sys.Helpers.Globals.Popup.Dialog("_New vendor request", null, fillNewVendorRequestDialog, CommitNewVendorRequestDialog);
                }
                else {
                    Log.Error("Not Implemented on server side");
                }
            }
            Vendor.OpenNewVendorRequestForm = OpenNewVendorRequestForm;
            /**
             * SERVER SIDE API
             */
            /**
            * This function build a login from candidateShortLogin, after removing all caracters != [a-z0-9_]
            * The accountId is not included
            * The login can only contain letters, numbers, dots (.), dashes (-), underscores (_) and arrobases (@).
            * Ex. BuildShortLogin(vendorName || vendorNumber)
            **/
            function BuildShortLogin(candidateShortLogin) {
                return candidateShortLogin;
            }
            function QueryVendorContact(email) {
                var login = g.Variable.GetValueAsString("ContactLogin");
                if (!login) {
                    // Search in ODUSER the vendor contact by email address
                    // We are in server script, so the query is synchronous.
                    var sAccountId = Lib.P2P.GetValidatorOrOwner().GetVars().GetValue_String("AccountId", 0);
                    var filter = "&(VENDOR=1)(EmailAddress=" + email + ")(Login=" + sAccountId + "$*)";
                    Log.Info("QueryVendorContact filter=" + filter);
                    Sys.GenericAPI.Query("ODUSER", filter, ["Login"], function (result, error) {
                        if (!error && result && result.length > 0) {
                            login = result[0].Login;
                            Log.Info("QueryVendorContact vendor found, login=" + login);
                        }
                    }, null, 1, { fastsearch: 1 });
                    if (!login) {
                        login = sAccountId + "$" + email.toLowerCase();
                    }
                }
                return g.Users.GetUserAsProcessAdmin(login);
            }
            Vendor.QueryVendorContact = QueryVendorContact;
            function GetVendorAttributes(companyCode, vendorNumber) {
                var attributes = {};
                var user = Lib.P2P.GetValidatorOrOwner();
                attributes.TimeZoneIdentifier = user.GetValue("TimeZoneIdentifier");
                attributes.TimeZoneIndex = user.GetValue("TimeZoneIndex");
                attributes.Language = user.GetValue("Language");
                attributes.Culture = user.GetValue("Culture");
                attributes.Company = g.Data.GetValue("VendorName__");
                attributes.EmailAddress = g.Data.GetValue("VendorEmail__");
                if (vendorNumber) {
                    attributes.Description = vendorNumber;
                    attributes.Company = g.Data.GetValue("VendorName__");
                    attributes.EmailAddress = g.Data.GetValue("VendorEmail__");
                    // Try to fill other attributes from "AP - Vendors" table
                    var query = Process.CreateQueryAsProcessAdmin();
                    query.SetSpecificTable("AP - Vendors__");
                    var queryFilter = "&(Number__=" + vendorNumber + ")(|(CompanyCode__=" + companyCode + ")(CompanyCode__=)(CompanyCode__!=*))";
                    query.SetFilter(queryFilter);
                    if (query.MoveFirst()) {
                        var record = query.MoveNextRecord();
                        if (record) {
                            var vars = record.GetVars();
                            attributes.Sub = vars.GetValue_String("Sub__", 0);
                            attributes.Street = vars.GetValue_String("Street__", 0);
                            attributes.POBox = vars.GetValue_String("PostOfficeBox__", 0);
                            attributes.City = vars.GetValue_String("City__", 0);
                            attributes.ZipCode = vars.GetValue_String("PostalCode__", 0);
                            attributes.MailState = vars.GetValue_String("Region__", 0);
                            attributes.FaxNumber = vars.GetValue_String("FaxNumber__", 0);
                            attributes.Country = vars.GetValue_String("Country__", 0);
                            attributes.PhoneNumber = vars.GetValue_String("PhoneNumber__", 0);
                            attributes.EmailAddress = attributes.EmailAddress || vars.GetValue_String("Email__", 0);
                        }
                    }
                    return attributes;
                }
                else if (g.Variable.GetValueAsString("NewVendor__") == "true") {
                    return attributes;
                }
                return {};
            }
            function QueryVendorLink(companyCode, vendorNumber) {
                var filter = "&(Number__=" + vendorNumber + ")(|(CompanyCode__=" + companyCode + ")(CompanyCode__=)(CompanyCode__!=*))";
                var query = Process.CreateQuery();
                query.SetSpecificTable("AP - Vendors links__");
                query.SetFilter(filter);
                query.MoveFirst();
                return query.MoveNextRecord();
            }
            function CreateVendorContact(companyCode, vendorNumber) {
                var vendorContact = null;
                var attributes = GetVendorAttributes(companyCode, vendorNumber);
                if (attributes.EmailAddress) // attributes.EmailAddress will be used as login
                 {
                    var accountId = Lib.P2P.GetValidatorOrOwner().GetValue("AccountId");
                    vendorContact = g.Users.GetUserAsProcessAdmin(accountId + "$_vendor_template_");
                    if (vendorContact) {
                        try {
                            var shortLogin = BuildShortLogin(attributes.EmailAddress);
                            vendorContact = vendorContact.Clone(shortLogin, attributes);
                        }
                        catch (ex) {
                            // when another process creating the same vendor by User.Clone at the same time,
                            // we may have constraint violation when creating it the second time.
                            // --> reset vendorContact = null to consider the vendor contact aleady exists
                            Log.Warn("Vendor contact clone failed: " + ex);
                            vendorContact = null;
                        }
                        if (!vendorContact) // the vendor contact aleady exists or could not be created
                         {
                            vendorContact = g.Users.GetUserAsProcessAdmin(accountId + "$" + attributes.EmailAddress);
                            // update vendor contact email address only when non email defined
                            if (vendorContact && !vendorContact.GetValue("EmailAddress")) {
                                vendorContact.SetValue("EmailAddress", attributes.EmailAddress);
                            }
                        }
                    }
                }
                return vendorContact;
            }
            function UpdateVendorContact(vendorContact, vendorNumber) {
                vendorContact.SetValue("Description", vendorNumber);
            }
            function CreateVendorLink(vendorContact, companyCode) {
                var shortLogin = vendorContact.GetValue("Login");
                if (shortLogin && shortLogin.indexOf("$") !== -1) {
                    shortLogin = shortLogin.substr(1 + shortLogin.indexOf("$"));
                }
                var vendorNumber = vendorContact.GetValue("Description");
                var record = QueryVendorLink(companyCode, vendorNumber);
                if (record) {
                    record.GetVars().AddValue_String("ShortLoginPAC__", shortLogin, true);
                    record.Commit();
                }
                else {
                    record = Process.CreateTableRecord("AP - Vendors links__");
                    if (record) {
                        var vars = record.GetVars();
                        vars.AddValue_String("Number__", vendorNumber, true);
                        vars.AddValue_String("CompanyCode__", companyCode, true);
                        vars.AddValue_String("ShortLoginPAC__", shortLogin, true);
                        record.Commit();
                    }
                }
                return record;
            }
            /**
             * ServerSide Only
             */
            function GetVendorContact() {
                if (Sys.ScriptInfo.IsClient()) {
                    Log.Error("Not Implemented on client side");
                    return null;
                }
                var autoCreateVendor = Sys.Parameters.GetInstance("PAC").GetParameter("AlwaysCreateVendor") === "1";
                var vendorContact = null;
                var vendorNumber = g.Data.GetValue("VendorNumber__") || g.Variable.GetValueAsString("VendorNumber__");
                var vendorEmail = g.Data.GetValue("VendorEmail__");
                var companyCode = g.Data.GetValue("CompanyCode__");
                if (vendorEmail) {
                    vendorContact = QueryVendorContact(vendorEmail);
                    if (!vendorContact && autoCreateVendor) {
                        vendorContact = CreateVendorContact(companyCode, vendorNumber);
                    }
                    else if (vendorContact && Sys.Helpers.IsEmpty(vendorContact.GetValue("Description"))) {
                        UpdateVendorContact(vendorContact, vendorNumber);
                    }
                    if (vendorContact && vendorNumber) {
                        var record = QueryVendorLink(companyCode, vendorNumber);
                        if (!record || Sys.Helpers.IsEmpty(record.GetVars().GetValue_String("ShortLoginPAC__", 0))) {
                            CreateVendorLink(vendorContact, companyCode);
                        }
                    }
                }
                return vendorContact;
            }
            Vendor.GetVendorContact = GetVendorContact;
            function QueryPOTransmissionPreferences(companyCode, vendorNumber) {
                var options = {
                    table: "Vendor_company_extended_properties__",
                    filter: "(&(VendorNumber__=" + vendorNumber + ")(|(CompanyCode__=" + companyCode + ")(CompanyCode__=)(CompanyCode__!=*)))",
                    attributes: ["*"],
                    maxRecords: 1,
                    callback: function (res) { return res.length ? res[0] : null; }
                };
                return Sys.GenericAPI.PromisedQuery(options);
            }
            Vendor.QueryPOTransmissionPreferences = QueryPOTransmissionPreferences;
        })(Vendor = Purchasing.Vendor || (Purchasing.Vendor = {}));
    })(Purchasing = Lib.Purchasing || (Lib.Purchasing = {}));
})(Lib || (Lib = {}));
