///#GLOBALS Lib Sys
/* LIB_DEFINITION{
  "name": "Lib_Purchasing_GRValidation",
  "libraryType": "LIB",
  "scriptType": "SERVER",
  "comment": "Purchasing library managing GR items",
  "require": [
    "Sys/Sys_Helpers_Array",
    "Sys/Sys_Decimal",
    "Lib_Purchasing_Items_V12.0.425.0",
    "Lib_CommonDialog_V12.0.425.0",
    "Sys/Sys_Helpers_Data",
    "[Lib_GR_Customization_Server]"
  ]
}*/
var Lib;
(function (Lib) {
    var Purchasing;
    (function (Purchasing) {
        var GRValidation;
        (function (GRValidation) {
            /**
             * Returns all items in PO form by PR number.
             * @returns {object} items map by PR number
             */
            function GetPOItemsInForm() {
                var allPOItems = {};
                Sys.Helpers.Data.ForEachTableItem("LineItems__", function (line) {
                    var poNumber = line.GetValue("OrderNumber__");
                    if (!(poNumber in allPOItems)) {
                        allPOItems[poNumber] = [];
                    }
                    allPOItems[poNumber].push(line);
                });
                return allPOItems;
            }
            /**
             * Call to check if a line in LineItems__ table as been over received.
             * @returns {boolean} true if succeeds, false if over received
             */
            function CheckOverReceivedItems() {
                try {
                    //Check if no item is over ordered
                    var allPOItems = GetPOItemsInForm();
                    //Get the GR Items already created for each PO Items present in this GR to retrieve alreadyReceivedQuantity quantity
                    var grItemsFilter_1 = "(&(!(Status__=Canceled))(|";
                    Sys.Helpers.Object.ForEach(allPOItems, function (poItems, poNumber) {
                        poItems.forEach(function (poItem) {
                            grItemsFilter_1 += "(&(OrderNumber__=" + poNumber + ")(LineNumber__=" + poItem.GetValue("LineNumber__") + "))";
                        });
                    });
                    grItemsFilter_1 += "))";
                    var grItems_1 = Lib.Purchasing.Items.GetItemsForDocumentSync(Lib.Purchasing.Items.GRItemsDBInfo, grItemsFilter_1, "OrderNumber__");
                    var grNumber_1 = Data.GetValue("GRNumber__");
                    var overReceived_1 = false;
                    Sys.Helpers.Data.ForEachTableItem("LineItems__", function (line) {
                        var isAmountBased = Lib.Purchasing.Items.IsAmountBasedItem(line);
                        var poNumber = line.GetValue("OrderNumber__");
                        var poLineNumber = line.GetValue("LineNumber__");
                        var fieldOrdered = isAmountBased ? "ItemOrderedAmount__" : "OrderedQuantity__";
                        var fieldReceived = isAmountBased ? "NetAmount__" : "ReceivedQuantity__";
                        var fieldReceivedInGRItems = isAmountBased ? "Amount__" : "Quantity__";
                        var valueOrdered = line.GetValue(fieldOrdered);
                        var valueReceived = line.GetValue(fieldReceived);
                        line.SetWarning(fieldReceived, "");
                        var valueAlreadyReceived = new Sys.Decimal(0);
                        if (grItems_1 && grItems_1[poNumber] && grItems_1[poNumber].length > 0) {
                            grItems_1[poNumber].forEach(function (grLine) {
                                // Ignore items from this GR
                                if (grLine.GetValue("LineNumber__") == poLineNumber && grLine.GetValue("GRNumber__") !== grNumber_1) {
                                    valueAlreadyReceived = valueAlreadyReceived.add(grLine.GetValue(fieldReceivedInGRItems));
                                }
                            });
                        }
                        var valueOverReceived = valueAlreadyReceived.add(valueReceived).minus(valueOrdered);
                        if (valueOverReceived.greaterThan(0) && !Sys.Helpers.TryCallFunction("Lib.GR.Customization.Common.AllowOverdelivery", line)) {
                            line.SetError(fieldReceived, "_Received qty outmatch ordered qty");
                            overReceived_1 = true;
                        }
                    });
                    return !overReceived_1;
                }
                catch (e) {
                    Lib.Purchasing.OnUnexpectedError(e.toString());
                    return false;
                }
            }
            GRValidation.CheckOverReceivedItems = CheckOverReceivedItems;
            /**
             * Update (or create) document items when GR is Created.
             */
            GRValidation.SynchronizeItems = (function () {
                function InitComputedDataForGRItemsSynchronizeConfig(options) {
                    var status = Data.GetValue("GRStatus__");
                    var computedData = {
                        common: {},
                        byLine: {}
                    };
                    // Retrieves the table.
                    var myTable = Data.GetTable("LineItems__");
                    var itemCount = myTable.GetItemCount();
                    var i = 0;
                    // Parses all lines.
                    while (i < itemCount) {
                        // Gets the current table line.
                        var currentItem = myTable.GetItem(i);
                        var lineNumber = currentItem.GetValue("LineNumber__");
                        var receivedQuantity = currentItem.GetValue("ReceivedQuantity__");
                        var receivedAmount = currentItem.GetValue("NetAmount__");
                        var completed = currentItem.GetValue("DeliveryCompleted__");
                        if (receivedQuantity !== 0 || receivedAmount !== 0 || completed) {
                            i++;
                            var initialValues = {
                                "Status": status
                            };
                            if (!options.justUpdateItems) {
                                initialValues.InvoicedAmount = 0;
                                initialValues.InvoicedQuantity = 0;
                            }
                            computedData.byLine[lineNumber] = initialValues;
                        }
                        else {
                            // Deletes the line and updates the total count.
                            itemCount = currentItem.RemoveItem();
                        }
                    }
                    return Lib.Purchasing.Items.GRItemsSynchronizeConfig.computedData = computedData;
                }
                function Synchronize(options) {
                    options = options || {};
                    try {
                        InitComputedDataForGRItemsSynchronizeConfig(options);
                        Lib.Purchasing.Items.Synchronize(Lib.Purchasing.Items.GRItemsSynchronizeConfig);
                        if (!options.justUpdateItems) {
                            var poRUIDEx = Data.GetValue("SourceRUID");
                            Lib.Purchasing.Items.ResumeDocumentToSynchronizeItems("Purchase order", poRUIDEx, "SynchronizeItems", JSON.stringify({ "actualRecipientDN": Data.GetValue("LastSavedOwnerId") || Data.GetValue("OwnerId") }));
                        }
                        else {
                            Log.Info("Don't resume PO to synchronize items (justUpdateItems option enabled).");
                        }
                        return true;
                    }
                    catch (e) {
                        Log.Error(e.toString());
                        Lib.CommonDialog.NextAlert.Define("_Items synchronization error", "_Items synchronization error message");
                        return false;
                    }
                }
                return function (options) {
                    return Sys.Helpers.Data.RollbackableSection(function (rollbackFn) {
                        return Synchronize(options) || rollbackFn();
                    });
                };
            })();
            GRValidation.OnCreateGRInERP = (function () {
                function CheckDeliveryCompleted(docData) {
                    var nCompleted = 0;
                    var table = Data.GetTable("LineItems__");
                    var nItems = table.GetItemCount();
                    for (var i = 0; i < nItems; i++) {
                        var lineItem = table.GetItem(i);
                        var receivedQuantity = lineItem.GetValue("ReceivedQuantity__");
                        var completed = lineItem.GetValue("DeliveryCompleted__");
                        Log.Info("completed=" + completed);
                        if (receivedQuantity !== 0 || completed) {
                            if (completed || receivedQuantity >= lineItem.GetValue("OpenQuantity__")) {
                                nCompleted++;
                            }
                        }
                    }
                    docData.localDoc = docData.localDoc || {};
                    docData.localDoc.deliveryCompleted = nCompleted === nItems;
                }
                return function (createDocInERP, docData) {
                    var ok = true;
                    // Here we have to get a number.
                    if (!Sys.Helpers.IsEmpty(docData.number)) {
                        CheckDeliveryCompleted(docData);
                    }
                    if (!ok) {
                        delete docData.number;
                        Log.Error("Could not create the next process, no document number");
                    }
                    return ok;
                };
            })();
            GRValidation.OnEditOrder = (function () {
                function FillFormWithLastPOUpdates() {
                    Log.Info("Fill form with last PO updates");
                    return Lib.Purchasing.GRItems.FillForm({
                        updateItems: true
                    });
                }
                function UpdateBudget() {
                    return Sys.Helpers.Promise.Create(function (resolve /*, reject: Function*/) {
                        Log.Info("Updating budget...");
                        if (!Lib.Purchasing.GRBudget.AsReceivedAfterPOEditing()) {
                            throw new Error("Error during updating budget");
                        }
                        resolve();
                    });
                }
                function SynchronizeItemsAfterUpdate() {
                    return Sys.Helpers.Promise.Create(function (resolve /*, reject: Function*/) {
                        Log.Info("Synchronizing items...");
                        if (!Lib.Purchasing.GRValidation.SynchronizeItems({ justUpdateItems: true })) {
                            throw new Error("Error during synchronizing budget");
                        }
                        resolve();
                    });
                }
                return function () {
                    Log.Info("OnEditOrder starting...");
                    var ret = true;
                    FillFormWithLastPOUpdates()
                        .Then(UpdateBudget)
                        .Then(SynchronizeItemsAfterUpdate)
                        .Catch(function (reason) {
                        Log.Error("OnEditOrder in error. Details: " + reason);
                        Lib.CommonDialog.NextAlert.Define("_GR edition error", "_GR edition error message", { isError: true, behaviorName: "GREditionError" });
                        Process.PreventApproval();
                        ret = false;
                    });
                    return ret;
                };
            })();
        })(GRValidation = Purchasing.GRValidation || (Purchasing.GRValidation = {}));
    })(Purchasing = Lib.Purchasing || (Lib.Purchasing = {}));
})(Lib || (Lib = {}));
