/* eslint-disable dot-notation */
///#GLOBALS Lib Sys
/*global localStorage*/
/// <reference path="../../PAC/Purchasing V2/Purchase Requisition process V2/typings_withDeleted/Controls_Purchase_Requisition_process_V2/index.d.ts"/>
/* LIB_DEFINITION{
  "name": "Lib_Purchasing_PRLineItems",
  "libraryType": "LIB",
  "scriptType": "CLIENT",
  "comment": "Purchasing library",
  "require": [
    "Lib_Purchasing_V12.0.425.0",
    "Lib_Purchasing_Items_V12.0.425.0",
    "Lib_Purchasing_ShipTo_Client_V12.0.425.0",
    "Sys/Sys_Decimal",
    "Sys/Sys_Helpers",
    "Sys/Sys_Helpers_Data",
    "Sys/Sys_Helpers_Controls",
    "Sys/Sys_OnDemand_Users",
    "Sys/Sys_GenericAPI_Client"
  ]
}*/
var Lib;
(function (Lib) {
    var Purchasing;
    (function (Purchasing) {
        function AutoFillLineItemsWithEmptyValue(fieldName, value, options) {
            Sys.Helpers.Data.ForEachTableItem("LineItems__", function (item, i) {
                var _a;
                if (!Lib.Purchasing.IsLineItemEmpty(item) && !item.GetValue(fieldName)) {
                    var extraCondition = options && options.extraCondition ? options.extraCondition(item) : true;
                    if (extraCondition) {
                        item.SetValue(fieldName, value);
                        (_a = options === null || options === void 0 ? void 0 : options.extraSetter) === null || _a === void 0 ? void 0 : _a.call(options, item);
                        Log.Info("Auto filled " + fieldName + " of line item with index '" + i + "'");
                    }
                }
            });
        }
        function RedirectToPunchout(items) {
            if (items.length == 1) {
                var item = items[0];
                if (item.punchoutConfig) {
                    Lib.Purchasing.Punchout.PR.OpenPunchoutSite(item.punchoutConfig, item.selectedItem);
                    return true;
                }
            }
            return false;
        }
        function CatalogCallback(storageValue) {
            Log.Info("Storage changed: " + JSON.stringify({ "type": storageValue.type, "key": storageValue.key, "oldValue": storageValue.oldValue, "newValue": storageValue.newValue }));
            if (storageValue.newValue) {
                try {
                    var items = JSON.parse(storageValue.newValue);
                    if (!RedirectToPunchout(items)) {
                        Purchasing.PRLineItems.NumberOrDescription.OnSelectItems(items);
                    }
                    localStorage.removeItem(storageValue.key);
                }
                catch (e) {
                    Log.Error("INVALID NewValue : " + e.message);
                }
            }
        }
        Purchasing.CatalogCallback = CatalogCallback;
        Purchasing.PRLineItems = {
            displayNoMoreLinesWarning: true,
            fromUserChange: false,
            leaveEmptyLine: true,
            LOCKED_FIELDS: ["ITEMUNITPRICE__", "ITEMNUMBER__", "ITEMDESCRIPTION__", "VENDORNAME__", "VENDORNUMBER__", "ITEMCURRENCY__", "ITEMUNIT__", "ITEMUNITDESCRIPTION__", "SUPPLYTYPENAME__", "SUPPLYTYPEID__", "LOCKED__"],
            // Used in LineItemsMissingBudgetKey to check that an item has a missing budget key
            REQUIRED_BUDGET_KEY_FIELDS: ["ItemCostCenterId__", "ItemGroup__", "CostType__"],
            ItemTypeDependenciesVisibilities: {
                "ItemStartDate__": {
                    "AmountBased": false, "QuantityBased": false, "ServiceBased": true
                },
                "ItemEndDate__": {
                    "AmountBased": false, "QuantityBased": false, "ServiceBased": true
                },
                "ItemRequestedDeliveryDate__": {
                    "AmountBased": true, "QuantityBased": true, "ServiceBased": false
                },
                "ItemQuantity__": {
                    "AmountBased": false, "QuantityBased": true, "ServiceBased": true
                },
                "ItemUnitPrice__": {
                    "AmountBased": false, "QuantityBased": true, "ServiceBased": true
                },
                "ItemUnit__": {
                    VisibleCondition: function () {
                        return Sys.Parameters.GetInstance("PAC").GetParameter("DisplayUnitOfMeasure");
                    },
                    "AmountBased": false, "QuantityBased": true, "ServiceBased": true
                }
            },
            OnAddItem: function (item, index) {
                var previousItem = Sys.Helpers.IsNumeric(index) && index > 0 ? Data.GetTable("LineItems__").GetItem(index - 1) : null;
                var quantityBased = Lib.Purchasing.Items.IsQuantityBasedItem(item);
                //default Value
                Lib.Purchasing.ShipTo.OnAddItem(item, previousItem);
                if (Sys.Helpers.IsEmpty(item.GetValue("WarehouseID__")) && Sys.Helpers.IsEmpty(item.GetValue("WarehouseName__"))) {
                    item.SetValue("WarehouseID__", Lib.P2P.UserProperties.GetValues(User.loginId).DefaultWarehouse__);
                    item.SetValue("WarehouseName__", Lib.P2P.UserProperties.GetValues(User.loginId).DefaultWarehouseName__);
                }
                if (Sys.Helpers.IsEmpty(item.GetValue("ItemCostCenterId__")) && Sys.Helpers.IsEmpty(item.GetValue("ItemCostCenterName__"))) {
                    if (previousItem && previousItem.GetValue("ItemCostCenterId__") && previousItem.GetValue("ItemCostCenterName__")) {
                        item.SetValue("ItemCostCenterId__", previousItem.GetValue("ItemCostCenterId__"));
                        item.SetValue("ItemCostCenterName__", previousItem.GetValue("ItemCostCenterName__"));
                    }
                    else {
                        item.SetValue("ItemCostCenterId__", Lib.P2P.UserProperties.GetValues(User.loginId).CostCenter__);
                        item.SetValue("ItemCostCenterName__", Lib.P2P.UserProperties.GetValues(User.loginId).CostCenter__Description__);
                    }
                }
                if (Sys.Helpers.IsEmpty(item.GetValue("VendorNumber__")) && Sys.Helpers.IsEmpty(item.GetValue("VendorName__"))) {
                    if (Variable.GetValueAsString("lastExtractedVendorNumber") && Variable.GetValueAsString("lastExtractedVendorName")) {
                        item.SetValue("VendorNumber__", Variable.GetValueAsString("lastExtractedVendorNumber"));
                        item.SetValue("VendorName__", Variable.GetValueAsString("lastExtractedVendorName"));
                    }
                    else if (previousItem && previousItem.GetValue("VendorNumber__") && previousItem.GetValue("VendorName__")) {
                        item.SetValue("VendorNumber__", previousItem.GetValue("VendorNumber__"));
                        item.SetValue("VendorName__", previousItem.GetValue("VendorName__"));
                    }
                }
                if (Sys.Helpers.IsEmpty(item.GetValue("FreeDimension1__")) && Sys.Helpers.IsEmpty(item.GetValue("FreeDimension1ID__"))) {
                    if (previousItem && previousItem.GetValue("FreeDimension1__") && previousItem.GetValue("FreeDimension1ID__")) {
                        item.SetValue("FreeDimension1__", previousItem.GetValue("FreeDimension1__"));
                        item.SetValue("FreeDimension1ID__", previousItem.GetValue("FreeDimension1ID__"));
                    }
                }
                if (Sys.Helpers.IsEmpty(item.GetValue("ProjectCodeDescription__")) && Sys.Helpers.IsEmpty(item.GetValue("ProjectCode__"))) {
                    if (previousItem && previousItem.GetValue("ProjectCodeDescription__") && previousItem.GetValue("ProjectCode__")) {
                        item.SetValue("ProjectCodeDescription__", previousItem.GetValue("ProjectCodeDescription__"));
                        item.SetValue("ProjectCode__", previousItem.GetValue("ProjectCode__"));
                    }
                }
                if (Sys.Helpers.IsEmpty(item.GetValue("ItemCurrency__")) && Sys.Helpers.IsEmpty(item.GetError("ItemCurrency__")) && item.GetError("ItemCurrency__") !== "The value could not be resolve") {
                    item.SetValue("ItemCurrency__", (previousItem && previousItem.GetValue("ItemCurrency__")) || Variable.GetValueAsString("companyCurrency"));
                    Purchasing.PRLineItems.ExchangeRate.Set(item);
                }
                if (Sys.Parameters.GetInstance("PAC").GetParameter("DisplayUnitOfMeasure") && quantityBased && Sys.Helpers.IsEmpty(item.GetValue("ItemUnit__"))) {
                    item.SetValue("ItemUnit__", (previousItem && previousItem.GetValue("ItemUnit__")) || Sys.Parameters.GetInstance("PAC").GetParameter("DefaultUnitOfMeasure"));
                }
                //if PRLineItems.leaveEmptyLine is here because when comming from clipboard action we don't want to set a default SupplyType when the user explicitly put nothing
                if (Purchasing.PRLineItems.leaveEmptyLine && previousItem && Sys.Helpers.IsEmpty(item.GetValue("SupplyTypeID__")) && Sys.Helpers.IsEmpty(item.GetError("SupplyTypeName__"))) {
                    item.SetValue("SupplyTypeID__", previousItem.GetValue("SupplyTypeID__"));
                    Purchasing.PRLineItems.SupplyType.FillInducedFields(item);
                    Purchasing.PRLineItems.SupplyType.FillBuyerAndRecipient(item, index);
                }
                else if (Sys.Helpers.IsEmpty(item.GetValue("CostType__")) && !Sys.Helpers.IsEmpty(item.GetValue("ItemGLAccount__"))) {
                    Lib.P2P.fillCostTypeFromGLAccount(item, "ItemGLAccount__")
                        .Then(function (resultItem) {
                        Lib.Purchasing.LayoutPR.DelayRebuildWorkflow();
                        Purchasing.PRLineItems.Budget.Fill({ resetBudgetID: true }, resultItem);
                    });
                }
                Lib.Purchasing.CheckPR.requiredFields.CheckItemsDeliveryDates(Lib.Purchasing.LayoutPR.GetCurrentLayout(), {
                    specificItemIndex: index,
                    ignoreRequiredCheck: true
                });
                Purchasing.PRLineItems.UpdateHasCapex();
                Purchasing.PRLineItems.Buyer.DelayCheckBuyers();
                Purchasing.PRLineItems.ComputeItemAmount(index);
                Purchasing.PRLineItems.SetLastLineItemType(item, index);
            },
            SetLastLineItemType: function (item, index) {
                var lastIndex = Controls.LineItems__.GetItemCount() - 1;
                if (index < lastIndex) {
                    var lastItem = Controls.LineItems__.GetItem(lastIndex);
                    if (Lib.Purchasing.IsLineItemEmpty(lastItem)) {
                        lastItem.SetValue("ItemType__", item.GetValue("ItemType__"));
                    }
                }
                Lib.Purchasing.Items.UpdateItemTypeDependencies(Purchasing.PRLineItems.ItemTypeDependenciesVisibilities);
            },
            SetRowButtonsVisibility: function (rowIndex) {
                var row = Controls.LineItems__.GetRow(rowIndex);
                var item = row.GetItem();
                if (item && Lib.Purchasing.IsLineItemEmpty(item)) {
                    Controls.LineItems__.HideTableRowDeleteForItem(rowIndex, true);
                    Controls.LineItems__.HideTableRowButtonDuplicateForItem(rowIndex, true);
                }
                else if (Lib.Purchasing.Punchout.PR.IsPunchoutLineRow(row)) { // we shall not duplicate a punchout line
                    Controls.LineItems__.HideTableRowDeleteForItem(rowIndex, false);
                    Controls.LineItems__.HideTableRowButtonDuplicateForItem(rowIndex, true);
                }
                else {
                    Controls.LineItems__.HideTableRowDeleteForItem(rowIndex, false);
                    Controls.LineItems__.HideTableRowButtonDuplicateForItem(rowIndex, false);
                }
            },
            OnRefreshRow: function (index) {
                var row = Controls.LineItems__.GetRow(index);
                var isReadOnly = Lib.Purchasing.LayoutPR.IsReadOnly();
                var isRequesterStep = Lib.Purchasing.LayoutPR.IsRequesterStep();
                var isBuyer = Lib.Purchasing.LayoutPR.IsBuyer();
                var isVendorReadonly = !isRequesterStep && !isBuyer;
                var isPunchoutLineRow = Lib.Purchasing.Punchout.PR.IsPunchoutLineRow(row);
                var isLocked = row.Locked__.IsChecked();
                var quantityBased = Lib.Purchasing.Items.IsQuantityBasedItem(row.GetItem());
                row.ItemType__.SetReadOnly(!isRequesterStep || isPunchoutLineRow || isLocked);
                row.ItemNumber__.SetReadOnly(!isRequesterStep || isPunchoutLineRow || isLocked);
                row.ItemDescription__.SetReadOnly(!isRequesterStep || isPunchoutLineRow || isLocked);
                row.VendorName__.SetReadOnly(isReadOnly || isVendorReadonly || isPunchoutLineRow || isLocked);
                row.VendorNumber__.SetReadOnly(isReadOnly || isVendorReadonly || isPunchoutLineRow || isLocked);
                row.ItemCurrency__.SetReadOnly(!isRequesterStep || isPunchoutLineRow || isLocked);
                row.ItemNetAmount__.SetReadOnly(!isRequesterStep || isPunchoutLineRow || isLocked || quantityBased);
                row.ItemNetAmount__.SetRequired(!quantityBased);
                row.ItemQuantity__.Hide(!quantityBased);
                row.ItemQuantity__.SetReadOnly(!isRequesterStep || isPunchoutLineRow || !quantityBased);
                row.ItemQuantity__.SetRequired(quantityBased);
                row.ItemUnitPrice__.Hide(!quantityBased);
                row.ItemUnitPrice__.SetReadOnly(!isRequesterStep || isPunchoutLineRow || isLocked || !quantityBased);
                row.ItemUnitPrice__.SetRequired(quantityBased);
                row.ItemOrderedAmount__.Hide(quantityBased);
                row.ItemOrderedQuantity__.Hide(!quantityBased);
                row.CanceledAmount__.Hide(quantityBased);
                row.CanceledQuantity__.Hide(!quantityBased);
                row.ItemUnit__.Hide(!quantityBased);
                row.ItemUnit__.SetReadOnly(!isRequesterStep || isPunchoutLineRow || isLocked);
                row.ItemUnitDescription__.SetReadOnly(!isRequesterStep || isPunchoutLineRow || isLocked);
                row.SupplyTypeID__.SetReadOnly(!isRequesterStep || isLocked);
                row.SupplyTypeName__.SetReadOnly(!isRequesterStep || isLocked);
                var serviceBased = Lib.Purchasing.Items.IsServiceBasedItem(row.GetItem());
                row.ItemStartDate__.Hide(!serviceBased);
                row.ItemStartDate__.SetReadOnly(!isRequesterStep || !serviceBased);
                row.ItemStartDate__.SetRequired(serviceBased);
                row.ItemEndDate__.Hide(!serviceBased);
                row.ItemEndDate__.SetReadOnly(!isRequesterStep || !serviceBased);
                row.ItemEndDate__.SetRequired(serviceBased);
                row.ItemRequestedDeliveryDate__.Hide(serviceBased);
                Purchasing.PRLineItems.SetRowButtonsVisibility(index);
                Purchasing.PRLineItems.Budget.Refresh(index);
                Purchasing.PRLineItems.CheckItemBudgetVisibility(index);
                Purchasing.PRLineItems.Recipient.OnRefreshRow(index);
                Purchasing.PRLineItems.Buyer.OnRefreshRow(index);
                Purchasing.PRLineItems.PONumber.SetLink(row);
                Lib.Purchasing.ShipTo.OnRefreshRow(index);
            },
            OnDeleteItem: function (item, index) {
                Lib.Purchasing.Items.UpdateItemTypeDependencies(Purchasing.PRLineItems.ItemTypeDependenciesVisibilities);
                Purchasing.PRLineItems.ComputeAmounts();
                Purchasing.PRLineItems.Buyer.DelayCheckBuyers();
                if (index == 0) {
                    Log.Info("Rebuilding workflow as first line has been deleted...");
                    // We rebuild the workflow as the buyer of the first line is used and this line has been removed
                    Lib.Purchasing.LayoutPR.DelayRebuildWorkflow();
                }
                Purchasing.PRLineItems.UpdateHasCapex();
                Purchasing.PRLineItems.LeaveEmptyLine();
            },
            UpdateHasCapex: function () {
                var hasCapex = false;
                Sys.Helpers.Data.ForEachTableItem("LineItems__", function (item /*, i: number*/) {
                    if (item.GetValue("CostType__") == "CapEx") {
                        hasCapex = true;
                        return;
                    }
                    Purchasing.PRLineItems.Budget.Fill({ resetBudgetID: true }, item);
                });
                Controls.HasCapex__.SetValue(hasCapex);
                Lib.Purchasing.LayoutPR.DelayRebuildWorkflow();
            },
            OnDuplicateItem: function (item, index, sourceItem, sourceIndex) {
                Lib.Purchasing.RemoveEmptyLineItem(Data.GetTable("LineItems__"));
                Purchasing.PRLineItems.ComputeItemAmount(index);
                Purchasing.PRLineItems.Buyer.DelayCheckBuyers();
                if (sourceIndex == 0) {
                    Log.Info("Rebuilding workflow as first line might have been change...");
                    // We rebuild the workflow as the buyer of the first line is used and this line has been removed
                    Lib.Purchasing.LayoutPR.DelayRebuildWorkflow();
                }
                Purchasing.PRLineItems.LeaveEmptyLine();
                Purchasing.PRLineItems.SetLastLineItemType(item, index);
            },
            // disable glaccount for which the cost center is not accessible / disable non browsable recipient
            UpdateLayout: function () {
                Lib.Purchasing.Items.UpdateItemTypeDependencies(Purchasing.PRLineItems.ItemTypeDependenciesVisibilities);
                for (var i = Controls.LineItems__.GetLineCount(true) - 1; i >= 0; i--) {
                    Purchasing.PRLineItems.OnRefreshRow(i);
                }
                Purchasing.PRLineItems.Buyer.DelayCheckBuyers();
            },
            Count: function () {
                var table = Data.GetTable("LineItems__");
                var count = 0, i, row;
                for (i = table.GetItemCount() - 1; i >= 0; i--) {
                    row = table.GetItem(i);
                    if (!Lib.Purchasing.IsLineItemEmpty(row)) {
                        count++;
                    }
                    else if (Purchasing.PRLineItems.leaveEmptyLine) {
                        if (i !== table.GetItemCount() - 1) { // Removes empty row unless it is the last in the table
                            if (row.Remove()) {
                                Log.Info("remove Line " + i + " ok");
                            }
                            else {
                                Log.Info("remove Line " + i + " fail");
                            }
                        }
                    }
                }
                return count;
            },
            LeaveEmptyLine: function () {
                var maxNbreLines = Controls.LineItems__.GetLineCount();
                var table = Data.GetTable("LineItems__");
                var itemCount = Purchasing.PRLineItems.Count();
                var lineCount = table.GetItemCount();
                if (itemCount <= maxNbreLines) {
                    if (Lib.Purchasing.LayoutPR.IsRequesterStep() && !Lib.Purchasing.LayoutPR.IsReadOnly()) {
                        if (itemCount === lineCount) {
                            if (itemCount === maxNbreLines) {
                                if (this.displayNoMoreLinesWarning) {
                                    this.displayNoMoreLinesWarning = false;
                                    Controls.LineItems__.HideTableRowButtonDuplicate(true);
                                    var Options_1 = {
                                        message: Language.Translate("You can not add more than {0} items", false, maxNbreLines),
                                        status: "warning"
                                    };
                                    var displayWarningInterval_1 = setInterval(function () {
                                        if (!Lib.Purchasing.LayoutPR.GlobalLayout.waitScreenDisplayed) {
                                            Popup.Snackbar(Options_1);
                                            clearInterval(displayWarningInterval_1);
                                        }
                                    }, 1000);
                                }
                            }
                            else {
                                Controls.LineItems__.HideTableRowButtonDuplicate(false);
                                table.AddItem();
                                this.displayNoMoreLinesWarning = true;
                            }
                            if (Purchasing.PRLineItems.fromUserChange && itemCount > 0) {
                                Purchasing.PRLineItems.fromUserChange = false;
                                itemCount--;
                                Purchasing.PRLineItems.OnAddItem(table.GetItem(itemCount), itemCount);
                            }
                        }
                    }
                }
                Purchasing.PRLineItems.fromUserChange = false;
            },
            Reset: function () {
                var table = Data.GetTable("LineItems__");
                var nblines = table.GetItemCount(), i, row;
                for (i = 0; i < nblines; i++) {
                    row = table.GetItem(0);
                    if (row) {
                        row.Remove();
                    }
                }
                Variable.SetValueAsString("lastExtractedVendorName", null);
                Variable.SetValueAsString("lastExtractedVendorNumber", null);
                Purchasing.PRLineItems.ComputeAmounts();
                Purchasing.PRLineItems.LeaveEmptyLine();
            },
            RequiredFields: {
                OnBlur: function () {
                    if (this.GetRow().GetLineNumber() === Data.GetTable("LineItems__").GetItemCount()) {
                        this.SetError("");
                    }
                }
            },
            CheckItemBudgetVisibility: function (index) {
                var row = Controls.LineItems__.GetRow(index);
                var isReviewer = Lib.Purchasing.LayoutPR.IsReviewer();
                var hide = Lib.Purchasing.LayoutPR.IsReadOnly() || (!isReviewer && !Purchasing.PRLineItems.Budget.HasVisibilityOnCurrentBudget(row.GetItem()));
                row.ItemGLAccount__.SetReadOnly(hide);
                row.ItemGLAccount__.Hide(!Sys.Parameters.GetInstance("PAC").GetParameter("DisplayGlAccount") || hide);
                // FT-012944: The Expense category field should be visible and writable for the budget owner (not mandatory)
                row.ItemGroupDescription__.SetReadOnly(hide);
                row.ItemGroupDescription__.Hide(!Sys.Parameters.GetInstance("PAC").GetParameter("DisplayExpenseCategory") || hide);
                row.ItemBudgetCurrency__.Hide(hide);
                row.ItemBudgetInitial__.Hide(hide);
                row.BudgetID__.Hide(hide);
                row.ItemBudgetRemaining__.Hide(hide);
                row.ItemPeriodCode__.Hide(hide);
            },
            UpdateLineItem: function (item, index) {
                // Recomputes line item properties from catalog information
                // After Item is filled with catalog information
                // Unit Price has been set from catalog, need to recompute the net Amount
                Purchasing.PRLineItems.ExchangeRate.Set(item);
                Purchasing.PRLineItems.ComputeItemAmount(index);
                // Fill supply type name
                Purchasing.PRLineItems.SupplyType.FillInducedFields(item, false, false);
                // Update ItemGroup then the Expense Category
                Purchasing.PRLineItems.GLAccount.UpdateGroup(item);
                // Fill buyer and recipient
                Purchasing.PRLineItems.SupplyType.FillBuyerAndRecipient(item, index);
                // Update Tax Rate
                Purchasing.PRLineItems.TaxCode.UpdateTaxRate(item);
                // Update vendor name
                Purchasing.PRLineItems.Vendor.UpdateVendorName(item);
            },
            UpdateLineItemCopyFromClipboard: function (item, index, rowData) {
                var promises = [];
                promises.push(Purchasing.PRLineItems.SupplyType.UpdateSupplyTypeID(item));
                promises.push(Purchasing.PRLineItems.Vendor.UpdateVendorNumber(item));
                promises.push(Purchasing.PRLineItems.CostCenterName.UpdateCostCenterID(item));
                return Sys.Helpers.Synchronizer.All(promises)
                    .Then(function () {
                    return Purchasing.PRLineItems.NumberOrDescription.UpdateNumberOrDescription(item)
                        .Then(function () {
                        if (Lib.Purchasing.Items.IsAmountBasedItem(item) && !Sys.Helpers.IsEmpty(rowData.ItemNetAmount__)) {
                            item.SetValue("ItemNetAmount__", rowData.ItemNetAmount__);
                        }
                        // if glAcccount is set by User, Update ItemGroup then the Expense Category
                        if (!Sys.Helpers.IsEmpty(item.GetValue("ItemGLAccount__"))) {
                            Purchasing.PRLineItems.GLAccount.UpdateGroup(item);
                        }
                        //if supplyType is set by User, update induced Fields
                        if (!Sys.Helpers.IsEmpty(item.GetValue("SupplyTypeID__"))) {
                            Purchasing.PRLineItems.SupplyType.FillInducedFields(item, false, false);
                            Purchasing.PRLineItems.SupplyType.FillBuyerAndRecipient(item, index);
                        }
                        if (!Sys.Helpers.IsEmpty(item.GetValue("VendorNumber__")) && Sys.Helpers.IsEmpty(item.GetValue("VendorName__"))) {
                            Purchasing.PRLineItems.Vendor.UpdateVendorName(item);
                        }
                        // Set the RequestedDeliveryDate__ if the item has a lead time stored in the catalog
                        if (Sys.Helpers.IsEmpty(item.GetValue("ItemRequestedDeliveryDate__"))) {
                            Purchasing.PRLineItems.RequestedDeliveryDate.Set(item, index);
                        }
                        // Unit Price has been set/updated, need to recompute the net Amount
                        Purchasing.PRLineItems.ExchangeRate.Set(item);
                        // Update Tax Rate
                        Purchasing.PRLineItems.TaxCode.UpdateTaxRate(item);
                        if (!Sys.Helpers.IsEmpty(item.GetValue("ItemShipToCompany__"))) {
                            Lib.Purchasing.ShipTo.FillFromShipToCompany(item, index === 0);
                        }
                        if (Sys.Helpers.TryGetFunction("Lib.PR.Customization.Client.UpdateLineItemCopyFromClipboard")) {
                            Sys.Helpers.TryCallFunction("Lib.PR.Customization.Client.UpdateLineItemCopyFromClipboard");
                        }
                        Purchasing.PRLineItems.OnAddItem(item, index);
                        Purchasing.PRLineItems.OnRefreshRow(index);
                        var row = Controls.LineItems__.GetRow(index);
                        if (Lib.Purchasing.Items.IsServiceBasedItem(item)) {
                            if (!row.ItemStartDate__.GetValue() && rowData.ItemStartDate__) {
                                row.ItemStartDate__.SetText(rowData.ItemStartDate__);
                            }
                            if (!row.ItemEndDate__.GetValue() && rowData.ItemEndDate__) {
                                row.ItemEndDate__.SetText(rowData.ItemEndDate__);
                            }
                            item.SetValue("ItemRequestedDeliveryDate__", item.GetValue("ItemStartDate__"));
                        }
                        else if ((!row.Locked__.GetValue() || !row.LeadTime__.GetValue()) && !Sys.Helpers.IsEmpty(rowData.ItemRequestedDeliveryDate__)) {
                            row.ItemRequestedDeliveryDate__.SetText(rowData.ItemRequestedDeliveryDate__);
                        }
                    });
                });
            },
            Budget: (function () {
                var isOut = false;
                var isUndefined = false;
                var budgetVisibility = null;
                // ID used to manage concurrent FillBudget tasks
                var fillBudgetID = 0;
                return {
                    IsOut: function () {
                        if (Lib.Purchasing.OutOfBudgetBehavior.IsAllowed()) {
                            return false;
                        }
                        return isOut;
                    },
                    IsUndefined: function () {
                        if (Lib.Purchasing.UndefinedBudgetBehavior.IsAllowed()) {
                            return false;
                        }
                        return isUndefined;
                    },
                    // Returns true if current user has read access to the line items budget
                    // if no item specified, returns true if at least one line item has accessible key value
                    HasVisibilityOnCurrentBudget: function (item) {
                        if (Lib.Budget.IsDisabled()) {
                            return true;
                        }
                        if (budgetVisibility) {
                            if (budgetVisibility.CheckAll()) {
                                return true;
                            }
                            if (item) {
                                return budgetVisibility.CheckLineItem(item);
                            }
                            // returns true if at least une line item has accessible key value
                            var lineItems = Data.GetTable("LineItems__");
                            var count = lineItems.GetItemCount();
                            for (var i = 0; i < count; i++) {
                                item = lineItems.GetItem(i);
                                if (!Lib.Purchasing.IsLineItemEmpty(item)) {
                                    if (budgetVisibility.CheckLineItem(item)) {
                                        return true;
                                    }
                                }
                            }
                        }
                        return false;
                    },
                    Fill: function (options, currentItem) {
                        options = options || {};
                        if (Lib.Budget.IsDisabled()) {
                            return;
                        }
                        if (options.resetBudgetID) {
                            this.ResetBudgetID(currentItem);
                        }
                        // keep in closure the current fillBudgetID
                        var myFillBudgetID = ++fillBudgetID;
                        Log.Info("Fill budget with ID " + myFillBudgetID);
                        Lib.Purchasing.LayoutPR.SetButtonsDisabled(true, "Start Budget.Fill");
                        if (budgetVisibility === null) {
                            Log.Info("Find available Cost centers for the current user");
                            Lib.Purchasing.LayoutPR.SetButtonsDisabled(true, "Init BudgetVisibility");
                            budgetVisibility = new Lib.Budget.Visibility({
                                login: User.loginId,
                                validationKeyColumns: Lib.P2P.GetBudgetValidationKeyColumns()
                            });
                            budgetVisibility.Ready()
                                .Then(function () {
                                Lib.Purchasing.LayoutPR.UpdateLayout();
                                Lib.Purchasing.LayoutPR.SetButtonsDisabled(false, "Init BudgetVisibility -> resolved");
                            })
                                .Catch(function () {
                                Lib.Purchasing.LayoutPR.SetButtonsDisabled(false, "Init BudgetVisibility -> rejected");
                            });
                        }
                        // compute budget
                        budgetVisibility.Ready()
                            .Then(function () {
                            // optimization if no visibility on budget.
                            if (!Purchasing.PRLineItems.Budget.HasVisibilityOnCurrentBudget()) {
                                Lib.Purchasing.LayoutPR.SetButtonsDisabled(false, "Compute Budget -> no visibility");
                            }
                            else {
                                Lib.Budget.GetBudgets({
                                    impactAction: "to approve",
                                    visibility: budgetVisibility
                                })
                                    .Then(function (budgets) {
                                    Log.Info("Ready to fill budget with current ID " + myFillBudgetID);
                                    if (myFillBudgetID === fillBudgetID) {
                                        isOut = false;
                                        isUndefined = false;
                                        Sys.Helpers.SilentChange(function () {
                                            Sys.Helpers.Data.ForEachTableItem("LineItems__", function (item, i) {
                                                if (!Lib.Purchasing.IsLineItemEmpty(item)) {
                                                    var budgetID = budgets.byItemIndex[i];
                                                    var BudgetVisibilityOnCurrentLine = budgetVisibility.CheckLineItem(item);
                                                    if (Sys.Helpers.IsString(budgetID)) {
                                                        var budget = budgets.byBudgetID[budgetID];
                                                        var prevBudgetViewed = Sys.Helpers.Data.IsTrue(item.GetValue("ItemBudgetViewed__"));
                                                        var prevOutOfBudget = Sys.Helpers.Data.IsTrue(item.GetValue("OutOfBudget__"));
                                                        item.SetValue("ItemBudgetInitial__", budget.Budget__);
                                                        item.SetValue("BudgetID__", budgetID);
                                                        if (budget.Closed__ && !prevBudgetViewed) {
                                                            item.SetError("BudgetID__", "_This budget is closed");
                                                        }
                                                        else {
                                                            item.SetValue("ItemBudgetViewed__", true);
                                                        }
                                                        item.SetValue("ItemBudgetRemaining__", budget.Remaining__);
                                                        Lib.P2P.CompanyCodesValue.QueryValues(Controls.CompanyCode__.GetValue()).Then(function (CCValues) {
                                                            if (Object.keys(CCValues).length > 0) {
                                                                item.SetValue("ItemBudgetCurrency__", CCValues.Currency__);
                                                            }
                                                        });
                                                        item.SetValue("ItemPeriodCode__", budget.PeriodCode__);
                                                        var outOfBudget = budget.Remaining__ < 0;
                                                        if (outOfBudget) {
                                                            // We only consider PR is out of budget if :
                                                            //  - it's the first time someone approves this budget
                                                            //  - the document has been already approved with this negative remaining budget
                                                            if (!prevBudgetViewed || prevOutOfBudget) {
                                                                item.SetValue("OutOfBudget__", true);
                                                                isOut = true;
                                                            }
                                                        }
                                                        else {
                                                            item.SetValue("OutOfBudget__", false);
                                                        }
                                                    }
                                                    //budgetID is undefined when the user don't have the visibility on line, or when a key value is missing
                                                    //reset only when hte user has the visibility
                                                    else if (BudgetVisibilityOnCurrentLine && (budgetID instanceof Lib.Budget.MissingBudgetIDError || !budgetID)) {
                                                        item.SetValue("ItemBudgetInitial__", null);
                                                        item.SetValue("BudgetID__", null);
                                                        item.SetError("BudgetID__", "");
                                                        item.SetValue("ItemBudgetRemaining__", null);
                                                        item.SetValue("ItemBudgetCurrency__", null);
                                                        item.SetValue("ItemPeriodCode__", null);
                                                        item.SetValue("ItemBudgetViewed__", true);
                                                        item.SetValue("OutOfBudget__", false);
                                                        //Budget is undefined if at least 1 line for witch the user have visibility on has no budget defined
                                                        isUndefined = true;
                                                    }
                                                    else if (BudgetVisibilityOnCurrentLine && (budgetID instanceof Lib.Budget.MultipleBudgetError)) {
                                                        item.SetError("BudgetID__", "_Multiple budget item error");
                                                        item.SetValue("ItemBudgetInitial__", null);
                                                        item.SetValue("BudgetID__", null);
                                                        item.SetValue("ItemBudgetRemaining__", null);
                                                        item.SetValue("ItemBudgetCurrency__", null);
                                                        item.SetValue("ItemPeriodCode__", null);
                                                        item.SetValue("ItemBudgetViewed__", true);
                                                        item.SetValue("OutOfBudget__", false);
                                                    }
                                                }
                                            });
                                        });
                                        Purchasing.PRLineItems.Budget.Refresh(); // visual refresh
                                    }
                                    else {
                                        Log.Info("Ignore this task because another concurrent FillBudget is pending or done with this ID " + fillBudgetID);
                                    }
                                    Lib.Purchasing.LayoutPR.SetButtonsDisabled(false, "Compute Budget -> resolved");
                                })
                                    .Catch(function (error) {
                                    if (myFillBudgetID === fillBudgetID) {
                                        Log.Error("Retrieve budgets with error: " + error);
                                    }
                                    Lib.Purchasing.LayoutPR.SetButtonsDisabled(false, "Compute Budget -> rejected");
                                });
                            }
                        });
                    },
                    Refresh: function (index) {
                        if (Lib.Budget.IsDisabled()) {
                            return;
                        }
                        var table = Controls.LineItems__, row;
                        function UpdateCellStyle(cell) {
                            if (cell.GetValue() < 0) {
                                cell.AddStyle("text-highlight-warning");
                            }
                            else {
                                cell.RemoveStyle("text-highlight-warning");
                            }
                        }
                        if (index) {
                            row = table.GetRow(index);
                            UpdateCellStyle(row.ItemBudgetRemaining__);
                        }
                        else {
                            var i = void 0;
                            for (i = table.GetLineCount(true) - 1; i >= 0; i--) {
                                row = table.GetRow(i);
                                UpdateCellStyle(row.ItemBudgetRemaining__);
                            }
                        }
                    },
                    ResetBudgetID: function (currentRow) {
                        // reset budgetID: for instance budget is only driven by assignments
                        if (currentRow) {
                            if (currentRow.BudgetID__ == null) {
                                currentRow.SetValue("BudgetID__", null);
                                if (currentRow.GetItem() != null) {
                                    currentRow.SetError("BudgetID__", "");
                                }
                                currentRow.SetValue("ItemBudgetViewed__", false);
                                currentRow.SetValue("OutOfBudget__", false);
                            }
                            else {
                                currentRow.BudgetID__.SetValue(null);
                                if (currentRow.GetItem() != null) {
                                    currentRow.BudgetID__.SetError("");
                                }
                                currentRow.ItemBudgetViewed__.SetValue(false);
                                currentRow.OutOfBudget__.SetValue(false);
                            }
                        }
                    }
                };
            })(),
            NumberOrDescription: {
                itemsNumber: {},
                itemFieldsMapping: {
                    "ITEMTAXCODE__.TAXRATE__": "ItemTaxRate__",
                    "ITEMGLACCOUNT__.GROUP__": "ItemGroup__",
                    "UNITOFMEASURE__.DESCRIPTION__": "ItemUnitDescription__",
                    "UNITOFMEASURE__": "ItemUnit__",
                    "VENDORNUMBER__.NAME__": "VendorName__",
                    "SUPPLYTYPEID__.NAME__": "SupplyTypeName__",
                    "SUPPLYTYPEID__.NOGOODSRECEIPT__": "NoGoodsReceipt__"
                },
                OnChange: function () {
                    Log.Info("Item number/description OnChange");
                    Purchasing.PRLineItems.fromUserChange = true;
                    var index = this.GetRow().GetLineNumber() - 1;
                    var item = this.GetItem();
                    if (Purchasing.PRLineItems.NumberOrDescription.itemsNumber[index]) {
                        if (Purchasing.PRLineItems.NumberOrDescription.itemsNumber[index] !== item.GetValue("ItemNumber__")) {
                            item.SetValue("ItemOrigin__", "FreeItem");
                        }
                        delete Purchasing.PRLineItems.NumberOrDescription.itemsNumber[index];
                    }
                    else {
                        item.SetValue("ItemOrigin__", "FreeItem");
                    }
                    //Will trigger OnAddItem, that will set quantity to 1, and will compute the amount
                    Purchasing.PRLineItems.LeaveEmptyLine();
                },
                $OnSelectItem: function (selectedItem, tableItem, tableItemIndex) {
                    var isUnitOfMeasureEnabled = Sys.Parameters.GetInstance("PAC").GetParameter("DisplayUnitOfMeasure");
                    var isCostTypeEnabled = Sys.Parameters.GetInstance("PAC").GetParameter("DisplayCostType");
                    selectedItem.GetValue = selectedItem.GetValue || function (attribute) {
                        return this[attribute.toUpperCase()];
                    };
                    Lib.Purchasing.CatalogHelper.CatalogItem.Attributes.forEach(function (attribute) {
                        var upperAttribute = attribute.toUpperCase();
                        //Do not fill unit of measure if not enabled in order to let SAP decide wich unit of measur he want. (ST by default)
                        if ((isUnitOfMeasureEnabled == true || upperAttribute.startsWith("UNITOFMEASURE__") == false)
                            && (isCostTypeEnabled == true || upperAttribute.startsWith("COSTTYPE__") == false)) {
                            if (Sys.Helpers.Data.FieldExistInTable("LineItems__", upperAttribute)) {
                                tableItem.SetValue(upperAttribute, selectedItem.GetValue(upperAttribute));
                            }
                            if (Purchasing.PRLineItems.NumberOrDescription.itemFieldsMapping[upperAttribute]) {
                                tableItem.SetValue(Purchasing.PRLineItems.NumberOrDescription.itemFieldsMapping[upperAttribute], selectedItem.GetValue(upperAttribute));
                            }
                        }
                    });
                    tableItem.SetValue("ItemOrigin__", "InternalCatalog");
                    Purchasing.PRLineItems.NumberOrDescription.itemsNumber[tableItemIndex] = selectedItem.GetValue("ItemNumber__");
                    //default value in case of upgrade
                    tableItem.SetValue("ItemType__", selectedItem.GetValue("ItemType__") || Lib.P2P.ItemType.QUANTITY_BASED);
                    tableItem.SetValue("ItemQuantity__", selectedItem.GetValue("ITEMQUANTITY__") || 1);
                    if (Lib.Purchasing.Items.IsAmountBasedItem(tableItem)) {
                        tableItem.SetValue("ItemNetAmount__", selectedItem.GetValue("ItemUnitPrice__"));
                    }
                    Purchasing.PRLineItems.ComputeItemAmount(tableItemIndex);
                    // Unit Price has been set from catalog, need to recompute the net Amount
                    Purchasing.PRLineItems.ExchangeRate.Set(tableItem);
                    // Set the RequestedDeliveryDate__ if the item has a lead time stored in the catalog
                    Purchasing.PRLineItems.RequestedDeliveryDate.Set(tableItem);
                    if (tableItem.GetValue("ItemGroup__")) {
                        //Item group come from "ITEMGLACCOUNT__.GROUP__", which is the default group for the catalog item's glAccount
                        Purchasing.PRLineItems.ExpenseCategory.Update(tableItem);
                        //Else it will be set by the group of the default glaccount of the supplyType
                    }
                    //supplyTypeId__ already set, now set all dependent empty fields
                    Purchasing.PRLineItems.SupplyType.FillInducedFields(tableItem, false, false);
                    Purchasing.PRLineItems.SupplyType.FillBuyerAndRecipient(tableItem, tableItemIndex);
                    //will call OnAddItem if it's a new item
                    Purchasing.PRLineItems.fromUserChange = true;
                    Purchasing.PRLineItems.LeaveEmptyLine();
                },
                OnSelectItems: function (selectedItems) {
                    Log.Info("Item number/description OnSelectItems");
                    var items;
                    if (Array.isArray(selectedItems) == false) {
                        items = [selectedItems];
                    }
                    else {
                        items = selectedItems;
                    }
                    var table = Data.GetTable("LineItems__");
                    var currentRowIndex = table.GetItemCount() - 1;
                    for (var i = 0; i < items.length; ++i) {
                        var item = items[i];
                        var currentItem = table.GetItem(currentRowIndex);
                        if (!currentItem || currentItem.GetValue("ItemDescription__")) {
                            var nbreNotAddedItems = items.length - i;
                            var maxLines = Controls.LineItems__.GetLineCount();
                            var errorMessage = Language.Translate(nbreNotAddedItems === 1 ? "_item has been ignored {0} {1}" : "_items have been ignored {0} {1}", false, nbreNotAddedItems, maxLines);
                            var Options = {
                                message: errorMessage,
                                status: "error",
                                timeout: 10000
                            };
                            Popup.Snackbar(Options);
                            break;
                        }
                        Purchasing.PRLineItems.NumberOrDescription.$OnSelectItem(item, currentItem, currentRowIndex);
                        currentRowIndex++;
                    }
                    if (Controls.LineItems__.GetLineCount() == currentRowIndex) {
                        Purchasing.PRLineItems.OnRefreshRow(currentRowIndex - 1);
                    }
                },
                OnSelectItem: function (selectedItem) {
                    Log.Info("Item number/description OnSelectItem");
                    var currentRow = this.GetRow();
                    var currentItem = currentRow.GetItem();
                    var currentRowIndex = currentRow.GetLineNumber() - 1;
                    Purchasing.PRLineItems.NumberOrDescription.$OnSelectItem(selectedItem, currentItem, currentRowIndex);
                    Purchasing.PRLineItems.SetLastLineItemType(currentItem, currentRowIndex);
                },
                FillLineItemFromCatalog: function (item, data) {
                    var isUnitOfMeasureEnabled = Sys.Parameters.GetInstance("PAC").GetParameter("DisplayUnitOfMeasure");
                    var isCostTypeEnabled = Sys.Parameters.GetInstance("PAC").GetParameter("DisplayCostType");
                    // fill item line
                    Lib.Purchasing.CatalogHelper.CatalogItem.Attributes.forEach(function (attribute) {
                        var upperAttribute = attribute.toUpperCase();
                        var mappingAttribute = Purchasing.PRLineItems.NumberOrDescription.itemFieldsMapping[upperAttribute];
                        var isItemLocked = data["LOCKED__"] == 1
                            && ((Purchasing.PRLineItems.LOCKED_FIELDS.indexOf(upperAttribute) != -1)
                                || (Purchasing.PRLineItems.LOCKED_FIELDS.indexOf(mappingAttribute) != -1));
                        //Do not fill unit of measure if not enabled in order to let SAP decide wich unit of measur he want. (ST by default)
                        if ((isUnitOfMeasureEnabled == true || upperAttribute.startsWith("UNITOFMEASURE__") == false)
                            && (isCostTypeEnabled == true || upperAttribute.startsWith("COSTTYPE__") == false)) {
                            if (Sys.Helpers.Data.FieldExistInTable("LineItems__", upperAttribute)
                                && (isItemLocked || (item.GetValue(upperAttribute) === null && Sys.Helpers.IsEmpty(item.GetError(upperAttribute))))) {
                                item.SetValue(upperAttribute, data[upperAttribute]);
                            }
                            if (mappingAttribute && (isItemLocked || (item.GetValue(mappingAttribute) === null && Sys.Helpers.IsEmpty(item.GetError(mappingAttribute))))) {
                                item.SetValue(mappingAttribute, data[upperAttribute]);
                            }
                        }
                    });
                    if (Sys.Helpers.Data.FieldExistInTable("LineItems__", "ITEMTYPE__") && data["ITEMTYPE__"] === Lib.P2P.ItemType.AMOUNT_BASED) {
                        item.SetValue("ItemType__", Lib.P2P.ItemType.AMOUNT_BASED);
                    }
                    item.SetValue("ItemOrigin__", "InternalCatalog");
                },
                UpdateNumberOrDescription: function (item) {
                    if (item.GetValue("ItemNumber__") !== null || item.GetValue("ItemDescription__") !== null) {
                        var filter = "&(|(ItemCompanyCode__=" + Data.GetValue("CompanyCode__") + ")(ItemCompanyCode__=)(ItemCompanyCode__!=*))(|(ItemNumber__=" + item.GetValue("ItemNumber__") + ")(ItemDescription__=" + item.GetValue("ItemDescription__") + "))";
                        var options = {
                            table: "PurchasingOrderedItems__",
                            filter: filter,
                            attributes: Lib.Purchasing.CatalogHelper.CatalogItem.Attributes,
                            maxRecords: 2,
                            additionalOptions: "EnableJoin=1"
                        };
                        return Sys.GenericAPI.PromisedQuery(options)
                            .Then(function (results) {
                            if (results && results.length === 1) {
                                var result = results[0];
                                Purchasing.PRLineItems.NumberOrDescription.FillLineItemFromCatalog(item, result);
                                return Sys.Helpers.Promise.Resolve(result);
                            }
                            return Sys.Helpers.Promise.Resolve(null);
                        });
                    }
                    return Sys.Helpers.Promise.Resolve(null);
                }
            },
            Quantity: {
                OnChange: function () {
                    Purchasing.PRLineItems.fromUserChange = true;
                    var index = this.GetRow().GetLineNumber() - 1;
                    Purchasing.PRLineItems.ComputeItemAmount(index);
                    Purchasing.PRLineItems.LeaveEmptyLine();
                }
            },
            RequestedDeliveryDate: {
                AddLeadTime: function (leadTime) {
                    var date = null;
                    if (!Sys.Helpers.IsEmpty(leadTime)) {
                        date = new Date();
                        date.setHours(0, 0, 0, 0);
                        date.setDate(date.getDate() + parseInt(leadTime, 10));
                    }
                    return date;
                },
                Set: function (item, index) {
                    var defaultRequestedDeliveryDate = null;
                    var leadTime = item.GetValue("LeadTime__");
                    if (!Sys.Helpers.IsEmpty(leadTime)) {
                        defaultRequestedDeliveryDate = Purchasing.PRLineItems.RequestedDeliveryDate.AddLeadTime(leadTime);
                    }
                    else {
                        // Get last added item without a lead time
                        var table = Data.GetTable("LineItems__");
                        var currentRowIndex = index === void 0 ? table.GetItemCount() - 3 : index;
                        var lastKeyedDate = null;
                        var lastKeyedDateFound = false;
                        while (!lastKeyedDateFound && currentRowIndex >= 0) {
                            var rowLeadTime = table.GetItem(currentRowIndex).GetValue("LeadTime__");
                            if (Sys.Helpers.IsEmpty(rowLeadTime)) {
                                lastKeyedDateFound = true;
                                lastKeyedDate = table.GetItem(currentRowIndex).GetValue("ItemRequestedDeliveryDate__");
                            }
                            else {
                                currentRowIndex--;
                            }
                        }
                        if (!Sys.Helpers.IsEmpty(lastKeyedDate)) {
                            defaultRequestedDeliveryDate = new Date(lastKeyedDate);
                        }
                    }
                    item.SetValue("ItemRequestedDeliveryDate__", defaultRequestedDeliveryDate);
                },
                OnChange: function () {
                    Purchasing.PRLineItems.fromUserChange = true;
                    var currentRow = this.GetRow();
                    var currentItem = this.GetItem();
                    if (Lib.Purchasing.CheckPR.requiredFields.CheckItemsDeliveryDates(Lib.Purchasing.LayoutPR.GetCurrentLayout(), {
                        // return item index - call with inWholeTable (1-based API)
                        specificItemIndex: currentRow.GetLineNumber(/*inWholeTable*/ true) - 1
                    })) {
                        var hasLeadTime = !Sys.Helpers.IsEmpty(currentItem.GetValue("LeadTime__"));
                        if (hasLeadTime) {
                            var newRequestedDeliveryDate = new Date(currentItem.GetValue("ItemRequestedDeliveryDate__"));
                            var defaultRequestedDeliveryDate = Purchasing.PRLineItems.RequestedDeliveryDate.AddLeadTime(currentItem.GetValue("LeadTime__"));
                            if (newRequestedDeliveryDate < defaultRequestedDeliveryDate) {
                                currentItem.SetWarning("ItemRequestedDeliveryDate__", "_Warning the requested delivery date is too soon (expected {0} or later)", Language.FormatDate(defaultRequestedDeliveryDate));
                            }
                            else {
                                currentItem.SetWarning("ItemRequestedDeliveryDate__", "");
                            }
                        }
                        else {
                            AutoFillLineItemsWithEmptyValue("ItemRequestedDeliveryDate__", currentItem.GetValue("ItemRequestedDeliveryDate__"), {
                                extraCondition: function (emptyItem) { return !Lib.Purchasing.Items.IsServiceBasedItem(emptyItem); }
                            });
                        }
                        if (Lib.Purchasing.LayoutPR.IsApprover()) {
                            Log.Info("Update budget");
                            Purchasing.PRLineItems.Budget.Fill({ resetBudgetID: true }, currentItem);
                        }
                        else {
                            Purchasing.PRLineItems.Budget.ResetBudgetID(currentItem);
                        }
                    }
                    Purchasing.PRLineItems.LeaveEmptyLine();
                }
            },
            UnitPrice: {
                OnChange: function () {
                    Purchasing.PRLineItems.fromUserChange = true;
                    var index = this.GetRow().GetLineNumber() - 1;
                    Purchasing.PRLineItems.ComputeItemAmount(index);
                    Purchasing.PRLineItems.LeaveEmptyLine();
                }
            },
            ItemCurrency: {
                OnSelectItem: function (item) {
                    var currentItem = this.GetItem();
                    if (item) {
                        var itemExchangeRate = parseFloat(item.GetValue("Rate__"));
                        var ratioFrom = parseFloat(item.GetValue("RatioFrom__"));
                        var ratioTo = parseFloat(item.GetValue("RatioTo__"));
                        var exchangeRate = new Sys.Decimal(itemExchangeRate).mul(ratioTo).div(ratioFrom);
                        var precision = Controls.LineItems__.ItemExchangeRate__.GetPrecision() || 2;
                        currentItem.SetValue("ItemExchangeRate__", Sys.Helpers.Round(exchangeRate, precision).toNumber());
                        AutoFillLineItemsWithEmptyValue("ItemCurrency__", item.GetValue("CurrencyFrom__"));
                        Purchasing.PRLineItems.ComputeAmounts(true);
                        Purchasing.PRLineItems.LeaveEmptyLine();
                    }
                    else {
                        currentItem.SetValue("ItemExchangeRate__", "");
                        currentItem.SetError("ItemExchangeRate__", "The value could not be resolve");
                    }
                }
            },
            PONumber: {
                SetLink: function (row) {
                    if (!Sys.Helpers.IsEmpty(row.PONumber__.GetValue())) {
                        row.PONumber__.DisplayAs({ type: "Link" });
                    }
                },
                OnClick: function (OrderNumber) {
                    Controls.LineItems__.PONumber__.Wait(true);
                    var options = {
                        table: "CDNAME#Purchase order V2",
                        filter: "OrderNumber__=" + OrderNumber,
                        attributes: ["MsnEx", "ValidationURL"],
                        sortOrder: null,
                        maxRecords: 1
                    };
                    Sys.GenericAPI.PromisedQuery(options)
                        .Then(function (queryResult) {
                        if (queryResult.length == 1) {
                            Process.OpenLink(queryResult[0].ValidationURL + "&OnQuit=Close");
                        }
                        else {
                            Popup.Alert("_Purchase order not found or access denied", false, null, "_Purchase order not found title");
                        }
                    })
                        .Catch(function ( /*error: string*/) {
                        Popup.Alert("_Purchase order not found or access denied", false, null, "_Purchase order not found title");
                    })
                        .Finally(function () {
                        Controls.LineItems__.PONumber__.Wait(false);
                    });
                }
            },
            ComputeItemAmount: function (index) {
                var item = Data.GetTable("LineItems__").GetItem(index);
                if (Lib.Purchasing.Items.IsAmountBasedItem(item)) {
                    var netAmount = item.GetValue("ItemNetAmount__");
                    if (!netAmount || netAmount === 0) {
                        netAmount = item.GetValue("ItemQuantity__");
                    }
                    item.SetValue("ItemQuantity__", netAmount || 0);
                    item.SetValue("ItemUnitPrice__", 1);
                    item.SetValue("ItemNetAmount__", netAmount || 0);
                }
                else {
                    var unitPrice = item.GetValue("ItemUnitPrice__") || 0;
                    var quantity = item.GetValue("ItemQuantity__");
                    if (item.IsNullOrEmpty("ItemQuantity__") || quantity === 0) {
                        item.SetValue("ItemQuantity__", 1);
                        quantity = 1;
                    }
                    var d_netAmount = new Sys.Decimal(quantity).mul(unitPrice);
                    var precision = Controls.LineItems__.ItemNetAmount__.GetPrecision() || 2;
                    item.SetValue("ItemNetAmount__", Sys.Helpers.Round(d_netAmount, precision).toNumber());
                }
                Purchasing.PRLineItems.ComputeAmounts();
            },
            ComputeAmounts: function (forceRebuildWorkflow) {
                if (forceRebuildWorkflow === void 0) { forceRebuildWorkflow = false; }
                Log.Info("ComputeAmounts");
                var table = Data.GetTable("LineItems__");
                // Call PRLineItems.Count() to remove empty lines (else we could have used a foreach loop)
                var count = Purchasing.PRLineItems.Count(), i;
                var items = [];
                for (i = 0; i < count; i++) {
                    items.push(table.GetItem(i));
                }
                var beforeTotalNetLocal = Data.GetValue("TotalNetLocalCurrency__");
                var beforeTotalNetAmount = Data.GetValue("TotalNetAmount__");
                var res = Lib.Purchasing.Items.ComputePRAmounts(items);
                Data.SetValue("TotalNetLocalCurrency__", res.netAmountLocal);
                var companyCodeValues = Lib.P2P.CompanyCodesValue.GetValues(Data.GetValue("CompanyCode__"));
                Data.SetValue("TotalNetAmount__", res.TotalNetAmount);
                Controls.TotalNetAmount__.SetLabel(Language.Translate("_Total net amount", false, res.Currency));
                Data.SetValue("Currency__", res.Currency);
                if (companyCodeValues && companyCodeValues.currencies) {
                    var exchangeRate = companyCodeValues.currencies.GetRate(res.Currency);
                    Data.SetValue("ExchangeRate__", exchangeRate);
                }
                Controls.TotalNetAmount__.Hide(!res.isMonoCurrency || !res.hasForeignCurrency);
                var afterTotalNetLocal = Data.GetValue("TotalNetLocalCurrency__");
                var afterTotalNetAmount = Data.GetValue("TotalNetAmount__");
                if (forceRebuildWorkflow || beforeTotalNetLocal != afterTotalNetLocal || beforeTotalNetAmount != afterTotalNetAmount) {
                    Lib.Purchasing.LayoutPR.DelayRebuildWorkflow();
                }
            },
            ExchangeRate: {
                Set: function (item) {
                    if (Lib.P2P.CompanyCodesValue.GetValues(Data.GetValue("CompanyCode__")).currencies) {
                        var exchangeRate = Lib.P2P.CompanyCodesValue.GetValues(Data.GetValue("CompanyCode__")).currencies.GetRate(item.GetValue("ItemCurrency__"));
                        item.SetValue("ItemExchangeRate__", exchangeRate);
                    }
                }
            },
            NetAmount: {
                OnChange: function () {
                    Purchasing.PRLineItems.fromUserChange = true;
                    var index = this.GetRow().GetLineNumber() - 1;
                    Purchasing.PRLineItems.ComputeItemAmount(index);
                    Purchasing.PRLineItems.LeaveEmptyLine();
                }
            },
            GLAccount: (function () {
                var timeoutID = null;
                return {
                    OnSelectItem: function (item) {
                        var _this = this;
                        if (item) {
                            if (timeoutID) {
                                clearTimeout(timeoutID);
                            }
                            timeoutID = setTimeout(function () {
                                var currentItem = _this.GetItem();
                                currentItem.SetValue("ItemGroup__", item.GetValue("Group__"));
                                Purchasing.PRLineItems.ExpenseCategory.Update(currentItem);
                                Lib.P2P.fillCostTypeFromGLAccount(currentItem, "ItemGLAccount__")
                                    .Then(function (resultItem) {
                                    Purchasing.PRLineItems.Budget.Fill({ resetBudgetID: true }, resultItem);
                                    Lib.Purchasing.LayoutPR.DelayRebuildWorkflow();
                                });
                                timeoutID = null;
                            });
                        }
                        else {
                            var currentItem = this.GetItem();
                            currentItem.SetValue("ItemGroup__", "");
                            currentItem.SetError("ItemGroup__", "The value could not be resolve");
                        }
                    },
                    OnChange: function () {
                        var _this = this;
                        if (!timeoutID) {
                            timeoutID = setTimeout(function () {
                                var currentRow = _this.GetRow();
                                var currentItem = _this.GetItem();
                                if (!currentRow.ItemGroupDescription__.IsVisible()) {
                                    currentItem.SetValue("ItemGroup__", null);
                                    Purchasing.PRLineItems.ExpenseCategory.Update(currentItem);
                                }
                                Lib.P2P.fillCostTypeFromGLAccount(currentItem, "ItemGLAccount__")
                                    .Then(function (resultItem) {
                                    Purchasing.PRLineItems.Budget.Fill({ resetBudgetID: true }, resultItem);
                                    Lib.Purchasing.LayoutPR.DelayRebuildWorkflow();
                                });
                                timeoutID = null;
                            });
                        }
                    },
                    UpdateGroup: function (item) {
                        var options = {
                            table: "AP - G/L accounts__",
                            filter: "&(Account__=" + item.GetValue("ItemGLAccount__") + ")(CompanyCode__=" + Data.GetValue("CompanyCode__") + ")",
                            attributes: ["Group__", "CostType__"],
                            sortOrder: null,
                            maxRecords: 1
                        };
                        Sys.GenericAPI.PromisedQuery(options)
                            .Then(function (queryResult) {
                            item.SetValue("ItemGroup__", queryResult.length == 1 ? queryResult[0].Group__ : null);
                            if (Sys.Helpers.IsEmpty(item.GetValue("CostType__"))) {
                                item.SetValue("CostType__", queryResult.length == 1 ? queryResult[0].CostType__ : null);
                            }
                        })
                            .Catch(function ( /*error: string*/) {
                            item.SetValue("ItemGroup__", null);
                        })
                            .Finally(function () {
                            Purchasing.PRLineItems.ExpenseCategory.Update(item);
                            Purchasing.PRLineItems.Budget.Fill({ resetBudgetID: true }, item);
                        });
                    }
                };
            })(),
            ProjectCode: {
                OnSelectItem: function (item) {
                    var currentItem = this.GetItem();
                    if (item) {
                        currentItem.SetValue("ProjectCodeDescription__", item.GetValue("Description__"));
                        Lib.Purchasing.LayoutPR.DelayRebuildWorkflow();
                    }
                    else {
                        currentItem.SetValue("ProjectCodeDescription__", "");
                        currentItem.SetError("ProjectCodeDescription__", "The value could not be resolved");
                    }
                },
                OnUnknownOrEmptyValue: function () {
                    var currentItem = this.GetItem();
                    currentItem.SetValue("ProjectCodeDescription__", null);
                    Lib.Purchasing.LayoutPR.DelayRebuildWorkflow();
                }
            },
            ProjectCodeDescription: {
                OnSelectItem: function (item) {
                    var currentItem = this.GetItem();
                    if (item) {
                        currentItem.SetValue("ProjectCode__", item.GetValue("ProjectCode__"));
                        Lib.Purchasing.LayoutPR.DelayRebuildWorkflow();
                    }
                    else {
                        currentItem.SetValue("ProjectCode__", "");
                        currentItem.SetError("ProjectCode__", "The value could not be resolved");
                    }
                },
                OnUnknownOrEmptyValue: function () {
                    var currentItem = this.GetItem();
                    currentItem.SetValue("ProjectCode__", null);
                    Lib.Purchasing.LayoutPR.DelayRebuildWorkflow();
                }
            },
            ExpenseCategory: {
                OnSelectItem: function (item) {
                    var expenseCategory = item.GetValue("ExpenseCategory__");
                    var currentItem = this.GetItem();
                    if (item) {
                        currentItem.SetValue("ItemGroup__", expenseCategory);
                        // Fill ItemGLAccount__ if expense category only used for 1 GLAccount
                        var options = {
                            table: "AP - G/L accounts__",
                            filter: "&(Group__=" + expenseCategory + ")(CompanyCode__=" + Data.GetValue("CompanyCode__") + ")",
                            attributes: ["Account__"],
                            sortOrder: null,
                            maxRecords: 2
                        };
                        Sys.GenericAPI.PromisedQuery(options)
                            .Then(function (queryResult) {
                            currentItem.SetValue("ItemGLAccount__", queryResult.length == 1 ? queryResult[0].Account__ : null);
                            if (Sys.Helpers.IsEmpty(currentItem.GetValue("CostType__"))) {
                                Lib.P2P.fillCostTypeFromGLAccount(currentItem, "ItemGLAccount__")
                                    .Then(function (resultItem) {
                                    Lib.Purchasing.LayoutPR.DelayRebuildWorkflow();
                                    Purchasing.PRLineItems.Budget.Fill({ resetBudgetID: true }, resultItem);
                                });
                            }
                            else {
                                Purchasing.PRLineItems.Budget.Fill({ resetBudgetID: true }, currentItem);
                            }
                        })
                            .Catch(function ( /*error: string*/) {
                            currentItem.SetValue("ItemGLAccount__", null);
                        });
                    }
                    else {
                        currentItem.SetValue("ItemGLAccount__", "");
                        currentItem.SetError("ItemGLAccount__", "The value could not be resolve");
                    }
                },
                OnUnknownOrEmptyValue: function () {
                    var currentRow = this.GetRow();
                    var currentItem = this.GetItem();
                    currentItem.SetValue("ItemGroup__", null);
                    if (!currentRow.ItemGLAccount__.IsVisible()) {
                        currentItem.SetValue("ItemGLAccount__", null);
                    }
                    if (!currentRow.CostType__.IsVisible()) {
                        currentItem.SetValue("CostType__", null);
                        Lib.Purchasing.LayoutPR.DelayRebuildWorkflow();
                    }
                    Purchasing.PRLineItems.Budget.Fill({ resetBudgetID: true }, currentItem);
                },
                Update: function (currentItem) {
                    var expenseCategoryId = currentItem.GetValue("ItemGroup__");
                    if (Sys.Helpers.IsEmpty(expenseCategoryId)) {
                        currentItem.SetValue("ItemGroupDescription__", null);
                    }
                    else {
                        var options = {
                            table: "P2P - Expense category__",
                            filter: "&(ExpenseCategory__=" + expenseCategoryId + ")(CompanyCode__=" + Data.GetValue("CompanyCode__") + ")",
                            attributes: ["Description__", "CostType__"],
                            sortOrder: null,
                            maxRecords: 1
                        };
                        Sys.GenericAPI.PromisedQuery(options)
                            .Then(function (queryResult) {
                            currentItem.SetValue("ItemGroupDescription__", queryResult.length == 1 ? queryResult[0].Description__ : null);
                            if (Sys.Helpers.IsEmpty(currentItem.GetValue("CostType__"))) {
                                currentItem.SetValue("CostType__", queryResult.length == 1 ? queryResult[0].CostType__ : null);
                            }
                        })
                            .Catch(function ( /*error: string*/) {
                            currentItem.SetValue("ItemGroupDescription__", null);
                        });
                    }
                }
            },
            CostCenterId: {
                OnSelectItem: function (item) {
                    var currentItem = this.GetItem();
                    if (item) {
                        currentItem.SetValue("ItemCostCenterName__", item.GetValue("Description__"));
                        Purchasing.PRLineItems.Budget.ResetBudgetID(currentItem);
                        Lib.Purchasing.LayoutPR.DelayRebuildWorkflow();
                    }
                    else {
                        currentItem.SetValue("ItemCostCenterName__", "");
                        currentItem.SetError("ItemCostCenterName__", "The value could not be resolve");
                    }
                },
                OnUnknownOrEmptyValue: function () {
                    var currentItem = this.GetItem();
                    currentItem.SetValue("ItemCostCenterName__", null);
                    Purchasing.PRLineItems.Budget.ResetBudgetID(currentItem);
                    Lib.Purchasing.LayoutPR.DelayRebuildWorkflow();
                },
                /*
                * return false if at least one row is correctly filled
                */
                IsEmpty: function () {
                    var table = Data.GetTable("LineItems__");
                    var count = Purchasing.PRLineItems.Count(), i, find = 0;
                    for (i = 0; i < count; i++) {
                        if (!Sys.Helpers.IsEmpty(table.GetItem(i).GetValue("ItemCostCenterId__"))) {
                            find += 1;
                        }
                    }
                    return find === 0;
                }
            },
            CostCenterName: {
                OnSelectItem: function (item) {
                    var currentItem = this.GetItem();
                    if (item) {
                        Purchasing.PRLineItems.fromUserChange = true;
                        currentItem.SetValue("ItemCostCenterId__", item.GetValue("CostCenter__"));
                        this.SetValue(item.GetValue("Description__")); // In SAP, the description is always uppercase.
                        Purchasing.PRLineItems.LeaveEmptyLine();
                        Purchasing.PRLineItems.Budget.ResetBudgetID(currentItem);
                        Lib.Purchasing.LayoutPR.DelayRebuildWorkflow();
                    }
                    else {
                        currentItem.SetValue("ItemCostCenterName__", "");
                        currentItem.SetError("ItemCostCenterName__", "The value could not be resolve");
                    }
                },
                OnUnknownOrEmptyValue: function () {
                    var currentItem = this.GetItem();
                    Purchasing.PRLineItems.fromUserChange = true;
                    currentItem.SetValue("ItemCostCenterId__", null);
                    Purchasing.PRLineItems.LeaveEmptyLine();
                    Purchasing.PRLineItems.Budget.ResetBudgetID(currentItem);
                    Lib.Purchasing.LayoutPR.DelayRebuildWorkflow();
                },
                UpdateCostCenterID: function (item) {
                    return Sys.Helpers.Promise.Create(function (resolve) {
                        var costCenterName = item.GetValue("ItemCostCenterName__");
                        if (costCenterName !== null) {
                            var options = {
                                table: "AP - Cost centers__",
                                filter: "&(Description__=" + costCenterName + ")(|(CompanyCode__=" + Data.GetValue("CompanyCode__") + ")(CompanyCode__=)(CompanyCode__!=*))",
                                attributes: ["CostCenter__"],
                                sortOrder: null,
                                maxRecords: 2
                            };
                            Sys.GenericAPI.PromisedQuery(options)
                                .Then(function (queryResult) {
                                var costCenterId = null;
                                if (queryResult && queryResult.length != 0) {
                                    if (queryResult.length == 1) {
                                        costCenterId = queryResult[0].CostCenter__;
                                    }
                                    else {
                                        // Vendor name ambiguous: multiple vendors have the same name
                                        item.SetError("ItemCostCenterName__", "The value could not be resolve");
                                        item.SetError("ItemCostCenterId__", "The value could not be resolve");
                                    }
                                }
                                else {
                                    item.SetError("ItemCostCenterName__", "No cost center found");
                                    item.SetError("ItemCostCenterId__", "No cost center found");
                                }
                                item.SetValue("ItemCostCenterId__", costCenterId);
                            })
                                .Catch(function ( /*error: string*/) {
                                item.SetValue("ItemCostCenterId__", null);
                            })
                                .Finally(function () {
                                resolve(item);
                            });
                        }
                        else {
                            resolve(item);
                        }
                    });
                }
            },
            WBSElement: {
                OnSelectItem: function (item) {
                    var currentItem = this.GetItem();
                    if (item) {
                        currentItem.SetValue("WBSElementID__", item.GetValue("WBSElementID__"));
                        Lib.Purchasing.LayoutPR.DelayRebuildWorkflow();
                    }
                    else {
                        currentItem.SetValue("WBSElementID__", "");
                        currentItem.SetError("WBSElementID__", "The value could not be resolved");
                    }
                },
                OnUnknownOrEmptyValue: function () {
                    var currentItem = this.GetItem();
                    currentItem.SetValue("WBSElementID__", null);
                    Lib.Purchasing.LayoutPR.DelayRebuildWorkflow();
                }
            },
            WBSElementID: {
                OnSelectItem: function (item) {
                    var currentItem = this.GetItem();
                    if (item) {
                        currentItem.SetValue("WBSElement__", item.GetValue("Description__"));
                        Lib.Purchasing.LayoutPR.DelayRebuildWorkflow();
                    }
                    else {
                        currentItem.SetValue("WBSElement__", "");
                        currentItem.SetError("WBSElement__", "The value could not be resolved");
                    }
                },
                OnUnknownOrEmptyValue: function () {
                    var currentItem = this.GetItem();
                    currentItem.SetValue("WBSElement__", null);
                    Lib.Purchasing.LayoutPR.DelayRebuildWorkflow();
                }
            },
            Vendor: {
                OnUnknownOrEmptyValue: function () {
                    Purchasing.PRLineItems.fromUserChange = true;
                    this.GetRow().VendorNumber__.SetValue("");
                    Variable.SetValueAsString("lastExtractedVendorName", null);
                    Variable.SetValueAsString("lastExtractedVendorNumber", null);
                    Purchasing.PRLineItems.LeaveEmptyLine();
                },
                OnSelectItem: function (item) {
                    var currentItem = this.GetItem();
                    if (item) {
                        Purchasing.PRLineItems.fromUserChange = true;
                        currentItem.SetValue("VendorName__", item.GetValue("Name__"));
                        currentItem.SetValue("VendorNumber__", item.GetValue("Number__"));
                        Variable.SetValueAsString("lastExtractedVendorName", null);
                        Variable.SetValueAsString("lastExtractedVendorNumber", null);
                        Purchasing.PRLineItems.LeaveEmptyLine();
                    }
                    else {
                        currentItem.SetValue("VendorName__", "");
                        currentItem.SetError("VendorName__", "The value could not be resolve");
                    }
                },
                UpdateVendorName: function (item) {
                    var options = {
                        table: "AP - Vendors__",
                        filter: "&(Number__=" + item.GetValue("VendorNumber__") + ")(|(CompanyCode__=" + Data.GetValue("CompanyCode__") + ")(CompanyCode__=)(CompanyCode__!=*))",
                        attributes: ["Name__"],
                        sortOrder: null,
                        maxRecords: 1
                    };
                    Sys.GenericAPI.PromisedQuery(options)
                        .Then(function (queryResult) {
                        item.SetValue("VendorName__", queryResult.length == 1 ? queryResult[0].Name__ : null);
                    })
                        .Catch(function ( /*error: string*/) {
                        item.SetValue("VendorName__", null);
                    });
                },
                UpdateVendorNumber: function (item) {
                    return Sys.Helpers.Promise.Create(function (resolve) {
                        var vendorName = item.GetValue("VendorName__");
                        if (vendorName !== null) {
                            var options = {
                                table: "AP - Vendors__",
                                filter: "&(Name__=" + vendorName + ")(|(CompanyCode__=" + Data.GetValue("CompanyCode__") + ")(CompanyCode__=)(CompanyCode__!=*))",
                                attributes: ["Number__"],
                                sortOrder: null,
                                maxRecords: 2
                            };
                            Sys.GenericAPI.PromisedQuery(options)
                                .Then(function (queryResult) {
                                var vendorNumber = null;
                                if (queryResult && queryResult.length != 0) {
                                    if (queryResult.length == 1) {
                                        vendorNumber = queryResult[0].Number__;
                                    }
                                    else {
                                        // Vendor name ambiguous: multiple vendors have the same name
                                        item.SetError("VendorName__", "The value could not be resolve");
                                        item.SetError("VendorNumber__", "The value could not be resolve");
                                    }
                                }
                                else {
                                    item.SetError("VendorName__", "No vendor found");
                                    item.SetError("VendorNumber__", "No vendor found");
                                }
                                item.SetValue("VendorNumber__", vendorNumber);
                            })
                                .Catch(function ( /*error: string*/) {
                                item.SetValue("VendorNumber__", null);
                            })
                                .Finally(function () {
                                resolve(item);
                            });
                        }
                        else {
                            resolve(item);
                        }
                    });
                }
            },
            SupplyType: {
                FillInducedFields: function (item, forceDefaultGLAccount, forceDefaultCostType) {
                    var supplyTypeID = item.GetValue("SupplyTypeID__");
                    if (supplyTypeID) {
                        Lib.Purchasing.CatalogHelper.SupplyTypesManager.Get(supplyTypeID)
                            .Then(function (supplyType) {
                            if (supplyType) {
                                item.SetValue("SupplyTypeName__", supplyType.name);
                                item.SetValue("NoGoodsReceipt__", supplyType.noGoodsReceipt);
                                if (forceDefaultGLAccount !== false || Sys.Helpers.IsEmpty(item.GetValue("ItemGLAccount__"))) {
                                    item.SetValue("ItemGLAccount__", supplyType.defaultGLAccount);
                                    item.SetValue("ItemGroup__", supplyType.defaultGLAccountGroup);
                                    Purchasing.PRLineItems.ExpenseCategory.Update(item);
                                    Purchasing.PRLineItems.Budget.Fill({ resetBudgetID: true }, item);
                                }
                                // Fill Cost type from item category if not filled from catalog
                                if (forceDefaultCostType !== false || Sys.Helpers.IsEmpty(item.GetValue("CostType__"))) {
                                    item.SetValue("CostType__", supplyType.defaultCostType);
                                    Purchasing.PRLineItems.Budget.Fill({ resetBudgetID: true }, item);
                                    Lib.Purchasing.LayoutPR.DelayRebuildWorkflow();
                                }
                                if (!forceDefaultCostType && Sys.Helpers.IsEmpty(item.GetValue("CostType__"))) {
                                    Lib.P2P.fillCostTypeFromGLAccount(item, "ItemGLAccount__");
                                }
                                if (supplyType != null) {
                                    item.SetValue("SupplyTypeFullpath__", supplyType.GetPath());
                                }
                            }
                        });
                    }
                },
                FillBuyerAndRecipient: function (item, itemIndex) {
                    var companyCode = Data.GetValue("CompanyCode__");
                    var supplyTypeId = item.GetValue("SupplyTypeID__");
                    Log.Info("FillBuyerAndRecipient: Supply type with ID:'" + supplyTypeId + "' & Company Code ID: '" + companyCode + "' & index: " + itemIndex + " lookup.");
                    if (!Sys.Helpers.IsEmpty(supplyTypeId) && !Sys.Helpers.IsEmpty(companyCode)) {
                        Lib.Purchasing.CatalogHelper.SupplyTypesManager.Get(supplyTypeId)
                            .Then(function (supplyType) {
                            if (supplyType) {
                                Log.Info("Updating buyer and recipient");
                                Purchasing.PRLineItems.Buyer.Update(item, supplyType.buyerLogin, itemIndex);
                                Purchasing.PRLineItems.Recipient.Update(item, supplyType.recipientLogin);
                            }
                            else {
                                item.SetError("SupplyTypeName__", "Supply type '" + supplyTypeId + "' not found");
                                // Reset Recipient login and name
                                item.SetValue("RecipientLogin__", "");
                                item.SetValue("RecipientName__", "");
                                // Reset buyer login and name
                                item.SetValue("BuyerLogin__", "");
                                item.SetValue("BuyerName__", "");
                            }
                        });
                    }
                    else {
                        // Reset Recipient login and name
                        item.SetValue("RecipientLogin__", "");
                        item.SetValue("RecipientName__", "");
                        // Reset buyer login and name
                        item.SetValue("BuyerLogin__", "");
                        item.SetValue("BuyerName__", "");
                    }
                },
                OnSelectItem: function (item) {
                    var currentItem = this.GetItem();
                    if (item) {
                        var itemIndex = this.GetRow().GetLineNumber() - 1;
                        Purchasing.PRLineItems.fromUserChange = true;
                        var id = item.GetValue("SupplyID__");
                        currentItem.SetValue("SupplyTypeID__", id);
                        Purchasing.PRLineItems.SupplyType.FillInducedFields(currentItem);
                        Purchasing.PRLineItems.SupplyType.FillBuyerAndRecipient(currentItem, itemIndex);
                        Purchasing.PRLineItems.LeaveEmptyLine();
                    }
                    else {
                        currentItem.SetValue("SupplyTypeName__", "");
                        currentItem.SetError("SupplyTypeName__", "The value could not be resolve");
                    }
                },
                OnUnknownOrEmptyValue: function () {
                    this.GetRow().SupplyTypeID__.SetValue("");
                },
                UpdateSupplyTypeID: function (item /*, line: number*/) {
                    return Sys.Helpers.Promise.Create(function (resolve) {
                        var supplyTypeName = item.GetValue("SupplyTypeName__");
                        if (supplyTypeName !== null) {
                            var options = {
                                table: "PurchasingSupply__",
                                filter: "&(Name__=" + supplyTypeName + ")(|(CompanyCode__=" + Data.GetValue("CompanyCode__") + ")(CompanyCode__=)(CompanyCode__!=*))",
                                attributes: ["SupplyID__", "DefaultCostType__"],
                                sortOrder: null,
                                maxRecords: 2
                            };
                            Sys.GenericAPI.PromisedQuery(options)
                                .Then(function (queryResult) {
                                var supplyTypeID = null;
                                var defaultCostType = null;
                                if (queryResult && queryResult.length != 0) {
                                    if (queryResult.length == 1) {
                                        supplyTypeID = queryResult[0].SupplyID__;
                                        defaultCostType = queryResult[0].DefaultCostType__;
                                    }
                                    else {
                                        // Supply name ambiguous: multiple vendors have the same name
                                        item.SetError("SupplyTypeName__", "The value could not be resolve");
                                        item.SetError("SupplyTypeID__", "The value could not be resolve");
                                    }
                                }
                                else {
                                    item.SetError("SupplyTypeName__", "No supply type found");
                                    item.SetError("SupplyTypeID__", "No supply type found");
                                }
                                item.SetValue("SupplyTypeID__", supplyTypeID);
                                if (!item.GetValue("CostType__")) {
                                    item.SetValue("CostType__", defaultCostType);
                                    Purchasing.PRLineItems.Budget.Fill({ resetBudgetID: true }, item);
                                    Lib.Purchasing.LayoutPR.DelayRebuildWorkflow();
                                }
                            })
                                .Catch(function ( /*error: string*/) {
                                item.SetValue("SupplyTypeID__", null);
                            })
                                .Finally(function () {
                                resolve(item);
                            });
                        }
                        else {
                            resolve(item);
                        }
                    });
                }
            },
            TaxCode: {
                OnSelectItem: function (item) {
                    var currentItem = this.GetItem();
                    if (item) {
                        currentItem.SetValue("ItemTaxCode__", item.GetValue("TaxCode__"));
                        currentItem.SetValue("ItemTaxRate__", item.GetValue("TaxRate__"));
                    }
                    else {
                        currentItem.SetValue("ItemTaxCode__", "");
                        currentItem.SetError("ItemTaxCode__", "The value could not be resolve");
                        currentItem.SetValue("ItemTaxRate__", null);
                    }
                    Lib.Purchasing.LayoutPR.DelayRebuildWorkflow();
                },
                OnUnknownOrEmptyValue: function () {
                    var currentItem = this.GetItem();
                    currentItem.SetValue("ItemTaxRate__", null);
                    Lib.Purchasing.LayoutPR.DelayRebuildWorkflow();
                },
                UpdateTaxRate: function (item) {
                    var options = {
                        table: "AP - Tax codes__",
                        filter: "&(TaxCode__=" + item.GetValue("ItemTaxCode__") + ")(|(CompanyCode__=" + Data.GetValue("CompanyCode__") + ")(CompanyCode__=)(CompanyCode__!=*))",
                        attributes: ["TaxRate__"],
                        sortOrder: null,
                        maxRecords: 1
                    };
                    Sys.GenericAPI.PromisedQuery(options)
                        .Then(function (queryResult) {
                        item.SetValue("ItemTaxRate__", queryResult.length == 1 ? queryResult[0].TaxRate__ : null);
                    })
                        .Catch(function ( /*error: string*/) {
                        item.SetValue("ItemTaxRate__", null);
                    });
                }
            },
            Buyer: {
                timerDelayCheckBuyers: 0,
                CheckBuyers: function () {
                    Log.Info("Checking buyers");
                    var lineItems = Data.GetTable("LineItems__");
                    var count = lineItems.GetItemCount();
                    for (var i = 0; i < count; i++) // include 1st row too to reset error or add empty buyer error
                     {
                        var item = lineItems.GetItem(i);
                        if (Lib.Purchasing.IsLineItemEmpty(item)) {
                            // Ignore empty lines
                            continue;
                        }
                        var buyerLogin = item.GetValue("BuyerLogin__");
                        if (Sys.Helpers.IsEmpty(buyerLogin)) {
                            // Set error if empty and no error already present
                            if (Sys.Helpers.IsEmpty(item.GetError("BuyerName__"))) {
                                item.SetError("BuyerName__", Language.Translate("_Buyer {0} not found for this supply type", false, ""));
                            }
                        }
                        else {
                            // Reset error
                            item.SetError("BuyerName__", "");
                        }
                    }
                },
                DelayCheckBuyers: function () {
                    if (Purchasing.PRLineItems.Buyer.timerDelayCheckBuyers !== 0) {
                        // cancel previous call
                        Log.Info("[DelayCheckBuyers] start and cancel previous");
                        clearTimeout(Purchasing.PRLineItems.Buyer.timerDelayCheckBuyers);
                    }
                    // Delay CheckBuyers
                    Purchasing.PRLineItems.Buyer.timerDelayCheckBuyers = setTimeout(function () {
                        Log.Info("[DelayCheckBuyers] trigger");
                        Purchasing.PRLineItems.Buyer.timerDelayCheckBuyers = 0;
                        Purchasing.PRLineItems.Buyer.CheckBuyers();
                    }, 200);
                },
                Update: function (item, buyerLogin, itemIndex) {
                    if (typeof itemIndex == "undefined") {
                        itemIndex = -1;
                    }
                    Log.Info("Buyer.Update start");
                    if (Sys.Helpers.IsEmpty(buyerLogin)) {
                        buyerLogin = Data.GetValue("RequisitionInitiator__");
                    }
                    Sys.OnDemand.Users.CacheByLogin.Get(buyerLogin, Lib.P2P.attributesForUserCache)
                        .Then(function (result) {
                        Log.Info("Buyer.Update: found " + buyerLogin);
                        var buyer = result[buyerLogin];
                        if (buyer.$error) {
                            Log.Error("UpdateBuyer: error on get properties: " + buyer.$error);
                            // Reset buyer login and name
                            item.SetValue("BuyerLogin__", "");
                            item.SetValue("BuyerName__", "");
                            item.SetError("BuyerName__", Language.Translate("_Buyer {0} not found for this supply type", false, buyerLogin));
                            return;
                        }
                        item.SetValue("BuyerLogin__", buyer.login);
                        item.SetValue("BuyerName__", buyer.displayname);
                        if (itemIndex == 0) {
                            Lib.Purchasing.LayoutPR.DelayRebuildWorkflow();
                        }
                        Purchasing.PRLineItems.Buyer.DelayCheckBuyers();
                    });
                },
                OnRefreshRow: function (index) {
                    var row = Controls.LineItems__.GetRow(index);
                    var item = row.GetItem();
                    if (item && Lib.Purchasing.IsLineItemEmpty(item)) {
                        row.BuyerName__.SetError("");
                    }
                }
            },
            Recipient: {
                Update: function (item, recipientLogin) {
                    if (Sys.Helpers.IsEmpty(recipientLogin)) {
                        recipientLogin = Data.GetValue("RequisitionInitiator__");
                    }
                    Log.Info("'Recipient '" + recipientLogin + "' lookup.");
                    if (recipientLogin) {
                        Sys.OnDemand.Users.CacheByLogin.Get(recipientLogin, Lib.P2P.attributesForUserCache)
                            .Then(function (result) {
                            var recipient = result[recipientLogin];
                            if (recipient.$error) {
                                Log.Error("UpdateRecipient: error on get properties: " + recipient.$error);
                                // Reset Recipient login and name
                                item.SetValue("RecipientLogin__", "");
                                item.SetValue("RecipientName__", "");
                                item.SetError("RecipientName__", Language.Translate("_Recipient {0} not found for this supply type", false, recipientLogin));
                                return;
                            }
                            item.SetValue("RecipientLogin__", recipient.login);
                            item.SetValue("RecipientName__", recipient.displayname);
                            item.SetError("RecipientName__", "");
                        });
                    }
                    else {
                        // Reset Recipient login and name
                        item.SetValue("RecipientLogin__", "");
                        item.SetValue("RecipientName__", "");
                        item.SetError("RecipientName__", Language.Translate("_Recipient {0} not found for this supply type", false, recipientLogin));
                    }
                },
                OnRefreshRow: function (index) {
                    var row = Controls.LineItems__.GetRow(index);
                    if (row.SupplyTypeID__.GetValue() && Lib.Purchasing.LayoutPR.IsRequesterStep()) {
                        Lib.Purchasing.CatalogHelper.SupplyTypesManager.Get(row.SupplyTypeID__.GetValue())
                            .Then(function (supplyType) {
                            var browsable = supplyType && supplyType.isRecipientBrowsable;
                            row.RecipientName__.SetReadOnly(!browsable);
                            // Set error if empty and no error already present
                            if (Sys.Helpers.IsEmpty(row.RecipientName__.GetValue()) && Sys.Helpers.IsEmpty(row.RecipientName__.GetError())) {
                                row.RecipientName__.SetError(Language.Translate("_Recipient {0} not found for this supply type", false, ""));
                            }
                        });
                    }
                }
            },
            ItemUnitDescription: {
                OnSelectItem: function (item) {
                    var currentItem = this.GetItem();
                    if (item) {
                        Purchasing.PRLineItems.fromUserChange = true;
                        currentItem.SetValue("ItemUnit__", item.GetValue("UnitOfMeasure__"));
                        AutoFillLineItemsWithEmptyValue("ItemUnit__", item.GetValue("UnitOfMeasure__"), {
                            extraSetter: function (emptyItem) { return emptyItem.SetValue("ItemUnitDescription__", item.GetValue("Description__")); }
                        });
                        Purchasing.PRLineItems.LeaveEmptyLine();
                    }
                    else {
                        currentItem.SetValue("UnitOfMeasure__", "");
                        currentItem.SetError("UnitOfMeasure__", "The value could not be resolve");
                    }
                },
                OnChange: function () {
                    var currentItem = this.GetItem();
                    AutoFillLineItemsWithEmptyValue("ItemUnit__", currentItem.GetValue("ItemUnit__"), {
                        extraSetter: function (emptyItem) { return emptyItem.SetValue("ItemUnitDescription__", currentItem.GetValue("ItemUnitDescription__")); }
                    });
                }
            },
            ItemUnit: {
                OnSelectItem: function (item) {
                    var currentItem = this.GetItem();
                    if (item) {
                        Purchasing.PRLineItems.fromUserChange = true;
                        currentItem.SetValue("ItemUnitDescription__", item.GetValue("Description__"));
                        AutoFillLineItemsWithEmptyValue("ItemUnit__", item.GetValue("ItemUnit__"), {
                            extraSetter: function (emptyItem) { return emptyItem.SetValue("ItemUnitDescription__", item.GetValue("Description__")); }
                        });
                        Purchasing.PRLineItems.LeaveEmptyLine();
                    }
                    else {
                        currentItem.SetValue("ItemUnit__", "");
                        currentItem.SetError("ItemUnit__", "The value could not be resolve");
                    }
                },
                OnChange: function () {
                    var currentItem = this.GetItem();
                    AutoFillLineItemsWithEmptyValue("ItemUnit__", currentItem.GetValue("ItemUnit__"), {
                        extraSetter: function (emptyItem) { return emptyItem.SetValue("ItemUnitDescription__", currentItem.GetValue("ItemUnitDescription__")); }
                    });
                }
            },
            ItemType: {
                /** @this ComboBox & IControlInTable<Purchase_Requisition_process_V2LineItemsTableRow> */
                OnChange: function () {
                    Lib.Purchasing.Items.UpdateItemTypeDependencies(Purchasing.PRLineItems.ItemTypeDependenciesVisibilities);
                    var index = this.GetRow().GetLineNumber(true) - 1;
                    var item = this.GetItem();
                    var quantity = item.GetValue("ItemQuantity__");
                    if (!Lib.Purchasing.Items.IsAmountBasedItem(item) && item.GetValue("ItemUnitPrice__") === 1 && quantity > 1) {
                        item.SetValue("ItemUnitPrice__", quantity);
                        item.SetValue("ItemQuantity__", 1);
                    }
                    Purchasing.PRLineItems.ComputeItemAmount(index);
                    Purchasing.PRLineItems.OnRefreshRow(index);
                    Purchasing.PRLineItems.LeaveEmptyLine();
                    Purchasing.PRLineItems.SetLastLineItemType(item, index);
                }
            },
            CostType: {
                OnChange: function () {
                    Purchasing.PRLineItems.fromUserChange = true;
                    var currentItem = this.GetItem();
                    Purchasing.PRLineItems.Budget.ResetBudgetID(currentItem);
                    // The workflow is recomputed when HasCapex value is updated
                    Purchasing.PRLineItems.UpdateHasCapex();
                    Lib.Purchasing.PRLineItems.LeaveEmptyLine();
                }
            },
            CheckStartEndDate: function (item) {
                Purchasing.PRLineItems.fromUserChange = true;
                var startDate = item.GetValue("ItemStartDate__");
                var endDate = item.GetValue("ItemEndDate__");
                if (startDate && endDate) {
                    var msg = startDate > endDate ? "_The start date cannot be later than the end date" : "";
                    item.SetError("ItemStartDate__", msg);
                    item.SetError("ItemEndDate__", msg);
                }
                else {
                    if (startDate) {
                        item.SetError("ItemStartDate__", "");
                    }
                    if (endDate) {
                        item.SetError("ItemEndDate__", "");
                    }
                }
                Purchasing.PRLineItems.LeaveEmptyLine();
            },
            StartDate: {
                OnChange: function () {
                    var item = this.GetItem();
                    var startDate = item.GetValue("ItemStartDate__");
                    item.SetValue("ItemRequestedDeliveryDate__", startDate);
                    item.SetError("ItemRequestedDeliveryDate__", "");
                    item.SetWarning("ItemRequestedDeliveryDate__", "");
                    Purchasing.PRLineItems.CheckStartEndDate(item);
                    if (!item.GetError("ItemStartDate__")) {
                        AutoFillLineItemsWithEmptyValue("ItemStartDate__", startDate, {
                            extraCondition: function (emptyItem) { return Lib.Purchasing.Items.IsServiceBasedItem(emptyItem); },
                            extraSetter: function (emptyItem) {
                                emptyItem.SetValue("ItemRequestedDeliveryDate__", startDate);
                                emptyItem.SetError("ItemRequestedDeliveryDate__", "");
                                emptyItem.SetWarning("ItemRequestedDeliveryDate__", "");
                                Purchasing.PRLineItems.CheckStartEndDate(emptyItem);
                            }
                        });
                    }
                    Purchasing.PRLineItems.RequestedDeliveryDate.OnChange.call(this);
                }
            },
            EndDate: {
                OnChange: function () {
                    var item = this.GetItem();
                    Purchasing.PRLineItems.CheckStartEndDate(item);
                    if (!item.GetError("ItemEndDate__")) {
                        AutoFillLineItemsWithEmptyValue("ItemEndDate__", item.GetValue("ItemEndDate__"), {
                            extraCondition: function (emptyItem) { return Lib.Purchasing.Items.IsServiceBasedItem(emptyItem); }
                        });
                    }
                }
            }
        };
    })(Purchasing = Lib.Purchasing || (Lib.Purchasing = {}));
})(Lib || (Lib = {}));
