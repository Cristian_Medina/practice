/* LIB_DEFINITION{
  "name": "LIB_DD_CUSTOMIZATION_COMMON",
  "libraryType": "LIB",
  "scriptType": "COMMON",
  "comment": "PS configurations that can be used in all scripts",
  "require": [
    "[Lib_DD_Common_V12.0.425.0]"
  ]
}*/

/**
 * Package DD common user exits
 * @namespace Lib.DD.Customization.Common
 */

///#GLOBALS Lib
var Lib = Lib || {};
Lib.DD = Lib.DD || {};
Lib.DD.Customization = Lib.DD.Customization || {};

Lib.DD.Customization.Common = (function()
{
	/**
	 * @lends Lib.DD.Customization.Common
	 */
	var Common =
	{
		/**
		 * This function is called to set unique client ID for AI model. By default it is main account ID.
		 * This function can be used to separate AI model for DEV/QA/PROD environment in the same account.
		 * Be aware to only used alphanumeric character for unique ID.
		 * Main account ID is concatenate to the client ID returned by this function
		 * @return {string} Unique client ID
		 * @example
		 * <pre><code>
		 *	SetClientId: function () {
		 *		return "dev";
		 * }
		 * </code></pre>
		 */
		SetClientId: function ()
		{
			return "";
		}
	};
	return Common;
})();
