///#GLOBALS Lib Sys
/* LIB_DEFINITION{
  "name": "Lib_Purchasing_CM_Workflow",
  "libraryType": "LIB",
  "scriptType": "COMMON",
  "comment": "Purchasing Catalog common library used to manage document workflow",
  "require": [
    "Lib_Purchasing_V12.0.425.0",
    "Sys/Sys_Helpers",
    "Sys/Sys_OnDemand_Users",
    "Sys/Sys_Helpers_Controls",
    "Sys/Sys_WorkflowController"
  ]
}*/
var Lib;
(function (Lib) {
    var Purchasing;
    (function (Purchasing) {
        var CM;
        (function (CM) {
            var Workflow;
            (function (Workflow) {
                Workflow.controller = Sys.WorkflowController.Create({ version: 1 });
                Workflow.parameters = {
                    actions: {
                        submit: {
                            image: "Catalog_submit.png"
                        },
                        submitted: {
                            image: "Catalog_submitted.png"
                        },
                        approve: {
                            image: "Catalog_approve_or_reject.png"
                        },
                        approved: {
                            image: "Catalog_approved.png"
                        },
                        reject: {},
                        rejected: {
                            image: "Catalog_rejected.png"
                        }
                    },
                    roles: {
                        requester: {},
                        approver: {}
                    },
                    mappingTable: {
                        tableName: "WorkflowTable__",
                        columns: {
                            WRKFUserName__: {
                                data: "name"
                            },
                            WRKFRole__: {
                                data: "role",
                                translate: true
                            },
                            WRKFDate__: {
                                data: "date"
                            },
                            WRKFComment__: {
                                data: "comment"
                            },
                            WRKFAction__: {
                                data: "action"
                            },
                            WRKFIsGroup__: {
                                data: "isGroup"
                            }
                        }
                    },
                    delayedData: {
                        isGroup: {
                            type: "isGroupInfo",
                            key: "login"
                        }
                    },
                    callbacks: {
                        OnError: function (msg) {
                            Log.Info(msg);
                        }
                    }
                };
                // public interface
                Workflow.enums = {
                    roles: Sys.Helpers.Object.ToEnum(Workflow.parameters.roles),
                    actions: Sys.Helpers.Object.ToEnum(Workflow.parameters.actions)
                };
                var requesterLogin = Data.GetValue("RequesterLogin__");
                var requesterName = Data.GetValue("RequesterName__");
                if (Sys.ScriptInfo.IsServer()) {
                    var ownerUser = Lib.Purchasing.GetOwner();
                    requesterLogin = requesterLogin || ownerUser.GetVars().GetValue_String("login", 0);
                    requesterName = requesterName || ownerUser.GetVars().GetValue_String("displayName", 0);
                }
                // const roles = enums.roles;
                // const actions = enums.actions;
                var rolesParameters = {
                    roles: {
                        requester: {
                            // In this sample, the requester "role" has only one user, the form creator.
                            OnBuild: function (callback) {
                                callback([
                                    {
                                        contributorId: "requester" + requesterLogin,
                                        role: "Requester",
                                        login: requesterLogin,
                                        name: requesterName,
                                        action: "submit"
                                    }
                                ]);
                                return true;
                            }
                        },
                        approver: {
                            // Obtains a list of approvers from the workflow rule.
                            OnBuild: function (callback) {
                                // Options for GetStepResults call
                                var getStepsOptions = {
                                    "debug": false,
                                    "fields": {
                                        "values": {
                                            "WorkflowType__": "catalogImportApproval",
                                            "RequisitionInitiator__": requesterLogin,
                                            "CatalogUpdateRequestType__": Lib.Purchasing.CM.Workflow.IsInternalUpdateRequest() ? "Internal" : "External"
                                        }
                                    },
                                    success: function (approvers, ruleApplied) {
                                        Log.Info("Catalog Import Approval Workflow, rule applied: " + ruleApplied);
                                        approvers = Sys.Helpers.Array.Map(approvers, function (approver) {
                                            return approver.login;
                                        });
                                        Sys.OnDemand.Users.GetUsersFromLogins(approvers, ["displayname", "emailaddress"], function (users) {
                                            // Build a contributor object with user information.
                                            var contributors = Sys.Helpers.Array.Map(users, function (user) {
                                                return {
                                                    contributorId: "approver" + user.login,
                                                    role: "Approver",
                                                    login: user.login,
                                                    email: user.exists ? user.emailaddress : user.login,
                                                    name: user.exists ? user.displayname : user.login,
                                                    action: "approve"
                                                };
                                            });
                                            callback(contributors);
                                        });
                                    },
                                    error: function (errorMessage) {
                                        Log.Error("Approval Workflow, error: " + errorMessage);
                                        Variable.SetValueAsString("MissingWorkflowRuleError", "true");
                                        Variable.SetValueAsString("WorkflowErrorMessage", errorMessage);
                                        callback([]);
                                    }
                                };
                                // Get a list of approvers from the workflow rule.
                                Sys.WorkflowEngine.GetStepsResult(getStepsOptions);
                                return true;
                            }
                        }
                    }
                };
                Sys.Helpers.Extend(true, Lib.Purchasing.CM.Workflow.parameters, rolesParameters);
                /**
                 * If the CMW process instance does not have a parent, it's internal (it has not been created by a Vendor)
                 */
                function IsInternalUpdateRequest() {
                    return Variable.GetValueAsString("CatalogManagmentType") === "internal";
                }
                Workflow.IsInternalUpdateRequest = IsInternalUpdateRequest;
            })(Workflow = CM.Workflow || (CM.Workflow = {}));
        })(CM = Purchasing.CM || (Purchasing.CM = {}));
    })(Purchasing = Lib.Purchasing || (Lib.Purchasing = {}));
})(Lib || (Lib = {}));
