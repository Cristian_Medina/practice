/* LIB_DEFINITION
{
    "name": "Lib_VendorRegistration_DocumentsTable",
    "libraryType": "LIB",
    "scriptType": "CLIENT",
    "comment": "VendorRegistration library",
    "require": [ "[Lib_AP_Customization_VendorRegistration]" ]
}
*/
///#GLOBALS Lib Sys
var Lib;
(function (Lib) {
    var VendorRegistration;
    (function (VendorRegistration) {
        var DocumentsTable;
        (function (DocumentsTable) {
            var documentsToAttach = [];
            var serializedDocuments = {
                documents: []
            };
            function Init() {
                documentsToAttach = [];
                Controls.DocumentsTable__.SetAtLeastOneLine(!ProcessInstance.isReadOnly);
                fillDocumentsTable();
                Controls.DocumentsTable__.DocumentFile__.OnFileUploadEnd = onFileUploadEnd;
                Controls.DocumentsTable__.DocumentFile__.OnClickOnFile = onClickOnFile;
                Controls.DocumentsTable__.DocumentFile__.OnDeleteFile = onDeleteFile;
                Controls.DocumentsTable__.DocumentType__.OnChange = onDocumentTypeChange;
            }
            DocumentsTable.Init = Init;
            function getSerializedDocument(docid) {
                for (var i = 0; i < serializedDocuments.documents.length; i++) {
                    var doc = serializedDocuments.documents[i];
                    if (doc.file._docId === docid) {
                        return doc;
                    }
                }
                return null;
            }
            function onDocumentTypeChange() {
                var control = this;
                var documentType = control.GetValue();
                var uploadedFiles = control.GetRow().DocumentFile__.GetUploadedFiles();
                if (uploadedFiles) {
                    uploadedFiles.forEach(function (uploadedFile) {
                        var doc = getSerializedDocument(uploadedFile._docId);
                        if (doc) {
                            doc.type = documentType;
                        }
                    });
                }
            }
            function onFileUploadEnd(success, file) {
                if (success) {
                    var row = this.GetRow();
                    var document = {
                        file: file,
                        type: row.DocumentType__.GetValue(),
                        status: "_Document inprogress",
                        attachmentIndex: -1,
                        endOfValidity: null
                    };
                    serializedDocuments.documents.push(document);
                    documentsToAttach.push(document);
                    if (!row.DocumentStatus__.GetValue()) {
                        // File was uploaded on an empty line (not on a deleted file line), add a new empty line
                        addNewLine();
                    }
                    row.DocumentStatus__.SetValue(Language.Translate(document.status));
                    Controls.DocumentsTable__.Wait(false);
                }
            }
            function onClickOnFile(event) {
                for (var i = 0; i < Attach.GetNbAttach(); i++) {
                    var attach = Attach.GetAttach(i);
                    if (attach.GetId() === event.file.id) {
                        Attach.Open(i);
                    }
                }
            }
            function onDeleteFile(event) {
                if (event.file.id) {
                    for (var i = 0; i < serializedDocuments.documents.length; i++) {
                        var file = serializedDocuments.documents[i].file;
                        if (file._docId === event.file.id) {
                            serializedDocuments.documents.splice(i, 1);
                        }
                    }
                    for (var i = 0; i < documentsToAttach.length; i++) {
                        var file = documentsToAttach[i].file;
                        if (file.GetId() === event.file.id) {
                            documentsToAttach.splice(i, 1);
                        }
                    }
                    var row = this.GetRow();
                    row.DocumentStatus__.SetValue(Language.Translate("_Pending deletion"));
                }
            }
            function fillDocumentsTable() {
                var serializedDocumentsJSON = Variable.GetValueAsString("DocumentsSummaryJSON");
                if (serializedDocumentsJSON) {
                    try {
                        serializedDocuments = JSON.parse(serializedDocumentsJSON);
                        var documentsCount = serializedDocuments.documents.length;
                        Controls.DocumentsTable__.SetItemCount(documentsCount);
                        for (var i = 0; i < documentsCount; i++) {
                            var doc = serializedDocuments.documents[i];
                            var row = Controls.DocumentsTable__.GetRow(i);
                            var attachment = Attach.GetAttach(doc.attachmentIndex);
                            if (attachment) {
                                var attachmentParams = {
                                    name: attachment.GetNiceName(doc.attachmentIndex),
                                    undeletable: ProcessInstance.isReadOnly
                                };
                                row.DocumentFile__.AddFile(attachment, attachmentParams);
                                row.DocumentStatus__.SetValue(Language.Translate(doc.status));
                                row.DocumentType__.SetValue(doc.type);
                                row.EndOfValidity__.SetValue(doc.endOfValidity);
                            }
                        }
                        addNewLine();
                    }
                    catch (ex) {
                        Log.Warn("JSON parsing exception - Unable to parse DocumentsSummary - " + ex);
                    }
                }
            }
            function addNewLine() {
                // Add a new line if we did not reach the table max lines count
                if (Data.GetTable("DocumentsTable__").GetItemCount() < (Controls.DocumentsTable__.GetLineCount() - 1) && !ProcessInstance.isReadOnly) {
                    Controls.DocumentsTable__.AddItem(false);
                }
            }
            function AttachDocumentsToRecord() {
                Controls.DocumentsTable__.Wait(true);
                var attachmentIndex = Attach.GetNbAttach();
                for (var i = 0; i < documentsToAttach.length; i++) {
                    var document = documentsToAttach[i];
                    var serializedDoc = getSerializedDocument(document.file._docId);
                    serializedDoc.attachmentIndex = attachmentIndex;
                    serializedDoc.status = "_Document uploaded";
                    serializedDoc.endOfValidity = document.endOfValidity;
                    Attach.AddAttach(document.file);
                    attachmentIndex++;
                }
                var documentsCount = serializedDocuments.documents.length;
                for (var i = 0; i < documentsCount; i++) {
                    var doc = serializedDocuments.documents[i];
                    var row = Controls.DocumentsTable__.GetRow(i);
                    doc.endOfValidity = row.EndOfValidity__.GetValue() || null;
                }
                Controls.DocumentsTable__.Wait(false);
                Variable.SetValueAsString("DocumentsSummaryJSON", JSON.stringify(serializedDocuments));
            }
            DocumentsTable.AttachDocumentsToRecord = AttachDocumentsToRecord;
            function UpdateDocumentTypeAvailableValues() {
                var docTypeValues = Sys.Helpers.TryCallFunction("Lib.AP.Customization.VendorRegistration.DocumentTypeAvailableValues");
                if (!docTypeValues || docTypeValues.length === 0) {
                    var country = Data.GetValue("Country__");
                    var docTypeDefaultValues = ["KBIS=_KBIS", "RIB=_RIB", "Other=_Other"];
                    var docTypeUSValues = ["RIB=_RIB", "W-9 form=_W9Form", "Other=_Other"];
                    var docTypeFRValues = ["KBIS=_KBIS", "RIB=_RIB", "Certificate of fiscal regularity=_CertificateOfFiscalRegularity", "Other=_Other"];
                    switch (country) {
                        case "US":
                            docTypeValues = docTypeUSValues;
                            break;
                        case "FR":
                            docTypeValues = docTypeFRValues;
                            break;
                        default:
                            docTypeValues = docTypeDefaultValues;
                    }
                }
                Controls.DocumentsTable__.GetColumnControl(1).SetAvailableValues(docTypeValues);
                for (var i = 0; i < Data.GetTable("DocumentsTable__").GetItemCount(); i++) {
                    Controls.DocumentsTable__.GetRow(i).DocumentType__.SetAvailableValues(docTypeValues);
                }
            }
            DocumentsTable.UpdateDocumentTypeAvailableValues = UpdateDocumentTypeAvailableValues;
        })(DocumentsTable = VendorRegistration.DocumentsTable || (VendorRegistration.DocumentsTable = {}));
    })(VendorRegistration = Lib.VendorRegistration || (Lib.VendorRegistration = {}));
})(Lib || (Lib = {}));
