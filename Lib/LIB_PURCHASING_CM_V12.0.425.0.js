///#GLOBALS Lib Sys
/* LIB_DEFINITION
{
    "name": "Lib_Purchasing_CM",
    "libraryType": "LIB",
    "scriptType": "COMMON",
    "comment": "Catalog Management library",
    "require": ["Sys/Sys_Helpers"]
}
*/
var Lib;
(function (Lib) {
    var Purchasing;
    (function (Purchasing) {
        var CM;
        (function (CM) {
            CM.extractedDataVariable = "ExtractedData";
            var extractedDataString = Variable.GetValueAsString(CM.extractedDataVariable);
            var extractedData = Sys.Helpers.IsEmpty(extractedDataString) ? {} : JSON.parse(extractedDataString);
            CM.EXTRACTED_DATA_TO_CATALOG_MAPPING = {
                Name: "ItemDescription__",
                Description: "ItemLongDescription__",
                SupplierPartID: "ItemNumber__",
                SupplierAuxPartID: "ItemSupplierPartAuxID__",
                ManufacturerName: "ManufacturerName__",
                ManufacturerID: "ManufacturerPartID__",
                UnitPrice: "ItemUnitPrice__",
                Currency: "ItemCurrency__",
                LeadTime: "LeadTime__",
                UOM: "UnitOfMeasure__",
                UNSPSC: "UNSPSC__",
                StartDate: "ValidityDate__",
                EndDate: "ExpirationDate__",
                CompanyCode: "ItemCompanyCode__",
                VendorNumber: "VendorNumber__",
                CostType: "CostType__",
                img: "Image__"
            };
            CM.EXTERACTED_DATA_TO_CSV_MAPPING = {
                Name: "Name",
                Description: "Description",
                SupplierPartID: "Supplier part ID",
                SupplierAuxPartID: "Supplier auxiliary part ID",
                ManufacturerName: "Manufacturer name",
                ManufacturerID: "Manufacturer part ID",
                UnitPrice: "Unit price",
                Currency: "Currency",
                LeadTime: "Lead time",
                UOM: "Unit of measurement",
                CostType: "Cost type",
                UNSPSC: "UNSPSC",
                StartDate: "Start date",
                EndDate: "End date",
                CompanyCode: "CompanyCode",
                VendorNumber: "VendorNumber",
                img: "Image",
                deleted: "Delete"
            };
            // Inverse the mapping
            CM.CSV_TO_EXTERACTED_DATA_MAPPING = {};
            for (var key in CM.EXTERACTED_DATA_TO_CSV_MAPPING) {
                if (Object.prototype.hasOwnProperty.call(CM.EXTERACTED_DATA_TO_CSV_MAPPING, key)) {
                    CM.CSV_TO_EXTERACTED_DATA_MAPPING[CM.EXTERACTED_DATA_TO_CSV_MAPPING[key]] = key;
                }
            }
            function SetExternalData(newExtractedData) {
                extractedDataString = newExtractedData ? JSON.stringify(newExtractedData) : null;
                Variable.SetValueAsString(CM.extractedDataVariable, extractedDataString);
                extractedData = newExtractedData || {
                    Lines: {},
                    ParsingError: [],
                    ProcessingError: []
                };
            }
            CM.SetExternalData = SetExternalData;
            function GetExternalData() {
                return extractedData;
            }
            CM.GetExternalData = GetExternalData;
        })(CM = Purchasing.CM || (Purchasing.CM = {}));
    })(Purchasing = Lib.Purchasing || (Lib.Purchasing = {}));
})(Lib || (Lib = {}));
