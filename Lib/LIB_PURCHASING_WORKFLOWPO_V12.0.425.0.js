///#GLOBALS Lib Sys
/* LIB_DEFINITION{
  "name": "Lib_Purchasing_WorkflowPO",
  "libraryType": "LIB",
  "scriptType": "COMMON",
  "comment": "Purchasing library",
  "require": [
    "Lib_Purchasing_V12.0.425.0",
    "Sys/Sys_Helpers"
  ]
}*/
var Lib;
(function (Lib) {
    var Purchasing;
    (function (Purchasing) {
        var WorkflowPO;
        (function (WorkflowPO) {
            // STD Globals Object
            var g = Sys.Helpers.Globals;
            function ResolveLogin(login) {
                var referenceValue = typeof g.User !== "undefined" ? g.User.loginId : Lib.P2P.GetOwner().GetValue("login");
                var firstDot = referenceValue.indexOf(".");
                var firstAt = referenceValue.indexOf("@");
                if (firstDot < firstAt) {
                    referenceValue = referenceValue.substr(firstDot + 1);
                }
                return login.replace("%[reference:login:demosubstring]", referenceValue);
            }
            function OnBuildBuyer(callback, role, action) {
                Log.Info("OnBuildBuyer(callback, role, action)");
                if (!Sys.Helpers.IsEmpty(g.Data.GetValue("BuyerLogin__"))) {
                    callback([{
                            contributorId: g.Data.GetValue("BuyerLogin__") + role,
                            role: role,
                            login: g.Data.GetValue("BuyerLogin__"),
                            email: g.Data.GetValue("BuyerLogin__"),
                            name: g.Data.GetValue("BuyerName__"),
                            action: action
                        }]);
                    return true;
                }
            }
            function OnBuildTreasurer(callback, role, action) {
                Log.Info("OnBuildTreasurer(callback, role, action)");
                var Login = ResolveLogin(Sys.Parameters.GetInstance("PAC").GetParameter("TreasurerLogin"));
                Log.Info("'Login '" + Login + "' lookup.");
                var callbackTreasurerInfo = function (result, error) {
                    if (error || result.length === 0) {
                        callback([]);
                        return;
                    }
                    var findInArray = function (array, callback) {
                        for (var idx in result) {
                            if (callback(result[idx])) {
                                return result[idx];
                            }
                        }
                        return null;
                    };
                    var element = findInArray(result, function (e) { return e.login === Login; });
                    if (!element) {
                        callback([]);
                        return;
                    }
                    callback([{
                            //mandatory fields
                            contributorId: element.login + role,
                            role: role,
                            //not mandatory fields
                            login: element.login,
                            email: element.emailaddress,
                            name: element.displayname,
                            action: action
                        }]);
                };
                Sys.GenericAPI.Query("ODUSER", "login=" + Login, ["login", "displayname", "emailaddress"], callbackTreasurerInfo, "login", 2);
                return true;
            }
            function UpdateRolesSequence(downPaymentAsked, workflow) {
                if (downPaymentAsked) {
                    workflow.SetRolesSequence(["buyer", "treasurer"]);
                }
                else {
                    workflow.SetRolesSequence(["buyer"]);
                }
            }
            WorkflowPO.UpdateRolesSequence = UpdateRolesSequence;
            /**
                * @exports WorkflowPO
                * @memberof Lib.Purchasing
                */
            WorkflowPO.Parameters = {
                actions: {
                    doReceipt: {
                        image: "PR_reception_grey.png"
                    },
                    received: {
                        image: "PR_reception_shipto.png"
                    },
                    doDownPayment: {
                        image: "PO_doDownpayment_grey.png"
                    },
                    downPaymentDone: {
                        image: "PO_doDownPayment.png"
                    },
                    doOrder: {
                        image: "PR_order_grey.png"
                    },
                    ordered: {
                        image: "PR_order.png"
                    },
                    canceled: {
                        image: "PR_cancel.png"
                    }
                },
                roles: {
                    buyer: {
                        OnBuild: function (callback) { return OnBuildBuyer(callback, Lib.Purchasing.roleBuyer, this.actions.doOrder.GetName()); }
                    },
                    treasurer: {
                        OnBuild: function (callback) { return OnBuildTreasurer(callback, Lib.Purchasing.roleTreasurer, this.actions.doDownPayment.GetName()); }
                    }
                },
                mappingTable: {
                    tableName: "POWorkflow__",
                    columns: {
                        WRKFUserName__: {
                            data: "name"
                        },
                        WRKFRole__: {
                            data: "role",
                            translate: true
                        },
                        WRKFDate__: {
                            data: "date"
                        },
                        WRKFComment__: {
                            data: "comment"
                        },
                        WRKFAction__: {
                            data: "action"
                        },
                        WRKFIsGroup__: {
                            data: "isGroup"
                        }
                    }
                },
                delayedData: {
                    isGroup: {
                        type: "isGroupInfo",
                        key: "login"
                    }
                },
                callbacks: {
                    OnError: function (msg) {
                        Log.Info(msg);
                    }
                }
            };
        })(WorkflowPO = Purchasing.WorkflowPO || (Purchasing.WorkflowPO = {}));
    })(Purchasing = Lib.Purchasing || (Lib.Purchasing = {}));
})(Lib || (Lib = {}));
