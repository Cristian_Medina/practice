///#GLOBALS Lib Sys
/* LIB_DEFINITION{
  "name": "Lib_Purchasing_POValidation",
  "libraryType": "LIB",
  "scriptType": "SERVER",
  "comment": "Purchasing library managing PO items",
  "require": [
    "Sys/Sys_Helpers_Array",
    "Sys/Sys_Helpers_Data",
    "Sys/Sys_Helpers_String",
    "Sys/Sys_Helpers_Database",
    "Sys/Sys_Parameters",
    "Sys/Sys_Decimal",
    "Lib_ERP_V12.0.425.0",
    "Lib_Purchasing_Items_V12.0.425.0",
    "Lib_Purchasing_POItems_V12.0.425.0",
    "Lib_CommonDialog_V12.0.425.0",
    "[Lib_Purchasing_Demo_V12.0.425.0]",
    "[Lib_PO_Customization_Server]"
  ]
}*/
var Lib;
(function (Lib) {
    var Purchasing;
    (function (Purchasing) {
        var POValidation;
        (function (POValidation) {
            /**
             * Call when the Purchase order is saved.
             */
            function OnSave() {
                // more to come...
            }
            POValidation.OnSave = OnSave;
            /**
            * Create a next alert to advise user when we try to execute an unknown action.
            * Process will be in validation state and wait for an action of user.
            * @param {string} currentAction name of the executed action
            * @param {string} currentName sub-name of the executed action
            */
            function OnUnknownAction(currentAction, currentName) {
                var knownAction = Sys.Helpers.TryCallFunction("Lib.PO.Customization.Server.OnUnknownAction", currentAction, currentName);
                if (knownAction !== true) {
                    Lib.Purchasing.OnUnknownAction(currentAction, currentName);
                }
            }
            POValidation.OnUnknownAction = OnUnknownAction;
            /**
             * Call to check if a line in LineItems__ table as been over ordered.
             * @returns {boolean} true if succeeds, false if over ordered
             */
            function CheckOverOrderedItems() {
                var overOrdered = false;
                Sys.Helpers.Data.ForEachTableItem("LineItems__", function (line) {
                    if (Lib.Purchasing.Items.IsQuantityBasedItem(line)) {
                        if (line.GetError("PRNumber__")) {
                            overOrdered = true;
                        }
                    }
                    else if (line.GetError("ItemNetAmount__")) {
                        overOrdered = true;
                    }
                });
                return !overOrdered;
            }
            POValidation.CheckOverOrderedItems = CheckOverOrderedItems;
            /**
             * Call to check if lines in LineItems__ table are still oderable.
             * We just need to check PR items status is still 'To order'
             * @returns {boolean} true if succeeds, false if over ordered
             */
            function CheckOrderableItems() {
                var orderable = true;
                Sys.Helpers.Data.ForEachTableItem("LineItems__", function (line) {
                    if (line.GetError("PRNumber__")) {
                        orderable = false;
                    }
                });
                return orderable;
            }
            POValidation.CheckOrderableItems = CheckOrderableItems;
            /**
             * Attaches PO attachments to the email transport
             * @param {xTransport} email the email transport to attach the files to
             * @returns {void}
             */
            function AddAttachments(email, firstAttachmentOnly) {
                var sendAttachments = Sys.Parameters.GetInstance("PAC").GetParameter("SendPOAttachments") === true;
                var nbAttach = firstAttachmentOnly ? 1 : Attach.GetNbAttach();
                for (var i = 0; i < nbAttach; i++) {
                    var attachFile = Attach.GetConvertedFile(i) || Attach.GetInputFile(i);
                    if (attachFile) {
                        var type = Attach.GetValue(i, "Purchasing_DocumentType");
                        var name = Attach.GetName(i);
                        if (type === "QUOTE") {
                            Log.Info("Attaching quote " + i + " to vendor notif: " + name);
                            Sys.EmailNotification.AddAttach(email, attachFile, name);
                        }
                        else if (type === "PO") {
                            Log.Info("Attaching PO " + i + " to vendor notif: " + name);
                            Sys.EmailNotification.AddAttach(email, attachFile, Lib.P2P.Export.GetName("PO", "OrderNumber__", parseInt(Attach.GetValue(i, "Purchasing_DocumentRevisionVersion"), 10)));
                        }
                        else if (type !== "CSV" && sendAttachments) {
                            Log.Info("Attaching attachment " + i + " to vendor notif: " + name);
                            Sys.EmailNotification.AddAttach(email, attachFile, name);
                        }
                    }
                }
            }
            POValidation.AddAttachments = AddAttachments;
            function SendEmailToRequesters(ItemsIndexArray) {
                var table = Data.GetTable("LineItems__");
                var Requesters = [];
                var ItemsPerRequester = [];
                Sys.Helpers.Array.ForEach(ItemsIndexArray, function (itemIndex) {
                    var currentItem = table.GetItem(itemIndex);
                    var currentRequesterLogin = Sys.Helpers.String.ExtractLoginFromDN(currentItem.GetValue("RequesterDN__"));
                    if (Requesters.indexOf(currentRequesterLogin) === -1) {
                        Requesters.push(currentRequesterLogin);
                        ItemsPerRequester[Requesters.indexOf(currentRequesterLogin)] = [];
                    }
                    ItemsPerRequester[Requesters.indexOf(currentRequesterLogin)].push(currentItem);
                });
                var buyerLogin = Data.GetValue("BuyerLogin__");
                for (var i = 0; i < ItemsPerRequester.length; i++) {
                    if (Requesters[i] != buyerLogin) {
                        var element = ItemsPerRequester[i];
                        var options = BuildEmailToRequester(Requesters[i], element);
                        var doSendNotif = Sys.Helpers.TryCallFunction("Lib.PR.Customization.Server.OnSendEmailNotification", options);
                        if (doSendNotif !== false) {
                            Sys.EmailNotification.SendEmailNotification(options);
                        }
                    }
                }
            }
            POValidation.SendEmailToRequesters = SendEmailToRequesters;
            function BuildEmailToRequester(requester, items) {
                var tempCustomTags = {
                    Items__: "",
                    putLink: false
                };
                var itemsCount = items.length;
                for (var i = 0; i < itemsCount; i++) {
                    var description = String(items[i].GetValue("ItemDescription__"));
                    var htmlRow = "<li>" + description.replace(/\n/g, "<br />") + "</li>";
                    tempCustomTags.Items__ += htmlRow;
                    var buyerLogin = items[i].GetValue("BuyerDN__");
                    if (Users.GetUser(requester).IsMemberOf(buyerLogin)) {
                        tempCustomTags.putLink = true;
                    }
                }
                var options = {
                    userId: requester,
                    subject: "_Purchase order sent",
                    template: "Purchasing_Email_NotifRequester.htm",
                    fromName: "Esker Purchase order",
                    backupUserAsCC: false,
                    sendToAllMembersIfGroup: Sys.Parameters.GetInstance("P2P").GetParameter("SendNotificationsToEachGroupMembers") === "1",
                    customTags: tempCustomTags,
                    escapeCustomTags: false
                };
                return options;
            }
            function SendNotifToVendor(vendor, template, editMode) {
                var customTag = null;
                var vendorEmail = Data.GetValue("VendorEmail__");
                if (editMode) {
                    vendorEmail = vendor;
                    if (Sys.Helpers.IsEmpty(vendorEmail)) // The notification is sent by "Custer Order" Extraction Script
                     {
                        return true;
                    }
                }
                else {
                    var msg = Language.Translate("_SentTo") + " : " + vendorEmail;
                    Lib.Purchasing.AddConversationItem(msg, Lib.Purchasing.ConversationTypes.Dialog);
                    if (Sys.Helpers.IsEmpty(vendorEmail) || vendor) // The notification is sent by "Custer Order" Extraction Script
                     {
                        return true;
                    }
                    var buyerComment = Data.GetValue("BuyerComment__");
                    if (buyerComment.length > 0) {
                        customTag = {
                            "AdditionalMessage_START__": "",
                            "AdditionalMessage_END__": "",
                            "PortalUrl": "www.esker.fr"
                        };
                        customTag["AdditionalMessage"] = buyerComment;
                        msg += "\r\n\r\n" + buyerComment;
                    }
                    else {
                        customTag = {
                            "AdditionalMessage_START__": "<!--",
                            "AdditionalMessage_END__": "-->"
                        };
                    }
                }
                var email = null;
                // FT-009990: The language used by default for the email notif should be the language of the vendor contact first and then (if not well defined), the language of the buyer
                var userId = Data.GetValue("OwnerId");
                Sys.GenericAPI.Query("ODUSER", "EMAILADDRESS=" + vendorEmail, ["LOGIN"], function (queryResult, error) {
                    if (!error && queryResult.length > 0) {
                        userId = queryResult[0].LOGIN;
                    }
                }, null, 1, { fastsearch: 1 });
                email = Sys.EmailNotification.CreateEmail({
                    emailAddress: vendorEmail,
                    userId: userId,
                    subject: null,
                    template: template,
                    customTags: customTag
                });
                var currentUser = Users.GetUser(Data.GetValue("OwnerId"));
                var CcEmailAddress = currentUser.GetValue("EmailAddress");
                CcEmailAddress += "\n";
                CcEmailAddress += Data.GetValue("EmailCarbonCopy__");
                if (!Sys.Helpers.IsEmpty(CcEmailAddress)) {
                    CcEmailAddress = CcEmailAddress.split("\n").join(";");
                    Sys.EmailNotification.AddCC(email, CcEmailAddress);
                }
                // The vendor was not created (AlwaysCreateVendor == "0") --> always attach in email (ignore AlwaysAttachPurchaseOrder)
                Lib.Purchasing.POValidation.AddAttachments(email, editMode);
                try {
                    Sys.EmailNotification.SendEmail(email);
                }
                catch (e) {
                    Log.Info("SendNotifToVendor error: " + e);
                    Lib.CommonDialog.NextAlert.Define("_Send PO to vendor error", "_Send PO to vendor error message", { isError: true, behaviorName: editMode ? "SendEmailNotifications" : "SendToVendorError" }, e.toString());
                    return false;
                }
                return true;
            }
            POValidation.SendNotifToVendor = SendNotifToVendor;
            function SendNotifToMe(template, editMode) {
                var currentUser = Users.GetUser(Data.GetValue("OwnerId"));
                var currentUserLogin = currentUser.GetValue("Login");
                var customTag = null;
                if (!editMode) {
                    var buyerComment = Data.GetValue("BuyerComment__");
                    if (buyerComment.length > 0) {
                        customTag = {
                            "AdditionalMessage_START__": "",
                            "AdditionalMessage_END__": "",
                            "PortalUrl": "www.esker.fr"
                        };
                        customTag["AdditionalMessage"] = buyerComment;
                    }
                    else {
                        customTag = {
                            "AdditionalMessage_START__": "<!--",
                            "AdditionalMessage_END__": "-->"
                        };
                    }
                }
                var email = Sys.EmailNotification.CreateEmail({
                    userId: currentUserLogin,
                    template: template,
                    customTags: customTag,
                    backupUserAsCC: true,
                    sendToAllMembersIfGroup: Sys.Parameters.GetInstance("P2P").GetParameter("SendNotificationsToEachGroupMembers") === "1"
                });
                var CcEmailAddress = Data.GetValue("EmailCarbonCopy__");
                if (!Sys.Helpers.IsEmpty(CcEmailAddress)) {
                    CcEmailAddress = CcEmailAddress.split("\n").join(";");
                    Sys.EmailNotification.AddCC(email, CcEmailAddress, true);
                }
                Sys.EmailNotification.AddSender(email, "notification@eskerondemand.com", "Esker Purchasing");
                Lib.Purchasing.POValidation.AddAttachments(email, editMode);
                Sys.EmailNotification.SendEmail(email);
            }
            POValidation.SendNotifToMe = SendNotifToMe;
            function NotifyVendorByEmail() {
                var sendOptions = JSON.parse(Variable.GetValueAsString("SendOption"));
                switch (sendOptions.sendOption) {
                    case "POEdit_EmailToVendor":
                        {
                            var ok = Lib.Purchasing.POValidation.SendNotifToVendor(sendOptions.email, "Purchasing_Email_NotifSupplier_PORevision.htm", true);
                            if (!ok) {
                                Process.PreventApproval();
                            }
                            break;
                        }
                    case "POEdit_DoNotSend":
                        Lib.Purchasing.POValidation.SendNotifToMe("Purchasing_Email_NotifBuyer_PORevisionDoNotSend.htm", true);
                        break;
                    default:
                        break;
                }
            }
            POValidation.NotifyVendorByEmail = NotifyVendorByEmail;
            /**
             * Synchronize PO items.
             * When the PO isn't yet ordered the PO items are updated according to the form table. After sent, the PO items
             * are updated according to the goods.
             * Trigger PR in order to synchronize PR items.
             * @returns {boolean} true if succeeds, false otherwise
             */
            POValidation.SynchronizeItems = (function () {
                function InitComputedDataForPOItemsSynchronizeConfig(options) {
                    var buyerVars = Users.GetUser(Data.GetValue("BuyerLogin__")).GetVars();
                    var buyerDn = buyerVars.GetValue_String("FullDn", 0);
                    var computedData = {
                        common: {
                            "BuyerDN": buyerDn
                        },
                        byLine: {}
                    };
                    Sys.Helpers.Data.ForEachTableItem("LineItems__", function (line) {
                        var lineNumber = line.GetValue("LineItemNumber__");
                        var recipientDn = line.GetValue("RecipientDN__");
                        var recipientLogin = recipientDn ? Sys.Helpers.String.ExtractLoginFromDN(recipientDn) : null;
                        // Do not update DeliveryDate and Status when (justUpdateItems == true)
                        computedData.byLine[lineNumber] = {
                            "DeliveryDate": options.justUpdateItems ? undefined : null,
                            "Status": options.justUpdateItems ? undefined : options.futureOrderStatus,
                            "RecipientLogin": recipientLogin
                        };
                    });
                    return Lib.Purchasing.Items.POItemsSynchronizeConfig.computedData = computedData;
                }
                function DependsOnGRItems(options) {
                    return Lib.Purchasing.POStatus.ForDelivery.indexOf(options.currentOrderStatus) !== -1;
                }
                function Synchronize(options) {
                    options = options || {};
                    if (!options.futureOrderStatus) {
                        options.futureOrderStatus = Data.GetValue("OrderStatus__");
                    }
                    if (!options.currentOrderStatus) {
                        options.currentOrderStatus = Data.GetValue("OrderStatus__");
                    }
                    try {
                        var poNumber = Data.GetValue("OrderNumber__");
                        var computedData = InitComputedDataForPOItemsSynchronizeConfig(options);
                        // Update (or create) document items when PO is pending (not yet ordered)
                        if (options.justUpdateItems || !DependsOnGRItems(options)) {
                            Log.Info("Update PO items with number '" + poNumber + "'");
                        }
                        // Update document items when PO will be canceled (no GR yet)
                        else if (options.futureOrderStatus === "Canceled") {
                            Log.Info("Update 'PO items' & 'AP - PO items' with number '" + poNumber + "' to cancel status");
                            UpdatePOItemsAfterCancel();
                            UpdatePOHeaderAfterCancel();
                        }
                        // Update document items when PO is waiting for update (Sent).
                        // Feed computed data according to the GR items.
                        else {
                            //synchronus call server side, no need of callback
                            Lib.Purchasing.POItems.UpdatePOFromGR(function (hasGoodsReceipt, hasValidGoodsReceipt, noGoodsReceiptItems) {
                                var ERPName = Lib.ERP.GetERPName();
                                var createDocInERP = Sys.Parameters.GetInstance("P2P_" + ERPName).GetParameter("CreateDocInERP");
                                // Update PO header/items
                                if (!createDocInERP) {
                                    var deliveredAmountTotal = UpdatePOItemsAfterReception();
                                    UpdatePOHeaderAfterReception(deliveredAmountTotal);
                                }
                                if (noGoodsReceiptItems && noGoodsReceiptItems.length && Sys.Parameters.GetInstance("PAC").GetParameter("DemoEnableInvoiceCreation") == "3") {
                                    Lib.Purchasing.Demo.GenerateVendorInvoice(Data.GetValue("OrderNumber__"), false, noGoodsReceiptItems);
                                }
                            }, computedData);
                        }
                        if (Lib.Purchasing.Items.Synchronize(Lib.Purchasing.Items.POItemsSynchronizeConfig, options.impactedPOItemIndexes)) {
                            if (!options.justUpdateItems) {
                                //Notify all PR
                                var allPRItems = options.allPRItems || Lib.Purchasing.POItems.GetPRItemsInForm();
                                Sys.Helpers.Object.ForEach(allPRItems, function (prItems /*, prNumber: string*/) {
                                    Lib.Purchasing.Items.ResumeDocumentToSynchronizeItems("Purchase requisition", prItems[0].GetValue("PRRUIDEX__"), options.resumeDocumentAction || "SynchronizeItems");
                                });
                            }
                            else {
                                Log.Info("Don't resume PR(s) to synchronize items (justUpdateItems option enabled).");
                            }
                            return true;
                        }
                        Transaction.Delete("SafePAC_PONUMBER");
                        return false;
                    }
                    catch (e) {
                        Log.Error(e.toString());
                        var title = void 0, message = void 0, behaviorName = void 0;
                        if (options.fromAction) {
                            title = "_Items synchronization from action error";
                            message = "_Items synchronization from action error message";
                            behaviorName = null;
                        }
                        else {
                            title = "_Items synchronization error";
                            message = "_Items synchronization error message";
                            behaviorName = "syncItemsFromActionError";
                        }
                        Lib.CommonDialog.NextAlert.Define(title, message, { isError: true, behaviorName: behaviorName });
                        return false;
                    }
                }
                return function (options) {
                    return Sys.Helpers.Data.RollbackableSection(function (rollbackFn) {
                        return Synchronize(options) || rollbackFn();
                    });
                };
            })();
            /**
             * Synchronize PO items. Must be call from an action. Determine the next state of document.
             */
            function SynchronizeItemsFromAction() {
                var status = Data.GetValue("OrderStatus__");
                if (Lib.Purchasing.POStatus.ForPOWorkflow.indexOf(status) !== -1) {
                    POValidation.SynchronizeItems({ fromAction: true });
                    Process.PreventApproval();
                }
                else if (Lib.Purchasing.POStatus.ForDelivery.indexOf(status) !== -1) {
                    // DoAction for doReceipt
                    Process.RecallScript("PostValidation_DoReceipt");
                }
            }
            POValidation.SynchronizeItemsFromAction = SynchronizeItemsFromAction;
            /**
             * Give read right to actual recipient (information sent from Good Receipt) if it is not in the list of recipients already
             * @param {*} actualRecipientDN full DN of actual recipient
             */
            function GiveReadRightToActualRecipientIfNeeded(actualRecipientDN) {
                var recipientsDNs = {};
                Sys.Helpers.Data.ForEachTableItem("LineItems__", function (line) {
                    var recipientDN = line.GetValue("RecipientDN__");
                    if (recipientDN && !recipientsDNs[recipientDN]) {
                        recipientsDNs[recipientDN] = true;
                    }
                });
                if (!recipientsDNs[actualRecipientDN]) {
                    Log.Info("Grant read right to actual recipient: " + actualRecipientDN);
                    Process.AddRight(actualRecipientDN, "read");
                }
            }
            POValidation.GiveReadRightToActualRecipientIfNeeded = GiveReadRightToActualRecipientIfNeeded;
            function GiveReadRightToBuyers(actualBuyerDN) {
                var buyersDNs = {};
                Sys.Helpers.Data.ForEachTableItem("LineItems__", function (line) {
                    var buyerDN = line.GetValue("BuyerDN__");
                    if (buyerDN && !buyersDNs[buyerDN]) {
                        buyersDNs[buyerDN] = true;
                    }
                });
                if (!buyersDNs[actualBuyerDN]) {
                    Log.Info("Grant read right to actual buyer: " + actualBuyerDN);
                    Process.AddRight(actualBuyerDN, "read");
                }
                for (var buyerDN in buyersDNs) {
                    if (Object.prototype.hasOwnProperty.call(buyersDNs, buyerDN)) {
                        Log.Info("Grant read right to buyer: " + buyerDN);
                        Process.AddRight(buyerDN, "read");
                    }
                }
            }
            POValidation.GiveReadRightToBuyers = GiveReadRightToBuyers;
            function InsertPOHeader(orderNumber, createDocInERP) {
                if (!Sys.Helpers.IsEmpty(Process.GetProcessID("AP - Purchase order - Headers__"))) {
                    var newRecord = Process.CreateTableRecord("AP - Purchase order - Headers__");
                    if (newRecord) {
                        var newVars = newRecord.GetVars();
                        newVars.AddValue_String("CompanyCode__", Data.GetValue("CompanyCode__"), true);
                        newVars.AddValue_String("VendorNumber__", Data.GetValue("VendorNumber__"), true);
                        newVars.AddValue_String("OrderNumber__", orderNumber, true);
                        newVars.AddValue_String("OrderedAmount__", Data.GetValue("TotalNetAmount__"), true);
                        newVars.AddValue_String("InvoicedAmount__", "0", true);
                        newVars.AddValue_String("DeliveredAmount__", "0", true);
                        newVars.AddValue_String("Buyer__", Data.GetValue("BuyerLogin__"), true);
                        newVars.AddValue_Long("IsLocalPO__", 1, true);
                        newVars.AddValue_Long("IsCreatedInERP__", createDocInERP ? 1 : 0, true);
                        newVars.AddValue_Date("OrderDate__", Data.GetValue("OrderDate__"), true);
                        newVars.AddValue_String("Currency__", Data.GetValue("Currency__"), true);
                        newRecord.Commit();
                        if (newRecord.GetLastError() !== 0) {
                            Log.Error("Commit failed: " + newRecord.GetLastErrorMessage());
                        }
                    }
                }
            }
            function UpdatePOHeaderAfterReception(deliveredAmountTotal) {
                if (!Sys.Helpers.IsEmpty(Process.GetProcessID("AP - Purchase order - Headers__"))) {
                    var tableName = "AP - Purchase order - Headers__";
                    var filter = "(OrderNumber__=" + Data.GetValue("OrderNumber__") + ")";
                    var attributes = [{ name: "DeliveredAmount__", value: deliveredAmountTotal }];
                    Sys.Helpers.Database.AddOrModifyTableRecord(tableName, filter, attributes);
                }
            }
            function UpdatePOHeaderAfterCancel() {
                if (!Sys.Helpers.IsEmpty(Process.GetProcessID("AP - Purchase order - Headers__"))) {
                    var tableName = "AP - Purchase order - Headers__";
                    var filter = "(OrderNumber__=" + Data.GetValue("OrderNumber__") + ")";
                    var attributes = [{ name: "OrderedAmount__", value: 0 }];
                    Sys.Helpers.Database.AddOrModifyTableRecord(tableName, filter, attributes);
                }
            }
            var POToAP_POItems = {
                common: {
                    "CompanyCode__": { name: "CompanyCode__" },
                    "VendorNumber__": { name: "VendorNumber__" },
                    "OrderDate__": { name: "OrderDate__" },
                    "OrderNumber__": { name: "OrderNumber__" }
                },
                lineItems: {
                    "LineItemNumber__": { name: "ItemNumber__" },
                    "ItemNumber__": { name: "PartNumber__" },
                    "ItemUnitPrice__": { name: "UnitPrice__" },
                    "ItemDescription__": {
                        name: "Description__",
                        preTreatment: function (description) {
                            return description.split(/(\r\n)+|\r+|\n+|\t+/)[0];
                        }
                    },
                    "ItemNetAmount__": { name: "OrderedAmount__" },
                    "ItemQuantity__": { name: "OrderedQuantity__" },
                    "ItemGLAccount__": { name: "GLAccount__" },
                    "ItemGroup__": { name: "Group__" },
                    "ItemCostCenterId__": { name: "CostCenter__" },
                    "BudgetID__": { name: "BudgetID__" },
                    "RecipientDN__": {
                        name: "Receiver__",
                        preTreatment: function (recipientDN) {
                            return Sys.Helpers.String.ExtractLoginFromDN(recipientDN);
                        }
                    },
                    "ItemTaxCode__": { name: "TaxCode__" },
                    "NoGoodsReceipt__": { name: "NoGoodsReceipt__" },
                    "ItemCurrency__": { name: "Currency__" },
                    "ItemType__": {
                        name: "ItemType__",
                        preTreatment: function (type) {
                            return type === Lib.P2P.ItemType.SERVICE_BASED ? Lib.P2P.ItemType.QUANTITY_BASED : type;
                        }
                    },
                    "CostType__": { name: "CostType__" },
                    "ProjectCode__": { name: "ProjectCode__" },
                    "ProjectCodeDescription__": { name: "ProjectCodeDescription__" },
                    "WBSElement__": { name: "WBSElement__" },
                    "WBSElementID__": { name: "WBSElementID__" },
                    "InternalOrder__": { name: "InternalOrder__" },
                    "FreeDimension1__": { name: "FreeDimension1__" },
                    "FreeDimension1ID__": { name: "FreeDimension1ID__" },
                    "ItemUnit__": { name: "UnitOfMeasureCode__" }
                },
                notMapped: {
                    // attribute : Initial value
                    "IsLocalPO__": 1,
                    "IsCreatedInERP__": function () {
                        return Sys.Parameters.GetInstance("P2P_" + Lib.ERP.GetERPName()).GetParameter("CreateDocInERP") ? 1 : 0;
                    },
                    "InvoicedAmount__": "0",
                    "InvoicedQuantity__": "0",
                    "DeliveredAmount__": "0",
                    "DeliveredQuantity__": "0"
                }
            };
            function UpdateAPPOItemsTable(operation /* "INSERT" | "UPDATE" */) {
                var tableName = "AP - Purchase order - Items__";
                if (!Sys.Helpers.IsEmpty(Process.GetProcessID(tableName))) {
                    var table = Data.GetTable("LineItems__");
                    var count = table.GetItemCount();
                    for (var i = 0; i < count; i++) {
                        var lineItem = table.GetItem(i);
                        UpdateAPPOItem(operation, lineItem);
                    }
                }
            }
            function UpdateAPPOItem(operation /* "INSERT" | "UPDATE" */, lineItem) {
                var filter = "(&(OrderNumber__=" + Data.GetValue("OrderNumber__") + ")(ItemNumber__=" + lineItem.GetValue("LineItemNumber__") + "))";
                var attributes = [];
                if (operation === "INSERT") {
                    var commonAttributes = POToAP_POItems.common;
                    for (var nameInForm in commonAttributes) {
                        if (Object.prototype.hasOwnProperty.call(commonAttributes, nameInForm)) {
                            var poAttribute = commonAttributes[nameInForm];
                            var poAttributeValue = Data.GetValue(nameInForm);
                            attributes.push({
                                name: poAttribute.name,
                                value: poAttribute.preTreatment ? poAttribute.preTreatment(poAttributeValue) : poAttributeValue
                            });
                        }
                    }
                    var notMappedAttributes = POToAP_POItems.notMapped;
                    for (var nameInForm in notMappedAttributes) {
                        if (Object.prototype.hasOwnProperty.call(notMappedAttributes, nameInForm)) {
                            attributes.push({
                                name: nameInForm,
                                value: typeof notMappedAttributes[nameInForm] === "function" ? notMappedAttributes[nameInForm]() : notMappedAttributes[nameInForm]
                            });
                        }
                    }
                }
                // Add line items
                var lineItemsAttributes = POToAP_POItems.lineItems;
                for (var nameInForm in lineItemsAttributes) {
                    if (Object.prototype.hasOwnProperty.call(lineItemsAttributes, nameInForm)) {
                        var poAttribute = lineItemsAttributes[nameInForm];
                        var poAttributeValue = lineItem.GetValue(nameInForm);
                        attributes.push({
                            name: poAttribute.name,
                            value: poAttribute.preTreatment ? poAttribute.preTreatment(poAttributeValue) : poAttributeValue
                        });
                    }
                }
                var customDimensions = Sys.Helpers.TryCallFunction("Lib.P2P.Customization.Common.GetCustomDimensions");
                if (customDimensions && customDimensions.poItems) {
                    var poItem = void 0;
                    for (var indexCustomPoItem = 0; indexCustomPoItem < customDimensions.poItems.length; indexCustomPoItem++) {
                        poItem = customDimensions.poItems[indexCustomPoItem];
                        attributes.push({
                            name: poItem.nameInTable,
                            value: lineItem.GetValue(poItem.nameInForm)
                        });
                    }
                }
                Sys.Helpers.Database.AddOrModifyTableRecord("AP - Purchase order - Items__", filter, attributes);
                Log.Info("Record written to DB : ", JSON.stringify(attributes));
            }
            POValidation.UpdateAPPOItem = UpdateAPPOItem;
            function UpdatePOItemsAfterReception() {
                var deliveredAmountTotal = new Sys.Decimal(0);
                if (!Sys.Helpers.IsEmpty(Process.GetProcessID("AP - Purchase order - Items__"))) {
                    var table = Data.GetTable("LineItems__");
                    var count = table.GetItemCount();
                    var todayDate = new Date();
                    todayDate.setUTCHours(12, 0, 0, 0);
                    for (var i = 0; i < count; i++) {
                        var lineItem = table.GetItem(i);
                        var deliveredQuantity = void 0;
                        // In case the item is not receptionable, the delivered amount and quantity are updated after the requested delivery date to be equal to the ordered amount and quantity
                        var noReceptionableItemUpdate = lineItem.GetValue("NoGoodsReceipt__") && Sys.Helpers.Date.CompareDate(todayDate, lineItem.GetValue("ItemRequestedDeliveryDate__")) > 0;
                        if (noReceptionableItemUpdate) {
                            deliveredQuantity = lineItem.GetValue("ItemQuantity__");
                        }
                        else {
                            deliveredQuantity = lineItem.GetValue("ItemTotalDeliveredQuantity__");
                        }
                        var deliveredAmount = new Sys.Decimal(deliveredQuantity).mul(lineItem.GetValue("ItemUnitPrice__"));
                        deliveredAmountTotal = deliveredAmountTotal.add(deliveredAmount);
                        var tableName = "AP - Purchase order - Items__";
                        var filter = "(&(OrderNumber__=" + Data.GetValue("OrderNumber__") + ")(ItemNumber__=" + lineItem.GetValue("LineItemNumber__") + "))";
                        var attributes = [
                            { name: "DeliveredAmount__", value: deliveredAmount.toNumber() },
                            { name: "DeliveredQuantity__", value: deliveredQuantity }
                        ];
                        Sys.Helpers.Database.AddOrModifyTableRecord(tableName, filter, attributes);
                    }
                }
                return deliveredAmountTotal.toNumber();
            }
            function UpdatePOItemsAfterCancel() {
                if (!Sys.Helpers.IsEmpty(Process.GetProcessID("AP - Purchase order - Items__"))) {
                    var table = Data.GetTable("LineItems__");
                    var count = table.GetItemCount();
                    for (var i = 0; i < count; i++) {
                        var lineItem = table.GetItem(i);
                        var tableName = "AP - Purchase order - Items__";
                        var filter = "(&(OrderNumber__=" + Data.GetValue("OrderNumber__") + ")(ItemNumber__=" + lineItem.GetValue("LineItemNumber__") + "))";
                        var attributes = [
                            { name: "OrderedAmount__", value: 0 },
                            { name: "OrderedQuantity__", value: 0 }
                        ];
                        Sys.Helpers.Database.AddOrModifyTableRecord(tableName, filter, attributes);
                    }
                }
            }
            function OnCreatePOInERP(createDocInERP, docData) {
                var ok = true;
                // Here we have to get a number.
                if (!Sys.Helpers.IsEmpty(docData.number)) {
                    InsertPOHeader(docData.number, createDocInERP);
                    UpdateAPPOItemsTable("INSERT");
                }
                if (!ok) {
                    delete docData.number;
                    Log.Error("Could not create the next process, no document number");
                }
                return ok;
            }
            POValidation.OnCreatePOInERP = OnCreatePOInERP;
            function AttachPO(PO_Number, PO_Version) {
                if (PO_Version === void 0) { PO_Version = 0; }
                var poAttached = false;
                var csvAttached = false;
                // Check if PO is already attached
                for (var i = Attach.GetNbAttach() - 1; i >= 0; i--) {
                    var type = Attach.GetValue(i, "Purchasing_DocumentType");
                    if (type === "PO") {
                        Log.Info("PO already attached: " + i);
                        poAttached = true;
                    }
                    else if (type === "CSV") {
                        Log.Info("CSV already attached: " + i);
                        csvAttached = true;
                    }
                }
                if (!poAttached || !csvAttached || PO_Version > 0) {
                    // First step - Get template file
                    var poTemplateInfos = Lib.Purchasing.POExport.GetPOTemplateInfos();
                    var isOldDocxTemplateExist = false;
                    var user = Lib.P2P.GetValidatorOrOwner();
                    isOldDocxTemplateExist = user.IsAvailableTemplate({
                        templateName: "PO_template_V2.docx",
                        language: poTemplateInfos.escapedCompanyCode
                    }) && poTemplateInfos.template == "PurchaseOrder.rpt";
                    if (isOldDocxTemplateExist) {
                        Log.Info("Old .docx purchase order template detected.");
                        poTemplateInfos.fileFormat = "DOCX";
                        poTemplateInfos.template = "PO_template_V2.docx";
                    }
                    var PO_TemplateFile = user.GetTemplateFile(poTemplateInfos.template, poTemplateInfos.escapedCompanyCode);
                    Log.Info("PO_TemplateFile used: " + PO_TemplateFile.GetFileName());
                    if (PO_TemplateFile == null) {
                        Log.Error("Failed to find template file: " + poTemplateInfos.template);
                        return false;
                    }
                    // Second step - Call correct converter according template file format
                    if (poTemplateInfos.fileFormat === "DOCX") // Microsoft Word mode
                     {
                        Log.Info("PO template detected as .DOCX format");
                        var csvFile = TemporaryFile.CreateFile("PO.csv", "utf16");
                        if (!csvFile) {
                            Log.Error("Temporaty file creating failed: PO.csv");
                            return false;
                        }
                        var csv = Lib.Purchasing.CreatePOCsv(PO_Number);
                        TemporaryFile.Append(csvFile, csv);
                    }
                    else if (poTemplateInfos.fileFormat === "RPT") // Crystal mode
                     {
                        Log.Info("PO template detected as .RPT format");
                        // Generate JSON
                        var jsonFile = TemporaryFile.CreateFile("PO.JSON", "utf16");
                        if (!jsonFile) {
                            Log.Error("Temporaty file creating failed: PO.json");
                            return false;
                        }
                        Lib.Purchasing.POExport.CreatePOJsonString(poTemplateInfos, function (jsonString) {
                            TemporaryFile.Append(jsonFile, jsonString);
                        });
                    }
                    else {
                        Log.Error("Error PO template file format is not recognized or compatible");
                        return false;
                    }
                    var iAttachPO = void 0, iAttachCSV = void 0;
                    if (!poAttached || PO_Version > 0) {
                        Log.Time("ConvertFile");
                        var pdfFile = null;
                        if (poTemplateInfos.fileFormat === "DOCX") // Microsoft Word mode
                         {
                            pdfFile = PO_TemplateFile.ConvertFile({ conversionType: "mailMerge", outputFormat: "pdf", csv: csvFile });
                        }
                        else if (poTemplateInfos.fileFormat === "RPT") // Crystal mode
                         {
                            pdfFile = jsonFile.ConvertFile({ conversionType: "crystal", report: PO_TemplateFile });
                        }
                        Log.TimeEnd("ConvertFile");
                        // Third step - Check if error(s) happened with PDF generation
                        if (!pdfFile) {
                            Log.Error("Error converting template to pdf");
                            return false;
                        }
                        var name = Lib.P2P.Export.GetName("PO", "OrderNumber__", PO_Version);
                        if (!Attach.AttachTemporaryFile(pdfFile, { name: name, attachAsConverted: true, attachAsFirst: true })) {
                            Log.Error("Error creating PO attachment");
                            return false;
                        }
                        iAttachPO = 0;
                        Attach.SetValue(iAttachPO, "Purchasing_DocumentType", "PO");
                        Attach.SetValue(iAttachPO, "Purchasing_DocumentRevisionVersion", PO_Version);
                    }
                    if (!csvAttached && poTemplateInfos.fileFormat === "DOCX") {
                        if (!Attach.AttachTemporaryFile(csvFile, { name: "PO.csv", attachAsConverted: true })) {
                            Log.Error("Error creating CSV attachment");
                            return false;
                        }
                        iAttachCSV = Attach.GetNbAttach() - 1;
                        Attach.SetValue(iAttachCSV, "Purchasing_DocumentType", "CSV");
                    }
                    var PO_TermsConditionsFile = void 0;
                    if (!Sys.Helpers.IsEmpty(poTemplateInfos.termsConditions)) {
                        var lastIndex = poTemplateInfos.termsConditions.lastIndexOf(".");
                        var partBefore = poTemplateInfos.termsConditions.substring(0, lastIndex);
                        var partAfter = poTemplateInfos.termsConditions.substring(lastIndex + 1);
                        PO_TermsConditionsFile = Process.GetResourceFile(partBefore + "_" + poTemplateInfos.escapedCompanyCode + "." + partAfter);
                        if (!PO_TermsConditionsFile) {
                            PO_TermsConditionsFile = Process.GetResourceFile(poTemplateInfos.termsConditions);
                        }
                        if (PO_TermsConditionsFile) {
                            // Merge PDF terms conditions
                            Log.Info("PO_TermsConditionsFile used: " + PO_TermsConditionsFile.GetFileName());
                            // ExecutePDFCommand will skip command if the command has already been executed, so add spaces inside it to force its execution
                            var dontSkipPdfCommand = "";
                            if (PO_Version > 0) {
                                dontSkipPdfCommand = Array(PO_Version + 1).join(" ");
                            }
                            var pdfCommands = ["-merge " + dontSkipPdfCommand + " %infile[" + (iAttachPO + 1) + "]% "];
                            pdfCommands.push(PO_TermsConditionsFile);
                            if (!Attach.PDFCommands(pdfCommands)) {
                                Log.Error("Error in PDF command: " + pdfCommands);
                                return false;
                            }
                        }
                        else {
                            Log.Error("File terms and conditions not found: " + poTemplateInfos.termsConditions);
                            return false;
                        }
                    }
                    Sys.Helpers.TryCallFunction("Lib.PO.Customization.Server.OnAttachPO", iAttachPO, iAttachCSV);
                }
                return true;
            }
            POValidation.AttachPO = AttachPO;
            function OnCanceledReception() {
                Log.Info("On canceled reception");
                // Allowing to know if delivery was completed before canceling GR
                var previousOpenOrderLocalCurrency = Data.GetValue("OpenOrderLocalCurrency__");
                var deliveryCompleted = previousOpenOrderLocalCurrency == 0;
                Log.Info("Re-synchronize items");
                var bok = POValidation.SynchronizeItems({ fromAction: false });
                if (bok) {
                    if (deliveryCompleted) {
                        Log.Info("Notify cancellation via the conversation");
                        var data = Variable.GetValueAsString("LastCanceledReceptionData");
                        data = JSON.parse(data);
                        // From the story "PAC - FT-018950 - Fix - issue when advanced payment request and auto reception enabled",
                        // it is decided that the user displayed in the conversation and the From of the notification email is
                        // the owner of the PO (Buyer). Otherwise, in case of Advanced payment, the user in the conversation
                        // would be the last validator (Treasurer).
                        var user = Lib.P2P.GetOwner();
                        var userDisplayName = user.GetValue("DisplayName");
                        var msg = Language.Translate("_Item OnCanceledReception", false, data.grNumber, userDisplayName, data.comment);
                        Lib.Purchasing.AddConversationItem(msg, Lib.Purchasing.ConversationTypes.ItemReception, true, null, user);
                    }
                    else {
                        Log.Info("No need to notify cancellation via the conversation because the delivery wasn't completed");
                    }
                    Process.WaitForUpdate();
                    Process.LeaveForm();
                }
                else {
                    Process.PreventApproval();
                }
            }
            POValidation.OnCanceledReception = OnCanceledReception;
            POValidation.EditOrder = (function () {
                function CheckAnyChanges(result) {
                    return Sys.Helpers.Promise.Create(function (resolve /*, reject: Function*/) {
                        result.changedFields = Lib.Purchasing.POEdition.ChangesManager.GetChangedFieldsList();
                        if (Object.keys(result.changedFields).length === 0) {
                            throw new Lib.Purchasing.POEdition.SkipProcessingException("No modification detected. Nothing to do.");
                        }
                        resolve(result);
                    });
                }
                function CallOnEditOrderUE(result) {
                    return Sys.Helpers.Promise.Create(function (resolve /*, reject: Function*/) {
                        Log.Info("Call user exit OnEditOrder if exists");
                        var changedFieldsList = [];
                        Sys.Helpers.Object.ForEach(result.changedFields, function (list) {
                            changedFieldsList = changedFieldsList.concat(list);
                        });
                        var options = {
                            changedFields: changedFieldsList.slice()
                        };
                        result.onEditOrderUEReturn = Sys.Helpers.TryCallFunction("Lib.PO.Customization.Server.OnEditOrder", options);
                        Log.Info(result.onEditOrderUEReturn ? "OnEditOrder called successfully" : "No return on OnEditOrder call");
                        result.onEditOrderUEReturn = result.onEditOrderUEReturn || {}; // create an empty by default
                        resolve(result);
                    });
                }
                function CheckOrderableOrOverOrderedItems(result) {
                    return Sys.Helpers.Promise.Create(function (resolve /*, reject: Function*/) {
                        var bok = true;
                        Lib.Purchasing.POItems.UpdatePOFromPRAndPO(true)
                            .Catch(function (e) {
                            Lib.Purchasing.OnUnexpectedError(e.toString());
                            bok = false;
                        });
                        bok = bok && Lib.Purchasing.POValidation.CheckOrderableItems();
                        bok = bok && Lib.Purchasing.POValidation.CheckOverOrderedItems();
                        if (!bok) {
                            throw new Error("Error: Some items are overordered");
                        }
                        resolve(result);
                    });
                }
                function ChangeInERP(result) {
                    return Sys.Helpers.Promise.Create(function (resolve /*, reject: Function*/) {
                        Log.Info("Change in ERP...");
                        if (result.rollbackingUpTo === "ChangeInERP") {
                            Lib.Purchasing.POEdition.Rollback.StopRollback();
                        }
                        else {
                            var res = Lib.ERP.ExecuteERPFunc("Change", "PO", Lib.Purchasing.POEdition.ChangesManager.Get(), Lib.Purchasing.POEdition.ChangesManager.Get(null, "LineItems__"));
                            if (res.error) {
                                if (!result.rollbackingUpTo) {
                                    Lib.CommonDialog.NextAlert.Define("_Change PO in ERP error", res.error, { isError: true });
                                    Lib.Purchasing.POEdition.Rollback.NeedRollbackUpTo("ChangeInERP");
                                }
                                throw new Error("Error during change PO in ERP");
                            }
                        }
                        resolve(result);
                    });
                }
                function UpdateBudget(result) {
                    return Sys.Helpers.Promise.Create(function (resolve /*, reject: Function*/) {
                        Log.Info("Updating budget...");
                        if (!Lib.Purchasing.POBudget.AsOrderedAfterEditing()) {
                            // detecting first time functional errors in order to advise user and prepare rollback
                            if (!result.rollbackingUpTo) {
                                var nextAlert = Lib.CommonDialog.NextAlert.GetNextAlert();
                                if (nextAlert && nextAlert.title === "_Budget update error" &&
                                    (nextAlert.message === "_Missing Accounting period" ||
                                        nextAlert.message === "_Closed budgets for accounting period" ||
                                        nextAlert.message === "_Multiple budget error")) {
                                    Lib.Purchasing.POEdition.Rollback.NeedRollbackUpTo("UpdateBudget");
                                }
                            }
                            throw new Error("Error during updating budget");
                        }
                        else if (result.rollbackingUpTo === "UpdateBudget") {
                            Lib.Purchasing.POEdition.Rollback.StopRollback();
                        }
                        resolve(result);
                    });
                }
                function SynchronizeImpactedItems(result) {
                    return Sys.Helpers.Promise.Create(function (resolve /*, reject: Function*/) {
                        Log.Info("Synchronizing items...");
                        if (!Lib.Purchasing.POValidation.SynchronizeItems({ impactedPOItemIndexes: result.impactedPOItemIndexes, resumeDocumentAction: result.action })) {
                            throw new Error("Error during synchronizing items");
                        }
                        resolve(result);
                    });
                }
                function GetImpactedItemIndexes(result) {
                    return Sys.Helpers.Promise.Create(function (resolve /*, reject: Function*/) {
                        Log.Info("Get index of the edited and (partially) received line items");
                        result.impactedItemIndexes = []; // impacted GR
                        result.impactedPOItemIndexes = []; // impacted PO items
                        result.addedItemIndexes = [];
                        result.removedItemIndexes = [];
                        result.additionalFeesHasChanged = false;
                        var realChanges = Lib.Purchasing.POEdition.ChangesManager.GetChangedItems();
                        if (realChanges.LineItems__) {
                            Sys.Helpers.Object.ForEach(realChanges.LineItems__.Changed, function (item) {
                                var itemNumber = item.GetValue("LineItemNumber__");
                                result.impactedPOItemIndexes.push(itemNumber);
                                var quantity = item.GetValue("ItemQuantity__");
                                var openQuantity = item.GetValue("ItemUndeliveredQuantity__");
                                if (quantity !== openQuantity && !item.GetValue("NoGoodsReceipt__")) // GR exists
                                 {
                                    result.impactedItemIndexes.push(itemNumber);
                                }
                            });
                            Sys.Helpers.Object.ForEach(realChanges.LineItems__.Added, function (item) {
                                var itemNumber = item.GetValue("LineItemNumber__");
                                result.impactedPOItemIndexes.push(itemNumber);
                                result.addedItemIndexes.push(itemNumber);
                            });
                            Sys.Helpers.Object.ForEach(realChanges.LineItems__.Deleted, function (key) {
                                result.removedItemIndexes.push(key);
                            });
                        }
                        if (realChanges.AdditionalFees__ && realChanges.AdditionalFees__.Added.length > 0 && realChanges.AdditionalFees__.Changed.length > 0 && realChanges.AdditionalFees__.Deleted.length > 0) {
                            result.additionalFeesHasChanged = true;
                        }
                        if (result.impactedItemIndexes.length === 0 && result.impactedPOItemIndexes.length === 0 && !result.additionalFeesHasChanged && result.removedItemIndexes.length === 0
                            && (!result.changedFields.AdditionalFees__ || (result.changedFields.AdditionalFees__.indexOf("Deleted") === -1 && result.changedFields.AdditionalFees__.indexOf("Added") === -1))) {
                            throw new Lib.Purchasing.POEdition.SkipProcessingException("No impacted item detected. Nothing to do.");
                        }
                        Log.Info("Impacted GR item numbers: " + result.impactedItemIndexes.join(","));
                        Log.Info("Impacted PO item numbers: " + result.impactedPOItemIndexes.join(","));
                        resolve(result);
                    });
                }
                function GetGRNumbers(result) {
                    return Sys.Helpers.Promise.Create(function (resolve /*, reject: Function*/) {
                        Log.Info("Get GR numbers we have to re-open in order to report modifications");
                        if (result.impactedItemIndexes.length === 0) {
                            throw new Lib.Purchasing.POEdition.SkipProcessingException("No impacted GR item detected.");
                        }
                        var filter = "(&";
                        filter += "(!(Status__=Canceled))";
                        filter += "(OrderNumber__=" + Data.GetValue("OrderNumber__") + ")";
                        filter += "(|";
                        result.impactedItemIndexes.forEach(function (itemLineNumber) {
                            filter += "(LineNumber__=" + itemLineNumber + ")";
                        });
                        filter += ")";
                        filter += ")";
                        Log.Info("Selecting GR items with filter: " + filter);
                        var grItems = Lib.Purchasing.Items.GetItemsForDocumentSync(Lib.Purchasing.Items.GRItemsDBInfo, filter, "GRNumber__");
                        if (grItems.length === 0) {
                            throw new Error("Unable to find any GR items.");
                        }
                        result.grNumbers = [];
                        Sys.Helpers.Object.ForEach(grItems, function (value, key) {
                            result.grNumbers.push(key);
                        });
                        resolve(result);
                    });
                }
                function ReOpenGR(result) {
                    return Sys.Helpers.Promise.Create(function (resolve /*, reject: Function*/) {
                        Log.Info("Re-opening impacted GR in order to report changes and update budget...");
                        var filter = "(|";
                        result.grNumbers.forEach(function (grNumber) {
                            filter += "(GRNumber__=" + grNumber + ")";
                        });
                        filter += ")";
                        Log.Info("Selecting GR with filter: " + filter);
                        var query = Process.CreateQueryAsProcessAdmin();
                        query.Reset();
                        query.SetSearchInArchive(true);
                        query.SetSpecificTable("CDNAME#Goods receipt V2");
                        query.SetFilter(filter);
                        query.MoveFirst();
                        var transport;
                        while ((transport = query.MoveNext())) {
                            var vars = transport.GetUninheritedVars();
                            var ruidEx = vars.GetValue_String("RuidEx", 0);
                            var state = vars.GetValue_Long("State", 0);
                            var archiveState = vars.GetValue_Long("ArchiveState", 0);
                            var transportToUpdate = Process.GetUpdatableTransportAsProcessAdmin(ruidEx);
                            var varsOfTransportToUpdate = transportToUpdate.GetUninheritedVars();
                            Log.Info("Re-opening GR with ruidEx: " + ruidEx);
                            // Supported states :
                            if (state === 90) {
                                Log.Info("Document is being processed and ready to be resumed.");
                            }
                            else if (state === 100 && archiveState) {
                                Log.Info("Document is in terminal state. Re-open it.");
                                varsOfTransportToUpdate.AddValue_Long("State", 90, true);
                            }
                            else {
                                throw new Error("Unexpected state of document, state: " + state + ", archiveState: " + archiveState);
                            }
                            if (!transportToUpdate.ResumeWithAction("OnEditOrder")) {
                                throw new Error("Cannot resume GR. Details: " + transportToUpdate.GetLastErrorMessage());
                            }
                        }
                        resolve(result);
                    });
                }
                function UpdateGR(result) {
                    return GetGRNumbers(result)
                        .Then(ReOpenGR)
                        .Catch(function (reason) {
                        if (reason instanceof Lib.Purchasing.POEdition.SkipProcessingException) {
                            Log.Warn("UpdateGR processing stopped. Details: " + reason);
                            return result;
                        }
                        throw reason;
                    });
                }
                function UpdateCustomerOrder(result) {
                    return Sys.Helpers.Promise.Create(function (resolve /*, reject: Function*/) {
                        Log.Info("Update Customer order");
                        var updateCOWithRevisedPO = Variable.GetValueAsString("updateCOWithRevisedPO") == "1";
                        if (updateCOWithRevisedPO) {
                            var query = Process.CreateQueryAsProcessAdmin();
                            query.SetSpecificTable("CDNAME#Customer Order");
                            var queryFilter = "(&(CanceledDateTime__!=*)(Sales_Order_Number__=" + Data.GetValue("OrderNumber__") + "))";
                            query.SetFilter(queryFilter);
                            if (query.MoveFirst()) {
                                var transport = query.MoveNext();
                                if (transport) {
                                    var vars = transport.GetUninheritedVars();
                                    vars.AddValue_Double("Total__", Data.GetValue("TotalNetAmount__"), true);
                                    vars.AddValue_Double("TotalNetAmount__", Data.GetValue("TotalNetAmount__"), true);
                                    vars.AddValue_Date("RevisionDateTime__", Data.GetValue("RevisionDateTime__"), true);
                                    //vars.DeleteAttribute("ConfirmationDatetime__");
                                    //vars.AddValue_Long("State", 70, true);
                                    var type = Attach.GetValue(0, "Purchasing_DocumentType");
                                    var attachFile = Attach.GetConvertedFile(0) || Attach.GetInputFile(0);
                                    if (attachFile != null) {
                                        var name = Attach.GetName(0);
                                        Log.Info("Attach name: " + name);
                                        var newAttach = transport.AddAttachEx(attachFile);
                                        var newAttachVars = newAttach.GetVars();
                                        newAttachVars.AddValue_Long("AttachAsFirst", 1, true);
                                        newAttachVars.AddValue_String("AttachOutputName", name, true);
                                        newAttachVars.AddValue_String("Purchasing_IsPO", type === "PO", true);
                                    }
                                    // Process it
                                    transport.Process();
                                    if (transport.GetLastError()) {
                                        throw new Error("Cannot update Customer order. Details: " + transport.GetLastErrorMessage() + "(" + transport.GetLastError() + ")");
                                    }
                                }
                            }
                            Variable.SetValueAsString("updateCOWithRevisedPO", "0");
                        }
                        resolve(result);
                    });
                }
                function ReAttachPO(result) {
                    return Sys.Helpers.Promise.Create(function (resolve /*, reject: Function*/) {
                        var needToReAttachPO = Lib.Purchasing.POEdition.ChangesManager.NeedToRegeneratePO(result.changedFields);
                        needToReAttachPO = needToReAttachPO && Variable.GetValueAsString("regeneratePO") === "1";
                        if (needToReAttachPO) {
                            var revisionVersion = parseInt(Variable.GetValueAsString("revisionVersion") || "0", 10) + 1;
                            Data.SetValue("RevisionDateTime__", new Date());
                            Data.SetValue("OrderRevised__", true);
                            Data.SetValue("ConfirmationDatetime__", "");
                            if (!Lib.Purchasing.POValidation.AttachPO(Data.GetValue("OrderNumber__"), revisionVersion)) {
                                throw new Error("Error while revising po.pdf");
                            }
                            Variable.SetValueAsString("regeneratePO", "0");
                            Variable.SetValueAsString("updateCOWithRevisedPO", "1");
                            Variable.SetValueAsString("revisionVersion", revisionVersion);
                        }
                        resolve(result);
                    });
                }
                function AddConversationMessage(result) {
                    return Sys.Helpers.Promise.Create(function (resolve /*, reject: Function*/) {
                        Log.Info("Add conversation message");
                        var conversationMessage;
                        if (result.onEditOrderUEReturn.conversationMessage) {
                            conversationMessage = result.onEditOrderUEReturn.conversationMessage;
                        }
                        // build conversation message if needed
                        else {
                            var table_1 = Data.GetTable("LineItems__");
                            var translatedFields_1 = [];
                            var changedFields = Lib.Purchasing.POEdition.ChangesManager.GetChangedFieldsList(true);
                            Sys.Helpers.Object.ForEach(changedFields.LineItems__, function (fieldName) {
                                if (Lib.Purchasing.POItems.customerInterestedItems.indexOf(fieldName) >= 0) {
                                    if (fieldName === "Deleted") {
                                        translatedFields_1.push(Language.Translate("_Item(s) removed"));
                                    }
                                    else if (fieldName === "Added") {
                                        translatedFields_1.push(Language.Translate("_Item(s) added"));
                                    }
                                    else {
                                        translatedFields_1.push(Language.Translate(table_1.GetItem(0).GetProperties(fieldName).label));
                                    }
                                }
                            });
                            if (changedFields.AdditionalFees__ && changedFields.AdditionalFees__.length > 0) {
                                translatedFields_1.push(Language.Translate("_AdditionalFees_pane"));
                            }
                            if (translatedFields_1.length > 0) {
                                conversationMessage = Language.Translate("_Fields modified {0}", false, translatedFields_1.join(", "));
                            }
                        }
                        if (conversationMessage) {
                            Lib.Purchasing.AddConversationItem(conversationMessage);
                        }
                        resolve(result);
                    });
                }
                function UpdateAPPOItems(result) {
                    return Sys.Helpers.Promise.Create(function (resolve /*, reject: Function*/) {
                        Log.Info(">> Update AP PO Items' table");
                        var tableName = "AP - Purchase order - Items__";
                        if (!Sys.Helpers.IsEmpty(Process.GetProcessID(tableName))) {
                            var table = Data.GetTable("LineItems__");
                            var count = table.GetItemCount();
                            for (var i = 0; i < count; i++) {
                                var lineItem = table.GetItem(i);
                                var itemNumber = lineItem.GetValue("LineItemNumber__");
                                if (result.addedItemIndexes.indexOf(itemNumber) >= 0) {
                                    UpdateAPPOItem("INSERT", lineItem);
                                }
                                else {
                                    UpdateAPPOItem("UPDATE", lineItem);
                                }
                            }
                            for (var y = 0; y < result.removedItemIndexes.length; y++) {
                                var key = result.removedItemIndexes[y].split("###");
                                var filter = "(&(OrderNumber__=" + Data.GetValue("OrderNumber__") + ")(PRNumber__=" + key[1] + ")(PRLineNumber__=" + key[2] + "))";
                                Sys.Helpers.Database.RemoveTableRecord("AP - Purchase order - Items__", filter);
                                Log.Info("Record removed to DB : ", JSON.stringify(filter));
                            }
                        }
                        Log.Info("<< Update AP PO Items' table");
                        resolve(result);
                    });
                }
                function SendEmailNotifications(result) {
                    return Sys.Helpers.Promise.Create(function ( /*resolve: Function, reject: Function*/) {
                        // Refresh the form's stored fields
                        Log.Info("Lib.Purchasing.Validation.EditOrder.SendEmailNotifications()");
                        var itemsIndex = [];
                        var table = Data.GetTable("LineItems__");
                        var count = table.GetItemCount();
                        for (var i = 0; i < count; i++) {
                            var lineItem = table.GetItem(i);
                            var itemNumber = lineItem.GetValue("LineItemNumber__");
                            if (result.addedItemIndexes.indexOf(itemNumber) >= 0) {
                                itemsIndex.push(i);
                            }
                        }
                        SendEmailToRequesters(itemsIndex);
                        var sendOptions = JSON.parse(Variable.GetValueAsString("SendOption"));
                        if (sendOptions && sendOptions.sendOption && (sendOptions.sendOption === "POEdit_EmailToVendor" || sendOptions.sendOption === "POEdit_DoNotSend")) {
                            Process.RecallScript("SendEmailNotifications");
                        }
                    });
                }
                function CatchEditOrderReject(reason, result) {
                    if (reason instanceof Lib.Purchasing.POEdition.SkipProcessingException) {
                        Log.Warn("EditOrder processing stopped. Details: " + reason);
                    }
                    else {
                        Log.Error("Edit order in error. Details: " + reason);
                        var newAlertArguments = [
                            "_PO edition error",
                            "_PO edition error message",
                            { isError: true, behaviorName: "POEditionError" }
                        ];
                        var nextAlert = Lib.CommonDialog.NextAlert.GetNextAlert();
                        if (nextAlert && nextAlert.isError) {
                            newAlertArguments[0] = nextAlert.title;
                            newAlertArguments[1] = nextAlert.message;
                            if (Sys.Helpers.IsArray(nextAlert.params)) {
                                newAlertArguments = newAlertArguments.concat(nextAlert.params);
                            }
                        }
                        Lib.CommonDialog.NextAlert.Define.apply(Lib.CommonDialog.NextAlert, newAlertArguments);
                        Process.PreventApproval();
                        result.editOrderReturn = false;
                    }
                }
                function LockImpactedPRItemsAndSynchronizeChanges(result) {
                    return Sys.Helpers.Promise.Create(function (resolve, reject) {
                        // Get list of PR Numbers
                        var allPRItems = Lib.Purchasing.POItems.GetPRItemsInForm();
                        var lockByPRNames = Object.keys(allPRItems);
                        var lockOk = Process.PreventConcurrentAccess(lockByPRNames, function () {
                            CheckAnyChanges(result)
                                .Then(CallOnEditOrderUE)
                                .Then(GetImpactedItemIndexes)
                                .Then(CheckOrderableOrOverOrderedItems)
                                .Then(SynchronizeImpactedItems)
                                .Then(function (r) {
                                resolve(r);
                            })
                                .Catch(function (reason) {
                                reject(reason, result);
                            });
                        });
                        if (!lockOk) {
                            reject("Lock failed", result);
                        }
                    });
                }
                function ResetEditionChanges(result) {
                    return Sys.Helpers.Promise.Create(function (resolve) {
                        Lib.Purchasing.POEdition.ChangesManager.Reset();
                        resolve(result);
                    });
                }
                return function () {
                    var rollbackingUpTo = Lib.Purchasing.POEdition.Rollback.GetRollbackUpTo();
                    Log.Info("EditOrder " + (rollbackingUpTo ? "rollback" : "") + " starting...");
                    var result = {
                        rollbackingUpTo: rollbackingUpTo,
                        changedFields: null,
                        onEditOrderUEReturn: null,
                        editOrderReturn: true,
                        action: "EditOrder"
                    };
                    LockImpactedPRItemsAndSynchronizeChanges(result)
                        .Then(UpdateBudget)
                        .Then(SynchronizeImpactedItems)
                        .Then(UpdateGR)
                        .Then(ChangeInERP)
                        .Then(ReAttachPO)
                        .Then(UpdateCustomerOrder)
                        .Then(UpdateAPPOItems)
                        .Then(AddConversationMessage)
                        .Then(ResetEditionChanges)
                        .Then(SendEmailNotifications)
                        .Catch(function (reason) {
                        CatchEditOrderReject(reason, result);
                    });
                    if (result.editOrderReturn) {
                        Process.WaitForUpdate();
                        Process.LeaveForm();
                    }
                    return result.editOrderReturn;
                };
            })();
        })(POValidation = Purchasing.POValidation || (Purchasing.POValidation = {}));
    })(Purchasing = Lib.Purchasing || (Lib.Purchasing = {}));
})(Lib || (Lib = {}));
