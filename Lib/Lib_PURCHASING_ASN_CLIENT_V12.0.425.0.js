///#GLOBALS Lib Sys
/* LIB_DEFINITION{
  "name": "Lib_Purchasing_ASN_client",
  "libraryType": "LIB",
  "scriptType": "CLIENT",
  "comment": "Purchasing library",
  "require": [
    "Lib_Purchasing_V12.0.425.0",
    "Sys/Sys_GenericAPI_Client",
    "Sys/Sys_Helpers_Object",
    "Sys/Sys_Helpers_Controls"
  ]
}*/
var Lib;
(function (Lib) {
    var Purchasing;
    (function (Purchasing) {
        var ASN;
        (function (ASN) {
            /**
             * @exports Punchout
             * @memberof Lib.Purchasing
             */
            ASN.defaultASNToPaneMapping = {
                "ASNNumber__": "ASNNumber__",
                "ASNShippingDate__": "ShippingDate__",
                "ASNExpectedDeliveryDate__": "ExpectedDeliveryDate__",
                "ASNStatus__": "Status__",
                "ASNCarrier__": "Carrier__"
            };
            function InitRelatedASNPane(ASNTable, ASNPane, fromPO, mapping) {
                if (fromPO === void 0) { fromPO = false; }
                if (mapping === void 0) { mapping = ASN.defaultASNToPaneMapping; }
                ASNPane.Hide(true);
                ASNTable.SetWidth("100%");
                var items;
                ASNTable.ASNNumber__.OnClick = function () {
                    Process.OpenLink({ url: items[this.GetRow().GetLineNumber() - 1].ValidationURL + "&OnQuit=Back", inCurrentTab: true });
                };
                ASNTable.ASNStatus__.OnClick = function () {
                    Process.OpenLink({ url: items[this.GetRow().GetLineNumber() - 1].ValidationURL + "&OnQuit=Back", inCurrentTab: true });
                };
                ASNTable.OnRefreshRow = function (index) {
                    refreshRow(ASNTable.GetRow(index));
                };
                return Sys.Helpers.Promise.Create(function (resolve, reject) {
                    var ASNQueryOptions = {
                        table: "CDNAME#Advanced Shipping Notice",
                        filter: "(OrderNumber__=" + Data.GetValue("OrderNumber__") + ")",
                        attributes: ["ASNNumber__", "ShippingDate__", "ExpectedDeliveryDate__", "Status__", "Carrier__", "ValidationURL"],
                        maxRecords: null,
                        additionalOptions: {
                            bigQuery: true,
                            queryOptions: "FastSearch=1"
                        }
                    };
                    if (fromPO) {
                        ASNQueryOptions.filter = "&" + ASNQueryOptions.filter + "(!(Status__=Draft))";
                    }
                    Sys.GenericAPI.PromisedQuery(ASNQueryOptions)
                        .Then(function (queryResults) {
                        items = queryResults;
                        if (queryResults.length > 0) {
                            Sys.Helpers.SilentChange(function () {
                                ASNPane.Hide(false);
                                ASNTable.SetItemCount(queryResults.length);
                                queryResults.forEach(function (asn, index) {
                                    var newItem = ASNTable.GetItem(index);
                                    Sys.Helpers.Object.ForEach(mapping, function (val, key) { return newItem.SetValue(key, asn[val]); });
                                });
                                Sys.Helpers.Controls.ForEachTableRow(ASNTable, function (row) {
                                    refreshRow(row);
                                });
                            });
                        }
                        resolve(queryResults.length);
                    })
                        .Catch(reject);
                });
            }
            ASN.InitRelatedASNPane = InitRelatedASNPane;
            function refreshRow(row) {
                row.ASNNumber__.DisplayAs({ type: "Link" });
                row.ASNStatus__.DisplayAs({ type: "Link" });
            }
        })(ASN = Purchasing.ASN || (Purchasing.ASN = {}));
    })(Purchasing = Lib.Purchasing || (Lib.Purchasing = {}));
})(Lib || (Lib = {}));
