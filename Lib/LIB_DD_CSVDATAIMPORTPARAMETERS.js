/* LIB_DEFINITION{
  "name": "Lib_DD_CSVDataImportParameters",
  "libraryType": "LIB",
  "scriptType": "SERVER",
  "comment": "Custom library extending Lib_CSVDataImportParameters",
  "versionable": false,
  "require": [
    "Sys/Sys",
    "Lib_V12.0.425.0"
  ]
}*/
///#GLOBALS Lib Sys
var Lib;
Sys.ExtendLib(Lib, "DD.CSVDataImportParameters", function ()
{
	return {
		Parameters: {
			CsvHasHeader: true,
			FileNamePattern: ".*",
			/* possible values:
			 * "full" - if the csv represent the whole content of the table
			 * "incremental" - if the csv represent a subset of the table */
			ReplicationMode: "full",
			ClearTable: false,
			NoDelete: true,
			NoUpdate: false,
			NoInsert: false
		},

		GetErpId: function (fileName)
		{
			//Useless for recipients import
			return "";
		},

		GetTableName: function (fileName)
		{
			return "ODUSER";
		},

		GetMappingFile: function (tableName)
		{
			return "mapping_recipients.xml";
		},

		GetJSON: function ()
		{
			return this.Parameters;
		}
	};
});
