///#GLOBALS Lib Sys
/* LIB_DEFINITION{
  "name": "Lib_Shipping",
  "libraryType": "LIB",
  "scriptType": "COMMON",
  "comment": "Shipping management",
  "require": [
    "Sys/Sys_Helpers",
    "[Lib_Shipping_Customization_V12.0.425.0]"
  ]
}*/
var Lib;
(function (Lib) {
    var Shipping;
    (function (Shipping) {
        var Carrier = /** @class */ (function () {
            function Carrier(_a) {
                var name = _a.name, link = _a.link;
                this._name = name;
                this._link = link;
            }
            Object.defineProperty(Carrier.prototype, "name", {
                get: function () {
                    return this._name;
                },
                enumerable: false,
                configurable: true
            });
            Carrier.prototype.GetLink = function (parcelNumber) {
                return this._link.replace("<parcelNumber>", parcelNumber);
            };
            return Carrier;
        }());
        var carriers = new Array();
        //#region standard carriers support
        var standardCarriers = [
            { name: "FedEx", link: "https://www.fedex.com/fedextrack/?trknbr=<parcelNumber>" },
            { name: "UPS", link: "https://www.ups.com/track?tracknum=<parcelNumber>" }
        ];
        standardCarriers.forEach(function (carrier) {
            carriers.push(new Carrier({ name: carrier.name, link: carrier.link }));
        });
        //#endregion
        //#region add custom carrier from UE
        var addCustomCarriers = Sys.Helpers.TryCallFunction("Lib.Shipping.Customization.Common.GetCustomCarriers");
        if (Array.isArray(addCustomCarriers)) {
            addCustomCarriers.forEach(function (customCarrier) {
                carriers.push(new Carrier({ name: customCarrier.name, link: customCarrier.link }));
            });
        }
        //#endregion
        function GetLink(name, parcelNumber) {
            var carriersWithName = carriers.filter(function (carrier) { return carrier.name == name; });
            if (carriersWithName.length > 0) {
                return carriersWithName[0].GetLink(parcelNumber);
            }
            return "";
        }
        Shipping.GetLink = GetLink;
        function GetNames() {
            return carriers.map(function (carrier) { return carrier.name; });
        }
        Shipping.GetNames = GetNames;
    })(Shipping = Lib.Shipping || (Lib.Shipping = {}));
})(Lib || (Lib = {}));
