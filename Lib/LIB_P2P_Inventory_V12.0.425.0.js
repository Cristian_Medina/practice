///#GLOBALS Lib Sys
/* LIB_DEFINITION
{
    "name": "Lib_P2P_Inventory",
    "libraryType": "LIB",
    "scriptType": "COMMON",
    "comment": "P2P library",
    "require": ["Sys/Sys","Sys/Sys_Helpers_LdapUtil"]
}
*/
var Lib;
(function (Lib) {
    var P2P;
    (function (P2P) {
        var Inventory;
        (function (Inventory_1) {
            var ldaputil = Sys.Helpers.LdapUtil;
            var Inventory = /** @class */ (function () {
                function Inventory(queryResult) {
                    this.record = queryResult.record;
                    this.ruidex = queryResult.GetValue("RUIDEX");
                    this.companyCode = queryResult.GetValue("CompanyCode__");
                    this.itemNumber = queryResult.GetValue("ItemNumber__");
                    this.warehouseID = queryResult.GetValue("WarehouseID__");
                    this.stock = queryResult.GetValue("CurrentStock__");
                    this.stocktakingDatetime = queryResult.GetValue("StocktakingDateTime__");
                    this.minimumThreshold = queryResult.GetValue("MinimumThreshold__");
                    this.expectedStockLevel = queryResult.GetValue("ExpectedStockLevel__");
                    this.minimumThresholdUOM = queryResult.GetValue("MinimumThresholdUOM__");
                    this.itemRuidex = queryResult.GetValue("ItemNumber__.RUIDEX");
                    this.itemDescription = queryResult.GetValue("ItemNumber__.ItemDescription__");
                    this.itemUnitPrice = queryResult.GetValue("ItemNumber__.ItemUnitPrice__");
                    this.itemCurrency = queryResult.GetValue("ItemNumber__.ItemCurrency__");
                }
                Inventory.fieldsMap = {
                    CompanyCode__: "string",
                    ItemNumber__: "string",
                    WarehouseID__: "string",
                    CurrentStock__: "string",
                    StocktakingDateTime__: "string",
                    MinimumThreshold__: "double",
                    ExpectedStockLevel__: "double",
                    MinimumThresholdUOM__: "string"
                };
                return Inventory;
            }());
            Inventory_1.Inventory = Inventory;
            var Warehouse = /** @class */ (function () {
                function Warehouse(queryResult) {
                    this.CompanyCode = queryResult.GetValue("CompanyCode__");
                    this.Name = queryResult.GetValue("WarehouseName__");
                    this.Description = queryResult.GetValue("WarehouseDescription__");
                }
                Warehouse.fieldsMap = {
                    CompanyCode__: "string",
                    WarehouseName__: "string",
                    WarehouseDescription__: "string"
                };
                return Warehouse;
            }());
            Inventory_1.Warehouse = Warehouse;
            var InventoryMovement = /** @class */ (function () {
                function InventoryMovement() {
                }
                InventoryMovement.QueryMovementsForRuidex = function (ruidex) {
                    var filter = "(MovementOrigin__=" + ruidex + ")";
                    return InventoryMovement.QueryMovements(filter);
                };
                InventoryMovement.QueryMovements = function (filter) {
                    var queryOptions = {
                        table: "P2P - Inventory movements__",
                        filter: filter,
                        attributes: [
                            "RUIDEX",
                            "CompanyCode__",
                            "ItemNumber__.ItemDescription__",
                            "ItemNumber__",
                            "LineNumber__",
                            "MovementDateTime__",
                            "MovementOrigin__",
                            "MovementType__",
                            "MovementValue__",
                            "UnitOfMeasure__",
                            "WarehouseID__.WarehouseName__",
                            "WarehouseID__"
                        ],
                        sortOrder: "MovementDateTime__ DESC",
                        maxRecords: null,
                        additionalOptions: {
                            recordBuilder: Sys.GenericAPI.BuildQueryResult,
                            bigQuery: true,
                            queryOptions: "EnableJoin=1"
                        }
                    };
                    return Sys.GenericAPI.PromisedQuery(queryOptions)
                        .Then(function (queryResults) {
                        var movements = [];
                        queryResults.forEach(function (dbMovement) {
                            movements.push(InventoryMovement.FromQueryResult(dbMovement));
                        });
                        return Sys.Helpers.Promise.Resolve(movements);
                    }).Catch(function (err) {
                        Log.Error("Error while deleting Inventory Movements table: " + JSON.stringify(err));
                        return Sys.Helpers.Promise.Reject();
                    });
                };
                InventoryMovement.FromQueryResult = function (queryResult) {
                    var movement = new InventoryMovement();
                    movement.companyCode = queryResult.GetValue("CompanyCode__");
                    movement.itemName = queryResult.GetValue("ItemNumber__.ItemDescription__");
                    movement.itemNumber = queryResult.GetValue("ItemNumber__");
                    movement.lineNumber = queryResult.GetValue("LineNumber__");
                    movement.movementDateTime = new Date(queryResult.GetValue("MovementDateTime__"));
                    movement.movementOrigin = queryResult.GetValue("MovementOrigin__");
                    movement.movementType = queryResult.GetValue("MovementType__");
                    movement.movementValue = parseFloat(queryResult.GetValue("MovementValue__"));
                    movement.unitOfMeasure = queryResult.GetValue("UnitOfMeasure__");
                    movement.warehouseID = queryResult.GetValue("WarehouseID__");
                    movement.warehouseName = queryResult.GetValue("WarehouseID__.WarehouseName__");
                    movement.record = queryResult.record;
                    return movement;
                };
                return InventoryMovement;
            }());
            Inventory_1.InventoryMovement = InventoryMovement;
            function GetWarehouse(CompanyCodes) {
                var options = {
                    table: "P2P - Warehouse__",
                    filter: ldaputil.FilterEqual("CompanyCode__", CompanyCodes).toString(),
                    attributes: ["CompanyCode__", "WarehouseID__", "WarehouseName__", "WarehouseDescription__"],
                    sortOrder: "ItemNumber__ ASC",
                    maxRecords: 100,
                    additionalOptions: {
                        recordBuilder: Sys.GenericAPI.BuildQueryResult,
                        fieldToTypeMap: Warehouse.fieldsMap
                    }
                };
                var warehouseMap = {};
                return Sys.GenericAPI.PromisedQuery(options).Then(function (queryResults) {
                    queryResults.forEach(function (res) {
                        warehouseMap[res.GetValue("WarehouseID__")] = new Warehouse(res);
                    });
                    return Sys.Helpers.Promise.Resolve(warehouseMap);
                });
            }
            Inventory_1.GetWarehouse = GetWarehouse;
            function QueryRealStock(InventoryMap) {
                var filterInventoryMovement = [];
                for (var key in InventoryMap) {
                    if (Object.prototype.hasOwnProperty.call(InventoryMap, key)) {
                        var filterItemNumber = ldaputil.FilterEqual("ItemNumber__", InventoryMap[key].itemNumber);
                        var filterWarehouse = ldaputil.FilterEqual("WarehouseID__", InventoryMap[key].warehouseID);
                        var filterMovementDateTime = ldaputil.FilterGreaterOrEqual("MovementDatetime__", InventoryMap[key].stocktakingDatetime);
                        filterInventoryMovement.push(ldaputil.FilterAnd(filterItemNumber, filterWarehouse, filterMovementDateTime));
                    }
                }
                var options = {
                    table: "P2P - Inventory Movements__",
                    filter: ldaputil.FilterOr.apply(ldaputil, filterInventoryMovement).toString(),
                    attributes: ["ItemNumber__", "WarehouseID__", "SUM(MovementValue__) 'STOCK'"],
                    sortOrder: "ItemNumber__ ASC",
                    maxRecords: 100,
                    groupBy: ["ItemNumber__", "WarehouseID__"],
                    additionalOptions: {
                        queryOptions: "FastSearch=-1"
                    }
                };
                var inventories = [];
                return Sys.GenericAPI.PromisedQuery(options)
                    .Then(function (queryResults) {
                    queryResults.forEach(function (res) {
                        InventoryMap[res.ItemNumber__].stock = res.STOCK;
                        inventories.push(InventoryMap[res.ItemNumber__]);
                    });
                    return Sys.Helpers.Promise.Resolve(inventories);
                });
            }
            function GetInventories(filter, queryExtendedFields, queryRealStock) {
                if (queryExtendedFields === void 0) { queryExtendedFields = true; }
                if (queryRealStock === void 0) { queryRealStock = false; }
                var filterInventory = [];
                filter.forEach(function (f) {
                    var currentFilter = [];
                    if (f.itemNumber) {
                        currentFilter.push(ldaputil.FilterEqual("ItemNumber__", f.itemNumber));
                    }
                    currentFilter.push(ldaputil.FilterEqual("WarehouseID__", f.warehouseID));
                    currentFilter.push(ldaputil.FilterEqual("CompanyCode__", f.companyCode));
                    filterInventory.push(ldaputil.FilterAnd.apply(ldaputil, currentFilter));
                });
                var options = {
                    table: "P2P - Inventory__",
                    filter: ldaputil.FilterOr.apply(ldaputil, filterInventory).toString(),
                    attributes: [
                        "RUIDEX",
                        "CompanyCode__",
                        "ItemNumber__",
                        "StocktakingDateTime__",
                        "WarehouseID__",
                        "MinimumThreshold__",
                        "ExpectedStockLevel__",
                        "CurrentStock__",
                        "MinimumThresholdUOM__"
                    ],
                    sortOrder: "ItemNumber__ ASC",
                    maxRecords: 100,
                    additionalOptions: {
                        recordBuilder: Sys.GenericAPI.BuildQueryResult,
                        fieldToTypeMap: Inventory.fieldsMap,
                        queryOptions: ""
                    }
                };
                if (queryExtendedFields) {
                    options.attributes.push("ItemNumber__.RUIDEX");
                    options.attributes.push("ItemNumber__.ItemDescription__");
                    options.attributes.push("ItemNumber__.ItemUnitPrice__");
                    options.attributes.push("ItemNumber__.ItemCurrency__");
                    options.additionalOptions.queryOptions = "EnableJoin=1";
                }
                var inventories = [];
                return Sys.GenericAPI.PromisedQuery(options)
                    .Then(function (queryResults) {
                    var inventoryMap = {};
                    queryResults.forEach(function (res) {
                        inventoryMap[res.GetValue("ItemNumber__")] = new Inventory(res);
                        inventories.push(inventoryMap[res.GetValue("ItemNumber__")]);
                    });
                    if (queryRealStock) {
                        return QueryRealStock(inventoryMap);
                    }
                    return Sys.Helpers.Promise.Resolve(inventories);
                });
            }
            Inventory_1.GetInventories = GetInventories;
            function IsEnabled() {
                return Sys.Parameters.GetInstance("P2P").GetParameter("EnableInventoryManagement");
            }
            Inventory_1.IsEnabled = IsEnabled;
        })(Inventory = P2P.Inventory || (P2P.Inventory = {}));
    })(P2P = Lib.P2P || (Lib.P2P = {}));
})(Lib || (Lib = {}));
