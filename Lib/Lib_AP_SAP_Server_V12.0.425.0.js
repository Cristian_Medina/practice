///#GLOBALS Lib Sys
/* LIB_DEFINITION{
  "name": "Lib_AP_SAP_SERVER",
  "libraryType": "LIB",
  "scriptType": "SERVER",
  "comment": "library: SAP AP routines",
  "require": [
    "Lib_AP_V12.0.425.0",
    "Lib_AP_SAP_V12.0.425.0",
    "Sys/Sys_Helpers_String_SAP",
    "Sys/Sys_Helpers_SAP"
  ]
}*/
var Lib;
(function (Lib) {
    var AP;
    (function (AP) {
        var SAP;
        (function (SAP) {
            var SAPTaxFromNetBapiServer = /** @class */ (function (_super) {
                __extends(SAPTaxFromNetBapiServer, _super);
                function SAPTaxFromNetBapiServer() {
                    var _this = _super.call(this) || this;
                    _this.bapiTaxFromNet = null;
                    return _this;
                }
                SAPTaxFromNetBapiServer.prototype.AddBapi = function (SAPBapiMgr) {
                    if (SAPBapiMgr && SAPBapiMgr.Connected) {
                        this.bapiTaxFromNet = SAPBapiMgr.Add("Z_ESK_CALCULATE_TAX_FRM_NET");
                        if (this.bapiTaxFromNet) {
                            this.SetLastError("");
                            return true;
                        }
                        this.SetLastError("Failed to add bapi (Z_Esk_Calculate_Tax_Frm_Net): " + SAPBapiMgr.GetLastError());
                    }
                    else {
                        this.SetLastError("Failed to connect to bapi manager");
                    }
                    return false;
                };
                SAPTaxFromNetBapiServer.prototype.Init = function (SAPBapiMgr, companyCode, taxCode, taxJurisdiction, currency, baseAmount) {
                    if (this.AddBapi(SAPBapiMgr)) {
                        this.bapiTaxFromNet.ExportsPool.SetValue("I_BUKRS", companyCode);
                        this.bapiTaxFromNet.ExportsPool.SetValue("I_MWSKZ", taxCode);
                        this.bapiTaxFromNet.ExportsPool.SetValue("I_PROTOKOLL", "");
                        this.bapiTaxFromNet.ExportsPool.SetValue("I_PRSDT", "");
                        this.bapiTaxFromNet.ExportsPool.SetValue("I_TXJCD", taxJurisdiction);
                        this.bapiTaxFromNet.ExportsPool.SetValue("I_WAERS", currency);
                        this.bapiTaxFromNet.ExportsPool.SetValue("I_WRBTR", baseAmount);
                        this.bapiTaxFromNet.ExportsPool.SetValue("I_ZBD1P", 0.0);
                        return true;
                    }
                    this.bapiTaxFromNet = null;
                    return false;
                };
                SAPTaxFromNetBapiServer.prototype.addAccDataParameters = function (item) {
                    var userExitResult = Sys.Helpers.TryCallFunction("Lib.AP.Customization.Common.SAP.ShouldAddAccDataParameterForTaxComputation");
                    if (!Sys.Helpers.IsBoolean(userExitResult) || !userExitResult || !item) {
                        return;
                    }
                    var accData = SAP.GetDefaultTaxAccDataFromItem(item);
                    var accDataUserExit = Sys.Helpers.TryCallFunction("Lib.AP.Customization.Common.SAP.GetAccDataParameterForTaxComputation", accData, item);
                    if (accDataUserExit) {
                        // Replace the structure by the one provided in the user exit
                        accData = accDataUserExit;
                    }
                    if (accData) {
                        var I_ACCDATA = this.bapiTaxFromNet.ExportsPool.Get("I_ACCDATA");
                        // tslint:disable-next-line: forin
                        for (var p in accData) {
                            if (Object.prototype.hasOwnProperty.call(accData, p)) {
                                I_ACCDATA.SetValue(p, accData[p]);
                            }
                        }
                    }
                };
                SAPTaxFromNetBapiServer.prototype.ReadResults = function (taxCode) {
                    if (this.bapiTaxFromNet) {
                        var taxAccounts = [];
                        // Read the BAPI results
                        var tables = this.bapiTaxFromNet.TablesPool;
                        var T_Mwdat = tables.Get("T_MWDAT");
                        // Return all the tax lines, not only one.
                        // Ex: Payables invoice with interstate tax on sales/purchases.
                        // Interstate tax is the responsibility of the person making the
                        // purchase and is not paid to the vendor.
                        for (var idx = 0; idx < T_Mwdat.Count; idx++) {
                            var aTaxAccount = void 0;
                            aTaxAccount = Lib.AP.SAP.GetNewSAPTaxAccount(T_Mwdat.Get(idx), taxCode);
                            taxAccounts.push(aTaxAccount);
                        }
                        return taxAccounts;
                    }
                    this.SetLastError("ReadResults shouldn't be call before Init");
                    return null;
                };
                SAPTaxFromNetBapiServer.prototype.GetTaxRateFromTaxCode = function (SAPBapiMgr, companyCode, taxCode, taxJurisdiction, currency, callback, item) {
                    if (this.Init(SAPBapiMgr, companyCode, taxCode, taxJurisdiction, currency, 1000.0)) {
                        this.addAccDataParameters(item);
                        var exceptionMessage = void 0;
                        exceptionMessage = this.bapiTaxFromNet.Call();
                        if (!exceptionMessage) {
                            var jsonParams = JSON.parse(this.bapiTaxFromNet.GetJsonParameters(false));
                            if (callback) {
                                callback(item, jsonParams.IMPORTS.E_FWSTE / 10);
                            }
                            return jsonParams.IMPORTS.E_FWSTE / 10;
                        }
                        this.SetLastError("An exception occured during bapi call (Z_Esk_Calculate_Tax_Frm_Net, taxcode :" + taxCode + "): " + exceptionMessage);
                    }
                    if (callback) {
                        callback(item, null);
                    }
                    return null;
                };
                SAPTaxFromNetBapiServer.prototype.Calculate = function (SAPBapiMgr, companyCode, taxCode, taxJurisdiction, currency, baseAmount) {
                    if (this.Init(SAPBapiMgr, companyCode, taxCode, taxJurisdiction, currency, baseAmount)) {
                        var exceptionMessage = this.bapiTaxFromNet.Call();
                        if (exceptionMessage === "") {
                            return this.ReadResults(taxCode);
                        }
                        this.SetLastError("An exception occured during bapi call (Z_Esk_Calculate_Tax_Frm_Net): " + exceptionMessage);
                    }
                    return null;
                };
                SAPTaxFromNetBapiServer.prototype.GetLastError = function () {
                    return this.lastError;
                };
                SAPTaxFromNetBapiServer.prototype.SetLastError = function (error) {
                    Log.Error(error);
                    this.lastError = error;
                };
                return SAPTaxFromNetBapiServer;
            }(SAP.SAPTaxFromNetBapi));
            var BAPI = /** @class */ (function () {
                function BAPI(sapName, name) {
                    this.Name = name ? name : sapName;
                    this.SapName = sapName;
                    this.FunctionModule = null;
                }
                BAPI.prototype.Init = function (bapiManager) {
                    if (bapiManager) {
                        this.FunctionModule = bapiManager.Add(this.SapName);
                        if (this.FunctionModule) {
                            Sys.Helpers.SAP.SetLastError("");
                        }
                        else if (bapiManager.GetLastError() === "EXCEPTION FU_NOT_FOUND RAISED") {
                            Sys.Helpers.SAP.SetLastError("The function module " + this.SapName + " is missing on SAP server.", true);
                        }
                        else {
                            Sys.Helpers.SAP.SetLastError("Failed to get BAPI definition: " + bapiManager.GetLastError(), true);
                        }
                    }
                    return this.FunctionModule;
                };
                BAPI.prototype.Reset = function () {
                    this.FunctionModule = null;
                };
                return BAPI;
            }());
            SAP.BAPI = BAPI;
            var BAPIController = /** @class */ (function () {
                function BAPIController() {
                    this.sapControl = null;
                    this.bapiManager = null;
                    this.sapCurrentLanguage = "";
                    this.BapiList = {};
                }
                BAPIController.prototype.AddBapi = function (bapi) {
                    this.BapiList[bapi.Name] = bapi;
                    return this.BapiList[bapi.Name];
                };
                BAPIController.prototype.GetBapi = function (bapiName) {
                    var bapi = this.BapiList[bapiName];
                    if (bapi) {
                        return bapi.FunctionModule;
                    }
                    return null;
                };
                BAPIController.prototype.InitBapi = function (bapiName) {
                    Log.Info("Init " + bapiName);
                    if (this.bapiManager && this.bapiManager.Connected) {
                        var bapi = this.BapiList[bapiName];
                        if (bapi) {
                            return bapi.Init(this.bapiManager);
                        }
                    }
                    else {
                        Sys.Helpers.SAP.SetLastError("InitBapi should not be called directly");
                    }
                    return null;
                };
                BAPIController.prototype.InitBapiManager = function (sapControl) {
                    Sys.Helpers.SAP.SetLastError("");
                    if (sapControl) {
                        if (!this.bapiManager || !this.bapiManager.Connected) {
                            var sapConfigName = Variable.GetValueAsString("SAPConfiguration");
                            Log.Info("SAP Configuration: " + sapConfigName);
                            this.sapControl = sapControl;
                            this.sapCurrentLanguage = sapControl.GetCurrentConnectionLanguage(sapConfigName);
                            var bapiMgr = sapControl.CreateBapiManager(sapConfigName);
                            if (!bapiMgr) {
                                Sys.Helpers.SAP.SetLastError("Failed to create Bapi Manager: " + sapControl.GetLastError(), true);
                            }
                            else {
                                if (bapiMgr.Connected) {
                                    this.bapiManager = bapiMgr;
                                    return true;
                                }
                                var technicalError = false;
                                var errorMsg = bapiMgr.GetLastLogonError();
                                if (!errorMsg) {
                                    technicalError = true;
                                    errorMsg = bapiMgr.GetLastError();
                                }
                                Sys.Helpers.SAP.SetLastError(errorMsg, technicalError);
                            }
                        }
                        else {
                            Sys.Helpers.SAP.SetLastError("BAPIController.InitBapiManager: bapiManager already connected");
                        }
                    }
                    else {
                        Sys.Helpers.SAP.SetLastError("BAPIController.InitBapiManager: sapControl is required");
                    }
                    return false;
                };
                // Initializes and connects the BapiManager and load the definition of all BAPIs
                BAPIController.prototype.Init = function (sapControl) {
                    if ((this.bapiManager && this.bapiManager.Connected) || this.InitBapiManager(sapControl)) {
                        var res = true;
                        for (var bapiName in this.BapiList) {
                            if (Object.prototype.hasOwnProperty.call(this.BapiList, bapiName)) {
                                var bapi = this.BapiList[bapiName];
                                if (bapi) {
                                    res = res && bapi.Init(this.bapiManager) !== null;
                                }
                            }
                        }
                        return res;
                    }
                    return false;
                };
                // Reset BAPI parameters to default values
                BAPIController.prototype.ResetBapi = function (bapiName) {
                    var bapi = this.BapiList[bapiName];
                    if (bapi) {
                        bapi.Reset();
                    }
                };
                // Resets all BAPIs parameters to default values
                BAPIController.prototype.ResetAllBapis = function () {
                    for (var bapiName in this.BapiList) {
                        if (Object.prototype.hasOwnProperty.call(this.BapiList, bapiName)) {
                            this.ResetBapi(bapiName);
                        }
                    }
                };
                /** Finalize all member variables.
                *	If you are working on BAPIs parameters in SAP, use the ReleaseBapiManagerByID API instead of FinalizeBapiManagerByID.
                *	This will force the SAPProxy to reload the BAPI definition from SAP with every script execution.
                *	Revert to FinalizeBapiManagerByID once the BAPI development is done to benefit from the cache mechanism and improve performances.
                */
                BAPIController.prototype.Finalize = function () {
                    if (this.sapControl && this.bapiManager) {
                        try {
                            this.BapiList = {};
                            this.sapControl.FinalizeBapiManagerByID(this.bapiManager.Id);
                        }
                        finally {
                            this.bapiManager = null;
                        }
                    }
                };
                // Translate key using the SAPProxy language file
                BAPIController.prototype.sapi18n = function (key, aValues) {
                    if (this.sapControl && key) {
                        var res = this.sapControl.i18n(key, this.sapCurrentLanguage);
                        // now replace the %i with the values
                        if (typeof aValues === "object" && aValues instanceof Array) {
                            for (var idx = 0; idx < aValues.length; idx++) {
                                res = res.replace("%" + (idx + 1), aValues[idx]);
                            }
                        }
                        return res;
                    }
                    return key;
                };
                return BAPIController;
            }());
            SAP.BAPIController = BAPIController;
            var SAPParameters = /** @class */ (function () {
                function SAPParameters() {
                    this.BapiController = null;
                }
                SAPParameters.prototype.AddBapi = function (sapName, name) {
                    if (this.BapiController) {
                        var bapi = Lib.AP.SAP.GetNewBapi(sapName, name);
                        this.BapiController.AddBapi(bapi);
                        return bapi;
                    }
                    return null;
                };
                SAPParameters.prototype.GetBapi = function (bapiName) {
                    if (this.BapiController) {
                        return this.BapiController.GetBapi(bapiName);
                    }
                    return null;
                };
                SAPParameters.prototype.GetTable = function (bapiName, name) {
                    if (this.BapiController) {
                        var bapi = this.BapiController.GetBapi(bapiName);
                        if (bapi && bapi.TablesPool) {
                            return bapi.TablesPool.Get(name);
                        }
                    }
                    return null;
                };
                SAPParameters.prototype.GetExport = function (bapiName, name) {
                    if (this.BapiController) {
                        var bapi = this.BapiController.GetBapi(bapiName);
                        if (bapi && bapi.ExportsPool) {
                            return bapi.ExportsPool.Get(name);
                        }
                    }
                    return null;
                };
                SAPParameters.prototype.GetImport = function (bapiName, name) {
                    if (this.BapiController) {
                        var bapi = this.BapiController.GetBapi(bapiName);
                        if (bapi && bapi.ExportsPool) {
                            return bapi.ImportsPool.Get(name);
                        }
                    }
                    return null;
                };
                SAPParameters.prototype.ResetParameters = function () {
                    if (this.BapiController) {
                        this.BapiController.ResetAllBapis();
                    }
                };
                SAPParameters.prototype.Finalize = function () {
                    this.ResetParameters();
                    if (this.BapiController) {
                        this.BapiController.Finalize();
                    }
                };
                return SAPParameters;
            }());
            SAP.SAPParameters = SAPParameters;
            function GetNewBapi(sapName, name) {
                return new BAPI(sapName, name);
            }
            SAP.GetNewBapi = GetNewBapi;
            function GetNewBapiController() {
                return new BAPIController();
            }
            SAP.GetNewBapiController = GetNewBapiController;
            function GetNewSAPParameters() {
                return new SAPParameters();
            }
            SAP.GetNewSAPParameters = GetNewSAPParameters;
            function GetCompanyCodePeriod(bapiCompanyCodeGetPeriod, companyCode, postingDate) {
                if (bapiCompanyCodeGetPeriod) {
                    var exports = bapiCompanyCodeGetPeriod.ExportsPool;
                    exports.Set("COMPANYCODEID", companyCode);
                    exports.Set("POSTING_DATE", postingDate);
                    bapiCompanyCodeGetPeriod.UseCache = true;
                    var exception = bapiCompanyCodeGetPeriod.Call();
                    if (!exception) {
                        var imports = bapiCompanyCodeGetPeriod.ImportsPool;
                        return {
                            Fiscal_Period: imports.Get("FISCAL_PERIOD"),
                            Fiscal_Year: imports.Get("FISCAL_YEAR")
                        };
                    }
                    Sys.Helpers.SAP.SetLastError("GetCompanyCodePeriod failed: " + exception);
                }
                return null;
            }
            SAP.GetCompanyCodePeriod = GetCompanyCodePeriod;
            function GetCompanyCodeCurrency(rfcReadTableBapi, companyCode) {
                var T001Results = Sys.Helpers.SAP.ReadSAPTable(rfcReadTableBapi, "T001", "WAERS", "BUKRS = '" + companyCode + "'", 1, 0, false, { "useCache": true });
                if (T001Results && T001Results.length > 0) {
                    return Sys.Helpers.String.SAP.Trim(T001Results[0].WAERS);
                }
                return "";
            }
            SAP.GetCompanyCodeCurrency = GetCompanyCodeCurrency;
            function ConvertToSAPLanguage(language) {
                // convert language for SAP
                language = language.toUpperCase();
                language = language === "ES" ? "SP" : language;
                return language;
            }
            SAP.ConvertToSAPLanguage = ConvertToSAPLanguage;
            var g_currentUserLanguage = "";
            function GetCurrentUserLanguage() {
                if (!g_currentUserLanguage) {
                    var currentUser = Users.GetUser(Data.GetValue("OwnerId"));
                    if (currentUser) {
                        g_currentUserLanguage = currentUser.GetVars().GetValue_String("language", 0);
                    }
                }
                return g_currentUserLanguage;
            }
            SAP.GetCurrentUserLanguage = GetCurrentUserLanguage;
            function UpdateGLCCDescriptions(item) {
                if (!item) {
                    return;
                }
                var account = item.GetValue("GLAccount__"), description;
                var bapiParams = Lib.AP.SAP.PurchaseOrder.GetBapiParameters();
                var companyCode = Lib.AP.GetLineItemCompanyCode(item);
                if (account && bapiParams) {
                    description = Lib.AP.SAP.GetGLAccountDescriptionServer(bapiParams.GetBapi("BAPI_GL_ACC_GETDETAIL"), companyCode, account, Lib.AP.SAP.GetCurrentUserLanguage());
                    item.SetValue("GLDescription__", description);
                }
                var costCenter = item.GetValue("CostCenter__");
                if (costCenter && bapiParams) {
                    description = Lib.AP.SAP.GetCostCenterDescription(bapiParams.GetBapi("RFC_READ_TABLE"), companyCode, costCenter, Lib.AP.SAP.GetCurrentUserLanguage());
                    item.SetValue("CCDescription__", description);
                }
            }
            SAP.UpdateGLCCDescriptions = UpdateGLCCDescriptions;
            function GetGLAccountDescriptionServer(bapiGLAccGetDetail, companyCode, account_no, language) {
                // Check in cache first
                if (!SAP.glDescriptionsCache[companyCode]) {
                    Lib.AP.SAP.InitGLDescriptionCacheFromLines(companyCode);
                }
                if (SAP.glDescriptionsCache[companyCode][account_no]) {
                    return SAP.glDescriptionsCache[companyCode][account_no];
                }
                // description not in cache, we have to query SAP
                var accountDescription = "";
                if (bapiGLAccGetDetail) {
                    var exports = bapiGLAccGetDetail.ExportsPool;
                    exports.Set("COMPANYCODE", companyCode);
                    exports.Set("GLACCT", Sys.Helpers.String.SAP.NormalizeID(account_no, 10));
                    exports.Set("LANGUAGE", Lib.AP.SAP.ConvertToSAPLanguage(language));
                    exports.Set("TEXT_ONLY", "X");
                    bapiGLAccGetDetail.UseCache = true;
                    var exception = bapiGLAccGetDetail.Call();
                    if (!exception) {
                        var imports = bapiGLAccGetDetail.ImportsPool;
                        var accDetail = imports.Get("ACCOUNT_DETAIL");
                        var longText = accDetail.GetValue("LONG_TEXT");
                        if (longText) {
                            accountDescription = longText;
                        }
                        else {
                            accountDescription = accDetail.GetValue("SHORT_TEXT");
                        }
                    }
                    // Add result to cache
                    SAP.glDescriptionsCache[companyCode][account_no] = accountDescription;
                }
                return accountDescription;
            }
            SAP.GetGLAccountDescriptionServer = GetGLAccountDescriptionServer;
            function GetCostCenterDescription(rfcReadTableBapi, companyCode, costCenter, language) {
                var description = "";
                if (rfcReadTableBapi && companyCode && costCenter && language) {
                    var nowDate = Sys.Helpers.Date.Date2RfcReadTableDate(new Date());
                    var filter = "BUKRS = '" + companyCode + "' AND KOST1 = '" + Sys.Helpers.String.SAP.NormalizeID(costCenter, 10) + "' \n AND SPRAS = '" + Lib.AP.SAP.ConvertToSAPLanguage(language) + "' AND DATAB <= '" + nowDate + "' AND DATBI >= '" + nowDate + "'";
                    var results = Sys.Helpers.SAP.ReadSAPTable(rfcReadTableBapi, "M_KOSTN", "MCTXT", filter, 1, 0, false, { "useCache": true });
                    if (results && results.length > 0) {
                        description = Sys.Helpers.String.SAP.Trim(results[0].MCTXT);
                    }
                }
                return description;
            }
            SAP.GetCostCenterDescription = GetCostCenterDescription;
            function GetInvoicePaymentTerms(rfcReadTableBapi, invoiceRefNumber, itemLine) {
                var invPaymentTerms = null;
                if (rfcReadTableBapi && invoiceRefNumber && invoiceRefNumber.companyCode && invoiceRefNumber.documentNumber && invoiceRefNumber.fiscalYear) {
                    var filter = "BELNR = '" + invoiceRefNumber.documentNumber + "' AND GJAHR = '" + invoiceRefNumber.fiscalYear + "'\n";
                    filter += " AND BUZEI = '" + itemLine + "' AND BUKRS = '" + invoiceRefNumber.companyCode + "'";
                    var BSEGResults = Sys.Helpers.SAP.ReadSAPTable(rfcReadTableBapi, "BSEG", "ZFBDT|ZTERM|ZLSPR", filter, 1, 0, false, { "useCache": false });
                    if (BSEGResults) {
                        if (BSEGResults.length > 0) {
                            invPaymentTerms = {
                                baseLineDate: Sys.Helpers.String.SAP.Trim(BSEGResults[0].ZFBDT),
                                paymentTerms: Sys.Helpers.String.SAP.Trim(BSEGResults[0].ZTERM),
                                paymentBlock: Sys.Helpers.String.SAP.Trim(BSEGResults[0].ZLSPR)
                            };
                        }
                        else {
                            var docNotFound = invoiceRefNumber.documentNumber + " " + invoiceRefNumber.companyCode + " " + invoiceRefNumber.fiscalYear;
                            Sys.Helpers.SAP.SetLastError("GetInvoicePaymentTerms: Document not found in BSEG: " + docNotFound);
                        }
                    }
                }
                return invPaymentTerms;
            }
            SAP.GetInvoicePaymentTerms = GetInvoicePaymentTerms;
            /**
            * SAP ReadSAPTable call to retrieve Material description
            */
            function GetMaterialDescriptionServer(rfcReadTableBapi, material, language) {
                if (rfcReadTableBapi) {
                    var filter = "MATNR = '" + material + "' AND SPRAS = '" + language + "'";
                    var MATNRResults = Sys.Helpers.SAP.ReadSAPTable(rfcReadTableBapi, "MAKT", "MAKTX", filter, 1, 0, false, { "useCache": true });
                    if (MATNRResults && MATNRResults.length > 0) {
                        return Sys.Helpers.String.SAP.Trim(MATNRResults[0].MAKTX);
                    }
                }
                return "";
            }
            SAP.GetMaterialDescriptionServer = GetMaterialDescriptionServer;
            Lib.AP.SAP.GetNewSAPTaxFromNetBapi = function () {
                return new SAPTaxFromNetBapiServer();
            };
            function GetTaxFromNetServer(SAPBapiMgr, companyCode, taxCode, taxJurisdiction, itemNumberTax, currency, baseAmount) {
                if (taxCode && Sys.Helpers.String.SAP.Trim(taxCode).length > 0) {
                    var sapTaxFromNetBapi = new SAPTaxFromNetBapiServer();
                    var taxAccounts = sapTaxFromNetBapi.Calculate(SAPBapiMgr, companyCode, taxCode, taxJurisdiction, currency, baseAmount);
                    if (taxAccounts) {
                        return taxAccounts;
                    }
                    Sys.Helpers.SAP.SetLastError(sapTaxFromNetBapi.GetLastError());
                }
                else {
                    Sys.Helpers.SAP.SetLastError("GetTaxFromNet, 'taxCode' is a required parameter");
                }
                return null;
            }
            SAP.GetTaxFromNetServer = GetTaxFromNetServer;
            function GetTaxFromGrossServer() {
                // Not implemented yet
                return null;
            }
            SAP.GetTaxFromGrossServer = GetTaxFromGrossServer;
            function AlternativePayeeCheck(rfcReadTableBapi, vendorNumber, altPayee) {
                if (rfcReadTableBapi && vendorNumber && altPayee) {
                    var LFZAResults = Sys.Helpers.SAP.ReadSAPTable(rfcReadTableBapi, "LFZA", "EMPFK", "LIFNR = '" + vendorNumber + "'", 0, 0, false, { "useCache": false });
                    if (LFZAResults) {
                        var payee = Sys.Helpers.String.SAP.NormalizeID(altPayee, 10);
                        for (var i = 0; i < LFZAResults.length; i++) {
                            if (payee === LFZAResults[i].EMPFK) {
                                return true;
                            }
                        }
                    }
                }
                return false;
            }
            SAP.AlternativePayeeCheck = AlternativePayeeCheck;
            /**
            * convert value to foreign currency
            * @param {object} bapiController
            * @param {number} value the value to be checked
            * @param {string} companyCode
            * @param {string} currencyKey local currency
            * @param {string} foreignCurrencyKey foreign currency
            * @param {string} translationDate date of translation to retrieve the ratio between local and foreign
            * @param {number} exchangeRate specific rate to apply on conversion
            * @param {string} typeOfRate Type of rate: M=Average rate G=Bank buying rate B=bank selling rate
            * @param {boolean} bUseExchangeRatesTable indicate if rate should be used from exchange rates table (TCURR)
            * @returns {number|null}
            */
            function ConvertToForeignCurrencyServer(bapiController, value, currencyKey, foreignCurrencyKey, translationDate, exchangeRate, typeOfRate, bUseExchangeRatesTable) {
                var bapiConvToForeignCurrency = bapiController.GetBapi("Z_ESK_CONV_TO_FOREIGN_CURRENCY");
                if (bapiConvToForeignCurrency) {
                    var exports = bapiConvToForeignCurrency.ExportsPool;
                    exports.Set("CLIENT", Sys.Helpers.SAP.GetSAPClient(bapiController.sapControl, Variable.GetValueAsString("SAPConfiguration")));
                    exports.Set("DATE", translationDate);
                    exports.Set("FOREIGN_CURRENCY", foreignCurrencyKey);
                    exports.Set("LOCAL_AMOUNT", value);
                    exports.Set("LOCAL_CURRENCY", currencyKey);
                    exports.Set("RATE", exchangeRate);
                    exports.Set("READ_TCURR", bUseExchangeRatesTable ? "X" : "");
                    exports.Set("TYPE_OF_RATE", typeOfRate);
                    var exception = bapiConvToForeignCurrency.Call();
                    if (!exception) {
                        var imports = bapiConvToForeignCurrency.ImportsPool;
                        return {
                            "DerivedRateType": imports.GetValue("DERIVED_RATE_TYPE"),
                            "ExchangeRate": imports.GetValue("EXCHANGE_RATE"),
                            "FixedRate": imports.GetValue("FIXED_RATE"),
                            "ForeignAmount": imports.GetValue("FOREIGN_AMOUNT"),
                            "ForeignFactor": imports.GetValue("FOREIGN_FACTOR"),
                            "LocalFactor": imports.GetValue("LOCAL_FACTOR")
                        };
                    }
                    var text = bapiController.sapi18n("_Currency rate %1 / %2 rate type %3 for %4 not maintained in the system settings", [foreignCurrencyKey, currencyKey, "M", translationDate]);
                    Sys.Helpers.SAP.SetLastError(text);
                }
                return null;
            }
            SAP.ConvertToForeignCurrencyServer = ConvertToForeignCurrencyServer;
            function GetVendorDetailsFromSAP(sCompanyCode, sVendorNumber, sVendorName) {
                if (!Variable.GetValueAsString("SAPConfiguration")) {
                    // SAPConfiguration is initiated by ERPManager init,
                    // So we try to init ERPManager if not defined
                    Lib.P2P.InitSAPConfiguration("SAP", "AP");
                }
                var params = Lib.AP.SAP.PurchaseOrder.GetBapiParameters();
                if (!params) {
                    Log.Error("extractionscript externalQueryFunction - Failed to initialize BAPI parameters");
                    return;
                }
                var filter = "BUKRS = '" + sCompanyCode + "'";
                if (sVendorName) {
                    filter += " AND MCOD1 = '" + Lib.AP.SAP.escapeToSQL(sVendorName).toUpperCase() + "'";
                }
                else if (sVendorNumber) {
                    filter += " AND LIFNR = '" + Sys.Helpers.String.SAP.NormalizeID(Lib.AP.SAP.escapeToSQL(sVendorNumber), 10) + "'";
                }
                var attributes = "NAME1|LIFNR|STRAS|PSTLZ|REGIO|LAND1|ORT01|ZTERM|QSSKZ";
                var customFields = Sys.Helpers.TryCallFunction("Lib.AP.Customization.Common.GetVendorCustomFields");
                if (customFields) {
                    Sys.Helpers.Array.ForEach(customFields, function (field) {
                        if (field.nameInSAP) {
                            attributes += "|" + field.nameInSAP;
                        }
                    });
                }
                var results = Sys.Helpers.SAP.ReadSAPTable(params.GetBapi("RFC_READ_TABLE"), "ZESK_VENDORS", attributes, filter, 0, 0, false, { "useCache": false });
                if (!results) {
                    Log.Error("RFC_READ_TABLE BAPI call on ZESK_VENDORS error: " + Sys.Helpers.SAP.GetLastError());
                    Log.Error("You might need to install the latest view definition on your SAP server");
                }
                else {
                    Lib.AP.SAP.FillVendorDetailsFromSAPResult(results);
                }
            }
            SAP.GetVendorDetailsFromSAP = GetVendorDetailsFromSAP;
            function FillVendorDetailsFromSAPResult(results) {
                if (results && results.length > 0) {
                    var item_1 = results[0];
                    Data.SetValue("VendorNumber__", Sys.Helpers.String.SAP.TrimLeadingZeroFromID(item_1.LIFNR));
                    Data.SetValue("VendorName__", Sys.Helpers.String.SAP.Trim(item_1.NAME1));
                    Data.SetValue("VendorStreet__", Sys.Helpers.String.SAP.Trim(item_1.STRAS));
                    Data.SetValue("VendorCity__", Sys.Helpers.String.SAP.Trim(item_1.ORT01));
                    Data.SetValue("VendorZipCode__", Sys.Helpers.String.SAP.Trim(item_1.PSTLZ));
                    Data.SetValue("VendorRegion__", Sys.Helpers.String.SAP.Trim(item_1.REGIO));
                    Data.SetValue("VendorCountry__", Sys.Helpers.String.SAP.Trim(item_1.LAND1));
                    Data.SetValue("PaymentTerms__", Sys.Helpers.String.SAP.Trim(item_1.ZTERM));
                    if (Sys.Parameters.GetInstance("AP").GetParameter("TaxesWithholdingTax", "") === "Basic") {
                        Data.SetValue("WithholdingTax__", Sys.Helpers.String.SAP.Trim(item_1.QSSKZ));
                    }
                    else if (Sys.Parameters.GetInstance("AP").GetParameter("TaxesWithholdingTax", "") === "Extended") {
                        Lib.AP.GetInvoiceDocument().GetExtendedWithholdingTax(item_1.LIFNR);
                    }
                    var customFields = Sys.Helpers.TryCallFunction("Lib.AP.Customization.Common.GetVendorCustomFields");
                    if (customFields) {
                        Sys.Helpers.Array.ForEach(customFields, function (field) {
                            if (field.nameInSAP) {
                                Data.SetValue(field.nameInForm, Sys.Helpers.String.SAP.Trim(item_1[field.nameInSAP]));
                            }
                        });
                    }
                    Lib.AP.Extraction.FillVendorContactEmail();
                }
            }
            SAP.FillVendorDetailsFromSAPResult = FillVendorDetailsFromSAPResult;
            function GetDuplicateFIInvoice(params, duplicateCandidates, companyCode, normalizedInvoiceNumber, normalizedVendorNumber, normalizedInvoiceDate, invoiceCurrency, normalizedInvoiceAmount) {
                var filter = Lib.AP.SAP.GetDuplicateFIInvoiceFilter(companyCode, normalizedInvoiceNumber, normalizedVendorNumber, normalizedInvoiceDate, invoiceCurrency, normalizedInvoiceAmount);
                if (!Lib.AP.InvoiceType.isPOInvoice() || (normalizedInvoiceNumber && normalizedInvoiceAmount)) {
                    // First we get all duplicate FI invoices
                    var result = Sys.Helpers.SAP.ReadSAPTable(params.GetBapi("RFC_READ_TABLE"), "BSIP", "BELNR|BUKRS|GJAHR", filter, 0, 0, false, { "useCache": false });
                    if (result && result.length > 0) {
                        // Then we filter to get only duplicate FI invoices which are not reversed
                        filter = Lib.AP.SAP.GetDuplicateFIInvoiceNotReversedFilter(result, companyCode, normalizedInvoiceNumber, normalizedInvoiceDate, invoiceCurrency);
                        var resultBkpf = Sys.Helpers.SAP.ReadSAPTable(params.GetBapi("RFC_READ_TABLE"), "BKPF", "BELNR|BUKRS|GJAHR|AWKEY", filter, 0, 0, false, { "useCache": false });
                        if (resultBkpf && resultBkpf.length > 0) {
                            // Then we get the duplicated MM invoices which are reversed and match the list of duplicate FI invoices which are not reversed
                            filter = Lib.AP.SAP.GetDuplicateReversedMMInvoicesFilter(resultBkpf, companyCode);
                            var resultRbkp = Sys.Helpers.SAP.ReadSAPTable(params.GetBapi("RFC_READ_TABLE"), "RBKP", "BELNR|BUKRS|GJAHR", filter, 0, 0, false, { "useCache": false });
                            // And we filter the list of FI invoices to remove the ones matching a reversed MM invoice (because it means the FI invoice is reversed too)
                            if (resultRbkp && resultRbkp.length > 0) {
                                Lib.AP.SAP.FilterResultsOnAWKEY(resultBkpf, resultRbkp);
                            }
                            Lib.AP.SAP.AddDuplicateCandidates(resultBkpf, duplicateCandidates);
                        }
                    }
                }
            }
            SAP.GetDuplicateFIInvoice = GetDuplicateFIInvoice;
            function GetDuplicateMMInvoice(params, duplicateCandidates, companyCode, normalizedInvoiceNumber, normalizedVendorNumber, normalizedInvoiceDate, invoiceCurrency, normalizedInvoiceAmount) {
                var filter = Lib.AP.SAP.GetDuplicateMMInvoiceFilter(companyCode, normalizedInvoiceNumber, normalizedVendorNumber, normalizedInvoiceDate, invoiceCurrency, normalizedInvoiceAmount);
                if (!Lib.AP.InvoiceType.isPOInvoice() || (normalizedInvoiceNumber && normalizedInvoiceAmount)) {
                    var result = Sys.Helpers.SAP.ReadSAPTable(params.GetBapi("RFC_READ_TABLE"), "RBKP", "BELNR|BUKRS|GJAHR", filter, 0, 0, false, { "useCache": false });
                    Lib.AP.SAP.AddDuplicateCandidates(result, duplicateCandidates);
                }
            }
            SAP.GetDuplicateMMInvoice = GetDuplicateMMInvoice;
            function GetDuplicateFIParkedInvoice(params, duplicateCandidates, companyCode, normalizedInvoiceNumber, normalizedVendorNumber, normalizedInvoiceDate, invoiceCurrency) {
                // use inv # if it exists, otherwise do not run query
                if (normalizedInvoiceNumber) {
                    var filter = "BUKRS = '" + companyCode + "'";
                    filter += "\n AND BSTAT = 'V'";
                    filter += "\n AND BLDAT = '" + normalizedInvoiceDate + "'";
                    filter += "\n AND WAERS = '" + invoiceCurrency + "'";
                    filter += "\n AND XBLNR = '" + normalizedInvoiceNumber + "'";
                    var resultVBKPF = Sys.Helpers.SAP.ReadSAPTable(params.GetBapi("RFC_READ_TABLE"), "VBKPF", "WAERS|BELNR|BUKRS|GJAHR", filter, 0, 0, false, { "useCache": false });
                    if (resultVBKPF && resultVBKPF.length > 0) {
                        filter = Lib.AP.SAP.GetDuplicateFIparkedInvoiceFilter(resultVBKPF, companyCode, normalizedInvoiceNumber, normalizedVendorNumber, normalizedInvoiceDate);
                        var resultVBSEGK = Sys.Helpers.SAP.ReadSAPTable(params.GetBapi("RFC_READ_TABLE"), "VBSEGK", "BELNR|BUKRS|GJAHR", filter, 0, 0, false, { "useCache": false });
                        Lib.AP.SAP.AddDuplicateCandidates(resultVBSEGK, duplicateCandidates);
                    }
                }
            }
            SAP.GetDuplicateFIParkedInvoice = GetDuplicateFIParkedInvoice;
            function GetDuplicateInvoiceServer(companyCode, invoiceType, invoiceNumber, vendorNumber, invoiceDate, invoiceAmount, invoiceCurrency) {
                var duplicateCandidates = [];
                if (companyCode && vendorNumber && invoiceDate && (invoiceAmount || invoiceNumber) && invoiceCurrency && invoiceType) {
                    var normalizedVendorNumber = Sys.Helpers.String.SAP.NormalizeID(vendorNumber, 10);
                    var normalizedInvoiceNumber = invoiceNumber ? Lib.AP.SAP.escapeToSQL(invoiceNumber.toUpperCase().substr(0, 16)) : null;
                    var normalizedInvoiceDate = Sys.Helpers.SAP.FormatToSAPDateTimeFormat(invoiceDate);
                    var normalizedInvoiceAmount = invoiceAmount ? invoiceAmount.toFixed(2) : null;
                    var params = Lib.AP.SAP.PurchaseOrder.GetBapiParameters();
                    if (params) {
                        // 1- Search in posted documents (FI)
                        Lib.AP.SAP.GetDuplicateFIInvoice(params, duplicateCandidates, companyCode, normalizedInvoiceNumber, normalizedVendorNumber, normalizedInvoiceDate, invoiceCurrency, normalizedInvoiceAmount);
                        // 2- Search for invoice receipts documents (MM)
                        Lib.AP.SAP.GetDuplicateMMInvoice(params, duplicateCandidates, companyCode, normalizedInvoiceNumber, normalizedVendorNumber, normalizedInvoiceDate, invoiceCurrency, normalizedInvoiceAmount);
                        // 3 - Search for parked documents (FI)
                        Lib.AP.SAP.GetDuplicateFIParkedInvoice(params, duplicateCandidates, companyCode, normalizedInvoiceNumber, normalizedVendorNumber, normalizedInvoiceDate, invoiceCurrency);
                    }
                }
                return duplicateCandidates;
            }
            SAP.GetDuplicateInvoiceServer = GetDuplicateInvoiceServer;
            SAP.externalCurrencyFactors = {};
            function GetExternalCurrencyFactor(params, currency) {
                if (!Lib.AP.SAP.externalCurrencyFactors[currency]) {
                    Lib.AP.SAP.externalCurrencyFactors[currency] = 1;
                    var customExternalFactors = Sys.Helpers.TryCallFunction("Lib.AP.Customization.Common.SAPCurrenciesExternalFactors");
                    if (customExternalFactors && customExternalFactors[currency]) {
                        Lib.AP.SAP.externalCurrencyFactors[currency] = customExternalFactors[currency];
                    }
                    else if (params && params.GetBapi("RFC_READ_TABLE")) {
                        //Query table TCURX
                        var bapi = params.GetBapi("RFC_READ_TABLE");
                        var filter = "CURRKEY = '" + currency + "'";
                        var result = Sys.Helpers.SAP.ReadSAPTable(bapi, "TCURX", "CURRDEC", filter, 1, 0, false);
                        if (result && result.length > 0) {
                            var nbDecimals = Sys.Helpers.String.SAP.Trim(result[0].CURRDEC);
                            Lib.AP.SAP.externalCurrencyFactors[currency] = Sys.Helpers.String.SAP.ConvertDecimalToExternalFactor(nbDecimals);
                        }
                    }
                    else {
                        Log.Warn("Bapi not initialized to retrieve sap external factor for currency " + currency + ", 1 will be used as default");
                    }
                }
                return Lib.AP.SAP.externalCurrencyFactors[currency];
            }
            SAP.GetExternalCurrencyFactor = GetExternalCurrencyFactor;
        })(SAP = AP.SAP || (AP.SAP = {}));
    })(AP = Lib.AP || (Lib.AP = {}));
})(Lib || (Lib = {}));
