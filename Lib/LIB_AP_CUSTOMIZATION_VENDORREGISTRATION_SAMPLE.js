/* eslint no-empty-function: "off", no-unused-vars: "off" */
/**
 * @file Lib.AP.Customization.VendorRegistration library
 * @namespace Lib.AP.Customization.VendorRegistration
 */

// eslint-disable-next-line no-redeclare
var Lib = Lib || {};
Lib.AP = Lib.AP || {};
Lib.AP.Customization = Lib.AP.Customization || {};

(function (parentLib)
{
	/**
	 * @lends Lib.AP.Customization.VendorRegistration
	 */
	parentLib.VendorRegistration =
	{
		/**
		* Function called in Vendor registration to customize available values
		* in DocumentType Combobox
		*
		* @memberof Lib.AP.Customization.VendorRegistration
		* @returns {array} result an Array of strings which represents availables values for document type, in the value=label format.
		* @example
		* <pre><code>
		* DocumentTypeAvailableValues: function()
		* {
		*	return ["KBIS=_KBIS", "RIB=_RIB", "Other=_Other"];
		* }
		* </code></pre>
		*/
		DocumentTypeAvailableValues: function()
		{
			return [];
		},

		/**
		* Function called in Vendor registration when OFAC verification is enabled
		* This specify the minimum scoring for alerting user
		* A message will be displayed in the form if a match is found with a score greater than minScore
		*
		* The score is a percentage of match between research and individual / entity found by OFAC
		* The score must be between 50 and 100 (default value is 80)
		* 100 : perfect match
		* 50 : approximate match
		*
		* @memberof Lib.AP.Customization.VendorRegistration
		* @returns {number} The minimum scoring for alerting user of match in OFAC list
		* @example
		* <pre><code>
		* GetOFACMinScore: function()
		* {
		*	return 80;
		* }
		* </code></pre>
		*/
		GetOFACMinScore: function()
		{
		}
	};
})(Lib.AP.Customization);
