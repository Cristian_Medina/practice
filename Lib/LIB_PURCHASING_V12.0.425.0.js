/* eslint-disable dot-notation */
///#GLOBALS Lib Sys
/* LIB_DEFINITION{
  "name": "Lib_Purchasing",
  "libraryType": "LIB",
  "scriptType": "COMMON",
  "comment": "Purchasing library",
  "require": [
    "Lib_V12.0.425.0",
    "Lib_P2P_V12.0.425.0",
    "Lib_P2P_CompanyCodesValue_V12.0.425.0",
    "Lib_CommonDialog_V12.0.425.0",
    "Lib_Parameters_P2P_V12.0.425.0",
    "[Lib_Custom_Parameters]",
    "Sys/Sys_Parameters",
    "Sys/Sys_Decimal",
    "Sys/Sys_Helpers_Date",
    "Sys/Sys_Helpers",
    "Sys/Sys_Helpers_String",
    "Sys/Sys_Helpers_Object",
    "Sys/Sys_Helpers_Data",
    "[Lib_Workflow_Customization_Common]",
    "[Lib_PO_Customization_Common]"
  ]
}*/
/**
* helpers for purchasing package
* @namespace Lib.Purchasing
*/
var Lib;
(function (Lib) {
    var Purchasing;
    (function (Purchasing) {
        var g = Sys.Helpers.Globals; // STD Globals Object
        Purchasing.AddLib = Lib.AddLib;
        Purchasing.ExtendLib = Lib.ExtendLib;
        //////////////////////////////////////////
        // Private members for PO generation
        //////////////////////////////////////////
        var POCSVMapping = {
            header: {
                "PODate": function (info) {
                    return Sys.Helpers.Date.ToLocaleDate(new Date(), info.culture);
                },
                "VendorNumber": "VendorNumber__",
                "VendorName": "VendorName__",
                "VendorAddress": "VendorAddress__",
                "ShipToCompany": "ShipToCompany__",
                "ShipToContact": "ShipToContact__",
                "ShipToAddress": "ShipToAddress__",
                "PO_number": function (info) {
                    return info.poNumber;
                },
                "Currency": "Currency__",
                "TotalNetAmount": "TotalNetAmount__",
                "TotalTaxAmount": "TotalTaxAmount__",
                "Total": "TotalAmountIncludingVAT__",
                "PaymentAmount": "PaymentAmount__",
                "BuyerComment": "BuyerComment__",
                "PaymentTerm": "PaymentTermCode__",
                "PaymentTermDescription": "PaymentTermDescription__",
                "PaymentMethod": "PaymentMethodCode__",
                "PaymentMethodDescription": "PaymentMethodDescription__",
                "ValidityStart__": "ValidityStart__",
                "ValidityEnd__": "ValidityEnd__"
            },
            items: {
                table: "LineItems__",
                fields: {
                    "ItemNumber": "ItemNumber__",
                    "Description": "ItemDescription__",
                    "Quantity": "ItemQuantity__",
                    "RequestedDeliveryDate": "ItemRequestedDeliveryDate__",
                    "UnitPrice": "ItemUnitPrice__",
                    "NetAmount": "ItemNetAmount__",
                    "TaxCode": "ItemTaxCode__",
                    "TaxRate": "ItemTaxRate__",
                    "TaxAmount": "ItemTaxAmount__",
                    "ItemShipToCompany": "ItemShipToCompany__",
                    "ItemShipToAddress": "ItemShipToAddress__"
                }
            }
        };
        // These fields are emptied in multiship mode because they are added on each item
        if (Sys.Parameters.GetInstance("PAC").GetParameter("MultiShipTo", false)) {
            POCSVMapping.header.ShipToCompany = "";
            POCSVMapping.header.ShipToAddress = "";
        }
        /**
        * Object use to manage parameter UndefinedBudgetBehavior
        */
        Purchasing.UndefinedBudgetBehavior = {
            // Current enumeration
            Allow: "allow",
            Warn: "warn",
            Prevent: "prevent",
            // Enumeration for retro-compatibility
            Old: {
                Allow: "0",
                Warn: "1",
                Prevent: "2"
            },
            /**
            * Returns the current value of the parameter UndefinedBudgetBehavior (either in Lib_Parameters_P2P for new documents or on record).
            * @return {string} value of UndefinedBudgetBehavior (in lower case)
            */
            Get: Sys.Helpers.Object.ConstantGetter(function () {
                return (Sys.Parameters.GetInstance("PAC").GetParameter("UndefinedBudgetBehavior") || Purchasing.UndefinedBudgetBehavior.Prevent).toLowerCase();
            }),
            /**
            * Determines if the specified value of UndefinedBudgetBehavior is the current one.
            * Support both new and old enumerations
            * @param {string} expected value of UndefinedBudgetBehavior (case sensitive)
            * @return {boolean}
            */
            Is: function (expectedBehavior) {
                var behavior = this.Get();
                return behavior === this[expectedBehavior] || behavior === this.Old[expectedBehavior];
            },
            IsAllowed: function () { return this.Is("Allow"); },
            IsWarned: function () { return this.Is("Warn"); },
            IsPrevented: function () { return this.Is("Prevent"); }
        };
        /**
        * Object use to manage parameter OutOfBudgetBehavior
        */
        Purchasing.OutOfBudgetBehavior = {
            // Current enumeration
            Allow: "allow",
            Warn: "warn",
            Prevent: "prevent",
            /**
            * Returns the current value of the parameter OutOfBudgetBehavior (either in Lib_Parameters_P2P for new documents or on record).
            * @return {string} value of OutOfBudgetBehavior (in lower case)
            */
            Get: Sys.Helpers.Object.ConstantGetter(function () {
                return (Sys.Parameters.GetInstance("PAC").GetParameter("OutOfBudgetBehavior") || Purchasing.OutOfBudgetBehavior.Warn).toLowerCase();
            }),
            /**
            * Determines if the specified value of OutOfBudgetBehavior is the current one.
            * Support both new and old enumerations
            * @param {string} expected value of OutOfBudgetBehavior (case sensitive)
            * @return {boolean}
            */
            Is: function (expectedBehavior) {
                var behavior = this.Get();
                return behavior === this[expectedBehavior];
            },
            IsAllowed: function () { return this.Is("Allow"); },
            IsWarned: function () { return this.Is("Warn"); },
            IsPrevented: function () { return this.Is("Prevent"); }
        };
        /**
        * Create a next alert to advise user when we try to execute an unknown action.
        * Process will be in validation state and wait for an action of user.
        * @param {string} currentAction name of the executed action
        * @param {string} currentName sub-name of the executed action
        */
        function OnUnknownAction(currentAction, currentName) {
            OnUnexpectedError("Ignoring unknown action " + currentAction + "-" + currentName);
        }
        Purchasing.OnUnknownAction = OnUnknownAction;
        /**
        * Create a next alert to advise user when we have any issue.
        * Process will be in validation state and wait for an action of user.
        * @param {string} err message of error
        */
        function OnUnexpectedError(err) {
            Log.Error(err);
            Lib.CommonDialog.NextAlert.Define("_Unexpected error", "_Unexpected error message", { isError: true, behaviorName: "onUnexpectedError" });
            if (Sys.ScriptInfo.IsServer()) {
                g.Process.PreventApproval();
            }
        }
        Purchasing.OnUnexpectedError = OnUnexpectedError;
        /**
        * Remove error on each field in error.
        * For instance, this method is only implemented in client side.
        */
        function ResetAllFieldsInError() {
            if (Sys.ScriptInfo.IsClient()) {
                Sys.Helpers.Object.ForEach(g.Controls, function (control) {
                    // header fields
                    if (Sys.Helpers.IsFunction(control.GetError) && control.GetError()) {
                        control.SetError("");
                    }
                    // Table case
                    if (Sys.Helpers.IsFunction(control.GetLineCount)) {
                        // retrieve column names
                        var nbColumns = control.GetColumnCount();
                        var columnsNames = [];
                        for (var i = 1; i <= nbColumns; i++) {
                            columnsNames.push(control.GetColumnControl(i).GetName());
                        }
                        // reset on Data rather than TableRow because we want reset every line (visible and not visible)
                        var nbItems = control.GetItemCount();
                        var _loop_1 = function (i) {
                            var item = control.GetItem(i);
                            columnsNames.forEach(function (fieldName) {
                                if (item.GetError(fieldName)) {
                                    item.SetError(fieldName, "");
                                }
                            });
                        };
                        for (var i = 0; i < nbItems; i++) {
                            _loop_1(i);
                        }
                    }
                });
            }
        }
        Purchasing.ResetAllFieldsInError = ResetAllFieldsInError;
        /**
        * Remove error or warning on each cell of column of table.
        */
        function ResetAllCellsInState(tableName, columnName, state) {
            var table = Data.GetTable(tableName);
            var nItems = table.GetItemCount();
            for (var itemIdx = 0; itemIdx < nItems; itemIdx++) {
                var item = table.GetItem(itemIdx);
                item["Set" + state].call(item, columnName, "");
            }
        }
        Purchasing.ResetAllCellsInState = ResetAllCellsInState;
        function SetDocumentReadonlyAndHideAllButtonsExceptQuit(quitButtonName) {
            if (Sys.ScriptInfo.IsClient()) {
                Sys.Helpers.Object.ForEach(g.Controls, function (control) {
                    var type = control.GetType();
                    if (type.startsWith("Panel")) {
                        control.SetReadOnly(true);
                    }
                    else if (type === "FormButton") {
                        control.SetDisabled(true);
                    }
                    else if (type === "SubmitButton") {
                        control.Hide(true);
                    }
                });
                quitButtonName = quitButtonName || "Close";
                var quitButton = g.Controls[quitButtonName];
                if (quitButton) {
                    quitButton.SetDisabled(false);
                    quitButton.Hide(false);
                }
                else {
                    Log.Error("Unable to find the quit button named: " + quitButtonName);
                }
            }
        }
        Purchasing.SetDocumentReadonlyAndHideAllButtonsExceptQuit = SetDocumentReadonlyAndHideAllButtonsExceptQuit;
        Purchasing.roleRequester = "_Role requester";
        Purchasing.roleBuyer = "_Role buyer";
        Purchasing.roleReviewer = "_Role reviewer";
        Purchasing.roleApprover = "_Role approver";
        Purchasing.roleReceiver = "_Role receiver";
        Purchasing.roleAdvisor = "_Role advisor";
        Purchasing.roleTreasurer = "_Role treasurer";
        Purchasing.sequenceRoleRequester = "requester";
        Purchasing.sequenceRoleBuyer = "buyer";
        Purchasing.sequenceRoleReviewer = "reviewer";
        Purchasing.sequenceRoleApprover = "approver";
        Purchasing.sequenceRoleReceiver = "receiver";
        Purchasing.sequenceRoleAdvisor = "advisor";
        Purchasing.sequenceRoleTreasurer = "treasurer";
        Purchasing.delimitersThousandsLocales = {
            // default works for en-US, en-GB, it-IT
            "default": ",",
            "fr-FR": " ",
            "fr-CH": "'",
            "de-DE": ".",
            "es-ES": "."
        };
        Purchasing.delimitersDecimalLocales = {
            // default works for en-US, en-GB, it-IT, fr-CH
            "default": ".",
            "fr-FR": ",",
            "de-DE": ",",
            "es-ES": ","
        };
        function CreatePOCsv(poNumber) {
            // Creating dynamically the PO csv.
            // Takes into account, user's culture and an user exit.
            var extraFields = Sys.Helpers.TryCallFunction("Lib.PO.Customization.Common.GetExtraPOCSVFields");
            if (extraFields) {
                Sys.Helpers.Extend(true, POCSVMapping, extraFields);
            }
            var val = Lib.Purchasing.GetValueAsString;
            var culture = Sys.ScriptInfo.IsClient() ? g.User.culture : Lib.P2P.GetValidatorOrOwner().GetValue("Culture");
            var info = {
                culture: culture,
                poNumber: poNumber
            };
            var csvHeader = [];
            var csvDataForHeaderFields = [];
            var csvDataForItemsFields = [];
            if (POCSVMapping.header) {
                Sys.Helpers.Object.ForEach(POCSVMapping.header, function (data, field) {
                    csvHeader.push(field);
                    var dataValue = Sys.Helpers.IsFunction(data) ? data(info) : val(g.Data, data, culture);
                    csvDataForHeaderFields.push(dataValue);
                });
            }
            if (POCSVMapping.items) {
                // feed csvHeader
                Sys.Helpers.Object.ForEach(POCSVMapping.items.fields || {}, function (_, field) {
                    csvHeader.push(field);
                });
                // feed csvData
                var tableName = POCSVMapping.items.table || "LineItems__";
                var table = Data.GetTable(tableName);
                Lib.Purchasing.RemoveEmptyLineItem(table);
                var nItems = table.GetItemCount();
                var _loop_2 = function (itemIdx) {
                    var item = table.GetItem(itemIdx);
                    var itemInfo = Sys.Helpers.Extend(true, {}, info, {
                        tableName: tableName,
                        table: table,
                        itemIdx: itemIdx,
                        item: item
                    });
                    var csvDataForItemIdxFields = [];
                    csvDataForItemsFields.push(csvDataForItemIdxFields);
                    Sys.Helpers.Object.ForEach(POCSVMapping.items.fields || {}, function (data) {
                        var dataValue = Sys.Helpers.IsFunction(data) ? data(itemInfo) : val(item, data, culture);
                        csvDataForItemIdxFields.push(dataValue);
                    });
                    // feed intrinsic csv columns in data
                    csvDataForItemIdxFields.push(itemIdx === nItems - 1 ? "1" : "");
                };
                for (var itemIdx = 0; itemIdx < nItems; itemIdx++) {
                    _loop_2(itemIdx);
                }
            }
            // intrinsic csv columns in header
            csvHeader.push("LastRecord");
            var csv = Lib.Purchasing.BuildCSVHeader(csvHeader);
            var csvFixed = Lib.Purchasing.BuildCSVLine(csvDataForHeaderFields);
            csvDataForItemsFields.forEach(function (fields) {
                csv += csvFixed + ";" + Lib.Purchasing.BuildCSVLine(fields) + "\n";
            });
            return csv;
        }
        Purchasing.CreatePOCsv = CreatePOCsv;
        function FeedJsonWithCustomTable(tableName, tableValues, info) {
            var result = [];
            var table = Data.GetTable(tableName);
            if (tableName === "LineItems__") {
                Lib.Purchasing.RemoveEmptyLineItem(table);
            }
            var nItems = table.GetItemCount();
            var _loop_3 = function (itemIdx) {
                var item = table.GetItem(itemIdx);
                var itemInfo = Sys.Helpers.Extend(true, {}, info, {
                    tableName: tableName,
                    table: table,
                    itemIdx: itemIdx,
                    item: item
                });
                // build a json for the current line item
                var jsonDataForItemIdxFields = {};
                // loop on all columns of the line item
                Sys.Helpers.Object.ForEach(tableValues.fields || {}, function (data) {
                    // keep original variable type
                    var dataValue = Sys.Helpers.IsFunction(data) ? data(itemInfo) : item.GetValue(data);
                    // except for Date type where we always set as a string with "YYYY-MM-DD" format
                    if (dataValue instanceof Date) {
                        dataValue = Sys.Helpers.Date.ToUTCDate(dataValue);
                    }
                    jsonDataForItemIdxFields[data] = dataValue;
                });
                result.push(jsonDataForItemIdxFields);
            };
            for (var itemIdx = 0; itemIdx < nItems; itemIdx++) {
                _loop_3(itemIdx);
            }
            return result;
        }
        Purchasing.FeedJsonWithCustomTable = FeedJsonWithCustomTable;
        function LoadParameters() {
            // !!! Add used parameters libs (Lib_Parameters_P2P, Lib_Custom_Parameters) in LIB_DEFINITION.required
            // to ensure that the instance names are registed in Sys.Parameters.
            return Sys.Parameters.IsReady();
        }
        Purchasing.LoadParameters = LoadParameters;
        function SetERPByCompanyCode(companyCode) {
            return Lib.P2P.CompanyCodesValue.QueryValues(companyCode).Then(function (CCValues) {
                return Sys.Helpers.Promise.Create(function (resolve) {
                    if (Object.keys(CCValues).length > 0) {
                        Sys.Parameters.GetInstance("PAC").Reload(CCValues.DefaultConfiguration__);
                        Sys.Parameters.GetInstance("AP").Reload(CCValues.DefaultConfiguration__);
                    }
                    else {
                        Log.Error("The requested company code is not in the company code table.");
                    }
                    Sys.Parameters.GetInstance("PAC").IsReady(function () {
                        Lib.ERP.InitERPName(null, true, "PAC");
                        Sys.Parameters.GetInstance("AP").IsReady(function () {
                            Lib.ERP.InitERPName(null, true, "AP");
                            resolve();
                        });
                    });
                });
            });
        }
        Purchasing.SetERPByCompanyCode = SetERPByCompanyCode;
        function InitTechnicalFields() {
            // Serialize ERP
            Lib.ERP.InitERPName(null, false, "PAC");
            // Serialize Parameters for PAC instance
            Sys.Parameters.GetInstance("P2P").Serialize();
            Sys.Parameters.GetInstance("PAC").Serialize();
            // Set form validity date (to prevent expiration errors)
            var numberOfMonthToAdd = parseInt(Sys.Parameters.GetInstance("PAC").GetParameter("ValidityDurationInMonths"), 10), submitDT = Data.GetValue("SubmitDateTime");
            if (submitDT) {
                submitDT.setMonth(submitDT.getMonth() + numberOfMonthToAdd);
                var validityDateTime = Data.GetValue("ValidityDateTime");
                var validityDT = Sys.Helpers.IsString(validityDateTime) ? new Date(validityDateTime) : validityDateTime;
                if (validityDT) {
                    submitDT = Sys.Helpers.Date.CompareDate(submitDT, validityDT) > 0 ? submitDT : validityDT;
                }
                Data.SetValue("ValidityDateTime", submitDT);
            }
            // Override the form archive duration of the process when a value is defined (may differ according to the environments)
            var archiveDuration = Sys.Parameters.GetInstance("PAC").GetParameter("ArchiveDurationInMonths", null);
            if (archiveDuration !== null) {
                Data.SetValue("ArchiveDuration", archiveDuration);
            }
        }
        Purchasing.InitTechnicalFields = InitTechnicalFields;
        /**
        * Formats a number with a template.
        * In the template, the string "$seq$" is replaced by the given number with 5 figures, padded with zeroes
        * Exemple: FormatSequenceNumber("1234$seq$", 56) => "123400056"
        */
        function FormatSequenceNumber(sequenceNumber, templateStringName) {
            return Sys.Parameters.GetInstance("PAC").GetParameter(templateStringName).replace("$seq$", Sys.Helpers.String.PadLeft(sequenceNumber, "0", 5));
        }
        Purchasing.FormatSequenceNumber = FormatSequenceNumber;
        function FormatNumber(value, format, delimitersThousands, delimitersDecimal) {
            var negP = false, signed = false, neg = false;
            // see if we should use parentheses for negative number or if we should prefix with a sign
            // if both are present we default to parentheses
            if (format.indexOf("(") > -1) {
                negP = true;
                format = format.slice(1, -1);
            }
            else if (format.indexOf("+") > -1) {
                signed = true;
                format = format.replace(/\+/g, "");
            }
            var d = "";
            var w = value.toString().split(".")[0];
            var precision = format.split(".")[1];
            var thousands = format.indexOf(",");
            if (precision) {
                d = value.toFixed(precision.length);
                w = d.split(".")[0];
                if (d.split(".")[1].length) {
                    d = delimitersDecimal + d.split(".")[1];
                }
                else {
                    d = "";
                }
            }
            else {
                w = value.toFixed();
            }
            // format number
            if (w.indexOf("-") > -1) {
                w = w.slice(1);
                neg = true;
            }
            if (thousands > -1) {
                w = w.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1" + delimitersThousands);
            }
            if (format.indexOf(".") === 0) {
                w = "";
            }
            return (negP && neg ? "(" : "") + (!negP && neg ? "-" : "") + (!neg && signed ? "+" : "") + w + d + (negP && neg ? ")" : "");
        }
        Purchasing.FormatNumber = FormatNumber;
        function NextNumber(type, fieldNumber) {
            var errorMsg = {
                "PR": "_Error while retrieving a new requisition number",
                "PO": "_Error while retrieving a new PO number",
                "GR": "_Error while retrieving a new GR number"
            };
            var sequenceName = {
                "PR": "ReqNum",
                "PO": "ReqNumPO",
                "GR": "ReqNumGR"
            };
            var numberFormat = {
                "PR": "NumberFormat",
                "PO": "NumberFormatPO",
                "GR": "NumberFormatGR"
            };
            var sNumber = "";
            var GetNumberFunc = Sys.Helpers.TryGetFunction("Lib." + type + ".Customization.Server.GetNumber");
            if (GetNumberFunc) {
                sNumber = GetNumberFunc(sequenceName[type]);
                if (!sNumber) {
                    Data.SetError(fieldNumber, errorMsg[type]);
                    Log.Info("Error while retrieving a new " + type + " number");
                }
                else {
                    Log.Info(type + " number:" + sNumber);
                }
            }
            else {
                var ReqNumberSequence = Process.GetSequence(sequenceName[type]);
                sNumber = ReqNumberSequence.GetNextValue();
                if (sNumber === "") {
                    Data.SetError(fieldNumber, errorMsg[type]);
                    Log.Info("Error while retrieving a new " + type + " number");
                }
                else {
                    sNumber = Lib.Purchasing.FormatSequenceNumber(sNumber, numberFormat[type]);
                    Log.Info(type + " number:" + sNumber);
                }
            }
            return sNumber;
        }
        Purchasing.NextNumber = NextNumber;
        function NumberToLocaleString(value, format, locales) {
            // eslint-disable-next-line dot-notation
            var delimitersThousands = String(this.delimitersThousandsLocales[locales] || this.delimitersThousandsLocales["default"]);
            // eslint-disable-next-line dot-notation
            var delimitersDecimal = String(this.delimitersDecimalLocales[locales] || this.delimitersDecimalLocales["default"]);
            return Lib.Purchasing.FormatNumber(value, format, delimitersThousands, delimitersDecimal);
        }
        Purchasing.NumberToLocaleString = NumberToLocaleString;
        function BuildCSVHeader(headers) {
            return headers.join(";") + "\n";
        }
        Purchasing.BuildCSVHeader = BuildCSVHeader;
        function BuildCSVLine(items) {
            return items.map(function (entry) {
                if (entry) {
                    return '"' + Sys.Helpers.String.EscapeValueForCSV(entry.toString()) + '"';
                }
                return '""';
            }).join(";");
        }
        Purchasing.BuildCSVLine = BuildCSVLine;
        function GetNumberAsString(value, culture, precision) {
            precision = Sys.Helpers.IsDefined(precision) ? precision : 2;
            // change format string according to the precision
            var fmt = "000,000." + Sys.Helpers.String.RepeatChar("0", precision);
            return Lib.Purchasing.NumberToLocaleString(value, fmt, culture);
        }
        Purchasing.GetNumberAsString = GetNumberAsString;
        function GetValueAsString(item, field, culture) {
            var value = item.GetValue(field);
            if (value instanceof Date) {
                return Sys.Helpers.Date.ToLocaleDate(value, culture);
            }
            if (typeof value === "number") {
                var precision = // let it undefined - default value will be set by GetNumberAsString function
                 void 0; // let it undefined - default value will be set by GetNumberAsString function
                if (Sys.ScriptInfo.IsServer()) {
                    var properties = item.GetProperties(field);
                    if (properties) {
                        precision = properties.precision;
                    }
                }
                return Lib.Purchasing.GetNumberAsString(value, culture, precision);
            }
            if (typeof value === "string") {
                return value;
            }
            return value;
        }
        Purchasing.GetValueAsString = GetValueAsString;
        function RemoveEmptyLineItem(table) {
            for (var i = 0; i < table.GetItemCount(); i++) {
                var row = table.GetItem(i);
                if (Lib.Purchasing.IsLineItemEmpty(row)) {
                    if (typeof row.Remove === "undefined") {
                        row.RemoveItem();
                    }
                    else {
                        row.Remove();
                    }
                }
            }
        }
        Purchasing.RemoveEmptyLineItem = RemoveEmptyLineItem;
        function GetLanguageFromCompanyCode(companyCode) {
            if (companyCode && companyCode.startsWith("FR")) {
                return "fr";
            }
            return "en";
        }
        Purchasing.GetLanguageFromCompanyCode = GetLanguageFromCompanyCode;
        //Keep It Here for backward compatibility
        function GetOwner() {
            return Lib.P2P.GetOwner();
        }
        Purchasing.GetOwner = GetOwner;
        //Keep It Here for backward compatibility
        function GetValidator() {
            return Lib.P2P.GetValidator();
        }
        Purchasing.GetValidator = GetValidator;
        //Keep It Here for backward compatibility
        function GetValidatorOrOwner() {
            return Lib.P2P.GetValidatorOrOwner();
        }
        Purchasing.GetValidatorOrOwner = GetValidatorOrOwner;
        //Keep It Here for backward compatibility
        function GetValidatorOrOwnerLogin() {
            return Lib.P2P.GetValidatorOrOwnerLogin();
        }
        Purchasing.GetValidatorOrOwnerLogin = GetValidatorOrOwnerLogin;
        //Keep It Here for backward compatibility
        function AddOnBehalfOf(currentContributor, comment) {
            return Lib.P2P.AddOnBehalfOf(currentContributor, comment);
        }
        Purchasing.AddOnBehalfOf = AddOnBehalfOf;
        function SetRightForP2PSupervisor() {
            var login = Lib.P2P.ResolveDemoLogin(Sys.Parameters.GetInstance("PAC").GetParameter("P2PSupervisorLogin"));
            if (login) {
                Log.Info("Grant read right to Supervisor: " + login);
                Process.AddRight(login, "read");
            }
        }
        Purchasing.SetRightForP2PSupervisor = SetRightForP2PSupervisor;
        Purchasing.URL = {
            AddParameter: function (url, name, value) {
                return url + (url.indexOf("?") < 0 ? "?" : "&") + encodeURIComponent(name) + "=" + encodeURIComponent(value);
            },
            RemoveParameter: function (url, name) {
                return url.replace(new RegExp("(\\?|&)" + encodeURIComponent(name) + "=([^&]+)(&?)", "gi"), function (match, p1, p2, p3 /*, offset, string*/) {
                    if (p1 === "?") {
                        return p1;
                    }
                    return p3;
                });
            }
        };
        var FieldReseter = /** @class */ (function () {
            function FieldReseter() {
                this.fieldsToReset = [];
            }
            /**
            * Push an item to reset.
            * Item must be
            *	{"table" : "tableName", "field": "fieldName"}
            * OR
            *	{"field": "fieldName"}
            */
            FieldReseter.prototype.push = function (item) {
                if (item.field) {
                    this.fieldsToReset.push(item);
                }
            };
            FieldReseter.prototype.Clean = function () {
                for (var i = 0; i < this.fieldsToReset.length; i++) {
                    if (this.fieldsToReset[i].table) {
                        var table = g.Data.GetTable(this.fieldsToReset[i].table);
                        for (var idx = 0; idx < table.GetItemCount(); idx++) {
                            var row = table.GetItem(idx);
                            row.SetValue(this.fieldsToReset[i].field, null);
                        }
                    }
                    else {
                        g.Data.SetValue(this.fieldsToReset[i].field, null);
                    }
                }
            };
            return FieldReseter;
        }());
        Purchasing.FieldReseter = FieldReseter;
        function IsLineItemEmpty(row) {
            return Sys.Helpers.IsEmpty(row.GetValue("ItemNumber__")) &&
                Sys.Helpers.IsEmpty(row.GetValue("ItemDescription__")) &&
                Sys.Helpers.IsEmpty(row.GetValue("ItemCostCenterId__")) &&
                Sys.Helpers.IsEmpty(row.GetValue("SupplyTypeID__")) &&
                Sys.Helpers.IsEmpty(row.GetValue("ItemShipToAddress__")) &&
                Sys.Helpers.IsEmpty(row.GetValue("VendorNumber__")) &&
                Sys.Helpers.IsEmpty(row.GetValue("CostType__")) &&
                Sys.Helpers.IsEmpty(row.GetValue("ItemRequestedDeliveryDate__")) &&
                row.GetValue("ItemNetAmount__") <= 0 &&
                Sys.Helpers.IsEmpty(row.GetValue("ItemGLAccount__")) &&
                (row.GetValue("ItemType__") !== Lib.P2P.ItemType.SERVICE_BASED ||
                    (Sys.Helpers.IsEmpty(row.GetValue("ItemStartDate__")) &&
                        Sys.Helpers.IsEmpty(row.GetValue("ItemEndDate__"))));
        }
        Purchasing.IsLineItemEmpty = IsLineItemEmpty;
        function IsLineItemsEmpty() {
            var table = Data.GetTable("LineItems__");
            var count = table.GetItemCount();
            return count == 0 || (count == 1 && Lib.Purchasing.IsLineItemEmpty(table.GetItem(0)));
        }
        Purchasing.IsLineItemsEmpty = IsLineItemsEmpty;
        function HasOnlyPunchoutItems() {
            if (Data.GetTable("LineItems__").GetItemCount() === 0) {
                return false;
            }
            return !Sys.Helpers.Data.FindTableItem("LineItems__", function (item) {
                return Sys.Helpers.IsEmpty(item.GetValue("ItemInCxml__")) && !Lib.Purchasing.IsLineItemEmpty(item);
            });
        }
        Purchasing.HasOnlyPunchoutItems = HasOnlyPunchoutItems;
        function HasPunchoutItems() {
            return !!Sys.Helpers.Data.FindTableItem("LineItems__", function (item) {
                return !Sys.Helpers.IsEmpty(item.GetValue("ItemInCxml__"));
            });
        }
        Purchasing.HasPunchoutItems = HasPunchoutItems;
        function AllItemsHaveSameDeliveryAddress() {
            return FirstLineWithDifferentDeliveryAddress() === -1;
        }
        Purchasing.AllItemsHaveSameDeliveryAddress = AllItemsHaveSameDeliveryAddress;
        function FirstLineWithDifferentDeliveryAddress() {
            var table = Data.GetTable("LineItems__");
            var count = table.GetItemCount();
            if (count === 0 || count === 1) {
                return -1;
            }
            var deliveryAddressID = table.GetItem(0).GetValue("ItemDeliveryAddressID__");
            for (var i = 1; i < count; i++) {
                var line = table.GetItem(i);
                if (line.GetValue("ItemDeliveryAddressID__") !== deliveryAddressID) {
                    return i;
                }
            }
            return -1;
        }
        Purchasing.FirstLineWithDifferentDeliveryAddress = FirstLineWithDifferentDeliveryAddress;
        Purchasing.ConversationTypes = {
            GeneralInformation: "10",
            ItemReception: "20",
            Viewed: "30",
            Modified: "40",
            Dialog: "50" // All "human" dialog
        };
        function AddConversationItem(msg, type, notifyByEmail, orderNumber, asUser) {
            var options = {
                ignoreIfExists: false,
                notifyByEmail: notifyByEmail,
                emailTemplate: "Event_MissedPurchasingItem.htm",
                emailCustomTags: null,
                asUser: asUser || null
            };
            if (notifyByEmail) {
                options.emailCustomTags =
                    {
                        OrderNumber__: orderNumber !== null && typeof orderNumber !== "undefined" ? orderNumber : g.Data.GetValue("OrderNumber__"),
                        Message__: msg
                    };
            }
            else {
                delete options.emailCustomTags;
            }
            g.Conversation.AddItem("Conversation__", { Type: type, Message: msg }, options);
        }
        Purchasing.AddConversationItem = AddConversationItem;
        function IsRequester(currentRole) {
            return currentRole === Lib.Purchasing.roleRequester;
        }
        Purchasing.IsRequester = IsRequester;
        function IsApprover(currentRole) {
            return currentRole === Lib.Purchasing.roleApprover;
        }
        Purchasing.IsApprover = IsApprover;
        function ContributorIsCurrentUser(contributor, currentUser) {
            return contributor && Lib.P2P.CurrentUserMatchesLogin(contributor.login, currentUser);
        }
        Purchasing.ContributorIsCurrentUser = ContributorIsCurrentUser;
        /**
         * Return true if the current user is in the reviewal(controller) workflow
         */
        function IsReviewerInWorkflow(workflow, currentUser) {
            var reviewersList = workflow.GetContributorsByRole(Lib.Purchasing.sequenceRoleReviewer);
            var additionalReviewersList = workflow.GetAdditionalContributors();
            additionalReviewersList = Sys.Helpers.Array.Filter(additionalReviewersList, function (contributor) {
                return contributor && contributor.role === Lib.Purchasing.roleReviewer;
            });
            return !!((reviewersList && Sys.Helpers.Array.Find(reviewersList, function (contributor) { return ContributorIsCurrentUser(contributor, currentUser); }))
                || (additionalReviewersList && Sys.Helpers.Array.Find(additionalReviewersList, function (contributor) { return ContributorIsCurrentUser(contributor, currentUser); })));
        }
        Purchasing.IsReviewerInWorkflow = IsReviewerInWorkflow;
        /**
         * Return true if the current user is in the approval workflow
         */
        function IsApproverInWorkflow(workflow, currentUser) {
            var approverList = workflow.GetContributorsByRole("approver");
            var additionalApproversList = workflow.GetAdditionalContributors();
            additionalApproversList = Sys.Helpers.Array.Filter(additionalApproversList, function (contributor) {
                return contributor && contributor.role === Lib.Purchasing.roleApprover;
            });
            return !!((approverList && Sys.Helpers.Array.Find(approverList, function (contributor) { return ContributorIsCurrentUser(contributor, currentUser); }))
                || (additionalApproversList && Sys.Helpers.Array.Find(additionalApproversList, function (contributor) { return ContributorIsCurrentUser(contributor, currentUser); })));
        }
        Purchasing.IsApproverInWorkflow = IsApproverInWorkflow;
        /**
         * Return true if the current user is in the buyers workflow
         */
        function IsBuyerInWorkflow(workflow, currentUser) {
            var buyersList = workflow.GetContributorsByRole("buyer");
            var additionalBuyersList = workflow.GetAdditionalContributors();
            additionalBuyersList = Sys.Helpers.Array.Filter(additionalBuyersList, function (contributor) {
                return contributor && contributor.role === Lib.Purchasing.roleBuyer;
            });
            return !!((buyersList && Sys.Helpers.Array.Find(buyersList, function (contributor) { return ContributorIsCurrentUser(contributor, currentUser); }))
                || (additionalBuyersList && Sys.Helpers.Array.Find(additionalBuyersList, function (contributor) { return ContributorIsCurrentUser(contributor, currentUser); })));
        }
        Purchasing.IsBuyerInWorkflow = IsBuyerInWorkflow;
        function IsMultipleBuyerInWorkflow(workflow) {
            var buyersList = workflow.GetContributorsByRole("buyer");
            // maybe too easy ? All buyer in additionalBuyersList must be in the contributors by role ??
            return buyersList.length > 1;
        }
        Purchasing.IsMultipleBuyerInWorkflow = IsMultipleBuyerInWorkflow;
        Purchasing.RequestedDeliveryDate = {
            IsReadOnly: function (currentRole) {
                return !Lib.Purchasing.IsRequester(currentRole);
            },
            IsRequired: function (currentRole) {
                return currentRole === Lib.Purchasing.roleRequester;
            }
        };
        function QueryVendor(email) {
            var query = g.Process.CreateQueryAsProcessAdmin();
            query.SetSpecificTable("ODUSER");
            var sAccountId = Lib.P2P.GetValidatorOrOwner().GetValue("AccountId");
            var login = g.Variable.GetValueAsString("ContactLogin");
            var queryFilter;
            if (login) {
                queryFilter = "(Login=" + login + ")";
            }
            else {
                queryFilter = "(Login=" + sAccountId + "$" + email.toLowerCase() + ")";
            }
            query.SetFilter(queryFilter);
            if (query.MoveFirst()) {
                return query.MoveNextRecord();
            }
            return null;
        }
        function GetVendorInfo(poData, attributes) {
            var vendorNumber = poData.GetValue("VendorNumber__"); // Field VendorNumber__ in PR and PO
            if (!vendorNumber) {
                // Field VendorNumber__ does not exist in GR
                vendorNumber = g.Variable.GetValueAsString("VendorNumber__");
            }
            if (vendorNumber) {
                var query = Process.CreateQueryAsProcessAdmin();
                query.SetSpecificTable("AP - Vendors__");
                var queryFilter = "(&(" + "Number__=" + vendorNumber + ")(CompanyCode__=" + poData.GetValue("CompanyCode__") + "))";
                query.SetFilter(queryFilter);
                if (query.MoveFirst()) {
                    var record = query.MoveNextRecord();
                    if (record) {
                        var vars = record.GetVars();
                        attributes["Description"] = vars.GetValue_String("Number__", 0);
                        attributes["Company"] = vars.GetValue_String("Name__", 0);
                        attributes["Sub"] = vars.GetValue_String("Sub__", 0);
                        attributes["Street"] = vars.GetValue_String("Street__", 0);
                        attributes["POBox"] = vars.GetValue_String("PostOfficeBox__", 0);
                        attributes["City"] = vars.GetValue_String("City__", 0);
                        attributes["ZipCode"] = vars.GetValue_String("PostalCode__", 0);
                        attributes["MailState"] = vars.GetValue_String("Region__", 0);
                        attributes["FaxNumber"] = vars.GetValue_String("FaxNumber__", 0);
                        attributes["Country"] = vars.GetValue_String("Country__", 0);
                        attributes["PhoneNumber"] = vars.GetValue_String("PhoneNumber__", 0);
                    }
                }
            }
            else {
                // New vendor --> parse VendorAddress__ to get attributes ?
            }
            attributes["TimeZoneIndex"] = Lib.P2P.GetValidatorOrOwner().GetValue("TimeZoneIndex");
            attributes["Language"] = Lib.P2P.GetValidatorOrOwner().GetValue("Language");
            attributes["Culture"] = Lib.P2P.GetValidatorOrOwner().GetValue("Culture");
        }
        function GetVendor(poData, email) {
            var user = QueryVendor(email);
            if (user === null && Sys.Parameters.GetInstance("PAC").GetParameter("AlwaysCreateVendor") == "1") {
                var template = QueryVendor("_vendor_template_");
                var templateUser = g.Users.GetUser(template);
                if (templateUser) {
                    var attributes = { "EmailAddress": email };
                    GetVendorInfo(poData, attributes);
                    templateUser.Clone(email, attributes);
                    user = QueryVendor(email);
                }
            }
            return user;
        }
        Purchasing.GetVendor = GetVendor;
        /**
        * This method calculates either the payment amount or the payment percentage from the defined input (either paymentPercent or paymentAmount)
        * and the downPaymentLocalCurrency.
        * If both paymentAmount and paymentPercent are defined, paymentPercent is ignored
        * Returns: { paymentAmount: xx, paymentPercent: xx, downPaymentLocalCurrency: xx }
        */
        function CalculateDownPayment(totalNetAmount, paymentAmount, paymentPercent, exchangeRate) {
            var res = {
                PaymentAmount: 0,
                PaymentPercent: 0,
                DownPaymentLocalCurrency: null
            };
            if (paymentAmount) {
                res.PaymentAmount = paymentAmount;
                res.PaymentPercent = new Sys.Decimal(paymentAmount).mul(100).div(totalNetAmount).toNumber();
            }
            else if (paymentPercent) {
                res.PaymentAmount = new Sys.Decimal(totalNetAmount).mul(paymentPercent).div(100.0).toNumber();
                res.PaymentPercent = paymentPercent;
            }
            // Set Local currency Values
            if (!exchangeRate) {
                exchangeRate = 1;
            }
            res.DownPaymentLocalCurrency = new Sys.Decimal(res.PaymentAmount).mul(exchangeRate).toNumber();
            return res;
        }
        Purchasing.CalculateDownPayment = CalculateDownPayment;
        Purchasing.PRStatus = {
            ForPRWorkflow: ["To complete", "To review", "To approve", "Canceled", "Rejected"],
            ForPOWorkflow: ["To order"],
            ForDelivery: ["To receive", "Received"]
        };
        Purchasing.POStatus = {
            ForPOWorkflow: ["To order", "To pay"],
            ForDelivery: ["To receive", "Received", "Auto receive"]
        };
        /**
        * Helper for AutoCreateOrder user exit
        */
        Purchasing.autoCreateOrderEnabled = null;
        function IsAutoCreateOrderEnabled() {
            if (this.autoCreateOrderEnabled === null) {
                var enabled = Variable.GetValueAsString("AutoCreateOrderEnabled");
                if (!enabled) {
                    this.autoCreateOrderEnabled = !!Sys.Helpers.TryCallFunction("Lib.PR.Customization.Server.IsAutoCreateOrderEnabled");
                }
                else {
                    this.autoCreateOrderEnabled = Sys.Helpers.String.ToBoolean(enabled);
                }
            }
            return this.autoCreateOrderEnabled;
        }
        Purchasing.IsAutoCreateOrderEnabled = IsAutoCreateOrderEnabled;
        function IsMonoBuyer(table) {
            var firstBuyer = table.GetItem(0).GetValue("BuyerLogin__");
            for (var i = 0; i < table.GetItemCount(); i++) {
                var row = table.GetItem(i);
                var itemBuyer = row.GetValue("BuyerLogin__");
                if (!Sys.Helpers.IsEmpty(itemBuyer) && itemBuyer !== firstBuyer) {
                    return false;
                }
            }
            return true;
        }
        Purchasing.IsMonoBuyer = IsMonoBuyer;
        function IsTotalyTakenFromStock(table) {
            for (var i = 0; i < table.GetItemCount(); i++) {
                var row = table.GetItem(i);
                var quantityTakenFromStock = row.GetValue("QuantityTakenFromStock__");
                var requestedQuantity = row.GetValue("ItemQuantity__");
                if (!Sys.Helpers.IsEmpty(quantityTakenFromStock) && requestedQuantity > quantityTakenFromStock) {
                    return false;
                }
            }
            return true;
        }
        Purchasing.IsTotalyTakenFromStock = IsTotalyTakenFromStock;
        function IsAutoReceiveOrderEnabled() {
            return !!this.GetAutoReceiveOrderData();
        }
        Purchasing.IsAutoReceiveOrderEnabled = IsAutoReceiveOrderEnabled;
        Purchasing.autoReceiveOrderData = null;
        function GetAutoReceiveOrderData() {
            if (this.autoReceiveOrderData === null) {
                var data = Variable.GetValueAsString("AutoReceiveOrderData");
                if (!data) {
                    // Extract informations from custo lib (if exist)
                    data = Sys.Helpers.TryCallFunction("Lib.PO.Customization.Server.IsAutoReceiveOrderEnabled");
                    // Check informations that we received from the custo lib
                    if (Sys.Helpers.IsBoolean(data) && data) // Did we set a boolean ? If yes, check that the boolean is explicity set to 'true' ?
                     {
                        Log.Info("Auto receive purchase order detected with boolean value 'true'");
                        data = {}; // Means that auto reception order is set to 'true' (enabled)
                    }
                    else if (!Sys.Helpers.IsPlainObject(data)) // Did we set a JSON ?
                     {
                        Log.Info("NO auto receive purchase order detected");
                        data = false;
                    }
                    else {
                        Log.Info("Auto receive purchase order detected with JSON object");
                    }
                }
                else {
                    data = JSON.parse(data);
                }
                this.autoReceiveOrderData = data;
            }
            return this.autoReceiveOrderData;
        }
        Purchasing.GetAutoReceiveOrderData = GetAutoReceiveOrderData;
        function CheckMultipleBudgets(budgets) {
            var table = budgets.documentData.GetTable(budgets.sourceTypeConfig.formTable);
            var multipleBudgets = [];
            var _loop_4 = function (i) {
                var budgetID = budgets.byItemIndex[i];
                if (budgetID instanceof Lib.Budget.MultipleBudgetError) {
                    var error_1 = budgetID;
                    var detailsTrc = Lib.Budget.Configuration.GetBudgetKeyColumns().map(function (budgetColumn) {
                        return budgetColumn + "=" + error_1.budgetKeyColumns[budgetColumn];
                    }).join(", ");
                    Log.Warn("Impossible to find a unique budget for the item [" + i + "] : " + detailsTrc);
                    multipleBudgets.push(error_1);
                }
            };
            for (var i = 0; i < table.GetItemCount(); i++) {
                _loop_4(i);
            }
            if (multipleBudgets.length > 0) {
                return {
                    $error: "_Multiple budget error",
                    $params: multipleBudgets.join("\n")
                };
            }
            return {}; // OK!
        }
        Purchasing.CheckMultipleBudgets = CheckMultipleBudgets;
        var APClerk = /** @class */ (function () {
            function APClerk() {
                this.login = Lib.P2P.ResolveDemoLogin(Sys.Parameters.GetInstance("PAC").GetParameter("APClerkLogin"));
                this.user = null;
                this.exists = null;
            }
            APClerk.prototype.Exists = function (needUser) {
                if (this.exists === null || (needUser && this.user === null)) {
                    if (!Variable.GetValueAsString("APClerkExists__") || needUser) {
                        this.user = g.Users.GetUser(this.login);
                        this.exists = this.user !== null;
                        Variable.SetValueAsString("APClerkExists__", this.exists ? "true" : "false");
                    }
                    else {
                        this.exists = Variable.GetValueAsString("APClerkExists__") === "true";
                    }
                    if (!this.exists) {
                        Log.Info("No AP Clerk found.");
                    }
                }
                return this.exists;
            };
            APClerk.prototype.GiveReadRight = function () {
                if (this.Exists()) {
                    Log.Info("Grant read right to AP Clerk: " + this.login);
                    Process.SetRight(this.login, "read");
                }
            };
            APClerk.prototype.StoreSomeInfo = function () {
                if (this.Exists(true)) {
                    Log.Info("Store some information about AP Clerk");
                    var userVars = this.user.GetVars();
                    Variable.SetValueAsString("APClerkName__", userVars.GetValue_String("DisplayName", 0));
                }
            };
            return APClerk;
        }());
        Purchasing.APClerk = APClerk;
        function GetAPClerk() {
            return new APClerk();
        }
        Purchasing.GetAPClerk = GetAPClerk;
        function IsImgInRessources(imageLink) {
            var isURLexpression = /(https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&\/\/=]*))/g;
            var isURLregex = new RegExp(isURLexpression);
            return !(imageLink && imageLink.match(isURLregex));
        }
        function GetCatalogImageUrl(imageLink) {
            if (IsImgInRessources(imageLink)) {
                imageLink = Process.GetImageURL(imageLink);
            }
            return imageLink;
        }
        Purchasing.GetCatalogImageUrl = GetCatalogImageUrl;
        function HashLoginID(loginID) {
            var hash = 0, i, chr;
            if (loginID.length === 0) {
                return hash;
            }
            for (i = 0; i < loginID.length; i++) {
                chr = loginID.charCodeAt(i);
                hash = ((hash << 5) - hash) + chr;
                hash |= 0; // Convert to 32bit integer
            }
            return hash;
        }
        Purchasing.HashLoginID = HashLoginID;
    })(Purchasing = Lib.Purchasing || (Lib.Purchasing = {}));
})(Lib || (Lib = {}));
