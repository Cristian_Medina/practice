///#GLOBALS Lib Sys
/* LIB_DEFINITION{
  "name": "Lib_Purchasing_GRItems",
  "libraryType": "LIB",
  "scriptType": "COMMON",
  "comment": "Purchasing library to manage items in GR",
  "require": [
    "Sys/Sys_Decimal",
    "Sys/Sys_Helpers_String",
    "Lib_Purchasing_Items_V12.0.425.0",
    "Lib_Purchasing_V12.0.425.0"
  ]
}*/
// Common interface
var Lib;
(function (Lib) {
    var Purchasing;
    (function (Purchasing) {
        var GRItems;
        (function (GRItems) {
            // Members declaration and implementation
            var POItemsDBInfo = Lib.Purchasing.Items.POItemsDBInfo;
            /**
             * Fill extra fields on PO items. This function is set as option when calling FillFormItems function.
             * @param {object} dbItem current po item in database
             * @param {object} item current gr item in form
             * @param {object} options options used to fill items
             */
            function CompleteFormItem(dbItem, item, options) {
                var lineNumber = dbItem.GetValue("LineNumber__");
                var orderedQuantity = new Sys.Decimal(parseFloat(dbItem.GetValue("OrderedQuantity__")));
                var orderedAmount = new Sys.Decimal(dbItem.GetValue("NetAmount__"));
                var alreadyReceivedQty = new Sys.Decimal(0);
                var alreadyReceivedAmnt = new Sys.Decimal(0);
                var deliveryCompleted = false;
                if (options && options.grItems) {
                    var grItemsForLine = options.grItems[lineNumber];
                    if (grItemsForLine) {
                        grItemsForLine.forEach(function (grItem) {
                            if (grItem.GetValue("DeliveryCompleted__")) {
                                deliveryCompleted = true;
                            }
                            alreadyReceivedQty = alreadyReceivedQty.add(grItem.GetValue("Quantity__") || 0);
                            alreadyReceivedAmnt = alreadyReceivedAmnt.add(grItem.GetValue("Amount__") || 0);
                        });
                    }
                }
                if (deliveryCompleted) {
                    return false;
                }
                var lineOpenQuantity = orderedQuantity.minus(alreadyReceivedQty);
                var lineOpenAmount = orderedAmount.minus(alreadyReceivedAmnt);
                item.SetValue("OpenQuantity__", Math.max(0, lineOpenQuantity.toNumber())); // Qty can never be negative
                item.SetValue("ItemOpenAmount__", lineOpenAmount.toNumber());
                item.SetValue("POBudgetID__", dbItem.GetValue("BudgetID__"));
                if (options.autoReceiveOrderData) // only two possible case : equal to 'null' -> return false or equal to 'json object' -> return true
                 {
                    item.SetValue("ReceivedQuantity__", item.GetValue("OpenQuantity__"));
                    item.SetValue("NetAmount__", item.GetValue("ItemOpenAmount__"));
                    item.SetValue("DeliveryCompleted__", true);
                }
                var requesterLogin = Sys.Helpers.String.ExtractLoginFromDN(dbItem.GetValue("RequesterDN__"));
                Sys.OnDemand.Users.CacheByLogin.Get(requesterLogin, Lib.P2P.attributesForUserCache).Then(function (result) {
                    var user = result[requesterLogin];
                    if (!user.$error) {
                        Sys.Helpers.SilentChange(function () {
                            item.SetValue("RequesterName__", user.displayname ? user.displayname : user.login);
                        });
                    }
                });
                var recipientLogin = Sys.Helpers.String.ExtractLoginFromDN(dbItem.GetValue("RecipientDN__"));
                Sys.OnDemand.Users.CacheByLogin.Get(recipientLogin, Lib.P2P.attributesForUserCache).Then(function (result) {
                    var user = result[recipientLogin];
                    if (!user.$error) {
                        Sys.Helpers.SilentChange(function () {
                            item.SetValue("RecipientName__", user.displayname ? user.displayname : user.login);
                        });
                    }
                });
                return true;
            }
            GRItems.CompleteFormItem = CompleteFormItem;
            function QueryPOItems(context) {
                return Sys.Helpers.Promise.Create(function (resolve, reject) {
                    var filter = "&";
                    if (context.options.ancestorsids) {
                        filter += "(PORUIDEX__=" + context.options.ancestorsids + ")";
                    }
                    else {
                        var PONumber = Variable.GetValueAsString("OrderNumber__");
                        filter += "(PONumber__=" + PONumber + ")";
                    }
                    filter += "(NoGoodsReceipt__!=true)";
                    if (filter) {
                        Sys.GenericAPI.Query(POItemsDBInfo.table, filter, ["*"], function (dbItems, error) {
                            if (error) {
                                reject("FillForm: error querying PO items with filter. Details: " + error);
                            }
                            else if (dbItems.length === 0) {
                                reject("FillForm: cannot find any PO items with filter: " + filter);
                            }
                            else {
                                try {
                                    // need to sort by line number casted in integer (PO item LineNumber__ is a string...)
                                    context.dbItems = dbItems.sort(function (dbItem1, dbItem2) {
                                        // Here GetValue returns integer because we provide a fieldToTypeMap to the recordBuilder
                                        return dbItem1.GetValue("LineNumber__") - dbItem2.GetValue("LineNumber__");
                                    });
                                    resolve(context);
                                }
                                catch (e) {
                                    reject(e.toString());
                                }
                            }
                        }, context.options.orderByClause || "", null, {
                            recordBuilder: Sys.GenericAPI.BuildQueryResult,
                            fieldToTypeMap: POItemsDBInfo.fieldsMap,
                            bigQuery: true
                        });
                    }
                    else {
                        reject("FillForm: error, no PO Number");
                    }
                });
            }
            function QueryAlreadyReceivedItems(context) {
                var PONumber = context.dbItems[0].GetValue("PONumber__");
                var grItemsFilter = "(&(!(Status__=Canceled))(OrderNumber__ =" + PONumber + "))";
                return Lib.Purchasing.Items.GetItemsForDocument(Lib.Purchasing.Items.GRItemsDBInfo, grItemsFilter, "LineNumber__")
                    .Then(function (items) {
                    context.options.grItems = items;
                    return context;
                });
            }
            function LoadForeignData(context) {
                return Lib.Purchasing.Items.LoadForeignData(context.dbItems, POItemsDBInfo)
                    .Then(function (foreignData) {
                    context.options.foreignData = foreignData;
                    return context;
                });
            }
            function FillGR(context) {
                context.fieldsInError = Lib.Purchasing.Items.FillFormItems(context.dbItems, Lib.Purchasing.Items.POItemsToGR, context.options);
                // Set comment from the user exit (if exist)
                if (context.options && context.options.autoReceiveOrderData) {
                    if (context.options.autoReceiveOrderData.Comment) {
                        Data.SetValue("Comment__", context.options.autoReceiveOrderData.Comment);
                    }
                    // Set develivery date from the user exit (if exist)
                    if (context.options.autoReceiveOrderData.DeliveryDate) {
                        Data.SetValue("DeliveryDate__", context.options.autoReceiveOrderData.DeliveryDate);
                    }
                    // Set develivery note from the user exit (if exist)
                    if (context.options.autoReceiveOrderData.DeliveryNote) {
                        Data.SetValue("DeliveryNote__", context.options.autoReceiveOrderData.DeliveryNote);
                    }
                }
                var deliveryDate = Data.GetValue("DeliveryDate__");
                if (!deliveryDate) {
                    Data.SetValue("DeliveryDate__", new Date());
                }
                var status = Data.GetValue("GRStatus__");
                if (status !== "Received") {
                    Data.SetValue("GRStatus__", "To receive");
                }
                return Lib.Purchasing.SetERPByCompanyCode(Data.GetValue("CompanyCode__"))
                    .Then(function () {
                    Lib.P2P.InitSAPConfiguration(Lib.ERP.GetERPName(), "PAC");
                    return context;
                });
            }
            function CheckError(context) {
                return Sys.Helpers.Promise.Create(function (resolve, reject) {
                    if (context.fieldsInError.length > 0) {
                        // Reject with error message of the first field
                        reject(Lib.Purchasing.Items.POItemsToGR.errorMessages[context.fieldsInError[0]] || "Some items have different values on the following fields: " + context.fieldsInError.join(", "));
                    }
                    else {
                        resolve(context);
                    }
                });
            }
            /**
             * Fill the purchase order form according to the selected PR items by the specified filter.
             * @param {object} options
             * @returns {promise}
             */
            function FillForm(options) {
                Log.Time("Lib.Purchasing.GRItems.FillForm");
                var context = {};
                context.options = options || {};
                context.options.fillItem = options.fillItem || CompleteFormItem;
                return Sys.Helpers.Promise.Create(function (resolve, reject) {
                    QueryPOItems(context)
                        .Then(QueryAlreadyReceivedItems)
                        .Then(LoadForeignData)
                        .Then(FillGR)
                        .Then(CheckError)
                        .Then(function () {
                        Log.TimeEnd("Lib.Purchasing.GRItems.FillForm");
                        resolve();
                    })
                        .Catch(function (rejectMsg) {
                        Log.TimeEnd("Lib.Purchasing.GRItems.FillForm");
                        reject(rejectMsg);
                    });
                });
            }
            GRItems.FillForm = FillForm;
        })(GRItems = Purchasing.GRItems || (Purchasing.GRItems = {}));
    })(Purchasing = Lib.Purchasing || (Lib.Purchasing = {}));
})(Lib || (Lib = {}));
