///#GLOBALS Lib Sys
/* LIB_DEFINITION{
  "name": "Lib_Shipping",
  "libraryType": "LIB",
  "scriptType": "CLIENT",
  "comment": "Shipping management",
  "require": [
    "Sys/Sys_Helpers",
    "Lib_Shipping_V12.0.425.0"
  ]
}*/
var Lib;
(function (Lib) {
    var Shipping;
    (function (Shipping) {
        var Client;
        (function (Client) {
            var isReadOnlyEnabled = false;
            function InitControl(readOnly) {
                if (readOnly === void 0) { readOnly = false; }
                Controls.CarrierCombo__.Hide(false);
                //Carrier will be print when CarrierCombo__ is set to Other
                Controls.Carrier__.Hide(true);
                //Link stay hide because button is used to click on it.
                Controls.TrackingLink__.Hide(true);
                //fill options from lib shpping and lib shipping customization
                var carriersOptions = Controls.CarrierCombo__.GetAvailableValues();
                Lib.Shipping.GetNames().forEach(function (name) {
                    carriersOptions.push(name + "=" + name);
                });
                carriersOptions.push("Other=Other");
                var alreadySetValue = Controls.CarrierCombo__.GetValue();
                //Set all values from Lib and customization values
                Controls.CarrierCombo__.SetAvailableValues(carriersOptions);
                if (alreadySetValue) {
                    Controls.CarrierCombo__.SetValue(alreadySetValue);
                    Lib.Shipping.Client.SetTrackingLink();
                }
                else {
                    Controls.Carrier__.SetValue(Controls.CarrierCombo__.GetValue());
                }
                Controls.Carrier__.Hide(Controls.CarrierCombo__.GetValue() != "Other");
                Controls.CarrierCombo__.OnChange = function () {
                    if (Controls.CarrierCombo__.GetValue() == "Other") {
                        Controls.Carrier__.Hide(false);
                        Controls.Carrier__.SetValue("");
                    }
                    else {
                        Controls.Carrier__.SetValue(Controls.CarrierCombo__.GetValue());
                        Controls.Carrier__.Hide(true);
                    }
                    SetTrackingLink();
                };
                Controls.TrackingNumber__.OnChange = SetTrackingLink;
                Controls.TrackingNumber__.SetBrowsable(!(Controls.CarrierCombo__.GetValue() == "Other" || !Controls.TrackingNumber__.GetValue()));
                Controls.TrackingNumber__.OnBrowse = function () {
                    Controls.TrackingLink__.Click();
                };
                isReadOnlyEnabled = readOnly;
                Lib.Shipping.Client.SetReadOnly(readOnly);
            }
            Client.InitControl = InitControl;
            function SetTrackingLink() {
                if (!isReadOnlyEnabled) {
                    var trackingNumber = Controls.TrackingNumber__.GetValue();
                    var carrier = Controls.CarrierCombo__.GetValue();
                    if (trackingNumber && carrier && carrier !== "Other") {
                        Controls.TrackingNumber__.SetBrowsable(true);
                        Controls.TrackingLink__.SetURL(Lib.Shipping.GetLink(carrier, trackingNumber));
                        Controls.TrackingLink__.SetText(Lib.Shipping.GetLink(carrier, trackingNumber));
                    }
                    else {
                        Controls.TrackingNumber__.SetBrowsable(false);
                        Controls.TrackingLink__.SetURL("");
                        Controls.TrackingLink__.SetText("");
                    }
                }
            }
            Client.SetTrackingLink = SetTrackingLink;
            function SetReadOnly(readOnly) {
                if (readOnly === void 0) { readOnly = true; }
                isReadOnlyEnabled = readOnly;
                Controls.CarrierCombo__.SetReadOnly(readOnly);
                Controls.Carrier__.SetReadOnly(readOnly);
            }
            Client.SetReadOnly = SetReadOnly;
        })(Client = Shipping.Client || (Shipping.Client = {}));
    })(Shipping = Lib.Shipping || (Lib.Shipping = {}));
})(Lib || (Lib = {}));
