///#GLOBALS Lib Sys
/* LIB_DEFINITION{
  "name": "Lib_Purchasing_CatalogHelper",
  "libraryType": "LIB",
  "scriptType": "CLIENT",
  "comment": "Purchasing library",
  "require": [
    "Lib_Purchasing_V12.0.425.0",
    "Sys/Sys_Helpers",
    "Sys/Sys_Helpers_Array",
    "Sys/Sys_GenericAPI_Client",
    "Sys/Sys_OnDemand_Users"
  ]
}*/
var Lib;
(function (Lib) {
    var Purchasing;
    (function (Purchasing) {
        var CatalogHelper;
        (function (CatalogHelper) {
            CatalogHelper.SupplyTypesManager = (function () {
                var purchasingSupplyPromiseCache = {};
                var supplyTypesCache = [];
                var Query = function () {
                    var currentCompanyCode = Data.GetValue("CompanyCode__");
                    Log.Info("LoadPurchasingSupplyType for CompanyCode : " + currentCompanyCode);
                    supplyTypesCache = [];
                    var options = {
                        table: "PurchasingSupply__",
                        filter: "(|(CompanyCode__=" + currentCompanyCode + ")(!(CompanyCode__=*))(CompanyCode__=))",
                        attributes: ["SupplyID__", "Name__", "CompanyCode__", "RecipientLogin__", "BuyerLogin__", "DefaultCostType__", "DefaultGLAccount__", "DefaultGLAccount__.Group__", "NoGoodsReceipt__", "ParentSupplyID__", "FullName__"],
                        sortOrder: "Name__ ASC",
                        maxRecords: Sys.Parameters.GetInstance("PAC").GetParameter("MaxDisplayedPurchasingCategories"),
                        additionalOptions: "EnableJoin=1"
                    };
                    return Sys.GenericAPI.PromisedQuery(options)
                        .Then(function (queryResult) {
                        return Sys.Helpers.Promise.Create(function (resolve) {
                            if (queryResult.length > 0) {
                                Sys.Helpers.Array.ForEach(queryResult, function (result) {
                                    supplyTypesCache.push({
                                        companyCode: result.CompanyCode__,
                                        name: result.Name__,
                                        fullName: result.FullName__,
                                        defaultCostType: result.DefaultCostType__,
                                        defaultGLAccount: result.DefaultGLAccount__,
                                        defaultGLAccountGroup: result["DefaultGLAccount__.Group__"],
                                        id: result.SupplyID__,
                                        recipientLogin: result.RecipientLogin__,
                                        buyerLogin: result.BuyerLogin__,
                                        isRecipientBrowsable: Sys.Helpers.IsEmpty(result.RecipientLogin__),
                                        noGoodsReceipt: result.NoGoodsReceipt__,
                                        parentId: result.ParentSupplyID__,
                                        childrenIds: [],
                                        GetPath: function () {
                                            if (this.fullName) {
                                                return this.fullName;
                                            }
                                            if (this.parentId) {
                                                var that_1 = this;
                                                var parent = Sys.Helpers.Array.Find(supplyTypesCache, function (item) {
                                                    return item.id == that_1.parentId && (item.companyCode == currentCompanyCode || Sys.Helpers.IsEmpty(item.companyCode));
                                                });
                                                var path = parent.GetPath() + "/" + this.id;
                                                return path;
                                            }
                                            return this.id;
                                        }
                                    });
                                });
                                resolve();
                            }
                            resolve();
                        });
                    })
                        .Then(function () {
                        Sys.Helpers.Array.ForEach(supplyTypesCache, function (supplyType) {
                            if (supplyType.parentId) {
                                var parent = Sys.Helpers.Array.Find(supplyTypesCache, function (parentSupply) {
                                    return parentSupply.id === supplyType.parentId;
                                });
                                parent.childrenIds.push(supplyType.id);
                            }
                        });
                    })
                        .Catch(function (error) {
                        Log.Error(error);
                        // nothing to do;
                    });
                };
                var ToString = function () {
                    var currentCompanyCode = Data.GetValue("CompanyCode__");
                    var str = "";
                    str += "*=_AllPurchasingSupply";
                    Sys.Helpers.Array.ForEach(supplyTypesCache, function (obj) {
                        if (obj.companyCode === currentCompanyCode || Sys.Helpers.IsEmpty(obj.companyCode)) {
                            str += "\n" + obj.name + "=" + obj.name;
                        }
                    });
                    return str;
                };
                var Get = function (supplyTypeId) {
                    var currentCompanyCode = Data.GetValue("CompanyCode__");
                    var promiseCacheKey = currentCompanyCode + "_" + supplyTypeId;
                    return purchasingSupplyPromiseCache[promiseCacheKey] || (purchasingSupplyPromiseCache[promiseCacheKey] = Sys.Helpers.Promise.Create(function (resolve) {
                        var supplyType = Sys.Helpers.Array.Find(supplyTypesCache, function (item) {
                            return item.id == supplyTypeId && (item.companyCode == currentCompanyCode || Sys.Helpers.IsEmpty(item.companyCode));
                        });
                        if (supplyType) {
                            resolve(supplyType);
                        }
                        else {
                            var options = {
                                table: "PurchasingSupply__",
                                filter: "(&(SupplyID__=" + supplyTypeId + ")(|(CompanyCode__=" + currentCompanyCode + ")(!(CompanyCode__=*))))",
                                attributes: ["SupplyID__", "Name__", "CompanyCode__", "RecipientLogin__", "BuyerLogin__", "DefaultCostType__", "DefaultGLAccount__", "DefaultGLAccount__.Group__", "NoGoodsReceipt__", "ParentSupplyID__", "FullName__"],
                                sortOrder: "Name__ ASC",
                                maxRecords: 1,
                                additionalOptions: "EnableJoin=1"
                            };
                            Sys.GenericAPI.PromisedQuery(options)
                                .Then(function (queryResult) {
                                if (queryResult.length > 0) {
                                    supplyType = {
                                        companyCode: queryResult[0].CompanyCode__,
                                        name: queryResult[0].Name__,
                                        fullName: queryResult[0].FullName__,
                                        defaultCostType: queryResult[0].DefaultCostType__,
                                        defaultGLAccount: queryResult[0].DefaultGLAccount__,
                                        defaultGLAccountGroup: queryResult[0]["DefaultGLAccount__.Group__"],
                                        id: queryResult[0].SupplyID__,
                                        recipientLogin: queryResult[0].RecipientLogin__,
                                        buyerLogin: queryResult[0].BuyerLogin__,
                                        isRecipientBrowsable: Sys.Helpers.IsEmpty(queryResult[0].RecipientLogin__),
                                        noGoodsReceipt: queryResult[0].NoGoodsReceipt__,
                                        parentId: queryResult[0].ParentSupplyID__
                                    };
                                    resolve(supplyType);
                                }
                                resolve(supplyType);
                            })
                                .Catch(function (error) {
                                Log.Error(error);
                                resolve(supplyType);
                            });
                        }
                    }));
                };
                var GetChildren = function (supplyTypeId) {
                    var currentCompanyCode = Data.GetValue("CompanyCode__");
                    var children = [];
                    if (supplyTypeId) {
                        var supplyType = Sys.Helpers.Array.Find(supplyTypesCache, function (item) {
                            return item.id == supplyTypeId && (item.companyCode == currentCompanyCode || Sys.Helpers.IsEmpty(item.companyCode));
                        });
                        if (supplyType) {
                            Sys.Helpers.Array.ForEach(supplyType.childrenIds, function (childID) {
                                var childObj = Sys.Helpers.Array.Find(supplyTypesCache, function (item) {
                                    return item.id == childID && (item.companyCode == currentCompanyCode || Sys.Helpers.IsEmpty(item.companyCode));
                                });
                                children.push({
                                    id: childObj.id,
                                    value: childObj.name
                                });
                            });
                        }
                    }
                    else {
                        // Sends all items without parents
                        Sys.Helpers.Array.ForEach(supplyTypesCache, function (item) {
                            if (Sys.Helpers.IsEmpty(item.parentId)) {
                                children.push({
                                    id: item.id,
                                    value: item.name
                                });
                            }
                        });
                    }
                    return children;
                };
                var GetChildrenTree = function (supplyTypeId) {
                    var allChildren = GetChildren(supplyTypeId);
                    Sys.Helpers.Array.ForEach(allChildren, function (child) {
                        allChildren = allChildren.concat(GetChildren(child.id));
                    });
                    return allChildren;
                };
                var GetFullPath = function (supplyTypeId) {
                    var currentCompanyCode = Data.GetValue("CompanyCode__");
                    var supplyType = Sys.Helpers.Array.Find(supplyTypesCache, function (item) {
                        return item.id == supplyTypeId && (item.companyCode == currentCompanyCode || Sys.Helpers.IsEmpty(item.companyCode));
                    });
                    if (supplyType != null) {
                        return supplyType.GetPath();
                    }
                    return "";
                };
                return {
                    Query: Query,
                    Get: Get,
                    ToString: ToString,
                    GetChildren: GetChildren,
                    GetChildrenTree: GetChildrenTree,
                    GetFullPath: GetFullPath
                };
            })();
            CatalogHelper.CatalogItem = {
                "Attributes": ["RUIDEX",
                    "ITEMDESCRIPTION__",
                    "ITEMLONGDESCRIPTION__",
                    "ITEMTYPE__",
                    "ITEMUNITPRICE__",
                    "ITEMCURRENCY__",
                    "ITEMNUMBER__",
                    "ITEMSUPPLIERPARTAUXID__",
                    "PUNCHOUTSITENAME__",
                    "IMAGE__",
                    "VENDORNUMBER__",
                    "VENDORNUMBER__.NAME__",
                    "VENDORNUMBER__.STREET__",
                    "VENDORNUMBER__.CITY__",
                    "VENDORNUMBER__.POSTALCODE__",
                    "VENDORNUMBER__.REGION__",
                    "VENDORNUMBER__.COUNTRY__",
                    "SUPPLYTYPEID__",
                    "SUPPLYTYPEID__.NAME__",
                    "SUPPLYTYPEID__.BUYERLOGIN__",
                    "SUPPLYTYPEID__.RECIPIENTLOGIN__",
                    "SUPPLYTYPEID__.DEFAULTGLACCOUNT__",
                    "SUPPLYTYPEID__.DEFAULTCOSTTYPE__",
                    "COSTTYPE__",
                    "ITEMGLACCOUNT__",
                    "ITEMGLACCOUNT__.GROUP__",
                    "ITEMTAXCODE__",
                    "ITEMTAXCODE__.TAXRATE__",
                    "UNITOFMEASURE__",
                    "UNITOFMEASURE__.DESCRIPTION__",
                    "SUPPLYTYPEID__.NOGOODSRECEIPT__",
                    "LEADTIME__",
                    "LOCKED__",
                    "GRADE__",
                    "GRADE_NUMBER__",
                    "ITEMTAGS__"
                ]
            };
        })(CatalogHelper = Purchasing.CatalogHelper || (Purchasing.CatalogHelper = {}));
    })(Purchasing = Lib.Purchasing || (Lib.Purchasing = {}));
})(Lib || (Lib = {}));
