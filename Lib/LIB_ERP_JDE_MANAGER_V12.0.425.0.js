/* eslint-disable class-methods-use-this */
/* LIB_DEFINITION{
  "name": "LIB_ERP_JDE_Manager",
  "libraryType": "LIB",
  "scriptType": "COMMON",
  "comment": "ERP Manager for JDE - system library",
  "require": [
    "Sys/Sys_Helpers",
    "Sys/Sys_Helpers_Object",
    "Lib_ERP_Manager_V12.0.425.0",
    "Lib_ERP_V12.0.425.0"
  ]
}*/
var Lib;
(function (Lib) {
    var ERP;
    (function (ERP) {
        var JDE;
        (function (JDE) {
            var Manager = /** @class */ (function (_super) {
                __extends(Manager, _super);
                function Manager() {
                    return _super.call(this, "JDE") || this;
                }
                return Manager;
            }(Lib.ERP.Manager.Instance));
            JDE.Manager = Manager;
            Lib.ERP.JDE.Manager.prototype.documentFactories = {};
        })(JDE = ERP.JDE || (ERP.JDE = {}));
    })(ERP = Lib.ERP || (Lib.ERP = {}));
})(Lib || (Lib = {}));
Lib.ERP.Manager.factories.JDE = function () {
    return new Lib.ERP.JDE.Manager();
};
