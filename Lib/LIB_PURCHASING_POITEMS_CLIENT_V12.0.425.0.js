///#GLOBALS Lib Sys
/* LIB_DEFINITION{
  "name": "Lib_Purchasing_POItems_Client",
  "libraryType": "LIB",
  "scriptType": "Client",
  "comment": "Purchasing library to manage items in PO",
  "require": [
    "Sys/Sys_Helpers_String",
    "Lib_Purchasing_POItems_V12.0.425.0",
    "Lib_Purchasing_Items_V12.0.425.0",
    "Lib_Purchasing_Vendor_V12.0.425.0",
    "Lib_Purchasing_V12.0.425.0",
    "Lib_Purchasing_Punchout_PO_Client_V12.0.425.0",
    "Sys/Sys_Helpers_LdapUtil",
    "[Sys/Sys_Helpers_Browse]",
    "[Lib_CommonDialog_V12.0.425.0]"
  ]
}*/
var Lib;
(function (Lib) {
    var Purchasing;
    (function (Purchasing) {
        var POItems;
        (function (POItems) {
            var g = Sys.Helpers.Globals;
            var itemsMap = {};
            var nextLineNumber = 0;
            function normalizeValue(fieldValue) {
                // normalize multiline string values
                if (Sys.Helpers.IsString(fieldValue)) {
                    fieldValue = fieldValue.replace(/\r\n/g, "\n");
                }
                return fieldValue;
            }
            function CheckSection(fields, item) {
                var allEquals = true;
                var allInitialEmpty = true;
                Sys.Helpers.Object.ForEach(fields, function (field) {
                    allEquals = allEquals && normalizeValue(Data.GetValue(field)) === normalizeValue(item.GetValue(field));
                    allInitialEmpty = allInitialEmpty && !Data.GetValue(field);
                });
                return allEquals || allInitialEmpty;
            }
            function QueryAlreadyOrderedQuantity(dbItem) {
                var PRItemsSynchronizeConfig = Lib.Purchasing.Items.PRItemsSynchronizeConfig;
                var POItemsDBInfo = Lib.Purchasing.Items.POItemsDBInfo;
                return Sys.Helpers.Promise.Create(function (resolve, reject) {
                    var checkQtyWith = null;
                    if (itemsMap[GetKey(dbItem)]) {
                        var poItemsFilter = "(&(Status__!=Canceled)(PRNumber__=" + dbItem.GetValue(PRItemsSynchronizeConfig.tableKey) + ")(PRLineNumber__=" + dbItem.GetValue(PRItemsSynchronizeConfig.lineKey) + "))";
                        Log.Info("Fetching ordered information from PO Items: " + poItemsFilter);
                        Sys.GenericAPI.Query(POItemsDBInfo.table, poItemsFilter, ["PONumber__", "PRNumber__", "PRLineNumber__", "OrderedQuantity__"], function (dbPOItems, error) {
                            if (error) {
                                Log.TimeEnd("Lib.Purchasing.POItems.FillForm");
                                reject("FillForm: error querying PO items for Quantity__ with filter. Details: " + error);
                            }
                            else {
                                if (dbPOItems.length !== 0) {
                                    var poOrderedQty_1 = {};
                                    var poNumber_1 = g.Data.GetValue("OrderNumber__");
                                    dbPOItems.forEach(function (dbPOItem) {
                                        var prNumber = dbPOItem.GetValue("PRNumber__");
                                        var prItemNumber = dbPOItem.GetValue("PRLineNumber__");
                                        var qty = dbPOItem.GetValue("OrderedQuantity__");
                                        if (!poOrderedQty_1[prNumber]) {
                                            poOrderedQty_1[prNumber] = {};
                                        }
                                        if (!poOrderedQty_1[prNumber][prItemNumber]) {
                                            poOrderedQty_1[prNumber][prItemNumber] = 0;
                                        }
                                        if (dbPOItem.GetValue("PONumber__") !== poNumber_1) {
                                            poOrderedQty_1[prNumber][prItemNumber] = new Sys.Decimal(poOrderedQty_1[prNumber][prItemNumber]).add(qty).toNumber();
                                        }
                                    });
                                    checkQtyWith = poOrderedQty_1;
                                }
                                resolve(checkQtyWith);
                            }
                        }, "", Lib.Purchasing.Items.MAXRECORDS, {
                            recordBuilder: Sys.GenericAPI.BuildQueryResult,
                            fieldToTypeMap: POItemsDBInfo.fieldsMap
                        });
                    }
                    else {
                        resolve(checkQtyWith);
                    }
                });
            }
            function AddSelectedItemToOrder(dbItem, checkQtyWith) {
                return Sys.Helpers.Promise.Create(function (resolve) {
                    var dbItems = [dbItem];
                    var PRItemsToPO = Lib.Purchasing.Items.PRItemsToPO;
                    var options = {
                        // no foreign data needed (ignore vendor information)
                        foreignData: null,
                        // quantity to order is probably obsolete when user selects item.
                        // the check of quantity in validation script will detect over-ordered items.
                        checkQtyWith: checkQtyWith,
                        // common API used to fill an item
                        fillItem: Lib.Purchasing.POItems.CompleteFormItem,
                        // we keep all items in table
                        resetItems: false,
                        // fill only item fields not common ones
                        doNotFillCommonFields: true
                    };
                    Lib.Purchasing.Items.FillFormItems(dbItems, PRItemsToPO, options);
                    // Trigger event in order to refresh other information on form
                    // retrieve last item (new item just added)
                    var itemCount = g.Controls.LineItems__.GetItemCount();
                    if (itemCount > 0) {
                        var index = itemCount - 1;
                        var isMultiShipTo = Sys.Parameters.GetInstance("PAC").GetParameter("MultiShipTo", false);
                        var vendorSectionAreEquals = CheckSection(["VendorName__", "VendorNumber__"], dbItem);
                        // multiShipTo : PO shipToAddress header is not used and therefore should not be compared
                        var shipToSectionAreEqualsOrIgnored = isMultiShipTo || CheckSection(["ShipToAddress__", "ShipToCompany__", "ShipToContact__", "ShipToPhone__", "ShipToEmail__"], dbItem);
                        var title = null, msg = null;
                        if (!vendorSectionAreEquals && !shipToSectionAreEqualsOrIgnored) {
                            title = "_Vendor and delivery address different";
                            msg = "_The vendor and the delivery address of the added item don't match the ones of the purchase order. Keep it?";
                        }
                        else if (!vendorSectionAreEquals) {
                            title = "_Vendor different";
                            msg = "_The vendor of the added item doesn't match the one of the purchase order. Keep it?";
                        }
                        else if (!shipToSectionAreEqualsOrIgnored) {
                            title = "_Delivery address different";
                            msg = "_The delivery address of the added item doesn't match the one of the purchase order. Keep it?";
                        }
                        if (title != null) {
                            Lib.CommonDialog.PopupYesCancel(function (action) {
                                if (action === "Cancel") {
                                    g.Controls.LineItems__.SetItemCount(itemCount - 1);
                                }
                                resolve();
                            }, title, msg, "_Yes", "_No");
                        }
                        var tableItem = g.Controls.LineItems__.GetItem(index);
                        if (Lib.Purchasing.Punchout.PO.IsEnabled()) {
                            Lib.Purchasing.Punchout.PO.CheckPunchoutItemValidity(tableItem);
                        }
                        g.Controls.LineItems__.OnAddItem(tableItem, index);
                        if (title == null) {
                            resolve();
                        }
                    }
                });
            }
            function InitAddItemsToOrderButton() {
                g.Controls.BrowsePRItems__.OnSelectItem = function (item) {
                    QueryAlreadyOrderedQuantity(item)
                        .Then(function (checkQtyWith) {
                        AddSelectedItemToOrder(item, checkQtyWith)
                            .Then(SetFilter);
                    });
                };
                g.Controls.AddItemsToOrderButton__.OnClick = function () {
                    SetFilter();
                    g.Controls.BrowsePRItems__.DoBrowse();
                };
                function SetFilter() {
                    var _a, _b;
                    // filter according to :
                    var allFilters = [];
                    // only items to order
                    allFilters.push(Sys.Helpers.LdapUtil.FilterEqual("Status__", "To order"));
                    // and items on which i'm buyer (ooto + groups)
                    allFilters.push(Sys.Helpers.LdapUtil.FilterEqual("BuyerDN__", "$submit-owner"));
                    if (!Sys.Helpers.IsEmpty(g.Data.GetValue("CompanyCode__"))) {
                        // same company code as already selected Items
                        allFilters.push(Sys.Helpers.LdapUtil.FilterEqual("CompanyCode__", g.Data.GetValue("CompanyCode__")));
                    }
                    if (!Sys.Helpers.IsEmpty(g.Data.GetValue("Currency__"))) {
                        // same currency as already selected Items
                        allFilters.push(Sys.Helpers.LdapUtil.FilterEqual("Currency__", g.Data.GetValue("Currency__")));
                    }
                    // coherence with already selected items
                    Sys.Helpers.Data.ForEachTableItem("LineItems__", function (item) {
                        // do not select items already selected
                        var PRNumberFilter = Sys.Helpers.LdapUtil.FilterEqual("PRNumber__", item.GetValue("PRNumber__"));
                        var LineNumberFilter = Sys.Helpers.LdapUtil.FilterEqual("LineNumber__", item.GetValue("PRLineNumber__"));
                        allFilters.push(Sys.Helpers.LdapUtil.FilterNot(Sys.Helpers.LdapUtil.FilterAnd(PRNumberFilter, LineNumberFilter)));
                    });
                    var DeletedItemsFilter = [];
                    var lineChanges = Lib.Purchasing.POEdition.ChangesManager.GetChangedItems().LineItems__;
                    if (lineChanges) {
                        Sys.Helpers.Array.ForEach(lineChanges.Deleted, function (idx) {
                            var keys = idx.split("###");
                            var PRNumberFilter = Sys.Helpers.LdapUtil.FilterEqual("PRNumber__", keys[1]);
                            var LineNumberFilter = Sys.Helpers.LdapUtil.FilterEqual("LineNumber__", keys[2]);
                            DeletedItemsFilter.push(Sys.Helpers.LdapUtil.FilterAnd(PRNumberFilter, LineNumberFilter));
                        });
                    }
                    var filter = (_a = Sys.Helpers.LdapUtil).FilterAnd.apply(_a, allFilters);
                    filter = Sys.Helpers.LdapUtil.FilterOr(filter, (_b = Sys.Helpers.LdapUtil).FilterOr.apply(_b, DeletedItemsFilter));
                    g.Controls.BrowsePRItems__.SetFilter(filter.toString());
                }
            }
            POItems.InitAddItemsToOrderButton = InitAddItemsToOrderButton;
            function StockItemsBeforeEdit() {
                Sys.Helpers.Data.ForEachTableItem("LineItems__", function (item) {
                    var lineItemNumber = parseInt(item.GetValue("LineItemNumber__"), 10);
                    itemsMap[GetKey(item)] = lineItemNumber;
                    if (nextLineNumber < lineItemNumber) {
                        nextLineNumber = lineItemNumber;
                    }
                });
            }
            POItems.StockItemsBeforeEdit = StockItemsBeforeEdit;
            function ReNumberLineItemAdd(item, isEditing) {
                if (isEditing) {
                    var lineNumber = itemsMap[GetKey(item)];
                    if (lineNumber) {
                        item.SetValue("LineItemNumber__", lineNumber);
                    }
                    else {
                        nextLineNumber += 10;
                        item.SetValue("LineItemNumber__", nextLineNumber);
                    }
                }
                else {
                    // re-number items
                    Lib.Purchasing.Items.NumberLineItems();
                }
            }
            POItems.ReNumberLineItemAdd = ReNumberLineItemAdd;
            function ReNumberLineItemRemove(item, index, isEditing) {
                if (isEditing) {
                    if (!itemsMap[GetKey(item)]) {
                        nextLineNumber -= 10;
                        var lineItemNumber = parseInt(item.GetValue("LineItemNumber__"), 10);
                        var lineItemsCount = Controls.LineItems__.GetItemCount();
                        for (var i = index; i < lineItemsCount; i++) {
                            Controls.LineItems__.GetItem(i).SetValue("LineItemNumber__", lineItemNumber);
                            lineItemNumber += 10;
                        }
                    }
                }
                else {
                    // re-number items
                    Lib.Purchasing.Items.NumberLineItems();
                }
            }
            POItems.ReNumberLineItemRemove = ReNumberLineItemRemove;
            function GetKey(item) {
                var PRLineNumer = item.GetValue("PRLineNumber__");
                if (!PRLineNumer) {
                    // if it's a PRItem
                    PRLineNumer = item.GetValue("LineNumber__");
                }
                return item.GetValue("PRNumber__") + '###' + PRLineNumer;
            }
        })(POItems = Purchasing.POItems || (Purchasing.POItems = {}));
    })(Purchasing = Lib.Purchasing || (Lib.Purchasing = {}));
})(Lib || (Lib = {}));
