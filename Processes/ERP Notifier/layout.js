{
	"*": {
		"GDRData": {
			"type": "GDRData",
			"options": {
				"version": 0
			},
			"stamp": 1,
			"data": []
		},
		"OCRParams": {
			"type": "OCRParams",
			"options": {
				"version": 0
			},
			"stamp": 2,
			"data": []
		},
		"Screen": {
			"type": "FlexibleFormLayout",
			"options": {
				"splitterType": "complex",
				"version": 0,
				"maxwidth": ""
			},
			"*": {
				"form-header": {
					"type": "FlexibleFormHeader",
					"options": {
						"version": 0,
						"maxwidth": ""
					},
					"stamp": 4,
					"data": []
				},
				"form-center": {
					"type": "FlexibleFormCenter",
					"options": {
						"version": 0,
						"maxwidth": ""
					},
					"*": {
						"form-content": {
							"type": "FlexibleFormContent",
							"options": {
								"version": 0
							},
							"*": {
								"form-content-top": {
									"type": "FlexibleFormSplitter",
									"options": {
										"autosize": true,
										"version": 1,
										"box": {
											"top": "0%",
											"left": "0%",
											"width": null,
											"height": "16%"
										},
										"hidden": false
									},
									"*": {
										"form-content-top-1": {
											"type": "FlexibleFormLine",
											"options": {
												"version": 0
											},
											"*": {
												"DocumentsPanel": {
													"type": "PanelDocument",
													"options": {
														"version": 0,
														"border": {},
														"labelLength": 0,
														"label": "_Documents",
														"iconURL": "",
														"TitleFontSize": "S",
														"hideActionButtons": false,
														"hideTitleBar": false,
														"hideTitle": false
													},
													"*": {
														"Document": {
															"data": [
																"documentscontroldata"
															],
															"type": "Documents",
															"options": {
																"version": 0
															},
															"stamp": 10
														}
													},
													"stamp": 9,
													"data": []
												}
											},
											"stamp": 8,
											"data": []
										}
									},
									"stamp": 7,
									"data": []
								},
								"form-content-bottom": {
									"type": "FlexibleFormSplitter",
									"options": {
										"version": 1,
										"box": {
											"top": "16%",
											"left": "0%",
											"width": null,
											"height": "84%"
										},
										"hidden": false
									},
									"*": {
										"form-content-left": {
											"type": "FlexibleFormSplitter",
											"options": {
												"version": 1,
												"box": {
													"top": "0%",
													"left": "0%",
													"width": "49%",
													"height": null
												},
												"hidden": false
											},
											"*": {
												"form-content-left-1": {
													"type": "FlexibleFormLine",
													"options": {
														"version": 0
													},
													"*": {
														"DataPanel": {
															"type": "PanelData",
															"options": {
																"border": {},
																"version": 0,
																"labelLength": 0,
																"label": "_Document data",
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": false,
																"hideTitleBar": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"hideTitle": false
															},
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"options": {
																		"controlsAssociation": {
																			"CompanyCode__": "LabelCompanyCode__",
																			"LabelCompanyCode__": "CompanyCode__",
																			"VendorNumber__": "LabelVendorNumber__",
																			"LabelVendorNumber__": "VendorNumber__",
																			"InvoiceNumber__": "LabelInvoiceNumber__",
																			"LabelInvoiceNumber__": "InvoiceNumber__",
																			"InvoiceAmount__": "LabelInvoiceAmount__",
																			"LabelInvoiceAmount__": "InvoiceAmount__",
																			"NetAmount__": "LabelNetAmount__",
																			"LabelNetAmount__": "NetAmount__",
																			"InvoiceCurrency__": "LabelInvoiceCurrency__",
																			"LabelInvoiceCurrency__": "InvoiceCurrency__"
																		},
																		"version": 0
																	},
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"options": {
																				"lines": 6,
																				"columns": 2,
																				"version": 0,
																				"ctrlsPos": {
																					"CompanyCode__": {
																						"line": 1,
																						"column": 2
																					},
																					"LabelCompanyCode__": {
																						"line": 1,
																						"column": 1
																					},
																					"VendorNumber__": {
																						"line": 3,
																						"column": 2
																					},
																					"LabelVendorNumber__": {
																						"line": 3,
																						"column": 1
																					},
																					"InvoiceNumber__": {
																						"line": 2,
																						"column": 2
																					},
																					"LabelInvoiceNumber__": {
																						"line": 2,
																						"column": 1
																					},
																					"InvoiceAmount__": {
																						"line": 4,
																						"column": 2
																					},
																					"LabelInvoiceAmount__": {
																						"line": 4,
																						"column": 1
																					},
																					"NetAmount__": {
																						"line": 5,
																						"column": 2
																					},
																					"LabelNetAmount__": {
																						"line": 5,
																						"column": 1
																					},
																					"InvoiceCurrency__": {
																						"line": 6,
																						"column": 2
																					},
																					"LabelInvoiceCurrency__": {
																						"line": 6,
																						"column": 1
																					}
																				},
																				"colspans": [
																					[],
																					[],
																					[],
																					[],
																					[],
																					[]
																				]
																			},
																			"data": [
																				"fields"
																			],
																			"*": {
																				"LabelCompanyCode__": {
																					"type": "Label",
																					"data": [
																						"CompanyCode__"
																					],
																					"options": {
																						"label": "_Company code",
																						"version": 0
																					},
																					"stamp": 43
																				},
																				"CompanyCode__": {
																					"type": "ShortText",
																					"data": [
																						"CompanyCode__"
																					],
																					"options": {
																						"label": "_Company code",
																						"activable": true,
																						"width": "180",
																						"version": 0,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"browsable": false,
																						"autocompletable": true
																					},
																					"stamp": 44
																				},
																				"LabelInvoiceNumber__": {
																					"type": "Label",
																					"data": [
																						"InvoiceNumber__"
																					],
																					"options": {
																						"label": "_Invoice number",
																						"version": 0
																					},
																					"stamp": 39
																				},
																				"InvoiceNumber__": {
																					"type": "ShortText",
																					"data": [
																						"InvoiceNumber__"
																					],
																					"options": {
																						"label": "_Invoice number",
																						"activable": true,
																						"width": "180",
																						"version": 0,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"browsable": false
																					},
																					"stamp": 40
																				},
																				"LabelVendorNumber__": {
																					"type": "Label",
																					"data": [
																						"VendorNumber__"
																					],
																					"options": {
																						"label": "_Vendor number",
																						"version": 0
																					},
																					"stamp": 47
																				},
																				"VendorNumber__": {
																					"type": "ShortText",
																					"data": [
																						"VendorNumber__"
																					],
																					"options": {
																						"label": "_Vendor number",
																						"activable": true,
																						"width": "180",
																						"version": 0,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"browsable": false
																					},
																					"stamp": 48
																				},
																				"LabelInvoiceAmount__": {
																					"type": "Label",
																					"data": [
																						"InvoiceAmount__"
																					],
																					"options": {
																						"label": "_Invoice amount"
																					},
																					"stamp": 51
																				},
																				"InvoiceAmount__": {
																					"type": "Decimal",
																					"data": [
																						"InvoiceAmount__"
																					],
																					"options": {
																						"version": 1,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"integer": false,
																						"label": "_Invoice amount",
																						"precision_internal": 2,
																						"precision_current": 2,
																						"activable": true,
																						"width": "180"
																					},
																					"stamp": 52
																				},
																				"LabelNetAmount__": {
																					"type": "Label",
																					"data": [
																						"NetAmount__"
																					],
																					"options": {
																						"label": "_Net amount"
																					},
																					"stamp": 53
																				},
																				"NetAmount__": {
																					"type": "Decimal",
																					"data": [
																						"NetAmount__"
																					],
																					"options": {
																						"version": 1,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"integer": false,
																						"label": "_Net amount",
																						"precision_internal": 2,
																						"precision_current": 2,
																						"activable": true,
																						"width": "180"
																					},
																					"stamp": 54
																				},
																				"LabelInvoiceCurrency__": {
																					"type": "Label",
																					"data": [
																						"InvoiceCurrency__"
																					],
																					"options": {
																						"label": "_Invoice currency"
																					},
																					"stamp": 55
																				},
																				"InvoiceCurrency__": {
																					"type": "ShortText",
																					"data": [
																						"InvoiceCurrency__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"label": "_Invoice currency",
																						"activable": true,
																						"width": "180",
																						"browsable": false
																					},
																					"stamp": 56
																				}
																			},
																			"stamp": 16
																		}
																	},
																	"stamp": 15,
																	"data": []
																}
															},
															"stamp": 14,
															"data": []
														}
													},
													"stamp": 13,
													"data": []
												},
												"form-content-left-2": {
													"type": "FlexibleFormLine",
													"options": {
														"version": 0
													},
													"*": {
														"SystemData": {
															"type": "PanelSystemData",
															"options": {
																"multicolumn": false,
																"version": 0,
																"border": {},
																"labelLength": 0,
																"label": "_System data",
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": false,
																"hideTitleBar": false,
																"hideTitle": false
															},
															"stamp": 18,
															"data": []
														}
													},
													"stamp": 17,
													"data": []
												},
												"form-content-left-3": {
													"type": "FlexibleFormLine",
													"options": {
														"version": 0
													},
													"*": {
														"NextProcess": {
															"type": "PanelNextProcess",
															"options": {
																"hidden": true,
																"version": 0,
																"border": {},
																"labelLength": 0,
																"label": "_Next Process",
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": false,
																"hideTitleBar": false,
																"hideTitle": false
															},
															"stamp": 20,
															"data": []
														}
													},
													"stamp": 19,
													"data": []
												}
											},
											"stamp": 12,
											"data": []
										},
										"form-content-right": {
											"type": "FlexibleFormSplitter",
											"options": {
												"version": 1,
												"box": {
													"top": "0%",
													"left": "49%",
													"width": "51%",
													"height": null
												},
												"hidden": false
											},
											"*": {
												"form-content-right-1": {
													"type": "FlexibleFormLine",
													"options": {
														"version": 0
													},
													"*": {
														"PreviewPanel": {
															"type": "PanelPreview",
															"options": {
																"border": {},
																"version": 0,
																"labelLength": 0,
																"label": "_Document Preview",
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": false,
																"hideTitleBar": false,
																"hideTitle": false
															},
															"*": {
																"Preview": {
																	"data": [
																		"previewdata"
																	],
																	"type": "Preview",
																	"options": {
																		"version": 0
																	},
																	"stamp": 24
																}
															},
															"stamp": 23,
															"data": []
														}
													},
													"stamp": 22,
													"data": []
												}
											},
											"stamp": 21,
											"data": []
										}
									},
									"stamp": 11,
									"data": []
								}
							},
							"stamp": 6,
							"data": []
						}
					},
					"stamp": 5,
					"data": []
				},
				"form-footer": {
					"type": "FlexibleFormFooter",
					"options": {
						"splitterType": "simple-h",
						"version": 0,
						"maxwidth": ""
					},
					"*": {
						"Save": {
							"type": "SubmitButton",
							"options": {
								"label": "_Save",
								"action": "save",
								"submit": true,
								"version": 0
							},
							"stamp": 26,
							"data": []
						},
						"Approve": {
							"type": "SubmitButton",
							"options": {
								"label": "_Approve",
								"action": "approve",
								"submit": true,
								"version": 0
							},
							"stamp": 27,
							"data": []
						},
						"Reject": {
							"type": "SubmitButton",
							"options": {
								"label": "_Reject",
								"action": "reject",
								"submit": true,
								"version": 0
							},
							"stamp": 28,
							"data": []
						},
						"Reprocess": {
							"type": "SubmitButton",
							"options": {
								"label": "_Reprocess",
								"action": "reprocess",
								"submit": true,
								"version": 0
							},
							"stamp": 29,
							"data": []
						},
						"Close": {
							"type": "SubmitButton",
							"options": {
								"label": "_Close",
								"action": "cancel",
								"submit": true,
								"version": 0
							},
							"stamp": 30,
							"data": []
						}
					},
					"stamp": 25,
					"data": []
				}
			},
			"stamp": 3,
			"data": []
		}
	},
	"stamps": 56,
	"data": []
}