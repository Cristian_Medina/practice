
///#GLOBALS Sys Lib

function GetPORuidEX(line)
{
	// remove " from line
	return line.replace(/"/g, "");
}

var reader = Sys.Helpers.Attach.getReader(0);
var line;

// Skip first line if CSV has header
reader.getLine();

while ((line = reader.getLine()) !== null)
{
	var ruidEx = GetPORuidEX(line);
	var poTransport = Process.GetUpdatableTransportAsProcessAdmin(ruidEx);
	poTransport.ResumeWithAction("SynchronizeItems");
}
