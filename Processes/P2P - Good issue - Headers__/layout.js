{
	"*": {
		"Screen": {
			"type": "FlexibleFormLayout",
			"options": {
				"splitterType": "simple-v",
				"version": 0
			},
			"*": {
				"form-header": {
					"type": "FlexibleFormHeader",
					"options": {
						"hidePanelsMenu": false,
						"version": 0
					},
					"stamp": 2,
					"data": []
				},
				"form-center": {
					"type": "FlexibleFormCenter",
					"options": {
						"version": 0
					},
					"*": {
						"form-content": {
							"type": "FlexibleFormContent",
							"options": {
								"version": 0
							},
							"*": {
								"form-content-center": {
									"type": "FlexibleFormSplitter",
									"options": {
										"version": 1,
										"box": {
											"top": "0%",
											"left": "0%",
											"width": "100%"
										},
										"hidden": false
									},
									"*": {
										"form-content-center-1": {
											"type": "FlexibleFormLine",
											"data": [],
											"options": {},
											"stamp": 9,
											"*": {
												"DataPanel": {
													"type": "PanelData",
													"options": {
														"border": {},
														"version": 0,
														"labelLength": 0,
														"iconURL": "",
														"TitleFontSize": "S",
														"hideActionButtons": false,
														"hideTitleBar": false,
														"hideTitle": false,
														"backgroundcolor": "default",
														"labelsAlignment": "right",
														"label": "_Document data"
													},
													"*": {
														"FieldsManager": {
															"type": "FieldsManager",
															"options": {
																"controlsAssociation": {
																	"VendorNumber__": "LabelVendorNumber__",
																	"LabelVendorNumber__": "VendorNumber__",
																	"SettlementNumber__": "LabelSettlementNumber__",
																	"LabelSettlementNumber__": "SettlementNumber__",
																	"GoodIssueDate__": "LabelGoodIssueDate__",
																	"LabelGoodIssueDate__": "GoodIssueDate__",
																	"GoodIssueAmount__": "LabelGoodIssueAmount__",
																	"LabelGoodIssueAmount__": "GoodIssueAmount__",
																	"CompanyCode__": "LabelCompanyCode__",
																	"LabelCompanyCode__": "CompanyCode__",
																	"GoodIssueNumber__": "LabelGoodIssueNumber__",
																	"LabelGoodIssueNumber__": "GoodIssueNumber__"
																},
																"version": 0
															},
															"*": {
																"Grid": {
																	"type": "GridLayout",
																	"options": {
																		"lines": 6,
																		"columns": 2,
																		"version": 0,
																		"ctrlsPos": {
																			"VendorNumber__": {
																				"line": 2,
																				"column": 2
																			},
																			"LabelVendorNumber__": {
																				"line": 2,
																				"column": 1
																			},
																			"SettlementNumber__": {
																				"line": 6,
																				"column": 2
																			},
																			"LabelSettlementNumber__": {
																				"line": 6,
																				"column": 1
																			},
																			"GoodIssueDate__": {
																				"line": 4,
																				"column": 2
																			},
																			"LabelGoodIssueDate__": {
																				"line": 4,
																				"column": 1
																			},
																			"GoodIssueAmount__": {
																				"line": 5,
																				"column": 2
																			},
																			"LabelGoodIssueAmount__": {
																				"line": 5,
																				"column": 1
																			},
																			"CompanyCode__": {
																				"line": 1,
																				"column": 2
																			},
																			"LabelCompanyCode__": {
																				"line": 1,
																				"column": 1
																			},
																			"GoodIssueNumber__": {
																				"line": 3,
																				"column": 2
																			},
																			"LabelGoodIssueNumber__": {
																				"line": 3,
																				"column": 1
																			}
																		},
																		"colspans": [
																			[],
																			[],
																			[],
																			[],
																			[],
																			[]
																		]
																	},
																	"data": [
																		"fields"
																	],
																	"*": {
																		"LabelCompanyCode__": {
																			"type": "Label",
																			"data": [
																				"CompanyCode__"
																			],
																			"options": {
																				"label": "_Company code"
																			},
																			"stamp": 30
																		},
																		"CompanyCode__": {
																			"type": "DatabaseComboBox",
																			"data": [
																				"CompanyCode__"
																			],
																			"options": {
																				"version": 1,
																				"label": "_Company code",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"minNbLines": 1,
																				"maxNbLines": 1,
																				"fTSmaxRecords": "20",
																				"browsable": true
																			},
																			"stamp": 31
																		},
																		"LabelVendorNumber__": {
																			"type": "Label",
																			"data": [
																				"VendorNumber__"
																			],
																			"options": {
																				"label": "_Vendor number"
																			},
																			"stamp": 14
																		},
																		"VendorNumber__": {
																			"type": "DatabaseComboBox",
																			"data": [
																				"VendorNumber__"
																			],
																			"options": {
																				"version": 1,
																				"label": "_Vendor number",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"minNbLines": 1,
																				"maxNbLines": 1,
																				"fTSmaxRecords": "20",
																				"browsable": true
																			},
																			"stamp": 15
																		},
																		"LabelGoodIssueNumber__": {
																			"type": "Label",
																			"data": [
																				"GoodIssueNumber__"
																			],
																			"options": {
																				"label": "_Good issue number"
																			},
																			"stamp": 32
																		},
																		"GoodIssueNumber__": {
																			"type": "ShortText",
																			"data": [
																				"GoodIssueNumber__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"version": 1,
																				"label": "_Good issue number",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"browsable": false
																			},
																			"stamp": 33
																		},
																		"LabelGoodIssueDate__": {
																			"type": "Label",
																			"data": [
																				"GoodIssueDate__"
																			],
																			"options": {
																				"label": "_Good issue date"
																			},
																			"stamp": 26
																		},
																		"GoodIssueDate__": {
																			"type": "DateTime",
																			"data": [
																				"GoodIssueDate__"
																			],
																			"options": {
																				"displayLongFormat": false,
																				"label": "_Good issue date",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format"
																			},
																			"stamp": 27
																		},
																		"LabelGoodIssueAmount__": {
																			"type": "Label",
																			"data": [
																				"GoodIssueAmount__"
																			],
																			"options": {
																				"label": "_Good issue amount"
																			},
																			"stamp": 28
																		},
																		"GoodIssueAmount__": {
																			"type": "Decimal",
																			"data": [
																				"GoodIssueAmount__"
																			],
																			"options": {
																				"version": 2,
																				"textSize": "S",
																				"textAlignment": "right",
																				"textStyle": "default",
																				"textColor": "default",
																				"integer": false,
																				"label": "_Good issue amount",
																				"precision_internal": 2,
																				"precision_current": 2,
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format"
																			},
																			"stamp": 29
																		},
																		"LabelSettlementNumber__": {
																			"type": "Label",
																			"data": [
																				"SettlementNumber__"
																			],
																			"options": {
																				"label": "_Settlement number"
																			},
																			"stamp": 22
																		},
																		"SettlementNumber__": {
																			"type": "ShortText",
																			"data": [
																				"SettlementNumber__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"version": 1,
																				"label": "_Settlement number",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"browsable": false
																			},
																			"stamp": 23
																		}
																	},
																	"stamp": 8
																}
															},
															"stamp": 7,
															"data": []
														}
													},
													"stamp": 6,
													"data": []
												}
											}
										}
									},
									"stamp": 5,
									"data": []
								}
							},
							"stamp": 4,
							"data": []
						}
					},
					"stamp": 3,
					"data": []
				},
				"form-footer": {
					"type": "FlexibleFormFooter",
					"options": {
						"splitterType": "simple-h",
						"version": 0
					},
					"*": {
						"Save": {
							"type": "SubmitButton",
							"options": {
								"label": "_Save",
								"action": "savequit",
								"submit": true,
								"version": 0,
								"shortCut": "S"
							},
							"stamp": 11,
							"data": []
						},
						"Close": {
							"type": "SubmitButton",
							"options": {
								"label": "_Close",
								"action": "cancel",
								"submit": true,
								"version": 0,
								"shortCut": "Q"
							},
							"stamp": 12,
							"data": []
						},
						"Delete": {
							"type": "SubmitButton",
							"options": {
								"label": "_Delete",
								"action": "delete",
								"submit": true,
								"version": 0,
								"disabled": true
							},
							"stamp": 13,
							"data": []
						}
					},
					"stamp": 10,
					"data": []
				}
			},
			"stamp": 1,
			"data": []
		}
	},
	"stamps": 33,
	"data": []
}