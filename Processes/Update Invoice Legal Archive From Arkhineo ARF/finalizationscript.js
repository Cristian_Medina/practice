///#GLOBALS Lib
var options = {
	maxElementsPerRecall: 50
};
Process.SetTimeOut(3600);
var NOT_FINISHED = 0;
if (Lib.UpdateArkhineoArchive.updateArchiveProcessFromDocument(options) === NOT_FINISHED)
{
	Process.RecallScript("update_arkhineo_archive_in_progress", true);
}