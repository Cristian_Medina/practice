{
	"*": {
		"Screen": {
			"type": "FlexibleFormLayout",
			"options": {
				"splitterType": "simple-v",
				"version": 0
			},
			"*": {
				"form-header": {
					"type": "FlexibleFormHeader",
					"options": {
						"hidePanelsMenu": false,
						"version": 0
					},
					"stamp": 2,
					"data": []
				},
				"form-center": {
					"type": "FlexibleFormCenter",
					"options": {
						"version": 0
					},
					"*": {
						"form-content": {
							"type": "FlexibleFormContent",
							"options": {
								"version": 0
							},
							"*": {
								"form-content-center": {
									"type": "FlexibleFormSplitter",
									"options": {
										"version": 1,
										"box": {
											"top": "0%",
											"left": "0%",
											"width": "100%"
										},
										"hidden": false
									},
									"*": {
										"form-content-center-1": {
											"type": "FlexibleFormLine",
											"data": [],
											"options": {},
											"stamp": 9,
											"*": {
												"DataPanel": {
													"type": "PanelData",
													"options": {
														"border": {},
														"version": 0,
														"labelLength": 0,
														"iconURL": "",
														"TitleFontSize": "S",
														"hideActionButtons": false,
														"hideTitleBar": false,
														"hideTitle": false,
														"backgroundcolor": "default",
														"labelsAlignment": "right",
														"label": "_Document data"
													},
													"*": {
														"FieldsManager": {
															"type": "FieldsManager",
															"options": {
																"controlsAssociation": {
																	"ToolRuidEx__": "LabelToolRuidEx__",
																	"LabelToolRuidEx__": "ToolRuidEx__",
																	"RequestTimestamp__": "LabelRequestTimestamp__",
																	"LabelRequestTimestamp__": "RequestTimestamp__",
																	"DocumentRuidEx__": "LabelDocumentRuidEx__",
																	"LabelDocumentRuidEx__": "DocumentRuidEx__",
																	"Status__": "LabelStatus__",
																	"LabelStatus__": "Status__",
																	"Details__": "LabelDetails__",
																	"LabelDetails__": "Details__"
																},
																"version": 0
															},
															"*": {
																"Grid": {
																	"type": "GridLayout",
																	"options": {
																		"lines": 5,
																		"columns": 2,
																		"version": 0,
																		"ctrlsPos": {
																			"ToolRuidEx__": {
																				"line": 1,
																				"column": 2
																			},
																			"LabelToolRuidEx__": {
																				"line": 1,
																				"column": 1
																			},
																			"RequestTimestamp__": {
																				"line": 2,
																				"column": 2
																			},
																			"LabelRequestTimestamp__": {
																				"line": 2,
																				"column": 1
																			},
																			"DocumentRuidEx__": {
																				"line": 3,
																				"column": 2
																			},
																			"LabelDocumentRuidEx__": {
																				"line": 3,
																				"column": 1
																			},
																			"Status__": {
																				"line": 4,
																				"column": 2
																			},
																			"LabelStatus__": {
																				"line": 4,
																				"column": 1
																			},
																			"Details__": {
																				"line": 5,
																				"column": 2
																			},
																			"LabelDetails__": {
																				"line": 5,
																				"column": 1
																			}
																		},
																		"colspans": [
																			[],
																			[],
																			[],
																			[],
																			[]
																		]
																	},
																	"data": [
																		"fields"
																	],
																	"*": {
																		"LabelToolRuidEx__": {
																			"type": "Label",
																			"data": [
																				"ToolRuidEx__"
																			],
																			"options": {
																				"label": "_ToolRuidEx"
																			},
																			"stamp": 14
																		},
																		"ToolRuidEx__": {
																			"type": "ShortText",
																			"data": [
																				"ToolRuidEx__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"version": 1,
																				"label": "_ToolRuidEx",
																				"activable": true,
																				"width": "250",
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"browsable": false,
																				"autocompletable": false
																			},
																			"stamp": 15
																		},
																		"LabelRequestTimestamp__": {
																			"type": "Label",
																			"data": [
																				"RequestTimestamp__"
																			],
																			"options": {
																				"label": "_RequestTimestamp"
																			},
																			"stamp": 16
																		},
																		"RequestTimestamp__": {
																			"type": "ShortText",
																			"data": [
																				"RequestTimestamp__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"version": 1,
																				"label": "_RequestTimestamp",
																				"activable": true,
																				"width": "250",
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"browsable": false,
																				"autocompletable": false
																			},
																			"stamp": 17
																		},
																		"LabelDocumentRuidEx__": {
																			"type": "Label",
																			"data": [
																				"DocumentRuidEx__"
																			],
																			"options": {
																				"label": "_DocumentRuidEx"
																			},
																			"stamp": 18
																		},
																		"DocumentRuidEx__": {
																			"type": "ShortText",
																			"data": [
																				"DocumentRuidEx__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"version": 1,
																				"label": "_DocumentRuidEx",
																				"activable": true,
																				"width": "250",
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"browsable": false,
																				"autocompletable": false
																			},
																			"stamp": 19
																		},
																		"LabelStatus__": {
																			"type": "Label",
																			"data": [
																				"Status__"
																			],
																			"options": {
																				"label": "_Status"
																			},
																			"stamp": 20
																		},
																		"Status__": {
																			"type": "ComboBox",
																			"data": [
																				"Status__"
																			],
																			"options": {
																				"possibleValues": {
																					"0": "_Pending",
																					"1": "_Success",
																					"2": "_Error"
																				},
																				"version": 1,
																				"keyValueMode": true,
																				"possibleKeys": {
																					"0": "0",
																					"1": "1",
																					"2": "2"
																				},
																				"label": "_Status",
																				"activable": true,
																				"width": "250",
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"DataSourceOrigin": "Standalone",
																				"DisplayedColumns": ""
																			},
																			"stamp": 21
																		},
																		"LabelDetails__": {
																			"type": "Label",
																			"data": [
																				"Details__"
																			],
																			"options": {
																				"label": "_Details"
																			},
																			"stamp": 22
																		},
																		"Details__": {
																			"type": "LongText",
																			"data": [
																				"Details__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"version": 1,
																				"minNbLines": 10,
																				"label": "_Details",
																				"activable": true,
																				"width": "800",
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"numberOfLines": 10,
																				"browsable": false
																			},
																			"stamp": 23
																		}
																	},
																	"stamp": 8
																}
															},
															"stamp": 7,
															"data": []
														}
													},
													"stamp": 6,
													"data": []
												}
											}
										}
									},
									"stamp": 5,
									"data": []
								}
							},
							"stamp": 4,
							"data": []
						}
					},
					"stamp": 3,
					"data": []
				},
				"form-footer": {
					"type": "FlexibleFormFooter",
					"options": {
						"splitterType": "simple-h",
						"version": 0
					},
					"*": {
						"Save": {
							"type": "SubmitButton",
							"options": {
								"label": "_Save",
								"action": "savequit",
								"submit": true,
								"version": 0,
								"shortCut": "S"
							},
							"stamp": 11,
							"data": []
						},
						"Close": {
							"type": "SubmitButton",
							"options": {
								"label": "_Close",
								"action": "cancel",
								"submit": true,
								"version": 0,
								"shortCut": "Q"
							},
							"stamp": 12,
							"data": []
						},
						"Delete": {
							"type": "SubmitButton",
							"options": {
								"label": "_Delete",
								"action": "delete",
								"submit": true,
								"version": 0,
								"disabled": true
							},
							"stamp": 13,
							"data": []
						}
					},
					"stamp": 10,
					"data": []
				}
			},
			"stamp": 1,
			"data": []
		}
	},
	"stamps": 23,
	"data": []
}