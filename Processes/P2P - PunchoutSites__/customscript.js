///#GLOBALS Lib Sys
var g_topMessageWarning = Lib.P2P.TopMessageWarning(Controls.TopPaneWarning, Controls.TopMessageWarning__);

// Hide this button which has been removed in S149
if (Controls.ShowSecret__)
{
	Controls.ShowSecret__.Hide(true);
}

// Hide this button which has been removed in S149
if (Controls.Currency__)
{
	Controls.Currency__.Hide(true);
}

var idUrlTrustedWithSpinnerByControlName = {};
// Trusted local cache. true/false
var isTrustedByControlName = {};

var checkUrlsTrustedCallbackId = 0;
// Hide "buyer email" part
Controls.Spacer_line2__.Hide(true);
Controls.User__.Hide(true);

// Hide fields in test part
Controls.TestLogin__.Hide(true);
Controls.TestName__.Hide(true);
Controls.TestRequest__.Hide(true);
Controls.TestResponse__.Hide(true);
Controls.TestPunchout__.SetDisabled(true);
Controls.TestPunchout__.SetText("_TestPunchout");
Controls.OverrideCredentials__.SetHelpData(Language.Translate("_OverrideCredentials tooltip"));
CheckUrlsTrusted();

Controls.Enabled__.OnChange = function()
{
	CheckUrlsTrusted();
};

Controls.PunchoutOrderURL__.OnChange = function ()
{
	isTrustedByControlName[this.GetName()] = null;
	CheckUrlsTrusted();
};

Controls.PunchoutURL__.OnChange = function ()
{
	isTrustedByControlName[this.GetName()] = null;
	CheckUrlsTrusted();
};

Controls.SenderIdentity__.OnChange = function ()
{
	UpdatePunchoutTestButton();
};

Controls.Identity__.OnChange = function ()
{
	UpdatePunchoutTestButton();
};

Controls.Secret__.OnChange = function ()
{
	UpdatePunchoutTestButton();
};

Controls.OverrideCredentials__.OnChange = function ()
{
	if (Controls.OverrideCredentials__.GetValue())
	{
		Controls.TestLogin__.Hide(false);
		Controls.TestName__.Hide(false);
	}
	else
	{
		Controls.TestLogin__.Hide(true);
		Controls.TestName__.Hide(true);
	}
};

Controls.TestPunchout__.OnClick = function ()
{
	Controls.TestRequest__.SetHTML("");
	Controls.TestResponse__.SetHTML("");
	Controls.TestStatus__.SetValue("");
	Controls.TestName__.SetReadOnly(true);
	Controls.TestLogin__.SetReadOnly(true);
	Controls.TestPunchout__.SetDisabled(true);
	Controls.TestStatus__.SetImageURL("loading.gif");

	var user = "";
	var email = "";

	if (Controls.OverrideCredentials__.GetValue())
	{
		user = Controls.TestName__.GetValue();
		email = Controls.TestLogin__.GetValue();
	}

	var config = {
		user: user,
		email: email,
		TestMode__: Controls.TestMode__.GetValue(),
		PunchoutURL__: Controls.PunchoutURL__.GetValue(),
		SenderDomain__: Controls.SenderDomain__.GetValue(),
		SenderIdentity__: Controls.SenderIdentity__.GetValue(),
		Domain__: Controls.Domain__.GetValue(),
		Identity__:  Controls.Identity__.GetValue(),
		Secret__: Controls.Secret__.GetValue()
	};

	var onPunchoutOpenResult = Sys.Helpers.TryCallFunction("Lib.PR.Customization.Client.OnPunchoutOpen");
	if (onPunchoutOpenResult)
	{
		config.extrinsic = onPunchoutOpenResult.extrinsic;
	}

	Punchout.Test(config,
		function (data)
		{
			var result;
			if (data.httpRequest)
			{
				result = {};
				HighlightSpecialChars(data.httpRequest, result);
				Controls.TestRequest__.SetHTML(result.html);
				Controls.TestRequest__.SetCSS(result.css);
			}
			if (data.httpResponse)
			{
				result = {};
				HighlightSpecialChars(data.httpResponse, result);
				Controls.TestResponse__.SetHTML(result.html);
				Controls.TestResponse__.SetCSS(result.css);
			}

			if (!data.error)
			{
				Controls.TestStatus__.RemoveStyle("highlight-danger");
				Controls.TestStatus__.AddStyle("highlight-success");
				Controls.TestStatus__.SetValue(Language.Translate("_Success"));
			}
			else
			{
				Controls.TestStatus__.RemoveStyle("highlight-success");
				Controls.TestStatus__.AddStyle("highlight-danger");

				var errorMessage = Language.Translate("_Fail") + "\n" + data.error;
				if (data.errorMessage)
				{
					errorMessage += "\n" + data.errorMessage;
				}

				Controls.TestStatus__.SetValue(errorMessage);
			}

			Controls.TestName__.SetReadOnly(false);
			Controls.TestLogin__.SetReadOnly(false);
			Controls.TestPunchout__.SetDisabled(false);
			Controls.TestStatus__.SetImageURL();
		});

	Controls.TestStatus__.Hide(false);
	Controls.TestRequest__.Hide(false);
	Controls.TestResponse__.Hide(false);
};

function CheckUrlsTrusted()
{
	checkUrlsTrustedCallbackId++;

	// Set some fields to required if we try to enable the configuration
	var isConfEnabled = Controls.Enabled__.GetValue();
	Controls.CompanyCode__.SetRequired(isConfEnabled);
	Controls.ConfigurationName__.SetRequired(isConfEnabled);
	Controls.Identity__.SetRequired(isConfEnabled);
	Controls.LogoURL__.SetRequired(isConfEnabled);
	Controls.Secret__.SetRequired(isConfEnabled);
	Controls.SenderIdentity__.SetRequired(isConfEnabled);
	Controls.SupplierID__.SetRequired(isConfEnabled);

	var controls = [];
	if (!Sys.Helpers.IsEmpty(Controls.PunchoutURL__.GetValue()))
	{
		controls.push(Controls.PunchoutURL__);
	}
	if (!Sys.Helpers.IsEmpty(Controls.PunchoutOrderURL__.GetValue()))
	{
		controls.push(Controls.PunchoutOrderURL__);
	}
	if (controls.length === 0)
	{
		return;
	}

	Sys.Helpers.Array.ForEach(controls, function (control)
	{
		control.SetImageURL();
	});

	Controls.TestPunchout__.SetDisabled(true); // Test button will be reenabled if urls are trusted

	if (Controls.Enabled__.GetValue() === false)
	{
		Sys.Helpers.Array.ForEach(controls, function(control) {
			control.SetError();
			control.SetImageURL();
		});
		Controls.Save.SetDisabled(false);
		g_topMessageWarning.Clear();
		return;
	}

	Controls.Save.SetDisabled(true);

	var promises = Sys.Helpers.Array.Map(controls, function (control) {
		return CheckUrlTrustedWithSpinner(control);
	});

	Sys.Helpers.Synchronizer.All(promises)
		.Then(
			function () {
				var id = checkUrlsTrustedCallbackId;
				return function (items) {
					if (id != checkUrlsTrustedCallbackId) {
						Log.Info("Ignored previous CheckUrlsTrusted result");
						return;
					}

					var allTrusted = true;
					for (var i in items) {
						var isTrusted = items[i];
						var control = controls[i];

						if (isTrusted)
						{
							control.SetError();
						}
						else
						{
							allTrusted = false;
							control.SetError("_URL is not trusted");
						}
					}

					if (allTrusted)
					{
						Log.Info("All urls trusted");
						g_topMessageWarning.Clear();
						Controls.Save.SetDisabled(false);
						UpdatePunchoutTestButton();
					}
					else
					{
						Log.Error("Some URL are not trusted");
						g_topMessageWarning.Clear();
						g_topMessageWarning.Add(Language.Translate("_Some urls are not trusted"));
						Controls.TestPunchout__.SetDisabled(true);
					}
				}
			}()
		);
}

function CheckUrlTrustedWithSpinner(control)
{
	if (typeof idUrlTrustedWithSpinnerByControlName[control.GetName()] == "undefined")
	{
		idUrlTrustedWithSpinnerByControlName[control.GetName()] = 0;
	}
	else
	{
		idUrlTrustedWithSpinnerByControlName[control.GetName()]++;
	}

	return Sys.Helpers.Promise.Create(function(resolve, reject)
	{
		var promise;
		if (typeof isTrustedByControlName[control.GetName()] === "boolean")
		{
			promise = Sys.Helpers.Promise.Resolve(isTrustedByControlName[control.GetName()]);
		}
		else if (control.GetValue() == null && control.GetValue() == "")
		{
			promise = Sys.Helpers.Promise.Resolve(false);
		}
		else
		{
			control.SetImageURL("loading.gif");
			promise = CheckUrlTrusted(control.GetValue());
		}

		promise
			.Then(function(id) {
				var id = idUrlTrustedWithSpinnerByControlName[control.GetName()];
				return function (isTrusted) {
					if (id != idUrlTrustedWithSpinnerByControlName[control.GetName()]) {
						Log.Info("Ignore previous CheckControlUrlTrusted result");
						return;
					}
					isTrustedByControlName[control.GetName()] = isTrusted;
					control.SetImageURL();
					resolve(isTrusted);
				}
			}());
	});
}

function CheckUrlTrusted(targetUrl)
{
	return Sys.Helpers.Promise.Create(function (resolve, reject) {
		Query.IsURLTrusted({
			targetURL: targetUrl,
			callback: function (result)
			{
				resolve(result.isTrusted);
			}
		});
	});
}

function UpdatePunchoutTestButton()
{
	if (Controls.PunchoutURL__.GetValue() === "" ||
		Controls.SenderIdentity__.GetValue() === "" ||
		Controls.Identity__.GetValue() === "" ||
		Controls.Secret__.GetValue() === "" ||
		isTrustedByControlName["PunchoutURL__"] != true)
	{
		Controls.TestPunchout__.SetDisabled(true);
	}
	else
	{
		Controls.TestPunchout__.SetDisabled(false);
	}
}

var dangerousChars = /[\u0000-\u001f\u007f-\u009f\u00ad\u061c\u200b-\u200f\u2028\u2029\u00A0\ufeff\ufff9-\ufffc]/g;
// \u200b => zero width space, the worst :)
// \u00ad => Soft hyphen
// \u00A0 => Non-breaking space (added from list)

var specialChars = {
	'\r': { text: "CR", help: "CR" },
	'\n': { text: "LF", help: "LF" },
	'\t': { text: "→",  help: "TAB" }	
};

// Transforms special chars of the given string into a result containing html and css
function HighlightSpecialChars(rawStr, result) {

	var html = "<pre>";
	var cssText = {}; // map char code => text to display with css beside (e.g. 13 => "CR")

	for (var i = 0; i < rawStr.length; i++)
	{
		var specialChar = specialChars[rawStr[i]];
		var helpText;
		if (specialChar)
		{
			helpText = "&#" + rawStr.charCodeAt(i) + " " + specialChar.help;
			html += "<span class='badge badge-small text" + rawStr.charCodeAt(i) +"' title='" + escapeHtml(helpText) + "'>" + escapeHtml(rawStr[i]) + "</span>";
			cssText[rawStr.charCodeAt(i)] = specialChar.text;

			if (rawStr[i] == '\n')
			{
				html += "<br/>";
			}
		}
		else if (rawStr[i].match(dangerousChars))
		{
			helpText = "&#" + rawStr.charCodeAt(i);
			html += "<span class='badge badge-small badge-error text" + rawStr.charCodeAt(i) + "' title='" + escapeHtml(helpText) + "'>" + escapeHtml(rawStr[i]) + "</span>";
			cssText[rawStr.charCodeAt(i)] = '0x' + rawStr.charCodeAt(i).toString(16);
		}
		else
		{
			html += escapeHtml(rawStr[i]);
		}
	}

	html += "</pre>";

	result.html = html;

	result.css = 
"pre {\n\
	width: 750px;\n\
	white-space: pre-wrap;\n\
	word-break: break-all;\n\
	word-wrap: break-word;\n\
	line-break: anywhere;\n\
	margin: 5px 0px\n\
}\n\
\n\
.badge {\n\
	display: inline-block;\n\
	padding: .25em .4em;\n\
	margin: 0 .1em;\n\
	font-weight: 700;\n\
	line-height: 1;\n\
	text-align: center;\n\
	white-space: nowrap;\n\
	vertical-align: baseline;\n\
	border-radius: .25rem;\n\
\n\
	color: #000;\n\
	background-color: #E0E2E5;\n\
}\n\
\n\
.badge-small {\n\
	font-size: 75%;\n\
}\n\
\n\
.badge-error {\n\
	color: #fff;\n\
	background-color: #a94442\n\
}\n\
\n\
";

	for (i in cssText) {
		result.css += ".text" + i + "::after {\n\
content: '" + cssText[i] + "';\n\
}\n\
";
	}
}

// List of HTML entities for escaping.
var htmlEscapes = {
	'&': '&amp;',
	'<': '&lt;',
	'>': '&gt;',
	'"': '&quot;',
	"'": '&#x27;',
	'/': '&#x2F;'
};
  
// Regex containing the keys listed immediately above.
var htmlEscaper = /[&<>"'\/]/g;

// Escape a string for HTML interpolation.
function escapeHtml(string) {
	return ('' + string).replace(htmlEscaper, function(match) {
		return htmlEscapes[match];
	});
};