{
	"*": {
		"Screen": {
			"type": "FlexibleFormLayout",
			"options": {
				"splitterType": "simple-v",
				"version": 0,
				"backColor": "text-backgroundcolor-default",
				"style": null
			},
			"*": {
				"form-header": {
					"type": "FlexibleFormHeader",
					"options": {
						"hidePanelsMenu": false,
						"version": 0
					},
					"stamp": 2,
					"data": []
				},
				"form-center": {
					"type": "FlexibleFormCenter",
					"options": {
						"version": 0
					},
					"*": {
						"form-content": {
							"type": "FlexibleFormContent",
							"options": {
								"version": 0
							},
							"*": {
								"form-content-center": {
									"type": "FlexibleFormSplitter",
									"options": {
										"version": 1,
										"box": {
											"top": "0%",
											"left": "0%",
											"width": "100%"
										},
										"hidden": false,
										"stickypanel": "TopPaneWarning"
									},
									"*": {
										"form-content-center-3": {
											"type": "FlexibleFormLine",
											"data": [],
											"options": {
												"version": 0
											},
											"stamp": 79,
											"*": {
												"TopPaneWarning": {
													"type": "PanelData",
													"data": [],
													"options": {
														"border": {
															"collapsed": false
														},
														"labelLength": 0,
														"iconURL": "",
														"TitleFontSize": "S",
														"hideActionButtons": false,
														"hideTitleBar": true,
														"hideTitle": false,
														"backgroundcolor": "color3",
														"labelsAlignment": "right",
														"label": "TopPaneWarning",
														"leftImageURL": "",
														"removeMargins": false,
														"elementsAlignment": "left",
														"hidden": true,
														"version": 0,
														"sameHeightAsSiblings": false
													},
													"stamp": 76,
													"*": {
														"FieldsManager": {
															"type": "FieldsManager",
															"data": [],
															"options": {
																"controlsAssociation": {},
																"version": 0
															},
															"stamp": 77,
															"*": {
																"Grid": {
																	"type": "GridLayout",
																	"data": [
																		"fields"
																	],
																	"options": {
																		"ctrlsPos": {
																			"TopMessageWarning__": {
																				"line": 1,
																				"column": 1
																			}
																		},
																		"lines": 1,
																		"columns": 2,
																		"colspans": [
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			]
																		],
																		"version": 0
																	},
																	"stamp": 78,
																	"*": {
																		"TopMessageWarning__": {
																			"type": "HTML",
																			"data": false,
																			"options": {
																				"label": "TopMessageWarning__",
																				"width": "100%",
																				"version": 0
																			},
																			"stamp": 82
																		}
																	}
																}
															}
														}
													}
												}
											}
										},
										"form-content-center-1": {
											"type": "FlexibleFormLine",
											"data": [],
											"options": {
												"version": 0
											},
											"stamp": 9,
											"*": {
												"DataPanel": {
													"type": "PanelData",
													"options": {
														"border": {},
														"version": 0,
														"labelLength": 0,
														"iconURL": "",
														"TitleFontSize": "S",
														"hideActionButtons": false,
														"hideTitleBar": true,
														"hideTitle": false,
														"backgroundcolor": "default",
														"labelsAlignment": "right",
														"label": "_Document data",
														"leftImageURL": "",
														"removeMargins": false,
														"elementsAlignment": "left",
														"sameHeightAsSiblings": false
													},
													"*": {
														"FieldsManager": {
															"type": "FieldsManager",
															"options": {
																"controlsAssociation": {
																	"ConfigurationName__": "LabelConfigurationName__",
																	"LabelConfigurationName__": "ConfigurationName__",
																	"LogoURL__": "LabelLogoURL__",
																	"LabelLogoURL__": "LogoURL__",
																	"SenderDomain__": "LabelSenderDomain__",
																	"LabelSenderDomain__": "SenderDomain__",
																	"SenderIdentity__": "LabelSenderIdentity__",
																	"LabelSenderIdentity__": "SenderIdentity__",
																	"Domain__": "LabelDomain__",
																	"LabelDomain__": "Domain__",
																	"Identity__": "LabelIdentity__",
																	"LabelIdentity__": "Identity__",
																	"Secret__": "LabelSecret__",
																	"LabelSecret__": "Secret__",
																	"PunchoutURL__": "LabelPunchoutURL__",
																	"LabelPunchoutURL__": "PunchoutURL__",
																	"User__": "LabelUser__",
																	"LabelUser__": "User__",
																	"CompanyCode__": "LabelCompanyCode__",
																	"LabelCompanyCode__": "CompanyCode__",
																	"SupplierID__": "LabelSupplierID__",
																	"LabelSupplierID__": "SupplierID__",
																	"SupplyTypeId__": "LabelSupplyTypeId__",
																	"LabelSupplyTypeId__": "SupplyTypeId__",
																	"PunchoutOrderURL__": "LabelPunchoutOrderURL__",
																	"LabelPunchoutOrderURL__": "PunchoutOrderURL__",
																	"Enabled__": "LabelEnabled__",
																	"LabelEnabled__": "Enabled__",
																	"TestMode__": "LabelTestMode__",
																	"LabelTestMode__": "TestMode__",
																	"TestLogin__": "LabelTestLogin__",
																	"LabelTestLogin__": "TestLogin__",
																	"TestName__": "LabelTestName__",
																	"LabelTestName__": "TestName__",
																	"TestPunchout__": "LabelTestPunchout__",
																	"LabelTestPunchout__": "TestPunchout__",
																	"TestRequest__": "LabelTestRequest__",
																	"LabelTestRequest__": "TestRequest__",
																	"TestResponse__": "LabelTestResponse__",
																	"LabelTestResponse__": "TestResponse__",
																	"OverrideCredentials__": "LabelOverrideCredentials__",
																	"LabelOverrideCredentials__": "OverrideCredentials__",
																	"TestStatus__": "LabelTestStatus__",
																	"LabelTestStatus__": "TestStatus__",
																	"DefaultLeadTime__": "LabelDefaultLeadTime__",
																	"LabelDefaultLeadTime__": "DefaultLeadTime__"
																},
																"version": 0
															},
															"*": {
																"Grid": {
																	"type": "GridLayout",
																	"options": {
																		"lines": 33,
																		"columns": 2,
																		"version": 0,
																		"ctrlsPos": {
																			"ConfigurationName__": {
																				"line": 4,
																				"column": 2
																			},
																			"LabelConfigurationName__": {
																				"line": 4,
																				"column": 1
																			},
																			"LogoURL__": {
																				"line": 9,
																				"column": 2
																			},
																			"LabelLogoURL__": {
																				"line": 9,
																				"column": 1
																			},
																			"SenderDomain__": {
																				"line": 17,
																				"column": 2
																			},
																			"LabelSenderDomain__": {
																				"line": 17,
																				"column": 1
																			},
																			"SenderIdentity__": {
																				"line": 18,
																				"column": 2
																			},
																			"LabelSenderIdentity__": {
																				"line": 18,
																				"column": 1
																			},
																			"Domain__": {
																				"line": 19,
																				"column": 2
																			},
																			"LabelDomain__": {
																				"line": 19,
																				"column": 1
																			},
																			"Identity__": {
																				"line": 20,
																				"column": 2
																			},
																			"LabelIdentity__": {
																				"line": 20,
																				"column": 1
																			},
																			"Secret__": {
																				"line": 21,
																				"column": 2
																			},
																			"LabelSecret__": {
																				"line": 21,
																				"column": 1
																			},
																			"PunchoutURL__": {
																				"line": 15,
																				"column": 2
																			},
																			"LabelPunchoutURL__": {
																				"line": 15,
																				"column": 1
																			},
																			"User__": {
																				"line": 23,
																				"column": 2
																			},
																			"LabelUser__": {
																				"line": 23,
																				"column": 1
																			},
																			"CompanyCode__": {
																				"line": 5,
																				"column": 2
																			},
																			"LabelCompanyCode__": {
																				"line": 5,
																				"column": 1
																			},
																			"SupplierID__": {
																				"line": 6,
																				"column": 2
																			},
																			"LabelSupplierID__": {
																				"line": 6,
																				"column": 1
																			},
																			"SupplyTypeId__": {
																				"line": 7,
																				"column": 2
																			},
																			"LabelSupplyTypeId__": {
																				"line": 7,
																				"column": 1
																			},
																			"Spacer_line__": {
																				"line": 11,
																				"column": 1
																			},
																			"Spacer_line2__": {
																				"line": 22,
																				"column": 1
																			},
																			"PunchoutOrderURL__": {
																				"line": 16,
																				"column": 2
																			},
																			"LabelPunchoutOrderURL__": {
																				"line": 16,
																				"column": 1
																			},
																			"Title__": {
																				"line": 1,
																				"column": 1
																			},
																			"Spacer_line3__": {
																				"line": 2,
																				"column": 1
																			},
																			"Spacer_line4__": {
																				"line": 13,
																				"column": 1
																			},
																			"NetworkAndCredentials__": {
																				"line": 12,
																				"column": 1
																			},
																			"Enabled__": {
																				"line": 3,
																				"column": 2
																			},
																			"LabelEnabled__": {
																				"line": 3,
																				"column": 1
																			},
																			"HTMLcontent__": {
																				"line": 14,
																				"column": 1
																			},
																			"TestMode__": {
																				"line": 10,
																				"column": 2
																			},
																			"LabelTestMode__": {
																				"line": 10,
																				"column": 1
																			},
																			"TestTile__": {
																				"line": 25,
																				"column": 1
																			},
																			"TestLogin__": {
																				"line": 28,
																				"column": 2
																			},
																			"LabelTestLogin__": {
																				"line": 28,
																				"column": 1
																			},
																			"TestName__": {
																				"line": 29,
																				"column": 2
																			},
																			"LabelTestName__": {
																				"line": 29,
																				"column": 1
																			},
																			"TestPunchout__": {
																				"line": 30,
																				"column": 2
																			},
																			"LabelTestPunchout__": {
																				"line": 30,
																				"column": 1
																			},
																			"TestRequest__": {
																				"line": 32,
																				"column": 2
																			},
																			"LabelTestRequest__": {
																				"line": 32,
																				"column": 1
																			},
																			"TestResponse__": {
																				"line": 33,
																				"column": 2
																			},
																			"LabelTestResponse__": {
																				"line": 33,
																				"column": 1
																			},
																			"Spacer__": {
																				"line": 24,
																				"column": 1
																			},
																			"Spacer2__": {
																				"line": 26,
																				"column": 1
																			},
																			"OverrideCredentials__": {
																				"line": 27,
																				"column": 2
																			},
																			"LabelOverrideCredentials__": {
																				"line": 27,
																				"column": 1
																			},
																			"TestStatus__": {
																				"line": 31,
																				"column": 2
																			},
																			"LabelTestStatus__": {
																				"line": 31,
																				"column": 1
																			},
																			"DefaultLeadTime__": {
																				"line": 8,
																				"column": 2
																			},
																			"LabelDefaultLeadTime__": {
																				"line": 8,
																				"column": 1
																			}
																		},
																		"colspans": [
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			],
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			],
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			],
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			],
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			],
																			[],
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			],
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			],
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[]
																		]
																	},
																	"data": [
																		"fields"
																	],
																	"*": {
																		"Title__": {
																			"type": "Title",
																			"data": false,
																			"options": {
																				"marge": 0,
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"backgroundcolor": "default",
																				"label": "_General",
																				"version": 0,
																				"iconClass": ""
																			},
																			"stamp": 64
																		},
																		"Spacer_line3__": {
																			"type": "Spacer",
																			"data": false,
																			"options": {
																				"height": "",
																				"width": "",
																				"color": "default",
																				"label": "Spacer line3",
																				"version": 0
																			},
																			"stamp": 65
																		},
																		"LabelEnabled__": {
																			"type": "Label",
																			"data": [
																				"Enabled__"
																			],
																			"options": {
																				"label": "_Enabled",
																				"version": 0
																			},
																			"stamp": 67
																		},
																		"Enabled__": {
																			"type": "CheckBox",
																			"data": [
																				"Enabled__"
																			],
																			"options": {
																				"label": "_Enabled",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"version": 0
																			},
																			"stamp": 68
																		},
																		"LabelConfigurationName__": {
																			"type": "Label",
																			"data": [
																				"ConfigurationName__"
																			],
																			"options": {
																				"label": "_ConfigurationName",
																				"version": 0
																			},
																			"stamp": 14
																		},
																		"ConfigurationName__": {
																			"type": "ShortText",
																			"data": [
																				"ConfigurationName__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "_ConfigurationName",
																				"activable": true,
																				"width": "230",
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"browsable": false,
																				"version": 0
																			},
																			"stamp": 15
																		},
																		"LabelCompanyCode__": {
																			"type": "Label",
																			"data": [
																				"CompanyCode__"
																			],
																			"options": {
																				"label": "_CompanyCode",
																				"version": 0
																			},
																			"stamp": 42
																		},
																		"CompanyCode__": {
																			"type": "DatabaseComboBox",
																			"data": [
																				"CompanyCode__"
																			],
																			"options": {
																				"version": 1,
																				"label": "_CompanyCode",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"fTSmaxRecords": "20",
																				"browsable": true,
																				"PreFillFTS": true,
																				"autoCompleteSearchMode": "startswith",
																				"searchMode": "matches"
																			},
																			"stamp": 43
																		},
																		"LabelSupplierID__": {
																			"type": "Label",
																			"data": [
																				"SupplierID__"
																			],
																			"options": {
																				"label": "_SupplierID",
																				"version": 0
																			},
																			"stamp": 44
																		},
																		"SupplierID__": {
																			"type": "DatabaseComboBox",
																			"data": [
																				"SupplierID__"
																			],
																			"options": {
																				"version": 1,
																				"label": "_SupplierID",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"fTSmaxRecords": "20",
																				"browsable": true,
																				"PreFillFTS": true,
																				"autoCompleteSearchMode": "startswith",
																				"searchMode": "matches"
																			},
																			"stamp": 45
																		},
																		"LabelSupplyTypeId__": {
																			"type": "Label",
																			"data": [
																				"SupplyTypeId__"
																			],
																			"options": {
																				"label": "_SupplyTypeId",
																				"version": 0
																			},
																			"stamp": 46
																		},
																		"SupplyTypeId__": {
																			"type": "DatabaseComboBox",
																			"data": [
																				"SupplyTypeId__"
																			],
																			"options": {
																				"version": 1,
																				"label": "_SupplyTypeId",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"fTSmaxRecords": "20",
																				"browsable": true,
																				"PreFillFTS": true,
																				"minNbLines": 1,
																				"maxNbLines": 1,
																				"RestrictSearch": true,
																				"autoCompleteSearchMode": "startswith",
																				"searchMode": "contains"
																			},
																			"stamp": 47
																		},
																		"LabelDefaultLeadTime__": {
																			"type": "Label",
																			"data": [
																				"DefaultLeadTime__"
																			],
																			"options": {
																				"label": "_DefaultLeadTime",
																				"version": 0
																			},
																			"stamp": 112
																		},
																		"DefaultLeadTime__": {
																			"type": "Integer",
																			"data": [
																				"DefaultLeadTime__"
																			],
																			"options": {
																				"version": 2,
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"integer": true,
																				"label": "_DefaultLeadTime",
																				"precision_internal": 0,
																				"precision_current": 0,
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"helpIconPosition": "Right",
																				"enablePlusMinus": false,
																				"browsable": false
																			},
																			"stamp": 113
																		},
																		"LabelLogoURL__": {
																			"type": "Label",
																			"data": [
																				"LogoURL__"
																			],
																			"options": {
																				"label": "_LogoURL",
																				"version": 0
																			},
																			"stamp": 18
																		},
																		"LogoURL__": {
																			"type": "ShortText",
																			"data": [
																				"LogoURL__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "_LogoURL",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"browsable": false,
																				"version": 0
																			},
																			"stamp": 19
																		},
																		"LabelTestMode__": {
																			"type": "Label",
																			"data": [
																				"TestMode__"
																			],
																			"options": {
																				"label": "_TestMode",
																				"version": 0
																			},
																			"stamp": 85
																		},
																		"TestMode__": {
																			"type": "CheckBox",
																			"data": [
																				"TestMode__"
																			],
																			"options": {
																				"label": "_TestMode",
																				"activable": true,
																				"width": 230,
																				"helpText": "_TestMode tooltip",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"version": 0
																			},
																			"stamp": 86
																		},
																		"Spacer_line__": {
																			"type": "Spacer",
																			"data": false,
																			"options": {
																				"height": "",
																				"width": "",
																				"color": "default",
																				"label": "Spacer line",
																				"version": 0
																			},
																			"stamp": 48
																		},
																		"NetworkAndCredentials__": {
																			"type": "Title",
																			"data": false,
																			"options": {
																				"marge": 0,
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"backgroundcolor": "default",
																				"label": "_Network and credentials",
																				"version": 0,
																				"iconClass": ""
																			},
																			"stamp": 63
																		},
																		"Spacer_line4__": {
																			"type": "Spacer",
																			"data": false,
																			"options": {
																				"height": "",
																				"width": "",
																				"color": "default",
																				"label": "Spacer line4",
																				"version": 0
																			},
																			"stamp": 66
																		},
																		"HTMLcontent__": {
																			"type": "HTML",
																			"data": false,
																			"options": {
																				"label": "HTMLcontent",
																				"width": "100%",
																				"version": 0
																			},
																			"stamp": 70
																		},
																		"LabelPunchoutURL__": {
																			"type": "Label",
																			"data": [
																				"PunchoutURL__"
																			],
																			"options": {
																				"label": "_PunchoutURL",
																				"version": 0
																			},
																			"stamp": 16
																		},
																		"PunchoutURL__": {
																			"type": "ShortText",
																			"data": [
																				"PunchoutURL__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "_PunchoutURL",
																				"activable": true,
																				"width": "500",
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"browsable": false,
																				"version": 0,
																				"length": 300
																			},
																			"stamp": 17
																		},
																		"LabelPunchoutOrderURL__": {
																			"type": "Label",
																			"data": [
																				"PunchoutOrderURL__"
																			],
																			"options": {
																				"label": "_PunchoutOrderURL",
																				"version": 0
																			},
																			"stamp": 56
																		},
																		"PunchoutOrderURL__": {
																			"type": "ShortText",
																			"data": [
																				"PunchoutOrderURL__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "_PunchoutOrderURL",
																				"activable": true,
																				"width": "500",
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"browsable": false,
																				"length": 300,
																				"version": 0
																			},
																			"stamp": 57
																		},
																		"LabelSenderDomain__": {
																			"type": "Label",
																			"data": [
																				"SenderDomain__"
																			],
																			"options": {
																				"label": "_SenderDomain",
																				"version": 0
																			},
																			"stamp": 22
																		},
																		"SenderDomain__": {
																			"type": "ShortText",
																			"data": [
																				"SenderDomain__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "_SenderDomain",
																				"activable": true,
																				"width": "500",
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"browsable": false,
																				"version": 0
																			},
																			"stamp": 23
																		},
																		"LabelSenderIdentity__": {
																			"type": "Label",
																			"data": [
																				"SenderIdentity__"
																			],
																			"options": {
																				"label": "_SenderIdentity",
																				"version": 0
																			},
																			"stamp": 24
																		},
																		"SenderIdentity__": {
																			"type": "ShortText",
																			"data": [
																				"SenderIdentity__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "_SenderIdentity",
																				"activable": true,
																				"width": "500",
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"browsable": false,
																				"version": 0
																			},
																			"stamp": 25
																		},
																		"LabelDomain__": {
																			"type": "Label",
																			"data": [
																				"Domain__"
																			],
																			"options": {
																				"label": "_Domain",
																				"version": 0
																			},
																			"stamp": 26
																		},
																		"Domain__": {
																			"type": "ShortText",
																			"data": [
																				"Domain__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "_Domain",
																				"activable": true,
																				"width": "500",
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"browsable": false,
																				"version": 0
																			},
																			"stamp": 27
																		},
																		"LabelIdentity__": {
																			"type": "Label",
																			"data": [
																				"Identity__"
																			],
																			"options": {
																				"label": "_Identity",
																				"version": 0
																			},
																			"stamp": 28
																		},
																		"Identity__": {
																			"type": "ShortText",
																			"data": [
																				"Identity__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "_Identity",
																				"activable": true,
																				"width": "500",
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"browsable": false,
																				"version": 0
																			},
																			"stamp": 29
																		},
																		"LabelSecret__": {
																			"type": "Label",
																			"data": [
																				"Secret__"
																			],
																			"options": {
																				"label": "_Secret",
																				"version": 0
																			},
																			"stamp": 30
																		},
																		"Secret__": {
																			"type": "Password",
																			"data": [
																				"Secret__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "_Secret",
																				"activable": true,
																				"width": "500",
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"browsable": false,
																				"version": 0
																			},
																			"stamp": 31
																		},
																		"Spacer_line2__": {
																			"type": "Spacer",
																			"data": false,
																			"options": {
																				"height": "",
																				"width": "",
																				"color": "default",
																				"label": "Spacer line2",
																				"version": 0
																			},
																			"stamp": 49
																		},
																		"LabelUser__": {
																			"type": "Label",
																			"data": [
																				"User__"
																			],
																			"options": {
																				"label": "_User",
																				"version": 0
																			},
																			"stamp": 32
																		},
																		"User__": {
																			"type": "ShortText",
																			"data": [
																				"User__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "_User",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"browsable": false,
																				"version": 0
																			},
																			"stamp": 33
																		},
																		"Spacer__": {
																			"type": "Spacer",
																			"data": false,
																			"options": {
																				"height": "",
																				"width": "",
																				"color": "default",
																				"label": "_Spacer",
																				"version": 0
																			},
																			"stamp": 98
																		},
																		"TestTile__": {
																			"type": "Title",
																			"data": false,
																			"options": {
																				"marge": 0,
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"backgroundcolor": "default",
																				"iconClass": "",
																				"label": "_TestTile",
																				"version": 0
																			},
																			"stamp": 87
																		},
																		"Spacer2__": {
																			"type": "Spacer",
																			"data": false,
																			"options": {
																				"height": "",
																				"width": "",
																				"color": "default",
																				"label": "_Spacer2",
																				"version": 0
																			},
																			"stamp": 99
																		},
																		"LabelOverrideCredentials__": {
																			"type": "Label",
																			"data": [],
																			"options": {
																				"label": "_OverrideCredentials",
																				"version": 0
																			},
																			"stamp": 100
																		},
																		"OverrideCredentials__": {
																			"type": "CheckBox",
																			"data": [],
																			"options": {
																				"label": "_OverrideCredentials",
																				"activable": true,
																				"width": 500,
																				"helpText": "_OverrideCredentials tooltip",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"notInDB": true,
																				"dataType": "Boolean",
																				"version": 0
																			},
																			"stamp": 101
																		},
																		"LabelTestLogin__": {
																			"type": "Label",
																			"data": [],
																			"options": {
																				"label": "_TestLogin",
																				"version": 0
																			},
																			"stamp": 88
																		},
																		"TestLogin__": {
																			"type": "ShortText",
																			"data": [],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"version": 1,
																				"label": "_TestLogin",
																				"activable": true,
																				"width": 500,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"notInDB": true,
																				"dataType": "String",
																				"browsable": false
																			},
																			"stamp": 89
																		},
																		"LabelTestName__": {
																			"type": "Label",
																			"data": [],
																			"options": {
																				"label": "_TestName",
																				"version": 0
																			},
																			"stamp": 90
																		},
																		"TestName__": {
																			"type": "ShortText",
																			"data": [],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"version": 1,
																				"label": "_TestName",
																				"activable": true,
																				"width": 500,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"notInDB": true,
																				"dataType": "String",
																				"browsable": false
																			},
																			"stamp": 91
																		},
																		"LabelTestPunchout__": {
																			"type": "Label",
																			"data": false,
																			"options": {
																				"label": "",
																				"version": 0
																			},
																			"stamp": 92
																		},
																		"TestPunchout__": {
																			"type": "FormButton",
																			"data": false,
																			"options": {
																				"buttonLabel": "Button",
																				"label": "",
																				"textPosition": "text-right",
																				"textSize": "MS",
																				"textStyle": "default",
																				"textColor": "default",
																				"nextprocess": {
																					"processName": "AP - Application Settings__",
																					"attachmentsMode": "all",
																					"willBeChild": true,
																					"returnToOriginalUrl": false
																				},
																				"urlImageOverlay": "",
																				"action": "none",
																				"url": "",
																				"version": 0
																			},
																			"stamp": 93
																		},
																		"LabelTestStatus__": {
																			"type": "Label",
																			"data": [],
																			"options": {
																				"label": "_TestStatus",
																				"version": 0
																			},
																			"stamp": 104
																		},
																		"TestStatus__": {
																			"type": "LongText",
																			"data": [],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"version": 1,
																				"minNbLines": 1,
																				"label": "_TestStatus",
																				"activable": true,
																				"width": "500",
																				"helpText": "",
																				"helpURL": "",
																				"readonly": true,
																				"helpFormat": "HTML Format",
																				"notInDB": true,
																				"dataType": "String",
																				"browsable": false
																			},
																			"stamp": 105
																		},
																		"LabelTestRequest__": {
																			"type": "Label",
																			"data": [],
																			"options": {
																				"label": "_TestRequest",
																				"version": 0
																			},
																			"stamp": 106
																		},
																		"TestRequest__": {
																			"type": "HTML",
																			"data": false,
																			"options": {
																				"label": "_TestRequest",
																				"version": 0,
																				"width": "800px",
																				"css": "@ pre {\n\twidth: 750px;\n\twhite-space: pre-wrap;\n\tword-break: break-all;\n\tword-wrap: break-word; /* for IE11 */\n\tline-break: anywhere;\n\tmargin: 5px 0px\n}@ \n\n.badge {\n    display: inline-block;\n    padding: .25em .4em;\n    margin: 0 .1em;\n    font-weight: 700;\n    line-height: 1;\n    text-align: center;\n    white-space: nowrap;\n    vertical-align: baseline;\n    border-radius: .25rem;\n\n    color: #000;\n    background-color: #E0E2E5;\n}@ \n\n.badge-small {\n    font-size: 75%;\n}@ \n\n.badge-error {\n\tcolor: #fff;\n\tbackground-color: #a94442\n}@ "
																			},
																			"stamp": 107
																		},
																		"LabelTestResponse__": {
																			"type": "Label",
																			"data": [],
																			"options": {
																				"label": "_TestResponse",
																				"version": 0
																			},
																			"stamp": 108
																		},
																		"TestResponse__": {
																			"type": "HTML",
																			"data": false,
																			"options": {
																				"label": "_TestResponse",
																				"width": "800px",
																				"css": "@ pre {\n\twidth: 750px;\n\twhite-space: pre-wrap;\n\tword-break: break-all;\n\tword-wrap: break-word; /* for IE11 */\n\tline-break: anywhere;\n\tmargin: 5px 0px\n}@ \n\n.badge {\n    display: inline-block;\n    padding: .25em .4em;\n    margin: 0 .1em;\n    font-weight: 700;\n    line-height: 1;\n    text-align: center;\n    white-space: nowrap;\n    vertical-align: baseline;\n    border-radius: .25rem;\n\n    color: #000;\n    background-color: #E0E2E5;\n}@ \n\n.badge-small {\n    font-size: 75%;\n}@ \n\n.badge-error {\n\tcolor: #fff;\n\tbackground-color: #a94442\n}@ ",
																				"version": 0
																			},
																			"stamp": 109
																		}
																	},
																	"stamp": 8
																}
															},
															"stamp": 7,
															"data": []
														}
													},
													"stamp": 6,
													"data": []
												}
											}
										}
									},
									"stamp": 5,
									"data": []
								}
							},
							"stamp": 4,
							"data": []
						}
					},
					"stamp": 3,
					"data": []
				},
				"form-footer": {
					"type": "FlexibleFormFooter",
					"options": {
						"splitterType": "simple-h",
						"version": 0
					},
					"*": {
						"Save": {
							"type": "SubmitButton",
							"options": {
								"label": "_Save",
								"action": "savequit",
								"submit": true,
								"version": 0,
								"shortCut": "S",
								"textPosition": "text-right",
								"textSize": "MS",
								"textStyle": "default",
								"textColor": "color1",
								"nextprocess": {
									"processName": "",
									"attachmentsMode": "all",
									"willBeChild": true,
									"returnToOriginalUrl": false
								}
							},
							"stamp": 11,
							"data": []
						},
						"Close": {
							"type": "SubmitButton",
							"options": {
								"label": "_Close",
								"action": "cancel",
								"submit": true,
								"version": 0,
								"shortCut": "Q"
							},
							"stamp": 12,
							"data": []
						},
						"Delete": {
							"type": "SubmitButton",
							"options": {
								"label": "_Delete",
								"action": "delete",
								"submit": true,
								"version": 0,
								"disabled": true
							},
							"stamp": 13,
							"data": []
						}
					},
					"stamp": 10,
					"data": []
				}
			},
			"stamp": 1,
			"data": []
		}
	},
	"stamps": 113,
	"data": []
}