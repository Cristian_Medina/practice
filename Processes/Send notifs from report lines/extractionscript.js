///#GLOBALS Lib
var reportParameter = Variable.GetValueAsString("reportParameter");
if (reportParameter)
{
	var params = JSON.parse(reportParameter);
	Sys.EmailNotification.SendReminders({
		template: params.template,
		fromName: params.translateFromName ? Language.Translate(params.fromName) : params.fromName,
		backupUserAsCC: true,
		escapeCustomTags: false,
		GetCustomTags: function (user)
		{
			var vars = user.dbInfo.GetVars();
			var baseUrl = Data.GetValue("ValidationUrl");
			baseUrl = baseUrl.substr(0, baseUrl.lastIndexOf("/"));
			var language = vars.GetValue_String("Language", 0);
			if (params.ValidationUrlV1)
			{
				params.ValidationUrl = (Lib.Version && Lib.Version.PAC) >= 2 ? params.ValidationUrlV2 : params.ValidationUrlV1;
			}
			var customTags = {
				"RecipientDisplayName": vars.GetValue_String("DisplayName", 0),
				"ApproverDisplayName": vars.GetValue_String("DisplayName", 0),
				"NumberOfItems": user.count,
				"Requisition": Language.TranslateInto(user.count > 1 ? "Requisitions" : "Requisition", language, false), //use in subject of ReminderForPRApproval
				"ValidationUrl": baseUrl + params.ValidationUrl,
				"DisplayValidationUrlsAsTables": !!params.displayAsTable
			};
			if (user.count > 1)
			{
				customTags.PlurialItems = true;
			}
			if (user.moreInfo.length > 0)
			{
				customTags.ValidationUrlList = BuildValidationUrlsHtml(user.moreInfo, baseUrl + params.ValidationUrl, params);
			}
			return customTags;
		},
		sendToAllMembersIfGroup: Sys.Parameters.GetInstance("P2P").GetParameter("SendNotificationsToEachGroupMembers") === "1"
	});
}

function BuildValidationUrlsHtml(lines, baseUrl, parameters)
{
	var columnsUrlDesc = parameters.columnsUrlDesc ? parameters.columnsUrlDesc.split(';') : [];
	var displayAsTable = !!parameters.displayAsTable;

	var htmlList = "";

	if (lines.length > 0 && columnsUrlDesc.length > 0)
	{
		if (displayAsTable)
		{
			htmlList = BuildValidationUrlsHtmlTable(lines, baseUrl, columnsUrlDesc);
		}
		else
		{
			htmlList = BuildValidationUrlsHtmlList(lines, baseUrl, columnsUrlDesc[0]);
		}
	}

	return htmlList;
}

function BuildValidationUrlsHtmlTable(lines, baseUrl, columnsUrlDesc)
{
	var htmlTable = "";
	var alreadyAdded = {};
	for (var i = 0; i < lines.length; i++)
	{
		var lineInfo = lines[i];

		if (!alreadyAdded[lineInfo[0]])
		{
			var url = encodeUrl(baseUrl + lineInfo[0]);

			var columns = "";
			for (var j = 0; j < columnsUrlDesc.length; j++)
			{
				var columnIndex = columnsUrlDesc[j];
				if (columnIndex < lineInfo.length)
				{
					columns += '<td style="padding-left: 30px;"><span>' + lineInfo[columnIndex] + "</span></td>";
				}
			}

			if (columns.length > 0)
			{
				columns = '<td style="padding-left: 30px;"><a href=' + url + ">" + Language.Translate("_View") + "</a></td>" + columns;
				htmlTable += "<tr>" + columns + "</tr>";

				alreadyAdded[lineInfo[0]] = true;
			}
		}

	}

	if (htmlTable.length > 0)
	{
		htmlTable = "<Table>" + htmlTable + "</Table>";
	}

	return htmlTable;
}

function BuildValidationUrlsHtmlList(lines, baseUrl, columnUrlDesc)
{
	var htmlList = "";
	var alreadyAdded = {};

	for (var i = 0; i < lines.length; i++)
	{
		var lineInfo = lines[i];

		if (!alreadyAdded[lineInfo[0]])
		{
			var url = encodeUrl(baseUrl + lineInfo[0]);

			if (columnUrlDesc < lineInfo.length)
			{
				htmlList += "<li><a href=" + url + ">" + lineInfo[columnUrlDesc] + "</a></li>";

				alreadyAdded[lineInfo[0]] = true;
			}
		}
	}

	if (htmlList.length > 0)
	{
		htmlList = "<ul>" + htmlList + "</ul>";
	}

	return htmlList;
}

function encodeUrl(url)
{
	return url.replace('#', encodeURIComponent('#'));
}