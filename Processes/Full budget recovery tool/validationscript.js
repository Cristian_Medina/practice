///#GLOBALS Lib Sys
var nbIDPerFilter = 50;
var currentName = Data.GetActionName();
var currentStep = Data.GetValue("CurrentStep__");
Log.Info("Execute action: " + currentName + ", step: " + currentStep);
var userActionResult = null;
var actionsByStep = {
	PR:
	{
		CheckBudget:
		{
			fn: CheckPRBudget,
			longAction: true
		},
		RecoverBudget:
		{
			fn: RecoverPRBudget,
			longAction: true
		}
	},
	PO:
	{
		CheckBudget:
		{
			fn: CheckPOBudget,
			longAction: true
		},
		RecoverBudget:
		{
			fn: RecoverPOBudget,
			longAction: true
		}
	},
	GR:
	{
		CheckBudget:
		{
			fn: CheckGRBudget,
			longAction: true
		},
		RecoverBudget:
		{
			fn: RecoverGRBudget,
			longAction: true
		}
	},
	Invoice:
	{
		CheckBudget:
		{
			fn: CheckInvoiceBudget,
			longAction: true
		},
		RecoverBudget:
		{
			fn: RecoverInvoiceBudget,
			longAction: true
		}
	},
	Budget:
	{
		RecomputeBudget:
		{
			fn: RecomputeBudget,
			longAction: true
		}
	}
};
///////////////////////////////////////////////
var QueryIterator = /** @class */ (function ()
{
	function QueryIterator(query, onTransport, multiQueryFn)
	{
		this.query = query;
		this.onTransport = onTransport;
		this.multiQueryFn = multiQueryFn;
		this.fetchedCount = 0;
	}
	QueryIterator.prototype.Next = function ()
	{
		var ret = this.query && (this.onTransport ? this.query.MoveNext() : this.query.MoveNextRecord());
		while (!ret && this.multiQueryFn)
		{
			var nextQuery = this.multiQueryFn();
			if (nextQuery)
			{
				this.query = nextQuery;
				ret = this.onTransport ? this.query.MoveNext() : this.query.MoveNextRecord();
			}
			else
			{
				break;
			}
		}
		if (ret)
		{
			this.fetchedCount++;
		}
		return ret;
	};
	QueryIterator.prototype.ForEach = function (callback)
	{
		var itr = this.Next();
		while (itr)
		{
			if (callback(itr))
			{
				break;
			}
			itr = this.Next();
		}
	};
	return QueryIterator;
}());

function InitUserActionResult(step, action)
{
	var str_userActionResults = Variable.GetValueAsString("UserActionResults");
	var userActionResults = (str_userActionResults && JSON.parse(str_userActionResults)) ||
	{};
	var userActionResultsForStep = userActionResults[step];
	if (!userActionResultsForStep)
	{
		userActionResults[step] = userActionResultsForStep = {};
	}
	var timestamp = Sys.Helpers.Date.Date2DBDateTime(new Date());
	var internal_userActionResult = userActionResultsForStep[action + " - " + timestamp] = {
		timestamp: timestamp
	};
	return {
		GetTimestamp: function ()
		{
			return timestamp;
		},
		AddResult: function (subAction, result)
		{
			internal_userActionResult[subAction] = result;
		},
		Serialize: function ()
		{
			Variable.SetValueAsString("UserActionResults", JSON.stringify(userActionResults));
		}
	};
}

function SaveImpactedBudgetIDs(budgetIDs)
{
	var str_impactedBudgetIDs = Variable.GetValueAsString("ImpactedBudgetIDs");
	var impactedBudgetIDs = (str_impactedBudgetIDs && JSON.parse(str_impactedBudgetIDs)) || [];
	impactedBudgetIDs = impactedBudgetIDs.concat(budgetIDs);
	impactedBudgetIDs = Sys.Helpers.Array.GetDistinctArray(impactedBudgetIDs);
	Variable.SetValueAsString("ImpactedBudgetIDs", JSON.stringify(impactedBudgetIDs));
}

function CheckBudget(options)
{
	Log.Info("Checking budget of " + options.docName + " documents...");
	var selectionMethod = options.multiSelectionMethod ? Data.GetValue("SelectionMethod__") : "_Doc";
	var filter = Data.GetValue("Filter__");
	userActionResult.AddResult("SelectionMethod",
	{
		method: selectionMethod,
		filter: filter
	});
	var documents;
	var getDocumentsOptions = Sys.Helpers.Extend(
	{}, options,
	{
		dataFileNeeded: true
	});
	if (selectionMethod === "_Doc")
	{
		documents = GetDocuments(filter, getDocumentsOptions);
	}
	else
	{
		documents = GetDocumentsViaItems(filter, getDocumentsOptions);
	}
	var result = {
		errors:
		{},
		warnings:
		{},
		docCount: 0,
		docInErrorCount: 0,
		docInWarningCount: 0,
		errorCount: 0,
		warningCount: 0
	};
	documents.ForEach(function (document)
	{
		var ruidex = document.GetUninheritedVars().GetValue_String("RUIDEX", 0);
		var checkBudgetIntegrityOptions = {
			initialBudgetIDByItems: options.GetInitialBudgetIDByItems ? options.GetInitialBudgetIDByItems(document) : null
		};
		Lib.Budget.CheckBudgetIntegrity(document, checkBudgetIntegrityOptions)
			.Then(function (resultOfCheckBudgetIntegrity)
			{
				if (resultOfCheckBudgetIntegrity.errors.length > 0)
				{
					result.errors[ruidex] = resultOfCheckBudgetIntegrity.errors;
					result.errorCount += resultOfCheckBudgetIntegrity.errors.length;
				}
				if (resultOfCheckBudgetIntegrity.warnings.length > 0)
				{
					result.warnings[ruidex] = resultOfCheckBudgetIntegrity.warnings;
					result.warningCount += resultOfCheckBudgetIntegrity.warnings.length;
				}
				var resultOfCheckBudget, impacted;
				if (resultOfCheckBudgetIntegrity.errors.length > 0)
				{
					resultOfCheckBudget = "error";
					impacted = true;
					result.docInErrorCount++;
				}
				else if (resultOfCheckBudgetIntegrity.warnings.length > 0)
				{
					resultOfCheckBudget = "warning";
					impacted = true;
					result.docInWarningCount++;
				}
				else
				{
					resultOfCheckBudget = "ok";
					impacted = false;
				}
				if (options.onEachDocCallback)
				{
					options.onEachDocCallback(document, resultOfCheckBudget);
				}
				if (impacted)
				{
					SaveImpactedBudgetIDs(Object.keys(resultOfCheckBudgetIntegrity.budgets.byBudgetID));
				}
			});
	});
	result.docCount = documents.fetchedCount;
	userActionResult.AddResult("CheckBudget", result);
}

function RecoverBudget(checkBudgetFn)
{
	var result = {
		requestCount: 0,
		errors:
		{}
	};
	checkBudgetFn(function (document, resultOfCheckBudget)
	{
		var ruidex = document.GetUninheritedVars().GetValue_String("RUIDEX", 0);
		if (resultOfCheckBudget === "error")
		{
			result.errors[ruidex] = Language.Translate("_BlockingBudgetIssues");
		}
		else if (resultOfCheckBudget === "warning")
		{
			var availabilityInfo_1 = null;
			CheckDocumentAvailability(document)
				.Then(function (res)
				{
					availabilityInfo_1 = res;
					return CreateRecoveryOperationDetails(document);
				})
				.Then(function ()
				{
					return UpdateDocumentAction(document, availabilityInfo_1);
				})
				.Then(function ()
				{
					result.requestCount++;
				})
				.Catch(function (error)
				{
					result.errors[ruidex] = Language.Translate("_RequestingRecoveryFailed", false, error.toString());
				})
				.Then(function ()
				{
					if (availabilityInfo_1 && availabilityInfo_1.ownershipToRelease)
					{
						document.ReleaseAsyncOwnership("FullBudgetRecovery");
					}
				});
		}
	});
	userActionResult.AddResult("RecoverBudget", result);
}

function CheckPRBudget(onEachPRCallback)
{
	CheckBudget(
	{
		docName: "PR",
		processName: "Purchase requisition V2",
		itemsTableName: "PAC - PR - Items__",
		docIDField: "RequisitionNumber__",
		itemsDocIDField: "PRNumber__",
		multiSelectionMethod: true,
		onEachDocCallback: onEachPRCallback
	});
}

function RecoverPRBudget()
{
	RecoverBudget(CheckPRBudget);
}

function CheckPOBudget(onEachPOCallback)
{
	CheckBudget(
	{
		docName: "PO",
		processName: "Purchase order V2",
		itemsTableName: "PAC - PO - Items__",
		docIDField: "OrderNumber__",
		itemsDocIDField: "PONumber__",
		multiSelectionMethod: true,
		GetInitialBudgetIDByItems: function (document)
		{
			var data = document.GetFormData();
			return Lib.Purchasing.POBudget.GetPRBudgetIDByItems(data);
		},
		onEachDocCallback: onEachPOCallback
	});
}

function RecoverPOBudget()
{
	RecoverBudget(CheckPOBudget);
}

function CheckGRBudget(onEachGRCallback)
{
	CheckBudget(
	{
		docName: "GR",
		processName: "Goods receipt V2",
		itemsTableName: "P2P - Goods receipt - Items__",
		docIDField: "GRNumber__",
		itemsDocIDField: "GRNumber__",
		multiSelectionMethod: true,
		GetInitialBudgetIDByItems: function (document)
		{
			var data = document.GetFormData();
			return Lib.Purchasing.GRBudget.GetPOBudgetIDByItems(data);
		},
		onEachDocCallback: onEachGRCallback
	});
}

function RecoverGRBudget()
{
	RecoverBudget(CheckGRBudget);
}

function CheckInvoiceBudget(onEachInvoiceCallback)
{
	CheckBudget(
	{
		docName: "Invoice",
		processName: "Vendor invoice",
		multiSelectionMethod: false,
		onEachDocCallback: onEachInvoiceCallback
	});
}

function RecoverInvoiceBudget()
{
	RecoverBudget(CheckInvoiceBudget);
}

function RecomputeBudget()
{
	Log.Info("Recomputing budgets...");
	var budgetIDs = Data.GetValue("BudgetIDs__").split("\n");
	var result = {
		budgetIDs: budgetIDs,
		errors:
		{}
	};
	budgetIDs.forEach(function (budgetID)
	{
		var budget = {};
		budget[budgetID] = true;
		var criticalSectionName = Lib.Budget.GetCriticalSection(budget);
		Log.Info("Try entering Budget critical section: " + criticalSectionName);
		var lockOK = Process.PreventConcurrentAccess(criticalSectionName, function ()
		{
			Log.Info("Entered budget critical section");
			var ok = Lib.Budget.ComputeBudgetFromOperationDetails(budgetID);
			if (!ok)
			{
				result.errors[budgetID] = {
					type: "RECOMPUTE_ERROR"
				};
			}
		}, null, 60);
		if (!lockOK)
		{
			result.errors[budgetID] = {
				type: "LOCK_ERROR",
				details: criticalSectionName
			};
		}
	});
	userActionResult.AddResult("RecomputeBudget", result);
}

function GetDocuments(filters, options)
{
	filters = Sys.Helpers.IsArray(filters) ? filters : [filters];
	var iFilter = 0;
	var query = GetNextQuery(iFilter++);
	return new QueryIterator(query, true, function ()
	{
		return GetNextQuery(iFilter++);
	});
	/////////
	function GetNextQuery(i)
	{
		if (i < filters.length)
		{
			var filter = "(&" + Sys.Helpers.String.ParenthesisLdapFilter(filters[i]) + (options.docSubFilter || "") + ")";
			var query_1 = Process.CreateQueryAsProcessAdmin();
			query_1.Reset();
			query_1.AddAttribute("*");
			if (options.dataFileNeeded)
			{
				query_1.AddAttribute("DataFile");
			}
			query_1.SetSpecificTable("CDNAME#" + options.processName);
			query_1.SetSearchInArchive(true);
			query_1.SetFilter(filter);
			query_1.MoveFirst();
			return query_1;
		}
		return null;
	}
}

function GetDocumentsViaItems(filters, options)
{
	var items = GetItems(filters, options);
	var docIDs = [];
	var docFilters = []; // filter with max nbIDPerFilter IDs
	items.ForEach(function (item)
	{
		var vars = item.GetVars();
		var id = vars.GetValue_String(options.itemsDocIDField, 0);
		docIDs.push("(" + options.docIDField + "=" + id + ")");
		if (docIDs.length === nbIDPerFilter)
		{
			docFilters.push("(|" + docIDs.join("") + ")");
			docIDs = [];
		}
	});
	if (docIDs.length > 0)
	{
		docFilters.push("(|" + docIDs.join("") + ")");
	}
	return GetDocuments(docFilters, options);
}

function GetItems(filters, options)
{
	filters = Sys.Helpers.IsArray(filters) ? filters : [filters];
	var iFilter = 0;
	var firstQuery = GetNextQuery(iFilter++);
	return new QueryIterator(firstQuery, false, function ()
	{
		return GetNextQuery(iFilter++);
	});
	/////////
	function GetNextQuery(i)
	{
		if (i < filters.length)
		{
			var query = Process.CreateQueryAsProcessAdmin();
			var filter = "(&" + Sys.Helpers.String.ParenthesisLdapFilter(filters[i]) + (options.docItemsSubFilter || "") + ")";
			query.Reset();
			query.AddAttribute(options.itemsDocIDField);
			query.SetSpecificTable(options.itemsTableName);
			query.SetFilter(filter);
			query.MoveFirst();
			return query;
		}
		return null;
	}
}

function CheckDocumentAvailability(document)
{
	return Sys.Helpers.Promise.Create(function (resolve)
	{
		var result = {};
		var vars = document.GetUninheritedVars();
		result.state = vars.GetValue_Long("State", 0);
		var archiveState = vars.GetValue_Long("ArchiveState", 0);
		var available = false;
		if (result.state >= 100 && archiveState)
		{
			available = true;
		}
		else if (result.state === 90)
		{
			available = true;
		}
		else if (result.state === 70)
		{
			var errorCode = document.GetAsyncOwnership("FullBudgetRecovery", 10000);
			if (errorCode !== 0)
			{
				throw new Error("Get ownership on document failed. Details: " + document.GetLastErrorMessage());
			}
			available = true;
			result.ownershipToRelease = true;
		}
		else if (result.state === 50)
		{
			var requestedActions = vars.GetValue_String("RequestedActions", 0);
			available = requestedActions === "approve|FullBudgetRecovery";
		}
		if (!available)
		{
			throw new Error("Unexpected state of document, state: " + result.state + ", archiveState: " + archiveState);
		}
		resolve(result);
	});
}

function CreateRecoveryOperationDetails(document)
{
	return Sys.Helpers.Promise.Create(function (resolve)
	{
		var ruidex = document.GetUninheritedVars().GetValue_String("RUIDEX", 0);
		var record = Process.CreateTableRecordAsProcessAdmin("PurchasingFullBudgetRecoveryOperationDetails__");
		var vars = record.GetVars();
		vars.AddValue_String("ToolRuidEx__", Data.GetValue("RuidEx"), true);
		vars.AddValue_String("RequestTimestamp__", userActionResult.GetTimestamp(), true);
		vars.AddValue_String("DocumentRuidEx__", ruidex, true);
		vars.AddValue_Long("Status__", 0, true);
		record.Commit();
		if (record.GetLastError())
		{
			throw new Error("Unable to save full recovery operation details. Details: " + record.GetLastErrorMessage());
		}
		resolve();
	});
}

function UpdateDocumentAction(document, availabilityInfo)
{
	return Sys.Helpers.Promise.Create(function (resolve)
	{
		if (availabilityInfo.state !== 50)
		{
			var ok = false;
			var vars = document.GetUninheritedVars();
			var extVars = document.GetExternalVars();
			var updatableDocument = void 0;
			if (availabilityInfo.state === 70)
			{
				updatableDocument = document;
			}
			else
			{
				var ruidex = vars.GetValue_String("RUIDEX", 0);
				updatableDocument = Process.GetUpdatableTransport(ruidex);
			}
			var varsToUpdate = updatableDocument.GetUninheritedVars();
			var extVarsToUpdate = updatableDocument.GetExternalVars();
			var str_recoveryData = extVars.GetValue_String("FullBudgetRecoveryData", 0);
			var recoveryData = (str_recoveryData && JSON.parse(str_recoveryData)) ||
			{};
			// Keep original state value...
			if (Sys.Helpers.IsUndefined(recoveryData.originalState))
			{
				recoveryData.originalState = availabilityInfo.state;
			}
			extVarsToUpdate.AddValue_String("FullBudgetRecoveryData", JSON.stringify(recoveryData), true);
			if (availabilityInfo.state >= 100)
			{
				varsToUpdate.AddValue_Long("State", 90, true);
			}
			if (availabilityInfo.state === 70)
			{
				varsToUpdate.AddValue_String("RequestedActions", "approve|FullBudgetRecovery", true);
				varsToUpdate.AddValue_String("NeedValidation", "0", true);
				updatableDocument.Validate("Full budget recovery triggered");
				ok = updatableDocument.GetLastError() === 0;
			}
			else
			{
				ok = !!updatableDocument.ResumeWithAction("FullBudgetRecovery");
				if (!ok)
				{
					ok = !!updatableDocument.ResumeWithActionAsync("FullBudgetRecovery");
				}
			}
			if (!ok)
			{
				throw new Error("Unable to update document action. Details: " + updatableDocument.GetLastErrorMessage());
			}
		}
		resolve();
	});
}

function SendEndOfLongActionNotification()
{
	var str_toNotify = Variable.GetValueAsString("EndOfLongActionToNotify");
	var toNotify = Sys.Helpers.String.ToBoolean(str_toNotify);
	if (!toNotify)
	{
		return;
	}
	var ownerID = Lib.P2P.GetValidatorOrOwnerLogin();
	var owner = Users.GetUser(ownerID);
	var ownerEmail = owner.GetValue("EmailAddress");
	if (Sys.Helpers.IsEmpty(ownerEmail))
	{
		Log.Error("No email address to notify user of the end of long action.");
		return;
	}
	var email = Sys.EmailNotification.CreateEmail(
	{
		emailAddress: ownerEmail,
		userId: ownerID,
		customTags:
		{
			"ValidationUrl": Data.GetValue("ValidationUrl")
		},
		template: "FullRecoveryTool_Email_EndOfLongAction.htm"
	});
	try
	{
		Sys.EmailNotification.SendEmail(email);
	}
	catch (e)
	{
		Log.Error("Cannot send the end of long action notification. Details: " + e);
	}
}
if (currentName === "Terminate")
{
	Log.Info("Terminating the full recovery tool instance");
}
else if (currentName === "save")
{
	Log.Info("Saving the full recovery tool instance");
}
else
{
	var stepActions = actionsByStep[currentStep];
	if (stepActions)
	{
		var stepAction = stepActions[currentName];
		if (stepAction)
		{
			userActionResult = InitUserActionResult(currentStep, currentName);
			stepAction.fn();
			if (stepAction.longAction)
			{
				SendEndOfLongActionNotification();
			}
		}
		else
		{
			Log.Error("Action " + currentName + " not supported by step " + currentStep);
		}
	}
	else
	{
		Log.Error("Unknown step " + currentStep);
	}
	if (userActionResult)
	{
		userActionResult.Serialize();
	}
	Process.PreventApproval();
}