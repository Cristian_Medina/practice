///#GLOBALS Lib Sys
function GetAppInstances()
{
	var P2PGlobalSettings = ProcessInstance.extendedProperties &&
		ProcessInstance.extendedProperties.appInstances &&
		ProcessInstance.extendedProperties.appInstances.P2P &&
		ProcessInstance.extendedProperties.appInstances.P2P.globalConfiguration;
	if (P2PGlobalSettings)
	{
		return {
			"P2P": true,
			"P2P_AP": P2PGlobalSettings.EnableAccountPayableGlobalSetting__ === "1",
			"P2P_CONTRACT": P2PGlobalSettings.EnableContractGlobalSetting__ === "1",
			"P2P_PAC": P2PGlobalSettings.EnablePurchasingGlobalSetting__ === "1",
			"P2P_EXPENSE": P2PGlobalSettings.EnableExpenseClaimsGlobalSetting__ === "1",
			"P2P_FLIPPO": P2PGlobalSettings.EnableFlipPOGlobalSetting__ === "1",
			"DD": Boolean(ProcessInstance.extendedProperties.appInstances.DD)
		};
	}
	return {};
}
var VendorDetailsHelper = {
	contactsFilterCache: null,
	controlFilters:
	{
		OrdersToReceiveCounter__: null,
		OrdersToInvoiceCounter__: null,
		TotalOrdersCounter__: null,
		InvoicesOverdueCounter__: null,
		InvoicesPendingCounter__: null,
		InvoicesOnHoldCounter__: null,
		InvoicesTotalCounter__: null,
		InvoicesDiscountCounter__: null
	},
	CompanyCurrency: null,
	Init: function ()
	{
		return Sys.Helpers.Promise.Create(function (resolve)
		{
			var vendorNumber = Process.GetURLParameter("vendornumber") || Process.GetURLParameter("number");
			VendorDetailsHelper.setFieldIfNotEmpty("CompanyCode__", Process.GetURLParameter("companycode"));
			VendorDetailsHelper.setFieldIfNotEmpty("VendorNumber__", vendorNumber);
			if (Process.GetURLParameter("quitOnClose") === "1")
			{
				// We were open in a new tab, close it
				Controls.Close.OnClick = function ()
				{
					Process.CloseTab();
				};
			}
			Controls.VendorDetailsCompanyCode__.SetText(Data.GetValue("CompanyCode__"));
			Controls.VendorDetailsVendorNumber__.SetText(Data.GetValue("VendorNumber__"));
			Controls.VendorInfoCompanyCode__.SetText(Data.GetValue("CompanyCode__"));
			Controls.VendorInfoVendorNumber__.SetText(Data.GetValue("VendorNumber__"));
			VendorDetailsHelper.controlFilters.OrdersToReceiveCounter__ = Sys.Helpers.LdapUtil.FilterAnd(Sys.Helpers.LdapUtil.FilterEqual("CompanyCode__", Data.GetValue("CompanyCode__")), Sys.Helpers.LdapUtil.FilterEqual("VendorNumber__", Data.GetValue("VendorNumber__")), Sys.Helpers.LdapUtil.FilterLesserOrEqual("State", "100"));
			VendorDetailsHelper.controlFilters.OrdersToInvoiceCounter__ = Sys.Helpers.LdapUtil.FilterAnd(Sys.Helpers.LdapUtil.FilterEqual("CompanyCode__", Data.GetValue("CompanyCode__")), Sys.Helpers.LdapUtil.FilterEqual("VendorNumber__", Data.GetValue("VendorNumber__")), Sys.Helpers.LdapUtil.FilterOr(Sys.Helpers.LdapUtil.FilterEqual("NoMoreInvoiceExpected__", "0"), Sys.Helpers.LdapUtil.FilterNot(Sys.Helpers.LdapUtil.FilterEqual("NoMoreInvoiceExpected__", "*"))));
			VendorDetailsHelper.controlFilters.TotalOrdersCounter__ = Sys.Helpers.LdapUtil.FilterAnd(Sys.Helpers.LdapUtil.FilterEqual("CompanyCode__", Data.GetValue("CompanyCode__")), Sys.Helpers.LdapUtil.FilterEqual("VendorNumber__", Data.GetValue("VendorNumber__")), Sys.Helpers.LdapUtil.FilterLesserOrEqual("State", "100"));
			VendorDetailsHelper.controlFilters.InvoicesOverdueCounter__ = Sys.Helpers.LdapUtil.FilterAnd(Sys.Helpers.LdapUtil.FilterEqual("CompanyCode__", Data.GetValue("CompanyCode__")), Sys.Helpers.LdapUtil.FilterEqual("VendorNumber__", Data.GetValue("VendorNumber__")), Sys.Helpers.LdapUtil.FilterLesserOrEqual("State", "100"));
			VendorDetailsHelper.controlFilters.InvoicesPendingCounter__ = Sys.Helpers.LdapUtil.FilterAnd(Sys.Helpers.LdapUtil.FilterEqual("CompanyCode__", Data.GetValue("CompanyCode__")), Sys.Helpers.LdapUtil.FilterEqual("VendorNumber__", Data.GetValue("VendorNumber__")), Sys.Helpers.LdapUtil.FilterLesserOrEqual("State", "100"), Sys.Helpers.LdapUtil.FilterOr(Sys.Helpers.LdapUtil.FilterEqual("InvoiceStatus__", "Received"), Sys.Helpers.LdapUtil.FilterEqual("InvoiceStatus__", "To verify"), Sys.Helpers.LdapUtil.FilterEqual("InvoiceStatus__", "Set aside"), Sys.Helpers.LdapUtil.FilterEqual("InvoiceStatus__", "To approve"), Sys.Helpers.LdapUtil.FilterEqual("InvoiceStatus__", "On hold"), Sys.Helpers.LdapUtil.FilterEqual("InvoiceStatus__", "To post"), Sys.Helpers.LdapUtil.FilterEqual("InvoiceStatus__", "To pay")));
			VendorDetailsHelper.controlFilters.InvoicesOnHoldCounter__ = Sys.Helpers.LdapUtil.FilterAnd(Sys.Helpers.LdapUtil.FilterEqual("CompanyCode__", Data.GetValue("CompanyCode__")), Sys.Helpers.LdapUtil.FilterEqual("VendorNumber__", Data.GetValue("VendorNumber__")), Sys.Helpers.LdapUtil.FilterLesserOrEqual("State", "100"));
			VendorDetailsHelper.controlFilters.InvoicesTotalCounter__ = Sys.Helpers.LdapUtil.FilterAnd(Sys.Helpers.LdapUtil.FilterEqual("CompanyCode__", Data.GetValue("CompanyCode__")), Sys.Helpers.LdapUtil.FilterEqual("VendorNumber__", Data.GetValue("VendorNumber__")), Sys.Helpers.LdapUtil.FilterLesserOrEqual("State", "100"));
			VendorDetailsHelper.controlFilters.InvoicesDiscountCounter__ = Sys.Helpers.LdapUtil.FilterAnd(Sys.Helpers.LdapUtil.FilterEqual("CompanyCode__", Data.GetValue("CompanyCode__")), Sys.Helpers.LdapUtil.FilterEqual("VendorNumber__", Data.GetValue("VendorNumber__")), Sys.Helpers.LdapUtil.FilterLesserOrEqual("State", "100"));
			VendorDetailsHelper.FillVendorInformations()
				.Then(VendorDetailsHelper.GetCompanyCodeData)
				.Then(resolve);
		});
	},
	UpdateFields: function (fields, value)
	{
		for (var _i = 0, fields_1 = fields; _i < fields_1.length; _i++)
		{
			var field = fields_1[_i];
			if (Controls[field])
			{
				var c = Controls[field];
				if (value)
				{
					c.SetText(value);
					c.Hide(false);
				}
				else if (field.startsWith("VendorDetails"))
				{
					c.Hide(true);
				}
			}
		}
	},
	FillVendorInformations: function ()
	{
		var mapping = {
			Name__: ["VendorDetailsName__", "VendorInfoName__"],
			Email__: ["VendorDetailsEmail__", "VendorInfoEmail__"],
			PhoneNumber__: ["VendorDetailsPhoneNumber__", "VendorInfoPhoneNumber__"],
			VATNumber__: ["VendorInfoVATNumber__"],
			DUNSNumber__: ["VendorInfoDUNSNumber__"],
			FaxNumber__: ["VendorInfoFaxNumber__"],
			PreferredInvoiceType__: ["VendorInfoPreferredInvoiceType__"],
			PaymentTermCode__: ["VendorInfoPaymentTermCode__"],
			Currency__: ["VendorInfoCurrency__"],
			Street__: ["Street__"],
			PostOfficeBox__: ["PostOfficeBox__"],
			City__: ["City__"],
			PostalCode__: ["PostalCode__"],
			Region__: ["Region__"],
			Sub__: ["Sub__"],
			Country__: ["Country__"]
		};
		return Sys.Helpers.Promise.Create(function (resolve)
		{
			Query.DBQuery(function ()
			{
				ProcessInstance.SetSilentChange(true);
				for (var _i = 0, _a = Object.keys(mapping); _i < _a.length; _i++)
				{
					var prop = _a[_i];
					VendorDetailsHelper.UpdateFields(mapping[prop], this.GetQueryValue(prop));
				}
				Lib.P2P.Address.SetFormattedAddressControl(Controls.VendorAddress__);
				Lib.P2P.Address.ComputeFormattedAddressWithOptions(
				{
					"isVariablesAddress": true,
					"address":
					{
						"ToName": "ToRemove",
						"ToSub": Controls.Sub__.GetText(),
						"ToMail": Controls.Street__.GetText(),
						"ToPostal": Controls.PostalCode__.GetText(),
						"ToCountry": Controls.Country__.GetText(),
						"ToCountryCode": Controls.Country__.GetText(),
						"ToState": Controls.Region__.GetText(),
						"ToCity": Controls.City__.GetText(),
						"ToPOBox": Controls.PostOfficeBox__.GetText(),
						"ForceCountry": true
					},
					"countryCode": Controls.Country__.GetText()
				});
				ProcessInstance.SetSilentChange(false);
				resolve();
			}, "AP - Vendors__", Object.keys(mapping).join("|"), VendorDetailsHelper.GetCompanyCodeAndVendorFilter().replace("VendorNumber__", "Number__"), null, 1, null, "FastSearch=1");
		});
	},
	GetCompanyCodeData: function ()
	{
		return Sys.Helpers.Promise.Create(function (resolve)
		{
			Query.DBQuery(function ()
			{
				VendorDetailsHelper.CompanyCurrency = this.GetQueryValue("Currency__");
				resolve();
			}, "PurchasingCompanycodes__", "Currency__", "CompanyCode__=" + Data.GetValue("CompanyCode__"), null, 1, null, "FastSearch=1");
		});
	},
	setFieldIfNotEmpty: function (fieldName, value)
	{
		if (value)
		{
			Data.SetValue(fieldName, value);
		}
	},
	GetCompanyCodeAndVendorFilter: function ()
	{
		var companyCodeAndVendorFilter = Sys.Helpers.LdapUtil.FilterAnd(Sys.Helpers.LdapUtil.FilterEqual("CompanyCode__", Data.GetValue("CompanyCode__")), Sys.Helpers.LdapUtil.FilterEqual("VendorNumber__", Data.GetValue("VendorNumber__")));
		return companyCodeAndVendorFilter.toString();
	},
	GetCompanyCodeAndVendorFilterForSDA: function ()
	{
		var companyCodeAndVendorFilterForSDA = Sys.Helpers.LdapUtil.FilterAnd(Sys.Helpers.LdapUtil.FilterEqual("CF_1711725718e_Company_code__", Data.GetValue("CompanyCode__")), Sys.Helpers.LdapUtil.FilterEqual("CF_17117258411_Vendor_number__", Data.GetValue("VendorNumber__")), Sys.Helpers.LdapUtil.FilterLesserOrEqual("State", "100"));
		return companyCodeAndVendorFilterForSDA.toString();
	},
	GetContactsFilter: function ()
	{
		return Sys.Helpers.Promise.Create(function (resolve)
		{
			if (!VendorDetailsHelper.contactsFilterCache)
			{
				// Get logins from VendorsLinks
				var vendorLinksFilter = Sys.Helpers.LdapUtil.FilterAnd(Sys.Helpers.LdapUtil.FilterEqual("Number__", Data.GetValue("VendorNumber__")), Sys.Helpers.LdapUtil.FilterEqual("CompanyCode__", Data.GetValue("CompanyCode__")));
				Query.DBQuery(function ()
				{
					var queryResults = this;
					var logins = [];
					var nbResults = queryResults.GetRecordsCount();
					if (nbResults === 0)
					{
						Log.Warn("Vendor company is not associated to any contacts in Vendors links__ table");
						VendorDetailsHelper.contactsFilterCache = "(1=0)";
					}
					else
					{
						var addLogin = function (login)
						{
							if (login && logins.indexOf(login) === -1)
							{
								logins.push(login);
							}
						};
						for (var i = 0; i < nbResults; i++)
						{
							addLogin(queryResults.GetQueryValue("ShortLogin__", i));
							addLogin(queryResults.GetQueryValue("ShortLoginPAC__", i));
						}
						VendorDetailsHelper.contactsFilterCache = "(|" + logins.map(function (login)
						{
							return "(Login=" + User.accountId + "$" + login + ")";
						}).join("") + ")";
					}
					resolve(VendorDetailsHelper.contactsFilterCache);
				}, "AP - Vendors links__", "ShortLogin__|ShortLoginPAC__", vendorLinksFilter.toString(), null, 100);
			}
			else
			{
				resolve(VendorDetailsHelper.contactsFilterCache);
			}
		});
	}
};
var ActionMenuHelper = {
	BindEvents: function ()
	{
		Controls.ActionMenu__.BindEvent("onActionLoad", function ()
		{
			Controls.ActionMenu__.FireEvent("onActionLoad",
			{
				appInstances: GetAppInstances(),
				tabs:
				{
					MoreDetails: Language.Translate("_More_Details")
				}
			});
		});
		Controls.ActionMenu__.BindEvent("MoreDetails", ActionMenuHelper.MoreDetails);
	},
	Init: function ()
	{
		return Sys.Helpers.Promise.Create(function (resolve)
		{
			resolve();
		});
	},
	MoreDetails: function ()
	{
		var panes = Controls.form_content_right.GetPanes();
		var expectedPanes = {
			VendorInformation: true,
			VendorAddress: true
		};
		for (var _i = 0, panes_1 = panes; _i < panes_1.length; _i++)
		{
			var pane = panes_1[_i];
			pane.Hide(!expectedPanes[pane.GetName()]);
		}
	}
};
var NavMenuHelper = {
	embeddedViewsTab: "_My documents-AP-Embedded",
	Menus:
	{
		overview:
		{
			panes:
			{
				PurchaseOrdersMetricsPanel: GetAppInstances().P2P_PAC,
				InvoicesMetricsPanel: GetAppInstances().P2P_AP,
				DiscountMetricsPanel: GetAppInstances().P2P_AP,
				ReportsPanel: GetAppInstances().P2P_AP,
				ScoringReportsPanel: Lib.AP && typeof Lib.AP.Scoring !== "undefined"
			},
			init: function ()
			{
				var onClickPOCounter = function ()
				{
					NavMenuHelper.Menus.overview.displayView("purchaseOrders");
				};
				var onClickInvoiceCounter = function ()
				{
					NavMenuHelper.Menus.overview.displayView("invoices");
				};
				var initReports = function ()
				{
					var companyCodeAndVendorFilter = VendorDetailsHelper.GetCompanyCodeAndVendorFilter();
					return Sys.Helpers.Promise.Create(function (resolve)
					{
						var promises = [];
						for (var _i = 0, _a = ["ReportsPanel"]; _i < _a.length; _i++)
						{
							var panel = _a[_i];
							var controls = Controls[panel].GetControls();
							for (var _b = 0, controls_1 = controls; _b < controls_1.length; _b++)
							{
								var report = controls_1[_b];
								report.Hide(false);
								report.SetAdditionalFilter(companyCodeAndVendorFilter);
								report.Refresh();
							}
						}
						if (NavMenuHelper.Menus.overview.panes.ReportsPanel)
						{
							promises.push(ReportDataHelper.GetInvoicingHistoryData(Data.GetValue("CompanyCode__"), Data.GetValue("VendorNumber__"))
								.Then(function (historicalData)
								{
									return Controls.ReportInvoicingHistory__.SetValue(historicalData);
								}));
						}
						if (NavMenuHelper.Menus.overview.panes.ScoringReportsPanel)
						{
							promises.push(Lib.AP.Scoring.GetScoringReportData(Data.GetValue("CompanyCode__"), Data.GetValue("VendorNumber__"))
								.Then(function (reportData)
								{
									Controls.ReportSupplierScoring__.SetValue(reportData);
									Controls.ReportSupplierScoring__.Hide(false);
								}));
						}
						if (promises.length > 0)
						{
							Sys.Helpers.Synchronizer.All(promises).Then(resolve);
						}
						else
						{
							resolve();
						}
					});
				};
				var initCounters = function ()
				{
					return Sys.Helpers.Promise.Create(function (resolve)
					{
						for (var _i = 0, _a = ["PurchaseOrdersMetricsPanel", "InvoicesMetricsPanel", "DiscountMetricsPanel"]; _i < _a.length; _i++)
						{
							var panel = _a[_i];
							var panelVisible = NavMenuHelper.Menus.overview.panes[panel];
							var controls = Controls[panel].GetControls();
							for (var _b = 0, controls_2 = controls; _b < controls_2.length; _b++)
							{
								var counter = controls_2[_b];
								if (panelVisible)
								{
									counter.Hide(false);
									var filter = VendorDetailsHelper.controlFilters[counter.GetName()].toString();
									counter.SetAdditionalFilter(filter);
									counter.Refresh();
									counter.OnClick = panel === "PurchaseOrdersMetricsPanel" ? onClickPOCounter : onClickInvoiceCounter;
								}
								else
								{
									counter.Hide(true);
								}
							}
						}
						resolve();
					});
				};
				return Sys.Helpers.Promise.Create(function (resolve)
				{
					initReports()
						.Then(initCounters)
						.Then(resolve);
				});
			},
			displayView: function (tabId)
			{
				Controls.NavMenu__.FireEvent("onChangeTab",
				{
					tabId: tabId
				});
			},
			onStart: function () {}
		},
		invoices:
		{
			panes:
			{
				InvoicesPane: true
			},
			init: function ()
			{
				return Sys.Helpers.Promise.Create(function (resolve)
				{
					Controls.Invoices__.SetView(
					{
						tabName: NavMenuHelper.embeddedViewsTab,
						filter: VendorDetailsHelper.GetCompanyCodeAndVendorFilter(),
						viewName: "_SIM_View_Invoices - embedded",
						processOrTableName: "Vendor invoice",
						checkProfileTab: false
					});
					Controls.Invoices__.OpenInNewTab(true);
					Controls.Invoices__.OpenInReadOnlyMode(false);
					resolve();
				});
			},
			onStart: function ()
			{
				Controls.Invoices__.Apply();
			}
		},
		purchaseOrders:
		{
			panes:
			{
				PurchaseOrdersPane: true
			},
			init: function ()
			{
				return Sys.Helpers.Promise.Create(function (resolve)
				{
					Controls.PurchaseOrders__.SetView(
					{
						tabName: NavMenuHelper.embeddedViewsTab,
						filter: VendorDetailsHelper.GetCompanyCodeAndVendorFilter(),
						viewName: "_SIM_View_PurchaseOrders - embedded",
						processOrTableName: "Purchase order V2",
						checkProfileTab: false
					});
					Controls.PurchaseOrders__.OpenInNewTab(true);
					Controls.PurchaseOrders__.OpenInReadOnlyMode(false);
					resolve();
				});
			},
			onStart: function ()
			{
				Controls.PurchaseOrders__.Apply();
			}
		},
		contracts:
		{
			panes:
			{
				ContractsPane: true
			},
			init: function ()
			{
				return Sys.Helpers.Promise.Create(function (resolve)
				{
					Controls.Contracts__.SetView(
					{
						tabName: NavMenuHelper.embeddedViewsTab,
						filter: VendorDetailsHelper.GetCompanyCodeAndVendorFilter(),
						viewName: "_SIM_View_Contracts - embedded",
						processOrTableName: "P2P - Contract",
						checkProfileTab: false
					});
					resolve();
				});
			},
			onStart: function ()
			{
				Controls.Contracts__.Apply();
			}
		},
		vendorRegistrations:
		{
			panes:
			{
				VendorRegistrationsPane: true
			},
			init: function ()
			{
				return Sys.Helpers.Promise.Create(function (resolve)
				{
					Controls.VendorRegistrations__.SetView(
					{
						tabName: NavMenuHelper.embeddedViewsTab,
						filter: VendorDetailsHelper.GetCompanyCodeAndVendorFilter(),
						viewName: "_SIM_View_Vendor Registrations - embedded",
						processOrTableName: "Vendor Registration",
						checkProfileTab: false
					});
					Controls.VendorRegistrations__.OpenInReadOnlyMode(false);
					resolve();
				});
			},
			onStart: function ()
			{
				Controls.VendorRegistrations__.Apply();
			}
		},
		relatedDoc:
		{
			panes:
			{
				RelatedDocPane: true
			},
			init: function ()
			{
				return Sys.Helpers.Promise.Create(function (resolve)
				{
					Controls.RelatedDoc__.SetView(
					{
						tabName: "_VendorRelatedDocEmbeddedTab",
						filter: VendorDetailsHelper.GetCompanyCodeAndVendorFilterForSDA(),
						viewName: "_VendorRelatedDocEmbeddedView",
						processOrTableName: "DD - SenderForm",
						checkProfileTab: false
					});
					resolve();
				});
			},
			onStart: function ()
			{
				Controls.RelatedDoc__.Apply();
			}
		},
		contacts:
		{
			panes:
			{
				ContactsPane: true
			},
			init: function ()
			{
				return Sys.Helpers.Promise.Create(function (resolve)
				{
					VendorDetailsHelper.GetContactsFilter().Then(function (filter)
					{
						Controls.ContactsList__.SetView(
						{
							tabName: "_VendorsContacts",
							filter: filter,
							viewName: "_SIM_View_Contacts - embedded",
							checkProfileTab: false
						});
						resolve();
					});
				});
			},
			onStart: function ()
			{
				Controls.ContactsList__.Apply();
			}
		},
		settings:
		{
			panes:
			{
				OrderPreferrencesPane: true,
				FlipPOPane: true
			},
			init: function ()
			{
				return Sys.Helpers.Promise.Create(function (resolve)
				{
					resolve();
				});
			},
			onStart: function () {}
		}
	},
	BindEvents: function ()
	{
		NavMenuHelper.OnClick(
		{
			args: "overview"
		});
		Controls.NavMenu__.BindEvent("onLoad", function ()
		{
			Controls.NavMenu__.FireEvent("onLoad",
			{
				appInstances: GetAppInstances(),
				tabs:
				{
					overview: Language.Translate("_NavOverview"),
					invoices: Language.Translate("_NavInvoices"),
					purchaseOrders: Language.Translate("_NavOrders"),
					contracts: Language.Translate("_NavContracts"),
					vendorRegistrations: Language.Translate("_NavVendorRegistrations"),
					relatedDoc: Language.Translate("_NavRelatedDoc"),
					contacts: Language.Translate("_NavContacts"),
					settings: Language.Translate("_NavSettings")
				},
				startMenu: "overview"
			});
		});
		Controls.NavMenu__.BindEvent("onClick", NavMenuHelper.OnClick);
		for (var m in this.Menus)
		{
			if (typeof this.Menus[m].bindEvents === "function")
			{
				this.Menus[m].bindEvents();
			}
		}
	},
	Init: function ()
	{
		return Sys.Helpers.Promise.Create(function (resolve)
		{
			Object.keys(NavMenuHelper.Menus).reduce(function (p, l)
				{
					return p.Then(function ()
					{
						return NavMenuHelper.Menus[l].init();
					});
				}, Sys.Helpers.Promise.Resolve())
				.Then(function ()
				{
					NavMenuHelper.OnClick(
					{
						args: "overview"
					});
					Process.SetHelpId(2526);
					resolve();
				});
		});
	},
	OnClick: function (evt)
	{
		var panes = Controls.form_content_right.GetPanes();
		var menu = NavMenuHelper.Menus[evt.args];
		for (var _i = 0, panes_2 = panes; _i < panes_2.length; _i++)
		{
			var pane = panes_2[_i];
			pane.Hide(!menu.panes[pane.GetName()]);
		}
		menu.onStart();
	}
};
var ReportDataHelper = {
	GetXValues: function (endDate)
	{
		var dates = [];
		for (var month = 0; month < 12; month++)
		{
			var d = new Date(endDate);
			d.setDate(1);
			d.setMonth(endDate.getMonth() - month);
			dates.push(Sys.Helpers.Date.Format(d, "yyyy-mm"));
		}
		return dates;
	},
	CurrencyFormat: function (yValue)
	{
		if (!yValue)
		{
			return "";
		}
		var currency = VendorDetailsHelper.CompanyCurrency;
		var options = {
			minimumFractionDigits: 0,
			maximumFractionDigits: 0
		};
		if (currency)
		{
			options.style = "currency";
			options.currency = currency.toUpperCase();
		}
		var formatter = new Intl.NumberFormat(User.culture, options);
		var formattedCurrency = formatter.format(123); //123 is only used to be replaced later
		var shortenedFormat = Language.FormatNumber(yValue,
		{
			shorten: true,
			shortenedFormat: Language.NumberShortenedFormats.auto
		});
		return formattedCurrency.replace("123", shortenedFormat);
	},
	GetInvoicingHistoryData: function (companyCode, vendorNumber)
	{
		var values = {
			pendingPayment:
			{},
			paid:
			{},
			overdue:
			{}
		};
		var dates = ReportDataHelper.GetXValues(new Date());
		var populateValuesAndDatesWithResults = function (queryResults, valueTypeToFill)
		{
			for (var _i = 0, queryResults_1 = queryResults; _i < queryResults_1.length; _i++)
			{
				var result = queryResults_1[_i];
				if (!valueTypeToFill[result.InvoiceDateMonth__])
				{
					valueTypeToFill[result.InvoiceDateMonth__] = 0;
				}
				valueTypeToFill[result.InvoiceDateMonth__] += parseFloat(result["__SUM__:LocalInvoiceAmount__"]);
			}
		};
		var buildInvoicingHistoryDataPromisedQuery = function (filter)
		{
			var options = {
				table: "CDNAME#Vendor invoice",
				filter: filter,
				attributes: ["__SUM__:LocalInvoiceAmount__", "SUBSTRING(CHR(InvoiceDate__),1,7) 'InvoiceDateMonth__'"],
				maxRecords: 100,
				additionalOptions:
				{
					asAdmin: true,
					queryOptions: "FastSearch=1"
				}
			};
			return Sys.GenericAPI.PromisedQuery(options);
		};
		return Sys.Helpers.Promise.Create(function (resolve, reject)
		{
			// Build promise for each query
			var promisedQueries = [];
			var now = new Date();
			var d12MonthsAgo = Sys.Helpers.Date.Date2DBDate(new Date(now.getFullYear() - 1, now.getMonth(), 0));
			var nowDBDate = Sys.Helpers.Date.Date2DBDate(now);
			// not paid,rejected,reversed,expired and DueDate not <= next 5 days
			var filterInvoicePendingPayment = Sys.Helpers.LdapUtil.FilterAnd(Sys.Helpers.LdapUtil.FilterEqual("CompanyCode__", companyCode), Sys.Helpers.LdapUtil.FilterEqual("VendorNumber__", vendorNumber), Sys.Helpers.LdapUtil.FilterGreaterOrEqual("InvoiceDate__", d12MonthsAgo), Sys.Helpers.LdapUtil.FilterNotIn("InvoiceStatus__", ["Paid", "Reversed", "Rejected", "Expired"]), Sys.Helpers.LdapUtil.FilterOr(Sys.Helpers.LdapUtil.FilterNot(Sys.Helpers.LdapUtil.FilterLesserOrEqual("DueDate__", nowDBDate)), Sys.Helpers.LdapUtil.FilterNotExist("DueDate__")));
			// paid
			var filterInvoicePaid = Sys.Helpers.LdapUtil.FilterAnd(Sys.Helpers.LdapUtil.FilterEqual("CompanyCode__", companyCode), Sys.Helpers.LdapUtil.FilterEqual("VendorNumber__", vendorNumber), Sys.Helpers.LdapUtil.FilterGreaterOrEqual("InvoiceDate__", d12MonthsAgo), Sys.Helpers.LdapUtil.FilterEqual("InvoiceStatus__", "Paid"));
			// not paid,rejected,reversed,expired and DueDate <= next 5 days
			var filterInvoicePaymentOverdue = Sys.Helpers.LdapUtil.FilterAnd(Sys.Helpers.LdapUtil.FilterEqual("CompanyCode__", companyCode), Sys.Helpers.LdapUtil.FilterEqual("VendorNumber__", vendorNumber), Sys.Helpers.LdapUtil.FilterGreaterOrEqual("InvoiceDate__", d12MonthsAgo), Sys.Helpers.LdapUtil.FilterNotIn("InvoiceStatus__", ["Paid", "Reversed", "Rejected", "Expired"]), Sys.Helpers.LdapUtil.FilterLesserOrEqual("DueDate__", nowDBDate));
			var promisePending = buildInvoicingHistoryDataPromisedQuery(filterInvoicePendingPayment.toString())
				.Then(function (queryResults)
				{
					return populateValuesAndDatesWithResults(queryResults, values.pendingPayment);
				});
			var promisePaid = buildInvoicingHistoryDataPromisedQuery(filterInvoicePaid.toString())
				.Then(function (queryResults)
				{
					return populateValuesAndDatesWithResults(queryResults, values.paid);
				});
			var promiseOverdue = buildInvoicingHistoryDataPromisedQuery(filterInvoicePaymentOverdue.toString())
				.Then(function (queryResults)
				{
					return populateValuesAndDatesWithResults(queryResults, values.overdue);
				});
			promisedQueries.push(promisePending);
			promisedQueries.push(promisePaid);
			promisedQueries.push(promiseOverdue);
			Sys.Helpers.Synchronizer.All(promisedQueries).Then(function ()
			{
				var abscissa = dates.sort();
				resolve(
				{
					type: Controls.ReportInvoicingHistory__.Types.VBarStacked,
					xAxis:
					{
						values: abscissa
					},
					yAxis:
					{
						min: 0
					},
					legend:
					{
						enabled: true,
						reversed: true
					},
					series: [
					{
						name: Language.Translate("_Paid"),
						type: Controls.ReportInvoicingHistory__.Types.VBarStacked,
						color: "rgba(0, 152, 170, 0.4)",
						dataLabelsColor: "rgba(0, 152, 170, 1)",
						dataLabelsFormatter: ReportDataHelper.CurrencyFormat,
						values: values.paid
					},
					{
						name: Language.Translate("_Pending"),
						type: Controls.ReportInvoicingHistory__.Types.VBarStacked,
						color: "rgba(241, 174, 0, 0.4)",
						dataLabelsColor: "rgba(241, 174, 0, 1)",
						dataLabelsFormatter: ReportDataHelper.CurrencyFormat,
						values: values.pendingPayment
					},
					{
						name: Language.Translate("_Overdue"),
						type: Controls.ReportInvoicingHistory__.Types.VBarStacked,
						color: "rgba(216, 38, 46, 0.4)",
						dataLabelsColor: "rgba(216, 38, 46, 1)",
						dataLabelsFormatter: ReportDataHelper.CurrencyFormat,
						values: values.overdue
					}],
					dataLabelsEnabled: true
				});
			});
		});
	}
};

function run()
{
	// BindEvents must be synchronous
	// The framework is executing HTML controls script just after customscript execution
	NavMenuHelper.BindEvents();
	ActionMenuHelper.BindEvents();
	// Init can be asynchronous
	return VendorDetailsHelper.Init()
		.Then(NavMenuHelper.Init)
		.Then(ActionMenuHelper.Init);
}
run();