{
	"*": {
		"Screen": {
			"type": "FlexibleFormLayout",
			"options": {
				"backColor": "text-backgroundcolor-color7",
				"splitterType": "vertical",
				"splitterOptions": {
					"sizeable": false,
					"sidebarWidth": 320,
					"unit": "px",
					"fixedSlider": true
				},
				"style": "FlexibleFormLight",
				"version": 0
			},
			"*": {
				"form-header": {
					"type": "FlexibleFormHeader",
					"options": {
						"hidePanelsMenu": true,
						"hideDesignButton": true,
						"version": 0
					},
					"stamp": 1,
					"data": []
				},
				"form-center": {
					"type": "FlexibleFormCenter",
					"options": {
						"version": 0
					},
					"*": {
						"form-content": {
							"type": "FlexibleFormContent",
							"options": {
								"version": 0
							},
							"*": {
								"form-content-left": {
									"type": "FlexibleFormSplitter",
									"options": {
										"version": 1,
										"hidden": false,
										"box": {
											"top": "0px",
											"left": "0px",
											"width": "320px"
										},
										"hideSeparator": true
									},
									"*": {
										"form-content-left-1": {
											"type": "FlexibleFormLine",
											"data": [],
											"options": {
												"version": 0
											},
											"stamp": 2,
											"*": {
												"VendorDetailsPane": {
													"type": "PanelData",
													"options": {
														"border": {},
														"version": 0,
														"labelLength": 0,
														"iconURL": "",
														"TitleFontSize": "M",
														"hideActionButtons": true,
														"hideTitleBar": true,
														"hideTitle": true,
														"backgroundcolor": "default",
														"labelsAlignment": "right",
														"label": "_VendorDetails",
														"leftImageURL": "",
														"removeMargins": false,
														"elementsAlignment": "left",
														"sameHeightAsSiblings": false
													},
													"*": {
														"FieldsManager": {
															"type": "FieldsManager",
															"options": {
																"controlsAssociation": {},
																"version": 0
															},
															"*": {
																"Grid": {
																	"type": "GridLayout",
																	"options": {
																		"lines": 14,
																		"columns": 2,
																		"version": 0,
																		"ctrlsPos": {
																			"Spacer_line1__": {
																				"line": 1,
																				"column": 1
																			},
																			"Vendor_Info__": {
																				"line": 2,
																				"column": 1
																			},
																			"Spacer_line2__": {
																				"line": 3,
																				"column": 1
																			},
																			"VendorDetailsName__": {
																				"line": 4,
																				"column": 1
																			},
																			"Spacer_line3__": {
																				"line": 5,
																				"column": 1
																			},
																			"CompanyCode__": {
																				"line": 6,
																				"column": 1
																			},
																			"VendorNumber__": {
																				"line": 7,
																				"column": 1
																			},
																			"VendorDetailsCompanyCode__": {
																				"line": 8,
																				"column": 1
																			},
																			"VendorDetailsVendorNumber__": {
																				"line": 9,
																				"column": 1
																			},
																			"VendorDetailsEmail__": {
																				"line": 10,
																				"column": 1
																			},
																			"VendorDetailsPhoneNumber__": {
																				"line": 11,
																				"column": 1
																			},
																			"Spacer_line4__": {
																				"line": 12,
																				"column": 1
																			},
																			"ActionMenu__": {
																				"line": 13,
																				"column": 1
																			},
																			"Spacer_line5__": {
																				"line": 14,
																				"column": 1
																			}
																		},
																		"colspans": [
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			],
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			],
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			],
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			],
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			],
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			],
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			],
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			],
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			],
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			],
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			],
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			],
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			],
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			]
																		]
																	},
																	"data": [
																		"fields"
																	],
																	"*": {
																		"Spacer_line1__": {
																			"type": "Spacer",
																			"data": false,
																			"options": {
																				"height": "",
																				"width": "",
																				"color": "default",
																				"label": "Spacer line",
																				"version": 0
																			},
																			"stamp": 3
																		},
																		"Vendor_Info__": {
																			"type": "HTML",
																			"data": false,
																			"options": {
																				"label": "HTML content",
																				"htmlContent": "<div>\n\t<div id=\"vendor\">\n\t\t<i class=\"fa fa-building-o fa-5x\" aria-hidden=\"true\"></i>\n\t</div>\n</div>",
																				"width": "100%",
																				"version": 0,
																				"css": "@ #vendor { text-align:center;}@ "
																			},
																			"stamp": 4
																		},
																		"Spacer_line2__": {
																			"type": "Spacer",
																			"data": false,
																			"options": {
																				"height": "",
																				"width": "",
																				"color": "default",
																				"label": "Spacer line",
																				"version": 0
																			},
																			"stamp": 5
																		},
																		"VendorDetailsName__": {
																			"type": "Text",
																			"data": false,
																			"options": {
																				"marge": 0,
																				"textSize": "XXL",
																				"textAlignment": "center",
																				"textStyle": "default",
																				"textColor": "default",
																				"backgroundcolor": "default",
																				"image_url": "",
																				"iconClass": "",
																				"label": "",
																				"version": 0
																			},
																			"stamp": 6
																		},
																		"Spacer_line3__": {
																			"type": "Spacer",
																			"data": false,
																			"options": {
																				"height": "",
																				"width": "",
																				"color": "default",
																				"label": "Spacer line",
																				"version": 0
																			},
																			"stamp": 7
																		},
																		"CompanyCode__": {
																			"type": "ShortText",
																			"data": [
																				"CompanyCode__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"iconClass": "fa fa-at fa-fw",
																				"version": 1,
																				"label": "_CompanyCode",
																				"activable": true,
																				"width": 150,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"browsable": false,
																				"readonly": true,
																				"hidden": true
																			},
																			"stamp": 8
																		},
																		"VendorNumber__": {
																			"type": "ShortText",
																			"data": [
																				"VendorNumber__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"iconClass": "fa fa-at fa-fw",
																				"version": 1,
																				"label": "_VendorNumber",
																				"activable": true,
																				"width": 150,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"browsable": false,
																				"readonly": true,
																				"hidden": true
																			},
																			"stamp": 9
																		},
																		"VendorDetailsCompanyCode__": {
																			"type": "Text",
																			"data": false,
																			"options": {
																				"marge": 0,
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"backgroundcolor": "default",
																				"iconClass": "fa fa-key fa-fw",
																				"label": "",
																				"version": 0
																			},
																			"stamp": 10
																		},
																		"VendorDetailsVendorNumber__": {
																			"type": "Text",
																			"data": false,
																			"options": {
																				"marge": 0,
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"backgroundcolor": "default",
																				"iconClass": "fa fa-key fa-fw",
																				"label": "",
																				"version": 0
																			},
																			"stamp": 11
																		},
																		"VendorDetailsEmail__": {
																			"type": "Text",
																			"data": false,
																			"options": {
																				"marge": 0,
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"backgroundcolor": "default",
																				"image_url": "",
																				"iconClass": "fa fa-at fa-fw",
																				"label": "",
																				"version": 0
																			},
																			"stamp": 12
																		},
																		"VendorDetailsPhoneNumber__": {
																			"type": "Text",
																			"data": false,
																			"options": {
																				"marge": 0,
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"backgroundcolor": "default",
																				"image_url": "",
																				"iconClass": "fa fa-phone fa-fw",
																				"label": "",
																				"version": 0
																			},
																			"stamp": 13
																		},
																		"Spacer_line4__": {
																			"type": "Spacer",
																			"data": false,
																			"options": {
																				"height": "",
																				"width": "",
																				"color": "default",
																				"label": "Spacer line",
																				"version": 0
																			},
																			"stamp": 14
																		},
																		"ActionMenu__": {
																			"type": "HTML",
																			"data": false,
																			"options": {
																				"label": "ActionMenu__",
																				"width": "100%",
																				"version": 0,
																				"htmlContent": "<input id=\"onActionLoad\" type=\"hidden\" onclick=\"onActionLoad(event);\">\n<div class=\"nav-body\">\n    <ul class=\"nav-topbar\">\n        <li class=\"nav-item\">\n            <a class=\"nav-link\" id=\"MoreDetails\" onclick=\"MoreDetails()\">_More_Details</a>\n        </li>\n    </ul>\n</div>\n<script>\n    window.postMessage({ eventName: \"onActionLoad\", control: \"ActionMenu__\" }, document.location.origin);\n    function onActionLoad(evt) {\n        for (var element in evt.tabs) {\n            var elem = document.getElementById(element);\n            if (elem)\n                elem.innerText = evt.tabs[element];\n        }\n    }\n    function MoreDetails() {\n        ClearTabs();\n        window.postMessage({ eventName: \"MoreDetails\", control: \"ActionMenu__\" }, document.location.origin);\n    }\n</script>"
																			},
																			"stamp": 15
																		},
																		"Spacer_line5__": {
																			"type": "Spacer",
																			"data": false,
																			"options": {
																				"height": "",
																				"width": "",
																				"color": "default",
																				"label": "Spacer line",
																				"version": 0
																			},
																			"stamp": 16
																		}
																	},
																	"stamp": 17
																}
															},
															"stamp": 18,
															"data": []
														}
													},
													"stamp": 19,
													"data": []
												}
											}
										},
										"form-content-left-2": {
											"type": "FlexibleFormLine",
											"data": [],
											"options": {
												"version": 0
											},
											"stamp": 20,
											"*": {
												"MenuPane": {
													"type": "PanelData",
													"data": [],
													"options": {
														"border": {},
														"labelLength": 0,
														"iconURL": "",
														"TitleFontSize": "M",
														"hideActionButtons": true,
														"hideTitleBar": true,
														"hideTitle": true,
														"backgroundcolor": "default",
														"labelsAlignment": "right",
														"label": "_Menu",
														"leftImageURL": "",
														"removeMargins": false,
														"elementsAlignment": "left",
														"version": 0,
														"sameHeightAsSiblings": false
													},
													"stamp": 21,
													"*": {
														"FieldsManager": {
															"type": "FieldsManager",
															"data": [],
															"options": {
																"controlsAssociation": {},
																"version": 0
															},
															"stamp": 22,
															"*": {
																"Grid": {
																	"type": "GridLayout",
																	"data": [
																		"fields"
																	],
																	"options": {
																		"ctrlsPos": {
																			"NavMenu__": {
																				"line": 1,
																				"column": 1
																			}
																		},
																		"lines": 1,
																		"columns": 2,
																		"colspans": [
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			]
																		],
																		"version": 0
																	},
																	"stamp": 23,
																	"*": {
																		"NavMenu__": {
																			"type": "HTML",
																			"data": false,
																			"options": {
																				"label": "HTML content",
																				"htmlContent": "<input id=\"onLoad\" type=\"hidden\" onclick=\"OnLoad(event);\">\n<input id=\"onPackageLoad\" type=\"hidden\" onclick=\"OnPackageLoad(event);\">\n<input id=\"onChangeTab\" type=\"hidden\" onclick=\"OnChangeTab(event);\">\n<div id=\"NavMenu\" class=\"nav-body\" style=\"\">\n\t<ul class=\"nav-sidebar\">\n\t\t<li class=\"nav-item\">\n\t\t\t<a id=\"overview\" onclick=\"OnClickTab(this.id);\" href=\"#\" class=\"nav-link\">\n\t\t\t\t<span class=\"icon fa fa-home\"></span>\n\t\t\t\t<span id=\"overviewLbl\"/>\n\t\t\t</a>\n\t\t</li>\n\t\t<li class=\"nav-item-divider\"></li>\n\t\t<li class=\"nav-item\">\n\t\t\t<a id=\"invoices\" style=\"display:none\" data-package=\"P2P_AP\" onclick=\"OnClickTab(this.id);\" href=\"#\" class=\"nav-link\">\n\t\t\t\t<span id=\"invoicesLbl\"/>\n\t\t\t</a>\n\t\t</li>\n\t\t<li class=\"nav-item\">\n\t\t\t<a id=\"purchaseOrders\" style=\"display:none\" data-package=\"P2P_PAC\" onclick=\"OnClickTab(this.id);\" href=\"#\" class=\"nav-link\">\n\t\t\t\t<span id=\"purchaseOrdersLbl\"/>\n\t\t\t</a>\n\t\t</li>\n\t\t<li class=\"nav-item\">\n\t\t\t<a id=\"contracts\" style=\"display:none\" data-package=\"P2P_CONTRACT\" onclick=\"OnClickTab(this.id);\" href=\"#\" class=\"nav-link\">\n\t\t\t\t<span id=\"contractsLbl\"/>\n\t\t\t</a>\n\t\t</li>\n\t\t<li class=\"nav-item\">\n\t\t\t<a id=\"vendorRegistrations\" style=\"display:none\" data-package=\"P2P_AP\" onclick=\"OnClickTab(this.id);\" href=\"#\" class=\"nav-link\">\n\t\t\t\t<span id=\"vendorRegistrationsLbl\"/>\n\t\t\t</a>\n\t\t</li>\n\t\t<li class=\"nav-item\">\n\t\t\t<a id=\"relatedDoc\" style=\"display:none\" data-package=\"DD\" onclick=\"OnClickTab(this.id);\" href=\"#\" class=\"nav-link\">\n\t\t\t\t<span id=\"relatedDocLbl\"/>\n\t\t\t</a>\n\t\t</li>\n\t\t<li class=\"nav-item-divider\"></li>\n\t\t<li class=\"nav-item\">\n\t\t\t<a id=\"contacts\" style=\"display:none\" data-package=\"P2P\" onclick=\"OnClickTab(this.id);\" href=\"#\" class=\"nav-link\">\n\t\t\t\t<span class=\"icon fa fa-users\"></span>\n\t\t\t\t<span id=\"contactsLbl\"/>\n\t\t\t</a>\n\t\t</li>\n\t\t<li class=\"nav-item\">\n\t\t\t<a id=\"settings\" style=\"display:none\" data-package=\"P2P\" href=\"#\" onclick=\"OnClickTab(this.id);\"class=\"nav-link\">\n\t\t\t\t<span class=\"icon fa fa-gear\"></span>\n\t\t\t\t<span id=\"settingsLbl\"/>\n\t\t\t</a>\n\t\t</li>\n\t</ul>\n</div>\n<script>\n\twindow.postMessage({eventName: \"onLoad\", control: \"NavMenu__\"}, document.location.origin);\n\tvar tabItems = document.getElementById(\"NavMenu\").getElementsByClassName(\"nav-link\");\n\tfunction OnClickTab(tabID) {\n\t\tClearTabs();\n\n\t\tvar tab = document.getElementById(tabID);\n\t\ttab.className = \"nav-link active\";\n\t\twindow.postMessage({eventName: \"onClick\", control: \"NavMenu__\", args: tab.id}, document.location.origin);\n\t}\n\tfunction OnChangeTab(evt)\n\t{\n\t\tOnClickTab(evt.tabId);\n\t}\n\tfunction OnLoad(evt) {\n\t\tfor (var lbl in evt.tabs)\n\t\t{\n\t\t\tvar elem = document.getElementById(lbl + \"Lbl\");\n\t\t\tif (elem)\n\t\t\t\telem.innerText = evt.tabs[lbl];\n\t\t}\n\t\tfor (var i = 0; i < tabItems.length; i++) {\n\t\t\tvar package = tabItems[i].getAttribute(\"data-package\");\n\t\t\tif (package && evt.appInstances[package])\n\t\t\t\ttabItems[i].style.display = \"inherit\";\n\t\t}\n\t\tvar startMenu = document.getElementById(evt.startMenu);\n\t\tstartMenu.className = \"nav-link active\";\n\t}\n\tfunction ClearTabs() {\n\t\tfor (var i = 0; i < tabItems.length; i++)\n\t\t\ttabItems[i].className = \"nav-link\";\n\t}\n</script>",
																				"width": "100%",
																				"version": 0
																			},
																			"stamp": 24
																		}
																	}
																}
															}
														}
													}
												}
											}
										}
									},
									"stamp": 25,
									"data": []
								}
							},
							"stamp": 26,
							"data": []
						},
						"form-content-right": {
							"type": "FlexibleFormSplitter",
							"options": {
								"version": 1,
								"hidden": false,
								"box": {
									"top": "0px",
									"left": "320px",
									"width": "calc(100 % - 320px)"
								}
							},
							"*": {
								"form-content-right-1": {
									"type": "FlexibleFormLine",
									"data": [],
									"options": {
										"additionalCssStyle": "flexParent",
										"version": 0
									},
									"stamp": 27,
									"*": {
										"PurchaseOrdersMetricsPanel": {
											"type": "PanelData",
											"data": [],
											"options": {
												"border": {},
												"iconURL": "",
												"flexPanel": true,
												"TitleFontSize": "M",
												"hideActionButtons": true,
												"hideTitleBar": false,
												"hideTitle": false,
												"sameHeightAsSiblings": false,
												"backgroundcolor": "default",
												"labelsAlignment": "right",
												"label": "_PurchaseOrdersMetricsPanel",
												"hidden": false,
												"leftImageURL": "",
												"removeMargins": true,
												"panelStyle": "FlexibleFormPanelLight flexPanel",
												"elementsAlignment": "center"
											},
											"stamp": 28,
											"*": {
												"FieldsManager": {
													"type": "FieldsManager",
													"data": [],
													"options": {
														"controlsAssociation": {}
													},
													"stamp": 29,
													"*": {
														"Grid": {
															"type": "HorizontalLayout",
															"data": [
																"fields"
															],
															"options": {
																"ctrlsPos": {
																	"OrdersToReceiveCounter__": {
																		"line": 1,
																		"column": 1
																	},
																	"OrdersToInvoiceCounter__": {
																		"line": 2,
																		"column": 1
																	},
																	"TotalOrdersCounter__": {
																		"line": 3,
																		"column": 1
																	}
																},
																"lines": 3,
																"colspans": [
																	[
																		{
																			"index": 0,
																			"length": 2
																		}
																	],
																	[
																		{
																			"index": 0,
																			"length": 2
																		}
																	],
																	[
																		{
																			"index": 0,
																			"length": 2
																		}
																	]
																],
																"version": 0
															},
															"stamp": 30,
															"*": {
																"OrdersToReceiveCounter__": {
																	"type": "Counter",
																	"data": false,
																	"options": {
																		"label": "_Edit a metric",
																		"width": 230,
																		"version": 0,
																		"viewName": "_Purchase orders to receive",
																		"tabName": "_Purchase orders",
																		"viewData": "Purchase order V2",
																		"uiCounterType": "CountAndSum",
																		"counterType": "CountAndSum",
																		"displayFormat": "{\"upCounter\":\"Count\",\"upFormat\":\"{0}\",\"downFormat\":\"{0}\"}",
																		"counterLabel": "_OrdersToReceiveCounterLabel",
																		"warningThreshold": "1",
																		"errorThreshold": "",
																		"counterSumType": "Currency",
																		"counterSumCurrencyParam": "LocalCurrency__",
																		"counterTypeParam": "OpenOrderLocalCurrency__",
																		"counterTypeParam2": "",
																		"operation": false,
																		"numberFormatting": "Automatic",
																		"numberOfDecimals": null,
																		"clickable": true,
																		"delayLoad": true
																	},
																	"stamp": 31
																},
																"OrdersToInvoiceCounter__": {
																	"type": "Counter",
																	"data": false,
																	"options": {
																		"label": "_Edit a metric",
																		"width": 230,
																		"version": 0,
																		"viewName": "_SIM_View_PurchaseOrdersHeaders - embedded",
																		"tabName": "_My tables-AP-Embedded",
																		"viewData": "AP - Purchase order - Headers__",
																		"uiCounterType": "SumAndCount",
																		"counterType": "CountAndSum",
																		"displayFormat": "{\"upCounter\":\"Sum\",\"upFormat\":\"{0}\",\"downFormat\":\"_{0} order(s)\"}",
																		"counterLabel": "_OrdersToInvoiceCounterLabel",
																		"warningThreshold": "",
																		"errorThreshold": "",
																		"counterSumType": "Currency",
																		"counterSumCurrencyParam": "Currency__",
																		"counterTypeParam": "OrderedAmount__",
																		"counterTypeParam2": "InvoicedAmount__",
																		"operation": true,
																		"numberFormatting": "Automatic",
																		"numberOfDecimals": null,
																		"clickable": true,
																		"delayLoad": true
																	},
																	"stamp": 32
																},
																"TotalOrdersCounter__": {
																	"type": "Counter",
																	"data": false,
																	"options": {
																		"label": "_Edit a metric",
																		"width": 230,
																		"version": 0,
																		"viewName": "_All purchase orders",
																		"tabName": "_Purchase orders",
																		"viewData": "Purchase order V2",
																		"uiCounterType": "SumAndCount",
																		"counterType": "CountAndSum",
																		"displayFormat": "{\"upCounter\":\"Sum\",\"upFormat\":\"{0}\",\"downFormat\":\"_{0} order(s)\"}",
																		"counterLabel": "_OrdersTotalCounterLabel",
																		"warningThreshold": "",
																		"errorThreshold": "",
																		"counterSumType": "Currency",
																		"counterSumCurrencyParam": "Currency__",
																		"counterTypeParam": "TotalNetAmount__",
																		"numberFormatting": "Automatic",
																		"numberOfDecimals": null,
																		"clickable": true,
																		"delayLoad": true,
																		"counterTypeParam2": "TotalNetAmount__",
																		"operation": false
																	},
																	"stamp": 33
																}
															}
														}
													}
												}
											}
										},
										"InvoicesMetricsPanel": {
											"type": "PanelData",
											"data": [],
											"options": {
												"border": {},
												"iconURL": "",
												"flexPanel": true,
												"TitleFontSize": "M",
												"hideActionButtons": true,
												"hideTitleBar": false,
												"hideTitle": false,
												"sameHeightAsSiblings": false,
												"backgroundcolor": "default",
												"labelsAlignment": "right",
												"label": "_InvoicesMetricsPanel",
												"hidden": false,
												"leftImageURL": "",
												"removeMargins": true,
												"panelStyle": "FlexibleFormPanelLight flexPanel",
												"elementsAlignment": "center"
											},
											"stamp": 34,
											"*": {
												"FieldsManager": {
													"type": "FieldsManager",
													"data": [],
													"options": {
														"controlsAssociation": {},
														"version": 0
													},
													"stamp": 35,
													"*": {
														"Grid": {
															"type": "HorizontalLayout",
															"data": [
																"fields"
															],
															"options": {
																"ctrlsPos": {
																	"InvoicesOverdueCounter__": {
																		"line": 1,
																		"column": 1
																	},
																	"InvoicesPendingCounter__": {
																		"line": 2,
																		"column": 1
																	},
																	"InvoicesOnHoldCounter__": {
																		"line": 3,
																		"column": 1
																	},
																	"InvoicesTotalCounter__": {
																		"line": 4,
																		"column": 1
																	}
																},
																"lines": 4,
																"colspans": [
																	[
																		{
																			"index": 0,
																			"length": 2
																		}
																	],
																	[
																		{
																			"index": 0,
																			"length": 2
																		}
																	],
																	[
																		{
																			"index": 0,
																			"length": 2
																		}
																	],
																	[
																		{
																			"index": 0,
																			"length": 2
																		}
																	]
																],
																"version": 0
															},
															"stamp": 36,
															"*": {
																"InvoicesOverdueCounter__": {
																	"type": "Counter",
																	"data": false,
																	"options": {
																		"label": "_Edit a metric",
																		"width": 230,
																		"version": 0,
																		"viewName": "_AP_SAP_View_Invoices overdue",
																		"tabName": "_My documents-AP_SAP",
																		"viewData": "Vendor invoice",
																		"uiCounterType": "SumAndCount",
																		"counterType": "CountAndSum",
																		"displayFormat": "{\"upCounter\":\"Sum\",\"upFormat\":\"{0}\",\"downFormat\":\"_{0} invoice(s)\"}",
																		"counterLabel": "_InvoicesOverdueCounterLabel",
																		"warningThreshold": "",
																		"errorThreshold": "1",
																		"counterSumType": "Currency",
																		"counterSumCurrencyParam": "LocalCurrency__",
																		"counterTypeParam": "LocalNetAmount__",
																		"numberFormatting": "Automatic",
																		"numberOfDecimals": null,
																		"clickable": true,
																		"delayLoad": true
																	},
																	"stamp": 37
																},
																"InvoicesPendingCounter__": {
																	"type": "Counter",
																	"data": false,
																	"options": {
																		"label": "_Edit a metric",
																		"width": 230,
																		"version": 0,
																		"viewName": "_AP_View - All invoices",
																		"tabName": "_My documents-AP_SAP",
																		"viewData": "Vendor invoice",
																		"uiCounterType": "SumAndCount",
																		"counterType": "CountAndSum",
																		"displayFormat": "{\"upCounter\":\"Sum\",\"upFormat\":\"{0}\",\"downFormat\":\"_{0} invoice(s)\"}",
																		"counterLabel": "_InvoicesPendingCounterLabel",
																		"warningThreshold": "",
																		"errorThreshold": "",
																		"counterSumType": "Currency",
																		"counterSumCurrencyParam": "LocalCurrency__",
																		"counterTypeParam": "LocalNetAmount__",
																		"numberFormatting": "Automatic",
																		"numberOfDecimals": null,
																		"clickable": true,
																		"delayLoad": true
																	},
																	"stamp": 38
																},
																"InvoicesOnHoldCounter__": {
																	"type": "Counter",
																	"data": false,
																	"options": {
																		"label": "_Edit a metric",
																		"width": 230,
																		"version": 0,
																		"viewName": "_AP_View - On hold",
																		"tabName": "_My documents-AP_SAP",
																		"viewData": "Vendor invoice",
																		"uiCounterType": "SumAndCount",
																		"counterType": "CountAndSum",
																		"displayFormat": "{\"upCounter\":\"Sum\",\"upFormat\":\"{0}\",\"downFormat\":\"_{0} invoice(s)\"}",
																		"counterLabel": "_InvoicesOnHoldCounterLabel",
																		"warningThreshold": "",
																		"errorThreshold": "1",
																		"counterSumType": "Currency",
																		"counterSumCurrencyParam": "LocalCurrency__",
																		"counterTypeParam": "LocalNetAmount__",
																		"numberFormatting": "Automatic",
																		"numberOfDecimals": null,
																		"clickable": true,
																		"delayLoad": true
																	},
																	"stamp": 39
																},
																"InvoicesTotalCounter__": {
																	"type": "Counter",
																	"data": false,
																	"options": {
																		"label": "_Edit a metric",
																		"width": 230,
																		"version": 0,
																		"viewName": "_AP_View - All invoices",
																		"tabName": "_My documents-AP_SAP",
																		"viewData": "Vendor invoice",
																		"uiCounterType": "SumAndCount",
																		"counterType": "CountAndSum",
																		"displayFormat": "{\"upCounter\":\"Sum\",\"upFormat\":\"{0}\",\"downFormat\":\"_{0} invoice(s)\"}",
																		"counterLabel": "_InvoicesTotalCounterLabel",
																		"warningThreshold": "",
																		"errorThreshold": "",
																		"counterSumType": "Currency",
																		"counterSumCurrencyParam": "LocalCurrency__",
																		"counterTypeParam": "LocalNetAmount__",
																		"numberFormatting": "Automatic",
																		"numberOfDecimals": null,
																		"clickable": true,
																		"delayLoad": true
																	},
																	"stamp": 40
																}
															}
														}
													}
												}
											}
										},
										"DiscountMetricsPanel": {
											"type": "PanelData",
											"data": [],
											"options": {
												"border": {},
												"iconURL": "",
												"flexPanel": true,
												"TitleFontSize": "M",
												"hideActionButtons": true,
												"hideTitleBar": false,
												"hideTitle": false,
												"sameHeightAsSiblings": false,
												"backgroundcolor": "default",
												"labelsAlignment": "right",
												"label": "_DiscountMetricsPanel",
												"hidden": false,
												"leftImageURL": "",
												"removeMargins": true,
												"panelStyle": "FlexibleFormPanelLight flexPanel",
												"elementsAlignment": "center"
											},
											"stamp": 170,
											"*": {
												"FieldsManager": {
													"type": "FieldsManager",
													"data": [],
													"options": {
														"controlsAssociation": {},
														"version": 0
													},
													"stamp": 171,
													"*": {
														"Grid": {
															"type": "HorizontalLayout",
															"data": [
																"fields"
															],
															"options": {
																"ctrlsPos": {
																	"InvoicesDiscountCounter__": {
																		"line": 1,
																		"column": 1
																	}
																},
																"lines": 1,
																"colspans": [
																	[
																		{
																			"index": 0,
																			"length": 2
																		}
																	]
																],
																"version": 0
															},
															"stamp": 172,
															"*": {
																"InvoicesDiscountCounter__": {
																	"type": "Counter",
																	"data": false,
																	"options": {
																		"label": "_Edit a metric",
																		"width": 230,
																		"version": 0,
																		"viewName": "_AP_SAP_View_Invoices with available discounts",
																		"tabName": "_My documents-AP_SAP",
																		"viewData": "Vendor invoice",
																		"uiCounterType": "SumAndCount",
																		"counterType": "CountAndSum",
																		"displayFormat": "{\"upCounter\":\"Sum\",\"upFormat\":\"{0}\",\"downFormat\":\"_{0} invoice(s)\"}",
																		"counterLabel": "_InvoicesDiscountCounterLabel",
																		"warningThreshold": "1",
																		"errorThreshold": "",
																		"counterSumType": "Currency",
																		"counterSumCurrencyParam": "LocalCurrency__",
																		"counterTypeParam": "LocalEstimatedDiscountAmount__",
																		"numberFormatting": "Automatic",
																		"numberOfDecimals": null,
																		"clickable": true,
																		"delayLoad": true
																	},
																	"stamp": 173
																}
															}
														}
													}
												}
											}
										}
									}
								},
								"form-content-right-2": {
									"type": "FlexibleFormLine",
									"data": [],
									"options": {
										"version": 0
									},
									"stamp": 41,
									"*": {
										"InvoicesPane": {
											"type": "PanelData",
											"data": [],
											"options": {
												"border": {},
												"labelLength": 0,
												"iconURL": "",
												"TitleFontSize": "M",
												"hideActionButtons": true,
												"hideTitleBar": false,
												"hideTitle": false,
												"backgroundcolor": "default",
												"labelsAlignment": "right",
												"label": "_Invoices",
												"leftImageURL": "",
												"removeMargins": false,
												"elementsAlignment": "left",
												"hidden": true,
												"version": 0,
												"sameHeightAsSiblings": false
											},
											"stamp": 42,
											"*": {
												"FieldsManager": {
													"type": "FieldsManager",
													"data": [],
													"options": {
														"controlsAssociation": {},
														"version": 0
													},
													"stamp": 43,
													"*": {
														"Grid": {
															"type": "GridLayout",
															"data": [
																"fields"
															],
															"options": {
																"ctrlsPos": {
																	"Spacer_line6__": {
																		"line": 1,
																		"column": 1
																	},
																	"Invoices__": {
																		"line": 2,
																		"column": 1
																	},
																	"Spacer_line7__": {
																		"line": 3,
																		"column": 1
																	}
																},
																"lines": 3,
																"columns": 2,
																"colspans": [
																	[
																		{
																			"index": 0,
																			"length": 2
																		}
																	],
																	[
																		{
																			"index": 0,
																			"length": 2
																		}
																	],
																	[
																		{
																			"index": 0,
																			"length": 2
																		}
																	]
																],
																"version": 0
															},
															"stamp": 44,
															"*": {
																"Spacer_line6__": {
																	"type": "Spacer",
																	"data": false,
																	"options": {
																		"height": "",
																		"width": "",
																		"color": "default",
																		"label": "Spacer line6",
																		"version": 0
																	},
																	"stamp": 45
																},
																"Invoices__": {
																	"type": "AdminList",
																	"data": false,
																	"options": {
																		"width": "100%",
																		"restrictToCurrentJobId": false,
																		"showActions": false,
																		"openInNewTab": true,
																		"openInReadOnlyMode": false,
																		"label": "View",
																		"version": 0
																	},
																	"stamp": 46
																},
																"Spacer_line7__": {
																	"type": "Spacer",
																	"data": false,
																	"options": {
																		"height": "",
																		"width": "",
																		"color": "default",
																		"label": "Spacer line7",
																		"version": 0
																	},
																	"stamp": 47
																}
															}
														}
													}
												}
											}
										}
									}
								},
								"form-content-right-3": {
									"type": "FlexibleFormLine",
									"data": [],
									"options": {
										"version": 0
									},
									"stamp": 48,
									"*": {
										"PurchaseOrdersPane": {
											"type": "PanelData",
											"data": [],
											"options": {
												"border": {},
												"labelLength": 0,
												"iconURL": "",
												"TitleFontSize": "M",
												"hideActionButtons": true,
												"hideTitleBar": false,
												"hideTitle": false,
												"backgroundcolor": "default",
												"labelsAlignment": "right",
												"label": "_PurchaseOrders",
												"leftImageURL": "",
												"removeMargins": false,
												"elementsAlignment": "left",
												"hidden": true,
												"version": 0,
												"sameHeightAsSiblings": false
											},
											"stamp": 49,
											"*": {
												"FieldsManager": {
													"type": "FieldsManager",
													"data": [],
													"options": {
														"controlsAssociation": {},
														"version": 0
													},
													"stamp": 50,
													"*": {
														"Grid": {
															"type": "GridLayout",
															"data": [
																"fields"
															],
															"options": {
																"ctrlsPos": {
																	"Spacer_line9__": {
																		"line": 1,
																		"column": 1
																	},
																	"PurchaseOrders__": {
																		"line": 2,
																		"column": 1
																	},
																	"Spacer_line10__": {
																		"line": 3,
																		"column": 1
																	}
																},
																"lines": 3,
																"columns": 2,
																"colspans": [
																	[
																		{
																			"index": 0,
																			"length": 2
																		}
																	],
																	[
																		{
																			"index": 0,
																			"length": 2
																		}
																	],
																	[
																		{
																			"index": 0,
																			"length": 2
																		}
																	]
																],
																"version": 0
															},
															"stamp": 51,
															"*": {
																"Spacer_line9__": {
																	"type": "Spacer",
																	"data": false,
																	"options": {
																		"height": "",
																		"width": "",
																		"color": "default",
																		"label": "Spacer line9",
																		"version": 0
																	},
																	"stamp": 52
																},
																"PurchaseOrders__": {
																	"type": "AdminList",
																	"data": false,
																	"options": {
																		"width": "100%",
																		"restrictToCurrentJobId": false,
																		"showActions": false,
																		"openInNewTab": true,
																		"openInReadOnlyMode": false,
																		"label": "View",
																		"version": 0
																	},
																	"stamp": 53
																},
																"Spacer_line10__": {
																	"type": "Spacer",
																	"data": false,
																	"options": {
																		"height": "",
																		"width": "",
																		"color": "default",
																		"label": "Spacer line10",
																		"version": 0
																	},
																	"stamp": 54
																}
															}
														}
													}
												}
											}
										}
									}
								},
								"form-content-right-4": {
									"type": "FlexibleFormLine",
									"data": [],
									"options": {
										"version": 0
									},
									"stamp": 55,
									"*": {
										"ContractsPane": {
											"type": "PanelData",
											"data": [],
											"options": {
												"border": {},
												"labelLength": 0,
												"iconURL": "",
												"TitleFontSize": "M",
												"hideActionButtons": true,
												"hideTitleBar": false,
												"hideTitle": false,
												"backgroundcolor": "default",
												"labelsAlignment": "right",
												"label": "_Contracts",
												"leftImageURL": "",
												"removeMargins": false,
												"elementsAlignment": "left",
												"hidden": true,
												"version": 0,
												"sameHeightAsSiblings": false
											},
											"stamp": 56,
											"*": {
												"FieldsManager": {
													"type": "FieldsManager",
													"data": [],
													"options": {
														"controlsAssociation": {},
														"version": 0
													},
													"stamp": 57,
													"*": {
														"Grid": {
															"type": "GridLayout",
															"data": [
																"fields"
															],
															"options": {
																"ctrlsPos": {
																	"Spacer_line11__": {
																		"line": 1,
																		"column": 1
																	},
																	"Contracts__": {
																		"line": 2,
																		"column": 1
																	},
																	"Spacer_line12__": {
																		"line": 3,
																		"column": 1
																	}
																},
																"lines": 3,
																"columns": 2,
																"colspans": [
																	[
																		{
																			"index": 0,
																			"length": 2
																		}
																	],
																	[
																		{
																			"index": 0,
																			"length": 2
																		}
																	],
																	[
																		{
																			"index": 0,
																			"length": 2
																		}
																	]
																],
																"version": 0
															},
															"stamp": 58,
															"*": {
																"Spacer_line11__": {
																	"type": "Spacer",
																	"data": false,
																	"options": {
																		"height": "",
																		"width": "",
																		"color": "default",
																		"label": "Spacer line11",
																		"version": 0
																	},
																	"stamp": 59
																},
																"Contracts__": {
																	"type": "AdminList",
																	"data": false,
																	"options": {
																		"width": "100%",
																		"restrictToCurrentJobId": false,
																		"showActions": false,
																		"openInNewTab": true,
																		"openInReadOnlyMode": false,
																		"label": "View",
																		"version": 0
																	},
																	"stamp": 60
																},
																"Spacer_line12__": {
																	"type": "Spacer",
																	"data": false,
																	"options": {
																		"height": "",
																		"width": "",
																		"color": "default",
																		"label": "Spacer line12",
																		"version": 0
																	},
																	"stamp": 61
																}
															}
														}
													}
												}
											}
										}
									}
								},
								"form-content-right-5": {
									"type": "FlexibleFormLine",
									"data": [],
									"options": {
										"version": 0
									},
									"stamp": 62,
									"*": {
										"VendorRegistrationsPane": {
											"type": "PanelData",
											"data": [],
											"options": {
												"border": {},
												"labelLength": 0,
												"iconURL": "",
												"TitleFontSize": "M",
												"hideActionButtons": true,
												"hideTitleBar": false,
												"hideTitle": false,
												"backgroundcolor": "default",
												"labelsAlignment": "right",
												"label": "_VendorRegistrations",
												"leftImageURL": "",
												"removeMargins": false,
												"elementsAlignment": "left",
												"hidden": true,
												"version": 0,
												"sameHeightAsSiblings": false
											},
											"stamp": 63,
											"*": {
												"FieldsManager": {
													"type": "FieldsManager",
													"data": [],
													"options": {
														"controlsAssociation": {},
														"version": 0
													},
													"stamp": 64,
													"*": {
														"Grid": {
															"type": "GridLayout",
															"data": [
																"fields"
															],
															"options": {
																"ctrlsPos": {
																	"Spacer_line18__": {
																		"line": 1,
																		"column": 1
																	},
																	"VendorRegistrations__": {
																		"line": 2,
																		"column": 1
																	},
																	"Spacer_line19__": {
																		"line": 3,
																		"column": 1
																	}
																},
																"lines": 3,
																"columns": 2,
																"colspans": [
																	[
																		{
																			"index": 0,
																			"length": 2
																		}
																	],
																	[
																		{
																			"index": 0,
																			"length": 2
																		}
																	],
																	[
																		{
																			"index": 0,
																			"length": 2
																		}
																	]
																],
																"version": 0
															},
															"stamp": 65,
															"*": {
																"Spacer_line18__": {
																	"type": "Spacer",
																	"data": false,
																	"options": {
																		"height": "",
																		"width": "",
																		"color": "default",
																		"label": "Spacer line18",
																		"version": 0
																	},
																	"stamp": 66
																},
																"VendorRegistrations__": {
																	"type": "AdminList",
																	"data": false,
																	"options": {
																		"width": "100%",
																		"restrictToCurrentJobId": false,
																		"showActions": false,
																		"openInNewTab": true,
																		"openInReadOnlyMode": false,
																		"label": "View",
																		"version": 0
																	},
																	"stamp": 67
																},
																"Spacer_line19__": {
																	"type": "Spacer",
																	"data": false,
																	"options": {
																		"height": "",
																		"width": "",
																		"color": "default",
																		"label": "Spacer line19",
																		"version": 0
																	},
																	"stamp": 68
																}
															}
														}
													}
												}
											}
										}
									}
								},
								"form-content-right-7": {
									"type": "FlexibleFormLine",
									"data": [],
									"options": {
										"version": 0
									},
									"stamp": 69,
									"*": {
										"ContactsPane": {
											"type": "PanelData",
											"data": [],
											"options": {
												"border": {},
												"labelLength": 0,
												"iconURL": "",
												"TitleFontSize": "M",
												"hideActionButtons": true,
												"hideTitleBar": false,
												"hideTitle": false,
												"backgroundcolor": "default",
												"labelsAlignment": "right",
												"label": "_Contacts",
												"leftImageURL": "",
												"removeMargins": false,
												"elementsAlignment": "left",
												"panelStyle": "FlexibleFormPanelLight",
												"hidden": true,
												"version": 0,
												"sameHeightAsSiblings": false
											},
											"stamp": 70,
											"*": {
												"FieldsManager": {
													"type": "FieldsManager",
													"data": [],
													"options": {
														"controlsAssociation": {},
														"version": 0
													},
													"stamp": 71,
													"*": {
														"Grid": {
															"type": "GridLayout",
															"data": [
																"fields"
															],
															"options": {
																"ctrlsPos": {
																	"ContactsList__": {
																		"line": 2,
																		"column": 1
																	},
																	"Spacer_line14__": {
																		"line": 1,
																		"column": 1
																	},
																	"Spacer_line17__": {
																		"line": 3,
																		"column": 1
																	}
																},
																"lines": 3,
																"columns": 2,
																"colspans": [
																	[
																		{
																			"index": 0,
																			"length": 2
																		}
																	],
																	[
																		{
																			"index": 0,
																			"length": 2
																		}
																	],
																	[
																		{
																			"index": 0,
																			"length": 2
																		}
																	]
																],
																"version": 0
															},
															"stamp": 72,
															"*": {
																"Spacer_line14__": {
																	"type": "Spacer",
																	"data": false,
																	"options": {
																		"height": "",
																		"width": "",
																		"color": "default",
																		"label": "Spacer line14",
																		"version": 0
																	},
																	"stamp": 73
																},
																"ContactsList__": {
																	"type": "AdminList",
																	"data": false,
																	"options": {
																		"width": "100%",
																		"restrictToCurrentJobId": false,
																		"showActions": false,
																		"openInNewTab": true,
																		"openInReadOnlyMode": true,
																		"label": "View",
																		"version": 0
																	},
																	"stamp": 74
																},
																"Spacer_line17__": {
																	"type": "Spacer",
																	"data": false,
																	"options": {
																		"height": "",
																		"width": "",
																		"color": "default",
																		"label": "Spacer line17",
																		"version": 0
																	},
																	"stamp": 75
																}
															}
														}
													}
												}
											}
										}
									}
								},
								"form-content-right-9": {
									"type": "FlexibleFormLine",
									"data": [],
									"options": {},
									"stamp": 76,
									"*": {
										"OrderPreferrencesPane": {
											"type": "PanelData",
											"data": [],
											"options": {
												"border": {},
												"labelLength": "220px",
												"iconURL": "",
												"TitleFontSize": "M",
												"hideActionButtons": true,
												"hideTitleBar": false,
												"hideTitle": false,
												"sameHeightAsSiblings": false,
												"backgroundcolor": "default",
												"labelsAlignment": "right",
												"label": "_OrderPreferrencesPane",
												"hidden": false,
												"leftImageURL": "",
												"removeMargins": false,
												"elementsAlignment": "left",
												"version": 0
											},
											"stamp": 77,
											"*": {
												"FieldsManager": {
													"type": "FieldsManager",
													"data": [],
													"options": {
														"controlsAssociation": {
															"POMethod__": "LabelPOMethod__",
															"LabelPOMethod__": "POMethod__"
														},
														"version": 0
													},
													"stamp": 78,
													"*": {
														"Grid": {
															"type": "GridLayout",
															"data": [
																"fields"
															],
															"options": {
																"ctrlsPos": {
																	"POMethod__": {
																		"line": 1,
																		"column": 2
																	},
																	"LabelPOMethod__": {
																		"line": 1,
																		"column": 1
																	}
																},
																"lines": 1,
																"columns": 2,
																"colspans": [
																	[]
																],
																"version": 0
															},
															"stamp": 79,
															"*": {
																"LabelPOMethod__": {
																	"type": "Label",
																	"data": [
																		"POMethod__"
																	],
																	"options": {
																		"label": "_POMethod",
																		"version": 0
																	},
																	"stamp": 80
																},
																"POMethod__": {
																	"type": "ComboBox",
																	"data": [
																		"POMethod__"
																	],
																	"options": {
																		"possibleValues": {
																			"0": "_Prompt",
																			"1": "_Email",
																			"2": "_Punchout",
																			"3": "_DontSend"
																		},
																		"maxNbLinesToDisplay": 10,
																		"version": 1,
																		"keyValueMode": true,
																		"possibleKeys": {
																			"0": "Prompt",
																			"1": "SendToVendor",
																			"2": "PunchoutMode",
																			"3": "DontSend"
																		},
																		"label": "_POMethod",
																		"activable": true,
																		"width": 230,
																		"helpText": "_POMethod_Help",
																		"helpURL": "",
																		"helpFormat": "HTML Format",
																		"helpIconPosition": "Right",
																		"DataSourceOrigin": "Standalone",
																		"DisplayedColumns": ""
																	},
																	"stamp": 81
																}
															}
														}
													}
												}
											}
										}
									}
								},
								"form-content-right-10": {
									"type": "FlexibleFormLine",
									"data": [],
									"options": {
										"version": 0
									},
									"stamp": 82,
									"*": {
										"FlipPOPane": {
											"type": "PanelData",
											"data": [],
											"options": {
												"border": {},
												"labelLength": "220px",
												"iconURL": "",
												"TitleFontSize": "M",
												"hideActionButtons": true,
												"hideTitleBar": false,
												"hideTitle": false,
												"backgroundcolor": "default",
												"labelsAlignment": "right",
												"label": "_FlipPOPane",
												"leftImageURL": "",
												"removeMargins": false,
												"elementsAlignment": "left",
												"hidden": false,
												"version": 0,
												"sameHeightAsSiblings": false
											},
											"stamp": 83,
											"*": {
												"FieldsManager": {
													"type": "FieldsManager",
													"options": {
														"controlsAssociation": {
															"VATNumber__": "LabelVATNumber__",
															"LabelVATNumber__": "VATNumber__",
															"SIRET__": "LabelSIRET__",
															"LabelSIRET__": "SIRET__",
															"InvoiceNumberSequenceId__": "LabelInvoiceNumberSequenceId__",
															"LabelInvoiceNumberSequenceId__": "InvoiceNumberSequenceId__",
															"PaymentTerms__": "LabelPaymentTerms__",
															"LabelPaymentTerms__": "PaymentTerms__",
															"SpecificMentions__": "LabelSpecificMentions__",
															"LabelSpecificMentions__": "SpecificMentions__"
														},
														"version": 0
													},
													"*": {
														"Grid": {
															"type": "GridLayout",
															"options": {
																"lines": 8,
																"columns": 2,
																"version": 0,
																"ctrlsPos": {
																	"Spacer_line__": {
																		"line": 1,
																		"column": 1
																	},
																	"FlipPODesc__": {
																		"line": 2,
																		"column": 1
																	},
																	"Spacer_line8__": {
																		"line": 3,
																		"column": 1
																	},
																	"LabelInvoiceNumberSequenceId__": {
																		"line": 4,
																		"column": 1
																	},
																	"InvoiceNumberSequenceId__": {
																		"line": 4,
																		"column": 2
																	},
																	"VATNumber__": {
																		"line": 5,
																		"column": 2
																	},
																	"LabelVATNumber__": {
																		"line": 5,
																		"column": 1
																	},
																	"SIRET__": {
																		"line": 6,
																		"column": 2
																	},
																	"LabelSIRET__": {
																		"line": 6,
																		"column": 1
																	},
																	"PaymentTerms__": {
																		"line": 7,
																		"column": 2
																	},
																	"LabelPaymentTerms__": {
																		"line": 7,
																		"column": 1
																	},
																	"SpecificMentions__": {
																		"line": 8,
																		"column": 2
																	},
																	"LabelSpecificMentions__": {
																		"line": 8,
																		"column": 1
																	}
																},
																"colspans": [
																	[
																		{
																			"index": 0,
																			"length": 2
																		}
																	],
																	[
																		{
																			"index": 0,
																			"length": 2
																		}
																	],
																	[
																		{
																			"index": 0,
																			"length": 2
																		}
																	],
																	[],
																	[],
																	[],
																	[],
																	[]
																]
															},
															"data": [
																"fields"
															],
															"*": {
																"Spacer_line__": {
																	"type": "Spacer",
																	"data": false,
																	"options": {
																		"height": "",
																		"width": "",
																		"color": "default",
																		"label": "Spacer line",
																		"version": 0
																	},
																	"stamp": 84
																},
																"FlipPODesc__": {
																	"type": "Text",
																	"data": false,
																	"options": {
																		"marge": 0,
																		"textSize": "S",
																		"textAlignment": "left",
																		"textStyle": "default",
																		"textColor": "default",
																		"backgroundcolor": "default",
																		"iconClass": "",
																		"label": "_FlipPOPane_Desc",
																		"version": 0
																	},
																	"stamp": 85
																},
																"Spacer_line8__": {
																	"type": "Spacer",
																	"data": false,
																	"options": {
																		"height": "",
																		"width": "",
																		"color": "default",
																		"label": "Spacer line8",
																		"version": 0
																	},
																	"stamp": 86
																},
																"LabelInvoiceNumberSequenceId__": {
																	"type": "Label",
																	"data": [
																		"InvoiceNumberSequenceId__"
																	],
																	"options": {
																		"label": "_Invoice number sequence id",
																		"version": 0
																	},
																	"stamp": 87
																},
																"InvoiceNumberSequenceId__": {
																	"type": "ShortText",
																	"data": [
																		"InvoiceNumberSequenceId__"
																	],
																	"options": {
																		"textSize": "S",
																		"textAlignment": "left",
																		"textStyle": "default",
																		"textColor": "default",
																		"version": 1,
																		"label": "_Invoice number sequence id",
																		"activable": true,
																		"width": 230,
																		"helpText": "_InvoiceNumberSequenceId_Help",
																		"helpURL": "",
																		"helpFormat": "HTML Format",
																		"browsable": false,
																		"readonly": true
																	},
																	"stamp": 88
																},
																"LabelVATNumber__": {
																	"type": "Label",
																	"data": [
																		"VATNumber__"
																	],
																	"options": {
																		"label": "_VATNumber",
																		"version": 0
																	},
																	"stamp": 89
																},
																"VATNumber__": {
																	"type": "ShortText",
																	"data": [
																		"VATNumber__"
																	],
																	"options": {
																		"textSize": "S",
																		"textAlignment": "left",
																		"textStyle": "default",
																		"textColor": "default",
																		"version": 1,
																		"label": "_VATNumber",
																		"activable": true,
																		"width": 230,
																		"helpText": "",
																		"helpURL": "",
																		"helpFormat": "HTML Format",
																		"length": 20,
																		"browsable": false
																	},
																	"stamp": 90
																},
																"LabelSIRET__": {
																	"type": "Label",
																	"data": [
																		"SIRET__"
																	],
																	"options": {
																		"label": "_SIRET",
																		"version": 0
																	},
																	"stamp": 91
																},
																"SIRET__": {
																	"type": "ShortText",
																	"data": [
																		"SIRET__"
																	],
																	"options": {
																		"textSize": "S",
																		"textAlignment": "left",
																		"textStyle": "default",
																		"textColor": "default",
																		"version": 1,
																		"label": "_SIRET",
																		"activable": true,
																		"width": 230,
																		"helpText": "",
																		"helpURL": "",
																		"helpFormat": "HTML Format",
																		"browsable": false
																	},
																	"stamp": 92
																},
																"LabelPaymentTerms__": {
																	"type": "Label",
																	"data": [
																		"PaymentTerms__"
																	],
																	"options": {
																		"label": "_PaymentTerms",
																		"version": 0
																	},
																	"stamp": 93
																},
																"PaymentTerms__": {
																	"type": "LongText",
																	"data": [
																		"PaymentTerms__"
																	],
																	"options": {
																		"textSize": "S",
																		"textAlignment": "left",
																		"textStyle": "default",
																		"textColor": "default",
																		"version": 1,
																		"minNbLines": 3,
																		"label": "_PaymentTerms",
																		"activable": true,
																		"width": "350",
																		"helpText": "",
																		"helpURL": "",
																		"helpFormat": "HTML Format",
																		"browsable": false
																	},
																	"stamp": 94
																},
																"LabelSpecificMentions__": {
																	"type": "Label",
																	"data": [
																		"SpecificMentions__"
																	],
																	"options": {
																		"label": "_SpecificMentions",
																		"version": 0
																	},
																	"stamp": 95
																},
																"SpecificMentions__": {
																	"type": "LongText",
																	"data": [
																		"SpecificMentions__"
																	],
																	"options": {
																		"textSize": "S",
																		"textAlignment": "left",
																		"textStyle": "default",
																		"textColor": "default",
																		"version": 1,
																		"minNbLines": 3,
																		"label": "_SpecificMentions",
																		"activable": true,
																		"width": "350",
																		"helpText": "",
																		"helpURL": "",
																		"helpFormat": "HTML Format",
																		"browsable": false
																	},
																	"stamp": 96
																}
															},
															"stamp": 97
														}
													},
													"stamp": 98,
													"data": []
												}
											}
										}
									}
								},
								"form-content-right-6": {
									"type": "FlexibleFormLine",
									"data": [],
									"options": {
										"version": 0
									},
									"stamp": 99,
									"*": {
										"RelatedDocPane": {
											"type": "PanelData",
											"data": [],
											"options": {
												"border": {},
												"labelLength": 0,
												"iconURL": "",
												"TitleFontSize": "S",
												"hideActionButtons": true,
												"hideTitleBar": false,
												"hideTitle": false,
												"backgroundcolor": "default",
												"labelsAlignment": "right",
												"label": "_Related_Doc",
												"hidden": false,
												"leftImageURL": "",
												"removeMargins": false,
												"elementsAlignment": "left",
												"version": 0,
												"sameHeightAsSiblings": false
											},
											"stamp": 100,
											"*": {
												"FieldsManager": {
													"type": "FieldsManager",
													"data": [],
													"options": {
														"controlsAssociation": {},
														"version": 0
													},
													"stamp": 101,
													"*": {
														"Grid": {
															"type": "GridLayout",
															"data": [
																"fields"
															],
															"options": {
																"ctrlsPos": {
																	"RelatedDoc__": {
																		"line": 2,
																		"column": 1
																	},
																	"Spacer_line13__": {
																		"line": 1,
																		"column": 1
																	},
																	"Spacer_line15__": {
																		"line": 3,
																		"column": 1
																	}
																},
																"lines": 3,
																"columns": 2,
																"colspans": [
																	[
																		{
																			"index": 0,
																			"length": 2
																		}
																	],
																	[
																		{
																			"index": 0,
																			"length": 2
																		}
																	],
																	[
																		{
																			"index": 0,
																			"length": 2
																		}
																	]
																],
																"version": 0
															},
															"stamp": 102,
															"*": {
																"Spacer_line13__": {
																	"type": "Spacer",
																	"data": false,
																	"options": {
																		"height": "",
																		"width": "",
																		"color": "default",
																		"label": "Spacer line13",
																		"version": 0
																	},
																	"stamp": 103
																},
																"RelatedDoc__": {
																	"type": "AdminList",
																	"data": false,
																	"options": {
																		"width": "100%",
																		"restrictToCurrentJobId": false,
																		"showActions": false,
																		"openInNewTab": false,
																		"openInReadOnlyMode": true,
																		"label": "Vue",
																		"version": 0
																	},
																	"stamp": 104
																},
																"Spacer_line15__": {
																	"type": "Spacer",
																	"data": false,
																	"options": {
																		"height": "",
																		"width": "",
																		"color": "default",
																		"label": "Spacer line15",
																		"version": 0
																	},
																	"stamp": 105
																}
															}
														}
													}
												}
											}
										}
									}
								},
								"form-content-right-8": {
									"type": "FlexibleFormLine",
									"data": [],
									"options": {
										"version": 0
									},
									"stamp": 106,
									"*": {
										"VendorInformation": {
											"type": "PanelData",
											"data": [],
											"options": {
												"border": {},
												"labelLength": 0,
												"iconURL": "",
												"TitleFontSize": "M",
												"hideActionButtons": true,
												"hideTitleBar": false,
												"hideTitle": false,
												"backgroundcolor": "default",
												"labelsAlignment": "right",
												"label": "_VendorInfo",
												"hidden": false,
												"leftImageURL": "",
												"removeMargins": false,
												"elementsAlignment": "left",
												"version": 0,
												"sameHeightAsSiblings": false
											},
											"stamp": 107,
											"*": {
												"FieldsManager": {
													"type": "FieldsManager",
													"options": {
														"controlsAssociation": {
															"VendorInfoName__": "LabelVendorInfoName__",
															"LabelVendorInfoName__": "VendorInfoName__",
															"VendorInfoVendorNumber__": "LabelVendorInfoVendorNumber__",
															"LabelVendorInfoVendorNumber__": "VendorInfoVendorNumber__",
															"VendorInfoVATNumber__": "LabelVendorInfoVATNumber__",
															"LabelVendorInfoVATNumber__": "VendorInfoVATNumber__",
															"VendorInfoFaxNumber__": "LabelVendorInfoFaxNumber__",
															"LabelVendorInfoFaxNumber__": "VendorInfoFaxNumber__",
															"VendorInfoPhoneNumber__": "LabelVendorInfoPhoneNumber__",
															"LabelVendorInfoPhoneNumber__": "VendorInfoPhoneNumber__",
															"VendorInfoPreferredInvoiceType__": "LabelVendorInfoPreferredInvoiceType__",
															"LabelVendorInfoPreferredInvoiceType__": "VendorInfoPreferredInvoiceType__",
															"VendorInfoCompanyCode__": "LabelVendorInfoCompanyCode__",
															"LabelVendorInfoCompanyCode__": "VendorInfoCompanyCode__",
															"VendorInfoEmail__": "LabelVendorInfoEmail__",
															"LabelVendorInfoEmail__": "VendorInfoEmail__",
															"VendorInfoPaymentTermCode__": "LabelVendorInfoPaymentTermCode__",
															"LabelVendorInfoPaymentTermCode__": "VendorInfoPaymentTermCode__",
															"VendorInfoCurrency__": "LabelVendorInfoCurrency__",
															"LabelVendorInfoCurrency__": "VendorInfoCurrency__",
															"Website__": "LabelWebsite__",
															"LabelWebsite__": "Website__",
															"NumberOfEmployees__": "LabelNumberOfEmployees__",
															"LabelNumberOfEmployees__": "NumberOfEmployees__",
															"YearsInBusiness__": "LabelYearsInBusiness__",
															"LabelYearsInBusiness__": "YearsInBusiness__",
															"TaxStatus__": "LabelTaxStatus__",
															"LabelTaxStatus__": "TaxStatus__",
															"CompanyStructure__": "LabelCompanyStructure__",
															"LabelCompanyStructure__": "CompanyStructure__",
															"PaymentMethod__": "LabelPaymentMethod__",
															"LabelPaymentMethod__": "PaymentMethod__",
															"VendorInfoDUNSNumber__": "LabelVendorInfoDUNSNumber__",
															"LabelVendorInfoDUNSNumber__": "VendorInfoDUNSNumber__"
														},
														"version": 0
													},
													"*": {
														"Grid": {
															"type": "GridLayout",
															"options": {
																"lines": 17,
																"columns": 2,
																"version": 0,
																"ctrlsPos": {
																	"VendorInfoName__": {
																		"line": 3,
																		"column": 2
																	},
																	"LabelVendorInfoName__": {
																		"line": 3,
																		"column": 1
																	},
																	"VendorInfoVendorNumber__": {
																		"line": 2,
																		"column": 2
																	},
																	"LabelVendorInfoVendorNumber__": {
																		"line": 2,
																		"column": 1
																	},
																	"VendorInfoVATNumber__": {
																		"line": 6,
																		"column": 2
																	},
																	"LabelVendorInfoVATNumber__": {
																		"line": 6,
																		"column": 1
																	},
																	"VendorInfoFaxNumber__": {
																		"line": 5,
																		"column": 2
																	},
																	"LabelVendorInfoFaxNumber__": {
																		"line": 5,
																		"column": 1
																	},
																	"VendorInfoPhoneNumber__": {
																		"line": 4,
																		"column": 2
																	},
																	"LabelVendorInfoPhoneNumber__": {
																		"line": 4,
																		"column": 1
																	},
																	"VendorInfoPreferredInvoiceType__": {
																		"line": 8,
																		"column": 2
																	},
																	"LabelVendorInfoPreferredInvoiceType__": {
																		"line": 8,
																		"column": 1
																	},
																	"VendorInfoCompanyCode__": {
																		"line": 1,
																		"column": 2
																	},
																	"LabelVendorInfoCompanyCode__": {
																		"line": 1,
																		"column": 1
																	},
																	"VendorInfoEmail__": {
																		"line": 13,
																		"column": 2
																	},
																	"LabelVendorInfoEmail__": {
																		"line": 13,
																		"column": 1
																	},
																	"VendorInfoPaymentTermCode__": {
																		"line": 10,
																		"column": 2
																	},
																	"LabelVendorInfoPaymentTermCode__": {
																		"line": 10,
																		"column": 1
																	},
																	"VendorInfoCurrency__": {
																		"line": 14,
																		"column": 2
																	},
																	"LabelVendorInfoCurrency__": {
																		"line": 14,
																		"column": 1
																	},
																	"Website__": {
																		"line": 17,
																		"column": 2
																	},
																	"LabelWebsite__": {
																		"line": 17,
																		"column": 1
																	},
																	"NumberOfEmployees__": {
																		"line": 15,
																		"column": 2
																	},
																	"LabelNumberOfEmployees__": {
																		"line": 15,
																		"column": 1
																	},
																	"YearsInBusiness__": {
																		"line": 16,
																		"column": 2
																	},
																	"LabelYearsInBusiness__": {
																		"line": 16,
																		"column": 1
																	},
																	"TaxStatus__": {
																		"line": 12,
																		"column": 2
																	},
																	"LabelTaxStatus__": {
																		"line": 12,
																		"column": 1
																	},
																	"CompanyStructure__": {
																		"line": 9,
																		"column": 2
																	},
																	"LabelCompanyStructure__": {
																		"line": 9,
																		"column": 1
																	},
																	"PaymentMethod__": {
																		"line": 11,
																		"column": 2
																	},
																	"LabelPaymentMethod__": {
																		"line": 11,
																		"column": 1
																	},
																	"VendorInfoDUNSNumber__": {
																		"line": 7,
																		"column": 2
																	},
																	"LabelVendorInfoDUNSNumber__": {
																		"line": 7,
																		"column": 1
																	}
																},
																"colspans": [
																	[],
																	[],
																	[],
																	[],
																	[],
																	[],
																	[],
																	[],
																	[],
																	[],
																	[],
																	[],
																	[],
																	[],
																	[],
																	[],
																	[]
																]
															},
															"data": [
																"fields"
															],
															"*": {
																"LabelVendorInfoCompanyCode__": {
																	"type": "Label",
																	"data": [],
																	"options": {
																		"label": "Company code",
																		"version": 0
																	},
																	"stamp": 108
																},
																"VendorInfoCompanyCode__": {
																	"type": "ShortText",
																	"data": [],
																	"options": {
																		"label": "Company code",
																		"activable": true,
																		"width": "100%",
																		"length": 50,
																		"version": 0,
																		"textSize": "S",
																		"textAlignment": "left",
																		"textStyle": "default",
																		"textColor": "default",
																		"readonly": true,
																		"notInDB": true,
																		"dataType": "String"
																	},
																	"stamp": 109
																},
																"LabelVendorInfoVendorNumber__": {
																	"type": "Label",
																	"data": [],
																	"options": {
																		"label": "Number",
																		"version": 0
																	},
																	"stamp": 110
																},
																"VendorInfoVendorNumber__": {
																	"type": "ShortText",
																	"data": [],
																	"options": {
																		"label": "Number",
																		"activable": true,
																		"width": "100%",
																		"length": 50,
																		"version": 0,
																		"textSize": "S",
																		"textAlignment": "left",
																		"textStyle": "default",
																		"textColor": "default",
																		"readonly": true,
																		"notInDB": true,
																		"dataType": "String"
																	},
																	"stamp": 111
																},
																"LabelVendorInfoName__": {
																	"type": "Label",
																	"data": [],
																	"options": {
																		"label": "Name",
																		"version": 0
																	},
																	"stamp": 112
																},
																"VendorInfoName__": {
																	"type": "ShortText",
																	"data": [],
																	"options": {
																		"label": "Name",
																		"activable": true,
																		"width": "100%",
																		"length": 35,
																		"version": 0,
																		"textSize": "S",
																		"textAlignment": "left",
																		"textStyle": "default",
																		"textColor": "default",
																		"readonly": true,
																		"notInDB": true,
																		"dataType": "String"
																	},
																	"stamp": 113
																},
																"LabelVendorInfoPhoneNumber__": {
																	"type": "Label",
																	"data": [],
																	"options": {
																		"label": "Phone number",
																		"version": 0
																	},
																	"stamp": 114
																},
																"VendorInfoPhoneNumber__": {
																	"type": "ShortText",
																	"data": [],
																	"options": {
																		"label": "Phone number",
																		"activable": true,
																		"width": "100%",
																		"length": 32,
																		"version": 0,
																		"textSize": "S",
																		"textAlignment": "left",
																		"textStyle": "default",
																		"textColor": "default",
																		"readonly": true,
																		"notInDB": true,
																		"dataType": "String"
																	},
																	"stamp": 115
																},
																"LabelVendorInfoFaxNumber__": {
																	"type": "Label",
																	"data": [],
																	"options": {
																		"label": "Fax number",
																		"version": 0
																	},
																	"stamp": 116
																},
																"VendorInfoFaxNumber__": {
																	"type": "ShortText",
																	"data": [],
																	"options": {
																		"label": "Fax number",
																		"activable": true,
																		"width": "100%",
																		"length": 32,
																		"version": 0,
																		"textSize": "S",
																		"textAlignment": "left",
																		"textStyle": "default",
																		"textColor": "default",
																		"readonly": true,
																		"notInDB": true,
																		"dataType": "String"
																	},
																	"stamp": 117
																},
																"LabelVendorInfoVATNumber__": {
																	"type": "Label",
																	"data": [],
																	"options": {
																		"label": "VAT number",
																		"version": 0
																	},
																	"stamp": 118
																},
																"VendorInfoVATNumber__": {
																	"type": "ShortText",
																	"data": [],
																	"options": {
																		"label": "VAT number",
																		"activable": true,
																		"width": "100%",
																		"length": 20,
																		"version": 0,
																		"textSize": "S",
																		"textAlignment": "left",
																		"textStyle": "default",
																		"textColor": "default",
																		"readonly": true,
																		"notInDB": true,
																		"dataType": "String"
																	},
																	"stamp": 119
																},
																"LabelVendorInfoDUNSNumber__": {
																	"type": "Label",
																	"data": [],
																	"options": {
																		"label": "_DUNSNumber",
																		"version": 0
																	},
																	"stamp": 120
																},
																"VendorInfoDUNSNumber__": {
																	"type": "ShortText",
																	"data": [],
																	"options": {
																		"textSize": "S",
																		"textAlignment": "left",
																		"textStyle": "default",
																		"textColor": "default",
																		"version": 1,
																		"label": "_DUNSNumber",
																		"activable": true,
																		"width": "100%",
																		"helpText": "",
																		"helpURL": "",
																		"helpFormat": "HTML Format",
																		"helpIconPosition": "Right",
																		"length": 9,
																		"notInDB": true,
																		"dataType": "String",
																		"browsable": false,
																		"readonly": true
																	},
																	"stamp": 121
																},
																"LabelVendorInfoPreferredInvoiceType__": {
																	"type": "Label",
																	"data": [],
																	"options": {
																		"label": "Preferred invoice type",
																		"version": 0
																	},
																	"stamp": 122
																},
																"VendorInfoPreferredInvoiceType__": {
																	"type": "ShortText",
																	"data": [],
																	"options": {
																		"label": "Preferred invoice type",
																		"activable": true,
																		"width": "100%",
																		"version": 0,
																		"textSize": "S",
																		"textAlignment": "left",
																		"textStyle": "default",
																		"textColor": "default",
																		"readonly": true,
																		"notInDB": true,
																		"dataType": "String"
																	},
																	"stamp": 123
																},
																"LabelCompanyStructure__": {
																	"type": "Label",
																	"data": [
																		"CompanyStructure__"
																	],
																	"options": {
																		"label": "_CompanyStructure",
																		"version": 0
																	},
																	"stamp": 124
																},
																"CompanyStructure__": {
																	"type": "ComboBox",
																	"data": [
																		"CompanyStructure__"
																	],
																	"options": {
																		"possibleValues": {
																			"0": "",
																			"1": "_Corporation",
																			"2": "_Partnership",
																			"3": "_Proprietorship"
																		},
																		"version": 1,
																		"keyValueMode": true,
																		"possibleKeys": {
																			"0": "",
																			"1": "_Corporation",
																			"2": "_Partnership",
																			"3": "_Proprietorship"
																		},
																		"label": "_CompanyStructure",
																		"activable": true,
																		"width": 230,
																		"helpText": "",
																		"helpURL": "",
																		"helpFormat": "HTML Format",
																		"DataSourceOrigin": "Standalone",
																		"DisplayedColumns": "",
																		"readonly": true,
																		"maxNbLinesToDisplay": 10
																	},
																	"stamp": 125
																},
																"LabelVendorInfoPaymentTermCode__": {
																	"type": "Label",
																	"data": [],
																	"options": {
																		"label": "Payment terms",
																		"version": 0
																	},
																	"stamp": 126
																},
																"VendorInfoPaymentTermCode__": {
																	"type": "ShortText",
																	"data": [],
																	"options": {
																		"label": "Payment terms",
																		"activable": true,
																		"width": "100%",
																		"length": 50,
																		"version": 0,
																		"textSize": "S",
																		"textAlignment": "left",
																		"textStyle": "default",
																		"textColor": "default",
																		"readonly": true,
																		"notInDB": true,
																		"dataType": "String"
																	},
																	"stamp": 127
																},
																"LabelPaymentMethod__": {
																	"type": "Label",
																	"data": [
																		"PaymentMethod__"
																	],
																	"options": {
																		"label": "_PaymentMethod",
																		"version": 0
																	},
																	"stamp": 128
																},
																"PaymentMethod__": {
																	"type": "ShortText",
																	"data": [
																		"PaymentMethod__"
																	],
																	"options": {
																		"textSize": "S",
																		"textAlignment": "left",
																		"textStyle": "default",
																		"textColor": "default",
																		"version": 1,
																		"label": "_PaymentMethod",
																		"activable": true,
																		"width": 230,
																		"helpText": "",
																		"helpURL": "",
																		"helpFormat": "HTML Format",
																		"browsable": false,
																		"readonly": true
																	},
																	"stamp": 129
																},
																"LabelTaxStatus__": {
																	"type": "Label",
																	"data": [
																		"TaxStatus__"
																	],
																	"options": {
																		"label": "_TaxStatus",
																		"version": 0
																	},
																	"stamp": 130
																},
																"TaxStatus__": {
																	"type": "ComboBox",
																	"data": [
																		"TaxStatus__"
																	],
																	"options": {
																		"possibleValues": {
																			"0": "",
																			"1": "_taxable",
																			"2": "_exempt",
																			"3": "_Other"
																		},
																		"version": 1,
																		"keyValueMode": true,
																		"possibleKeys": {
																			"0": "",
																			"1": "_taxable",
																			"2": "_exempt",
																			"3": "_Other"
																		},
																		"label": "_TaxStatus",
																		"activable": true,
																		"width": 230,
																		"helpText": "",
																		"helpURL": "",
																		"helpFormat": "HTML Format",
																		"DataSourceOrigin": "Standalone",
																		"DisplayedColumns": "",
																		"readonly": true,
																		"maxNbLinesToDisplay": 10
																	},
																	"stamp": 131
																},
																"LabelVendorInfoEmail__": {
																	"type": "Label",
																	"data": [],
																	"options": {
																		"label": "Email",
																		"version": 0
																	},
																	"stamp": 132
																},
																"VendorInfoEmail__": {
																	"type": "ShortText",
																	"data": [],
																	"options": {
																		"label": "Email",
																		"activable": true,
																		"width": "100%",
																		"version": 0,
																		"textSize": "S",
																		"textAlignment": "left",
																		"textStyle": "default",
																		"textColor": "default",
																		"readonly": true,
																		"notInDB": true,
																		"dataType": "String"
																	},
																	"stamp": 133
																},
																"LabelVendorInfoCurrency__": {
																	"type": "Label",
																	"data": [],
																	"options": {
																		"label": "Currency",
																		"version": 0
																	},
																	"stamp": 134
																},
																"VendorInfoCurrency__": {
																	"type": "ShortText",
																	"data": [],
																	"options": {
																		"textSize": "S",
																		"textAlignment": "left",
																		"textStyle": "default",
																		"textColor": "default",
																		"label": "Currency",
																		"activable": true,
																		"width": "100%",
																		"length": 5,
																		"browsable": false,
																		"version": 0,
																		"readonly": true,
																		"notInDB": true,
																		"dataType": "String"
																	},
																	"stamp": 135
																},
																"LabelWebsite__": {
																	"type": "Label",
																	"data": [
																		"Website__"
																	],
																	"options": {
																		"label": "_Website",
																		"version": 0
																	},
																	"stamp": 136
																},
																"Website__": {
																	"type": "ShortText",
																	"data": [
																		"Website__"
																	],
																	"options": {
																		"textSize": "S",
																		"textAlignment": "left",
																		"textStyle": "default",
																		"textColor": "default",
																		"version": 1,
																		"label": "_Website",
																		"activable": true,
																		"width": "100%",
																		"helpText": "",
																		"helpURL": "",
																		"helpFormat": "HTML Format",
																		"browsable": false,
																		"readonly": true
																	},
																	"stamp": 137
																},
																"LabelYearsInBusiness__": {
																	"type": "Label",
																	"data": [
																		"YearsInBusiness__"
																	],
																	"options": {
																		"label": "_YearsInBusiness",
																		"version": 0
																	},
																	"stamp": 138
																},
																"YearsInBusiness__": {
																	"type": "Integer",
																	"data": [
																		"YearsInBusiness__"
																	],
																	"options": {
																		"version": 2,
																		"textSize": "S",
																		"textAlignment": "left",
																		"textStyle": "default",
																		"textColor": "default",
																		"integer": true,
																		"label": "_YearsInBusiness",
																		"precision_internal": 0,
																		"precision_current": 0,
																		"activable": true,
																		"width": "100%",
																		"helpText": "",
																		"helpURL": "",
																		"helpFormat": "HTML Format",
																		"browsable": false,
																		"readonly": true
																	},
																	"stamp": 139
																},
																"LabelNumberOfEmployees__": {
																	"type": "Label",
																	"data": [
																		"NumberOfEmployees__"
																	],
																	"options": {
																		"label": "_NumberOfEmployees",
																		"version": 0
																	},
																	"stamp": 140
																},
																"NumberOfEmployees__": {
																	"type": "Integer",
																	"data": [
																		"NumberOfEmployees__"
																	],
																	"options": {
																		"version": 2,
																		"textSize": "S",
																		"textAlignment": "left",
																		"textStyle": "default",
																		"textColor": "default",
																		"integer": true,
																		"label": "_NumberOfEmployees",
																		"precision_internal": 0,
																		"precision_current": 0,
																		"activable": true,
																		"width": "100%",
																		"helpText": "",
																		"helpURL": "",
																		"helpFormat": "HTML Format",
																		"browsable": false,
																		"readonly": true
																	},
																	"stamp": 141
																}
															},
															"stamp": 142
														}
													},
													"stamp": 143,
													"data": []
												}
											}
										},
										"VendorAddress": {
											"type": "PanelData",
											"data": [],
											"options": {
												"border": {},
												"labelLength": 0,
												"iconURL": "",
												"TitleFontSize": "M",
												"hideActionButtons": true,
												"hideTitleBar": false,
												"hideTitle": false,
												"backgroundcolor": "default",
												"labelsAlignment": "right",
												"label": "_VendorAddress",
												"hidden": false,
												"leftImageURL": "",
												"removeMargins": false,
												"elementsAlignment": "left",
												"panelStyle": "FlexibleFormPanelLight",
												"version": 0,
												"sameHeightAsSiblings": false
											},
											"stamp": 144,
											"*": {
												"FieldsManager": {
													"type": "FieldsManager",
													"data": [],
													"options": {
														"controlsAssociation": {
															"LabelStreet__": "Street__",
															"Street__": "LabelStreet__",
															"LabelPostOfficeBox__": "PostOfficeBox__",
															"PostOfficeBox__": "LabelPostOfficeBox__",
															"LabelCity__": "City__",
															"City__": "LabelCity__",
															"LabelPostalCode__": "PostalCode__",
															"PostalCode__": "LabelPostalCode__",
															"LabelRegion__": "Region__",
															"Region__": "LabelRegion__",
															"LabelCountry__": "Country__",
															"Country__": "LabelCountry__",
															"VendorAddress__": "LabelVendorAddress__",
															"LabelVendorAddress__": "VendorAddress__",
															"Sub__": "LabelSub__",
															"LabelSub__": "Sub__"
														},
														"version": 0
													},
													"stamp": 145,
													"*": {
														"Grid": {
															"type": "GridLayout",
															"data": [
																"fields"
															],
															"options": {
																"ctrlsPos": {
																	"Sub__": {
																		"line": 1,
																		"column": 2
																	},
																	"LabelSub__": {
																		"line": 1,
																		"column": 1
																	},
																	"LabelStreet__": {
																		"line": 2,
																		"column": 1
																	},
																	"Street__": {
																		"line": 2,
																		"column": 2
																	},
																	"LabelPostOfficeBox__": {
																		"line": 3,
																		"column": 1
																	},
																	"PostOfficeBox__": {
																		"line": 3,
																		"column": 2
																	},
																	"LabelCity__": {
																		"line": 4,
																		"column": 1
																	},
																	"City__": {
																		"line": 4,
																		"column": 2
																	},
																	"LabelPostalCode__": {
																		"line": 5,
																		"column": 1
																	},
																	"PostalCode__": {
																		"line": 5,
																		"column": 2
																	},
																	"LabelRegion__": {
																		"line": 6,
																		"column": 1
																	},
																	"Region__": {
																		"line": 6,
																		"column": 2
																	},
																	"LabelCountry__": {
																		"line": 7,
																		"column": 1
																	},
																	"Country__": {
																		"line": 7,
																		"column": 2
																	},
																	"VendorAddress__": {
																		"line": 8,
																		"column": 2
																	},
																	"LabelVendorAddress__": {
																		"line": 8,
																		"column": 1
																	}
																},
																"lines": 8,
																"columns": 2,
																"colspans": [
																	[],
																	[],
																	[],
																	[],
																	[],
																	[],
																	[],
																	[]
																],
																"version": 0
															},
															"stamp": 146,
															"*": {
																"Sub__": {
																	"type": "ShortText",
																	"data": [],
																	"options": {
																		"label": "_Sub",
																		"activable": true,
																		"width": "100%",
																		"length": 3,
																		"version": 0,
																		"textSize": "S",
																		"textAlignment": "left",
																		"textStyle": "default",
																		"textColor": "default",
																		"readonly": true,
																		"notInDB": true,
																		"dataType": "String"
																	},
																	"stamp": 147
																},
																"LabelSub__": {
																	"type": "Label",
																	"data": [],
																	"options": {
																		"label": "_Sub",
																		"version": 0
																	},
																	"stamp": 148
																},
																"Country__": {
																	"type": "ShortText",
																	"data": [],
																	"options": {
																		"label": "Country",
																		"activable": true,
																		"width": "100%",
																		"length": 3,
																		"version": 0,
																		"textSize": "S",
																		"textAlignment": "left",
																		"textStyle": "default",
																		"textColor": "default",
																		"readonly": true,
																		"notInDB": true,
																		"dataType": "String"
																	},
																	"stamp": 149
																},
																"LabelCountry__": {
																	"type": "Label",
																	"data": [],
																	"options": {
																		"label": "Country",
																		"version": 0
																	},
																	"stamp": 150
																},
																"Region__": {
																	"type": "ShortText",
																	"data": [],
																	"options": {
																		"label": "Region",
																		"activable": true,
																		"width": "100%",
																		"length": 3,
																		"version": 0,
																		"textSize": "S",
																		"textAlignment": "left",
																		"textStyle": "default",
																		"textColor": "default",
																		"readonly": true,
																		"notInDB": true,
																		"dataType": "String"
																	},
																	"stamp": 151
																},
																"LabelRegion__": {
																	"type": "Label",
																	"data": [],
																	"options": {
																		"label": "Region",
																		"version": 0
																	},
																	"stamp": 152
																},
																"PostalCode__": {
																	"type": "ShortText",
																	"data": [],
																	"options": {
																		"label": "Postal code",
																		"activable": true,
																		"width": "100%",
																		"length": 10,
																		"version": 0,
																		"textSize": "S",
																		"textAlignment": "left",
																		"textStyle": "default",
																		"textColor": "default",
																		"readonly": true,
																		"notInDB": true,
																		"dataType": "String"
																	},
																	"stamp": 153
																},
																"LabelPostalCode__": {
																	"type": "Label",
																	"data": [],
																	"options": {
																		"label": "Postal code",
																		"version": 0
																	},
																	"stamp": 154
																},
																"City__": {
																	"type": "ShortText",
																	"data": [],
																	"options": {
																		"label": "City",
																		"activable": true,
																		"width": "100%",
																		"length": 35,
																		"version": 0,
																		"textSize": "S",
																		"textAlignment": "left",
																		"textStyle": "default",
																		"textColor": "default",
																		"readonly": true,
																		"notInDB": true,
																		"dataType": "String"
																	},
																	"stamp": 155
																},
																"LabelCity__": {
																	"type": "Label",
																	"data": [],
																	"options": {
																		"label": "City",
																		"version": 0
																	},
																	"stamp": 156
																},
																"PostOfficeBox__": {
																	"type": "ShortText",
																	"data": [],
																	"options": {
																		"textSize": "S",
																		"textAlignment": "left",
																		"textStyle": "default",
																		"textColor": "default",
																		"label": "PostOfficeBox",
																		"activable": true,
																		"width": "100%",
																		"helpText": "",
																		"helpURL": "",
																		"helpFormat": "HTML Format",
																		"browsable": false,
																		"version": 0,
																		"length": 20,
																		"readonly": true,
																		"notInDB": true,
																		"dataType": "String"
																	},
																	"stamp": 157
																},
																"LabelPostOfficeBox__": {
																	"type": "Label",
																	"data": [],
																	"options": {
																		"label": "PostOfficeBox",
																		"version": 0
																	},
																	"stamp": 158
																},
																"Street__": {
																	"type": "ShortText",
																	"data": [],
																	"options": {
																		"label": "Street",
																		"activable": true,
																		"width": "100%",
																		"length": 50,
																		"version": 0,
																		"textSize": "S",
																		"textAlignment": "left",
																		"textStyle": "default",
																		"textColor": "default",
																		"readonly": true,
																		"notInDB": true,
																		"dataType": "String"
																	},
																	"stamp": 159
																},
																"LabelStreet__": {
																	"type": "Label",
																	"data": [],
																	"options": {
																		"label": "Street",
																		"version": 0
																	},
																	"stamp": 160
																},
																"LabelVendorAddress__": {
																	"type": "Label",
																	"data": [],
																	"options": {
																		"label": "_Vendor Address",
																		"version": 0
																	},
																	"stamp": 161
																},
																"VendorAddress__": {
																	"type": "LongText",
																	"data": [],
																	"options": {
																		"textSize": "S",
																		"textAlignment": "left",
																		"textStyle": "default",
																		"textColor": "default",
																		"version": 1,
																		"minNbLines": 3,
																		"label": "_Vendor Address",
																		"activable": true,
																		"width": "100%",
																		"helpText": "",
																		"helpURL": "",
																		"helpFormat": "HTML Format",
																		"numberOfLines": 10,
																		"readonly": true,
																		"notInDB": true,
																		"dataType": "String",
																		"browsable": false
																	},
																	"stamp": 162
																}
															}
														}
													}
												}
											}
										}
									}
								},
								"form-content-right-12": {
									"type": "FlexibleFormLine",
									"data": [],
									"options": {
										"additionalCssStyle": "flexParent"
									},
									"stamp": 179,
									"*": {
										"ReportsPanel": {
											"type": "PanelData",
											"data": [],
											"options": {
												"border": {},
												"iconURL": "",
												"flexPanel": true,
												"TitleFontSize": "M",
												"hideActionButtons": true,
												"hideTitleBar": true,
												"hideTitle": true,
												"sameHeightAsSiblings": false,
												"backgroundcolor": "default",
												"labelsAlignment": "right",
												"label": "_ReportsPanel",
												"hidden": false,
												"leftImageURL": "",
												"removeMargins": false,
												"panelStyle": "FlexibleFormPanelLight flexPanel",
												"elementsAlignment": "left"
											},
											"stamp": 174,
											"*": {
												"FieldsManager": {
													"type": "FieldsManager",
													"data": [],
													"options": {
														"controlsAssociation": {}
													},
													"stamp": 175,
													"*": {
														"Grid": {
															"type": "HorizontalLayout",
															"data": [
																"fields"
															],
															"options": {
																"ctrlsPos": {
																	"ReportDiscountForecast__": {
																		"line": 1,
																		"column": 1
																	},
																	"ReportInvoicingHistory__": {
																		"line": 2,
																		"column": 1
																	}
																},
																"lines": 2,
																"colspans": [
																	[
																		{
																			"index": 0,
																			"length": 2
																		}
																	],
																	[
																		{
																			"index": 0,
																			"length": 2
																		}
																	]
																]
															},
															"stamp": 176,
															"*": {
																"ReportDiscountForecast__": {
																	"type": "Report",
																	"data": false,
																	"options": {
																		"label": "_ReportDiscountForecast",
																		"width": 230,
																		"version": 0,
																		"reportWidth": "Medium",
																		"reportHeight": "Medium",
																		"reportName": "_ReportName_VIP_AP_DiscountForecast",
																		"reportTitle": "_DashReport_AP_DiscountForecast",
																		"delayLoad": true
																	},
																	"stamp": 182
																},
																"ReportInvoicingHistory__": {
																	"type": "Report",
																	"data": false,
																	"options": {
																		"label": "_ReportInvoicingHistory",
																		"width": 230,
																		"version": 0,
																		"reportWidth": "ExtraLarge",
																		"reportHeight": "Medium",
																		"reportName": "",
																		"reportTitle": "_DashReport_AP_InvoicingHistory",
																		"delayLoad": true
																	},
																	"stamp": 183
																}
															}
														}
													}
												}
											}
										}
									}
								},
								"form-content-right-14": {
									"type": "FlexibleFormLine",
									"data": [],
									"options": {
										"additionalCssStyle": "flexParent"
									},
									"stamp": 193,
									"*": {
										"ScoringReportsPanel": {
											"type": "PanelData",
											"data": [],
											"options": {
												"border": {},
												"iconURL": "",
												"TitleFontSize": "M",
												"hideActionButtons": true,
												"hideTitleBar": true,
												"hideTitle": true,
												"sameHeightAsSiblings": false,
												"backgroundcolor": "default",
												"labelsAlignment": "right",
												"label": "_ScoringReportsPanel",
												"hidden": false,
												"leftImageURL": "",
												"removeMargins": false,
												"panelStyle": "FlexibleFormPanelLight flexPanel",
												"elementsAlignment": "left"
											},
											"stamp": 185,
											"*": {
												"FieldsManager": {
													"type": "FieldsManager",
													"data": [],
													"options": {
														"controlsAssociation": {}
													},
													"stamp": 186,
													"*": {
														"Grid": {
															"type": "HorizontalLayout",
															"data": [
																"fields"
															],
															"options": {
																"ctrlsPos": {
																	"ReportSupplierScoring__": {
																		"line": 1,
																		"column": 1
																	}
																},
																"lines": 1,
																"columns": 2,
																"colspans": [
																	[
																		{
																			"index": 0,
																			"length": 2
																		}
																	]
																]
															},
															"stamp": 187,
															"*": {
																"ReportSupplierScoring__": {
																	"type": "Report",
																	"data": false,
																	"options": {
																		"label": "_ReportSupplierScoring",
																		"width": 230,
																		"version": 0,
																		"reportWidth": "Large",
																		"reportHeight": "Large",
																		"reportName": "",
																		"reportTitle": "_DashReport_AP_SupplierScoring",
																		"delayLoad": false
																	},
																	"stamp": 194
																}
															}
														}
													}
												}
											}
										}
									}
								}
							},
							"stamp": 163,
							"data": []
						}
					},
					"stamp": 164,
					"data": []
				},
				"form-footer": {
					"type": "FlexibleFormFooter",
					"options": {
						"splitterType": "simple-h",
						"version": 0
					},
					"*": {
						"Save": {
							"type": "SubmitButton",
							"options": {
								"label": "_Save",
								"action": "save",
								"submit": true,
								"version": 0,
								"shortCut": "S"
							},
							"stamp": 165,
							"data": []
						},
						"Close": {
							"type": "SubmitButton",
							"options": {
								"label": "_Close",
								"action": "cancel",
								"submit": true,
								"version": 0,
								"shortCut": "Q"
							},
							"stamp": 166,
							"data": []
						},
						"Delete": {
							"type": "SubmitButton",
							"options": {
								"label": "_Delete",
								"action": "delete",
								"submit": true,
								"version": 0,
								"disabled": true
							},
							"stamp": 167,
							"data": []
						}
					},
					"stamp": 168,
					"data": []
				}
			},
			"stamp": 169,
			"data": []
		}
	},
	"stamps": 194,
	"data": []
}