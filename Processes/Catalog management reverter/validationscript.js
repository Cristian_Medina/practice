///#GLOBALS Lib Sys
////////////////
/// Entry point
////////////////
var validationscript;
(function (validationscript) {
    var IsInternalUpdateRequest = Lib.Purchasing.CM.Workflow.IsInternalUpdateRequest();
    var currentName = Data.GetActionName();
    var currentAction = Data.GetActionType();
    Log.Info("-- CM reverter validation Script -- Name: '" + (currentName ? currentName : "<empty>") + "', Action: '" + (currentAction ? currentAction : "<empty>") + "'");
    // First validation
    if (currentName === "" && currentAction === "") {
        if (Data.GetValue("State") == 50) {
            Log.Info("Validation in touchless");
            currentName = "Approve";
            currentAction = "approve";
        }
    }
    if (currentName === "Approve" && currentAction === "approve") {
        if (!Lib.Purchasing.CM.ProcessedData(Lib.Purchasing.CM.RevertLines)) {
            Lib.CommonDialog.NextAlert.Define("_CM revert error", "_CM revert error message", {
                isError: true,
                behaviorName: "CMRevertFailure"
            });
        }
        else {
            Lib.CommonDialog.NextAlert.Define("_CM revert success", "_CM revert success message", {
                isError: false,
                behaviorName: "CMRevertSuccess"
            });
            var transport = Process.GetUpdatableTransportAsProcessAdmin(Data.GetValue("CMWRuidEx__"));
            MarkAsReverted(transport);
            if (!IsInternalUpdateRequest) {
                transport = Process.GetUpdatableTransportAsProcessAdmin(Data.GetValue("CMRuidEx__"));
                MarkAsReverted(transport);
            }
        }
    }
    function MarkAsReverted(transport) {
        var vars = transport.GetUninheritedVars();
        vars.AddValue_String("Status__", "Reverted", true);
        transport.Process();
        if (transport.GetLastError() !== 0) {
            throw new Error("Cannot process CM. Details: " + transport.GetLastErrorMessage());
        }
    }
})(validationscript || (validationscript = {}));
