///#GLOBALS Lib
/* Purchase requisition extraction script */
var extractionscript;
(function (extractionscript) {
    var currentName = Data.GetActionName(), currentAction = Data.GetActionType(), companyCode = Data.GetValue("CompanyCode__");
    Log.Info("Clear WorkflowData");
    Variable.SetValueAsString("wkfdata", "");
    Log.Info("-- PR Extraction Script -- Name: '" + (currentName ? currentName : "<empty>") + "', Action: '" + (currentAction ? currentAction : "<empty>") + "'");
    function SetFatalError(status) {
        Variable.SetValueAsString("Extraction_status__", status);
        throw { fatalError: true };
    }
    function OnInboundEmail() {
        // Process created from an inbound email ?
        if (Data.GetValue("SourceRUID").toUpperCase().indexOf("ISM.") === 0) {
            var fromAdress = Data.GetValue("FromAddress");
            Log.Info("OnInboundEmail: try to forward this process to the quote by email sender: " + fromAdress);
            var user = Users.GetUser(fromAdress), forwarded = user && Process.Forward(fromAdress); //Check if user exists and is accessible
            if (!forwarded) {
                Log.Error("OnInboundEmail: Process forwarding to " + fromAdress + " failed");
                var defaultRequester = Sys.Parameters.GetInstance("PAC").GetParameter("DefaultQuoteByEmailRequester");
                if (defaultRequester) {
                    user = Lib.P2P.GetOwner();
                    defaultRequester = Lib.P2P.ResolveDemoLogin(defaultRequester);
                    Log.Info("OnInboundEmail: try to forward this process to the default quote by email requester: " + defaultRequester);
                    forwarded = Process.Forward(defaultRequester);
                    if (!forwarded) {
                        Log.Error("OnInboundEmail: Process forwarding to " + defaultRequester + " failed");
                    }
                    else {
                        user = Users.GetUser(defaultRequester);
                    }
                }
                else {
                    Log.Info("OnInboundEmail: No default quote by email requester defined");
                }
            }
            if (forwarded) {
                Log.Info("OnInboundEmail: Process forwarding succeeded");
                Log.Info("OnInboundEmail: Schedule a notification by email");
                Variable.SetValueAsString("NotifyQuoteByEmailRequester__", "1");
                var vars = user.GetVars(), fullName = vars.GetValue_String("DisplayName", 0);
                Data.SetValue("RequisitionInitiator__", vars.GetValue_String("Login", 0));
                Data.SetValue("RequesterName__", fullName);
                var originatingUserLogin = vars.GetValue_String("Login", 0);
                Log.Info("Grant read right to originating user: " + originatingUserLogin);
                Process.SetRight(originatingUserLogin, "read");
            }
            else {
                SetFatalError(Language.Translate("_Unable to forward this purchase requisition following receipt of the quotation"));
            }
        }
        else {
            Log.Info("OnInboundEmail: no treatment to do");
        }
    }
    /** ****************** **/
    /** Vendor recignition **/
    /** ****************** **/
    function GetDocumentCulture() {
        var documentCulture = Variable.GetValueAsString("DocumentCulture");
        if (!documentCulture) {
            documentCulture = Sys.Parameters.GetInstance("PAC").GetParameter("AvailableDocumentCultures");
            if (!documentCulture) {
                documentCulture = ["en-US", "en-GB", "fr-FR"];
            }
        }
        if (typeof documentCulture === "string") {
            documentCulture = documentCulture.split(",");
        }
        return documentCulture;
    }
    var Vendor = {
        Fill: function (queryResult) {
            if (queryResult) {
                Variable.SetValueAsString("lastExtractedVendorName", queryResult.GetValue_String("Name__", 0));
                Variable.SetValueAsString("lastExtractedVendorNumber", queryResult.GetValue_String("Number__", 0));
                Sys.Helpers.TryCallFunction("Lib.PR.Customization.Server.OnVendorRecognition", queryResult);
            }
        },
        Highlight: function (area) {
            if (area) {
                /*
                 * Can't highlight from field in table / wait for workaround
                let highlightColor_border = 0xFFFFFF,
                    highlightColor_background = 0xFFCC00;
                area.zone.Highlight(true, highlightColor_background, highlightColor_border, "VendorName__");
                area.zone.Highlight(true, highlightColor_background, highlightColor_border, "VendorAddress__");
                */
            }
            else {
                var areaList = Document.GetHighlightedAreaList();
                for (var i = areaList.length - 1; i >= 0; i--) {
                    areaList[i].zone.Highlight(false);
                }
            }
        }
    };
    /** ******** **/
    /** RUN PART **/
    /** ******** **/
    // set some values on record
    Lib.Purchasing.InitTechnicalFields();
    Lib.P2P.InitSAPConfiguration(Lib.ERP.GetERPName(), "PAC");
    Vendor.Highlight();
    Lib.P2P.UserProperties.QueryValues(Lib.P2P.GetOwner().GetValue("Login")).Then(function (UserPropertiesValues) {
        companyCode = companyCode || UserPropertiesValues.CompanyCode__;
    });
    Data.SetValue("CompanyCode__", companyCode);
    Log.Info("Company code=" + companyCode);
    Lib.P2P.FirstTimeRecognition_Vendor.Recognize(GetDocumentCulture(), companyCode, null, null, Vendor.Fill, Vendor.Highlight);
    try {
        if (currentAction !== "reprocess") {
            OnInboundEmail();
            Variable.SetValueAsString("Extraction_status__", "SUCCESS");
        }
    }
    catch (e) {
        if (!("fatalError" in e)) {
            throw e;
        }
    }
    Sys.Helpers.TryCallFunction("Lib.PR.Customization.Server.OnExtractionScriptEnd");
})(extractionscript || (extractionscript = {}));
