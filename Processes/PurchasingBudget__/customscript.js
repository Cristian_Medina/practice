Sys.Helpers.Array.ForEach([
	"Budget__",
	"ToApprove__",
	"Committed__",
	"Ordered__",
	"Received__",
	"InvoicedPO__",
	"InvoicedNonPO__"
], function(field)
{
	if (field in Controls && Controls[field].GetValue() === null)
	{
		Controls[field].SetValue(0);
	}
});

Controls.CompanyCode__.OnChange = function()
{
	Sys.Helpers.Array.ForEach([
		"CostCenter__",
		"Group__",
		"PeriodCode__"
	], function(field){
		Controls[field].SetValue(null);
	});
};

Sys.Parameters.GetInstance("P2P").IsReady(function ()
{
	var hideLowBudgetFields = Sys.Parameters.GetInstance("P2P").GetParameter("EnableLowBudgetNotification") !== "1";
	Controls.WarningThreshold__.Hide(hideLowBudgetFields);
	Controls.OwnerLogin__.Hide(hideLowBudgetFields);
	Controls.NotifyOwner__.Hide(hideLowBudgetFields);
});
