{
	"*": {
		"Screen": {
			"type": "FlexibleFormLayout",
			"options": {
				"splitterType": "simple-v",
				"version": 0,
				"backColor": "text-backgroundcolor-default",
				"style": null
			},
			"*": {
				"form-header": {
					"type": "FlexibleFormHeader",
					"options": {
						"hidePanelsMenu": false,
						"version": 0
					},
					"stamp": 2,
					"data": []
				},
				"form-center": {
					"type": "FlexibleFormCenter",
					"options": {
						"version": 0
					},
					"*": {
						"form-content": {
							"type": "FlexibleFormContent",
							"options": {
								"version": 0
							},
							"*": {
								"form-content-center": {
									"type": "FlexibleFormSplitter",
									"options": {
										"version": 1,
										"box": {
											"top": "0%",
											"left": "0%",
											"width": "100%"
										},
										"hidden": false,
										"hideSeparator": true
									},
									"*": {
										"form-content-center-1": {
											"type": "FlexibleFormLine",
											"data": [],
											"options": {
												"version": 0
											},
											"stamp": 9,
											"*": {
												"DataPanel": {
													"type": "PanelData",
													"options": {
														"border": {},
														"version": 0,
														"labelLength": 0,
														"iconURL": "",
														"TitleFontSize": "S",
														"hideActionButtons": false,
														"hideTitleBar": false,
														"label": "_Document data",
														"backgroundcolor": "default",
														"labelsAlignment": "right",
														"hideTitle": false,
														"sameHeightAsSiblings": false
													},
													"*": {
														"FieldsManager": {
															"type": "FieldsManager",
															"options": {
																"controlsAssociation": {
																	"Budget__": "LabelBudget__",
																	"LabelBudget__": "Budget__",
																	"Committed__": "LabelCommitted__",
																	"LabelCommitted__": "Committed__",
																	"ToApprove__": "LabelToApprove__",
																	"LabelToApprove__": "ToApprove__",
																	"CompanyCode__": "CompanyCode___label__",
																	"CompanyCode___label__": "CompanyCode__",
																	"Ordered__": "LabelOrdered__",
																	"LabelOrdered__": "Ordered__",
																	"Received__": "LabelReceived__",
																	"LabelReceived__": "Received__",
																	"PeriodCode__": "LabelPeriod_code__",
																	"LabelPeriod_code__": "PeriodCode__",
																	"InvoicedNonPO__": "LabelInvoicedNonPO__",
																	"LabelInvoicedNonPO__": "InvoicedNonPO__",
																	"InvoicedPO__": "LabelInvoicedPO__",
																	"LabelInvoicedPO__": "InvoicedPO__",
																	"CostCenter__": "LabelCostCenter__",
																	"LabelCostCenter__": "CostCenter__",
																	"Group__": "LabelGroup__",
																	"LabelGroup__": "Group__",
																	"BudgetID__": "LabelBudgetID__",
																	"LabelBudgetID__": "BudgetID__",
																	"Description__": "LabelDescription__",
																	"LabelDescription__": "Description__",
																	"Closed__": "LabelClosed__",
																	"LabelClosed__": "Closed__",
																	"CostType__": "LabelCostType__",
																	"LabelCostType__": "CostType__",
																	"WarningThreshold__": "LabelWarningThreshold__",
																	"LabelWarningThreshold__": "WarningThreshold__",
																	"OwnerLogin__": "LabelOwnerLogin__",
																	"LabelOwnerLogin__": "OwnerLogin__",
																	"NotifyOwner__": "LabelNotifyOwner__",
																	"LabelNotifyOwner__": "NotifyOwner__"
																},
																"version": 0
															},
															"*": {
																"Grid": {
																	"type": "GridLayout",
																	"options": {
																		"lines": 18,
																		"columns": 2,
																		"version": 0,
																		"ctrlsPos": {
																			"Budget__": {
																				"line": 8,
																				"column": 2
																			},
																			"LabelBudget__": {
																				"line": 8,
																				"column": 1
																			},
																			"Committed__": {
																				"line": 10,
																				"column": 2
																			},
																			"LabelCommitted__": {
																				"line": 10,
																				"column": 1
																			},
																			"ToApprove__": {
																				"line": 9,
																				"column": 2
																			},
																			"LabelToApprove__": {
																				"line": 9,
																				"column": 1
																			},
																			"CompanyCode___label__": {
																				"line": 1,
																				"column": 1
																			},
																			"CompanyCode__": {
																				"line": 1,
																				"column": 2
																			},
																			"Ordered__": {
																				"line": 11,
																				"column": 2
																			},
																			"LabelOrdered__": {
																				"line": 11,
																				"column": 1
																			},
																			"Received__": {
																				"line": 12,
																				"column": 2
																			},
																			"LabelReceived__": {
																				"line": 12,
																				"column": 1
																			},
																			"PeriodCode__": {
																				"line": 4,
																				"column": 2
																			},
																			"LabelPeriod_code__": {
																				"line": 4,
																				"column": 1
																			},
																			"InvoicedNonPO__": {
																				"line": 14,
																				"column": 2
																			},
																			"LabelInvoicedNonPO__": {
																				"line": 14,
																				"column": 1
																			},
																			"InvoicedPO__": {
																				"line": 13,
																				"column": 2
																			},
																			"LabelInvoicedPO__": {
																				"line": 13,
																				"column": 1
																			},
																			"CostCenter__": {
																				"line": 5,
																				"column": 2
																			},
																			"LabelCostCenter__": {
																				"line": 5,
																				"column": 1
																			},
																			"Group__": {
																				"line": 6,
																				"column": 2
																			},
																			"LabelGroup__": {
																				"line": 6,
																				"column": 1
																			},
																			"BudgetID__": {
																				"line": 2,
																				"column": 2
																			},
																			"LabelBudgetID__": {
																				"line": 2,
																				"column": 1
																			},
																			"Description__": {
																				"line": 3,
																				"column": 2
																			},
																			"LabelDescription__": {
																				"line": 3,
																				"column": 1
																			},
																			"Closed__": {
																				"line": 15,
																				"column": 2
																			},
																			"LabelClosed__": {
																				"line": 15,
																				"column": 1
																			},
																			"CostType__": {
																				"line": 7,
																				"column": 2
																			},
																			"LabelCostType__": {
																				"line": 7,
																				"column": 1
																			},
																			"WarningThreshold__": {
																				"line": 17,
																				"column": 2
																			},
																			"LabelWarningThreshold__": {
																				"line": 17,
																				"column": 1
																			},
																			"OwnerLogin__": {
																				"line": 18,
																				"column": 2
																			},
																			"LabelOwnerLogin__": {
																				"line": 18,
																				"column": 1
																			},
																			"NotifyOwner__": {
																				"line": 16,
																				"column": 2
																			},
																			"LabelNotifyOwner__": {
																				"line": 16,
																				"column": 1
																			}
																		},
																		"colspans": [
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[]
																		]
																	},
																	"data": [
																		"fields"
																	],
																	"*": {
																		"CompanyCode___label__": {
																			"type": "Label",
																			"data": [
																				"CompanyCode__"
																			],
																			"options": {
																				"label": "Company code",
																				"version": 0
																			},
																			"stamp": 26
																		},
																		"CompanyCode__": {
																			"type": "DatabaseComboBox",
																			"data": [
																				"CompanyCode__"
																			],
																			"options": {
																				"browsable": true,
																				"label": "_Company code",
																				"activable": true,
																				"width": 230,
																				"fTSmaxRecords": "20",
																				"version": 0,
																				"PreFillFTS": true,
																				"autoCompleteSearchMode": "startswith",
																				"searchMode": "matches"
																			},
																			"stamp": 27
																		},
																		"LabelBudgetID__": {
																			"type": "Label",
																			"data": [
																				"BudgetID__"
																			],
																			"options": {
																				"label": "_BudgetID",
																				"version": 0
																			},
																			"stamp": 46
																		},
																		"BudgetID__": {
																			"type": "ShortText",
																			"data": [
																				"BudgetID__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "_BudgetID",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"browsable": false,
																				"version": 0
																			},
																			"stamp": 47
																		},
																		"LabelDescription__": {
																			"type": "Label",
																			"data": [
																				"Description__"
																			],
																			"options": {
																				"label": "_Description",
																				"version": 0
																			},
																			"stamp": 48
																		},
																		"Description__": {
																			"type": "ShortText",
																			"data": [
																				"Description__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "_Description",
																				"activable": true,
																				"width": 500,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"browsable": false,
																				"version": 0,
																				"length": 120
																			},
																			"stamp": 49
																		},
																		"LabelPeriod_code__": {
																			"type": "Label",
																			"data": [
																				"PeriodCode__"
																			],
																			"options": {
																				"label": "_Period code",
																				"version": 0
																			},
																			"stamp": 34
																		},
																		"PeriodCode__": {
																			"type": "DatabaseComboBox",
																			"data": [
																				"PeriodCode__"
																			],
																			"options": {
																				"browsable": true,
																				"label": "_Period code",
																				"activable": true,
																				"width": 230,
																				"fTSmaxRecords": "20",
																				"PreFillFTS": true,
																				"version": 0,
																				"autoCompleteSearchMode": "startswith",
																				"searchMode": "matches"
																			},
																			"stamp": 35
																		},
																		"LabelCostCenter__": {
																			"type": "Label",
																			"data": [
																				"CostCenter__"
																			],
																			"options": {
																				"label": "_Cost center",
																				"version": 0
																			},
																			"stamp": 40
																		},
																		"CostCenter__": {
																			"type": "DatabaseComboBox",
																			"data": [
																				"CostCenter__"
																			],
																			"options": {
																				"browsable": true,
																				"label": "_Cost center",
																				"activable": true,
																				"width": 230,
																				"PreFillFTS": true,
																				"fTSmaxRecords": "100",
																				"version": 0,
																				"autoCompleteSearchMode": "startswith",
																				"searchMode": "matches"
																			},
																			"stamp": 41
																		},
																		"LabelGroup__": {
																			"type": "Label",
																			"data": [
																				"Group__"
																			],
																			"options": {
																				"label": "_Group",
																				"version": 0
																			},
																			"stamp": 44
																		},
																		"Group__": {
																			"type": "ShortText",
																			"data": [
																				"Group__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "_Group",
																				"activable": true,
																				"width": 230,
																				"browsable": false,
																				"version": 0
																			},
																			"stamp": 45
																		},
																		"LabelCostType__": {
																			"type": "Label",
																			"data": [
																				"CostType__"
																			],
																			"options": {
																				"label": "_CostType",
																				"version": 0
																			},
																			"stamp": 52
																		},
																		"CostType__": {
																			"type": "ComboBox",
																			"data": [
																				"CostType__"
																			],
																			"options": {
																				"possibleValues": {
																					"0": "",
																					"1": "_OpEx",
																					"2": "_CapEx"
																				},
																				"version": 1,
																				"keyValueMode": true,
																				"possibleKeys": {
																					"0": "",
																					"1": "OpEx",
																					"2": "CapEx"
																				},
																				"label": "_CostType",
																				"activable": true,
																				"width": 80,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"DataSourceOrigin": "Standalone",
																				"DisplayedColumns": "",
																				"maxNbLinesToDisplay": 10
																			},
																			"stamp": 53
																		},
																		"LabelBudget__": {
																			"type": "Label",
																			"data": [
																				"Budget__"
																			],
																			"options": {
																				"label": "_Budget",
																				"version": 0
																			},
																			"stamp": 18
																		},
																		"Budget__": {
																			"type": "Decimal",
																			"data": [
																				"Budget__"
																			],
																			"options": {
																				"integer": false,
																				"label": "_Budget",
																				"precision_internal": 2,
																				"activable": true,
																				"width": 230,
																				"version": 0,
																				"textSize": "S",
																				"textAlignment": "right",
																				"textStyle": "default",
																				"textColor": "default",
																				"precision_current": 2,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"defaultValue": "0"
																			},
																			"stamp": 19
																		},
																		"LabelToApprove__": {
																			"type": "Label",
																			"data": [
																				"ToApprove__"
																			],
																			"options": {
																				"label": "_ToApprove",
																				"version": 0
																			},
																			"stamp": 22
																		},
																		"ToApprove__": {
																			"type": "Decimal",
																			"data": [
																				"ToApprove__"
																			],
																			"options": {
																				"integer": false,
																				"label": "_ToApprove",
																				"precision_internal": 2,
																				"activable": true,
																				"width": 230,
																				"version": 0,
																				"textSize": "S",
																				"textAlignment": "right",
																				"textStyle": "default",
																				"textColor": "default",
																				"precision_current": 2,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"defaultValue": "0"
																			},
																			"stamp": 23
																		},
																		"LabelCommitted__": {
																			"type": "Label",
																			"data": [
																				"Committed__"
																			],
																			"options": {
																				"label": "_Committed",
																				"version": 0
																			},
																			"stamp": 20
																		},
																		"Committed__": {
																			"type": "Decimal",
																			"data": [
																				"Committed__"
																			],
																			"options": {
																				"integer": false,
																				"label": "_Committed",
																				"precision_internal": 2,
																				"activable": true,
																				"width": 230,
																				"version": 0,
																				"textSize": "S",
																				"textAlignment": "right",
																				"textStyle": "default",
																				"textColor": "default",
																				"precision_current": 2,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"defaultValue": "0"
																			},
																			"stamp": 21
																		},
																		"LabelOrdered__": {
																			"type": "Label",
																			"data": [
																				"Ordered__"
																			],
																			"options": {
																				"label": "_Ordered",
																				"version": 0
																			},
																			"stamp": 28
																		},
																		"Ordered__": {
																			"type": "Decimal",
																			"data": [
																				"Ordered__"
																			],
																			"options": {
																				"version": 1,
																				"textSize": "S",
																				"textAlignment": "right",
																				"textStyle": "default",
																				"textColor": "default",
																				"integer": false,
																				"label": "_Ordered",
																				"precision_internal": 2,
																				"activable": true,
																				"width": 230,
																				"precision_current": 2,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"defaultValue": "0"
																			},
																			"stamp": 29
																		},
																		"LabelReceived__": {
																			"type": "Label",
																			"data": [
																				"Received__"
																			],
																			"options": {
																				"label": "_Received",
																				"version": 0
																			},
																			"stamp": 30
																		},
																		"Received__": {
																			"type": "Decimal",
																			"data": [
																				"Received__"
																			],
																			"options": {
																				"version": 1,
																				"textSize": "S",
																				"textAlignment": "right",
																				"textStyle": "default",
																				"textColor": "default",
																				"integer": false,
																				"label": "_Received",
																				"precision_internal": 2,
																				"activable": true,
																				"width": 230,
																				"precision_current": 2,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"defaultValue": "0"
																			},
																			"stamp": 31
																		},
																		"LabelInvoicedPO__": {
																			"type": "Label",
																			"data": [
																				"InvoicedPO__"
																			],
																			"options": {
																				"label": "_Invoiced po",
																				"version": 0
																			},
																			"stamp": 38
																		},
																		"InvoicedPO__": {
																			"type": "Decimal",
																			"data": [
																				"InvoicedPO__"
																			],
																			"options": {
																				"version": 1,
																				"textSize": "S",
																				"textAlignment": "right",
																				"textStyle": "default",
																				"textColor": "default",
																				"integer": false,
																				"label": "_Invoiced po",
																				"precision_internal": 2,
																				"activable": true,
																				"width": 230,
																				"autocompletable": false,
																				"precision_current": 2,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"defaultValue": "0"
																			},
																			"stamp": 39
																		},
																		"LabelInvoicedNonPO__": {
																			"type": "Label",
																			"data": [
																				"InvoicedNonPO__"
																			],
																			"options": {
																				"label": "_Invoiced non po",
																				"version": 0
																			},
																			"stamp": 36
																		},
																		"InvoicedNonPO__": {
																			"type": "Decimal",
																			"data": [
																				"InvoicedNonPO__"
																			],
																			"options": {
																				"version": 1,
																				"textSize": "S",
																				"textAlignment": "right",
																				"textStyle": "default",
																				"textColor": "default",
																				"integer": false,
																				"label": "_Invoiced non po",
																				"precision_internal": 2,
																				"activable": true,
																				"width": 230,
																				"autocompletable": false,
																				"precision_current": 2,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"defaultValue": "0"
																			},
																			"stamp": 37
																		},
																		"LabelClosed__": {
																			"type": "Label",
																			"data": [
																				"Closed__"
																			],
																			"options": {
																				"label": "_Closed",
																				"version": 0
																			},
																			"stamp": 50
																		},
																		"Closed__": {
																			"type": "CheckBox",
																			"data": [
																				"Closed__"
																			],
																			"options": {
																				"label": "_Closed",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"version": 0
																			},
																			"stamp": 51
																		},
																		"LabelNotifyOwner__": {
																			"type": "Label",
																			"data": [
																				"NotifyOwner__"
																			],
																			"options": {
																				"label": "_NotifyOwner",
																				"version": 0
																			},
																			"stamp": 58
																		},
																		"NotifyOwner__": {
																			"type": "CheckBox",
																			"data": [
																				"NotifyOwner__"
																			],
																			"options": {
																				"label": "_NotifyOwner",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"helpIconPosition": "Right",
																				"version": 0
																			},
																			"stamp": 59
																		},
																		"LabelWarningThreshold__": {
																			"type": "Label",
																			"data": [
																				"WarningThreshold__"
																			],
																			"options": {
																				"label": "_WarningThreshold",
																				"version": 0
																			},
																			"stamp": 54
																		},
																		"WarningThreshold__": {
																			"type": "Integer",
																			"data": [
																				"WarningThreshold__"
																			],
																			"options": {
																				"version": 2,
																				"textSize": "S",
																				"textAlignment": "right",
																				"textStyle": "default",
																				"textColor": "default",
																				"integer": true,
																				"label": "_WarningThreshold",
																				"precision_internal": 0,
																				"precision_current": 0,
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"helpIconPosition": "Right",
																				"enablePlusMinus": false,
																				"browsable": false
																			},
																			"stamp": 55
																		},
																		"LabelOwnerLogin__": {
																			"type": "Label",
																			"data": [
																				"OwnerLogin__"
																			],
																			"options": {
																				"label": "_OwnerLogin",
																				"version": 0
																			},
																			"stamp": 56
																		},
																		"OwnerLogin__": {
																			"type": "ShortText",
																			"data": [
																				"OwnerLogin__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"version": 1,
																				"label": "_OwnerLogin",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"helpIconPosition": "Right",
																				"length": 80,
																				"browsable": false
																			},
																			"stamp": 57
																		}
																	},
																	"stamp": 8
																}
															},
															"stamp": 7,
															"data": []
														}
													},
													"stamp": 6,
													"data": []
												}
											}
										}
									},
									"stamp": 5,
									"data": []
								}
							},
							"stamp": 4,
							"data": []
						}
					},
					"stamp": 3,
					"data": []
				},
				"form-footer": {
					"type": "FlexibleFormFooter",
					"options": {
						"splitterType": "simple-h",
						"version": 0
					},
					"*": {
						"Save": {
							"type": "SubmitButton",
							"options": {
								"label": "_Save",
								"action": "savequit",
								"submit": true,
								"version": 0,
								"shortCut": "S",
								"disabled": true
							},
							"stamp": 11,
							"data": []
						},
						"Close": {
							"type": "SubmitButton",
							"options": {
								"label": "_Close",
								"action": "cancel",
								"submit": true,
								"version": 0,
								"shortCut": "Q"
							},
							"stamp": 12,
							"data": []
						},
						"Delete": {
							"type": "SubmitButton",
							"options": {
								"label": "_Delete",
								"action": "delete",
								"submit": true,
								"version": 0,
								"disabled": true
							},
							"stamp": 13,
							"data": []
						}
					},
					"stamp": 10,
					"data": []
				}
			},
			"stamp": 1,
			"data": []
		}
	},
	"stamps": 59,
	"data": []
}