///#GLOBALS Lib
/* Vendor Invoice Payment script */

function run()
{
	var actionType = Data.GetActionType().toLowerCase();
	var actionName = Data.GetActionName().toLowerCase();
	var vendorInvoiceProcessName = Variable.GetValueAsString("VendorInvoiceProcessName");

	if (actionType === "approve" && actionName === "update")
	{
		var mode = Lib.AP.UpdatePaymentDetails.GetMode();

		switch (mode)
		{
			case "SingleInvoice":
			{
				// From single invoice: update the invoice
				//Wait finalization script to be sure that the invoice will be filled before we reopen it
				Data.SetValue("KeepOpenAfterApproval", "waitForFinalization");

				var ruidex = Data.GetValue("SourceRUID");
				var payments = Lib.AP.UpdatePaymentDetails.CreatePaymentsForUpdate(ruidex);
				Lib.ERP.CreateManagerFromRecord("CDNAME#Vendor Invoice", "(ruidex=" + ruidex + ")", function(erpMgr)
				{
					if (erpMgr)
					{
						Lib.AP.UpdatePaymentDetails.erpMgr = erpMgr;
					}
					Lib.AP.UpdatePaymentDetails.JSONToUpdatePaymentDetails(payments, Lib.AP.UpdatePaymentDetails.GetCurrentUser(), vendorInvoiceProcessName, "ruidex");
				});
				break;
			}
			case "CSV":
			{
				var csvCheckError = Lib.AP.UpdatePaymentDetails.CheckCSVHeaders();
				if (csvCheckError)
				{
					var messageTranslated = Language.Translate(csvCheckError);
					Variable.SetValueAsString("errorMessage", messageTranslated);
					Process.PreventApproval();
				}
				// Do nothing, job will be done in finalization script
				break;
			}
			//case "AdminList":
			default:
			{
				// From admin list: do nothing, job will be done in finalization script
				break;
			}
		}
	}
}

run();

