{
	"*": {
		"GDRData": {
			"type": "GDRData",
			"options": {
				"version": 0
			},
			"stamp": 1,
			"data": []
		},
		"OCRParams": {
			"type": "OCRParams",
			"options": {
				"version": 0
			},
			"stamp": 2,
			"data": []
		},
		"Screen": {
			"type": "FlexibleFormLayout",
			"options": {
				"splitterType": "complex",
				"splitterOptions": {
					"sizeable": false,
					"sidebarTopHeight": 130,
					"sidebarLeftWidth": 1024,
					"unit": "px",
					"fixedSlider": true
				},
				"version": 0,
				"maxwidth": 1024,
				"style": "FlexibleFormLight",
				"backColor": "text-backgroundcolor-default"
			},
			"*": {
				"form-header": {
					"type": "FlexibleFormHeader",
					"options": {
						"hidePanelsMenu": true,
						"hideDesignButton": true,
						"version": 0,
						"maxwidth": 1024
					},
					"stamp": 3,
					"data": []
				},
				"form-center": {
					"type": "FlexibleFormCenter",
					"options": {
						"version": 0,
						"maxwidth": 1024
					},
					"*": {
						"form-content": {
							"type": "FlexibleFormContent",
							"options": {
								"version": 0
							},
							"*": {
								"form-content-top": {
									"type": "FlexibleFormSplitter",
									"options": {
										"autosize": true,
										"version": 1,
										"box": {
											"top": "0%",
											"left": "0%",
											"width": null,
											"height": "17%"
										},
										"hidden": false,
										"hideSeparator": true
									},
									"*": {
										"form-content-top-1": {
											"type": "FlexibleFormLine",
											"data": [],
											"options": {
												"version": 0
											},
											"stamp": 4,
											"*": {
												"WizardStepsPane": {
													"type": "PanelData",
													"data": [],
													"options": {
														"border": {},
														"labelLength": 0,
														"iconURL": "",
														"TitleFontSize": "M",
														"hideActionButtons": true,
														"hideTitleBar": true,
														"hideTitle": true,
														"backgroundcolor": "color1",
														"labelsAlignment": "center",
														"label": "_WizardStepsPane",
														"hidden": false,
														"leftImageURL": "",
														"removeMargins": true,
														"elementsAlignment": "center",
														"version": 0
													},
													"stamp": 5,
													"*": {
														"FieldsManager": {
															"type": "FieldsManager",
															"data": [],
															"options": {
																"controlsAssociation": {
																	"LabelGeneralStepButton__": "ImportStepButton__",
																	"LabelPreviewStepButton__": "OperationStepButton__",
																	"WorkflowStepButton__": "LabelWorkflowStepButton__",
																	"LabelWorkflowStepButton__": "WorkflowStepButton__",
																	"SummaryStepButton__": "LabelSummaryStepButton__",
																	"LabelSummaryStepButton__": "SummaryStepButton__",
																	"OperationStepButton__": "LabelPreviewStepButton__",
																	"ImportStepButton__": "LabelGeneralStepButton__"
																},
																"version": 0
															},
															"stamp": 6,
															"*": {
																"Grid": {
																	"type": "HorizontalLayout",
																	"data": [
																		"fields"
																	],
																	"options": {
																		"ctrlsPos": {
																			"WizardTopSpacer__": {
																				"line": 1,
																				"column": 1
																			},
																			"WizardBottomSpacer__": {
																				"line": 7,
																				"column": 1
																			},
																			"Spacer2__": {
																				"line": 3,
																				"column": 1
																			},
																			"Spacer3__": {
																				"line": 5,
																				"column": 1
																			},
																			"WorkflowStepButton__": {
																				"line": 4,
																				"column": 1
																			},
																			"SummaryStepButton__": {
																				"line": 6,
																				"column": 1
																			},
																			"ImportStepButton__": {
																				"line": 2,
																				"column": 1
																			}
																		},
																		"lines": 7,
																		"colspans": [
																			[
																				[
																					{
																						"index": 0,
																						"length": 2
																					}
																				]
																			],
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			],
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			],
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			],
																			[
																				[
																					{
																						"index": 0,
																						"length": 2
																					}
																				]
																			],
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			],
																			[
																				[
																					{
																						"index": 0,
																						"length": 2
																					}
																				]
																			],
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			],
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			]
																		],
																		"version": 0
																	},
																	"stamp": 7,
																	"*": {
																		"WizardTopSpacer__": {
																			"type": "Spacer",
																			"data": false,
																			"options": {
																				"height": "15",
																				"width": "100%",
																				"color": "default",
																				"label": "_WizardTopSpacer",
																				"version": 0
																			},
																			"stamp": 8
																		},
																		"ImportStepButton__": {
																			"type": "FormButton",
																			"data": false,
																			"options": {
																				"buttonLabel": "_ImportStepButton",
																				"label": "_Button",
																				"textPosition": "text-below",
																				"textSize": "S",
																				"textStyle": "default",
																				"textColor": "color8",
																				"nextprocess": {
																					"processName": "AP - Application Settings Wizard",
																					"attachmentsMode": "all",
																					"willBeChild": true,
																					"returnToOriginalUrl": false
																				},
																				"urlImageOverlay": "",
																				"awesomeClasses": "fa fa-cloud-upload fa-3x",
																				"style": 5,
																				"width": "100%",
																				"action": "none",
																				"url": "",
																				"version": 0
																			},
																			"stamp": 9
																		},
																		"WizardBottomSpacer__": {
																			"type": "Spacer",
																			"data": false,
																			"options": {
																				"height": "15",
																				"width": "100%",
																				"color": "default",
																				"label": "_WizardBottomSpacer",
																				"version": 0
																			},
																			"stamp": 10
																		},
																		"Spacer2__": {
																			"type": "Text",
																			"data": false,
																			"options": {
																				"marge": 0,
																				"textSize": "XL",
																				"textAlignment": "center",
																				"textStyle": "bold",
																				"textColor": "color6",
																				"backgroundcolor": "default",
																				"iconClass": "",
																				"label": "___",
																				"version": 0
																			},
																			"stamp": 11
																		},
																		"WorkflowStepButton__": {
																			"type": "FormButton",
																			"data": false,
																			"options": {
																				"buttonLabel": "_WorkflowStepButton",
																				"label": "_Button",
																				"textPosition": "text-below",
																				"textSize": "S",
																				"textStyle": "default",
																				"textColor": "color9",
																				"nextprocess": {
																					"processName": "AP - Application Settings Wizard",
																					"attachmentsMode": "all",
																					"willBeChild": true,
																					"returnToOriginalUrl": false
																				},
																				"urlImageOverlay": "",
																				"awesomeClasses": "fa fa-check-square-o fa-3x",
																				"style": 5,
																				"width": "100%",
																				"action": "none",
																				"url": "",
																				"version": 0
																			},
																			"stamp": 12
																		},
																		"Spacer3__": {
																			"type": "Text",
																			"data": false,
																			"options": {
																				"marge": 0,
																				"textSize": "XL",
																				"textAlignment": "center",
																				"textStyle": "bold",
																				"textColor": "color6",
																				"backgroundcolor": "default",
																				"iconClass": "",
																				"label": "___",
																				"version": 0
																			},
																			"stamp": 13
																		},
																		"SummaryStepButton__": {
																			"type": "FormButton",
																			"data": false,
																			"options": {
																				"buttonLabel": "_SummaryStepButton",
																				"label": "_Button",
																				"textPosition": "text-below",
																				"textSize": "S",
																				"textStyle": "default",
																				"textColor": "color9",
																				"nextprocess": {
																					"processName": "AP - Application Settings Wizard",
																					"attachmentsMode": "all",
																					"willBeChild": true,
																					"returnToOriginalUrl": false
																				},
																				"urlImageOverlay": "",
																				"awesomeClasses": "fa fa-flag-checkered fa-3x",
																				"style": 5,
																				"width": "100%",
																				"action": "none",
																				"url": "",
																				"version": 0
																			},
																			"stamp": 14
																		}
																	}
																}
															}
														}
													}
												}
											}
										}
									},
									"stamp": 15,
									"data": []
								},
								"form-content-bottom": {
									"type": "FlexibleFormSplitter",
									"options": {
										"version": 1,
										"box": {
											"top": 17,
											"left": 0,
											"width": 100,
											"height": 83
										},
										"hidden": false
									},
									"*": {
										"form-content-left": {
											"type": "FlexibleFormSplitter",
											"options": {
												"version": 1,
												"box": {
													"top": "0%",
													"left": "0%",
													"width": "100%",
													"height": null
												},
												"hidden": false
											},
											"*": {
												"form-content-left-1": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 16,
													"*": {
														"GeneralInformationsPane": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": "230",
																"iconURL": "",
																"TitleFontSize": "L",
																"hideActionButtons": true,
																"hideTitleBar": true,
																"hideTitle": true,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "_GeneralInformationsPane",
																"hidden": false,
																"leftImageURL": "",
																"removeMargins": false,
																"elementsAlignment": "left",
																"version": 0
															},
															"stamp": 17,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"Status__": "LabelStatus__",
																			"LabelStatus__": "Status__",
																			"SubmissionDateTime__": "LabelSubmissionDateTime__",
																			"LabelSubmissionDateTime__": "SubmissionDateTime__",
																			"RequesterLogin__": "LabelRequesterLogin__",
																			"LabelRequesterLogin__": "RequesterLogin__",
																			"RequesterName__": "LabelRequesterName__",
																			"LabelRequesterName__": "RequesterName__",
																			"Reason__": "LabelReason__",
																			"LabelReason__": "Reason__",
																			"ImportDescription__": "LabelImportDescription__",
																			"LabelImportDescription__": "ImportDescription__",
																			"DownloadTemplate__": "LabelDownloadTemplate__",
																			"LabelDownloadTemplate__": "DownloadTemplate__",
																			"CompanyCode__": "LabelCompanyCode__",
																			"LabelCompanyCode__": "CompanyCode__",
																			"VendorNumber__": "LabelVendorNumber__",
																			"LabelVendorNumber__": "VendorNumber__",
																			"VendorName__": "LabelVendorName__",
																			"LabelVendorName__": "VendorName__",
																			"ValidationDateTime__": "LabelValidationDateTime__",
																			"LabelValidationDateTime__": "ValidationDateTime__"
																		},
																		"version": 0
																	},
																	"stamp": 18,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"Status__": {
																						"line": 4,
																						"column": 2
																					},
																					"LabelStatus__": {
																						"line": 4,
																						"column": 1
																					},
																					"SubmissionDateTime__": {
																						"line": 8,
																						"column": 2
																					},
																					"LabelSubmissionDateTime__": {
																						"line": 8,
																						"column": 1
																					},
																					"RequesterLogin__": {
																						"line": 6,
																						"column": 2
																					},
																					"LabelRequesterLogin__": {
																						"line": 6,
																						"column": 1
																					},
																					"RequesterName__": {
																						"line": 7,
																						"column": 2
																					},
																					"LabelRequesterName__": {
																						"line": 7,
																						"column": 1
																					},
																					"Reason__": {
																						"line": 5,
																						"column": 2
																					},
																					"LabelReason__": {
																						"line": 5,
																						"column": 1
																					},
																					"ImportDescription__": {
																						"line": 13,
																						"column": 2
																					},
																					"LabelImportDescription__": {
																						"line": 13,
																						"column": 1
																					},
																					"DownloadTemplate__": {
																						"line": 14,
																						"column": 2
																					},
																					"LabelDownloadTemplate__": {
																						"line": 14,
																						"column": 1
																					},
																					"CompanyCode__": {
																						"line": 10,
																						"column": 2
																					},
																					"LabelCompanyCode__": {
																						"line": 10,
																						"column": 1
																					},
																					"VendorNumber__": {
																						"line": 11,
																						"column": 2
																					},
																					"LabelVendorNumber__": {
																						"line": 11,
																						"column": 1
																					},
																					"VendorName__": {
																						"line": 12,
																						"column": 2
																					},
																					"LabelVendorName__": {
																						"line": 12,
																						"column": 1
																					},
																					"ValidationDateTime__": {
																						"line": 9,
																						"column": 2
																					},
																					"LabelValidationDateTime__": {
																						"line": 9,
																						"column": 1
																					},
																					"HelpDescription__": {
																						"line": 1,
																						"column": 1
																					},
																					"Spacer__": {
																						"line": 2,
																						"column": 1
																					},
																					"Spacer4__": {
																						"line": 3,
																						"column": 1
																					}
																				},
																				"lines": 14,
																				"columns": 2,
																				"colspans": [
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[]
																				],
																				"version": 0
																			},
																			"stamp": 19,
																			"*": {
																				"HelpDescription__": {
																					"type": "Text",
																					"data": false,
																					"options": {
																						"marge": 245,
																						"textSize": "S",
																						"textAlignment": "center",
																						"textStyle": "bold",
																						"textColor": "color1",
																						"backgroundcolor": "default",
																						"iconClass": "",
																						"label": "_HelpDescription",
																						"version": 0
																					},
																					"stamp": 20
																				},
																				"Spacer__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "",
																						"width": "",
																						"color": "default",
																						"label": "_Spacer",
																						"version": 0
																					},
																					"stamp": 23
																				},
																				"Spacer4__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "",
																						"width": "",
																						"color": "default",
																						"label": "_Spacer4",
																						"version": 0
																					},
																					"stamp": 199
																				},
																				"LabelStatus__": {
																					"type": "Label",
																					"data": [
																						"Status__"
																					],
																					"options": {
																						"label": "_Status",
																						"version": 0
																					},
																					"stamp": 24
																				},
																				"Status__": {
																					"type": "ComboBox",
																					"data": [
																						"Status__"
																					],
																					"options": {
																						"possibleValues": {
																							"0": "_Draft",
																							"1": "_ToApprove",
																							"2": "_Approved",
																							"3": "_Rejected",
																							"4": "_Reverted"
																						},
																						"version": 1,
																						"keyValueMode": true,
																						"possibleKeys": {
																							"0": "Draft",
																							"1": "ToApprove",
																							"2": "Approved",
																							"3": "Rejected",
																							"4": "Reverted"
																						},
																						"label": "_Status",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"DataSourceOrigin": "Standalone",
																						"DisplayedColumns": "",
																						"readonly": true
																					},
																					"stamp": 25
																				},
																				"LabelReason__": {
																					"type": "Label",
																					"data": [
																						"Reason__"
																					],
																					"options": {
																						"label": "_Reason",
																						"version": 0
																					},
																					"stamp": 26
																				},
																				"Reason__": {
																					"type": "LongText",
																					"data": [
																						"Reason__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"minNbLines": 2,
																						"label": "_Reason",
																						"activable": true,
																						"width": "500",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"numberOfLines": 999,
																						"browsable": false
																					},
																					"stamp": 27
																				},
																				"LabelRequesterLogin__": {
																					"type": "Label",
																					"data": [
																						"RequesterLogin__"
																					],
																					"options": {
																						"label": "_RequesterLogin",
																						"version": 0
																					},
																					"stamp": 28
																				},
																				"RequesterLogin__": {
																					"type": "ShortText",
																					"data": [
																						"RequesterLogin__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"label": "_RequesterLogin",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"browsable": false,
																						"readonly": true
																					},
																					"stamp": 29
																				},
																				"LabelRequesterName__": {
																					"type": "Label",
																					"data": [
																						"RequesterName__"
																					],
																					"options": {
																						"label": "_RequesterName",
																						"version": 0
																					},
																					"stamp": 30
																				},
																				"RequesterName__": {
																					"type": "ShortText",
																					"data": [
																						"RequesterName__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"label": "_RequesterName",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"browsable": false,
																						"readonly": true
																					},
																					"stamp": 31
																				},
																				"LabelSubmissionDateTime__": {
																					"type": "Label",
																					"data": [
																						"SubmissionDateTime__"
																					],
																					"options": {
																						"label": "_SubmissionDateTime",
																						"version": 0
																					},
																					"stamp": 32
																				},
																				"SubmissionDateTime__": {
																					"type": "RealDateTime",
																					"data": [
																						"SubmissionDateTime__"
																					],
																					"options": {
																						"readonly": true,
																						"displayLongFormat": false,
																						"label": "_SubmissionDateTime",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"version": 0
																					},
																					"stamp": 33
																				},
																				"LabelValidationDateTime__": {
																					"type": "Label",
																					"data": [
																						"ValidationDateTime__"
																					],
																					"options": {
																						"label": "_ValidationDateTime",
																						"version": 0
																					},
																					"stamp": 34
																				},
																				"ValidationDateTime__": {
																					"type": "RealDateTime",
																					"data": [
																						"ValidationDateTime__"
																					],
																					"options": {
																						"readonly": true,
																						"displayLongFormat": false,
																						"label": "_ValidationDateTime",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"version": 0
																					},
																					"stamp": 35
																				},
																				"LabelCompanyCode__": {
																					"type": "Label",
																					"data": [
																						"CompanyCode__"
																					],
																					"options": {
																						"label": "_CompanyCode",
																						"version": 0
																					},
																					"stamp": 36
																				},
																				"CompanyCode__": {
																					"type": "ComboBox",
																					"data": [
																						"CompanyCode__"
																					],
																					"options": {
																						"possibleValues": {},
																						"version": 1,
																						"keyValueMode": false,
																						"possibleKeys": {},
																						"label": "_CompanyCode",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"DataSourceOrigin": "Standalone",
																						"SavedColumn": "CompanyCode__",
																						"DisplayedColumns": "CompanyName__"
																					},
																					"stamp": 37
																				},
																				"LabelVendorNumber__": {
																					"type": "Label",
																					"data": [
																						"VendorNumber__"
																					],
																					"options": {
																						"label": "_VendorNumber",
																						"hidden": true,
																						"version": 0
																					},
																					"stamp": 38
																				},
																				"VendorNumber__": {
																					"type": "ShortText",
																					"data": [
																						"VendorNumber__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"label": "_VendorNumber",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"hidden": true,
																						"readonly": true,
																						"browsable": false
																					},
																					"stamp": 39
																				},
																				"LabelVendorName__": {
																					"type": "Label",
																					"data": [
																						"VendorName__"
																					],
																					"options": {
																						"label": "_VendorName",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 40
																				},
																				"VendorName__": {
																					"type": "ShortText",
																					"data": [
																						"VendorName__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"label": "_VendorName",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"browsable": false,
																						"autocompletable": false,
																						"readonly": true,
																						"hidden": true
																					},
																					"stamp": 41
																				},
																				"LabelImportDescription__": {
																					"type": "Label",
																					"data": [
																						"ImportDescription__"
																					],
																					"options": {
																						"label": "_ImportDescription",
																						"version": 0
																					},
																					"stamp": 42
																				},
																				"ImportDescription__": {
																					"type": "LongText",
																					"data": [
																						"ImportDescription__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"minNbLines": 2,
																						"label": "_ImportDescription",
																						"activable": true,
																						"width": "500",
																						"helpText": "_HelpCatalogUpdateRequest_tooltip",
																						"helpURL": "5022",
																						"helpFormat": "HTML Format",
																						"numberOfLines": 999,
																						"browsable": false
																					},
																					"stamp": 43
																				},
																				"LabelDownloadTemplate__": {
																					"type": "Label",
																					"data": false,
																					"options": {
																						"label": "_DownloadTemplate",
																						"version": 0
																					},
																					"stamp": 21
																				},
																				"DownloadTemplate__": {
																					"type": "Link",
																					"data": false,
																					"options": {
																						"version": 1,
																						"link": "xuf/resource.file?level=account&name=CatalogTemplate.csv&type=TYPE_MISC",
																						"openInCurrentWindow": false,
																						"label": "_DownloadTemplate",
																						"text": "_CSVTemplate"
																					},
																					"stamp": 22
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-12": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 44,
													"*": {
														"AttachmentsPane": {
															"type": "PanelDocument",
															"options": {
																"version": 0,
																"border": {},
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "L",
																"hideActionButtons": true,
																"hideTitleBar": false,
																"hideTitle": false,
																"label": "_Documents",
																"hidden": false
															},
															"*": {
																"Document": {
																	"data": [
																		"documentscontroldata"
																	],
																	"type": "Documents",
																	"options": {
																		"version": 0,
																		"downloadButton": true,
																		"previewButton": false
																	},
																	"stamp": 45
																}
															},
															"stamp": 46,
															"data": []
														}
													}
												},
												"form-content-left-4": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 47,
													"*": {
														"ParsingErrorsPane": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "L",
																"hideActionButtons": true,
																"hideTitleBar": false,
																"hideTitle": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "_ParsingErrorsPane",
																"hidden": true,
																"leftImageURL": "",
																"removeMargins": false,
																"elementsAlignment": "left",
																"version": 0
															},
															"stamp": 48,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {},
																		"version": 0
																	},
																	"stamp": 49,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"ParsingErrorItems__": {
																						"line": 1,
																						"column": 1
																					}
																				},
																				"lines": 1,
																				"columns": 2,
																				"colspans": [
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					]
																				],
																				"version": 0
																			},
																			"stamp": 50,
																			"*": {
																				"ParsingErrorItems__": {
																					"type": "Table",
																					"data": [],
																					"options": {
																						"version": 5,
																						"lines": 10,
																						"columns": 3,
																						"displayEmptyLines": false,
																						"alwaysOneLine": false,
																						"menuOptions": {
																							"disableInsertLine": true,
																							"disableAddLine": true,
																							"hideTopNavigation": true,
																							"hideBottomNavigation": true,
																							"hideTableRowMenu": true,
																							"hideTableRowDelete": true,
																							"hideTableRowAdd": true,
																							"hideTableRowDuplicate": true,
																							"addButtonShouldInsert": false
																						},
																						"item": {
																							"display": null
																						},
																						"group": {
																							"name": "GDR\nPage"
																						},
																						"label": "_ErrorItems",
																						"readonly": true,
																						"notInDB": true,
																						"dataType": "Enumeration",
																						"subsection": null
																					},
																					"stamp": 51,
																					"*": {
																						"Actions": {
																							"type": "$TableActions",
																							"subtype": "Actions",
																							"options": {
																								"version": 0
																							},
																							"stamp": 52,
																							"*": {
																								"Top": {
																									"type": "$TableActions",
																									"subtype": "Top",
																									"options": {
																										"version": 0
																									},
																									"stamp": 53
																								},
																								"Bottom": {
																									"type": "$TableActions",
																									"subtype": "Top",
																									"options": {
																										"version": 0
																									},
																									"stamp": 54
																								}
																							}
																						},
																						"Header": {
																							"type": "$TableRow",
																							"subtype": "Header",
																							"options": {
																								"version": 0
																							},
																							"stamp": 55,
																							"data": [
																								null
																							],
																							"*": {
																								"Cell1": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 56,
																									"data": [],
																									"*": {
																										"CSVLineNumber__": {
																											"type": "Label",
																											"data": [],
																											"options": {
																												"label": "_CSVLineNumber",
																												"version": 0
																											},
																											"stamp": 57,
																											"position": [
																												"_Integer"
																											]
																										}
																									}
																								},
																								"Cell2": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {},
																									"stamp": 58,
																									"data": [],
																									"*": {
																										"CSVLine__": {
																											"type": "Label",
																											"data": null,
																											"options": {
																												"label": "_CSVLine"
																											},
																											"stamp": 59,
																											"position": [
																												"_LongText"
																											]
																										}
																									}
																								},
																								"Cell3": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {},
																									"stamp": 60,
																									"data": [],
																									"*": {
																										"ErrorStatus__": {
																											"type": "Label",
																											"data": null,
																											"options": {
																												"label": "_ErrorStatus"
																											},
																											"stamp": 61,
																											"position": [
																												"_LongText"
																											]
																										}
																									}
																								}
																							}
																						},
																						"Clipboard": {
																							"type": "$TableRow",
																							"subtype": "Clipboard",
																							"options": {
																								"version": 0
																							},
																							"stamp": 62,
																							"data": [
																								null
																							],
																							"*": {
																								"Cell1": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 63,
																									"data": [],
																									"*": {
																										"CSVLineNumber__": {
																											"type": "Integer",
																											"data": [],
																											"options": {
																												"version": 2,
																												"textSize": "S",
																												"textAlignment": "right",
																												"textStyle": "default",
																												"textColor": "default",
																												"integer": true,
																												"label": "_CSVLineNumber",
																												"precision_internal": 2,
																												"precision_current": 2,
																												"activable": true,
																												"width": "70",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"enablePlusMinus": false,
																												"readonly": true,
																												"notInDB": true,
																												"dataType": "Number",
																												"browsable": false,
																												"autocompletable": false
																											},
																											"stamp": 64,
																											"position": [
																												"_Integer"
																											]
																										}
																									}
																								},
																								"Cell2": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {},
																									"stamp": 65,
																									"data": [],
																									"*": {
																										"CSVLine__": {
																											"type": "LongText",
																											"data": null,
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"minNbLines": 1,
																												"label": "_CSVLine",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"numberOfLines": 999,
																												"readonly": true,
																												"notInDB": true,
																												"dataType": "String",
																												"browsable": false
																											},
																											"stamp": 66,
																											"position": [
																												"_LongText"
																											]
																										}
																									}
																								},
																								"Cell3": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {},
																									"stamp": 67,
																									"data": [],
																									"*": {
																										"ErrorStatus__": {
																											"type": "LongText",
																											"data": null,
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"minNbLines": 1,
																												"label": "_ErrorStatus",
																												"activable": true,
																												"width": "350",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"numberOfLines": 999,
																												"readonly": true,
																												"notInDB": true,
																												"dataType": "String",
																												"browsable": false
																											},
																											"stamp": 68,
																											"position": [
																												"_LongText"
																											]
																										}
																									}
																								}
																							}
																						}
																					}
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-11": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 69,
													"*": {
														"PreviewAddedPane": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "L",
																"hideActionButtons": true,
																"hideTitleBar": false,
																"hideTitle": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "_PreviewAddedPane",
																"hidden": true,
																"leftImageURL": "",
																"removeMargins": false,
																"elementsAlignment": "left",
																"version": 0
															},
															"stamp": 70,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {},
																		"version": 0
																	},
																	"stamp": 71,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"AddedItems__": {
																						"line": 1,
																						"column": 1
																					}
																				},
																				"lines": 1,
																				"columns": 2,
																				"colspans": [
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					]
																				],
																				"version": 0
																			},
																			"stamp": 72,
																			"*": {
																				"AddedItems__": {
																					"type": "Table",
																					"data": [],
																					"options": {
																						"version": 5,
																						"lines": 10,
																						"columns": 7,
																						"displayEmptyLines": false,
																						"alwaysOneLine": false,
																						"menuOptions": {
																							"disableInsertLine": true,
																							"disableAddLine": true,
																							"hideTopNavigation": true,
																							"hideBottomNavigation": true,
																							"hideTableRowMenu": true,
																							"hideTableRowDelete": true,
																							"hideTableRowAdd": true,
																							"hideTableRowDuplicate": true,
																							"addButtonShouldInsert": false
																						},
																						"item": {
																							"display": null
																						},
																						"group": {
																							"name": "GDR\nPage"
																						},
																						"label": "_AddedItems",
																						"readonly": true,
																						"notInDB": true,
																						"dataType": "Enumeration",
																						"subsection": null
																					},
																					"stamp": 73,
																					"*": {
																						"Actions": {
																							"type": "$TableActions",
																							"subtype": "Actions",
																							"options": {
																								"version": 0
																							},
																							"stamp": 74,
																							"*": {
																								"Top": {
																									"type": "$TableActions",
																									"subtype": "Top",
																									"options": {
																										"version": 0
																									},
																									"stamp": 75
																								},
																								"Bottom": {
																									"type": "$TableActions",
																									"subtype": "Top",
																									"options": {
																										"version": 0
																									},
																									"stamp": 76
																								}
																							}
																						},
																						"Header": {
																							"type": "$TableRow",
																							"subtype": "Header",
																							"options": {
																								"version": 0
																							},
																							"stamp": 77,
																							"data": [
																								null
																							],
																							"*": {
																								"Cell1": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 78,
																									"data": [],
																									"*": {
																										"Number__": {
																											"type": "Label",
																											"data": [],
																											"options": {
																												"label": "_Number",
																												"version": 0
																											},
																											"stamp": 79,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell2": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 80,
																									"data": [],
																									"*": {
																										"Description__": {
																											"type": "Label",
																											"data": [],
																											"options": {
																												"label": "_Description",
																												"version": 0
																											},
																											"stamp": 81,
																											"position": [
																												"_LongText"
																											]
																										}
																									}
																								},
																								"Cell3": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 82,
																									"data": [],
																									"*": {
																										"ManufacturerName__": {
																											"type": "Label",
																											"data": [],
																											"options": {
																												"label": "_ManufacturerName",
																												"version": 0
																											},
																											"stamp": 83,
																											"position": [
																												"_ShortText"
																											]
																										}
																									}
																								},
																								"Cell4": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 84,
																									"data": [],
																									"*": {
																										"UnitPrice__": {
																											"type": "Label",
																											"data": [],
																											"options": {
																												"label": "_UnitPrice",
																												"version": 0
																											},
																											"stamp": 85,
																											"position": [
																												"_Decimal"
																											]
																										}
																									}
																								},
																								"Cell5": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 86,
																									"data": [],
																									"*": {
																										"Currency__": {
																											"type": "Label",
																											"data": [],
																											"options": {
																												"label": "_Currency",
																												"version": 0
																											},
																											"stamp": 87,
																											"position": [
																												"_ShortText"
																											]
																										}
																									}
																								},
																								"Cell6": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 88,
																									"data": [],
																									"*": {
																										"UnitOfMeasure__": {
																											"type": "Label",
																											"data": [],
																											"options": {
																												"label": "_UnitOfMeasure",
																												"version": 0
																											},
																											"stamp": 89,
																											"position": [
																												"_ShortText"
																											]
																										}
																									}
																								},
																								"Cell7": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 90,
																									"data": [],
																									"*": {
																										"UNSPSC__": {
																											"type": "Label",
																											"data": [],
																											"options": {
																												"label": "_UNSPSC",
																												"version": 0
																											},
																											"stamp": 91,
																											"position": [
																												"_ShortText"
																											]
																										}
																									}
																								}
																							}
																						},
																						"Clipboard": {
																							"type": "$TableRow",
																							"subtype": "Clipboard",
																							"options": {
																								"version": 0
																							},
																							"stamp": 92,
																							"data": [
																								null
																							],
																							"*": {
																								"Cell1": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 93,
																									"data": [],
																									"*": {
																										"Number__": {
																											"type": "ShortText",
																											"data": [],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"label": "_Number",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"readonly": true,
																												"notInDB": true,
																												"dataType": "String",
																												"browsable": false,
																												"autocompletable": false
																											},
																											"stamp": 94,
																											"position": [
																												"_ShortText"
																											]
																										}
																									}
																								},
																								"Cell2": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 95,
																									"data": [],
																									"*": {
																										"Description__": {
																											"type": "LongText",
																											"data": [],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"minNbLines": 1,
																												"label": "_Description",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"numberOfLines": 999,
																												"readonly": true,
																												"notInDB": true,
																												"dataType": "String",
																												"browsable": false
																											},
																											"stamp": 96,
																											"position": [
																												"_LongText"
																											]
																										}
																									}
																								},
																								"Cell3": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 97,
																									"data": [],
																									"*": {
																										"ManufacturerName__": {
																											"type": "ShortText",
																											"data": [],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"label": "_ManufacturerName",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"readonly": true,
																												"notInDB": true,
																												"dataType": "String",
																												"browsable": false,
																												"autocompletable": false
																											},
																											"stamp": 98,
																											"position": [
																												"_ShortText"
																											]
																										}
																									}
																								},
																								"Cell4": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 99,
																									"data": [],
																									"*": {
																										"UnitPrice__": {
																											"type": "Decimal",
																											"data": [],
																											"options": {
																												"version": 2,
																												"textSize": "S",
																												"textAlignment": "right",
																												"textStyle": "default",
																												"textColor": "default",
																												"integer": false,
																												"label": "_UnitPrice",
																												"precision_internal": 2,
																												"precision_current": 2,
																												"activable": true,
																												"width": "100",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"enablePlusMinus": false,
																												"readonly": true,
																												"notInDB": true,
																												"dataType": "Number",
																												"autocompletable": false
																											},
																											"stamp": 100,
																											"position": [
																												"_Decimal"
																											]
																										}
																									}
																								},
																								"Cell5": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 101,
																									"data": [],
																									"*": {
																										"Currency__": {
																											"type": "ShortText",
																											"data": [],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"label": "_Currency",
																												"activable": true,
																												"width": "70",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"readonly": true,
																												"notInDB": true,
																												"dataType": "String",
																												"browsable": false,
																												"autocompletable": false
																											},
																											"stamp": 102,
																											"position": [
																												"_ShortText"
																											]
																										}
																									}
																								},
																								"Cell6": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 103,
																									"data": [],
																									"*": {
																										"UnitOfMeasure__": {
																											"type": "ShortText",
																											"data": [],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"label": "_UnitOfMeasure",
																												"activable": true,
																												"width": "80",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"readonly": true,
																												"notInDB": true,
																												"dataType": "String",
																												"browsable": false,
																												"autocompletable": false
																											},
																											"stamp": 104,
																											"position": [
																												"_ShortText"
																											]
																										}
																									}
																								},
																								"Cell7": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 105,
																									"data": [],
																									"*": {
																										"UNSPSC__": {
																											"type": "ShortText",
																											"data": [],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"label": "_UNSPSC",
																												"activable": true,
																												"width": "80",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"readonly": true,
																												"notInDB": true,
																												"dataType": "String",
																												"browsable": false,
																												"autocompletable": false
																											},
																											"stamp": 106,
																											"position": [
																												"_ShortText"
																											]
																										}
																									}
																								}
																							}
																						}
																					}
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-6": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 107,
													"*": {
														"PreviewModifiedPane": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "L",
																"hideActionButtons": true,
																"hideTitleBar": false,
																"hideTitle": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "_PreviewModifiedPane",
																"hidden": true,
																"leftImageURL": "",
																"removeMargins": false,
																"elementsAlignment": "left",
																"version": 0
															},
															"stamp": 108,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {},
																		"version": 0
																	},
																	"stamp": 109,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"ModifiedItems__": {
																						"line": 1,
																						"column": 1
																					}
																				},
																				"lines": 1,
																				"columns": 2,
																				"colspans": [
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					]
																				],
																				"version": 0
																			},
																			"stamp": 110,
																			"*": {
																				"ModifiedItems__": {
																					"type": "Table",
																					"data": [],
																					"options": {
																						"version": 5,
																						"lines": 10,
																						"columns": 7,
																						"displayEmptyLines": false,
																						"alwaysOneLine": false,
																						"menuOptions": {
																							"disableInsertLine": true,
																							"disableAddLine": true,
																							"hideTopNavigation": true,
																							"hideBottomNavigation": true,
																							"hideTableRowMenu": true,
																							"hideTableRowDelete": true,
																							"hideTableRowAdd": true,
																							"hideTableRowDuplicate": true,
																							"addButtonShouldInsert": false
																						},
																						"item": {
																							"display": null
																						},
																						"group": {
																							"name": "GDR\nPage"
																						},
																						"label": "_ModifiedItems",
																						"readonly": true,
																						"notInDB": true,
																						"dataType": "Enumeration",
																						"subsection": null
																					},
																					"stamp": 111,
																					"*": {
																						"Actions": {
																							"type": "$TableActions",
																							"subtype": "Actions",
																							"options": {
																								"version": 0
																							},
																							"stamp": 112,
																							"*": {
																								"Top": {
																									"type": "$TableActions",
																									"subtype": "Top",
																									"options": {
																										"version": 0
																									},
																									"stamp": 113
																								},
																								"Bottom": {
																									"type": "$TableActions",
																									"subtype": "Top",
																									"options": {
																										"version": 0
																									},
																									"stamp": 114
																								}
																							}
																						},
																						"Header": {
																							"type": "$TableRow",
																							"subtype": "Header",
																							"options": {
																								"version": 0
																							},
																							"stamp": 115,
																							"data": [
																								null
																							],
																							"*": {
																								"Cell1": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 116,
																									"data": [],
																									"*": {
																										"Number__": {
																											"type": "Label",
																											"data": [],
																											"options": {
																												"label": "_Number",
																												"version": 0
																											},
																											"stamp": 117,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell2": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 118,
																									"data": [],
																									"*": {
																										"Description__": {
																											"type": "Label",
																											"data": [],
																											"options": {
																												"label": "_Description",
																												"version": 0
																											},
																											"stamp": 119,
																											"position": [
																												"_LongText"
																											]
																										}
																									}
																								},
																								"Cell3": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 120,
																									"data": [],
																									"*": {
																										"ManufacturerName__": {
																											"type": "Label",
																											"data": [],
																											"options": {
																												"label": "_ManufacturerName",
																												"version": 0
																											},
																											"stamp": 121,
																											"position": [
																												"_ShortText"
																											]
																										}
																									}
																								},
																								"Cell4": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 122,
																									"data": [],
																									"*": {
																										"UnitPrice__": {
																											"type": "Label",
																											"data": [],
																											"options": {
																												"label": "_UnitPrice",
																												"version": 0
																											},
																											"stamp": 123,
																											"position": [
																												"_Decimal"
																											]
																										}
																									}
																								},
																								"Cell5": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 124,
																									"data": [],
																									"*": {
																										"Currency__": {
																											"type": "Label",
																											"data": [],
																											"options": {
																												"label": "_Currency",
																												"version": 0
																											},
																											"stamp": 125,
																											"position": [
																												"_ShortText"
																											]
																										}
																									}
																								},
																								"Cell6": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 126,
																									"data": [],
																									"*": {
																										"UnitOfMeasure__": {
																											"type": "Label",
																											"data": [],
																											"options": {
																												"label": "_UnitOfMeasure",
																												"version": 0
																											},
																											"stamp": 127,
																											"position": [
																												"_ShortText"
																											]
																										}
																									}
																								},
																								"Cell7": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 128,
																									"data": [],
																									"*": {
																										"UNSPSC__": {
																											"type": "Label",
																											"data": [],
																											"options": {
																												"label": "_UNSPSC",
																												"version": 0
																											},
																											"stamp": 129,
																											"position": [
																												"_ShortText"
																											]
																										}
																									}
																								}
																							}
																						},
																						"Clipboard": {
																							"type": "$TableRow",
																							"subtype": "Clipboard",
																							"options": {
																								"version": 0
																							},
																							"stamp": 130,
																							"data": [
																								null
																							],
																							"*": {
																								"Cell1": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 131,
																									"data": [],
																									"*": {
																										"Number__": {
																											"type": "ShortText",
																											"data": [],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"label": "_Number",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"readonly": true,
																												"notInDB": true,
																												"dataType": "String",
																												"browsable": false,
																												"autocompletable": false
																											},
																											"stamp": 132,
																											"position": [
																												"_ShortText"
																											]
																										}
																									}
																								},
																								"Cell2": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 133,
																									"data": [],
																									"*": {
																										"Description__": {
																											"type": "LongText",
																											"data": [],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"minNbLines": 1,
																												"label": "_Description",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"numberOfLines": 999,
																												"readonly": true,
																												"notInDB": true,
																												"dataType": "String",
																												"browsable": false
																											},
																											"stamp": 134,
																											"position": [
																												"_LongText"
																											]
																										}
																									}
																								},
																								"Cell3": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 135,
																									"data": [],
																									"*": {
																										"ManufacturerName__": {
																											"type": "ShortText",
																											"data": [],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"label": "_ManufacturerName",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"readonly": true,
																												"notInDB": true,
																												"dataType": "String",
																												"browsable": false,
																												"autocompletable": false
																											},
																											"stamp": 136,
																											"position": [
																												"_ShortText"
																											]
																										}
																									}
																								},
																								"Cell4": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 137,
																									"data": [],
																									"*": {
																										"UnitPrice__": {
																											"type": "Decimal",
																											"data": [],
																											"options": {
																												"version": 2,
																												"textSize": "S",
																												"textAlignment": "right",
																												"textStyle": "default",
																												"textColor": "default",
																												"integer": false,
																												"label": "_UnitPrice",
																												"precision_internal": 2,
																												"precision_current": 2,
																												"activable": true,
																												"width": "100",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"enablePlusMinus": false,
																												"readonly": true,
																												"notInDB": true,
																												"dataType": "Number",
																												"autocompletable": false
																											},
																											"stamp": 138,
																											"position": [
																												"_Decimal"
																											]
																										}
																									}
																								},
																								"Cell5": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 139,
																									"data": [],
																									"*": {
																										"Currency__": {
																											"type": "ShortText",
																											"data": [],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"label": "_Currency",
																												"activable": true,
																												"width": "70",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"readonly": true,
																												"notInDB": true,
																												"dataType": "String",
																												"browsable": false,
																												"autocompletable": false
																											},
																											"stamp": 140,
																											"position": [
																												"_ShortText"
																											]
																										}
																									}
																								},
																								"Cell6": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 141,
																									"data": [],
																									"*": {
																										"UnitOfMeasure__": {
																											"type": "ShortText",
																											"data": [],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"label": "_UnitOfMeasure",
																												"activable": true,
																												"width": "80",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"readonly": true,
																												"notInDB": true,
																												"dataType": "String",
																												"browsable": false,
																												"autocompletable": false
																											},
																											"stamp": 142,
																											"position": [
																												"_ShortText"
																											]
																										}
																									}
																								},
																								"Cell7": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 143,
																									"data": [],
																									"*": {
																										"UNSPSC__": {
																											"type": "ShortText",
																											"data": [],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"label": "_UNSPSC",
																												"activable": true,
																												"width": "80",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"readonly": true,
																												"notInDB": true,
																												"dataType": "String",
																												"browsable": false,
																												"autocompletable": false
																											},
																											"stamp": 144,
																											"position": [
																												"_ShortText"
																											]
																										}
																									}
																								}
																							}
																						}
																					}
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-7": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 145,
													"*": {
														"PreviewDeletedPane": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "L",
																"hideActionButtons": true,
																"hideTitleBar": false,
																"hideTitle": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "_PreviewDeletedPane",
																"hidden": true,
																"leftImageURL": "",
																"removeMargins": false,
																"elementsAlignment": "left",
																"version": 0
															},
															"stamp": 146,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {},
																		"version": 0
																	},
																	"stamp": 147,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"DeletedItems__": {
																						"line": 1,
																						"column": 1
																					}
																				},
																				"lines": 1,
																				"columns": 2,
																				"colspans": [
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					]
																				],
																				"version": 0
																			},
																			"stamp": 148,
																			"*": {
																				"DeletedItems__": {
																					"type": "Table",
																					"data": [],
																					"options": {
																						"version": 5,
																						"lines": 10,
																						"columns": 7,
																						"displayEmptyLines": false,
																						"alwaysOneLine": false,
																						"menuOptions": {
																							"disableInsertLine": true,
																							"disableAddLine": true,
																							"hideTopNavigation": true,
																							"hideBottomNavigation": true,
																							"hideTableRowMenu": true,
																							"hideTableRowDelete": true,
																							"hideTableRowAdd": true,
																							"hideTableRowDuplicate": true,
																							"addButtonShouldInsert": false
																						},
																						"item": {
																							"display": null
																						},
																						"group": {
																							"name": "GDR\nPage"
																						},
																						"label": "_DeletedItems",
																						"readonly": true,
																						"notInDB": true,
																						"dataType": "Enumeration",
																						"subsection": null
																					},
																					"stamp": 149,
																					"*": {
																						"Actions": {
																							"type": "$TableActions",
																							"subtype": "Actions",
																							"options": {
																								"version": 0
																							},
																							"stamp": 150,
																							"*": {
																								"Top": {
																									"type": "$TableActions",
																									"subtype": "Top",
																									"options": {
																										"version": 0
																									},
																									"stamp": 151
																								},
																								"Bottom": {
																									"type": "$TableActions",
																									"subtype": "Top",
																									"options": {
																										"version": 0
																									},
																									"stamp": 152
																								}
																							}
																						},
																						"Header": {
																							"type": "$TableRow",
																							"subtype": "Header",
																							"options": {
																								"version": 0
																							},
																							"stamp": 153,
																							"data": [
																								null
																							],
																							"*": {
																								"Cell1": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 154,
																									"data": [],
																									"*": {
																										"Number__": {
																											"type": "Label",
																											"data": [],
																											"options": {
																												"label": "_Number",
																												"version": 0
																											},
																											"stamp": 155,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell2": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 156,
																									"data": [],
																									"*": {
																										"Description__": {
																											"type": "Label",
																											"data": [],
																											"options": {
																												"label": "_Description",
																												"version": 0
																											},
																											"stamp": 157,
																											"position": [
																												"_LongText"
																											]
																										}
																									}
																								},
																								"Cell3": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 158,
																									"data": [],
																									"*": {
																										"ManufacturerName__": {
																											"type": "Label",
																											"data": [],
																											"options": {
																												"label": "_ManufacturerName",
																												"version": 0
																											},
																											"stamp": 159,
																											"position": [
																												"_ShortText"
																											]
																										}
																									}
																								},
																								"Cell4": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 160,
																									"data": [],
																									"*": {
																										"UnitPrice__": {
																											"type": "Label",
																											"data": [],
																											"options": {
																												"label": "_UnitPrice",
																												"version": 0
																											},
																											"stamp": 161,
																											"position": [
																												"_Decimal"
																											]
																										}
																									}
																								},
																								"Cell5": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 162,
																									"data": [],
																									"*": {
																										"Currency__": {
																											"type": "Label",
																											"data": [],
																											"options": {
																												"label": "_Currency",
																												"version": 0
																											},
																											"stamp": 163,
																											"position": [
																												"_ShortText"
																											]
																										}
																									}
																								},
																								"Cell6": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 164,
																									"data": [],
																									"*": {
																										"UnitOfMeasure__": {
																											"type": "Label",
																											"data": [],
																											"options": {
																												"label": "_UnitOfMeasure",
																												"version": 0
																											},
																											"stamp": 165,
																											"position": [
																												"_ShortText"
																											]
																										}
																									}
																								},
																								"Cell7": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 166,
																									"data": [],
																									"*": {
																										"UNSPSC__": {
																											"type": "Label",
																											"data": [],
																											"options": {
																												"label": "_UNSPSC",
																												"version": 0
																											},
																											"stamp": 167,
																											"position": [
																												"_ShortText"
																											]
																										}
																									}
																								}
																							}
																						},
																						"Clipboard": {
																							"type": "$TableRow",
																							"subtype": "Clipboard",
																							"options": {
																								"version": 0
																							},
																							"stamp": 168,
																							"data": [
																								null
																							],
																							"*": {
																								"Cell1": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 169,
																									"data": [],
																									"*": {
																										"Number__": {
																											"type": "ShortText",
																											"data": [],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"label": "_Number",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"readonly": true,
																												"notInDB": true,
																												"dataType": "String",
																												"browsable": false,
																												"autocompletable": false
																											},
																											"stamp": 170,
																											"position": [
																												"_ShortText"
																											]
																										}
																									}
																								},
																								"Cell2": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 171,
																									"data": [],
																									"*": {
																										"Description__": {
																											"type": "LongText",
																											"data": [],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"minNbLines": 1,
																												"label": "_Description",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"numberOfLines": 999,
																												"readonly": true,
																												"notInDB": true,
																												"dataType": "String",
																												"browsable": false
																											},
																											"stamp": 172,
																											"position": [
																												"_LongText"
																											]
																										}
																									}
																								},
																								"Cell3": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 173,
																									"data": [],
																									"*": {
																										"ManufacturerName__": {
																											"type": "ShortText",
																											"data": [],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"label": "_ManufacturerName",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"readonly": true,
																												"notInDB": true,
																												"dataType": "String",
																												"browsable": false,
																												"autocompletable": false
																											},
																											"stamp": 174,
																											"position": [
																												"_ShortText"
																											]
																										}
																									}
																								},
																								"Cell4": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 175,
																									"data": [],
																									"*": {
																										"UnitPrice__": {
																											"type": "Decimal",
																											"data": [],
																											"options": {
																												"version": 2,
																												"textSize": "S",
																												"textAlignment": "right",
																												"textStyle": "default",
																												"textColor": "default",
																												"integer": false,
																												"label": "_UnitPrice",
																												"precision_internal": 2,
																												"precision_current": 2,
																												"activable": true,
																												"width": "100",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"enablePlusMinus": false,
																												"readonly": true,
																												"notInDB": true,
																												"dataType": "Number",
																												"autocompletable": false
																											},
																											"stamp": 176,
																											"position": [
																												"_Decimal"
																											]
																										}
																									}
																								},
																								"Cell5": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 177,
																									"data": [],
																									"*": {
																										"Currency__": {
																											"type": "ShortText",
																											"data": [],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"label": "_Currency",
																												"activable": true,
																												"width": "70",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"readonly": true,
																												"notInDB": true,
																												"dataType": "String",
																												"browsable": false,
																												"autocompletable": false
																											},
																											"stamp": 178,
																											"position": [
																												"_ShortText"
																											]
																										}
																									}
																								},
																								"Cell6": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 179,
																									"data": [],
																									"*": {
																										"UnitOfMeasure__": {
																											"type": "ShortText",
																											"data": [],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"label": "_UnitOfMeasure",
																												"activable": true,
																												"width": "80",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"readonly": true,
																												"notInDB": true,
																												"dataType": "String",
																												"browsable": false,
																												"autocompletable": false
																											},
																											"stamp": 180,
																											"position": [
																												"_ShortText"
																											]
																										}
																									}
																								},
																								"Cell7": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 181,
																									"data": [],
																									"*": {
																										"UNSPSC__": {
																											"type": "ShortText",
																											"data": [],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"label": "_UNSPSC",
																												"activable": true,
																												"width": "80",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"readonly": true,
																												"notInDB": true,
																												"dataType": "String",
																												"browsable": false,
																												"autocompletable": false
																											},
																											"stamp": 182,
																											"position": [
																												"_ShortText"
																											]
																										}
																									}
																								}
																							}
																						}
																					}
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-17": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 183,
													"*": {
														"NextProcess": {
															"type": "PanelNextProcess",
															"options": {
																"hidden": true,
																"version": 0,
																"border": {
																	"collapsed": true
																},
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "L",
																"hideActionButtons": false,
																"hideTitleBar": false,
																"hideTitle": false,
																"label": "_Next Process"
															},
															"stamp": 184,
															"data": []
														}
													}
												},
												"form-content-left-16": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 185,
													"*": {
														"SystemData": {
															"type": "PanelSystemData",
															"options": {
																"multicolumn": false,
																"version": 0,
																"border": {
																	"collapsed": true
																},
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "L",
																"hideActionButtons": false,
																"hideTitleBar": false,
																"hideTitle": false,
																"label": "_System data",
																"hidden": true
															},
															"stamp": 186,
															"data": []
														}
													}
												}
											},
											"stamp": 187,
											"data": []
										},
										"form-content-right": {
											"type": "FlexibleFormSplitter",
											"options": {
												"version": 1,
												"box": {
													"top": 0,
													"left": 45,
													"width": 55,
													"height": 100
												},
												"hidden": true,
												"hideSeparator": true
											},
											"*": {
												"form-content-right-1": {
													"type": "FlexibleFormLine",
													"options": {
														"version": 0
													},
													"*": {
														"PreviewPanel": {
															"type": "PanelPreview",
															"options": {
																"border": {},
																"version": 0,
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": false,
																"hideTitleBar": false,
																"hideTitle": false,
																"label": "_Document Preview"
															},
															"*": {
																"Preview": {
																	"data": [
																		"previewdata"
																	],
																	"type": "Preview",
																	"options": {
																		"version": 0
																	},
																	"stamp": 188
																}
															},
															"stamp": 189,
															"data": []
														}
													},
													"stamp": 190,
													"data": []
												}
											},
											"stamp": 191,
											"data": []
										}
									},
									"stamp": 192,
									"data": []
								}
							},
							"stamp": 193,
							"data": []
						}
					},
					"stamp": 194,
					"data": []
				},
				"form-footer": {
					"type": "FlexibleFormFooter",
					"options": {
						"splitterType": "simple-h",
						"version": 0,
						"maxwidth": 1024
					},
					"*": {
						"Close": {
							"type": "SubmitButton",
							"options": {
								"label": "_Close",
								"action": "cancel",
								"submit": true,
								"version": 0
							},
							"stamp": 195,
							"data": []
						},
						"Submit": {
							"type": "SubmitButton",
							"data": false,
							"options": {
								"label": "_Submit",
								"textPosition": "text-right",
								"textSize": "MS",
								"textStyle": "default",
								"textColor": "color1",
								"nextprocess": {
									"processName": "AP - Application Settings Wizard",
									"attachmentsMode": "all",
									"willBeChild": true,
									"returnToOriginalUrl": false
								},
								"urlImageOverlay": "",
								"style": 1,
								"url": "",
								"position": null,
								"action": "approve",
								"version": 0
							},
							"stamp": 196
						}
					},
					"stamp": 197,
					"data": []
				}
			},
			"stamp": 198,
			"data": []
		}
	},
	"stamps": 199,
	"data": []
}