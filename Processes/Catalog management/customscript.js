///#GLOBALS Lib
Variable.SetValueAsString("CatalogManagmentType", "external");
Process.SetHelpId("5022");
ProcessInstance.SetSilentChange(true);
Controls.AttachmentsPane.AllowChangeOrUploadReferenceDocument(false);
var currentStatus = Data.GetValue("Status__");
var CompanyHelper = {
    hasError: false,
    GetCompanyValues: function () {
        var comboValues = [];
        var companies = CompanyHelper.VendorLinkInfo;
        if (Object.keys(companies).length === 0) {
            return comboValues;
        }
        for (var companyCode in companies) {
            if (companies.hasOwnProperty(companyCode)) {
                comboValues.push(companyCode + "=" + companies[companyCode]["CompanyName__"]);
            }
        }
        return comboValues;
    },
    GetVendorNumber: function () {
        return CompanyHelper.VendorLinkInfo[Data.GetValue("CompanyCode__")]["VendorNumber__"];
    },
    GetVendorName: function () {
        return CompanyHelper.VendorLinkInfo[Data.GetValue("CompanyCode__")]["VendorName__"];
    },
    VendorLinkInfo: {},
    InitCompanyBrowse: function () {
        if (Variable.GetValueAsString("vendorLinksInfos") !== "{}") {
            CompanyHelper.VendorLinkInfo = JSON.parse(Variable.GetValueAsString("vendorLinksInfos"));
            var comboValues = CompanyHelper.GetCompanyValues();
            var selectedCompanyCode = Data.GetValue("CompanyCode__");
            if (comboValues.length) {
                Controls.CompanyCode__.SetText(comboValues.join("\n"));
                Controls.CompanyCode__.SetRequired(true);
            }
            if (selectedCompanyCode && selectedCompanyCode !== "null") {
                if (!comboValues.length) {
                    Controls.CompanyCode__.SetText(selectedCompanyCode);
                }
                Controls.CompanyCode__.SetValue(selectedCompanyCode);
            }
            Controls.CompanyCode__.Hide(comboValues.length <= 1);
        }
        else {
            CompanyHelper.hasError = true;
            Controls.CompanyCode__.Hide(true);
            Popup.Alert(["_PleaseContactYourBuyer:No Vendor link record"], true, null, "_Submission failed");
        }
    }
};
function DisplayParsingError() {
    var tableControl = Controls.ParsingErrorItems__;
    tableControl.SetItemCount(0);
    tableControl.SetWidth("100%");
    tableControl.SetExtendableColumn("CSVLine__");
    var extractedData = Lib.Purchasing.CM.GetExternalData();
    var errorCount = 0;
    if (extractedData.ParsingError) {
        extractedData.ParsingError.forEach(function (value) {
            var newErrorItem = tableControl.AddItem();
            newErrorItem.SetValue("CSVLineNumber__", value.LineNumber);
            newErrorItem.SetValue("CSVLine__", value.CSVLine);
            newErrorItem.SetValue("ErrorStatus__", Language.Translate(value.ErrorStatus));
            errorCount++;
        });
        setTimeout(function () {
            var lineItemsCount = Math.min(tableControl.GetItemCount(), tableControl.GetLineCount());
            for (var lineIdx = 0; lineIdx < lineItemsCount; lineIdx++) {
                var row = tableControl.GetRow(lineIdx);
                row.AddStyle("highlight-danger");
            }
        }, 100);
    }
    Controls.ParsingErrorsPane.Hide(errorCount === 0);
    Controls.ParsingErrorsPane.SetLabel(Language.Translate("_ParsingErrorsPane {0}", true, errorCount));
    return errorCount != 0;
}
function InitImportStep() {
    Controls.HelpDescription__.SetWidth("495px");
    Controls.HelpDescription__.Hide(false);
    Controls.ImportStepButton__.SetTextColor("color8");
    Controls.WorkflowStepButton__.SetTextColor("color9");
    Controls.SummaryStepButton__.SetTextColor("color9");
    Controls.GeneralInformationsPane.Hide(false);
    Controls.Status__.Hide(true);
    Controls.Reason__.Hide(true);
    Controls.RequesterLogin__.Hide(true);
    Controls.RequesterName__.Hide(true);
    if (Sys.Helpers.IsEmpty(Data.GetValue("RequesterLogin__"))) {
        Data.SetValue("RequesterLogin__", User.loginId);
        Data.SetValue("RequesterName__", User.fullName);
        Controls.CompanyCode__.Hide(true);
    }
    else {
        CompanyHelper.InitCompanyBrowse();
    }
    Controls.SubmissionDateTime__.Hide(true);
    Controls.ValidationDateTime__.Hide(true);
    Controls.ImportDescription__.Hide(false);
    Controls.DownloadTemplate__.Hide(false);
    Controls.AttachmentsPane.Hide(false);
    var lineDisplayed = Lib.Purchasing.CM.DisplayPreviews(true);
    var CSVhasError = DisplayParsingError();
    Controls.AttachmentsPane.AllowChangeOrUploadReferenceDocument(true);
    var previousCompanyCode = Data.GetValue("CompanyCode__");
    Controls.CompanyCode__.OnChange = function () {
        function onConfirmOKClick() {
            Data.SetValue("VendorNumber__", CompanyHelper.GetVendorNumber());
            Data.SetValue("VendorName__", CompanyHelper.GetVendorName());
            //DO NOT CHANGE ACTION NAME : Document action name is used with keepUserDataWhenChangingDocument to keep the user Data when reprocessing
            ProcessInstance.Resubmit("Document");
        }
        function onConfirmCancelClick() {
            Data.SetValue("CompanyCode__", previousCompanyCode);
        }
        Popup.Confirm(["_The document will be reprocessed: Do you want to continue ?"], false, onConfirmOKClick, onConfirmCancelClick, "_CompanyCode changed");
    };
    Controls.Submit.SetLabel("_Send to approval");
    if (Variable.GetValueAsString("IsCMSubmitable") === "false") {
        Popup.Alert(["_PleaseContactYourBuyer:CM already being validated"], true, null, "_Submission failed");
        Controls.Submit.SetDisabled(true);
    }
    else {
        Controls.Submit.SetDisabled(CompanyHelper.hasError || CSVhasError || !Controls.AttachmentsPane.IsDocumentLoaded() || lineDisplayed === 0);
    }
}
function InitWorkflowStep() {
    Controls.HelpDescription__.Hide();
    Controls.ImportStepButton__.SetTextColor("color9");
    Controls.WorkflowStepButton__.SetTextColor("color8");
    Controls.SummaryStepButton__.SetTextColor("color9");
    Controls.GeneralInformationsPane.Hide(false);
    CompanyHelper.InitCompanyBrowse();
    Controls.Status__.Hide(false);
    Controls.Reason__.Hide(true);
    Controls.RequesterLogin__.Hide(true);
    Controls.RequesterName__.Hide(true);
    Controls.SubmissionDateTime__.Hide(false);
    Controls.ValidationDateTime__.Hide(true);
    Controls.ImportDescription__.Hide(false);
    Controls.DownloadTemplate__.Hide(true);
    Controls.AttachmentsPane.Hide(false);
    Lib.Purchasing.CM.DisplayPreviews(true, "ActionToDo");
    Controls.Submit.Hide(true);
    if (Variable.GetValueAsString("MissingWorkflowRuleError") == "true") {
        Popup.Alert(["_Approval workflow not set correctly on buyer side"], true, null, "_Workflow approval error");
    }
}
function InitResultStep() {
    Controls.HelpDescription__.Hide();
    Controls.ImportStepButton__.SetTextColor("color9");
    Controls.WorkflowStepButton__.SetTextColor("color9");
    Controls.SummaryStepButton__.SetTextColor("color8");
    Controls.GeneralInformationsPane.Hide(false);
    CompanyHelper.InitCompanyBrowse();
    Controls.Status__.Hide(false);
    Controls.Reason__.Hide(Sys.Helpers.IsEmpty(Data.GetValue("Reason__")));
    Controls.RequesterLogin__.Hide(true);
    Controls.RequesterName__.Hide(true);
    Controls.SubmissionDateTime__.Hide(false);
    Controls.ValidationDateTime__.Hide(false);
    Controls.ImportDescription__.Hide(false);
    Controls.DownloadTemplate__.Hide(true);
    Controls.CompanyCode__.SetReadOnly(true);
    Controls.ImportDescription__.SetReadOnly(false);
    Controls.AttachmentsPane.Hide(false);
    if (currentStatus !== "Rejected") {
        Lib.Purchasing.CM.DisplayPreviews(true, "ActionDone");
    }
    else {
        Lib.Purchasing.CM.DisplayPreviews(false);
    }
    Controls.Submit.Hide(true);
}
function HideTechnicalFields() {
    Controls.NextProcess.Hide(true);
    Controls.SystemData.Hide(true);
}
function HandleEvents() {
    Controls.AttachmentsPane.OnDocumentDeleted = function () {
        Lib.Purchasing.CM.SetExternalData(null);
        InitImportStep();
    };
}
function Initform() {
    switch (currentStatus) {
        case "ToApprove":
        case "Failed":
            InitWorkflowStep();
            break;
        case "Approved":
        case "Rejected":
        case "Reverted":
            InitResultStep();
            break;
        case "Draft":
        default:
            InitImportStep();
            break;
    }
    HideTechnicalFields();
    HandleEvents();
    // Show next alert notifications
    Lib.CommonDialog.NextAlert.Show({
        "CMCreationInfo": {
            IsShowable: function () {
                return !!Data.GetActionName();
            },
            Popup: function (nextAlert) {
                Popup.Alert([nextAlert.message], false, function () {
                    ProcessInstance.Quit("quit");
                }, nextAlert.title, true);
            }
        }
    });
}
Initform();
