var Extraction;
(function (Extraction) {
    Data.SetValue("ArchiveBehavior", "30"); // Do not archive Source Document.
    ///#GLOBALS Lib Sys
    /* Expense extraction script */
    var currentName = Data.GetActionName();
    var currentAction = Data.GetActionType();
    Log.Info("-- Expense Extraction Script -- Name: '" + (currentName ? currentName : "<empty>") + "', Action: '" + (currentAction ? currentAction : "<empty>") + "'");
    // When the process is created from an inbound channel, SourceRUID can be
    // Email inbound channel (ISM.XXXXX) or Email preprocessing (CD#XXXXX)
    var sourceRUID = Data.GetValue("SourceRUID");
    if (sourceRUID) {
        Log.Info("Expense created from: " + sourceRUID + ", email: " + Data.GetValue("FromAddress"));
        var currentUser = Users.GetUser(Data.GetValue("OwnerId"));
        Data.SetValue("OwnerName__", currentUser.GetValue("DisplayName"));
        Lib.Expense.LoadUserProperties(currentUser.GetValue("Login"))();
    }
    if (Sys.Parameters.GetInstance("P2P").GetParameter("EnableAIForExpenses") === "1" && Variable.GetValueAsString("TurnOffExtraction") != "yes") {
        var predictionJson = Sys.Expense.Extraction.QueryExtraction("AIForExpenses:GuessContent", Attach.GetGDRDataFile());
        try {
            Variable.SetValueAsString("AIResult", predictionJson);
            var prediction = JSON.parse(predictionJson);
            if (typeof prediction.error === "undefined") {
                Lib.Expense.FillExpenseWithPredictions(prediction);
                Lib.Expense.UpdateControl();
            }
            else {
                Log.Error("Error accessing AI server: " + predictionJson);
            }
        }
        catch (e) {
            Log.Error("Could not parse the prediction JSON: " + predictionJson);
        }
    }
    Sys.Helpers.TryCallFunction("Lib.Expense.Customization.Server.OnExtractionScriptEnd");
})(Extraction || (Extraction = {}));
