var Validation;
(function (Validation) {
    ///#GLOBALS Lib Sys
    var currentName = Data.GetActionName();
    var currentAction = Data.GetActionType();
    Log.Info("-- Expense Validation Script -- Name: '" + (currentName ? currentName : "<empty>") + "', Action: '" + (currentAction ? currentAction : "<empty>") + "' , Device: '" + Data.GetActionDevice());
    /** ********* **/
    /** FUNCTIONS **/
    /** ********* **/
    function IsFormInError() {
        if (Data.IsFormInError()) {
            Log.Info("Form in error.");
            return true;
        }
        return false;
    }
    function Save(doNotif) {
        if (!doNotif) {
            Lib.Expense.LayoutManager.CleanHiddenFields();
        }
        Lib.Expense.LayoutManager.FillEmptyFields();
        // Enforce the reimbursable amount server-side
        Lib.Expense.FillReimbursableAmount();
        var toSubmit = Sys.Helpers.TryCallFunction("Lib.Expense.Customization.Common.IsToSubmitExpense");
        // if not implemented or not evaluated
        if (!Sys.Helpers.IsBoolean(toSubmit)) {
            toSubmit = Lib.Expense.LayoutManager.CheckAllLayoutRequiredFields() && Lib.Expense.LayoutManager.CheckFieldsValidity();
        }
        var expenseNumber = Data.GetValue("ExpenseNumber__");
        if (!expenseNumber) {
            expenseNumber = Lib.Expense.NextNumber("Expense", "ExpenseNumber__");
            Data.SetValue("ExpenseNumber__", expenseNumber);
        }
        if (expenseNumber && toSubmit && !IsFormInError()) {
            Data.SetValue("ExpenseStatus__", "To submit");
        }
        else {
            Data.SetValue("ExpenseStatus__", "Draft");
        }
        Lib.Expense.LayoutManager.SetRequiredFields(false);
        if (doNotif) {
            var customTags = {
                Number: expenseNumber,
                ValidationUrl: Data.GetValue("ValidationUrl")
            };
            if (!Data.IsNullOrEmpty("TotalAmount__")) {
                customTags["Total"] = Data.GetValue("TotalAmount__");
                customTags["Currency"] = Data.GetValue("TotalAmountCurrency__");
            }
            var date = Data.GetValue("Date__");
            if (date) {
                customTags["Date"] = Helpers.Date.DateToShortFormat(date);
            }
            SendEmailNotification(customTags);
            SendPushNotification(customTags);
        }
        Process.PreventApproval();
        Process.LeaveForm();
    }
    function SendEmailNotification(customTags) {
        var options = {
            userId: Users.GetUser(Data.GetValue("OwnerId")).GetValue("Login"),
            emailAddress: Data.GetValue("FromAddress"),
            subject: "_Expense has been created",
            template: "Expense_Email_NotifExpenseCreated.htm",
            customTags: customTags,
            escapeCustomTags: true,
            fromName: "Esker Expense management"
        };
        Log.Info("Notify the Expense creation to " + options.emailAddress);
        var doSendNotif = Sys.Helpers.TryCallFunction("Lib.Expense.Customization.Server.OnSendEmailNotification", options);
        if (doSendNotif !== false) {
            Sys.EmailNotification.SendEmailNotification(options);
        }
    }
    function SendPushNotification(customTags) {
        // Send push notifications if enabled
        if (Process.GetProcessDefinition().PushNotification === true) {
            var pushNotificationType = (Sys.Parameters.GetInstance("Expense").GetParameter("PushNotificationType") || Sys.Parameters.GetInstance("P2P").GetParameter("PushNotificationType") || "").toLowerCase();
            Log.Info("Sending " + pushNotificationType + " push notification");
            if (pushNotificationType == "short" || pushNotificationType == "full") {
                Sys.PushNotification.SendNotifToUser({
                    user: Users.GetUser(Data.GetValue("OwnerId")),
                    id: (pushNotificationType == "short") ? Process.GetProcessID() : Data.GetValue("ruidex"),
                    template: "Expense_PushNotif_NotifExpenseCreated_" + pushNotificationType + ".txt",
                    sendToBackupUser: false,
                    customTags: customTags
                });
            }
        }
    }
    function OnSubmitReport() {
        Log.Info("This expense is pending approval");
        RemoveVisibilityOnExpense();
        var actionData = GetActionDataFromExpenseReport();
        GiveVisibilityOnExpense(actionData);
        SetManager(actionData.manager);
        ResetActionDataFromExpenseReport();
        Process.WaitForUpdate();
    }
    function OnRollbackSubmitReport() {
        Log.Info("The submission of this expense has been rollbacked");
        SetManager();
        RemoveVisibilityOnExpense();
        ResetActionDataFromExpenseReport();
        Process.PreventApproval();
    }
    function OnApprovedReport() {
        Log.Info("This expense has been approved");
        ResetActionDataFromExpenseReport();
        Process.WaitForUpdate();
    }
    function OnValidatedReport() {
        Log.Info("This expense is validated");
        ResetActionDataFromExpenseReport();
    }
    function OnForwardReport() {
        Log.Info("This expense has been forwarded");
        var actionData = GetActionDataFromExpenseReport();
        GiveVisibilityOnExpense(actionData);
        ResetActionDataFromExpenseReport();
        Process.WaitForUpdate();
    }
    function OnRequestFurtherApprovalReport() {
        OnForwardReport();
    }
    function OnBackToUserReport() {
        Log.Info("This expense is back to user");
        SetManager();
        ResetActionDataFromExpenseReport();
        Process.PreventApproval();
    }
    function Delete() {
        Lib.Expense.LayoutManager.CleanHiddenFields();
        Data.SetValue("ExpenseStatus__", "Deleted");
        Data.SetValue("State", 300);
        Process.LeaveForm();
    }
    function GetActionDataFromExpenseReport() {
        var actionData = Variable.GetValueAsString("FromExpenseReport_ActionData");
        return actionData && JSON.parse(actionData);
    }
    function ResetActionDataFromExpenseReport() {
        Variable.SetValueAsString("FromExpenseReport_ActionData", "");
    }
    function SetManager(manager) {
        var managerUser = manager ? Users.GetUser(manager) : null;
        if (managerUser) {
            Data.SetValue("Manager__", managerUser.GetValue("DisplayName"));
            Data.SetValue("ManagerDN__", managerUser.GetValue("FullDn"));
        }
        else {
            Data.SetValue("Manager__", "");
            Data.SetValue("ManagerDN__", "");
        }
    }
    function GiveVisibilityOnExpense(actionData) {
        Log.Info("Give visibility on this expense to report contributors");
        if (actionData) {
            if (actionData.contributors) {
                actionData.contributors.forEach(function (login) {
                    Log.Info("contributor: " + login);
                    Process.SetRight(login, "read");
                });
            }
        }
        else {
            Log.Warn("No contributors specified.");
        }
    }
    function RemoveVisibilityOnExpense() {
        Log.Info("Remove visibility on this expense to report contributors");
        Process.ResetRights();
    }
    /** ******** **/
    /** RUN PART **/
    /** ******** **/
    Lib.P2P.SetBillingInfo("EX001");
    Lib.Expense.InitTechnicalFields();
    if (currentName == "" && currentAction == "") {
        // When the process is created from an inbound channel, SourceRUID can be
        // Email inbound channel (ISM.XXXXX) or Email preprocessing (CD#XXXXX)
        Save(!!Data.GetValue("SourceRUID"));
    }
    else if (currentName !== "" && currentAction !== "") {
        if (currentAction === "approve_asynchronous" || currentAction === "approve" || currentAction === "ResumeWithAction") {
            Lib.CommonDialog.NextAlert.Reset();
            switch (currentName) {
                case "save":
                    break;
                case "Save":
                    Save();
                    break;
                case "DeleteExpense":
                    Delete();
                    break;
                case "FromExpenseReport_Submit":
                    OnSubmitReport();
                    break;
                case "FromExpenseReport_RollbackSubmit":
                    OnRollbackSubmitReport();
                    break;
                case "FromExpenseReport_BackToUser":
                    OnBackToUserReport();
                    break;
                case "FromExpenseReport_Approve":
                    OnApprovedReport();
                    break;
                case "FromExpenseReport_Validated":
                    OnValidatedReport();
                    break;
                case "FromExpenseReport_Forward":
                    OnForwardReport();
                    break;
                case "FromExpenseReport_RequestFurtherApproval":
                    OnRequestFurtherApprovalReport();
                    break;
                default:
                    Lib.Expense.OnUnknownAction(currentAction, currentName);
                    break;
            }
        }
        else if (currentAction !== "reprocess" && currentAction !== "reprocess_asynchronous") {
            Lib.Expense.OnUnknownAction(currentAction, currentName);
        }
        else if (Data.GetActionDevice() === "mobile") {
            Save();
        }
    }
    else {
        Lib.Expense.OnUnknownAction(currentAction, currentName);
    }
})(Validation || (Validation = {}));
