{
	"*": {
		"Screen": {
			"type": "FlexibleFormLayout",
			"options": {
				"splitterType": "simple-v",
				"version": 0
			},
			"*": {
				"form-header": {
					"type": "FlexibleFormHeader",
					"options": {
						"hidePanelsMenu": false,
						"version": 0
					},
					"stamp": 2,
					"data": []
				},
				"form-center": {
					"type": "FlexibleFormCenter",
					"options": {
						"version": 0
					},
					"*": {
						"form-content": {
							"type": "FlexibleFormContent",
							"options": {
								"version": 0
							},
							"*": {
								"form-content-center": {
									"type": "FlexibleFormSplitter",
									"options": {
										"version": 1,
										"box": {
											"top": "0%",
											"left": "0%",
											"width": "100%"
										},
										"hidden": false
									},
									"*": {
										"form-content-center-1": {
											"type": "FlexibleFormLine",
											"data": [],
											"options": {
												"version": 0
											},
											"stamp": 9,
											"*": {
												"DataPanel": {
													"type": "PanelData",
													"options": {
														"border": {},
														"version": 0,
														"labelLength": 0,
														"iconURL": "",
														"TitleFontSize": "S",
														"hideActionButtons": false,
														"hideTitleBar": false,
														"hideTitle": false,
														"backgroundcolor": "default",
														"labelsAlignment": "right",
														"label": "_Document data"
													},
													"*": {
														"FieldsManager": {
															"type": "FieldsManager",
															"options": {
																"controlsAssociation": {
																	"Description__": "LabelDescription__",
																	"LabelDescription__": "Description__",
																	"UnitOfMeasure__": "LabelUnitOfMeasure__",
																	"LabelUnitOfMeasure__": "UnitOfMeasure__",
																	"CompanyCode__": "LabelCompanyCode__",
																	"LabelCompanyCode__": "CompanyCode__",
																	"IdentifierInERP__": "LabelIdentifierInERP__",
																	"LabelIdentifierInERP__": "IdentifierInERP__"
																},
																"version": 0
															},
															"*": {
																"Grid": {
																	"type": "GridLayout",
																	"options": {
																		"lines": 4,
																		"columns": 2,
																		"version": 0,
																		"ctrlsPos": {
																			"Description__": {
																				"line": 3,
																				"column": 2
																			},
																			"LabelDescription__": {
																				"line": 3,
																				"column": 1
																			},
																			"UnitOfMeasure__": {
																				"line": 1,
																				"column": 2
																			},
																			"LabelUnitOfMeasure__": {
																				"line": 1,
																				"column": 1
																			},
																			"CompanyCode__": {
																				"line": 4,
																				"column": 2
																			},
																			"LabelCompanyCode__": {
																				"line": 4,
																				"column": 1
																			},
																			"IdentifierInERP__": {
																				"line": 2,
																				"column": 2
																			},
																			"LabelIdentifierInERP__": {
																				"line": 2,
																				"column": 1
																			}
																		},
																		"colspans": [
																			[],
																			[],
																			[],
																			[]
																		]
																	},
																	"data": [
																		"fields"
																	],
																	"*": {
																		"LabelUnitOfMeasure__": {
																			"type": "Label",
																			"data": [
																				"UnitOfMeasure__"
																			],
																			"options": {
																				"label": "_UnitOfMeasure",
																				"version": 0
																			},
																			"stamp": 14
																		},
																		"UnitOfMeasure__": {
																			"type": "ShortText",
																			"data": [
																				"UnitOfMeasure__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"version": 1,
																				"label": "_UnitOfMeasure",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"browsable": false,
																				"length": 3
																			},
																			"stamp": 15
																		},
																		"LabelIdentifierInERP__": {
																			"type": "Label",
																			"data": [
																				"IdentifierInERP__"
																			],
																			"options": {
																				"label": "_IdentifierInERP"
																			},
																			"stamp": 20
																		},
																		"IdentifierInERP__": {
																			"type": "ShortText",
																			"data": [
																				"IdentifierInERP__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"version": 1,
																				"label": "_IdentifierInERP",
																				"activable": true,
																				"width": 230
																			},
																			"stamp": 21
																		},
																		"LabelDescription__": {
																			"type": "Label",
																			"data": [
																				"Description__"
																			],
																			"options": {
																				"label": "_Description",
																				"version": 0
																			},
																			"stamp": 16
																		},
																		"Description__": {
																			"type": "ShortText",
																			"data": [
																				"Description__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"version": 1,
																				"label": "_Description",
																				"activable": true,
																				"width": 230
																			},
																			"stamp": 17
																		},
																		"LabelCompanyCode__": {
																			"type": "Label",
																			"data": [
																				"CompanyCode__"
																			],
																			"options": {
																				"label": "_CompanyCode"
																			},
																			"stamp": 18
																		},
																		"CompanyCode__": {
																			"type": "DatabaseComboBox",
																			"data": [
																				"CompanyCode__"
																			],
																			"options": {
																				"version": 1,
																				"label": "_CompanyCode",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"minNbLines": 1,
																				"maxNbLines": 1,
																				"fTSmaxRecords": "20",
																				"browsable": true,
																				"PreFillFTS": true
																			},
																			"stamp": 19
																		}
																	},
																	"stamp": 8
																}
															},
															"stamp": 7,
															"data": []
														}
													},
													"stamp": 6,
													"data": []
												}
											}
										}
									},
									"stamp": 5,
									"data": []
								}
							},
							"stamp": 4,
							"data": []
						}
					},
					"stamp": 3,
					"data": []
				},
				"form-footer": {
					"type": "FlexibleFormFooter",
					"options": {
						"splitterType": "simple-h",
						"version": 0
					},
					"*": {
						"Save": {
							"type": "SubmitButton",
							"options": {
								"label": "_Save",
								"action": "savequit",
								"submit": true,
								"version": 0,
								"shortCut": "S"
							},
							"stamp": 11,
							"data": []
						},
						"Close": {
							"type": "SubmitButton",
							"options": {
								"label": "_Close",
								"action": "cancel",
								"submit": true,
								"version": 0,
								"shortCut": "Q"
							},
							"stamp": 12,
							"data": []
						},
						"Delete": {
							"type": "SubmitButton",
							"options": {
								"label": "_Delete",
								"action": "delete",
								"submit": true,
								"version": 0,
								"disabled": true
							},
							"stamp": 13,
							"data": []
						}
					},
					"stamp": 10,
					"data": []
				}
			},
			"stamp": 1,
			"data": []
		}
	},
	"stamps": 21,
	"data": []
}