{
	"*": {
		"GDRData": {
			"type": "GDRData",
			"options": {
				"version": 0
			},
			"stamp": 1,
			"data": []
		},
		"OCRParams": {
			"type": "OCRParams",
			"options": {
				"version": 0
			},
			"stamp": 2,
			"data": []
		},
		"Screen": {
			"type": "FlexibleFormLayout",
			"options": {
				"splitterType": "complex",
				"version": 0,
				"maxwidth": "",
				"backColor": "text-backgroundcolor-color7",
				"style": "FlexibleFormLight"
			},
			"*": {
				"form-header": {
					"type": "FlexibleFormHeader",
					"options": {
						"hidePanelsMenu": true,
						"hideDesignButton": true,
						"version": 0,
						"maxwidth": ""
					},
					"stamp": 3,
					"data": []
				},
				"form-center": {
					"type": "FlexibleFormCenter",
					"options": {
						"version": 0,
						"maxwidth": ""
					},
					"*": {
						"form-content": {
							"type": "FlexibleFormContent",
							"options": {
								"version": 0
							},
							"*": {
								"form-content-top": {
									"type": "FlexibleFormSplitter",
									"options": {
										"autosize": true,
										"version": 1,
										"box": {
											"top": "0%",
											"left": "0%",
											"width": null,
											"height": "22%"
										},
										"hidden": false
									},
									"*": {
										"form-content-top-1": {
											"type": "FlexibleFormLine",
											"options": {
												"version": 0
											},
											"*": {
												"DocumentsPanel": {
													"type": "PanelDocument",
													"options": {
														"version": 0,
														"border": {},
														"labelLength": 0,
														"iconURL": "",
														"TitleFontSize": "S",
														"hideActionButtons": false,
														"hideTitleBar": false,
														"hideTitle": false,
														"label": "_Documents"
													},
													"*": {
														"Document": {
															"data": [
																"documentscontroldata"
															],
															"type": "Documents",
															"options": {
																"version": 0
															},
															"stamp": 4
														}
													},
													"stamp": 5,
													"data": []
												}
											},
											"stamp": 6,
											"data": []
										}
									},
									"stamp": 7,
									"data": []
								},
								"form-content-bottom": {
									"type": "FlexibleFormSplitter",
									"options": {
										"version": 1,
										"box": {
											"top": 0,
											"left": 0,
											"width": 100,
											"height": 100
										},
										"hidden": false,
										"hideSeparator": true
									},
									"*": {
										"form-content-left": {
											"type": "FlexibleFormSplitter",
											"options": {
												"version": 1,
												"box": {
													"top": "0%",
													"left": "0%",
													"width": "45%",
													"height": null
												},
												"hidden": false
											},
											"*": {
												"form-content-left-1": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 8,
													"*": {
														"PreviewPanel": {
															"type": "PanelPreview",
															"options": {
																"border": {},
																"version": 0,
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": false,
																"hideTitleBar": false,
																"hideTitle": false,
																"label": "_Document Preview",
																"hidden": false
															},
															"*": {
																"Preview": {
																	"data": [
																		"previewdata"
																	],
																	"type": "Preview",
																	"options": {
																		"version": 0
																	},
																	"stamp": 9
																}
															},
															"stamp": 10,
															"data": []
														}
													}
												}
											},
											"stamp": 11,
											"data": []
										},
										"form-content-right": {
											"type": "FlexibleFormSplitter",
											"options": {
												"version": 1,
												"box": {
													"top": 0,
													"left": 45,
													"width": 55,
													"height": 100
												},
												"hidden": false
											},
											"*": {
												"form-content-right-1": {
													"type": "FlexibleFormLine",
													"options": {
														"version": 0
													},
													"*": {
														"Payer": {
															"type": "PanelData",
															"options": {
																"border": {
																	"collapsed": false
																},
																"version": 0,
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": true,
																"hideTitleBar": false,
																"hideTitle": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "_Payer",
																"leftImageURL": "",
																"removeMargins": false,
																"elementsAlignment": "left",
																"hidden": false,
																"labelLength": "30%"
															},
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"options": {
																		"controlsAssociation": {
																			"PayerCompany__": "LabelPayerCompany__",
																			"LabelPayerCompany__": "PayerCompany__",
																			"PayerCompanyCode__": "LabelPayerCompanyCode__",
																			"LabelPayerCompanyCode__": "PayerCompanyCode__",
																			"PayerContact__": "LabelPayerContact__",
																			"LabelPayerContact__": "PayerContact__",
																			"PayerAddress__": "LabelPayerAddress__",
																			"LabelPayerAddress__": "PayerAddress__",
																			"restored_PaymentDate__": "restored_PaymentDate___label__",
																			"restored_PaymentDate___label__": "restored_PaymentDate__",
																			"restored_VendorAddress__": "restored_VendorAddress___label__",
																			"restored_VendorAddress___label__": "restored_VendorAddress__",
																			"restored_VendorName__": "restored_VendorName___label__",
																			"restored_VendorName___label__": "restored_VendorName__",
																			"restored_VendorNumber__": "restored_VendorNumber___label__",
																			"restored_VendorNumber___label__": "restored_VendorNumber__"
																		},
																		"version": 0
																	},
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"options": {
																				"lines": 3,
																				"columns": 2,
																				"version": 0,
																				"ctrlsPos": {
																					"PayerCompany__": {
																						"line": 2,
																						"column": 2
																					},
																					"LabelPayerCompany__": {
																						"line": 2,
																						"column": 1
																					},
																					"PayerCompanyCode__": {
																						"line": 1,
																						"column": 2
																					},
																					"LabelPayerCompanyCode__": {
																						"line": 1,
																						"column": 1
																					},
																					"PayerAddress__": {
																						"line": 3,
																						"column": 2
																					},
																					"LabelPayerAddress__": {
																						"line": 3,
																						"column": 1
																					}
																				},
																				"colspans": [
																					[],
																					[],
																					[]
																				]
																			},
																			"data": [
																				"fields"
																			],
																			"*": {
																				"LabelPayerCompanyCode__": {
																					"type": "Label",
																					"data": [
																						"PayerCompanyCode__"
																					],
																					"options": {
																						"label": "_PayerCompanyCode",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 12
																				},
																				"PayerCompanyCode__": {
																					"type": "ShortText",
																					"data": [
																						"PayerCompanyCode__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"label": "_PayerCompanyCode",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"hidden": true,
																						"browsable": false
																					},
																					"stamp": 13
																				},
																				"LabelPayerCompany__": {
																					"type": "Label",
																					"data": [
																						"PayerCompany__"
																					],
																					"options": {
																						"label": "_PayerCompany",
																						"version": 0
																					},
																					"stamp": 14
																				},
																				"PayerCompany__": {
																					"type": "DatabaseComboBox",
																					"data": [
																						"PayerCompany__"
																					],
																					"options": {
																						"version": 1,
																						"autoCompleteSearchMode": "startswith",
																						"searchMode": "contains",
																						"label": "_PayerCompany",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"minNbLines": 1,
																						"maxNbLines": 1,
																						"fTSmaxRecords": "20",
																						"browsable": true
																					},
																					"stamp": 15
																				},
																				"LabelPayerAddress__": {
																					"type": "Label",
																					"data": [
																						"PayerAddress__"
																					],
																					"options": {
																						"label": "_PayerAddress",
																						"version": 0
																					},
																					"stamp": 16
																				},
																				"PayerAddress__": {
																					"type": "LongText",
																					"data": [
																						"PayerAddress__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"minNbLines": 4,
																						"label": "_PayerAddress",
																						"activable": true,
																						"width": "275",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"numberOfLines": 4,
																						"readonly": true,
																						"browsable": false
																					},
																					"stamp": 17
																				}
																			},
																			"stamp": 18
																		}
																	},
																	"stamp": 19,
																	"data": []
																}
															},
															"stamp": 20,
															"data": []
														}
													},
													"stamp": 21,
													"data": []
												},
												"form-content-right-3": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 22,
													"*": {
														"Payee": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": true,
																"hideTitleBar": false,
																"hideTitle": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "_Payee",
																"hidden": false,
																"leftImageURL": "",
																"removeMargins": false,
																"elementsAlignment": "left",
																"version": 0,
																"labelLength": "30%"
															},
															"stamp": 23,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"LabelVendorName__": "VendorName__",
																			"VendorName__": "LabelVendorName__",
																			"LabelVendorAddress__": "VendorAddress__",
																			"VendorAddress__": "LabelVendorAddress__",
																			"LabelVendorNumber__": "VendorNumber__",
																			"VendorNumber__": "LabelVendorNumber__"
																		},
																		"version": 0
																	},
																	"stamp": 24,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"LabelVendorName__": {
																						"line": 2,
																						"column": 1
																					},
																					"VendorName__": {
																						"line": 2,
																						"column": 2
																					},
																					"LabelVendorAddress__": {
																						"line": 3,
																						"column": 1
																					},
																					"VendorAddress__": {
																						"line": 3,
																						"column": 2
																					},
																					"LabelVendorNumber__": {
																						"line": 1,
																						"column": 1
																					},
																					"VendorNumber__": {
																						"line": 1,
																						"column": 2
																					}
																				},
																				"lines": 3,
																				"columns": 2,
																				"colspans": [
																					[],
																					[],
																					[]
																				],
																				"version": 0
																			},
																			"stamp": 25,
																			"*": {
																				"VendorAddress__": {
																					"type": "LongText",
																					"data": [
																						"VendorAddress__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"minNbLines": 3,
																						"label": "_VendorAddress",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"browsable": false
																					},
																					"stamp": 26
																				},
																				"LabelVendorNumber__": {
																					"type": "Label",
																					"data": [
																						"VendorNumber__"
																					],
																					"options": {
																						"label": "_VendorNumber",
																						"version": 0
																					},
																					"stamp": 27
																				},
																				"LabelVendorName__": {
																					"type": "Label",
																					"data": [
																						"VendorName__"
																					],
																					"options": {
																						"label": "_VendorName",
																						"version": 0
																					},
																					"stamp": 28
																				},
																				"LabelVendorAddress__": {
																					"type": "Label",
																					"data": [
																						"VendorAddress__"
																					],
																					"options": {
																						"label": "_VendorAddress",
																						"version": 0
																					},
																					"stamp": 29
																				},
																				"VendorName__": {
																					"type": "DatabaseComboBox",
																					"data": [
																						"VendorName__"
																					],
																					"options": {
																						"version": 1,
																						"autoCompleteSearchMode": "startswith",
																						"searchMode": "contains",
																						"label": "_VendorName",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"minNbLines": 1,
																						"maxNbLines": 1,
																						"fTSmaxRecords": "20",
																						"browsable": true
																					},
																					"stamp": 30
																				},
																				"VendorNumber__": {
																					"type": "DatabaseComboBox",
																					"data": [
																						"VendorNumber__"
																					],
																					"options": {
																						"version": 1,
																						"autoCompleteSearchMode": "startswith",
																						"searchMode": "contains",
																						"label": "_VendorNumber",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"minNbLines": 1,
																						"maxNbLines": 1,
																						"fTSmaxRecords": "20",
																						"browsable": true
																					},
																					"stamp": 31
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-right-5": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 32,
													"*": {
														"Information": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": true,
																"hideTitleBar": false,
																"hideTitle": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "_Information",
																"hidden": false,
																"leftImageURL": "",
																"removeMargins": false,
																"elementsAlignment": "left",
																"version": 0,
																"labelLength": "30%"
															},
															"stamp": 33,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"CreationDate__": "LabelCreationDate__",
																			"LabelCreationDate__": "CreationDate__",
																			"EffectiveDate__": "LabelEffectiveDate__",
																			"LabelEffectiveDate__": "EffectiveDate__",
																			"TotalNetAmount__": "LabelTotalNetAmount__",
																			"LabelTotalNetAmount__": "TotalNetAmount__",
																			"Currency__": "LabelCurrency__",
																			"LabelCurrency__": "Currency__",
																			"PaymentMethodDescription__": "LabelPaymentMethodDescription__",
																			"LabelPaymentMethodDescription__": "PaymentMethodDescription__",
																			"PaymentMethodCode__": "LabelPaymentMethodCode__",
																			"LabelPaymentMethodCode__": "PaymentMethodCode__",
																			"PaymentDate__": "LabelPaymentDate__",
																			"LabelPaymentDate__": "PaymentDate__",
																			"PaymentReference__": "LabelPaymentReference__",
																			"LabelPaymentReference__": "PaymentReference__"
																		},
																		"version": 0
																	},
																	"stamp": 34,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"TotalNetAmount__": {
																						"line": 5,
																						"column": 2
																					},
																					"LabelTotalNetAmount__": {
																						"line": 5,
																						"column": 1
																					},
																					"Currency__": {
																						"line": 6,
																						"column": 2
																					},
																					"LabelCurrency__": {
																						"line": 6,
																						"column": 1
																					},
																					"PaymentMethodDescription__": {
																						"line": 3,
																						"column": 2
																					},
																					"LabelPaymentMethodDescription__": {
																						"line": 3,
																						"column": 1
																					},
																					"PaymentMethodCode__": {
																						"line": 2,
																						"column": 2
																					},
																					"LabelPaymentMethodCode__": {
																						"line": 2,
																						"column": 1
																					},
																					"PaymentDate__": {
																						"line": 4,
																						"column": 2
																					},
																					"LabelPaymentDate__": {
																						"line": 4,
																						"column": 1
																					},
																					"PaymentReference__": {
																						"line": 1,
																						"column": 2
																					},
																					"LabelPaymentReference__": {
																						"line": 1,
																						"column": 1
																					}
																				},
																				"lines": 6,
																				"columns": 2,
																				"colspans": [
																					[],
																					[],
																					[],
																					[],
																					[],
																					[]
																				],
																				"version": 0
																			},
																			"stamp": 35,
																			"*": {
																				"LabelPaymentReference__": {
																					"type": "Label",
																					"data": [
																						"PaymentReference__"
																					],
																					"options": {
																						"label": "_PaymentReference"
																					},
																					"stamp": 36
																				},
																				"PaymentReference__": {
																					"type": "ShortText",
																					"data": [
																						"PaymentReference__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"label": "_PaymentReference",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"browsable": false
																					},
																					"stamp": 37
																				},
																				"LabelPaymentMethodCode__": {
																					"type": "Label",
																					"data": [
																						"PaymentMethodCode__"
																					],
																					"options": {
																						"label": "_PaymentMethodCode",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 38
																				},
																				"PaymentMethodCode__": {
																					"type": "ShortText",
																					"data": [
																						"PaymentMethodCode__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"label": "_PaymentMethodCode",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"hidden": true,
																						"browsable": false
																					},
																					"stamp": 39
																				},
																				"LabelPaymentMethodDescription__": {
																					"type": "Label",
																					"data": [
																						"PaymentMethodDescription__"
																					],
																					"options": {
																						"label": "_PaymentMethodDescription",
																						"version": 0
																					},
																					"stamp": 40
																				},
																				"PaymentMethodDescription__": {
																					"type": "ShortText",
																					"data": [
																						"PaymentMethodDescription__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"label": "_PaymentMethodDescription",
																						"activable": true,
																						"width": 230
																					},
																					"stamp": 41
																				},
																				"LabelPaymentDate__": {
																					"type": "Label",
																					"data": [
																						"PaymentDate__"
																					],
																					"options": {
																						"label": "_PaymentDate",
																						"version": 0
																					},
																					"stamp": 42
																				},
																				"PaymentDate__": {
																					"type": "DateTime",
																					"data": [
																						"PaymentDate__"
																					],
																					"options": {
																						"displayLongFormat": false,
																						"label": "_PaymentDate",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"version": 0
																					},
																					"stamp": 43
																				},
																				"LabelTotalNetAmount__": {
																					"type": "Label",
																					"data": [
																						"TotalNetAmount__"
																					],
																					"options": {
																						"label": "_TotalNetAmount",
																						"version": 0
																					},
																					"stamp": 44
																				},
																				"TotalNetAmount__": {
																					"type": "Decimal",
																					"data": [
																						"TotalNetAmount__"
																					],
																					"options": {
																						"version": 2,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"integer": false,
																						"label": "_TotalNetAmount",
																						"precision_internal": 2,
																						"precision_current": 2,
																						"activable": true,
																						"width": 230
																					},
																					"stamp": 45
																				},
																				"LabelCurrency__": {
																					"type": "Label",
																					"data": [
																						"Currency__"
																					],
																					"options": {
																						"label": "_Currency",
																						"version": 0
																					},
																					"stamp": 46
																				},
																				"Currency__": {
																					"type": "ShortText",
																					"data": [
																						"Currency__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"label": "_Currency",
																						"activable": true,
																						"width": 230
																					},
																					"stamp": 47
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-right-7": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 48,
													"*": {
														"Line_items": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": false,
																"hideTitleBar": false,
																"hideTitle": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "_Line items",
																"hidden": false,
																"leftImageURL": "",
																"removeMargins": false,
																"elementsAlignment": "left",
																"version": 0,
																"labelLength": "30%"
															},
															"stamp": 49,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {},
																		"version": 0
																	},
																	"stamp": 50,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"Table__": {
																						"line": 1,
																						"column": 1
																					}
																				},
																				"lines": 1,
																				"columns": 2,
																				"colspans": [
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					]
																				],
																				"version": 0
																			},
																			"stamp": 51,
																			"*": {
																				"Table__": {
																					"type": "Table",
																					"data": [
																						"Table__"
																					],
																					"options": {
																						"version": 5,
																						"lines": 10,
																						"columns": 4,
																						"displayEmptyLines": false,
																						"alwaysOneLine": true,
																						"menuOptions": {
																							"disableInsertLine": false,
																							"disableAddLine": false,
																							"hideTopNavigation": false,
																							"hideBottomNavigation": false,
																							"hideTableRowMenu": false,
																							"hideTableRowDelete": true,
																							"hideTableRowAdd": true,
																							"hideTableRowDuplicate": true,
																							"addButtonShouldInsert": false,
																							"hideTableRowButtonDuplicate": true
																						},
																						"item": {
																							"display": null
																						},
																						"group": {
																							"name": "GDR\nPage"
																						},
																						"label": "_Invoices"
																					},
																					"stamp": 52,
																					"*": {
																						"Actions": {
																							"type": "$TableActions",
																							"subtype": "Actions",
																							"options": {
																								"version": 0
																							},
																							"stamp": 53,
																							"*": {
																								"Top": {
																									"type": "$TableActions",
																									"subtype": "Top",
																									"options": {
																										"version": 0
																									},
																									"stamp": 54
																								},
																								"Bottom": {
																									"type": "$TableActions",
																									"subtype": "Top",
																									"options": {
																										"version": 0
																									},
																									"stamp": 55
																								}
																							}
																						},
																						"Header": {
																							"type": "$TableRow",
																							"subtype": "Header",
																							"options": {
																								"version": 0
																							},
																							"stamp": 56,
																							"data": [
																								null
																							],
																							"*": {
																								"Cell1": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 57,
																									"data": [],
																									"*": {
																										"DocumentNumber__": {
																											"type": "Label",
																											"data": [
																												"DocumentNumber__"
																											],
																											"options": {
																												"label": "_DocumentNumber",
																												"version": 0
																											},
																											"stamp": 58,
																											"position": [
																												"_ShortText"
																											]
																										}
																									}
																								},
																								"Cell2": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 59,
																									"data": [],
																									"*": {
																										"InvoiceNumber__": {
																											"type": "Label",
																											"data": [
																												"InvoiceNumber__"
																											],
																											"options": {
																												"label": "_InvoiceNumber",
																												"version": 0
																											},
																											"stamp": 60,
																											"position": [
																												"_ShortText"
																											]
																										}
																									}
																								},
																								"Cell3": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 61,
																									"data": [],
																									"*": {
																										"InvoiceDate__": {
																											"type": "Label",
																											"data": [
																												"InvoiceDate__"
																											],
																											"options": {
																												"label": "_InvoiceDate",
																												"version": 0
																											},
																											"stamp": 62,
																											"position": [
																												"_DateTime"
																											]
																										}
																									}
																								},
																								"Cell4": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 63,
																									"data": [],
																									"*": {
																										"NetAmount__": {
																											"type": "Label",
																											"data": [
																												"NetAmount__"
																											],
																											"options": {
																												"label": "_NetAmount",
																												"version": 0
																											},
																											"stamp": 64,
																											"position": [
																												"_Decimal"
																											]
																										}
																									}
																								}
																							}
																						},
																						"Clipboard": {
																							"type": "$TableRow",
																							"subtype": "Clipboard",
																							"options": {
																								"version": 0
																							},
																							"stamp": 65,
																							"data": [
																								null
																							],
																							"*": {
																								"Cell1": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 66,
																									"data": [],
																									"*": {
																										"DocumentNumber__": {
																											"type": "ShortText",
																											"data": [
																												"DocumentNumber__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"label": "_DocumentNumber",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"readonly": true,
																												"browsable": false
																											},
																											"stamp": 67,
																											"position": [
																												"_ShortText"
																											]
																										}
																									}
																								},
																								"Cell2": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 68,
																									"data": [],
																									"*": {
																										"InvoiceNumber__": {
																											"type": "ShortText",
																											"data": [
																												"InvoiceNumber__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"label": "_InvoiceNumber",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"readonly": true,
																												"browsable": false
																											},
																											"stamp": 69,
																											"position": [
																												"_ShortText"
																											]
																										}
																									}
																								},
																								"Cell3": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 70,
																									"data": [],
																									"*": {
																										"InvoiceDate__": {
																											"type": "DateTime",
																											"data": [
																												"InvoiceDate__"
																											],
																											"options": {
																												"displayLongFormat": false,
																												"label": "_InvoiceDate",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"readonly": true,
																												"version": 0
																											},
																											"stamp": 71,
																											"position": [
																												"_DateTime"
																											]
																										}
																									}
																								},
																								"Cell4": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 72,
																									"data": [],
																									"*": {
																										"NetAmount__": {
																											"type": "Decimal",
																											"data": [
																												"NetAmount__"
																											],
																											"options": {
																												"version": 2,
																												"textSize": "S",
																												"textAlignment": "right",
																												"textStyle": "default",
																												"textColor": "default",
																												"integer": false,
																												"label": "_NetAmount",
																												"precision_internal": 2,
																												"precision_current": 2,
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"enablePlusMinus": false,
																												"readonly": true
																											},
																											"stamp": 73,
																											"position": [
																												"_Decimal"
																											]
																										}
																									}
																								}
																							}
																						}
																					}
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-right-9": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 74,
													"*": {
														"NextProcess": {
															"type": "PanelNextProcess",
															"options": {
																"hidden": true,
																"version": 0,
																"border": {},
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": false,
																"hideTitleBar": false,
																"hideTitle": false,
																"label": "_Next Process"
															},
															"stamp": 75,
															"data": []
														}
													}
												},
												"form-content-right-11": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 76,
													"*": {
														"SystemData": {
															"type": "PanelSystemData",
															"options": {
																"multicolumn": false,
																"version": 0,
																"border": {
																	"maximized": false
																},
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": false,
																"hideTitleBar": false,
																"hideTitle": false,
																"label": "_System data",
																"hidden": true
															},
															"stamp": 77,
															"data": []
														}
													}
												}
											},
											"stamp": 78,
											"data": []
										}
									},
									"stamp": 79,
									"data": []
								}
							},
							"stamp": 80,
							"data": []
						}
					},
					"stamp": 81,
					"data": []
				},
				"form-footer": {
					"type": "FlexibleFormFooter",
					"options": {
						"splitterType": "simple-h",
						"version": 0,
						"maxwidth": ""
					},
					"*": {
						"Save": {
							"type": "SubmitButton",
							"options": {
								"label": "_Save",
								"action": "save",
								"submit": true,
								"version": 0,
								"textPosition": "text-right",
								"textSize": "MS",
								"textStyle": "default",
								"textColor": "color1",
								"nextprocess": {
									"processName": "AP - Application Settings Wizard",
									"attachmentsMode": "all",
									"willBeChild": true,
									"returnToOriginalUrl": false
								},
								"urlImageOverlay": "",
								"url": "",
								"position": null,
								"hidden": true
							},
							"stamp": 82,
							"data": []
						},
						"Approve": {
							"type": "SubmitButton",
							"options": {
								"label": "_Approve",
								"action": "approve",
								"submit": true,
								"version": 0,
								"textPosition": "text-right",
								"textSize": "MS",
								"textStyle": "default",
								"textColor": "color1",
								"nextprocess": {
									"processName": "AP - Application Settings Wizard",
									"attachmentsMode": "all",
									"willBeChild": true,
									"returnToOriginalUrl": false
								},
								"urlImageOverlay": "",
								"url": "",
								"position": null,
								"hidden": true
							},
							"stamp": 83,
							"data": []
						},
						"Reject": {
							"type": "SubmitButton",
							"options": {
								"label": "_Reject",
								"action": "reject",
								"submit": true,
								"version": 0,
								"textPosition": "text-right",
								"textSize": "MS",
								"textStyle": "default",
								"textColor": "color1",
								"nextprocess": {
									"processName": "AP - Application Settings Wizard",
									"attachmentsMode": "all",
									"willBeChild": true,
									"returnToOriginalUrl": false
								},
								"urlImageOverlay": "",
								"url": "",
								"position": null,
								"hidden": true
							},
							"stamp": 84,
							"data": []
						},
						"Reprocess": {
							"type": "SubmitButton",
							"options": {
								"label": "_Reprocess",
								"action": "reprocess",
								"submit": true,
								"version": 0,
								"textPosition": "text-right",
								"textSize": "MS",
								"textStyle": "default",
								"textColor": "color1",
								"nextprocess": {
									"processName": "AP - Application Settings Wizard",
									"attachmentsMode": "all",
									"willBeChild": true,
									"returnToOriginalUrl": false
								},
								"urlImageOverlay": "",
								"url": "",
								"position": null,
								"hidden": true
							},
							"stamp": 85,
							"data": []
						},
						"Close": {
							"type": "SubmitButton",
							"options": {
								"label": "_Close",
								"action": "cancel",
								"submit": true,
								"version": 0
							},
							"stamp": 86,
							"data": []
						},
						"DownloadCrystalReportsDataFile": {
							"type": "SubmitButton",
							"data": false,
							"options": {
								"label": "DownloadCrystalReportsDataFile",
								"textPosition": "text-right",
								"textSize": "MS",
								"textStyle": "default",
								"textColor": "color1",
								"nextprocess": {
									"processName": "AP - Application Settings Wizard",
									"attachmentsMode": "all",
									"willBeChild": true,
									"returnToOriginalUrl": false
								},
								"urlImageOverlay": "",
								"url": "",
								"position": null,
								"hidden": true,
								"action": "none",
								"version": 0
							},
							"stamp": 87
						}
					},
					"stamp": 88,
					"data": []
				}
			},
			"stamp": 89,
			"data": []
		}
	},
	"stamps": 89,
	"data": []
}