///#GLOBALS Lib
var PaymentData = JSON.parse(Variable.GetValueAsString("ExtractedData"));
function AttachRA() {
    Log.Time("AttachRA");
    if (!Lib.Purchasing.RA.Server.AttachRA()) {
        if (!Lib.CommonDialog.NextAlert.GetNextAlert()) {
            Lib.CommonDialog.NextAlert.Define("_Remittance advice attaching error", "_Error attaching remittance advice", { isError: true });
        }
        Log.TimeEnd("AttachRA");
        return false;
    }
    Log.TimeEnd("AttachRA");
    return true;
}
function SendEmailNotification(subject, template) {
    var customTags = {
        PaymentReference__: PaymentData.ExtractedData.PaymentId,
        Currency__: PaymentData.ExtractedData.Currency,
        VendorNumber__: PaymentData.ExtractedData.VendorId,
        CompanyCode__: PaymentData.ExtractedData.VendorCompanyCode
    };
    var vendorUser = Lib.AP.VendorPortal.GetVendorUser(customTags);
    if (vendorUser) {
        Lib.AP.VendorPortal.AddFormattedValues(customTags, vendorUser);
        customTags.CompanyName__ = Data.GetValue("PayerCompany__ ");
        customTags.PortalUrl = Data.GetValue("ValidationUrl");
        customTags.TotalNetAmount__ = vendorUser.GetFormattedNumber(PaymentData.ExtractedData.TotalNetAmount);
        customTags.PaymentDate__ = Sys.Helpers.Date.ToLocaleDate(new Date(PaymentData.ExtractedData.PaymentDate), vendorUser.GetValue("culture"));
        var dst_email = vendorUser.GetVars().GetValue_String("EmailAddress", 0);
        if (dst_email) {
            var emailOptions = {
                "subject": Language.Translate(subject),
                "template": template,
                "customTags": customTags
            };
            var doSendNotif = Sys.Helpers.TryCallFunction("Lib.AP.Customization.VendorPortal.OnSendVendorNotification", emailOptions);
            if (doSendNotif !== false) {
                var email = Sys.EmailNotification.CreateEmailWithUser(vendorUser, dst_email, emailOptions.subject, emailOptions.template, emailOptions.customTags, true);
                if (email) {
                    Sys.EmailNotification.SendEmail(email);
                }
            }
        }
    }
}
Data.SetValue("PaymentDate__", PaymentData.ExtractedData.PaymentDate);
Lib.Purchasing.RA.ComputeInvoicesTable();
AttachRA();
SendEmailNotification("_RemittanceAdviceAvailable", "AP-Vendor_RemittanceAdvicePublished.htm");
