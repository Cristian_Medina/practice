{
	"*": {
		"GDRData": {
			"type": "GDRData",
			"options": {
				"version": 0
			},
			"stamp": 1,
			"data": []
		},
		"OCRParams": {
			"type": "OCRParams",
			"options": {
				"version": 0
			},
			"stamp": 2,
			"data": []
		},
		"Screen": {
			"type": "FlexibleFormLayout",
			"options": {
				"splitterType": "complex",
				"version": 0,
				"maxwidth": "",
				"backColor": "text-backgroundcolor-color7",
				"style": "FlexibleFormLight"
			},
			"*": {
				"form-header": {
					"type": "FlexibleFormHeader",
					"options": {
						"version": 0,
						"maxwidth": ""
					},
					"stamp": 4,
					"data": []
				},
				"form-center": {
					"type": "FlexibleFormCenter",
					"options": {
						"version": 0,
						"maxwidth": ""
					},
					"*": {
						"form-content": {
							"type": "FlexibleFormContent",
							"options": {
								"version": 0
							},
							"*": {
								"form-content-top": {
									"type": "FlexibleFormSplitter",
									"options": {
										"autosize": true,
										"version": 1,
										"box": {
											"top": 0,
											"left": 0,
											"width": 100,
											"height": 25
										},
										"hidden": true
									},
									"*": {},
									"stamp": 7,
									"data": []
								},
								"form-content-bottom": {
									"type": "FlexibleFormSplitter",
									"options": {
										"version": 1,
										"box": {
											"top": 0,
											"left": 0,
											"width": 100,
											"height": 100
										},
										"hidden": false,
										"hideSeparator": true
									},
									"*": {
										"form-content-left": {
											"type": "FlexibleFormSplitter",
											"options": {
												"version": 1,
												"box": {
													"top": "0%",
													"left": "0%",
													"width": "44%",
													"height": null
												},
												"hidden": false
											},
											"*": {
												"form-content-left-1": {
													"type": "FlexibleFormLine",
													"options": {
														"version": 0
													},
													"*": {
														"PreviewPanel": {
															"type": "PanelPreview",
															"options": {
																"border": {},
																"version": 0,
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": false,
																"hideTitleBar": false,
																"label": "_Document Preview",
																"hideTitle": false
															},
															"*": {
																"Preview": {
																	"data": [
																		"previewdata"
																	],
																	"type": "Preview",
																	"options": {
																		"version": 0
																	},
																	"stamp": 24
																}
															},
															"stamp": 23,
															"data": []
														}
													},
													"stamp": 13,
													"data": []
												}
											},
											"stamp": 12,
											"data": []
										},
										"form-content-right": {
											"type": "FlexibleFormSplitter",
											"options": {
												"version": 1,
												"box": {
													"top": 0,
													"left": 44,
													"width": 56,
													"height": 100
												},
												"hidden": false
											},
											"*": {
												"form-content-right-5": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 206,
													"*": {
														"TitlePane": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": "200",
																"iconURL": "",
																"TitleFontSize": "L",
																"hideActionButtons": true,
																"hideTitleBar": true,
																"backgroundcolor": "color7",
																"labelsAlignment": "right",
																"label": "_TitlePane",
																"leftImageURL": "",
																"version": 0,
																"hideTitle": false
															},
															"stamp": 74,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"CustomerInvoiceStatus__": "LabelCustomerInvoiceStatus__",
																			"LabelCustomerInvoiceStatus__": "CustomerInvoiceStatus__"
																		},
																		"version": 0
																	},
																	"stamp": 75,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"Title__": {
																						"line": 1,
																						"column": 1
																					},
																					"Spacer_line__": {
																						"line": 2,
																						"column": 1
																					},
																					"LabelCustomerInvoiceStatus__": {
																						"line": 3,
																						"column": 1
																					},
																					"CustomerInvoiceStatus__": {
																						"line": 3,
																						"column": 2
																					}
																				},
																				"lines": 3,
																				"columns": 2,
																				"colspans": [
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[]
																				],
																				"version": 0
																			},
																			"stamp": 76,
																			"*": {
																				"Title__": {
																					"type": "HTML",
																					"data": false,
																					"options": {
																						"label": "InvoiceTitle__",
																						"width": 897,
																						"htmlContent": "654\n",
																						"version": 0
																					},
																					"stamp": 157
																				},
																				"Spacer_line__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "",
																						"width": "",
																						"color": "default",
																						"label": "Spacer line",
																						"version": 0
																					},
																					"stamp": 109
																				},
																				"CustomerInvoiceStatus__": {
																					"type": "ComboBox",
																					"data": [
																						"CustomerInvoiceStatus__"
																					],
																					"options": {
																						"possibleValues": {
																							"0": "To send",
																							"1": "Draft",
																							"2": "Awaiting reception",
																							"3": "Awaiting payment approval",
																							"4": "Awaiting payment transaction",
																							"5": "Paid",
																							"6": "Rejected"
																						},
																						"keyValueMode": true,
																						"possibleKeys": {
																							"0": "To send",
																							"1": "Draft",
																							"2": "Awaiting reception",
																							"3": "Awaiting payment approval",
																							"4": "Awaiting payment transaction",
																							"5": "Paid",
																							"6": "Rejected"
																						},
																						"label": "_CustomerInvoiceStatus",
																						"activable": true,
																						"width": "230",
																						"readonly": false,
																						"version": 0
																					},
																					"stamp": 218
																				},
																				"LabelCustomerInvoiceStatus__": {
																					"type": "Label",
																					"data": [
																						"CustomerInvoiceStatus__"
																					],
																					"options": {
																						"label": "_CustomerInvoiceStatus",
																						"version": 0
																					},
																					"stamp": 219
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-right-2": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 196,
													"*": {
														"Buttons": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "L",
																"hideActionButtons": true,
																"hideTitleBar": true,
																"backgroundcolor": "default",
																"labelsAlignment": "center",
																"label": "Buttons",
																"leftImageURL": "",
																"version": 0,
																"hideTitle": false
															},
															"stamp": 189,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"Exit__": "LabelExit__",
																			"LabelExit__": "Exit__",
																			"Submit__": "LabelSubmit__",
																			"LabelSubmit__": "Submit__",
																			"RefreshPreview__": "LabelRefreshPreview__",
																			"LabelRefreshPreview__": "RefreshPreview__",
																			"ProposeEarlyPayment__": "LabelProposeEarlyPayment__",
																			"LabelProposeEarlyPayment__": "ProposeEarlyPayment__"
																		},
																		"version": 0
																	},
																	"stamp": 190,
																	"*": {
																		"Grid": {
																			"type": "HorizontalLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"RefreshPreview__": {
																						"line": 1,
																						"column": 2
																					},
																					"LabelRefreshPreview__": {
																						"line": 1,
																						"column": 1
																					},
																					"ProposeEarlyPayment__": {
																						"line": 2,
																						"column": 2
																					},
																					"LabelProposeEarlyPayment__": {
																						"line": 2,
																						"column": 1
																					},
																					"Submit__": {
																						"line": 3,
																						"column": 2
																					},
																					"LabelSubmit__": {
																						"line": 3,
																						"column": 1
																					},
																					"Exit__": {
																						"line": 4,
																						"column": 2
																					},
																					"LabelExit__": {
																						"line": 4,
																						"column": 1
																					},
																					"Spacer_line2__": {
																						"line": 5,
																						"column": 1
																					}
																				},
																				"lines": 5,
																				"colspans": [
																					[],
																					[],
																					[],
																					[],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					]
																				],
																				"version": 0
																			},
																			"stamp": 192,
																			"*": {
																				"LabelRefreshPreview__": {
																					"type": "Label",
																					"data": false,
																					"options": {
																						"label": "",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 366
																				},
																				"RefreshPreview__": {
																					"type": "FormButton",
																					"data": false,
																					"options": {
																						"buttonLabel": "_Refresh Preview",
																						"label": "",
																						"textPosition": "text-right",
																						"textSize": "MS",
																						"textStyle": "default",
																						"textColor": "default",
																						"urlImage": "AP_RefreshPreview.png",
																						"urlImageOverlay": "",
																						"style": 4,
																						"action": "approve",
																						"url": "",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 367
																				},
																				"LabelProposeEarlyPayment__": {
																					"type": "Label",
																					"data": false,
																					"options": {
																						"label": "",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 403
																				},
																				"ProposeEarlyPayment__": {
																					"type": "FormButton",
																					"data": false,
																					"options": {
																						"buttonLabel": "_Propose Early Payment",
																						"label": "",
																						"textPosition": "text-right",
																						"textSize": "MS",
																						"textStyle": "default",
																						"textColor": "default",
																						"urlImage": "BoutonSmall_generate_green.png",
																						"urlImageOverlay": "",
																						"style": 4,
																						"action": "none",
																						"url": "",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 402
																				},
																				"LabelSubmit__": {
																					"type": "Label",
																					"data": false,
																					"options": {
																						"label": "",
																						"version": 0
																					},
																					"stamp": 199
																				},
																				"Submit__": {
																					"type": "FormButton",
																					"data": false,
																					"options": {
																						"buttonLabel": "_Submit",
																						"label": "",
																						"urlImage": "AP_check_button.png",
																						"style": 4,
																						"version": 0
																					},
																					"stamp": 200
																				},
																				"LabelExit__": {
																					"type": "Label",
																					"data": false,
																					"options": {
																						"label": "",
																						"version": 0
																					},
																					"stamp": 201
																				},
																				"Exit__": {
																					"type": "FormButton",
																					"data": false,
																					"options": {
																						"buttonLabel": "_Quit",
																						"label": "",
																						"urlImage": "AP_exit_button.png",
																						"style": 4,
																						"version": 0
																					},
																					"stamp": 202
																				},
																				"Spacer_line2__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "",
																						"width": "",
																						"color": "default",
																						"label": "Spacer line2",
																						"version": 0
																					},
																					"stamp": 209
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-right-3": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 95,
													"*": {
														"Invoice_details": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": "200",
																"iconURL": "AP_HeaderDataPanel.png",
																"TitleFontSize": "L",
																"hideActionButtons": true,
																"hideTitleBar": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "_Invoice details",
																"leftImageURL": "",
																"version": 0,
																"hideTitle": false,
																"removeMargins": false,
																"sameHeightAsSiblings": true
															},
															"stamp": 42,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"Invoice_number__": "LabelInvoice_number__",
																			"LabelInvoice_number__": "Invoice_number__",
																			"Invoice_date__": "LabelInvoice_date__",
																			"LabelInvoice_date__": "Invoice_date__",
																			"Currency__": "LabelCurrency__",
																			"LabelCurrency__": "Currency__",
																			"Due_date__": "LabelDue_date__",
																			"LabelDue_date__": "Due_date__",
																			"Net_amount__": "LabelNet_amount__",
																			"LabelNet_amount__": "Net_amount__",
																			"Company__": "LabelCompany__",
																			"LabelCompany__": "Company__",
																			"PortalRuidEx__": "LabelPortalRuidEx__",
																			"LabelPortalRuidEx__": "PortalRuidEx__",
																			"ReceptionMethod__": "LabelReceptionMethod__",
																			"LabelReceptionMethod__": "ReceptionMethod__",
																			"VIRuidEx__": "LabelVIRuidEx__",
																			"LabelVIRuidEx__": "VIRuidEx__",
																			"Invoice_amount__": "LabelInvoice_amount__",
																			"LabelInvoice_amount__": "Invoice_amount__",
																			"LabelRejectReason__": "RejectReason__",
																			"RejectReason__": "LabelRejectReason__",
																			"Order_number__": "LabelOrder_number__",
																			"LabelOrder_number__": "Order_number__",
																			"Tax_amount__": "LabelTax_amount__",
																			"LabelTax_amount__": "Tax_amount__",
																			"Source_RuidEx__": "LabelSource_RuidEx__",
																			"LabelSource_RuidEx__": "Source_RuidEx__",
																			"FlipPO_RuidEx__": "LabelFlipPO_RuidEx__",
																			"LabelFlipPO_RuidEx__": "FlipPO_RuidEx__",
																			"PaymentTerms__": "LabelPaymentTerms__",
																			"LabelPaymentTerms__": "PaymentTerms__"
																		},
																		"version": 0
																	},
																	"stamp": 43,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"Invoice_number__": {
																						"line": 3,
																						"column": 2
																					},
																					"LabelInvoice_number__": {
																						"line": 3,
																						"column": 1
																					},
																					"Invoice_date__": {
																						"line": 4,
																						"column": 2
																					},
																					"LabelInvoice_date__": {
																						"line": 4,
																						"column": 1
																					},
																					"Currency__": {
																						"line": 8,
																						"column": 2
																					},
																					"LabelCurrency__": {
																						"line": 8,
																						"column": 1
																					},
																					"Due_date__": {
																						"line": 9,
																						"column": 2
																					},
																					"LabelDue_date__": {
																						"line": 9,
																						"column": 1
																					},
																					"Net_amount__": {
																						"line": 7,
																						"column": 2
																					},
																					"LabelNet_amount__": {
																						"line": 7,
																						"column": 1
																					},
																					"Company__": {
																						"line": 1,
																						"column": 2
																					},
																					"LabelCompany__": {
																						"line": 1,
																						"column": 1
																					},
																					"PortalRuidEx__": {
																						"line": 10,
																						"column": 2
																					},
																					"LabelPortalRuidEx__": {
																						"line": 10,
																						"column": 1
																					},
																					"ReceptionMethod__": {
																						"line": 11,
																						"column": 2
																					},
																					"LabelReceptionMethod__": {
																						"line": 11,
																						"column": 1
																					},
																					"VIRuidEx__": {
																						"line": 12,
																						"column": 2
																					},
																					"LabelVIRuidEx__": {
																						"line": 12,
																						"column": 1
																					},
																					"Invoice_amount__": {
																						"line": 5,
																						"column": 2
																					},
																					"LabelInvoice_amount__": {
																						"line": 5,
																						"column": 1
																					},
																					"LabelRejectReason__": {
																						"line": 13,
																						"column": 1
																					},
																					"RejectReason__": {
																						"line": 13,
																						"column": 2
																					},
																					"Order_number__": {
																						"line": 2,
																						"column": 2
																					},
																					"LabelOrder_number__": {
																						"line": 2,
																						"column": 1
																					},
																					"Tax_amount__": {
																						"line": 6,
																						"column": 2
																					},
																					"LabelTax_amount__": {
																						"line": 6,
																						"column": 1
																					},
																					"Source_RuidEx__": {
																						"line": 14,
																						"column": 2
																					},
																					"LabelSource_RuidEx__": {
																						"line": 14,
																						"column": 1
																					},
																					"FlipPO_RuidEx__": {
																						"line": 15,
																						"column": 2
																					},
																					"LabelFlipPO_RuidEx__": {
																						"line": 15,
																						"column": 1
																					},
																					"PaymentTerms__": {
																						"line": 16,
																						"column": 2
																					},
																					"LabelPaymentTerms__": {
																						"line": 16,
																						"column": 1
																					},
																					"Spacer__": {
																						"line": 17,
																						"column": 1
																					}
																				},
																				"lines": 17,
																				"columns": 2,
																				"colspans": [
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					]
																				],
																				"version": 0
																			},
																			"stamp": 44,
																			"*": {
																				"RejectReason__": {
																					"type": "ComboBox",
																					"data": [
																						"RejectReason__"
																					],
																					"options": {
																						"possibleValues": {
																							"0": "",
																							"1": "Document already processed",
																							"2": "Document is not an invoice",
																							"3": "Invoice is not compliant",
																							"4": "Unreadable invoice / bad quality scan",
																							"5": "Invalid signature",
																							"6": "Other reason"
																						},
																						"version": 1,
																						"keyValueMode": true,
																						"possibleKeys": {
																							"0": "-- none --",
																							"1": "Document already processed",
																							"2": "Document is not an invoice",
																							"3": "Invoice is not compliant",
																							"4": "Unreadable invoice / bad quality scan",
																							"5": "Invalid signature",
																							"6": "Other reason"
																						},
																						"label": "_RejectReason",
																						"activable": true,
																						"width": "230",
																						"readonly": true,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format"
																					},
																					"stamp": 273
																				},
																				"LabelRejectReason__": {
																					"type": "Label",
																					"data": [
																						"RejectReason__"
																					],
																					"options": {
																						"label": "_RejectReason",
																						"version": 0
																					},
																					"stamp": 272
																				},
																				"LabelOrder_number__": {
																					"type": "Label",
																					"data": [
																						"Order_number__"
																					],
																					"options": {
																						"label": "_Order number",
																						"version": 0
																					},
																					"stamp": 311
																				},
																				"Order_number__": {
																					"type": "ShortText",
																					"data": [
																						"Order_number__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"label": "_Order number",
																						"activable": true,
																						"width": "150",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"browsable": false,
																						"readonly": true
																					},
																					"stamp": 312
																				},
																				"LabelCompany__": {
																					"type": "Label",
																					"data": [
																						"Company__"
																					],
																					"options": {
																						"label": "_Company",
																						"version": 0
																					},
																					"stamp": 249
																				},
																				"Company__": {
																					"type": "ComboBox",
																					"data": [
																						"Company__"
																					],
																					"options": {
																						"possibleValues": {},
																						"keyValueMode": false,
																						"possibleKeys": {},
																						"label": "_Company",
																						"activable": true,
																						"width": "150",
																						"version": 0
																					},
																					"stamp": 250
																				},
																				"LabelInvoice_number__": {
																					"type": "Label",
																					"data": [
																						"Invoice_number__"
																					],
																					"options": {
																						"label": "_Invoice number",
																						"version": 0
																					},
																					"stamp": 63
																				},
																				"Invoice_number__": {
																					"type": "ShortText",
																					"data": [
																						"Invoice_number__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"label": "_Invoice number",
																						"activable": true,
																						"width": "150",
																						"browsable": false,
																						"version": 0,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format"
																					},
																					"stamp": 64
																				},
																				"LabelInvoice_date__": {
																					"type": "Label",
																					"data": [
																						"Invoice_date__"
																					],
																					"options": {
																						"label": "_Invoice date",
																						"version": 0
																					},
																					"stamp": 65
																				},
																				"Invoice_date__": {
																					"type": "DateTime",
																					"data": [
																						"Invoice_date__"
																					],
																					"options": {
																						"displayLongFormat": false,
																						"label": "_Invoice date",
																						"activable": true,
																						"width": "150",
																						"version": 0
																					},
																					"stamp": 66
																				},
																				"LabelTax_amount__": {
																					"type": "Label",
																					"data": [
																						"Tax_amount__"
																					],
																					"options": {
																						"label": "_Tax Amount",
																						"version": 0
																					},
																					"stamp": 313
																				},
																				"Tax_amount__": {
																					"type": "Decimal",
																					"data": [
																						"Tax_amount__"
																					],
																					"options": {
																						"version": 2,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"integer": false,
																						"label": "_Tax Amount",
																						"precision_internal": 2,
																						"precision_current": 2,
																						"activable": true,
																						"width": "150",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format"
																					},
																					"stamp": 314
																				},
																				"LabelNet_amount__": {
																					"type": "Label",
																					"data": [
																						"Net_amount__"
																					],
																					"options": {
																						"label": "_Net amount",
																						"version": 0
																					},
																					"stamp": 67
																				},
																				"Net_amount__": {
																					"type": "Decimal",
																					"data": [
																						"Net_amount__"
																					],
																					"options": {
																						"version": 1,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"integer": false,
																						"label": "_Net amount",
																						"precision_internal": 2,
																						"activable": true,
																						"width": "150",
																						"precision_current": 2
																					},
																					"stamp": 68
																				},
																				"LabelInvoice_amount__": {
																					"type": "Label",
																					"data": [
																						"Invoice_amount__"
																					],
																					"options": {
																						"label": "_Invoice amount",
																						"version": 0
																					},
																					"stamp": 257
																				},
																				"Invoice_amount__": {
																					"type": "Decimal",
																					"data": [
																						"Invoice_amount__"
																					],
																					"options": {
																						"version": 1,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"integer": false,
																						"label": "_Invoice amount",
																						"precision_internal": 2,
																						"precision_current": 2,
																						"activable": true,
																						"width": "150"
																					},
																					"stamp": 258
																				},
																				"LabelCurrency__": {
																					"type": "Label",
																					"data": [
																						"Currency__"
																					],
																					"options": {
																						"label": "_Currency",
																						"version": 0
																					},
																					"stamp": 69
																				},
																				"Currency__": {
																					"type": "ShortText",
																					"data": [
																						"Currency__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"label": "_Currency",
																						"activable": true,
																						"width": "150",
																						"version": 0,
																						"browsable": false
																					},
																					"stamp": 70
																				},
																				"LabelDue_date__": {
																					"type": "Label",
																					"data": [
																						"Due_date__"
																					],
																					"options": {
																						"label": "_Due date",
																						"version": 0
																					},
																					"stamp": 71
																				},
																				"Due_date__": {
																					"type": "DateTime",
																					"data": [
																						"Due_date__"
																					],
																					"options": {
																						"displayLongFormat": false,
																						"label": "_Due date",
																						"activable": true,
																						"width": "150",
																						"version": 0
																					},
																					"stamp": 72
																				},
																				"LabelPortalRuidEx__": {
																					"type": "Label",
																					"data": [
																						"PortalRuidEx__"
																					],
																					"options": {
																						"label": "_Portal Ruidex",
																						"version": 0
																					},
																					"stamp": 251
																				},
																				"PortalRuidEx__": {
																					"type": "ShortText",
																					"data": [
																						"PortalRuidEx__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"label": "_Portal Ruidex",
																						"activable": true,
																						"width": 230,
																						"version": 0,
																						"readonly": true,
																						"browsable": false,
																						"autocompletable": false
																					},
																					"stamp": 252
																				},
																				"LabelReceptionMethod__": {
																					"type": "Label",
																					"data": [
																						"ReceptionMethod__"
																					],
																					"options": {
																						"label": "_ReceptionMethod",
																						"version": 0
																					},
																					"stamp": 253
																				},
																				"ReceptionMethod__": {
																					"type": "ShortText",
																					"data": [
																						"ReceptionMethod__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"label": "_ReceptionMethod",
																						"activable": true,
																						"width": 230,
																						"defaultValue": "Vendor portal",
																						"browsable": false,
																						"version": 0,
																						"readonly": true,
																						"autocompletable": false
																					},
																					"stamp": 254
																				},
																				"LabelVIRuidEx__": {
																					"type": "Label",
																					"data": [
																						"VIRuidEx__"
																					],
																					"options": {
																						"label": "_VendorInvoiceID",
																						"version": 0
																					},
																					"stamp": 255
																				},
																				"VIRuidEx__": {
																					"type": "ShortText",
																					"data": [
																						"VIRuidEx__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"label": "_VendorInvoiceID",
																						"activable": true,
																						"width": 230,
																						"browsable": false,
																						"version": 0,
																						"readonly": true,
																						"autocompletable": false
																					},
																					"stamp": 256
																				},
																				"LabelSource_RuidEx__": {
																					"type": "Label",
																					"data": [
																						"Source_RuidEx__"
																					],
																					"options": {
																						"label": "_SourceRuidEx",
																						"hidden": true
																					},
																					"stamp": 322
																				},
																				"Source_RuidEx__": {
																					"type": "ShortText",
																					"data": [
																						"Source_RuidEx__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"label": "_SourceRuidEx",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"hidden": true,
																						"browsable": false,
																						"activable": true,
																						"width": 230,
																						"readonly": true,
																						"autocompletable": false
																					},
																					"stamp": 323
																				},
																				"LabelFlipPO_RuidEx__": {
																					"type": "Label",
																					"data": [
																						"FlipPO_RuidEx__"
																					],
																					"options": {
																						"label": "_FlipPO_RuidEx",
																						"hidden": true,
																						"version": 0
																					},
																					"stamp": 368
																				},
																				"FlipPO_RuidEx__": {
																					"type": "ShortText",
																					"data": [
																						"FlipPO_RuidEx__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"label": "_FlipPO_RuidEx",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"helpIconPosition": "Right",
																						"hidden": true,
																						"readonly": true,
																						"browsable": false
																					},
																					"stamp": 369
																				},
																				"LabelPaymentTerms__": {
																					"type": "Label",
																					"data": [
																						"PaymentTerms__"
																					],
																					"options": {
																						"label": "_PaymentTerms",
																						"version": 0
																					},
																					"stamp": 385
																				},
																				"PaymentTerms__": {
																					"type": "ShortText",
																					"data": [
																						"PaymentTerms__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"label": "_PaymentTerms",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"helpIconPosition": "Right",
																						"browsable": false,
																						"hidden": true,
																						"readonly": true
																					},
																					"stamp": 386
																				},
																				"Spacer__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "",
																						"width": "",
																						"color": "default",
																						"label": "_Spacer",
																						"version": 0
																					},
																					"stamp": 395
																				}
																			}
																		}
																	}
																}
															}
														},
														"ContactsInformation": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {
																	"collapsed": false
																},
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": true,
																"hideTitleBar": false,
																"hideTitle": true,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "_ContactsInformation",
																"hidden": false,
																"leftImageURL": "",
																"removeMargins": false,
																"elementsAlignment": "left",
																"sameHeightAsSiblings": true,
																"version": 0
															},
															"stamp": 315,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"Vendor__": "LabelVendor__",
																			"LabelVendor__": "Vendor__",
																			"BillTo__": "LabelBillTo__",
																			"LabelBillTo__": "BillTo__"
																		},
																		"version": 0
																	},
																	"stamp": 316,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"Vendor__": {
																						"line": 1,
																						"column": 2
																					},
																					"LabelVendor__": {
																						"line": 1,
																						"column": 1
																					},
																					"BillTo__": {
																						"line": 2,
																						"column": 2
																					},
																					"LabelBillTo__": {
																						"line": 2,
																						"column": 1
																					}
																				},
																				"lines": 2,
																				"columns": 2,
																				"colspans": [
																					[],
																					[]
																				],
																				"version": 0
																			},
																			"stamp": 317,
																			"*": {
																				"LabelVendor__": {
																					"type": "Label",
																					"data": [
																						"Vendor__"
																					],
																					"options": {
																						"label": "_Vendor",
																						"version": 0
																					},
																					"stamp": 318
																				},
																				"Vendor__": {
																					"type": "LongText",
																					"data": [
																						"Vendor__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"minNbLines": 3,
																						"numberOfLines": 5,
																						"label": "_Vendor",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"browsable": false,
																						"readonly": true
																					},
																					"stamp": 319
																				},
																				"LabelBillTo__": {
																					"type": "Label",
																					"data": [
																						"BillTo__"
																					],
																					"options": {
																						"label": "_BillTo"
																					},
																					"stamp": 320
																				},
																				"BillTo__": {
																					"type": "LongText",
																					"data": [
																						"BillTo__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"minNbLines": 3,
																						"numberOfLines": 5,
																						"label": "_BillTo",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"readonly": true,
																						"browsable": false
																					},
																					"stamp": 321
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-right-8": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {},
													"stamp": 324,
													"*": {
														"Line_Items": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"iconURL": "AP_Line_items.png",
																"TitleFontSize": "M",
																"hideActionButtons": true,
																"hideTitleBar": false,
																"hideTitle": false,
																"sameHeightAsSiblings": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "_Line Items",
																"hidden": false,
																"isSvgIcon": false,
																"leftImageURL": "",
																"removeMargins": false,
																"elementsAlignment": "left"
															},
															"stamp": 325,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {}
																	},
																	"stamp": 326,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"Table__": {
																						"line": 1,
																						"column": 1
																					},
																					"LineItems__": {
																						"line": 1,
																						"column": 1
																					}
																				},
																				"lines": 1,
																				"columns": 2,
																				"colspans": [
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					]
																				]
																			},
																			"stamp": 327,
																			"*": {
																				"LineItems__": {
																					"type": "Table",
																					"data": [
																						"LineItems__"
																					],
																					"options": {
																						"version": 4,
																						"lines": 20,
																						"columns": 9,
																						"displayEmptyLines": false,
																						"alwaysOneLine": true,
																						"menuOptions": {
																							"disableInsertLine": true,
																							"disableAddLine": true,
																							"hideTopNavigation": true,
																							"hideBottomNavigation": false,
																							"hideTableRowMenu": false,
																							"hideTableRowDelete": true,
																							"hideTableRowAdd": true,
																							"addButtonShouldInsert": false,
																							"hideTableRowDuplicate": true,
																							"hideTableRowButtonDuplicate": true
																						},
																						"item": {
																							"display": null
																						},
																						"group": {
																							"name": "GDR\nPage"
																						},
																						"label": "_Line items",
																						"subsection": null
																					},
																					"stamp": 328,
																					"*": {
																						"Actions": {
																							"type": "$TableActions",
																							"subtype": "Actions",
																							"options": {
																								"version": 0
																							},
																							"stamp": 329,
																							"*": {
																								"Top": {
																									"type": "$TableActions",
																									"subtype": "Top",
																									"options": {
																										"version": 0
																									},
																									"stamp": 330
																								},
																								"Bottom": {
																									"type": "$TableActions",
																									"subtype": "Top",
																									"options": {
																										"version": 0
																									},
																									"stamp": 331
																								}
																							}
																						},
																						"Header": {
																							"type": "$TableRow",
																							"subtype": "Header",
																							"options": {
																								"version": 0
																							},
																							"stamp": 332,
																							"data": [
																								null
																							],
																							"*": {
																								"Cell1": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": 50,
																										"version": 0
																									},
																									"stamp": 333,
																									"data": [],
																									"*": {
																										"ItemNumber__": {
																											"type": "Label",
																											"data": [
																												"ItemNumber__"
																											],
																											"options": {
																												"label": "_ItemNumber",
																												"minwidth": 50,
																												"version": 0
																											},
																											"stamp": 334,
																											"position": [
																												"_ShortText"
																											]
																										}
																									}
																								},
																								"Cell2": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": 140,
																										"version": 0
																									},
																									"stamp": 335,
																									"data": [],
																									"*": {
																										"Description__": {
																											"type": "Label",
																											"data": [
																												"Description__"
																											],
																											"options": {
																												"label": "_Description",
																												"minwidth": 140,
																												"version": 0
																											},
																											"stamp": 336,
																											"position": [
																												"_ShortText2"
																											]
																										}
																									}
																								},
																								"Cell3": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": "80",
																										"version": 0
																									},
																									"stamp": 337,
																									"data": [],
																									"*": {
																										"ExpectedQuantity__": {
																											"type": "Label",
																											"data": [
																												"ExpectedQuantity__"
																											],
																											"options": {
																												"label": "_ExpectedQuantity",
																												"minwidth": "80",
																												"version": 0
																											},
																											"stamp": 338,
																											"position": [
																												"_Decimal"
																											]
																										}
																									}
																								},
																								"Cell4": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": "0",
																										"hidden": false,
																										"version": 0
																									},
																									"stamp": 362,
																									"data": [],
																									"*": {
																										"MaxExpectedQuantity__": {
																											"type": "Label",
																											"data": [
																												"MaxExpectedQuantity__"
																											],
																											"options": {
																												"label": "_MaxExpectedQuantity",
																												"minwidth": "100",
																												"hidden": true,
																												"version": 0
																											},
																											"stamp": 363,
																											"position": [
																												"_Decimal"
																											]
																										}
																									}
																								},
																								"Cell5": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": "80",
																										"version": 0
																									},
																									"stamp": 345,
																									"data": [],
																									"*": {
																										"ExpectedAmount__": {
																											"type": "Label",
																											"data": [
																												"ExpectedAmount__"
																											],
																											"options": {
																												"label": "_ExpectedAmount",
																												"minwidth": "80",
																												"version": 0
																											},
																											"stamp": 346,
																											"position": [
																												"_Decimal"
																											]
																										}
																									}
																								},
																								"Cell6": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": 50,
																										"version": 0
																									},
																									"stamp": 339,
																									"data": [],
																									"*": {
																										"UnitOfMeasureCode__": {
																											"type": "Label",
																											"data": [
																												"UnitOfMeasureCode__"
																											],
																											"options": {
																												"label": "_UnitOfMeasureCode",
																												"minwidth": 50,
																												"version": 0
																											},
																											"stamp": 340,
																											"position": [
																												"_ShortText"
																											]
																										}
																									}
																								},
																								"Cell7": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": 50,
																										"version": 0
																									},
																									"stamp": 341,
																									"data": [],
																									"*": {
																										"UnitPrice__": {
																											"type": "Label",
																											"data": [
																												"UnitPrice__"
																											],
																											"options": {
																												"label": "_Unit price",
																												"minwidth": 50,
																												"version": 0
																											},
																											"stamp": 342,
																											"position": [
																												"_Decimal"
																											]
																										}
																									}
																								},
																								"Cell8": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": "50",
																										"version": 0
																									},
																									"stamp": 343,
																									"data": [],
																									"*": {
																										"TaxRate__": {
																											"type": "Label",
																											"data": [
																												"TaxRate__"
																											],
																											"options": {
																												"label": "_Tax rate",
																												"minwidth": "50",
																												"version": 0
																											},
																											"stamp": 344,
																											"position": [
																												"_Decimal"
																											]
																										}
																									}
																								},
																								"Cell9": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": "0",
																										"hidden": true
																									},
																									"stamp": 371,
																									"data": [],
																									"*": {
																										"SESIdentifier__": {
																											"type": "Label",
																											"data": [
																												"SESIdentifier__"
																											],
																											"options": {
																												"label": "_SESIdentifier",
																												"minwidth": 140,
																												"hidden": true
																											},
																											"stamp": 372,
																											"position": [
																												"_ShortText"
																											]
																										}
																									}
																								}
																							}
																						},
																						"Clipboard": {
																							"type": "$TableRow",
																							"subtype": "Clipboard",
																							"options": {
																								"version": 0
																							},
																							"stamp": 347,
																							"data": [
																								null
																							],
																							"*": {
																								"Cell1": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 348,
																									"data": [],
																									"*": {
																										"ItemNumber__": {
																											"type": "ShortText",
																											"data": [
																												"ItemNumber__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 0,
																												"label": "_ItemNumber",
																												"activable": true,
																												"width": 100,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"helpIconPosition": "Right",
																												"readonly": true,
																												"browsable": false
																											},
																											"stamp": 349,
																											"position": [
																												"_ShortText"
																											]
																										}
																									}
																								},
																								"Cell2": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 350,
																									"data": [],
																									"*": {
																										"Description__": {
																											"type": "ShortText",
																											"data": [
																												"Description__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 0,
																												"label": "_Description",
																												"activable": true,
																												"width": 250,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"helpIconPosition": "Right",
																												"readonly": true,
																												"browsable": false
																											},
																											"stamp": 351,
																											"position": [
																												"_ShortText2"
																											]
																										}
																									}
																								},
																								"Cell3": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 352,
																									"data": [],
																									"*": {
																										"ExpectedQuantity__": {
																											"type": "Decimal",
																											"data": [
																												"ExpectedQuantity__"
																											],
																											"options": {
																												"version": 0,
																												"textSize": "S",
																												"textAlignment": "right",
																												"textStyle": "default",
																												"textColor": "default",
																												"integer": false,
																												"label": "_ExpectedQuantity",
																												"precision_internal": 2,
																												"precision_current": 2,
																												"activable": true,
																												"width": 100,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"helpIconPosition": "Right",
																												"enablePlusMinus": false
																											},
																											"stamp": 353,
																											"position": [
																												"_Decimal"
																											]
																										}
																									}
																								},
																								"Cell4": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"hidden": false,
																										"version": 0
																									},
																									"stamp": 364,
																									"data": [],
																									"*": {
																										"MaxExpectedQuantity__": {
																											"type": "Decimal",
																											"data": [
																												"MaxExpectedQuantity__"
																											],
																											"options": {
																												"version": 2,
																												"textSize": "S",
																												"textAlignment": "right",
																												"textStyle": "default",
																												"textColor": "default",
																												"integer": false,
																												"label": "_MaxExpectedQuantity",
																												"precision_internal": 2,
																												"precision_current": 2,
																												"activable": true,
																												"width": "100",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"helpIconPosition": "Right",
																												"enablePlusMinus": false,
																												"hidden": true,
																												"readonly": true,
																												"autocompletable": false
																											},
																											"stamp": 365,
																											"position": [
																												"_Decimal"
																											]
																										}
																									}
																								},
																								"Cell5": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 360,
																									"data": [],
																									"*": {
																										"ExpectedAmount__": {
																											"type": "Decimal",
																											"data": [
																												"ExpectedAmount__"
																											],
																											"options": {
																												"version": 0,
																												"textSize": "S",
																												"textAlignment": "right",
																												"textStyle": "default",
																												"textColor": "default",
																												"integer": false,
																												"label": "_ExpectedAmount",
																												"precision_internal": 2,
																												"precision_current": 2,
																												"activable": true,
																												"width": 150,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"helpIconPosition": "Right",
																												"enablePlusMinus": false,
																												"readonly": true
																											},
																											"stamp": 361,
																											"position": [
																												"_Decimal"
																											]
																										}
																									}
																								},
																								"Cell6": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 354,
																									"data": [],
																									"*": {
																										"UnitOfMeasureCode__": {
																											"type": "ShortText",
																											"data": [
																												"UnitOfMeasureCode__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 0,
																												"label": "_UnitOfMeasureCode",
																												"activable": true,
																												"width": 80,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"helpIconPosition": "Right",
																												"length": 140,
																												"readonly": true,
																												"browsable": false
																											},
																											"stamp": 355,
																											"position": [
																												"_ShortText"
																											]
																										}
																									}
																								},
																								"Cell7": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 356,
																									"data": [],
																									"*": {
																										"UnitPrice__": {
																											"type": "Decimal",
																											"data": [
																												"UnitPrice__"
																											],
																											"options": {
																												"version": 0,
																												"textSize": "S",
																												"textAlignment": "right",
																												"textStyle": "default",
																												"textColor": "default",
																												"integer": false,
																												"label": "_Unit price",
																												"precision_internal": 2,
																												"precision_current": 2,
																												"activable": true,
																												"width": 80,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"helpIconPosition": "Right",
																												"enablePlusMinus": false,
																												"readonly": true
																											},
																											"stamp": 357,
																											"position": [
																												"_Decimal"
																											]
																										}
																									}
																								},
																								"Cell8": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 358,
																									"data": [],
																									"*": {
																										"TaxRate__": {
																											"type": "Decimal",
																											"data": [
																												"TaxRate__"
																											],
																											"options": {
																												"version": 0,
																												"textSize": "S",
																												"textAlignment": "right",
																												"textStyle": "default",
																												"textColor": "default",
																												"integer": false,
																												"label": "_Tax rate",
																												"precision_internal": 2,
																												"precision_current": 2,
																												"activable": true,
																												"width": 80,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"helpIconPosition": "Right",
																												"precision_min": 2,
																												"enablePlusMinus": false
																											},
																											"stamp": 359,
																											"position": [
																												"_Decimal"
																											]
																										}
																									}
																								},
																								"Cell9": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"hidden": true
																									},
																									"stamp": 370,
																									"data": [],
																									"*": {
																										"SESIdentifier__": {
																											"type": "ShortText",
																											"data": [
																												"SESIdentifier__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"label": "_SESIdentifier",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"helpIconPosition": "Right",
																												"hidden": true,
																												"readonly": true,
																												"browsable": false,
																												"autocompletable": false
																											},
																											"stamp": 373,
																											"position": [
																												"_ShortText"
																											]
																										}
																									}
																								}
																							}
																						}
																					}
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-right-13": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 243,
													"*": {
														"PaymentDetails": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": "200",
																"iconURL": "AP_PaymentDetailsPanel.png",
																"TitleFontSize": "L",
																"hideActionButtons": true,
																"hideTitleBar": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "_Payment Details",
																"leftImageURL": "",
																"version": 0,
																"hidden": true,
																"hideTitle": false
															},
															"stamp": 221,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"Payment_date__": "LabelPayment_date__",
																			"LabelPayment_date__": "Payment_date__",
																			"Payment_method__": "LabelPayment_method__",
																			"LabelPayment_method__": "Payment_method__",
																			"Payment_reference__": "LabelPayment_reference__",
																			"LabelPayment_reference__": "Payment_reference__"
																		},
																		"version": 0
																	},
																	"stamp": 222,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"Payment_date__": {
																						"line": 1,
																						"column": 2
																					},
																					"LabelPayment_date__": {
																						"line": 1,
																						"column": 1
																					},
																					"Payment_method__": {
																						"line": 2,
																						"column": 2
																					},
																					"LabelPayment_method__": {
																						"line": 2,
																						"column": 1
																					},
																					"Payment_reference__": {
																						"line": 3,
																						"column": 2
																					},
																					"LabelPayment_reference__": {
																						"line": 3,
																						"column": 1
																					}
																				},
																				"lines": 3,
																				"columns": 2,
																				"colspans": [
																					[],
																					[],
																					[]
																				],
																				"version": 0
																			},
																			"stamp": 223,
																			"*": {
																				"LabelPayment_date__": {
																					"type": "Label",
																					"data": [
																						"Payment_date__"
																					],
																					"options": {
																						"label": "_payment Date",
																						"version": 0
																					},
																					"stamp": 231
																				},
																				"Payment_date__": {
																					"type": "DateTime",
																					"data": [
																						"Payment_date__"
																					],
																					"options": {
																						"displayLongFormat": false,
																						"label": "_payment Date",
																						"activable": true,
																						"width": 230,
																						"readonly": true,
																						"autocompletable": false,
																						"version": 0
																					},
																					"stamp": 232
																				},
																				"LabelPayment_method__": {
																					"type": "Label",
																					"data": [
																						"Payment_method__"
																					],
																					"options": {
																						"label": "_payment Method",
																						"version": 0
																					},
																					"stamp": 233
																				},
																				"Payment_method__": {
																					"type": "ComboBox",
																					"data": [
																						"Payment_method__"
																					],
																					"options": {
																						"possibleValues": {
																							"0": "",
																							"1": "Cash",
																							"2": "Check",
																							"3": "Credit card",
																							"4": "EFT",
																							"5": "Other"
																						},
																						"keyValueMode": true,
																						"possibleKeys": {
																							"0": "0",
																							"1": "Cash",
																							"2": "Check",
																							"3": "Credit card",
																							"4": "EFT",
																							"5": "Other"
																						},
																						"label": "_payment Method",
																						"activable": true,
																						"width": 230,
																						"readonly": true,
																						"version": 0
																					},
																					"stamp": 234
																				},
																				"LabelPayment_reference__": {
																					"type": "Label",
																					"data": [
																						"Payment_reference__"
																					],
																					"options": {
																						"label": "_Payment Reference",
																						"version": 0
																					},
																					"stamp": 235
																				},
																				"Payment_reference__": {
																					"type": "ShortText",
																					"data": [
																						"Payment_reference__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"label": "_Payment Reference",
																						"activable": true,
																						"width": 230,
																						"readonly": true,
																						"browsable": false,
																						"autocompletable": false,
																						"version": 0
																					},
																					"stamp": 236
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-right-10": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 384,
													"*": {
														"Early_payment_offer_pane": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"iconURL": "AP_OnHoldExpiration.png",
																"TitleFontSize": "L",
																"hideActionButtons": true,
																"hideTitleBar": false,
																"hideTitle": false,
																"sameHeightAsSiblings": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "_Early_payment_offer_pane",
																"hidden": true,
																"leftImageURL": "",
																"removeMargins": false,
																"elementsAlignment": "left",
																"isSvgIcon": false,
																"version": 0,
																"labelLength": 0
															},
															"stamp": 375,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"DiscountLimitDate__": "LabelDiscountLimitDate__",
																			"LabelDiscountLimitDate__": "DiscountLimitDate__",
																			"DiscountPercent__": "LabelDiscountPercent__",
																			"LabelDiscountPercent__": "DiscountPercent__",
																			"EstimatedDiscountAmount__": "LabelEstimatedDiscountAmount__",
																			"LabelEstimatedDiscountAmount__": "EstimatedDiscountAmount__",
																			"InvoiceAmountWithDiscount__": "LabelInvoiceAmountWithDiscount__",
																			"LabelInvoiceAmountWithDiscount__": "InvoiceAmountWithDiscount__"
																		},
																		"version": 0
																	},
																	"stamp": 376,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"DiscountLimitDate__": {
																						"line": 1,
																						"column": 2
																					},
																					"LabelDiscountLimitDate__": {
																						"line": 1,
																						"column": 1
																					},
																					"DiscountPercent__": {
																						"line": 2,
																						"column": 2
																					},
																					"LabelDiscountPercent__": {
																						"line": 2,
																						"column": 1
																					},
																					"EstimatedDiscountAmount__": {
																						"line": 3,
																						"column": 2
																					},
																					"LabelEstimatedDiscountAmount__": {
																						"line": 3,
																						"column": 1
																					},
																					"InvoiceAmountWithDiscount__": {
																						"line": 4,
																						"column": 2
																					},
																					"LabelInvoiceAmountWithDiscount__": {
																						"line": 4,
																						"column": 1
																					},
																					"SubmitEarlyPaymentDate__": {
																						"line": 5,
																						"column": 1
																					},
																					"DiscountSpacer__": {
																						"line": 6,
																						"column": 1
																					}
																				},
																				"lines": 6,
																				"columns": 2,
																				"colspans": [
																					[],
																					[],
																					[],
																					[],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[]
																				],
																				"version": 0
																			},
																			"stamp": 377,
																			"*": {
																				"LabelDiscountLimitDate__": {
																					"type": "Label",
																					"data": [
																						"DiscountLimitDate__"
																					],
																					"options": {
																						"label": "_DiscountLimitDate",
																						"version": 0
																					},
																					"stamp": 387
																				},
																				"DiscountLimitDate__": {
																					"type": "DateTime",
																					"data": [
																						"DiscountLimitDate__"
																					],
																					"options": {
																						"displayLongFormat": false,
																						"label": "_DiscountLimitDate",
																						"activable": true,
																						"width": "150",
																						"helpText": "_TooltipToSelectDiscountLimitDate",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"helpIconPosition": "Right",
																						"version": 0
																					},
																					"stamp": 388
																				},
																				"LabelDiscountPercent__": {
																					"type": "Label",
																					"data": [
																						"DiscountPercent__"
																					],
																					"options": {
																						"label": "_DiscountPercent",
																						"version": 0
																					},
																					"stamp": 389
																				},
																				"DiscountPercent__": {
																					"type": "ShortText",
																					"data": [
																						"DiscountPercent__"
																					],
																					"options": {
																						"version": 0,
																						"textSize": "S",
																						"textAlignment": "right",
																						"textStyle": "default",
																						"textColor": "default",
																						"label": "_DiscountPercent",
																						"activable": true,
																						"width": "150",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"helpIconPosition": "Right",
																						"readonly": true
																					},
																					"stamp": 390
																				},
																				"LabelEstimatedDiscountAmount__": {
																					"type": "Label",
																					"data": [
																						"EstimatedDiscountAmount__"
																					],
																					"options": {
																						"label": "_EstimatedDiscountAmount",
																						"version": 0
																					},
																					"stamp": 391
																				},
																				"EstimatedDiscountAmount__": {
																					"type": "Decimal",
																					"data": [
																						"EstimatedDiscountAmount__"
																					],
																					"options": {
																						"version": 2,
																						"textSize": "S",
																						"textAlignment": "right",
																						"textStyle": "default",
																						"textColor": "default",
																						"integer": false,
																						"label": "_EstimatedDiscountAmount",
																						"precision_internal": 2,
																						"precision_current": 2,
																						"activable": true,
																						"width": "150",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"helpIconPosition": "Right",
																						"enablePlusMinus": false,
																						"readonly": true
																					},
																					"stamp": 392
																				},
																				"LabelInvoiceAmountWithDiscount__": {
																					"type": "Label",
																					"data": [
																						"InvoiceAmountWithDiscount__"
																					],
																					"options": {
																						"label": "_InvoiceAmountWithDiscount",
																						"version": 0
																					},
																					"stamp": 393
																				},
																				"InvoiceAmountWithDiscount__": {
																					"type": "Decimal",
																					"data": [
																						"InvoiceAmountWithDiscount__"
																					],
																					"options": {
																						"version": 2,
																						"textSize": "S",
																						"textAlignment": "right",
																						"textStyle": "default",
																						"textColor": "default",
																						"integer": false,
																						"label": "_InvoiceAmountWithDiscount",
																						"precision_internal": 2,
																						"precision_current": 2,
																						"activable": true,
																						"width": "150",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"helpIconPosition": "Right",
																						"enablePlusMinus": false,
																						"readonly": true
																					},
																					"stamp": 394
																				},
																				"SubmitEarlyPaymentDate__": {
																					"type": "FormButton",
																					"data": false,
																					"options": {
																						"buttonLabel": "_SubmitEarlyPaymentDate",
																						"label": "_Button",
																						"textPosition": "text-right",
																						"textSize": "MS",
																						"textStyle": "default",
																						"textColor": "default",
																						"urlImageOverlay": "",
																						"hidden": false,
																						"action": "none",
																						"url": "",
																						"width": "",
																						"style": 1
																					},
																					"stamp": 405
																				},
																				"Spacer2__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "",
																						"width": "",
																						"color": "default",
																						"label": "_Spacer2"
																					},
																					"stamp": 404
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-right-9": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 154,
													"*": {
														"DocumentsPanel": {
															"type": "PanelDocument",
															"options": {
																"version": 0,
																"border": {},
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "L",
																"hideActionButtons": false,
																"hideTitleBar": false,
																"label": "_Documents",
																"hidden": true,
																"hideTitle": false
															},
															"*": {
																"Document": {
																	"data": [
																		"documentscontroldata"
																	],
																	"type": "Documents",
																	"options": {
																		"version": 1,
																		"previewButton": true,
																		"downloadButton": true
																	},
																	"stamp": 10
																}
															},
															"stamp": 9,
															"data": []
														}
													}
												},
												"form-content-right-11": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 175,
													"*": {
														"SystemData": {
															"type": "PanelSystemData",
															"options": {
																"multicolumn": false,
																"version": 0,
																"border": {
																	"collapsed": true
																},
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "L",
																"hideActionButtons": false,
																"hideTitleBar": false,
																"label": "_System data",
																"hidden": true,
																"hideTitle": false
															},
															"stamp": 18,
															"data": []
														}
													}
												},
												"form-content-right-12": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 166,
													"*": {
														"NextProcess": {
															"type": "PanelNextProcess",
															"options": {
																"hidden": true,
																"version": 0,
																"border": {},
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "L",
																"hideActionButtons": false,
																"hideTitleBar": false,
																"label": "_Next Process",
																"hideTitle": false
															},
															"stamp": 20,
															"data": []
														}
													}
												},
												"form-content-right-7": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 269,
													"*": {
														"Event_history": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": "200",
																"iconURL": "AP_comment.png",
																"TitleFontSize": "L",
																"hideActionButtons": true,
																"hideTitleBar": false,
																"hideTitle": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "_Events",
																"leftImageURL": "",
																"removeMargins": false,
																"version": 0
															},
															"stamp": 260,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {},
																		"version": 0
																	},
																	"stamp": 261,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"ConversationUI__": {
																						"line": 1,
																						"column": 1
																					}
																				},
																				"lines": 1,
																				"columns": 2,
																				"colspans": [
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					]
																				],
																				"version": 0
																			},
																			"stamp": 262,
																			"*": {
																				"ConversationUI__": {
																					"type": "ConversationUI",
																					"data": false,
																					"options": {
																						"label": "_ConversationUI",
																						"activable": true,
																						"width": 1033,
																						"tableName": "Conversation__",
																						"version": 0,
																						"conversationOptions": {
																							"ignoreIfExists": false,
																							"notifyByEmail": true
																						}
																					},
																					"stamp": 271
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												}
											},
											"stamp": 21,
											"data": []
										}
									},
									"stamp": 11,
									"data": []
								}
							},
							"stamp": 6,
							"data": []
						}
					},
					"stamp": 5,
					"data": []
				},
				"form-footer": {
					"type": "FlexibleFormFooter",
					"options": {
						"splitterType": "simple-h",
						"version": 0,
						"maxwidth": ""
					},
					"*": {},
					"stamp": 25,
					"data": []
				}
			},
			"stamp": 3,
			"data": []
		}
	},
	"stamps": 405,
	"data": []
}