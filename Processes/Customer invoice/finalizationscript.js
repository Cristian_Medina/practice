///#GLOBALS Lib Sys

function resetAreas()
{
	for (var i = 0; i < Data.GetNbFields(); i++)
	{
		var fieldName = Data.GetFieldName(i);
		if (Data.GetArea(fieldName))
		{
			Data.SetValue(fieldName, Data.GetValue(fieldName));
		}
	}
}

function submitVIP()
{
	try
	{
		var VIPTransportRuidEx = Transaction.Read("VIPTransportRuidEx");
		if (VIPTransportRuidEx)
		{
			Data.SetValue("VIRuidEx__", VIPTransportRuidEx);
		}
		else if (!Lib.AP.VendorPortal.CreateVIPTransport())
		{
			throw "";
		}
	}
	catch (exception)
	{
		Log.Error("An error occured while sending invoice to the billed entity");
		throw Language.Translate("_An error occured while sending invoice to the billed entity");
	}
}

function run()
{
	// Wait for workflow to finish
	var status = Data.GetValue("CustomerInvoiceStatus__");
	if (status === Lib.AP.CIStatus.ToSend || status === Lib.AP.CIStatus.Draft)
	{
		submitVIP();
		Data.SetValue("CustomerInvoiceStatus__", Lib.AP.CIStatus.AwaitingReception);
		// Reset capture area to avoid mismatch when the field values are updated when the APSpecialist verifies the invoice (waiting to be able to update areas as well as values).
		resetAreas();
		Process.WaitForUpdate();
	}
	else if (status === Lib.AP.CIStatus.AwaitingReception || status === Lib.AP.CIStatus.AwaitingPaymentApproval)
	{
		Log.Info("Wait for workflow to complete before closing invoice (" + status + ")");

		// Make sure the customer invoice does not expire if AP workflow takes more than 2 months
		var validityDT = Data.GetValue("SubmitDateTime");
		validityDT.setMonth(validityDT.getMonth() + 16);
		Data.SetValue("ValidityDateTime", validityDT);

		Process.WaitForUpdate();
	}
	else if (status === Lib.AP.CIStatus.Rejected)
	{
		// force state to Rejected when created as Rejected from the VIP or auto-rejected from the VIP
		Log.Info("Created as Rejected or auto-rejected");
		Data.SetValue("State", 400);
	}
	else
	{
		Log.Info("Workflow is complete, archive invoice");
	}
}

run();
