///#GLOBALS Lib Sys

function InitArchiveDuration()
{
	var archiveDuration = Sys.Parameters.GetInstance("AP").GetParameter("ArchiveDurationVendorPortalInMonth", null);
	if (archiveDuration !== null)
	{
		Data.SetValue("ArchiveDuration", archiveDuration);
	}
}

//#region action
function actionSubmit()
{
	Variable.SetValueAsString("lasterror", "");
	if (Lib.AP.CustomerInvoiceType.isFlipPO())
	{
		var generateInvoiceNumber = Sys.Helpers.TryCallFunction("Lib.AP.Customization.VendorPortal.GenerateInvoiceNumber");
		if (!Data.GetValue("Invoice_number__") && generateInvoiceNumber)
		{
			Data.SetValue("Invoice_number__", Lib.AP.VendorPortal.CreateFromFlipPO.getNextInvoiceNumber());
		}
		// Generate final version of the invoice image based on the form values the vendor changed.
		Lib.AP.VendorPortal.CreateFromFlipPO.validatePDF(Lib.AP.VendorPortal.CreateFromFlipPO.getCIDataFromForm());

		var bSign = Sys.Helpers.TryCallFunction("Lib.AP.Customization.VendorPortal.SignInvoice");
		if (bSign && !Lib.AP.VendorPortal.CreateFromFlipPO.signInvoice())
		{
			Variable.SetValueAsString("lasterror", "_ErrorMessageSignatureFailed");
			Process.PreventApproval();
		}
	}
}

function actionJoinVIToCIChat()
{
	if (Data.GetActionType() === "ResumeWithAction")
	{
		Lib.AP.VendorPortal.JoinVIToCIChat(false);
	}
}

function actionRefreshPreview()
{
	if (Lib.AP.CustomerInvoiceType.isFlipPO())
	{
		var overrideValues = Lib.AP.VendorPortal.CreateFromFlipPO.getCIDataFromForm();
		overrideValues.CustomerInvoiceStatus__ = Data.GetValue("CustomerInvoiceStatus__");
		Lib.AP.VendorPortal.CreateFromFlipPO.validatePDF(overrideValues);
	}
	Process.PreventApproval();
}

function UpdateInvoiceDynamicDiscount()
{
	var VIRuidEx = Data.GetValue("VIRuidEx__");
	if (!VIRuidEx)
	{
		Log.Error("UpdateInvoiceDynamicDiscount: no VIRuidEx__ field set");
		return false;
	}
	var EstimatedDiscountAmount = Data.GetValue("EstimatedDiscountAmount__");
	var DiscountLimitDate = Data.GetValue("DiscountLimitDate__");

	if ((!EstimatedDiscountAmount && EstimatedDiscountAmount !== 0) || !DiscountLimitDate)
	{
		Log.Error("UpdateInvoiceDynamicDiscount: EstimatedDiscountAmount or DiscountLimitDate empty");
		return false;
	}

	try
	{
		var exchangeRateStr;
		var filter = "RuidEx=" + VIRuidEx;
		var processQuery = Process.CreateQueryAsProcessAdmin();
		processQuery.SetSpecificTable("CDNAME#vendor invoice");
		processQuery.SetFilter(filter);
		processQuery.SetSearchInArchive(true);
		processQuery.SetAttributesList("ExchangeRate__");
		if (processQuery.MoveFirst())
		{
			var updatableInvoice = processQuery.MoveNext();
			if (updatableInvoice)
			{
				var updatableInvoiceVars = updatableInvoice.GetUninheritedVars();
				exchangeRateStr = updatableInvoiceVars.GetValue_String("ExchangeRate__", 0);
			}
		}
		var exchangeRate = parseFloat(exchangeRateStr);
		var localDiscountAmount = exchangeRate ? EstimatedDiscountAmount * exchangeRate : EstimatedDiscountAmount;

		var jsonParams = {
			"fields": {
				"EstimatedDiscountAmount__": "" + EstimatedDiscountAmount,
				"LocalEstimatedDiscountAmount__": "" + localDiscountAmount,
				"DiscountLimitDate__": Sys.Helpers.Date.Format(DiscountLimitDate, "isoDate") + " " + Sys.Helpers.Date.Format(DiscountLimitDate, "isoTime")
			}
		};
		var ret = Process.UpdateProcessInstanceDataAsync(VIRuidEx, JSON.stringify(jsonParams));
		if (!ret)
		{
			Log.Error("UpdateInvoiceDynamicDiscount: Failed to UpdateProcessInstanceDataAsync");
			return false;
		}
		var message = Language.Translate("_Invoice discounting proposal, discount amount of {0} if payment before the {1}.", false,
			EstimatedDiscountAmount + " " + Data.GetValue("Currency__"), Variable.GetValueAsString("DiscountLimitDateFormated"));
		var options =
		{
			ignoreIfExists: false
		};

		Conversation.AddItem("Conversation__", {
			Message: message,
			Type: "10"
		}, options);
	}
	catch (e)
	{
		Log.Error(e);
		return false;
	}
	return true;
}

function actionProposeEarlyPayment()
{
	if (Lib.AP.VendorPortal.isDynamicDiscountingEnabled())
	{
		var isSuccess = UpdateInvoiceDynamicDiscount();
		Process.PreventApproval();
		if (isSuccess)
		{
			Process.LeaveForm();
		}
	}
	else
	{
		Process.PreventApproval();
	}
}

function actionPendingPayment()
{
	// Dynamic discounting - Go back to state 70
	Process.PreventApproval();
}

function getCleanActionName(actionName)
{
	var res = actionName.toLowerCase();
	if (res.substr(-2) === "__")
	{
		res = res.substr(0, res.length - 2);
	}
	return res;
}
//#endregion

/** ******** **/
/** RUN PART **/
/** ******** **/
function executeRequestedAction()
{
	InitArchiveDuration();

	var currentName = Data.GetActionName();
	var currentAction = Data.GetActionType();

	Log.Info("currentAction:" + currentAction);
	Log.Info("currentName:" + currentName);


	var approveActionMap = {};
	approveActionMap.submit = { "execute": actionSubmit };
	approveActionMap.joinvitocichat = { "execute": actionJoinVIToCIChat };
	approveActionMap.refreshpreview = { "execute": actionRefreshPreview };
	approveActionMap.submitearlypaymentdate = { "execute": actionProposeEarlyPayment };
	approveActionMap.invoicependingpayment = { "execute": actionPendingPayment };

	var action = approveActionMap[getCleanActionName(currentName)];
	if (action)
	{
		action.execute();
	}
	else if (Lib.AP.CustomerInvoiceType.isFlipPO())
	{
		Process.PreventApproval();
	}

	//force empty billing label
	Data.SetValue("ProcessingLabel", "");
}

executeRequestedAction();
Data.SetValue("PortalRuidEx__", Data.GetValue("RuidEx"));
