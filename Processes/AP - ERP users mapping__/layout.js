{
	"*": {
		"Screen": {
			"type": "FlexibleFormLayout",
			"options": {
				"splitterType": "simple-v",
				"version": 0
			},
			"*": {
				"form-header": {
					"type": "FlexibleFormHeader",
					"options": {
						"hidePanelsMenu": false,
						"version": 0
					},
					"stamp": 2,
					"data": []
				},
				"form-center": {
					"type": "FlexibleFormCenter",
					"options": {
						"version": 0
					},
					"*": {
						"form-content": {
							"type": "FlexibleFormContent",
							"options": {
								"version": 0
							},
							"*": {
								"form-content-center": {
									"type": "FlexibleFormSplitter",
									"options": {
										"version": 1,
										"box": {
											"top": "0%",
											"left": "0%",
											"width": "100%"
										},
										"hidden": false
									},
									"*": {
										"form-content-center-1": {
											"type": "FlexibleFormLine",
											"data": [],
											"options": {
												"version": 0
											},
											"stamp": 9,
											"*": {
												"DataPanel": {
													"type": "PanelData",
													"options": {
														"border": {},
														"version": 0,
														"labelLength": 0,
														"iconURL": "",
														"TitleFontSize": "S",
														"hideActionButtons": false,
														"hideTitleBar": false,
														"hideTitle": false,
														"backgroundcolor": "default",
														"labelsAlignment": "right",
														"label": "_Document data"
													},
													"*": {
														"FieldsManager": {
															"type": "FieldsManager",
															"options": {
																"controlsAssociation": {
																	"ERPID__": "LabelERPID__",
																	"LabelERPID__": "ERPID__",
																	"Login__": "LabelLogin__",
																	"LabelLogin__": "Login__"
																},
																"version": 0
															},
															"*": {
																"Grid": {
																	"type": "GridLayout",
																	"options": {
																		"lines": 2,
																		"columns": 2,
																		"version": 0,
																		"ctrlsPos": {
																			"ERPID__": {
																				"line": 1,
																				"column": 2
																			},
																			"LabelERPID__": {
																				"line": 1,
																				"column": 1
																			},
																			"Login__": {
																				"line": 2,
																				"column": 2
																			},
																			"LabelLogin__": {
																				"line": 2,
																				"column": 1
																			}
																		},
																		"colspans": [
																			[],
																			[]
																		]
																	},
																	"data": [
																		"fields"
																	],
																	"*": {
																		"LabelERPID__": {
																			"type": "Label",
																			"data": [
																				"ERPID__"
																			],
																			"options": {
																				"label": "ERPID",
																				"version": 0
																			},
																			"stamp": 14
																		},
																		"ERPID__": {
																			"type": "ShortText",
																			"data": [
																				"ERPID__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "ERPID",
																				"activable": true,
																				"width": 230,
																				"version": 0
																			},
																			"stamp": 15
																		},
																		"LabelLogin__": {
																			"type": "Label",
																			"data": [
																				"Login__"
																			],
																			"options": {
																				"label": "Login"
																			},
																			"stamp": 18
																		},
																		"Login__": {
																			"type": "DatabaseComboBox",
																			"data": [
																				"Login__"
																			],
																			"options": {
																				"version": 1,
																				"label": "Login",
																				"activable": true,
																				"width": 230,
																				"fTSmaxRecords": "20",
																				"browsable": true
																			},
																			"stamp": 19
																		}
																	},
																	"stamp": 8
																}
															},
															"stamp": 7,
															"data": []
														}
													},
													"stamp": 6,
													"data": []
												}
											}
										}
									},
									"stamp": 5,
									"data": []
								}
							},
							"stamp": 4,
							"data": []
						}
					},
					"stamp": 3,
					"data": []
				},
				"form-footer": {
					"type": "FlexibleFormFooter",
					"options": {
						"splitterType": "simple-h",
						"version": 0
					},
					"*": {
						"Save": {
							"type": "SubmitButton",
							"options": {
								"label": "_Save",
								"action": "savequit",
								"submit": true,
								"version": 0
							},
							"stamp": 11,
							"data": []
						},
						"Close": {
							"type": "SubmitButton",
							"options": {
								"label": "_Close",
								"action": "cancel",
								"submit": true,
								"version": 0
							},
							"stamp": 12,
							"data": []
						},
						"Delete": {
							"type": "SubmitButton",
							"options": {
								"label": "_Delete",
								"action": "delete",
								"submit": true,
								"version": 0,
								"disabled": true
							},
							"stamp": 13,
							"data": []
						}
					},
					"stamp": 10,
					"data": []
				}
			},
			"stamp": 1,
			"data": []
		}
	},
	"stamps": 19,
	"data": []
}