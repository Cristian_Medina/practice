/*******************************
Expected ERP ACK Format
********************************

<?xml version="1.0" encoding="utf-8"?>
<ERPAck>
    <ERPID> ERP ID </ERPID>
    <EskerInvoiceID> Esker ID </EskerInvoiceID>
    <ERPPostingError> ERP Error </ERPPostingError>
</ERPAck>

*/
var xpathERPID = "/ERPAck/ERPID";
var xpathEskerID = "/ERPAck/EskerInvoiceID";
var xpathERPError = "/ERPAck/ERPPostingError";
var xpathHolds = "/ERPAck/Holds/Hold";
/*******************************
Read the ERP Acknowledgment File
********************************/
Data.SetValue("Document_ERP_ID__", Attach.GetValueFromXPath(0, xpathERPID));
Data.SetValue("EskerID__", Attach.GetValueFromXPath(0, xpathEskerID));
Data.SetValue("ERPPostingError__", Attach.GetValueFromXPath(0, xpathERPError));
var xmlFile = Attach.GetInputFile(0);
if (xmlFile)
{
	var xmlDoc = Process.CreateXMLDOMElement(xmlFile);
	var holdList = xmlDoc.selectNodes(xpathHolds);
	var jsonHolds = [];
	for (var i = 0; i < holdList.length; i++)
	{
		var holdNode = holdList.item(i);
		var jsonHold = {
			HoldName__: holdNode.selectSingleNode("HoldName").text,
			HoldDate__: holdNode.selectSingleNode("HoldDate").text,
			HoldReason__: holdNode.selectSingleNode("HoldReason").text
		};
		jsonHolds.push(jsonHold);
	}
	Variable.SetValueAsString("Holds__", JSON.stringify(jsonHolds));
}