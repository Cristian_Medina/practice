///#GLOBALS Lib
var ERPID = Data.GetValue("Document_ERP_ID__");
var EskerID = Data.GetValue("EskerID__");
var ERPError = Data.GetValue("ERPPostingError__");
var Holds = Variable.GetValueAsString("Holds__");
var VALIDITY_DURATION = 6;
var FormErrors = [];

function retryScript(errorMsg, longMsg)
{
	Log.Error(errorMsg + ": " + longMsg);
	if (Process.GetScriptRetryCount() < Process.GetScriptMaxRetryCount() - 1)
	{
		// Generate an exception for script to retry
		throw errorMsg + ". Retrying...";
	}
	else
	{
		Data.SetError("EskerID__", errorMsg);
	}
}
/*******************************
Update the invoice
********************************/
function updateInvoice(transport)
{
	var vars = transport.GetUninheritedVars();
	var externalVariables = transport.GetExternalVars();
	var state = vars.GetValue_Long("State", 0);
	var waitingForUpdate = vars.GetValue_Long("WaitingForUpdate", 0);
	if (state === 90 && waitingForUpdate)
	{
		Log.Info("Updating invoice with document ID:" + ERPID);
		vars.AddValue_String("ERPInvoiceNumber__", ERPID, true);
		vars.AddValue_String("ERPPostingError__", ERPError, true);
		vars.AddValue_String("ERPAckRuidEx__", Data.GetValue("RuidEx"), true);
		externalVariables.AddValue_String("ScheduledActionParameters", Holds, true);
		if (!transport.ResumeWithAction("continueAfterERPAck"))
		{
			retryScript("Failed resume processing on invoice", transport.GetLastErrorMessage());
		}
	}
	else
	{
		// The invoice is not in the expected state: failure
		var errorMsg = "Cannot update invoice with document ID " + ERPID + " because it is not in the expected state (state=" + state + ", waitingForUpdate=" + waitingForUpdate + ")";
		Log.Error(errorMsg);
		if (!waitingForUpdate)
		{
			FormErrors.push("The invoice is not expecting an update from the ERP");
		}
		if (state !== 90)
		{
			FormErrors.push("The invoice is not in the expected state: '" + state + "' instead of '90'");
		}
		var errors = FormErrors.join("\n");
		Data.SetValue("ProcessingError__", errors);
		Data.SetValue("State", 200);
	}
}
/*******************************
Find the invoice to update
********************************/
function findAndUpdateInvoice()
{
	//MSN and ProcessID are mandatory to commit modification
	Query.Reset();
	Query.AddAttribute("MSN");
	Query.AddAttribute("ProcessID");
	Query.AddAttribute("ERPInvoiceNumber__");
	Query.AddAttribute("State");
	Query.AddAttribute("WaitingForUpdate");
	Query.SetSearchInArchive(true);
	var ruid = EskerID;
	if (ruid && ruid.indexOf("#") < 0)
	{
		// add missing process ID
		ruid = "CD#" + Process.GetProcessID("Vendor invoice") + "." + EskerID;
	}
	Query.SetFilter("RUIDEX=" + ruid);
	Query.MoveFirst();
	var eddTransport = Query.MoveNext();
	if (eddTransport)
	{
		updateInvoice(eddTransport);
		// Activate a timeout on the process
		var newValidityDate = new Date();
		newValidityDate.setHours(newValidityDate.getHours() + VALIDITY_DURATION);
		Data.SetValue("ValidityDateTime", newValidityDate);
		// Wait for a response from the VIP
		Process.WaitForUpdate();
	}
	else
	{
		Data.SetValue("ProcessingError__", "Cannot find any vendor invoice matching the Esker ID: " + EskerID);
		Data.SetValue("State", 200);
	}
}

function main()
{
	if (Data.GetValue("State") < 50)
	{
		// Validation script executed after extraction script, don't do anything
		// Validation script will be reexecuted as the process don't expect a manual approval
		return;
	}
	// Neither ERP Id nor ERP Error are present
	if ((!ERPID || !ERPID.trim()) && !ERPError)
	{
		Data.SetValue("ProcessingError__", "Neither ERP Id nor ERP Error were provided");
		Data.SetValue("State", 200);
		return;
	}
	if (Data.GetActionName() === Lib.AP.ERPAcknowledgment.Actions.VIPErpAcknowledgmentDone)
	{
		Log.Info("Notification received from vendor invoice. ErpAck completed successfuly.");
		// In case the VIP says ok, restore the ValidityDateTime to 2 month
		var validityDateTime = Data.GetValue("ValidityDateTime");
		validityDateTime.setMonth(validityDateTime.getMonth() + 2);
		Data.SetValue("ValidityDateTime", validityDateTime);
	}
	else
	{
		// By default, update the VIP
		findAndUpdateInvoice();
	}
}
main();