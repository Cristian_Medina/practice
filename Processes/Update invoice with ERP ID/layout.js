{
	"*": {
		"GDRData": {
			"type": "GDRData",
			"options": {
				"version": 0
			},
			"stamp": 1,
			"data": []
		},
		"OCRParams": {
			"type": "OCRParams",
			"options": {
				"version": 0
			},
			"stamp": 2,
			"data": []
		},
		"Screen": {
			"type": "FlexibleFormLayout",
			"options": {
				"splitterType": "complex",
				"version": 0,
				"maxwidth": ""
			},
			"*": {
				"form-header": {
					"type": "FlexibleFormHeader",
					"options": {
						"version": 0,
						"maxwidth": ""
					},
					"stamp": 3,
					"data": []
				},
				"form-center": {
					"type": "FlexibleFormCenter",
					"options": {
						"version": 0,
						"maxwidth": ""
					},
					"*": {
						"form-content": {
							"type": "FlexibleFormContent",
							"options": {
								"version": 0
							},
							"*": {
								"form-content-top": {
									"type": "FlexibleFormSplitter",
									"options": {
										"autosize": true,
										"version": 1,
										"box": {
											"top": "0%",
											"left": "0%",
											"width": null,
											"height": "21%"
										},
										"hidden": true
									},
									"*": {},
									"stamp": 4,
									"data": []
								},
								"form-content-bottom": {
									"type": "FlexibleFormSplitter",
									"options": {
										"version": 1,
										"box": {
											"top": "0%",
											"left": "0%",
											"width": null,
											"height": "100%"
										},
										"hidden": false,
										"hideSeparator": true
									},
									"*": {
										"form-content-left": {
											"type": "FlexibleFormSplitter",
											"options": {
												"version": 1,
												"box": {
													"top": "0%",
													"left": "0%",
													"width": "100%",
													"height": null
												},
												"hidden": false
											},
											"*": {
												"form-content-left-1": {
													"type": "FlexibleFormLine",
													"options": {
														"version": 0
													},
													"*": {
														"DocumentsPanel": {
															"type": "PanelDocument",
															"options": {
																"version": 0,
																"border": {},
																"labelLength": 0,
																"label": "_Documents",
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": false,
																"hideTitleBar": false
															},
															"*": {
																"Document": {
																	"data": [
																		"documentscontroldata"
																	],
																	"type": "Documents",
																	"options": {
																		"version": 0
																	},
																	"stamp": 5
																}
															},
															"stamp": 6,
															"data": []
														}
													},
													"stamp": 7,
													"data": []
												},
												"form-content-left-5": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 8,
													"*": {
														"DataPanel": {
															"type": "PanelData",
															"options": {
																"border": {},
																"version": 0,
																"labelLength": 0,
																"label": "_Document data",
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": false,
																"hideTitleBar": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right"
															},
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"options": {
																		"controlsAssociation": {
																			"EskerID__": "LabelEskerID__",
																			"LabelEskerID__": "EskerID__",
																			"Document_ERP_ID__": "LabelDocument_ERP_ID__",
																			"LabelDocument_ERP_ID__": "Document_ERP_ID__",
																			"ERPPostingError__": "LabelERPPostingError__",
																			"LabelERPPostingError__": "ERPPostingError__",
																			"ProcessingError__": "LabelProcessingError__",
																			"LabelProcessingError__": "ProcessingError__"
																		},
																		"version": 0
																	},
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"options": {
																				"lines": 4,
																				"columns": 2,
																				"version": 0,
																				"ctrlsPos": {
																					"EskerID__": {
																						"line": 1,
																						"column": 2
																					},
																					"LabelEskerID__": {
																						"line": 1,
																						"column": 1
																					},
																					"Document_ERP_ID__": {
																						"line": 2,
																						"column": 2
																					},
																					"LabelDocument_ERP_ID__": {
																						"line": 2,
																						"column": 1
																					},
																					"ERPPostingError__": {
																						"line": 3,
																						"column": 2
																					},
																					"LabelERPPostingError__": {
																						"line": 3,
																						"column": 1
																					},
																					"ProcessingError__": {
																						"line": 4,
																						"column": 2
																					},
																					"LabelProcessingError__": {
																						"line": 4,
																						"column": 1
																					}
																				},
																				"colspans": [
																					[],
																					[],
																					[],
																					[]
																				]
																			},
																			"data": [
																				"fields"
																			],
																			"*": {
																				"LabelEskerID__": {
																					"type": "Label",
																					"data": [
																						"EskerID__"
																					],
																					"options": {
																						"label": "Identifier of invoice to update",
																						"version": 0
																					},
																					"stamp": 9
																				},
																				"EskerID__": {
																					"type": "ShortText",
																					"data": [
																						"EskerID__"
																					],
																					"options": {
																						"label": "Identifier of invoice to update",
																						"activable": true,
																						"width": 270,
																						"version": 0,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default"
																					},
																					"stamp": 10
																				},
																				"LabelDocument_ERP_ID__": {
																					"type": "Label",
																					"data": [
																						"Document_ERP_ID__"
																					],
																					"options": {
																						"label": "ERP Document Identifier",
																						"version": 0
																					},
																					"stamp": 11
																				},
																				"Document_ERP_ID__": {
																					"type": "ShortText",
																					"data": [
																						"Document_ERP_ID__"
																					],
																					"options": {
																						"label": "ERP Document Identifier",
																						"activable": true,
																						"width": 270,
																						"version": 0,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"browsable": false
																					},
																					"stamp": 12
																				},
																				"LabelERPPostingError__": {
																					"type": "Label",
																					"data": [
																						"ERPPostingError__"
																					],
																					"options": {
																						"label": "ERP posting error",
																						"version": 0
																					},
																					"stamp": 13
																				},
																				"ERPPostingError__": {
																					"type": "ShortText",
																					"data": [
																						"ERPPostingError__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"label": "ERP posting error",
																						"activable": true,
																						"width": 270,
																						"browsable": false,
																						"version": 0,
																						"length": 1024
																					},
																					"stamp": 14
																				},
																				"LabelProcessingError__": {
																					"type": "Label",
																					"data": [
																						"ProcessingError__"
																					],
																					"options": {
																						"label": "_ProcessingError"
																					},
																					"stamp": 37
																				},
																				"ProcessingError__": {
																					"type": "LongText",
																					"data": [
																						"ProcessingError__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"minNbLines": 3,
																						"label": "_ProcessingError",
																						"activable": true,
																						"width": "500",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"browsable": false
																					},
																					"stamp": 38
																				}
																			},
																			"stamp": 15
																		}
																	},
																	"stamp": 16,
																	"data": []
																}
															},
															"stamp": 17,
															"data": []
														}
													}
												},
												"form-content-left-2": {
													"type": "FlexibleFormLine",
													"options": {
														"version": 0
													},
													"*": {
														"SystemData": {
															"type": "PanelSystemData",
															"options": {
																"multicolumn": false,
																"version": 0,
																"border": {
																	"collapsed": false
																},
																"labelLength": 0,
																"label": "_System data",
																"hidden": true,
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": false,
																"hideTitleBar": false
															},
															"stamp": 18,
															"data": []
														}
													},
													"stamp": 19,
													"data": []
												},
												"form-content-left-3": {
													"type": "FlexibleFormLine",
													"options": {
														"version": 0
													},
													"*": {
														"NextProcess": {
															"type": "PanelNextProcess",
															"options": {
																"hidden": true,
																"version": 0,
																"border": {},
																"labelLength": 0,
																"label": "_Next Process",
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": false,
																"hideTitleBar": false
															},
															"stamp": 20,
															"data": []
														}
													},
													"stamp": 21,
													"data": []
												}
											},
											"stamp": 22,
											"data": []
										},
										"form-content-right": {
											"type": "FlexibleFormSplitter",
											"options": {
												"version": 1,
												"box": {
													"top": "0%",
													"left": "45%",
													"width": "55%",
													"height": null
												},
												"hidden": true,
												"hideSeparator": true
											},
											"*": {
												"form-content-right-1": {
													"type": "FlexibleFormLine",
													"options": {
														"version": 0
													},
													"*": {
														"PreviewPanel": {
															"type": "PanelPreview",
															"options": {
																"border": {},
																"version": 0,
																"labelLength": 0,
																"label": "_Document Preview",
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": false,
																"hideTitleBar": false
															},
															"*": {
																"Preview": {
																	"data": [
																		"previewdata"
																	],
																	"type": "Preview",
																	"options": {
																		"version": 0
																	},
																	"stamp": 23
																}
															},
															"stamp": 24,
															"data": []
														}
													},
													"stamp": 25,
													"data": []
												}
											},
											"stamp": 26,
											"data": []
										}
									},
									"stamp": 27,
									"data": []
								}
							},
							"stamp": 28,
							"data": []
						}
					},
					"stamp": 29,
					"data": []
				},
				"form-footer": {
					"type": "FlexibleFormFooter",
					"options": {
						"splitterType": "simple-h",
						"version": 0,
						"maxwidth": ""
					},
					"*": {
						"Save": {
							"type": "SubmitButton",
							"options": {
								"label": "_Save",
								"action": "save",
								"submit": true,
								"version": 0
							},
							"stamp": 30,
							"data": []
						},
						"Approve": {
							"type": "SubmitButton",
							"options": {
								"label": "_Approve",
								"action": "approve",
								"submit": true,
								"version": 0
							},
							"stamp": 31,
							"data": []
						},
						"Reject": {
							"type": "SubmitButton",
							"options": {
								"label": "_Reject",
								"action": "reject",
								"submit": true,
								"version": 0
							},
							"stamp": 32,
							"data": []
						},
						"Reprocess": {
							"type": "SubmitButton",
							"options": {
								"label": "_Reprocess",
								"action": "reprocess",
								"submit": true,
								"version": 0
							},
							"stamp": 33,
							"data": []
						},
						"Close": {
							"type": "SubmitButton",
							"options": {
								"label": "_Close",
								"action": "cancel",
								"submit": true,
								"version": 0
							},
							"stamp": 34,
							"data": []
						}
					},
					"stamp": 35,
					"data": []
				}
			},
			"stamp": 36,
			"data": []
		}
	},
	"stamps": 38,
	"data": []
}