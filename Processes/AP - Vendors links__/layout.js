{
	"*": {
		"Screen": {
			"type": "FlexibleFormLayout",
			"options": {
				"splitterType": "simple-v",
				"version": 0
			},
			"*": {
				"form-header": {
					"type": "FlexibleFormHeader",
					"options": {
						"hidePanelsMenu": false,
						"version": 0
					},
					"stamp": 2,
					"data": []
				},
				"form-center": {
					"type": "FlexibleFormCenter",
					"options": {
						"version": 0
					},
					"*": {
						"form-content": {
							"type": "FlexibleFormContent",
							"options": {
								"version": 0
							},
							"*": {
								"form-content-center": {
									"type": "FlexibleFormSplitter",
									"options": {
										"version": 1,
										"box": {
											"top": "0%",
											"left": "0%",
											"width": "100%"
										},
										"hidden": false
									},
									"*": {
										"form-content-center-1": {
											"type": "FlexibleFormLine",
											"data": [],
											"options": {
												"version": 0
											},
											"stamp": 9,
											"*": {
												"DataPanel": {
													"type": "PanelData",
													"options": {
														"border": {},
														"version": 0,
														"labelLength": 0,
														"iconURL": "",
														"TitleFontSize": "S",
														"hideActionButtons": false,
														"hideTitleBar": false,
														"backgroundcolor": "default",
														"labelsAlignment": "right",
														"label": "_Document data",
														"hideTitle": false
													},
													"*": {
														"FieldsManager": {
															"type": "FieldsManager",
															"options": {
																"controlsAssociation": {
																	"CompanyCode__": "LabelCompanyCode__",
																	"LabelCompanyCode__": "CompanyCode__",
																	"ShortLogin__": "LabelShortLogin__",
																	"LabelShortLogin__": "ShortLogin__",
																	"Number__": "LabelNumber__",
																	"LabelNumber__": "Number__",
																	"ShortLoginPAC__": "LabelShortLoginPAC__",
																	"LabelShortLoginPAC__": "ShortLoginPAC__",
																	"Configuration__": "LabelConfiguration__",
																	"LabelConfiguration__": "Configuration__"
																},
																"version": 0
															},
															"*": {
																"Grid": {
																	"type": "GridLayout",
																	"options": {
																		"lines": 5,
																		"columns": 2,
																		"version": 0,
																		"ctrlsPos": {
																			"CompanyCode__": {
																				"line": 1,
																				"column": 2
																			},
																			"LabelCompanyCode__": {
																				"line": 1,
																				"column": 1
																			},
																			"ShortLogin__": {
																				"line": 3,
																				"column": 2
																			},
																			"LabelShortLogin__": {
																				"line": 3,
																				"column": 1
																			},
																			"Number__": {
																				"line": 2,
																				"column": 2
																			},
																			"LabelNumber__": {
																				"line": 2,
																				"column": 1
																			},
																			"ShortLoginPAC__": {
																				"line": 4,
																				"column": 2
																			},
																			"LabelShortLoginPAC__": {
																				"line": 4,
																				"column": 1
																			},
																			"Configuration__": {
																				"line": 5,
																				"column": 2
																			},
																			"LabelConfiguration__": {
																				"line": 5,
																				"column": 1
																			}
																		},
																		"colspans": [
																			[],
																			[],
																			[],
																			[],
																			[]
																		]
																	},
																	"data": [
																		"fields"
																	],
																	"*": {
																		"LabelCompanyCode__": {
																			"type": "Label",
																			"data": [
																				"CompanyCode__"
																			],
																			"options": {
																				"label": "_CompanyCode",
																				"version": 0
																			},
																			"stamp": 14
																		},
																		"CompanyCode__": {
																			"type": "DatabaseComboBox",
																			"data": [
																				"CompanyCode__"
																			],
																			"options": {
																				"label": "_CompanyCode",
																				"activable": true,
																				"width": "180",
																				"PreFillFTS": true,
																				"fTSmaxRecords": "20",
																				"browsable": true,
																				"version": 0
																			},
																			"stamp": 15
																		},
																		"LabelNumber__": {
																			"type": "Label",
																			"data": [
																				"Number__"
																			],
																			"options": {
																				"label": "_Number",
																				"version": 0
																			},
																			"stamp": 16
																		},
																		"Number__": {
																			"type": "DatabaseComboBox",
																			"data": [
																				"Number__"
																			],
																			"options": {
																				"label": "_Number",
																				"activable": true,
																				"width": "180",
																				"PreFillFTS": true,
																				"fTSmaxRecords": "20",
																				"browsable": true,
																				"version": 0
																			},
																			"stamp": 17
																		},
																		"LabelShortLogin__": {
																			"type": "Label",
																			"data": [
																				"ShortLogin__"
																			],
																			"options": {
																				"label": "_ShortLogin",
																				"version": 0
																			},
																			"stamp": 19
																		},
																		"ShortLogin__": {
																			"type": "ShortText",
																			"data": [
																				"ShortLogin__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "_ShortLogin",
																				"activable": true,
																				"width": "180",
																				"length": 80,
																				"browsable": false,
																				"version": 0
																			},
																			"stamp": 20
																		},
																		"LabelShortLoginPAC__": {
																			"type": "Label",
																			"data": [
																				"ShortLoginPAC__"
																			],
																			"options": {
																				"label": "_ShortLoginPAC",
																				"version": 0
																			},
																			"stamp": 23
																		},
																		"ShortLoginPAC__": {
																			"type": "DatabaseComboBox",
																			"data": [
																				"ShortLoginPAC__"
																			],
																			"options": {
																				"version": 1,
																				"label": "_ShortLoginPAC",
																				"activable": true,
																				"width": "180",
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"PreFillFTS": true,
																				"fTSmaxRecords": "20",
																				"browsable": true
																			},
																			"stamp": 24
																		},
																		"LabelConfiguration__": {
																			"type": "Label",
																			"data": [
																				"Configuration__"
																			],
																			"options": {
																				"label": "_Configuration",
																				"version": 0
																			},
																			"stamp": 25
																		},
																		"Configuration__": {
																			"type": "DatabaseComboBox",
																			"data": [
																				"Configuration__"
																			],
																			"options": {
																				"version": 1,
																				"label": "_Configuration",
																				"activable": true,
																				"width": "180",
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"PreFillFTS": true,
																				"fTSmaxRecords": "20",
																				"browsable": true
																			},
																			"stamp": 26
																		}
																	},
																	"stamp": 8
																}
															},
															"stamp": 7,
															"data": []
														}
													},
													"stamp": 6,
													"data": []
												}
											}
										}
									},
									"stamp": 5,
									"data": []
								}
							},
							"stamp": 4,
							"data": []
						}
					},
					"stamp": 3,
					"data": []
				},
				"form-footer": {
					"type": "FlexibleFormFooter",
					"options": {
						"splitterType": "simple-h",
						"version": 0
					},
					"*": {
						"Save": {
							"type": "SubmitButton",
							"options": {
								"label": "_Save",
								"action": "savequit",
								"submit": true,
								"version": 0,
								"nextprocess": {
									"processName": "AP - Cost centers__",
									"attachmentsMode": "all",
									"willBeChild": true
								},
								"url": "",
								"shortCut": "S"
							},
							"stamp": 11,
							"data": []
						},
						"Close": {
							"type": "SubmitButton",
							"options": {
								"label": "_Close",
								"action": "cancel",
								"submit": true,
								"version": 0,
								"shortCut": "Q"
							},
							"stamp": 12,
							"data": []
						}
					},
					"stamp": 10,
					"data": []
				}
			},
			"stamp": 1,
			"data": []
		}
	},
	"stamps": 26,
	"data": []
}