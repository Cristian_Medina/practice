{
	"*": {
		"Screen": {
			"type": "FlexibleFormLayout",
			"options": {
				"splitterType": "simple-v",
				"version": 0
			},
			"*": {
				"form-header": {
					"type": "FlexibleFormHeader",
					"options": {
						"hidePanelsMenu": false,
						"version": 0
					},
					"stamp": 1,
					"data": []
				},
				"form-center": {
					"type": "FlexibleFormCenter",
					"options": {
						"version": 0
					},
					"*": {
						"form-content": {
							"type": "FlexibleFormContent",
							"options": {
								"version": 0
							},
							"*": {
								"form-content-center": {
									"type": "FlexibleFormSplitter",
									"options": {
										"version": 1,
										"box": {
											"top": "0%",
											"left": "0%",
											"width": "100%"
										},
										"hidden": false
									},
									"*": {
										"form-content-center-1": {
											"type": "FlexibleFormLine",
											"data": [],
											"options": {
												"version": 0
											},
											"stamp": 2,
											"*": {
												"DataPanel": {
													"type": "PanelData",
													"options": {
														"border": {},
														"version": 0,
														"labelLength": 0,
														"label": "_Document data",
														"iconURL": "",
														"TitleFontSize": "S",
														"hideActionButtons": false,
														"hideTitleBar": false,
														"backgroundcolor": "default",
														"labelsAlignment": "right",
														"hideTitle": false
													},
													"*": {
														"FieldsManager": {
															"type": "FieldsManager",
															"options": {
																"controlsAssociation": {
																	"Account__": "LabelAccount__",
																	"LabelAccount__": "Account__",
																	"Description__": "LabelDescription__",
																	"LabelDescription__": "Description__",
																	"CompanyCode__": "LabelCompanyCode__",
																	"LabelCompanyCode__": "CompanyCode__",
																	"Allocable1__": "LabelAllocable1__",
																	"LabelAllocable1__": "Allocable1__",
																	"Allocable2__": "LabelAllocable2__",
																	"LabelAllocable2__": "Allocable2__",
																	"Allocable3__": "LabelAllocable3__",
																	"LabelAllocable3__": "Allocable3__",
																	"Allocable4__": "LabelAllocable4__",
																	"LabelAllocable4__": "Allocable4__",
																	"Allocable5__": "LabelAllocable5__",
																	"LabelAllocable5__": "Allocable5__",
																	"Group__": "LabelGroup__",
																	"LabelGroup__": "Group__",
																	"Manager__": "LabelManager__",
																	"LabelManager__": "Manager__"
																},
																"version": 0
															},
															"*": {
																"Grid": {
																	"type": "GridLayout",
																	"options": {
																		"lines": 10,
																		"columns": 2,
																		"version": 0,
																		"ctrlsPos": {
																			"Account__": {
																				"line": 2,
																				"column": 2
																			},
																			"LabelAccount__": {
																				"line": 2,
																				"column": 1
																			},
																			"Description__": {
																				"line": 3,
																				"column": 2
																			},
																			"LabelDescription__": {
																				"line": 3,
																				"column": 1
																			},
																			"CompanyCode__": {
																				"line": 1,
																				"column": 2
																			},
																			"LabelCompanyCode__": {
																				"line": 1,
																				"column": 1
																			},
																			"Allocable1__": {
																				"line": 5,
																				"column": 2
																			},
																			"LabelAllocable1__": {
																				"line": 5,
																				"column": 1
																			},
																			"Allocable2__": {
																				"line": 6,
																				"column": 2
																			},
																			"LabelAllocable2__": {
																				"line": 6,
																				"column": 1
																			},
																			"Allocable3__": {
																				"line": 7,
																				"column": 2
																			},
																			"LabelAllocable3__": {
																				"line": 7,
																				"column": 1
																			},
																			"Allocable4__": {
																				"line": 8,
																				"column": 2
																			},
																			"LabelAllocable4__": {
																				"line": 8,
																				"column": 1
																			},
																			"Allocable5__": {
																				"line": 9,
																				"column": 2
																			},
																			"LabelAllocable5__": {
																				"line": 9,
																				"column": 1
																			},
																			"Group__": {
																				"line": 4,
																				"column": 2
																			},
																			"LabelGroup__": {
																				"line": 4,
																				"column": 1
																			},
																			"Manager__": {
																				"line": 10,
																				"column": 2
																			},
																			"LabelManager__": {
																				"line": 10,
																				"column": 1
																			}
																		},
																		"colspans": [
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[]
																		]
																	},
																	"data": [
																		"fields"
																	],
																	"*": {
																		"LabelCompanyCode__": {
																			"type": "Label",
																			"data": [
																				"CompanyCode__"
																			],
																			"options": {
																				"label": "Company code",
																				"version": 0
																			},
																			"stamp": 3
																		},
																		"CompanyCode__": {
																			"type": "DatabaseComboBox",
																			"data": [
																				"CompanyCode__"
																			],
																			"options": {
																				"browsable": true,
																				"label": "Company code",
																				"activable": true,
																				"width": 500,
																				"fTSmaxRecords": "20",
																				"version": 0,
																				"PreFillFTS": true
																			},
																			"stamp": 4
																		},
																		"LabelAccount__": {
																			"type": "Label",
																			"data": [
																				"Account__"
																			],
																			"options": {
																				"label": "Account",
																				"version": 0
																			},
																			"stamp": 5
																		},
																		"Account__": {
																			"type": "ShortText",
																			"data": [
																				"Account__"
																			],
																			"options": {
																				"label": "Account",
																				"activable": true,
																				"width": "500",
																				"length": 30,
																				"version": 0,
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default"
																			},
																			"stamp": 6
																		},
																		"LabelDescription__": {
																			"type": "Label",
																			"data": [
																				"Description__"
																			],
																			"options": {
																				"label": "Description",
																				"version": 0
																			},
																			"stamp": 7
																		},
																		"Description__": {
																			"type": "ShortText",
																			"data": [
																				"Description__"
																			],
																			"options": {
																				"label": "Description",
																				"activable": true,
																				"width": "500",
																				"length": 256,
																				"version": 0,
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default"
																			},
																			"stamp": 8
																		},
																		"LabelGroup__": {
																			"type": "Label",
																			"data": [
																				"Group__"
																			],
																			"options": {
																				"label": "_Group",
																				"version": 0
																			},
																			"stamp": 30
																		},
																		"Group__": {
																			"type": "ShortText",
																			"data": [
																				"Group__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "_Group",
																				"activable": true,
																				"width": "500",
																				"browsable": false,
																				"version": 0,
																				"length": 64
																			},
																			"stamp": 31
																		},
																		"LabelAllocable1__": {
																			"type": "Label",
																			"data": [
																				"Allocable1__"
																			],
																			"options": {
																				"label": "Allocable1",
																				"version": 0
																			},
																			"stamp": 9
																		},
																		"Allocable1__": {
																			"type": "CheckBox",
																			"data": [
																				"Allocable1__"
																			],
																			"options": {
																				"label": "Allocable1",
																				"activable": true,
																				"version": 0
																			},
																			"stamp": 10
																		},
																		"LabelAllocable2__": {
																			"type": "Label",
																			"data": [
																				"Allocable2__"
																			],
																			"options": {
																				"label": "Allocable2",
																				"version": 0
																			},
																			"stamp": 11
																		},
																		"Allocable2__": {
																			"type": "CheckBox",
																			"data": [
																				"Allocable2__"
																			],
																			"options": {
																				"label": "Allocable2",
																				"activable": true,
																				"version": 0
																			},
																			"stamp": 12
																		},
																		"LabelAllocable3__": {
																			"type": "Label",
																			"data": [
																				"Allocable3__"
																			],
																			"options": {
																				"label": "Allocable3",
																				"version": 0
																			},
																			"stamp": 13
																		},
																		"Allocable3__": {
																			"type": "CheckBox",
																			"data": [
																				"Allocable3__"
																			],
																			"options": {
																				"label": "Allocable3",
																				"activable": true,
																				"version": 0
																			},
																			"stamp": 14
																		},
																		"LabelAllocable4__": {
																			"type": "Label",
																			"data": [
																				"Allocable4__"
																			],
																			"options": {
																				"label": "Allocable4",
																				"version": 0
																			},
																			"stamp": 15
																		},
																		"Allocable4__": {
																			"type": "CheckBox",
																			"data": [
																				"Allocable4__"
																			],
																			"options": {
																				"label": "Allocable4",
																				"activable": true,
																				"version": 0
																			},
																			"stamp": 16
																		},
																		"LabelAllocable5__": {
																			"type": "Label",
																			"data": [
																				"Allocable5__"
																			],
																			"options": {
																				"label": "Allocable5",
																				"version": 0
																			},
																			"stamp": 17
																		},
																		"Allocable5__": {
																			"type": "CheckBox",
																			"data": [
																				"Allocable5__"
																			],
																			"options": {
																				"label": "Allocable5",
																				"activable": true,
																				"version": 0
																			},
																			"stamp": 18
																		},
																		"LabelManager__": {
																			"type": "Label",
																			"data": [
																				"Select_from_a_table__"
																			],
																			"options": {
																				"label": "Manager"
																			},
																			"stamp": 34
																		},
																		"Manager__": {
																			"type": "DatabaseComboBox",
																			"data": [
																				"Manager__"
																			],
																			"options": {
																				"version": 1,
																				"label": "Manager",
																				"activable": true,
																				"width": "500",
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"PreFillFTS": true,
																				"fTSmaxRecords": "20",
																				"length": 80,
																				"browsable": true
																			},
																			"stamp": 35
																		}
																	},
																	"stamp": 19
																}
															},
															"stamp": 20,
															"data": []
														}
													},
													"stamp": 21,
													"data": []
												}
											}
										}
									},
									"stamp": 22,
									"data": []
								}
							},
							"stamp": 23,
							"data": []
						}
					},
					"stamp": 24,
					"data": []
				},
				"form-footer": {
					"type": "FlexibleFormFooter",
					"options": {
						"splitterType": "simple-h",
						"version": 0
					},
					"*": {
						"Save": {
							"type": "SubmitButton",
							"options": {
								"label": "_Save",
								"action": "savequit",
								"submit": true,
								"version": 0,
								"shortCut": "S"
							},
							"stamp": 25,
							"data": []
						},
						"Close": {
							"type": "SubmitButton",
							"options": {
								"label": "_Close",
								"action": "cancel",
								"submit": true,
								"version": 0,
								"shortCut": "Q"
							},
							"stamp": 26,
							"data": []
						},
						"Delete": {
							"type": "SubmitButton",
							"options": {
								"label": "_Delete",
								"action": "delete",
								"submit": true,
								"version": 0,
								"disabled": true
							},
							"stamp": 27,
							"data": []
						}
					},
					"stamp": 28,
					"data": []
				}
			},
			"stamp": 29,
			"data": []
		}
	},
	"stamps": 35,
	"data": []
}