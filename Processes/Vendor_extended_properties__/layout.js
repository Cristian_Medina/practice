{
	"*": {
		"Screen": {
			"type": "FlexibleFormLayout",
			"options": {
				"backColor": "text-backgroundcolor-color7",
				"splitterType": "vertical",
				"splitterOptions": {
					"sizeable": false,
					"sidebarWidth": 320,
					"unit": "px",
					"fixedSlider": true
				},
				"style": "FlexibleFormLight",
				"version": 0
			},
			"*": {
				"form-header": {
					"type": "FlexibleFormHeader",
					"options": {
						"hidePanelsMenu": true,
						"hideDesignButton": true,
						"version": 0
					},
					"stamp": 1,
					"data": []
				},
				"form-center": {
					"type": "FlexibleFormCenter",
					"options": {
						"version": 0
					},
					"*": {
						"form-content": {
							"type": "FlexibleFormContent",
							"options": {
								"version": 0
							},
							"*": {
								"form-content-left": {
									"type": "FlexibleFormSplitter",
									"options": {
										"version": 1,
										"hidden": false,
										"box": {
											"top": "0px",
											"left": "0px",
											"width": "320px"
										}
									},
									"*": {
										"form-content-left-1": {
											"type": "FlexibleFormLine",
											"data": [],
											"options": {
												"version": 0
											},
											"stamp": 2,
											"*": {
												"VendorDetailsPane": {
													"type": "PanelData",
													"options": {
														"border": {},
														"version": 0,
														"labelLength": 0,
														"iconURL": "",
														"TitleFontSize": "M",
														"hideActionButtons": true,
														"hideTitleBar": true,
														"hideTitle": true,
														"backgroundcolor": "default",
														"labelsAlignment": "right",
														"label": "_VendorDetails",
														"leftImageURL": "",
														"removeMargins": false,
														"elementsAlignment": "left"
													},
													"*": {
														"FieldsManager": {
															"type": "FieldsManager",
															"options": {
																"controlsAssociation": {
																	"Login__": "LabelLogin__",
																	"LabelLogin__": "Login__"
																},
																"version": 0
															},
															"*": {
																"Grid": {
																	"type": "GridLayout",
																	"options": {
																		"lines": 13,
																		"columns": 2,
																		"version": 0,
																		"ctrlsPos": {
																			"Spacer_line1__": {
																				"line": 1,
																				"column": 1
																			},
																			"LabelLogin__": {
																				"line": 2,
																				"column": 1
																			},
																			"Login__": {
																				"line": 2,
																				"column": 2
																			},
																			"Vendor_Info__": {
																				"line": 3,
																				"column": 1
																			},
																			"Spacer_line2__": {
																				"line": 4,
																				"column": 1
																			},
																			"VendorDetailsName__": {
																				"line": 5,
																				"column": 1
																			},
																			"Spacer_line3__": {
																				"line": 6,
																				"column": 1
																			},
																			"VendorDetailsCompany__": {
																				"line": 7,
																				"column": 1
																			},
																			"VendorDetailsID__": {
																				"line": 8,
																				"column": 1
																			},
																			"VendorDetailsEmail__": {
																				"line": 9,
																				"column": 1
																			},
																			"VendorDetailsPhoneNumber__": {
																				"line": 10,
																				"column": 1
																			},
																			"Spacer_line4__": {
																				"line": 11,
																				"column": 1
																			},
																			"ActionMenu__": {
																				"line": 12,
																				"column": 1
																			},
																			"Spacer_line5__": {
																				"line": 13,
																				"column": 1
																			}
																		},
																		"colspans": [
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			],
																			[],
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			],
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			],
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			],
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			],
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			],
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			],
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			],
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			],
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			],
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			],
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			]
																		]
																	},
																	"data": [
																		"fields"
																	],
																	"*": {
																		"Spacer_line1__": {
																			"type": "Spacer",
																			"data": false,
																			"options": {
																				"height": "",
																				"width": "",
																				"color": "default",
																				"label": "Spacer line",
																				"version": 0
																			},
																			"stamp": 3
																		},
																		"LabelLogin__": {
																			"type": "Label",
																			"data": [
																				"Login__"
																			],
																			"options": {
																				"label": "_Login",
																				"hidden": true,
																				"version": 0
																			},
																			"stamp": 4
																		},
																		"Login__": {
																			"type": "ShortText",
																			"data": [
																				"Login__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"version": 1,
																				"label": "_Login",
																				"activable": true,
																				"width": 175,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"hidden": true,
																				"browsable": false
																			},
																			"stamp": 5
																		},
																		"Vendor_Info__": {
																			"type": "HTML",
																			"data": false,
																			"options": {
																				"label": "HTML content",
																				"htmlContent": "<div>\n\t<div id=\"vendor\">\n\t\t<i class=\"fa fa-user-circle fa-5x\" aria-hidden=\"true\"></i>\n\t</div>\n</div>",
																				"width": "100%",
																				"version": 0,
																				"css": "@ #vendor { text-align:center;}@ "
																			},
																			"stamp": 6
																		},
																		"Spacer_line2__": {
																			"type": "Spacer",
																			"data": false,
																			"options": {
																				"height": "",
																				"width": "",
																				"color": "default",
																				"label": "Spacer line10",
																				"version": 0
																			},
																			"stamp": 7
																		},
																		"VendorDetailsName__": {
																			"type": "Text",
																			"data": false,
																			"options": {
																				"marge": 0,
																				"textSize": "XXL",
																				"textAlignment": "center",
																				"textStyle": "default",
																				"textColor": "default",
																				"backgroundcolor": "default",
																				"image_url": "",
																				"iconClass": "",
																				"label": "Vendor Name",
																				"version": 0
																			},
																			"stamp": 8
																		},
																		"Spacer_line3__": {
																			"type": "Spacer",
																			"data": false,
																			"options": {
																				"height": "",
																				"width": "",
																				"color": "default",
																				"label": "Spacer line8",
																				"version": 0
																			},
																			"stamp": 9
																		},
																		"VendorDetailsID__": {
																			"type": "Text",
																			"data": false,
																			"options": {
																				"marge": 0,
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"backgroundcolor": "default",
																				"image_url": "",
																				"iconClass": "fa fa-key fa-fw",
																				"label": "",
																				"version": 0
																			},
																			"stamp": 10
																		},
																		"VendorDetailsCompany__": {
																			"type": "Text",
																			"data": false,
																			"options": {
																				"marge": 0,
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"backgroundcolor": "default",
																				"image_url": "",
																				"iconClass": "fa fa-building-o fa-fw",
																				"label": "",
																				"version": 0
																			},
																			"stamp": 11
																		},
																		"VendorDetailsEmail__": {
																			"type": "Text",
																			"data": false,
																			"options": {
																				"marge": 0,
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"backgroundcolor": "default",
																				"image_url": "",
																				"iconClass": "fa fa-at fa-fw",
																				"label": "",
																				"version": 0
																			},
																			"stamp": 12
																		},
																		"VendorDetailsPhoneNumber__": {
																			"type": "Text",
																			"data": false,
																			"options": {
																				"marge": 0,
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"backgroundcolor": "default",
																				"image_url": "",
																				"iconClass": "fa fa-phone fa-fw",
																				"label": "",
																				"version": 0
																			},
																			"stamp": 13
																		},
																		"Spacer_line4__": {
																			"type": "Spacer",
																			"data": false,
																			"options": {
																				"height": "",
																				"width": "",
																				"color": "default",
																				"label": "Spacer line",
																				"version": 0
																			},
																			"stamp": 14
																		},
																		"ActionMenu__": {
																			"type": "HTML",
																			"data": false,
																			"options": {
																				"label": "ActionMenu__",
																				"width": "100%",
																				"hidden": false,
																				"version": 0,
																				"htmlContent": "<input id=\"onActionLoad\" type=\"hidden\" onclick=\"onActionLoad(event);\">\n<div class=\"nav-body\">\n\t<ul class=\"nav-topbar\">\n\t\t<li class=\"nav-item\">\n\t\t\t<a class=\"nav-link\" id=\"MoreDetails\" onclick=\"OnClickTab('moreDetails')\">_More_Details</a>\n\t\t</li>\n\t\t<li class=\"nav-item-divider\" />\n\t\t<li class=\"nav-item\">\n\t\t\t<a class=\"nav-link\" id=\"Edit\" onclick=\"EditAction()\">_Edit</a>\n\t\t</li>\n\t\t<li class=\"nav-item-divider\" />\n\t\t<li class=\"nav-item\">\n\t\t\t<a class=\"nav-link\" id=\"ResendWelcomeEmail\" onclick=\"ResendWelcomeEmailAction()\">_Resend welcome email</a>\n\t\t</li>\n\t\t<li class=\"nav-item-divider\" />\n\t\t<li class=\"nav-item\">\n\t\t\t<a class=\"nav-link\" id=\"ResetPassword\" onclick=\"ResetPasswordAction()\">_Reset password</a>\n\t\t</li>\n\t</ul>\n</div>\n<script>\n\twindow.postMessage({ eventName: \"onActionLoad\", control: \"ActionMenu__\" }, document.location.origin);\n\tfunction onActionLoad(evt)\n\t{\n\t\tfor (var element in evt.tabs)\n\t\t{\n\t\t\tvar elem = document.getElementById(element);\n\t\t\tif (elem)\n\t\t\t\telem.innerText = evt.tabs[element];\n\t\t}\n\t}\n\n\tfunction EditAction()\n\t{\n\t\twindow.postMessage({ eventName: \"EditAction\", control: \"ActionMenu__\" }, document.location.origin);\n\t}\n\n\tfunction ResendWelcomeEmailAction()\n\t{\n\t\twindow.postMessage({ eventName: \"WelcomeEmailAction\", control: \"ActionMenu__\" }, document.location.origin);\n\t}\n\n\tfunction ResetPasswordAction()\n\t{\n\t\twindow.postMessage({ eventName: \"ResetPasswordAction\", control: \"ActionMenu__\" }, document.location.origin);\n\t}\n\tfunction MoreDetails()\n\t{\n\t\twindow.postMessage({ eventName: \"MoreDetails\", control: \"ActionMenu__\" }, document.location.origin);\n\t}\n</script>"
																			},
																			"stamp": 15
																		},
																		"Spacer_line5__": {
																			"type": "Spacer",
																			"data": false,
																			"options": {
																				"height": "",
																				"width": "",
																				"color": "default",
																				"label": "Spacer line",
																				"version": 0
																			},
																			"stamp": 16
																		}
																	},
																	"stamp": 17
																}
															},
															"stamp": 18,
															"data": []
														}
													},
													"stamp": 19,
													"data": []
												}
											}
										},
										"form-content-left-2": {
											"type": "FlexibleFormLine",
											"data": [],
											"options": {
												"version": 0
											},
											"stamp": 20,
											"*": {
												"MenuPane": {
													"type": "PanelData",
													"data": [],
													"options": {
														"border": {},
														"labelLength": 0,
														"iconURL": "",
														"TitleFontSize": "M",
														"hideActionButtons": true,
														"hideTitleBar": true,
														"hideTitle": true,
														"backgroundcolor": "default",
														"labelsAlignment": "right",
														"label": "_Menu",
														"leftImageURL": "",
														"removeMargins": false,
														"elementsAlignment": "left",
														"version": 0
													},
													"stamp": 21,
													"*": {
														"FieldsManager": {
															"type": "FieldsManager",
															"data": [],
															"options": {
																"controlsAssociation": {},
																"version": 0
															},
															"stamp": 22,
															"*": {
																"Grid": {
																	"type": "GridLayout",
																	"data": [
																		"fields"
																	],
																	"options": {
																		"ctrlsPos": {
																			"NavMenu__": {
																				"line": 1,
																				"column": 1
																			}
																		},
																		"lines": 1,
																		"columns": 2,
																		"colspans": [
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			]
																		],
																		"version": 0
																	},
																	"stamp": 23,
																	"*": {
																		"NavMenu__": {
																			"type": "HTML",
																			"data": false,
																			"options": {
																				"label": "HTML content",
																				"htmlContent": "<input id=\"onLoad\" type=\"hidden\" onclick=\"OnLoad(event);\">\n<input id=\"onPackageLoad\" type=\"hidden\" onclick=\"OnPackageLoad(event);\">\n<div id=\"NavMenu\" class=\"nav-body\" style=\"\">\n\t<ul class=\"nav-sidebar\">\n\t\t<li class=\"nav-item\">\n\t\t\t<a id=\"vendors\" style=\"display:none\" data-package=\"P2P_AP\" onclick=\"OnClickTab(this.id);\" href=\"#\" class=\"nav-link\">\n\t\t\t\t<span class=\"icon fa fa-building-o\"></span>\n\t\t\t\t<span id=\"vendorsLbl\"/>\n\t\t\t</a>\n\t\t</li>\n\t\t<li class=\"nav-item\">\n\t\t\t<a id=\"activity\" onclick=\"OnClickTab(this.id);\" href=\"#\" class=\"nav-link\">\n\t\t\t\t<span class=\"icon fa fa-line-chart\"></span>\n\t\t\t\t<span id=\"activityLbl\"/>\n\t\t\t</a>\n\t\t</li>\n\t\t<li class=\"nav-item\">\n\t\t\t<a id=\"settings\" style=\"display:none\" data-package=\"P2P\" href=\"#\" onclick=\"OnClickTab(this.id);\"class=\"nav-link\">\n\t\t\t\t<span class=\"icon fa fa-gear\"></span>\n\t\t\t\t<span id=\"settingsLbl\"/>\n\t\t\t</a>\n\t\t</li>\n\t\t<li class=\"nav-item\" hidden>\n\t\t\t<a id=\"moreDetails\" onclick=\"OnClickTab(this.id);\" href=\"#\" class=\"nav-link\">\n\t\t\t</a>\n\t\t</li>\n\t</ul>\n</div>\n<script>\n\twindow.postMessage({eventName: \"onLoad\", control: \"NavMenu__\"}, document.location.origin);\n    var tabItems = document.getElementById(\"NavMenu\").getElementsByClassName(\"nav-link\");\n\tfunction OnClickTab(tabID) {\n        for (var i = 0; i < tabItems.length; i++)\n\t\t\ttabItems[i].className = \"nav-link\";\n\t\t\n\t\tvar tab = document.getElementById(tabID);\n\t\ttab.className = \"nav-link active\";\n\t\twindow.postMessage({eventName: \"onClick\", control: \"NavMenu__\", args: tab.id}, document.location.origin);\n\t}\n\tfunction OnLoad(evt) {\n\t\tfor (var lbl in evt.tabs)\n\t\t{\n\t\t\tvar elem = document.getElementById(lbl + \"Lbl\");\n\t\t\tif (elem)\n\t\t\t\telem.innerText = evt.tabs[lbl];\n\t\t}\n\t\tfor (var i = 0; i < tabItems.length; i++) {\n\t\t\tvar package = tabItems[i].getAttribute(\"data-package\");\n\t\t\tif (package && evt.appInstances[package])\n\t\t\t\ttabItems[i].style.display = \"inherit\";\n\t\t}\n\t\tvar startMenu = document.getElementById(evt.startMenu);\n\t\tstartMenu.className = \"nav-link active\";\n\t}\n</script>",
																				"width": "100%",
																				"version": 0
																			},
																			"stamp": 24
																		}
																	}
																}
															}
														}
													}
												}
											}
										}
									},
									"stamp": 25,
									"data": []
								}
							},
							"stamp": 26,
							"data": []
						},
						"form-content-right": {
							"type": "FlexibleFormSplitter",
							"options": {
								"version": 1,
								"hidden": false,
								"box": {
									"top": "0px",
									"left": "320px",
									"width": "calc(100 % - 320px)"
								}
							},
							"*": {
								"form-content-right-2": {
									"type": "FlexibleFormLine",
									"data": [],
									"options": {
										"version": 0
									},
									"stamp": 27,
									"*": {
										"GeneralInformationPane": {
											"type": "PanelData",
											"data": [],
											"options": {
												"border": {},
												"labelLength": 0,
												"iconURL": "",
												"TitleFontSize": "S",
												"hideActionButtons": true,
												"hideTitleBar": false,
												"hideTitle": false,
												"backgroundcolor": "default",
												"labelsAlignment": "left",
												"label": "_General_Information_Pane",
												"hidden": false,
												"leftImageURL": "",
												"removeMargins": false,
												"elementsAlignment": "left",
												"version": 0
											},
											"stamp": 28,
											"*": {
												"FieldsManager": {
													"type": "FieldsManager",
													"data": [],
													"options": {
														"controlsAssociation": {
															"Company__": "LabelCompany__",
															"LabelCompany__": "Company__",
															"Title__": "LabelTitle__",
															"LabelTitle__": "Title__",
															"FirstName__": "LabelFirstName__",
															"LabelFirstName__": "FirstName__",
															"MiddleName__": "LabelMiddleName__",
															"LabelMiddleName__": "MiddleName__",
															"LastName__": "LabelLastName__",
															"LabelLastName__": "LastName__",
															"DisplayName_": "LabelDisplayName__",
															"LabelDisplayName__": "DisplayName__",
															"AdditionalInformation__": "LabelAdditionalInformation__",
															"LabelAdditionalInformation__": "AdditionalInformation__",
															"EmailAddress__": "LabelEmailAddress__",
															"LabelEmailAddress__": "EmailAddress__",
															"PhoneNumber__": "LabelPhoneNumber__",
															"LabelPhoneNumber__": "PhoneNumber__",
															"MobileNumber__": "LabelMobileNumber__",
															"LabelMobileNumber__": "MobileNumber__",
															"FaxNumber__": "LabelFaxNumber__",
															"LabelFaxNumber__": "FaxNumber__"
														},
														"version": 0
													},
													"stamp": 29,
													"*": {
														"Grid": {
															"type": "GridLayout",
															"data": [
																"fields"
															],
															"options": {
																"ctrlsPos": {
																	"Company__": {
																		"line": 1,
																		"column": 2
																	},
																	"LabelCompany__": {
																		"line": 1,
																		"column": 1
																	},
																	"Title__": {
																		"line": 2,
																		"column": 2
																	},
																	"LabelTitle__": {
																		"line": 2,
																		"column": 1
																	},
																	"FirstName__": {
																		"line": 3,
																		"column": 2
																	},
																	"LabelFirstName__": {
																		"line": 3,
																		"column": 1
																	},
																	"MiddleName__": {
																		"line": 4,
																		"column": 2
																	},
																	"LabelMiddleName__": {
																		"line": 4,
																		"column": 1
																	},
																	"LastName__": {
																		"line": 5,
																		"column": 2
																	},
																	"LabelLastName__": {
																		"line": 5,
																		"column": 1
																	},
																	"DisplayName__": {
																		"line": 6,
																		"column": 2
																	},
																	"LabelDisplayName__": {
																		"line": 6,
																		"column": 1
																	},
																	"AdditionalInformation__": {
																		"line": 7,
																		"column": 2
																	},
																	"LabelAdditionalInformation__": {
																		"line": 7,
																		"column": 1
																	},
																	"EmailAddress__": {
																		"line": 8,
																		"column": 2
																	},
																	"LabelEmailAddress__": {
																		"line": 8,
																		"column": 1
																	},
																	"PhoneNumber__": {
																		"line": 9,
																		"column": 2
																	},
																	"LabelPhoneNumber__": {
																		"line": 9,
																		"column": 1
																	},
																	"MobileNumber__": {
																		"line": 10,
																		"column": 2
																	},
																	"LabelMobileNumber__": {
																		"line": 10,
																		"column": 1
																	},
																	"FaxNumber__": {
																		"line": 11,
																		"column": 2
																	},
																	"LabelFaxNumber__": {
																		"line": 11,
																		"column": 1
																	},
																	"GeneralInformationPane_Loader__": {
																		"line": 12,
																		"column": 1
																	}
																},
																"lines": 12,
																"columns": 2,
																"colspans": [
																	[],
																	[],
																	[],
																	[],
																	[],
																	[],
																	[],
																	[],
																	[],
																	[],
																	[],
																	[
																		{
																			"index": 0,
																			"length": 2
																		}
																	]
																],
																"version": 0
															},
															"stamp": 30,
															"*": {
																"LabelCompany__": {
																	"type": "Label",
																	"data": [],
																	"options": {
																		"label": "_Company",
																		"version": 0
																	},
																	"stamp": 31
																},
																"Company__": {
																	"type": "ShortText",
																	"data": [],
																	"options": {
																		"textSize": "S",
																		"textAlignment": "left",
																		"textStyle": "default",
																		"textColor": "default",
																		"version": 1,
																		"label": "_Company",
																		"activable": true,
																		"width": 230,
																		"helpText": "",
																		"helpURL": "",
																		"helpFormat": "HTML Format",
																		"readonly": true,
																		"notInDB": true,
																		"browsable": false,
																		"dataType": "String",
																		"autocompletable": false
																	},
																	"stamp": 32
																},
																"LabelTitle__": {
																	"type": "Label",
																	"data": [],
																	"options": {
																		"label": "_Title",
																		"version": 0
																	},
																	"stamp": 33
																},
																"Title__": {
																	"type": "ShortText",
																	"data": [],
																	"options": {
																		"textSize": "S",
																		"textAlignment": "left",
																		"textStyle": "default",
																		"textColor": "default",
																		"version": 1,
																		"label": "_Title",
																		"activable": true,
																		"width": 230,
																		"helpText": "",
																		"helpURL": "",
																		"helpFormat": "HTML Format",
																		"readonly": true,
																		"notInDB": true,
																		"browsable": false,
																		"dataType": "String",
																		"autocompletable": false
																	},
																	"stamp": 34
																},
																"LabelFirstName__": {
																	"type": "Label",
																	"data": [],
																	"options": {
																		"label": "_FirstName",
																		"version": 0
																	},
																	"stamp": 35
																},
																"FirstName__": {
																	"type": "ShortText",
																	"data": [],
																	"options": {
																		"textSize": "S",
																		"textAlignment": "left",
																		"textStyle": "default",
																		"textColor": "default",
																		"version": 1,
																		"label": "_FirstName",
																		"activable": true,
																		"width": 230,
																		"helpText": "",
																		"helpURL": "",
																		"helpFormat": "HTML Format",
																		"readonly": true,
																		"notInDB": true,
																		"browsable": false,
																		"dataType": "String",
																		"autocompletable": false
																	},
																	"stamp": 36
																},
																"LabelMiddleName__": {
																	"type": "Label",
																	"data": [],
																	"options": {
																		"label": "_MiddleName",
																		"version": 0
																	},
																	"stamp": 37
																},
																"MiddleName__": {
																	"type": "ShortText",
																	"data": [],
																	"options": {
																		"textSize": "S",
																		"textAlignment": "left",
																		"textStyle": "default",
																		"textColor": "default",
																		"version": 1,
																		"label": "_MiddleName",
																		"activable": true,
																		"width": 230,
																		"helpText": "",
																		"helpURL": "",
																		"helpFormat": "HTML Format",
																		"readonly": true,
																		"notInDB": true,
																		"browsable": false,
																		"dataType": "String",
																		"autocompletable": false
																	},
																	"stamp": 38
																},
																"LabelLastName__": {
																	"type": "Label",
																	"data": [],
																	"options": {
																		"label": "_LastName",
																		"version": 0
																	},
																	"stamp": 39
																},
																"LastName__": {
																	"type": "ShortText",
																	"data": [],
																	"options": {
																		"textSize": "S",
																		"textAlignment": "left",
																		"textStyle": "default",
																		"textColor": "default",
																		"version": 1,
																		"label": "_LastName",
																		"activable": true,
																		"width": 230,
																		"helpText": "",
																		"helpURL": "",
																		"helpFormat": "HTML Format",
																		"readonly": true,
																		"notInDB": true,
																		"browsable": false,
																		"dataType": "String",
																		"autocompletable": false
																	},
																	"stamp": 40
																},
																"LabelDisplayName__": {
																	"type": "Label",
																	"data": [],
																	"options": {
																		"label": "_DisplayName",
																		"version": 0
																	},
																	"stamp": 41
																},
																"DisplayName__": {
																	"type": "ShortText",
																	"data": [],
																	"options": {
																		"textSize": "S",
																		"textAlignment": "left",
																		"textStyle": "default",
																		"textColor": "default",
																		"version": 1,
																		"label": "_DisplayName",
																		"activable": true,
																		"width": 230,
																		"helpText": "",
																		"helpURL": "",
																		"helpFormat": "HTML Format",
																		"readonly": true,
																		"notInDB": true,
																		"browsable": false,
																		"dataType": "String",
																		"autocompletable": false
																	},
																	"stamp": 42
																},
																"LabelAdditionalInformation__": {
																	"type": "Label",
																	"data": [],
																	"options": {
																		"label": "_Additional information",
																		"version": 0
																	},
																	"stamp": 43
																},
																"AdditionalInformation__": {
																	"type": "ShortText",
																	"data": [],
																	"options": {
																		"textSize": "S",
																		"textAlignment": "left",
																		"textStyle": "default",
																		"textColor": "default",
																		"version": 1,
																		"label": "_Additional information",
																		"activable": true,
																		"width": 230,
																		"helpText": "",
																		"helpURL": "",
																		"helpFormat": "HTML Format",
																		"readonly": true,
																		"notInDB": true,
																		"browsable": false,
																		"dataType": "String",
																		"autocompletable": false
																	},
																	"stamp": 44
																},
																"LabelEmailAddress__": {
																	"type": "Label",
																	"data": [],
																	"options": {
																		"label": "_EmailAddress",
																		"version": 0
																	},
																	"stamp": 45
																},
																"EmailAddress__": {
																	"type": "ShortText",
																	"data": [],
																	"options": {
																		"textSize": "S",
																		"textAlignment": "left",
																		"textStyle": "default",
																		"textColor": "default",
																		"version": 1,
																		"label": "_EmailAddress",
																		"activable": true,
																		"width": 230,
																		"helpText": "",
																		"helpURL": "",
																		"helpFormat": "HTML Format",
																		"readonly": true,
																		"notInDB": true,
																		"browsable": false,
																		"dataType": "String",
																		"autocompletable": false
																	},
																	"stamp": 46
																},
																"LabelPhoneNumber__": {
																	"type": "Label",
																	"data": [],
																	"options": {
																		"label": "_PhoneNumber",
																		"version": 0
																	},
																	"stamp": 47
																},
																"PhoneNumber__": {
																	"type": "ShortText",
																	"data": [],
																	"options": {
																		"textSize": "S",
																		"textAlignment": "left",
																		"textStyle": "default",
																		"textColor": "default",
																		"version": 1,
																		"label": "_PhoneNumber",
																		"activable": true,
																		"width": 230,
																		"helpText": "",
																		"helpURL": "",
																		"helpFormat": "HTML Format",
																		"readonly": true,
																		"notInDB": true,
																		"browsable": false,
																		"dataType": "String",
																		"autocompletable": false
																	},
																	"stamp": 48
																},
																"LabelMobileNumber__": {
																	"type": "Label",
																	"data": [],
																	"options": {
																		"label": "_MobileNumber",
																		"version": 0
																	},
																	"stamp": 49
																},
																"MobileNumber__": {
																	"type": "ShortText",
																	"data": [],
																	"options": {
																		"textSize": "S",
																		"textAlignment": "left",
																		"textStyle": "default",
																		"textColor": "default",
																		"version": 1,
																		"label": "_MobileNumber",
																		"activable": true,
																		"width": 230,
																		"helpText": "",
																		"helpURL": "",
																		"helpFormat": "HTML Format",
																		"readonly": true,
																		"notInDB": true,
																		"browsable": false,
																		"dataType": "String",
																		"autocompletable": false
																	},
																	"stamp": 50
																},
																"LabelFaxNumber__": {
																	"type": "Label",
																	"data": [],
																	"options": {
																		"label": "_FaxNumber",
																		"version": 0
																	},
																	"stamp": 51
																},
																"FaxNumber__": {
																	"type": "ShortText",
																	"data": [],
																	"options": {
																		"textSize": "S",
																		"textAlignment": "left",
																		"textStyle": "default",
																		"textColor": "default",
																		"version": 1,
																		"label": "_FaxNumber",
																		"activable": true,
																		"width": 230,
																		"helpText": "",
																		"helpURL": "",
																		"helpFormat": "HTML Format",
																		"readonly": true,
																		"notInDB": true,
																		"browsable": false,
																		"dataType": "String",
																		"autocompletable": false
																	},
																	"stamp": 52
																},
																"GeneralInformationPane_Loader__": {
																	"type": "HTML",
																	"data": false,
																	"options": {
																		"label": "GeneralInformationPane_Loader__",
																		"version": 0,
																		"width": "100%"
																	},
																	"stamp": 53
																}
															}
														}
													}
												}
											}
										}
									}
								},
								"form-content-right-4": {
									"type": "FlexibleFormLine",
									"data": [],
									"options": {
										"version": 0
									},
									"stamp": 54,
									"*": {
										"MailAddressPane": {
											"type": "PanelData",
											"data": [],
											"options": {
												"border": {},
												"labelLength": 0,
												"iconURL": "",
												"TitleFontSize": "S",
												"hideActionButtons": true,
												"hideTitleBar": false,
												"hideTitle": false,
												"backgroundcolor": "default",
												"labelsAlignment": "left",
												"label": "_Mail_Address_Pane",
												"hidden": false,
												"leftImageURL": "",
												"removeMargins": false,
												"elementsAlignment": "left",
												"version": 0
											},
											"stamp": 55,
											"*": {
												"FieldsManager": {
													"type": "FieldsManager",
													"data": [],
													"options": {
														"controlsAssociation": {
															"MailSub__": "LabelMailSub__",
															"LabelMailSub__": "MailSub__",
															"Street__": "LabelStreet__",
															"LabelStreet__": "Street__",
															"POBox__": "LabelPOBox__",
															"LabelPOBox__": "POBox__",
															"City__": "LabelCity__",
															"LabelCity__": "City__",
															"MailState__": "LabelMailState__",
															"LabelMailState__": "MailState__",
															"Country__": "LabelCountry__",
															"LabelCountry__": "Country__",
															"ZipCode__": "LabelZipCode__",
															"LabelZipCode__": "ZipCode__"
														},
														"version": 0
													},
													"stamp": 56,
													"*": {
														"Grid": {
															"type": "GridLayout",
															"data": [
																"fields"
															],
															"options": {
																"ctrlsPos": {
																	"MailSub__": {
																		"line": 1,
																		"column": 2
																	},
																	"LabelMailSub__": {
																		"line": 1,
																		"column": 1
																	},
																	"Street__": {
																		"line": 2,
																		"column": 2
																	},
																	"LabelStreet__": {
																		"line": 2,
																		"column": 1
																	},
																	"POBox__": {
																		"line": 3,
																		"column": 2
																	},
																	"LabelPOBox__": {
																		"line": 3,
																		"column": 1
																	},
																	"City__": {
																		"line": 5,
																		"column": 2
																	},
																	"LabelCity__": {
																		"line": 5,
																		"column": 1
																	},
																	"MailState__": {
																		"line": 6,
																		"column": 2
																	},
																	"LabelMailState__": {
																		"line": 6,
																		"column": 1
																	},
																	"Country__": {
																		"line": 7,
																		"column": 2
																	},
																	"LabelCountry__": {
																		"line": 7,
																		"column": 1
																	},
																	"ZipCode__": {
																		"line": 4,
																		"column": 2
																	},
																	"LabelZipCode__": {
																		"line": 4,
																		"column": 1
																	},
																	"MailAddressPane_Loader__": {
																		"line": 8,
																		"column": 1
																	}
																},
																"lines": 8,
																"columns": 2,
																"colspans": [
																	[],
																	[],
																	[],
																	[],
																	[],
																	[],
																	[],
																	[
																		{
																			"index": 0,
																			"length": 2
																		}
																	]
																],
																"version": 0
															},
															"stamp": 57,
															"*": {
																"LabelMailSub__": {
																	"type": "Label",
																	"data": [],
																	"options": {
																		"label": "_MailSub",
																		"version": 0
																	},
																	"stamp": 58
																},
																"MailSub__": {
																	"type": "ShortText",
																	"data": [],
																	"options": {
																		"textSize": "S",
																		"textAlignment": "left",
																		"textStyle": "default",
																		"textColor": "default",
																		"version": 1,
																		"label": "_MailSub",
																		"activable": true,
																		"width": 230,
																		"helpText": "",
																		"helpURL": "",
																		"helpFormat": "HTML Format",
																		"readonly": true,
																		"notInDB": true,
																		"browsable": false,
																		"dataType": "String",
																		"autocompletable": false
																	},
																	"stamp": 59
																},
																"LabelStreet__": {
																	"type": "Label",
																	"data": [],
																	"options": {
																		"label": "_Street",
																		"version": 0
																	},
																	"stamp": 60
																},
																"Street__": {
																	"type": "ShortText",
																	"data": [],
																	"options": {
																		"textSize": "S",
																		"textAlignment": "left",
																		"textStyle": "default",
																		"textColor": "default",
																		"version": 1,
																		"label": "_Street",
																		"activable": true,
																		"width": 230,
																		"helpText": "",
																		"helpURL": "",
																		"helpFormat": "HTML Format",
																		"readonly": true,
																		"notInDB": true,
																		"browsable": false,
																		"dataType": "String",
																		"autocompletable": false
																	},
																	"stamp": 61
																},
																"LabelPOBox__": {
																	"type": "Label",
																	"data": [],
																	"options": {
																		"label": "_POBox",
																		"version": 0
																	},
																	"stamp": 62
																},
																"POBox__": {
																	"type": "ShortText",
																	"data": [],
																	"options": {
																		"textSize": "S",
																		"textAlignment": "left",
																		"textStyle": "default",
																		"textColor": "default",
																		"version": 1,
																		"label": "_POBox",
																		"activable": true,
																		"width": 230,
																		"helpText": "",
																		"helpURL": "",
																		"helpFormat": "HTML Format",
																		"readonly": true,
																		"notInDB": true,
																		"browsable": false,
																		"dataType": "String",
																		"autocompletable": false
																	},
																	"stamp": 63
																},
																"LabelZipCode__": {
																	"type": "Label",
																	"data": [],
																	"options": {
																		"label": "_Zip code",
																		"version": 0
																	},
																	"stamp": 64
																},
																"ZipCode__": {
																	"type": "ShortText",
																	"data": [],
																	"options": {
																		"textSize": "S",
																		"textAlignment": "left",
																		"textStyle": "default",
																		"textColor": "default",
																		"version": 1,
																		"label": "_Zip code",
																		"activable": true,
																		"width": 230,
																		"helpText": "",
																		"helpURL": "",
																		"helpFormat": "HTML Format",
																		"readonly": true,
																		"notInDB": true,
																		"browsable": false,
																		"dataType": "String",
																		"autocompletable": false
																	},
																	"stamp": 65
																},
																"LabelCity__": {
																	"type": "Label",
																	"data": [],
																	"options": {
																		"label": "_City",
																		"version": 0
																	},
																	"stamp": 66
																},
																"City__": {
																	"type": "ShortText",
																	"data": [],
																	"options": {
																		"textSize": "S",
																		"textAlignment": "left",
																		"textStyle": "default",
																		"textColor": "default",
																		"version": 1,
																		"label": "_City",
																		"activable": true,
																		"width": 230,
																		"helpText": "",
																		"helpURL": "",
																		"helpFormat": "HTML Format",
																		"readonly": true,
																		"notInDB": true,
																		"browsable": false,
																		"dataType": "String",
																		"autocompletable": false
																	},
																	"stamp": 67
																},
																"LabelMailState__": {
																	"type": "Label",
																	"data": [],
																	"options": {
																		"label": "_MailState",
																		"version": 0
																	},
																	"stamp": 68
																},
																"MailState__": {
																	"type": "ShortText",
																	"data": [],
																	"options": {
																		"textSize": "S",
																		"textAlignment": "left",
																		"textStyle": "default",
																		"textColor": "default",
																		"version": 1,
																		"label": "_MailState",
																		"activable": true,
																		"width": 230,
																		"helpText": "",
																		"helpURL": "",
																		"helpFormat": "HTML Format",
																		"readonly": true,
																		"notInDB": true,
																		"browsable": false,
																		"dataType": "String",
																		"autocompletable": false
																	},
																	"stamp": 69
																},
																"LabelCountry__": {
																	"type": "Label",
																	"data": [],
																	"options": {
																		"label": "_Country",
																		"version": 0
																	},
																	"stamp": 70
																},
																"Country__": {
																	"type": "ShortText",
																	"data": [],
																	"options": {
																		"textSize": "S",
																		"textAlignment": "left",
																		"textStyle": "default",
																		"textColor": "default",
																		"version": 1,
																		"label": "_Country",
																		"activable": true,
																		"width": 230,
																		"helpText": "",
																		"helpURL": "",
																		"helpFormat": "HTML Format",
																		"readonly": true,
																		"notInDB": true,
																		"browsable": false,
																		"dataType": "String",
																		"autocompletable": false
																	},
																	"stamp": 71
																},
																"MailAddressPane_Loader__": {
																	"type": "HTML",
																	"data": false,
																	"options": {
																		"label": "MailAddressPane_Loader__",
																		"version": 0,
																		"width": "100%"
																	},
																	"stamp": 72
																}
															}
														}
													}
												}
											}
										}
									}
								},
								"form-content-right-6": {
									"type": "FlexibleFormLine",
									"data": [],
									"options": {
										"version": 0
									},
									"stamp": 73,
									"*": {
										"RegionalSettingsPane": {
											"type": "PanelData",
											"data": [],
											"options": {
												"border": {},
												"labelLength": 0,
												"iconURL": "",
												"TitleFontSize": "S",
												"hideActionButtons": true,
												"hideTitleBar": false,
												"hideTitle": false,
												"backgroundcolor": "default",
												"labelsAlignment": "left",
												"label": "_Regional_Settings_Pane",
												"hidden": false,
												"leftImageURL": "",
												"removeMargins": false,
												"elementsAlignment": "left",
												"version": 0
											},
											"stamp": 74,
											"*": {
												"FieldsManager": {
													"type": "FieldsManager",
													"data": [],
													"options": {
														"controlsAssociation": {
															"Language__": "LabelLanguage__",
															"LabelLanguage__": "Language__",
															"ExportEncoding__": "LabelExportEncoding__",
															"LabelExportEncoding__": "ExportEncoding__",
															"TimeZone__": "LabelTimeZone__",
															"LabelTimeZone__": "TimeZone__",
															"RegionalSettings__": "LabelRegionalSettings__",
															"LabelRegionalSettings__": "RegionalSettings__"
														},
														"version": 0
													},
													"stamp": 75,
													"*": {
														"Grid": {
															"type": "GridLayout",
															"data": [
																"fields"
															],
															"options": {
																"ctrlsPos": {
																	"Language__": {
																		"line": 1,
																		"column": 2
																	},
																	"LabelLanguage__": {
																		"line": 1,
																		"column": 1
																	},
																	"ExportEncoding__": {
																		"line": 3,
																		"column": 2
																	},
																	"LabelExportEncoding__": {
																		"line": 3,
																		"column": 1
																	},
																	"TimeZone__": {
																		"line": 4,
																		"column": 2
																	},
																	"LabelTimeZone__": {
																		"line": 4,
																		"column": 1
																	},
																	"RegionalSettings__": {
																		"line": 2,
																		"column": 2
																	},
																	"LabelRegionalSettings__": {
																		"line": 2,
																		"column": 1
																	},
																	"RegionalSettingsPane_Loader__": {
																		"line": 5,
																		"column": 1
																	}
																},
																"lines": 5,
																"columns": 2,
																"colspans": [
																	[],
																	[],
																	[],
																	[],
																	[
																		{
																			"index": 0,
																			"length": 2
																		}
																	]
																],
																"version": 0
															},
															"stamp": 76,
															"*": {
																"LabelLanguage__": {
																	"type": "Label",
																	"data": [],
																	"options": {
																		"label": "_Language",
																		"version": 0
																	},
																	"stamp": 77
																},
																"Language__": {
																	"type": "ComboBox",
																	"data": [],
																	"options": {
																		"possibleValues": {},
																		"version": 1,
																		"keyValueMode": false,
																		"label": "_Language",
																		"possibleKeys": {},
																		"activable": true,
																		"width": 230,
																		"helpText": "",
																		"helpURL": "",
																		"helpFormat": "HTML Format",
																		"DataSourceOrigin": "Standalone",
																		"DisplayedColumns": "",
																		"readonly": true,
																		"notInDB": true,
																		"dataType": "String"
																	},
																	"stamp": 78
																},
																"LabelRegionalSettings__": {
																	"type": "Label",
																	"data": [],
																	"options": {
																		"label": "_Regional_Settings",
																		"version": 0
																	},
																	"stamp": 79
																},
																"RegionalSettings__": {
																	"type": "ComboBox",
																	"data": [],
																	"options": {
																		"possibleValues": {},
																		"version": 1,
																		"keyValueMode": false,
																		"label": "_Regional_Settings",
																		"possibleKeys": {},
																		"activable": true,
																		"width": 230,
																		"helpText": "",
																		"helpURL": "",
																		"helpFormat": "HTML Format",
																		"DataSourceOrigin": "Standalone",
																		"DisplayedColumns": "",
																		"readonly": true,
																		"notInDB": true,
																		"dataType": "String"
																	},
																	"stamp": 80
																},
																"LabelExportEncoding__": {
																	"type": "Label",
																	"data": [],
																	"options": {
																		"label": "_ExportEncoding",
																		"version": 0
																	},
																	"stamp": 81
																},
																"ExportEncoding__": {
																	"type": "ComboBox",
																	"data": [],
																	"options": {
																		"possibleValues": {},
																		"version": 1,
																		"keyValueMode": false,
																		"label": "_ExportEncoding",
																		"possibleKeys": {},
																		"activable": true,
																		"width": 230,
																		"helpText": "",
																		"helpURL": "",
																		"helpFormat": "HTML Format",
																		"DataSourceOrigin": "Standalone",
																		"DisplayedColumns": "",
																		"readonly": true,
																		"notInDB": true,
																		"dataType": "String"
																	},
																	"stamp": 82
																},
																"LabelTimeZone__": {
																	"type": "Label",
																	"data": [],
																	"options": {
																		"label": "_TimeZone",
																		"version": 0
																	},
																	"stamp": 83
																},
																"TimeZone__": {
																	"type": "ComboBox",
																	"data": [],
																	"options": {
																		"possibleValues": {},
																		"version": 1,
																		"keyValueMode": false,
																		"label": "_TimeZone",
																		"possibleKeys": {},
																		"activable": true,
																		"width": 230,
																		"helpText": "",
																		"helpURL": "",
																		"helpFormat": "HTML Format",
																		"DataSourceOrigin": "Standalone",
																		"DisplayedColumns": "",
																		"readonly": true,
																		"notInDB": true,
																		"dataType": "String"
																	},
																	"stamp": 84
																},
																"RegionalSettingsPane_Loader__": {
																	"type": "HTML",
																	"data": false,
																	"options": {
																		"label": "RegionalSettingsPane_Loader__",
																		"version": 0,
																		"width": "100%"
																	},
																	"stamp": 85
																}
															}
														}
													}
												}
											}
										}
									}
								},
								"form-content-right-8": {
									"type": "FlexibleFormLine",
									"data": [],
									"options": {
										"version": 0
									},
									"stamp": 86,
									"*": {
										"VendorAdvancedFieldsPane": {
											"type": "PanelData",
											"data": [],
											"options": {
												"border": {},
												"labelLength": 0,
												"iconURL": "",
												"TitleFontSize": "S",
												"hideActionButtons": true,
												"hideTitleBar": false,
												"hideTitle": false,
												"backgroundcolor": "default",
												"labelsAlignment": "left",
												"label": "_Vendor_Advanced_Fields_Pane",
												"hidden": false,
												"leftImageURL": "",
												"removeMargins": false,
												"elementsAlignment": "left",
												"version": 0
											},
											"stamp": 87,
											"*": {
												"FieldsManager": {
													"type": "FieldsManager",
													"data": [],
													"options": {
														"controlsAssociation": {
															"AdditionalField1__": "LabelAdditionalField1__",
															"LabelAdditionalField1__": "AdditionalField1__",
															"AdditionalField2__": "LabelAdditionalField2__",
															"LabelAdditionalField2__": "AdditionalField2__",
															"AdditionalField3__": "LabelAdditionalField3__",
															"LabelAdditionalField3__": "AdditionalField3__",
															"AdditionalField4__": "LabelAdditionalField4__",
															"LabelAdditionalField4__": "AdditionalField4__",
															"AdditionalField5__": "LabelAdditionalField5__",
															"LabelAdditionalField5__": "AdditionalField5__"
														},
														"version": 0
													},
													"stamp": 88,
													"*": {
														"Grid": {
															"type": "GridLayout",
															"data": [
																"fields"
															],
															"options": {
																"ctrlsPos": {
																	"AdditionalField1__": {
																		"line": 1,
																		"column": 2
																	},
																	"LabelAdditionalField1__": {
																		"line": 1,
																		"column": 1
																	},
																	"AdditionalField2__": {
																		"line": 2,
																		"column": 2
																	},
																	"LabelAdditionalField2__": {
																		"line": 2,
																		"column": 1
																	},
																	"AdditionalField3__": {
																		"line": 3,
																		"column": 2
																	},
																	"LabelAdditionalField3__": {
																		"line": 3,
																		"column": 1
																	},
																	"AdditionalField4__": {
																		"line": 4,
																		"column": 2
																	},
																	"LabelAdditionalField4__": {
																		"line": 4,
																		"column": 1
																	},
																	"AdditionalField5__": {
																		"line": 5,
																		"column": 2
																	},
																	"LabelAdditionalField5__": {
																		"line": 5,
																		"column": 1
																	},
																	"VendorAdvancedFieldsPane_Loader__": {
																		"line": 6,
																		"column": 1
																	}
																},
																"lines": 6,
																"columns": 2,
																"colspans": [
																	[],
																	[],
																	[],
																	[],
																	[],
																	[
																		{
																			"index": 0,
																			"length": 2
																		}
																	]
																],
																"version": 0
															},
															"stamp": 89,
															"*": {
																"LabelAdditionalField1__": {
																	"type": "Label",
																	"data": [],
																	"options": {
																		"label": "_AdditionalField1",
																		"version": 0
																	},
																	"stamp": 90
																},
																"AdditionalField1__": {
																	"type": "ShortText",
																	"data": [],
																	"options": {
																		"textSize": "S",
																		"textAlignment": "left",
																		"textStyle": "default",
																		"textColor": "default",
																		"version": 1,
																		"label": "_AdditionalField1",
																		"activable": true,
																		"width": 230,
																		"helpText": "",
																		"helpURL": "",
																		"helpFormat": "HTML Format",
																		"readonly": true,
																		"notInDB": true,
																		"browsable": false,
																		"dataType": "String",
																		"autocompletable": false
																	},
																	"stamp": 91
																},
																"LabelAdditionalField2__": {
																	"type": "Label",
																	"data": [],
																	"options": {
																		"label": "_AdditionalField2",
																		"version": 0
																	},
																	"stamp": 92
																},
																"AdditionalField2__": {
																	"type": "ShortText",
																	"data": [],
																	"options": {
																		"textSize": "S",
																		"textAlignment": "left",
																		"textStyle": "default",
																		"textColor": "default",
																		"version": 1,
																		"label": "_AdditionalField2",
																		"activable": true,
																		"width": 230,
																		"helpText": "",
																		"helpURL": "",
																		"helpFormat": "HTML Format",
																		"readonly": true,
																		"notInDB": true,
																		"browsable": false,
																		"dataType": "String",
																		"autocompletable": false
																	},
																	"stamp": 93
																},
																"LabelAdditionalField3__": {
																	"type": "Label",
																	"data": [],
																	"options": {
																		"label": "_AdditionalField3",
																		"version": 0
																	},
																	"stamp": 94
																},
																"AdditionalField3__": {
																	"type": "ShortText",
																	"data": [],
																	"options": {
																		"textSize": "S",
																		"textAlignment": "left",
																		"textStyle": "default",
																		"textColor": "default",
																		"version": 1,
																		"label": "_AdditionalField3",
																		"activable": true,
																		"width": 230,
																		"helpText": "",
																		"helpURL": "",
																		"helpFormat": "HTML Format",
																		"readonly": true,
																		"notInDB": true,
																		"browsable": false,
																		"dataType": "String",
																		"autocompletable": false
																	},
																	"stamp": 95
																},
																"LabelAdditionalField4__": {
																	"type": "Label",
																	"data": [],
																	"options": {
																		"label": "_AdditionalField4",
																		"version": 0
																	},
																	"stamp": 96
																},
																"AdditionalField4__": {
																	"type": "ShortText",
																	"data": [],
																	"options": {
																		"textSize": "S",
																		"textAlignment": "left",
																		"textStyle": "default",
																		"textColor": "default",
																		"version": 1,
																		"label": "_AdditionalField4",
																		"activable": true,
																		"width": 230,
																		"helpText": "",
																		"helpURL": "",
																		"helpFormat": "HTML Format",
																		"readonly": true,
																		"notInDB": true,
																		"browsable": false,
																		"dataType": "String",
																		"autocompletable": false
																	},
																	"stamp": 97
																},
																"LabelAdditionalField5__": {
																	"type": "Label",
																	"data": [],
																	"options": {
																		"label": "_AdditionalField5",
																		"version": 0
																	},
																	"stamp": 98
																},
																"AdditionalField5__": {
																	"type": "ShortText",
																	"data": [],
																	"options": {
																		"textSize": "S",
																		"textAlignment": "left",
																		"textStyle": "default",
																		"textColor": "default",
																		"version": 1,
																		"label": "_AdditionalField5",
																		"activable": true,
																		"width": 230,
																		"helpText": "",
																		"helpURL": "",
																		"helpFormat": "HTML Format",
																		"readonly": true,
																		"notInDB": true,
																		"browsable": false,
																		"dataType": "String",
																		"autocompletable": false
																	},
																	"stamp": 99
																},
																"VendorAdvancedFieldsPane_Loader__": {
																	"type": "HTML",
																	"data": false,
																	"options": {
																		"label": "CustomerAdvancedFieldsPane_Loader__",
																		"version": 0,
																		"width": "100%"
																	},
																	"stamp": 100
																}
															}
														}
													}
												}
											}
										}
									}
								},
								"form-content-right-9": {
									"type": "FlexibleFormLine",
									"data": [],
									"options": {
										"version": 0
									},
									"stamp": 101,
									"*": {
										"WelcomeSettingsPane": {
											"type": "PanelData",
											"data": [],
											"options": {
												"border": {},
												"labelLength": 0,
												"iconURL": "",
												"TitleFontSize": "S",
												"hideActionButtons": true,
												"hideTitleBar": false,
												"hideTitle": false,
												"backgroundcolor": "default",
												"labelsAlignment": "left",
												"label": "_Welcome_Settings_Pane",
												"hidden": false,
												"leftImageURL": "",
												"removeMargins": false,
												"elementsAlignment": "left",
												"version": 0
											},
											"stamp": 102,
											"*": {
												"FieldsManager": {
													"type": "FieldsManager",
													"data": [],
													"options": {
														"controlsAssociation": {
															"DisplayWelcomePageOnNextLogin__": "LabelDisplayWelcomePageOnNextLogin__",
															"LabelDisplayWelcomePageOnNextLogin__": "DisplayWelcomePageOnNextLogin__"
														},
														"version": 0
													},
													"stamp": 103,
													"*": {
														"Grid": {
															"type": "GridLayout",
															"data": [
																"fields"
															],
															"options": {
																"ctrlsPos": {
																	"DisplayWelcomePageOnNextLogin__": {
																		"line": 1,
																		"column": 2
																	},
																	"LabelDisplayWelcomePageOnNextLogin__": {
																		"line": 1,
																		"column": 1
																	},
																	"WelcomeSettingsPane_Loader__": {
																		"line": 2,
																		"column": 1
																	}
																},
																"lines": 2,
																"columns": 2,
																"colspans": [
																	[],
																	[
																		{
																			"index": 0,
																			"length": 2
																		}
																	]
																],
																"version": 0
															},
															"stamp": 104,
															"*": {
																"LabelDisplayWelcomePageOnNextLogin__": {
																	"type": "Label",
																	"data": [],
																	"options": {
																		"label": "_DisplayWelcomePageOnNextLogin",
																		"version": 0
																	},
																	"stamp": 105
																},
																"DisplayWelcomePageOnNextLogin__": {
																	"type": "CheckBox",
																	"data": [],
																	"options": {
																		"label": "_DisplayWelcomePageOnNextLogin",
																		"activable": true,
																		"width": 230,
																		"version": 0,
																		"helpText": "",
																		"helpURL": "",
																		"helpFormat": "HTML Format",
																		"readonly": true,
																		"dataType": "Boolean",
																		"notInDB": true
																	},
																	"stamp": 106
																},
																"WelcomeSettingsPane_Loader__": {
																	"type": "HTML",
																	"data": false,
																	"options": {
																		"label": "WelcomeSettingsPane_Loader__",
																		"version": 0,
																		"width": "100%"
																	},
																	"stamp": 107
																}
															}
														}
													}
												}
											}
										}
									}
								},
								"form-content-right-23": {
									"type": "FlexibleFormLine",
									"data": [],
									"options": {
										"version": 0
									},
									"stamp": 108,
									"*": {
										"ActivityPane": {
											"type": "PanelData",
											"data": [],
											"options": {
												"border": {},
												"labelLength": 0,
												"iconURL": "",
												"TitleFontSize": "M",
												"hideActionButtons": true,
												"hideTitleBar": false,
												"hideTitle": false,
												"backgroundcolor": "default",
												"labelsAlignment": "left",
												"label": "_Activity_Pane",
												"leftImageURL": "",
												"removeMargins": false,
												"elementsAlignment": "left",
												"panelStyle": "FlexibleFormPanelLight",
												"hidden": true,
												"version": 0
											},
											"stamp": 109,
											"*": {
												"FieldsManager": {
													"type": "FieldsManager",
													"data": [],
													"options": {
														"controlsAssociation": {},
														"version": 0
													},
													"stamp": 110,
													"*": {
														"Grid": {
															"type": "GridLayout",
															"data": [
																"fields"
															],
															"options": {
																"ctrlsPos": {
																	"EventsHistory_NavMenu__": {
																		"line": 1,
																		"column": 1
																	},
																	"EventHistoryView__": {
																		"line": 2,
																		"column": 1
																	}
																},
																"lines": 2,
																"columns": 2,
																"colspans": [
																	[
																		{
																			"index": 0,
																			"length": 2
																		}
																	],
																	[
																		{
																			"index": 0,
																			"length": 2
																		}
																	]
																],
																"version": 0
															},
															"stamp": 111,
															"*": {
																"EventsHistory_NavMenu__": {
																	"type": "HTML",
																	"data": false,
																	"options": {
																		"label": "EventsHistory_NavMenu",
																		"htmlContent": "<input id=\"onEventHistoryLoad\" type=\"hidden\" onclick=\"OnEventHistoryLoad(event);\">\n<div id=\"EventHistoryMenu\" class=\"nav-body\" style=\"\">\n    <ul class=\"nav-topbar\">\n        <li class=\"nav-item\">\n            <a id=\"allEvents\" onclick=\"OnClickHistoryTab(this);\" href=\"#\" class=\"nav-link\">\n                <span id=\"allEventsLbl\" />\n            </a>\n        </li><li class=\"nav-item-divider\"></li>\n        <li class=\"nav-item\">\n            <a href=\"#\" id=\"persoInfEditEvents\" onclick=\"OnClickHistoryTab(this);\" class=\"nav-link active\">\n                <span id=\"persoInfEditEventsLbl\" />\n            </a>\n        </li>\n        <li class=\"nav-item-divider\"></li>\n        <li class=\"nav-item\">\n            <a id=\"actionsEvents\" onclick=\"OnClickHistoryTab(this);\" href=\"#\" class=\"nav-link\">\n                <span id=\"actionsEventsLbl\" />\n            </a>\n        </li>\n        <li class=\"nav-item-divider\"></li>\n        <li class=\"nav-item\">\n            <a id=\"loginEvents\" onclick=\"OnClickHistoryTab(this);\" href=\"#\" class=\"nav-link\">\n                <span id=\"loginEventsLbl\" />\n            </a>\n        </li>\n    </ul>\n</div>\n<script>\n    window.postMessage({ eventName: \"onLoad\", control: \"EventsHistory_NavMenu__\" }, document.location.origin);\n    var tabEvtHistoryItems = document.getElementById(\"EventHistoryMenu\").getElementsByClassName(\"nav-link\");\n    function OnClickHistoryTab(tab)\n    {\n        for (var i = 0; i < tabEvtHistoryItems.length; i++)\n            tabEvtHistoryItems[i].className = \"nav-link\";\n\n        tab.className = \"nav-link active\";\n        window.postMessage({ eventName: \"onClick\", control: \"EventsHistory_NavMenu__\", args: tab.id }, document.location.origin);\n    }\n    function OnEventHistoryLoad(evt)\n    {\n        for (var lbl in evt.tabs)\n        {\n            var elem = document.getElementById(lbl + \"Lbl\");\n            if (elem)\n                elem.innerText = evt.tabs[lbl];\n        }\n    }\n</script>",
																		"width": "100%",
																		"version": 0,
																		"css": "@ .nav-body {margin-top:5px;margin-bottom:5px;}@ "
																	},
																	"stamp": 112
																},
																"EventHistoryView__": {
																	"type": "AdminList",
																	"data": false,
																	"options": {
																		"width": "100%",
																		"restrictToCurrentJobId": false,
																		"showActions": false,
																		"openInNewTab": false,
																		"openInReadOnlyMode": true,
																		"label": "View",
																		"version": 0
																	},
																	"stamp": 113
																}
															}
														}
													}
												}
											}
										}
									}
								},
								"form-content-right-7": {
									"type": "FlexibleFormLine",
									"data": [],
									"options": {
										"version": 0
									},
									"stamp": 114,
									"*": {
										"GeneralPane": {
											"type": "PanelData",
											"data": [],
											"options": {
												"border": {},
												"labelLength": 0,
												"iconURL": "",
												"TitleFontSize": "M",
												"hideActionButtons": true,
												"hideTitle": false,
												"backgroundcolor": "default",
												"labelsAlignment": "right",
												"label": "_Params_General",
												"hidden": false,
												"leftImageURL": "",
												"removeMargins": false,
												"elementsAlignment": "left",
												"version": 0
											},
											"stamp": 115,
											"*": {
												"FieldsManager": {
													"type": "FieldsManager",
													"data": [],
													"options": {
														"controlsAssociation": {
															"Params_Login__": "LabelParams_Login__",
															"LabelParams_Login__": "Params_Login__",
															"Params_ProfileLink__": "LabelParams_ProfileLink__",
															"LabelParams_ProfileLink__": "Params_ProfileLink__"
														},
														"version": 0
													},
													"stamp": 116,
													"*": {
														"Grid": {
															"type": "GridLayout",
															"data": [
																"fields"
															],
															"options": {
																"ctrlsPos": {
																	"Params_Login__": {
																		"line": 1,
																		"column": 2
																	},
																	"LabelParams_Login__": {
																		"line": 1,
																		"column": 1
																	},
																	"Params_ProfileLink__": {
																		"line": 2,
																		"column": 2
																	},
																	"LabelParams_ProfileLink__": {
																		"line": 2,
																		"column": 1
																	}
																},
																"lines": 2,
																"columns": 2,
																"colspans": [
																	[],
																	[]
																],
																"version": 0
															},
															"stamp": 117,
															"*": {
																"LabelParams_Login__": {
																	"type": "Label",
																	"data": [],
																	"options": {
																		"label": "_Params_Login",
																		"version": 0
																	},
																	"stamp": 118
																},
																"Params_Login__": {
																	"type": "ShortText",
																	"data": [],
																	"options": {
																		"textSize": "S",
																		"textAlignment": "left",
																		"textStyle": "default",
																		"textColor": "default",
																		"version": 1,
																		"label": "_Params_Login",
																		"activable": true,
																		"width": "150",
																		"helpText": "",
																		"helpURL": "",
																		"helpFormat": "HTML Format",
																		"readonly": true,
																		"notInDB": true,
																		"dataType": "String",
																		"browsable": false
																	},
																	"stamp": 119
																},
																"LabelParams_ProfileLink__": {
																	"type": "Label",
																	"data": false,
																	"options": {
																		"label": "_Params_ProfileLink",
																		"version": 0
																	},
																	"stamp": 120
																},
																"Params_ProfileLink__": {
																	"type": "Link",
																	"data": false,
																	"options": {
																		"version": 1,
																		"link": "https://www.esker.com",
																		"openInCurrentWindow": false,
																		"label": "_Params_ProfileLink"
																	},
																	"stamp": 121
																}
															}
														}
													}
												}
											}
										},
										"SecurityPane": {
											"type": "PanelData",
											"data": [],
											"options": {
												"border": {},
												"labelLength": 0,
												"iconURL": "",
												"TitleFontSize": "M",
												"hideActionButtons": true,
												"hideTitle": false,
												"backgroundcolor": "default",
												"labelsAlignment": "right",
												"label": "_Params_Security",
												"hidden": false,
												"leftImageURL": "",
												"removeMargins": false,
												"elementsAlignment": "left",
												"version": 0
											},
											"stamp": 122,
											"*": {
												"FieldsManager": {
													"type": "FieldsManager",
													"data": [],
													"options": {
														"controlsAssociation": {
															"Params_PasswordFree__": "LabelParams_PasswordFree__",
															"LabelParams_PasswordFree__": "Params_PasswordFree__",
															"Params_LockLogin__": "LabelParams_LockLogin__",
															"LabelParams_LockLogin__": "Params_LockLogin__",
															"Params_ResetPassword__": "LabelParams_ResetPassword__",
															"LabelParams_ResetPassword__": "Params_ResetPassword__"
														},
														"version": 0
													},
													"stamp": 123,
													"*": {
														"Grid": {
															"type": "GridLayout",
															"data": [
																"fields"
															],
															"options": {
																"ctrlsPos": {
																	"Params_PasswordFree__": {
																		"line": 1,
																		"column": 2
																	},
																	"LabelParams_PasswordFree__": {
																		"line": 1,
																		"column": 1
																	},
																	"Params_LockLogin__": {
																		"line": 2,
																		"column": 2
																	},
																	"LabelParams_LockLogin__": {
																		"line": 2,
																		"column": 1
																	},
																	"Params_ResetPassword__": {
																		"line": 3,
																		"column": 2
																	},
																	"LabelParams_ResetPassword__": {
																		"line": 3,
																		"column": 1
																	}
																},
																"lines": 3,
																"columns": 2,
																"colspans": [
																	[],
																	[],
																	[]
																],
																"version": 0
															},
															"stamp": 124,
															"*": {
																"LabelParams_PasswordFree__": {
																	"type": "Label",
																	"data": [],
																	"options": {
																		"label": "_Params_PasswordFree",
																		"version": 0
																	},
																	"stamp": 125
																},
																"Params_PasswordFree__": {
																	"type": "CheckBox",
																	"data": [],
																	"options": {
																		"label": "_Params_PasswordFree",
																		"activable": true,
																		"width": "30",
																		"version": 0,
																		"helpText": "",
																		"helpURL": "",
																		"helpFormat": "HTML Format",
																		"readonly": true,
																		"notInDB": true,
																		"dataType": "Boolean"
																	},
																	"stamp": 126
																},
																"LabelParams_LockLogin__": {
																	"type": "Label",
																	"data": [],
																	"options": {
																		"label": "_Params_LockLogin",
																		"version": 0
																	},
																	"stamp": 127
																},
																"Params_LockLogin__": {
																	"type": "CheckBox",
																	"data": [],
																	"options": {
																		"label": "_Params_LockLogin",
																		"activable": true,
																		"width": "30",
																		"version": 0,
																		"helpText": "",
																		"helpURL": "",
																		"helpFormat": "HTML Format",
																		"readonly": true,
																		"notInDB": true,
																		"dataType": "Boolean"
																	},
																	"stamp": 128
																},
																"LabelParams_ResetPassword__": {
																	"type": "Label",
																	"data": false,
																	"options": {
																		"label": "",
																		"version": 0
																	},
																	"stamp": 129
																},
																"Params_ResetPassword__": {
																	"type": "FormButton",
																	"data": false,
																	"options": {
																		"buttonLabel": "_Params_ResetPassword",
																		"label": "",
																		"version": 0,
																		"textPosition": "text-right",
																		"textSize": "MS",
																		"textStyle": "default",
																		"textColor": "color1",
																		"nextprocess": {
																			"processName": "AP - Application Settings__",
																			"attachmentsMode": "all",
																			"willBeChild": true,
																			"returnToOriginalUrl": false
																		},
																		"urlImageOverlay": "",
																		"action": "none",
																		"url": "",
																		"style": 6
																	},
																	"stamp": 130
																}
															}
														}
													}
												}
											}
										}
									}
								},
								"form-content-right-10": {
									"type": "FlexibleFormLine",
									"data": [],
									"options": {
										"version": 0
									},
									"stamp": 131,
									"*": {
										"VendorsMasterDataPane": {
											"type": "PanelData",
											"data": [],
											"options": {
												"border": {},
												"labelLength": 0,
												"iconURL": "",
												"TitleFontSize": "M",
												"hideActionButtons": true,
												"hideTitleBar": false,
												"hideTitle": false,
												"backgroundcolor": "default",
												"labelsAlignment": "right",
												"label": "_VendorsMasterData",
												"leftImageURL": "",
												"removeMargins": false,
												"elementsAlignment": "left",
												"hidden": false,
												"version": 0
											},
											"stamp": 132,
											"*": {
												"FieldsManager": {
													"type": "FieldsManager",
													"options": {
														"controlsAssociation": {},
														"version": 0
													},
													"*": {
														"Grid": {
															"type": "GridLayout",
															"options": {
																"lines": 1,
																"columns": 2,
																"version": 0,
																"ctrlsPos": {
																	"VendorsMasterDataList__": {
																		"line": 1,
																		"column": 1
																	}
																},
																"colspans": [
																	[
																		{
																			"index": 0,
																			"length": 2
																		}
																	]
																]
															},
															"data": [
																"fields"
															],
															"*": {
																"VendorsMasterDataList__": {
																	"type": "HTML",
																	"data": false,
																	"options": {
																		"label": "HTML content",
																		"htmlContent": "<div id=\"VendorMenu\" class=\"nav-body\" style=\"\">\n\t<ul class=\"nav-sidebar\">\n\t\t<li class=\"nav-item-divider\"></li>\n\t\t<!-- ###Insert Items Here### -->\n\t\t<li class=\"nav-item-divider\"></li>\n\t</ul>\n</div>\n<script>\n\tfunction OnClickVendor(tabID) {\n\t\tvar tabItems = document.getElementById(\"VendorMenu\").getElementsByClassName(\"nav-link\");\n\t\tfor (var i = 0; i < tabItems.length; i++)\n\t\t\ttabItems[i].className = \"nav-link\";\n\t\t\n\t\tvar tab = document.getElementById(tabID);\n\t\ttab.className = \"nav-link active\";\n\t\twindow.postMessage({eventName: \"onClickVendor\", control: \"VendorsMasterDataList__\", args: tab.id}, document.location.origin);\n\t}\n</script>",
																		"width": "100%",
																		"version": 0,
																		"css": ""
																	},
																	"stamp": 133
																}
															},
															"stamp": 134
														}
													},
													"stamp": 135,
													"data": []
												}
											}
										}
									}
								}
							},
							"stamp": 136,
							"data": []
						}
					},
					"stamp": 137,
					"data": []
				},
				"form-footer": {
					"type": "FlexibleFormFooter",
					"options": {
						"splitterType": "simple-h",
						"version": 0
					},
					"*": {
						"Save": {
							"type": "SubmitButton",
							"options": {
								"label": "_Save",
								"action": "save",
								"submit": true,
								"version": 0,
								"shortCut": "S"
							},
							"stamp": 138,
							"data": []
						},
						"Close": {
							"type": "SubmitButton",
							"options": {
								"label": "_Close",
								"action": "cancel",
								"submit": true,
								"version": 0,
								"shortCut": "Q"
							},
							"stamp": 139,
							"data": []
						},
						"Delete": {
							"type": "SubmitButton",
							"options": {
								"label": "_Delete",
								"action": "delete",
								"submit": true,
								"version": 0,
								"disabled": true
							},
							"stamp": 140,
							"data": []
						}
					},
					"stamp": 141,
					"data": []
				}
			},
			"stamp": 142,
			"data": []
		}
	},
	"stamps": 142,
	"data": []
}