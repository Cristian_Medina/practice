var SendPopup = {
	_title: "_Popup title",
	_languageList: ["en=_English", "fr=_French", "de=_German", "es=_Spanish", "it=_Italian", "ZH-CN=_Simplified Chinese"],
	_error: null,
	_defaultValue: null,
	_controlNames:
	{
		email: "ctrlVendorEmail",
		language: "ctrlVendorLanguage"
	},
	Show: function ()
	{
		Popup.Dialog(this._title, null, this.Fill, this.Commit, this.Validate, null, this.Cancel);
	},
	Fill: function (dialog)
	{
		var ctrlEmail = dialog.AddText(SendPopup._controlNames.email, "_email");
		var languageCombo = dialog.AddComboBox(SendPopup._controlNames.language, "_vendor language");
		languageCombo.SetAvailableValues(SendPopup._languageList);
		var commitButton = dialog.GetControl("ButtonOk");
		commitButton.SetLabel(Language.Translate("_send invitation button"));
		if (SendPopup._defaultValue)
		{
			languageCombo.SetValue(SendPopup._defaultValue.lang);
			ctrlEmail.SetValue(SendPopup._defaultValue.userLogin);
		}
		if (SendPopup._error)
		{
			ctrlEmail.SetError(SendPopup._error);
		}
	},
	Commit: function (dialog)
	{
		var dEmail = dialog.GetControl(SendPopup._controlNames.email);
		var dLang = dialog.GetControl(SendPopup._controlNames.language);
		var lang = dLang.GetValue();
		var userLogin = dEmail.GetValue();
		SendPopup._error = null;
		SendPopup._defaultValue = {
			lang: lang,
			userLogin: userLogin
		};
		Controls.DataPanel.Wait(true);
		Query.DBQuery(
			/**
			 * @this QueryResult
			 */
			function ()
			{
				Controls.DataPanel.Wait(false);
				if (this.GetRecordsCount() > 0)
				{
					SendPopup._error = "_Vendor already exist";
					SendPopup.Show();
				}
				else
				{
					Controls.VendorEmail__.SetValue(userLogin);
					Controls.VendorLanguage__.SetValue(lang);
					ProcessInstance.ApproveAsynchronous("ok");
				}
			}, "ODUSER", "", "login=" + User.accountId + "$" + userLogin, "", 1);
	},
	Validate: function (dialog)
	{
		var ctrlEmail = dialog.GetControl(SendPopup._controlNames.email);
		if (!Sys.Helpers.String.IsEmail(ctrlEmail.GetValue()))
		{
			ctrlEmail.SetError("_Use correct email");
			return false;
		}
		return true;
	},
	Cancel: function ()
	{
		SendPopup.OnClose();
		return false;
	},
	OnClose: function ()
	{
		if (ProcessInstance.isOpenInPopup)
		{
			ProcessInstance.Quit("quit");
		}
	}
};
ProcessInstance.SetSilentChange(true);
Process.SetHelpId(2561);
Controls.DataPanel.Hide(true);
Controls.SystemData.Hide(true);
SendPopup.Show();