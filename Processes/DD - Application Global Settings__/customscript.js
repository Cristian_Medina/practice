///#GLOBALS Lib Sys

Process.SetHelpId("7015");
var dialogManager = {
	getMainAccountId: function() 
	{
		if (!User.dn)
		{
			throw "User.dn empty";
		}

		var regex = /ou=([^,]+),ou=ESK,s=eoo/;
		var dnMatches = regex.exec(User.dn);
		if(!dnMatches || dnMatches.length < 2)
		{
			throw "User.dn not consistent";
		}

		return dnMatches[1];
	},
	computeUniqueClientId: function(customClientId)
	{
		if (!User)
		{
			throw "Cannot get client id if no User instanciated";
		}
		var mainaccountID = this.getMainAccountId();
		mainaccountID = mainaccountID.toLowerCase();
		var clientId = customClientId ? customClientId.toLowerCase() + "-" + mainaccountID : mainaccountID;
		var regexOnlyAplhanumeric = /^[a-z0-9][a-z0-9\\.\\-]{1,61}[a-z0-9]$/;
		if (!regexOnlyAplhanumeric.test(clientId))
		{
			throw "Wrong client id - only alphanumeric character is allowed and should be lowercase: " + clientId;
		}

		return clientId;
	},
	fillConfirmAIDeletion: function(dialog)
	{
		var confirmationMessage = dialog.AddDescription("confirmationMessage", "");
		confirmationMessage.SetText("_confirmationMessage");
		
		dialog.AddSeparator();
		var clientIDControl = dialog.AddText("clientIDText", "_clientID", "200px");
		clientIDControl.SetHelpData(null, "7022", "HTML Format");
		
		dialog.AddSeparator();
		var confirmationCheckBox = dialog.AddCheckBox("confirmationCheckBox", "");
		confirmationCheckBox.SetText("_confirmationCheckBox");
		dialog.RequireControl("confirmationCheckbox", true);

		var customClientId = Sys.Helpers.TryCallFunction("Lib.DD.Customization.Common.SetClientId");
		if (customClientId)
		{
			clientIDControl.SetValue(customClientId);
		}
	},
	validateConfirmAIDeletion: function(dialog)
	{
		var isChecked = dialog.GetControl("confirmationCheckBox").GetValue();
		if (isChecked)
		{
			return true;
		}
		dialog.GetControl("confirmationCheckBox").SetError("This field is required!");
		return false;
	},
	commitConfirmAIDeletion: function(dialog)
	{
		var clientId = dialogManager.computeUniqueClientId(dialog.GetControl("clientIDText").GetValue());
		var requestData = {
			"client_id": clientId
		};
		//Call WS to delete all AI data for this account
		Query.HTTPQuery({
			method: "POST",
			targetURL: "GuessEmailType",
			urlName: "UrlResetAI",
			headers: {
				"Content-Type": "application/json"
			},
			data: JSON.stringify(requestData),
			timeout: 5000,
			callback: function(xmlHttpRequest)
			{
				if (xmlHttpRequest.status !== 200)
				{
					var errorMessage = Language.Translate("Error while reseting SDA AI ") + xmlHttpRequest.status + " " + xmlHttpRequest.statusText;
					Log.Error(errorMessage);
					Log.Error(xmlHttpRequest.responseText);
					Popup.Alert(errorMessage, true);
					return;
				}

				var response;
				try
				{
					response = JSON.parse(xmlHttpRequest.responseText);
					if (response.deletion)
					{
						Log.Info("SDA AI reset done");
						Popup.Alert("_SDA AI reset done", false, null, "_SDA AI reset done title");
					}
					else
					{
						Log.Info("SDA AI : no data to delete");
						Popup.Alert("_SDA AI : no data to delete", false, null, "_SDA AI no data title");
					}
				}
				catch (e)
				{
					Log.Error("Error while parsing reset_ai response as JSON: " + e);
					Log.Error(xmlHttpRequest.responseText);
				}
			}
		});
		return true;
	},
	confirmAIDeletionPopup: function()
	{
		return Popup.Dialog("Delete AI data for this account", null, this.fillConfirmAIDeletion, this.commitConfirmAIDeletion, this.validateConfirmAIDeletion, null);
	}
};

Controls.DeleteAIData__.OnClick = function() 
{
	return dialogManager.confirmAIDeletionPopup();
};