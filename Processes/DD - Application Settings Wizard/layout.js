{
	"*": {
		"GDRData": {
			"type": "GDRData",
			"options": {
				"version": 0
			},
			"stamp": 1,
			"data": []
		},
		"OCRParams": {
			"type": "OCRParams",
			"options": {
				"version": 0
			},
			"stamp": 2,
			"data": []
		},
		"Screen": {
			"type": "FlexibleFormLayout",
			"options": {
				"splitterType": "complex",
				"version": 0,
				"maxwidth": 1440,
				"backColor": "text-backgroundcolor-default",
				"style": null
			},
			"*": {
				"form-header": {
					"type": "FlexibleFormHeader",
					"options": {
						"version": 0,
						"maxwidth": 1440
					},
					"stamp": 3,
					"data": []
				},
				"form-center": {
					"type": "FlexibleFormCenter",
					"options": {
						"version": 0,
						"maxwidth": 1440
					},
					"*": {
						"form-content": {
							"type": "FlexibleFormContent",
							"options": {
								"version": 0
							},
							"*": {
								"form-content-top": {
									"type": "FlexibleFormSplitter",
									"options": {
										"autosize": true,
										"version": 1,
										"box": {
											"top": "0%",
											"left": "0%",
											"width": null,
											"height": "17%"
										},
										"hidden": true
									},
									"*": {
										"form-content-top-1": {
											"type": "FlexibleFormLine",
											"options": {
												"version": 0
											},
											"*": {
												"DocumentsPanel": {
													"type": "PanelDocument",
													"options": {
														"version": 0,
														"border": {},
														"labelLength": 0,
														"iconURL": "",
														"TitleFontSize": "S",
														"hideActionButtons": false,
														"hideTitleBar": false,
														"hideTitle": false,
														"label": "_Documents",
														"hidden": true,
														"sameHeightAsSiblings": false
													},
													"*": {
														"Document": {
															"data": [
																"documentscontroldata"
															],
															"type": "Documents",
															"options": {
																"version": 0
															},
															"stamp": 4
														}
													},
													"stamp": 5,
													"data": []
												}
											},
											"stamp": 6,
											"data": []
										}
									},
									"stamp": 7,
									"data": []
								},
								"form-content-bottom": {
									"type": "FlexibleFormSplitter",
									"options": {
										"version": 1,
										"box": {
											"top": "0%",
											"left": "0%",
											"width": null,
											"height": "100%"
										},
										"hidden": false,
										"hideSeparator": true
									},
									"*": {
										"form-content-left": {
											"type": "FlexibleFormSplitter",
											"options": {
												"version": 1,
												"box": {
													"top": "0%",
													"left": "0%",
													"width": "55%",
													"height": null
												},
												"hidden": false
											},
											"*": {
												"form-content-center-3": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 8,
													"*": {
														"Wizard_steps": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": true,
																"hideTitleBar": true,
																"hideTitle": true,
																"backgroundcolor": "color1",
																"labelsAlignment": "right",
																"label": "Wizard steps",
																"leftImageURL": "",
																"removeMargins": false,
																"version": 0,
																"hidden": false,
																"sameHeightAsSiblings": false
															},
															"stamp": 9,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"Wizard_steps__": "LabelWizard_steps__",
																			"LabelWizard_steps__": "Wizard_steps__"
																		},
																		"version": 0
																	},
																	"stamp": 10,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"Spacer_line__": {
																						"line": 2,
																						"column": 1
																					},
																					"Wizard_steps__": {
																						"line": 1,
																						"column": 2
																					},
																					"LabelWizard_steps__": {
																						"line": 1,
																						"column": 1
																					},
																					"Wizard_step_desc__": {
																						"line": 3,
																						"column": 1
																					},
																					"Spacer_line2__": {
																						"line": 4,
																						"column": 1
																					}
																				},
																				"lines": 4,
																				"columns": 2,
																				"colspans": [
																					[],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					]
																				],
																				"version": 0
																			},
																			"stamp": 11,
																			"*": {
																				"Wizard_steps__": {
																					"type": "HTML",
																					"data": false,
																					"options": {
																						"label": "",
																						"width": "",
																						"css": "@ .wizard-container {\n\tleft: 50%;\n\tposition: absolute;\n}@ \n.wizard-timeline {\n\tcontent: \"\";\n\theight: 3px;\n\tmargin-top: 35px;\n\tposition: absolute;\n\tbackground-color: #00b4bd;\n\tz-index: 10;\n\tleft: 0px;\n}@ \n.wizard {\n\theight: 150px;\n\tpadding-bottom: 15px;\n\tz-index: 30;\n\tleft: 0px;\n\tposition: absolute;\n\tcolor: #263645;\n}@ \n.wizard td {\n\ttext-align: center;\n\tfont-size: 14px;\n}@ \n.wizard td i {\n\twidth: 50px;\n\tpadding: 10px;\n}@ \n.wizard td.text {\n\ttext-transform: uppercase;\n\twhite-space: pre-wrap;\n\tvertical-align: top;\n\tfont-size: 12px;\n}@ \n.wizard td.wizardSelected {\n\tcolor: White;\n}@ ",
																						"version": 0
																					},
																					"stamp": 12
																				},
																				"Spacer_line__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "130",
																						"width": "",
																						"color": "default",
																						"label": "Spacer line",
																						"version": 0
																					},
																					"stamp": 13
																				},
																				"Wizard_step_desc__": {
																					"type": "Text",
																					"data": false,
																					"options": {
																						"marge": 0,
																						"textSize": "M",
																						"textAlignment": "center",
																						"textStyle": "default",
																						"textColor": "color8",
																						"backgroundcolor": "default",
																						"label": "Description",
																						"version": 0,
																						"image_url": "",
																						"iconClass": ""
																					},
																					"stamp": 14
																				},
																				"LabelWizard_steps__": {
																					"type": "Label",
																					"data": false,
																					"options": {
																						"label": "",
																						"version": 0
																					},
																					"stamp": 15
																				},
																				"Spacer_line2__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "",
																						"width": "",
																						"color": "default",
																						"label": "Spacer line2",
																						"version": 0
																					},
																					"stamp": 16
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-center-4": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 17,
													"*": {
														"TitleArrow": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": "300",
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": false,
																"hideTitleBar": true,
																"hideTitle": true,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "TitleArrow",
																"leftImageURL": "",
																"removeMargins": false,
																"version": 0,
																"hidden": false,
																"sameHeightAsSiblings": false
															},
															"stamp": 18,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"LabelConfigurationName__": "ConfigurationName__",
																			"ConfigurationName__": "LabelConfigurationName__",
																			"LabelConfiguration_type__": "Configuration_type__",
																			"Configuration_type__": "LabelConfiguration_type__",
																			"LabelDocument_Type__": "Document_Type__",
																			"Document_Type__": "LabelDocument_Type__",
																			"Family__": "LabelFamily__",
																			"LabelFamily__": "Family__",
																			"Routing__": "LabelRouting__",
																			"LabelRouting__": "Routing__"
																		},
																		"version": 0
																	},
																	"stamp": 19,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"HTMLArrow__": {
																						"line": 1,
																						"column": 1
																					},
																					"LabelConfigurationName__": {
																						"line": 3,
																						"column": 1
																					},
																					"ConfigurationName__": {
																						"line": 3,
																						"column": 2
																					},
																					"Spacer_line23__": {
																						"line": 2,
																						"column": 1
																					},
																					"LabelConfiguration_type__": {
																						"line": 4,
																						"column": 1
																					},
																					"Configuration_type__": {
																						"line": 4,
																						"column": 2
																					},
																					"LabelDocument_Type__": {
																						"line": 5,
																						"column": 1
																					},
																					"Document_Type__": {
																						"line": 5,
																						"column": 2
																					},
																					"Family__": {
																						"line": 6,
																						"column": 2
																					},
																					"LabelFamily__": {
																						"line": 6,
																						"column": 1
																					},
																					"Routing__": {
																						"line": 7,
																						"column": 2
																					},
																					"LabelRouting__": {
																						"line": 7,
																						"column": 1
																					}
																				},
																				"lines": 7,
																				"columns": 2,
																				"colspans": [
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[],
																					[],
																					[],
																					[],
																					[]
																				],
																				"version": 0
																			},
																			"stamp": 20,
																			"*": {
																				"Document_Type__": {
																					"type": "ComboBox",
																					"data": [
																						"Document_type__"
																					],
																					"options": {
																						"possibleValues": {},
																						"version": 1,
																						"keyValueMode": false,
																						"possibleKeys": {},
																						"label": "Document_type__",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"DataSourceOrigin": "Standalone",
																						"DisplayedColumns": "",
																						"maxNbLinesToDisplay": 10
																					},
																					"stamp": 1281
																				},
																				"LabelDocument_Type__": {
																					"type": "Label",
																					"data": [
																						"Document_Type__"
																					],
																					"options": {
																						"label": "Document_Type__",
																						"version": 0
																					},
																					"stamp": 1280
																				},
																				"Configuration_type__": {
																					"type": "ComboBox",
																					"data": [
																						"Configuration_type__"
																					],
																					"options": {
																						"possibleValues": {
																							"0": "Configuration_type_DEMO",
																							"1": "Configuration_type_TEST",
																							"2": "Configuration_type_PROD"
																						},
																						"version": 1,
																						"keyValueMode": true,
																						"possibleKeys": {
																							"0": "DEMO",
																							"1": "TEST",
																							"2": "PROD"
																						},
																						"label": "Configuration_type__",
																						"activable": true,
																						"width": 230,
																						"maxNbLinesToDisplay": 10
																					},
																					"stamp": 47
																				},
																				"LabelConfiguration_type__": {
																					"type": "Label",
																					"data": [
																						"Configuration_type__"
																					],
																					"options": {
																						"label": "Configuration_type__",
																						"version": 0
																					},
																					"stamp": 46
																				},
																				"ConfigurationName__": {
																					"type": "ShortText",
																					"data": [
																						"ConfigurationName__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"label": "ConfigurationName__",
																						"activable": true,
																						"width": 230,
																						"browsable": false,
																						"version": 0,
																						"autocompletable": false,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format"
																					},
																					"stamp": 27
																				},
																				"Spacer_line23__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "",
																						"width": "",
																						"color": "default",
																						"label": "Spacer line23",
																						"version": 0
																					},
																					"stamp": 754
																				},
																				"LabelConfigurationName__": {
																					"type": "Label",
																					"data": [
																						"ConfigurationName__"
																					],
																					"options": {
																						"label": "ConfigurationName__",
																						"version": 0
																					},
																					"stamp": 26
																				},
																				"HTMLArrow__": {
																					"type": "HTML",
																					"data": false,
																					"options": {
																						"label": "HTMLArrow",
																						"htmlContent": "<div class=\"wizard-arrow\"></div>",
																						"css": "@ .wizard-arrow {\nbackground-color: white;\nwidth: 0;\nheight: 0;\nborder-style: solid;\nborder-width: 20px 25px 0 25px;\nborder-color: #008998 transparent transparent transparent;\nposition: absolute;\ntop: -10px;\nheight: 9px;\nleft: 50%;\nmargin-left: -25px;\n}@ ",
																						"width": 962,
																						"version": 0
																					},
																					"stamp": 21
																				},
																				"LabelFamily__": {
																					"type": "Label",
																					"data": [
																						"Family__"
																					],
																					"options": {
																						"label": "Family",
																						"version": 0
																					},
																					"stamp": 1956
																				},
																				"Family__": {
																					"type": "ComboBox",
																					"data": [
																						"Family__"
																					],
																					"options": {
																						"possibleValues": {},
																						"version": 1,
																						"keyValueMode": true,
																						"possibleKeys": {},
																						"label": "Family",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"DataSourceOrigin": "Standalone",
																						"DisplayedColumns": "",
																						"maxNbLinesToDisplay": 10
																					},
																					"stamp": 1957
																				},
																				"LabelRouting__": {
																					"type": "Label",
																					"data": [
																						"Routing__"
																					],
																					"options": {
																						"label": "Routing",
																						"version": 0
																					},
																					"stamp": 1995
																				},
																				"Routing__": {
																					"type": "ComboBox",
																					"data": [
																						"Routing__"
																					],
																					"options": {
																						"possibleValues": {
																							"0": "Deliver message",
																							"1": "Routing message"
																						},
																						"version": 1,
																						"keyValueMode": true,
																						"possibleKeys": {
																							"0": "Deliver message",
																							"1": "Routing message"
																						},
																						"label": "Routing",
																						"activable": true,
																						"width": 230,
																						"helpText": "_routingTooltip",
																						"helpURL": "7016",
																						"helpFormat": "HTML Format",
																						"DataSourceOrigin": "Standalone",
																						"DisplayedColumns": "",
																						"maxNbLinesToDisplay": 10
																					},
																					"stamp": 1996
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-7": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 1824,
													"*": {
														"General_pane": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": "300",
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": true,
																"hideTitleBar": false,
																"hideTitle": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "General_pane",
																"leftImageURL": "",
																"removeMargins": false,
																"version": 0,
																"hidden": false,
																"sameHeightAsSiblings": false
															},
															"stamp": 23,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"SubmittedDocType__": "LabelSubmittedDocType__",
																			"LabelSubmittedDocType__": "SubmittedDocType__",
																			"LabelEnable_Configuration__": "Enable_Configuration__",
																			"Enable_Configuration__": "LabelEnable_Configuration__"
																		},
																		"version": 0
																	},
																	"stamp": 24,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"SubmittedDocType__": {
																						"line": 2,
																						"column": 2
																					},
																					"LabelSubmittedDocType__": {
																						"line": 2,
																						"column": 1
																					},
																					"Spacer_line5__": {
																						"line": 5,
																						"column": 1
																					},
																					"DisableExtraction__": {
																						"line": 3,
																						"column": 2
																					},
																					"LabelDisableExtraction__": {
																						"line": 3,
																						"column": 1
																					},
																					"Info_NoCriteria_Configuration__": {
																						"line": 4,
																						"column": 1
																					},
																					"LabelEnable_Configuration__": {
																						"line": 1,
																						"column": 1
																					},
																					"Enable_Configuration__": {
																						"line": 1,
																						"column": 2
																					}
																				},
																				"lines": 5,
																				"columns": 2,
																				"colspans": [
																					[],
																					[],
																					[],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					]
																				],
																				"version": 0
																			},
																			"stamp": 25,
																			"*": {
																				"LabelEnable_Configuration__": {
																					"type": "Label",
																					"data": [
																						"Enable_Configuration__"
																					],
																					"options": {
																						"label": "_EnableConfiguration",
																						"version": 0
																					},
																					"stamp": 1933
																				},
																				"Enable_Configuration__": {
																					"type": "CheckBox",
																					"data": [
																						"Enable_Configuration__"
																					],
																					"options": {
																						"label": "_EnableConfiguration",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"version": 0
																					},
																					"stamp": 1934
																				},
																				"LabelSubmittedDocType__": {
																					"type": "Label",
																					"data": [
																						"SubmittedDocType__"
																					],
																					"options": {
																						"label": "SubmittedDocType__",
																						"version": 0
																					},
																					"stamp": 747
																				},
																				"SubmittedDocType__": {
																					"type": "ComboBox",
																					"data": [
																						"SubmittedDocType__"
																					],
																					"options": {
																						"possibleValues": {
																							"0": "SubmittedDocType__PDF",
																							"1": "SubmittedDocType__TIF",
																							"2": "SubmittedDocType__UBL",
																							"3": "SubmittedDocType__UBLPDF",
																							"4": "SubmittedDocType__XML",
																							"5": "SubmittedDocType__XMLPDF"
																						},
																						"version": 1,
																						"keyValueMode": true,
																						"possibleKeys": {
																							"0": "pdf",
																							"1": "tif",
																							"2": "ubl",
																							"3": "ubl_pdf",
																							"4": "customxml",
																							"5": "customxml_pdf"
																						},
																						"label": "SubmittedDocType__",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"DataSourceOrigin": "Standalone",
																						"DisplayedColumns": "",
																						"maxNbLinesToDisplay": 10
																					},
																					"stamp": 748
																				},
																				"LabelDisableExtraction__": {
																					"type": "Label",
																					"data": [
																						"DisableExtraction__"
																					],
																					"options": {
																						"label": "_Disable_Extraction",
																						"version": 0
																					},
																					"stamp": 1764
																				},
																				"DisableExtraction__": {
																					"type": "CheckBox",
																					"data": [
																						"DisableExtraction__"
																					],
																					"options": {
																						"label": "_Disable_Extraction",
																						"activable": true,
																						"width": 230,
																						"helpText": "_This option is used only with Esker loader and inbound channel. When extraction is disable, the acceptance criteria and the splitting are disable too. The extraction script is still launched.",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"version": 0
																					},
																					"stamp": 1765
																				},
																				"Info_NoCriteria_Configuration__": {
																					"type": "Text",
																					"data": false,
																					"options": {
																						"marge": 310,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "italic",
																						"textColor": "default",
																						"backgroundcolor": "default",
																						"iconClass": "",
																						"label": "Info_NoCriteria_Configuration",
																						"version": 0
																					},
																					"stamp": 1839
																				},
																				"Spacer_line5__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "",
																						"width": "",
																						"color": "default",
																						"label": "Spacer line5",
																						"version": 0
																					},
																					"stamp": 1327
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-20": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 1859,
													"*": {
														"Activate_Recognition_pane": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": "300",
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": true,
																"hideTitleBar": false,
																"hideTitle": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "Activate_Recognition_pane",
																"leftImageURL": "",
																"removeMargins": false,
																"elementsAlignment": "left",
																"version": 0,
																"sameHeightAsSiblings": false
															},
															"stamp": 1852,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"LabelConfigurationSelection_Enable__": "ConfigurationSelection_Enable__",
																			"ConfigurationSelection_Enable__": "LabelConfigurationSelection_Enable__"
																		},
																		"version": 0
																	},
																	"stamp": 1853,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"LabelConfigurationSelection_Enable__": {
																						"line": 1,
																						"column": 1
																					},
																					"ConfigurationSelection_Enable__": {
																						"line": 1,
																						"column": 2
																					},
																					"Info_Templates_Associated__": {
																						"line": 2,
																						"column": 1
																					}
																				},
																				"lines": 2,
																				"columns": 2,
																				"colspans": [
																					[],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					]
																				],
																				"version": 0
																			},
																			"stamp": 1854,
																			"*": {
																				"ConfigurationSelection_Enable__": {
																					"type": "CheckBox",
																					"data": [
																						"ConfigurationSelection_Enable__"
																					],
																					"options": {
																						"label": "ConfigurationSelection_Enable__",
																						"activable": true,
																						"version": 0,
																						"helpText": "_Select this option to apply this configuration according to specific criteria (file name or string in document)",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"width": "230"
																					},
																					"stamp": 29
																				},
																				"Info_Templates_Associated__": {
																					"type": "Text",
																					"data": false,
																					"options": {
																						"marge": 310,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "italic",
																						"textColor": "default",
																						"backgroundcolor": "default",
																						"iconClass": "",
																						"label": "Info_Templates_Associated",
																						"hidden": true,
																						"version": 0
																					},
																					"stamp": 1840
																				},
																				"LabelConfigurationSelection_Enable__": {
																					"type": "Label",
																					"data": [
																						"ConfigurationSelection_Enable__"
																					],
																					"options": {
																						"label": "ConfigurationSelection_Enable__",
																						"version": 0
																					},
																					"stamp": 28
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-21": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 1874,
													"*": {
														"Recognition_Match_Layout_pane": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": "300",
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": true,
																"hideTitleBar": true,
																"hideTitle": true,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "Recognition_Match_Layout_pane",
																"leftImageURL": "",
																"removeMargins": false,
																"elementsAlignment": "left",
																"version": 0,
																"sameHeightAsSiblings": false
															},
															"stamp": 1867,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"LabelMatchDocumentLayout__": "MatchDocumentLayout__",
																			"MatchDocumentLayout__": "LabelMatchDocumentLayout__"
																		},
																		"version": 0
																	},
																	"stamp": 1868,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"LabelMatchDocumentLayout__": {
																						"line": 1,
																						"column": 1
																					},
																					"MatchDocumentLayout__": {
																						"line": 1,
																						"column": 2
																					}
																				},
																				"lines": 1,
																				"columns": 2,
																				"colspans": [
																					[]
																				],
																				"version": 0
																			},
																			"stamp": 1869,
																			"*": {
																				"MatchDocumentLayout__": {
																					"type": "CheckBox",
																					"data": [
																						"MatchDocumentLayout__"
																					],
																					"options": {
																						"label": "MatchDocumentLayout__",
																						"activable": true,
																						"width": 230,
																						"helpText": "_AutoLearningCriteriaTooltip",
																						"helpURL": "7011",
																						"helpFormat": "HTML Format",
																						"version": 0
																					},
																					"stamp": 1846
																				},
																				"LabelMatchDocumentLayout__": {
																					"type": "Label",
																					"data": [
																						"MatchDocumentLayout__"
																					],
																					"options": {
																						"label": "MatchDocumentLayout__",
																						"version": 0
																					},
																					"stamp": 1845
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-23": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 1887,
													"*": {
														"Recognition_Match_Layout_Data_pane": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {
																	"collapsed": false
																},
																"labelLength": "300",
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": true,
																"hideTitleBar": true,
																"hideTitle": true,
																"backgroundcolor": "color7",
																"labelsAlignment": "right",
																"label": "Recognition_Match_Layout_Data_pane",
																"leftImageURL": "",
																"removeMargins": false,
																"elementsAlignment": "left",
																"version": 0,
																"hidden": true,
																"sameHeightAsSiblings": false
															},
															"stamp": 1879,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"LabelTemplateIdList__": "TemplateIdList__",
																			"TemplateIdList__": "LabelTemplateIdList__",
																			"IsAutolearningConf__": "LabelIsAutolearningConf__",
																			"LabelIsAutolearningConf__": "IsAutolearningConf__"
																		},
																		"version": 0
																	},
																	"stamp": 1880,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"LabelTemplateIdList__": {
																						"line": 1,
																						"column": 1
																					},
																					"TemplateIdList__": {
																						"line": 1,
																						"column": 2
																					},
																					"LabelIsAutolearningConf__": {
																						"line": 2,
																						"column": 1
																					},
																					"IsAutolearningConf__": {
																						"line": 2,
																						"column": 2
																					}
																				},
																				"lines": 2,
																				"columns": 2,
																				"colspans": [
																					[],
																					[]
																				],
																				"version": 0
																			},
																			"stamp": 1881,
																			"*": {
																				"TemplateIdList__": {
																					"type": "ShortText",
																					"data": [
																						"TemplateIdList__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"label": "TemplateIdList__",
																						"activable": true,
																						"width": "",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"browsable": false,
																						"autocompletable": false,
																						"hidden": true,
																						"defaultValue": "[]"
																					},
																					"stamp": 1771
																				},
																				"LabelTemplateIdList__": {
																					"type": "Label",
																					"data": false,
																					"options": {
																						"label": "TemplateIdList__",
																						"hidden": true,
																						"version": 0
																					},
																					"stamp": 1772
																				},
																				"LabelIsAutolearningConf__": {
																					"type": "Label",
																					"data": [
																						"IsAutolearningConf__"
																					],
																					"options": {
																						"label": "IsAutolearningConf__",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 2151
																				},
																				"IsAutolearningConf__": {
																					"type": "CheckBox",
																					"data": [
																						"IsAutolearningConf__"
																					],
																					"options": {
																						"label": "IsAutolearningConf__",
																						"activable": true,
																						"readonly": true,
																						"version": 0,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"hidden": true
																					},
																					"stamp": 2152
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-27": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 1909,
													"*": {
														"Recognition_Match_Filename_pane": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": "300",
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": true,
																"hideTitleBar": true,
																"hideTitle": true,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "Recognition_Match_Filename_pane",
																"leftImageURL": "",
																"removeMargins": false,
																"elementsAlignment": "left",
																"version": 0,
																"sameHeightAsSiblings": false
															},
															"stamp": 1892,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"LabelUseFileNameRegEx__": "MatchFilenameRegex__",
																			"MatchFilenameRegex__": "LabelUseFileNameRegEx__"
																		},
																		"version": 0
																	},
																	"stamp": 1893,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"LabelUseFileNameRegEx__": {
																						"line": 1,
																						"column": 1
																					},
																					"MatchFilenameRegex__": {
																						"line": 1,
																						"column": 2
																					}
																				},
																				"lines": 1,
																				"columns": 2,
																				"colspans": [
																					[]
																				],
																				"version": 0
																			},
																			"stamp": 1894,
																			"*": {
																				"MatchFilenameRegex__": {
																					"type": "CheckBox",
																					"data": [
																						"MatchFilenameRegex__"
																					],
																					"options": {
																						"label": "_Match filename with regular expression",
																						"activable": true,
																						"width": 230,
																						"helpText": "_Select this option to apply this configuration according to a specific filename",
																						"helpURL": "7007",
																						"helpFormat": "HTML Format",
																						"version": 0
																					},
																					"stamp": 1776
																				},
																				"LabelUseFileNameRegEx__": {
																					"type": "Label",
																					"data": [
																						"MatchFilenameRegex__"
																					],
																					"options": {
																						"label": "_Match filename with regular expression",
																						"version": 0
																					},
																					"stamp": 1775
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-28": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 1921,
													"*": {
														"Recognition_Match_Filename_Data_pane": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": "300",
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": true,
																"hideTitleBar": true,
																"hideTitle": true,
																"backgroundcolor": "color7",
																"labelsAlignment": "right",
																"label": "Recognition_Match_Filename_Data_pane",
																"leftImageURL": "",
																"removeMargins": false,
																"elementsAlignment": "left",
																"version": 0,
																"hidden": true,
																"sameHeightAsSiblings": false
															},
															"stamp": 1900,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"LabelFilename_regular_expression__": "Filename_regular_expression__",
																			"Filename_regular_expression__": "LabelFilename_regular_expression__",
																			"LabelFileNameRegEx_CaseSensitive__": "FileNameRegEx_CaseSensitive__",
																			"FileNameRegEx_CaseSensitive__": "LabelFileNameRegEx_CaseSensitive__"
																		},
																		"version": 0
																	},
																	"stamp": 1901,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"LabelFilename_regular_expression__": {
																						"line": 1,
																						"column": 1
																					},
																					"Filename_regular_expression__": {
																						"line": 1,
																						"column": 2
																					},
																					"LabelFileNameRegEx_CaseSensitive__": {
																						"line": 2,
																						"column": 1
																					},
																					"FileNameRegEx_CaseSensitive__": {
																						"line": 2,
																						"column": 2
																					}
																				},
																				"lines": 2,
																				"columns": 2,
																				"colspans": [
																					[],
																					[]
																				],
																				"version": 0
																			},
																			"stamp": 1902,
																			"*": {
																				"FileNameRegEx_CaseSensitive__": {
																					"type": "CheckBox",
																					"data": [
																						"FileNameRegEx_CaseSensitive__"
																					],
																					"options": {
																						"label": "MatchCase__",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"version": 0
																					},
																					"stamp": 1782
																				},
																				"LabelFileNameRegEx_CaseSensitive__": {
																					"type": "Label",
																					"data": [
																						"FileNameRegEx_CaseSensitive__"
																					],
																					"options": {
																						"label": "MatchCase__",
																						"version": 0
																					},
																					"stamp": 1781
																				},
																				"Filename_regular_expression__": {
																					"type": "ShortText",
																					"data": [
																						"Filename_regular_expression__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"label": "_Filename regular expression",
																						"activable": true,
																						"width": 230,
																						"helpText": "_Follow the link above for acces help to create regular expression",
																						"helpURL": "2127",
																						"helpFormat": "HTML Format",
																						"browsable": false
																					},
																					"stamp": 1774
																				},
																				"LabelFilename_regular_expression__": {
																					"type": "Label",
																					"data": [
																						"Filename_regular_expression__"
																					],
																					"options": {
																						"label": "_Filename regular expression",
																						"version": 0
																					},
																					"stamp": 1773
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-19": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 1794,
													"*": {
														"Features_pane": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": "300",
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": true,
																"hideTitleBar": true,
																"hideTitle": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "Features_pane",
																"leftImageURL": "",
																"removeMargins": false,
																"elementsAlignment": "left",
																"version": 0,
																"sameHeightAsSiblings": false
															},
															"stamp": 1786,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"LabelMoreSubmissionOptions__": "MoreSubmissionOptions__",
																			"MoreSubmissionOptions__": "LabelMoreSubmissionOptions__"
																		},
																		"version": 0
																	},
																	"stamp": 1787,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"LabelMoreSubmissionOptions__": {
																						"line": 1,
																						"column": 1
																					},
																					"MoreSubmissionOptions__": {
																						"line": 1,
																						"column": 2
																					}
																				},
																				"lines": 1,
																				"columns": 2,
																				"colspans": [
																					[]
																				],
																				"version": 0
																			},
																			"stamp": 1788,
																			"*": {
																				"MoreSubmissionOptions__": {
																					"type": "HTML",
																					"data": false,
																					"options": {
																						"label": " ",
																						"htmlContent": "<span class=\"adv\"><i class=\"fa fa-plus-square\"></i> $TITLE$</span>",
																						"css": "@ .adv { cursor: pointer; }@ \n.adv:hover { text-decoration:underline; }@ ",
																						"version": 0
																					},
																					"stamp": 48
																				},
																				"LabelMoreSubmissionOptions__": {
																					"type": "Label",
																					"data": false,
																					"options": {
																						"label": " ",
																						"version": 0
																					},
																					"stamp": 49
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-center-16": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 64,
													"*": {
														"General_advanced_options_pane": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": "300",
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": false,
																"hideTitleBar": true,
																"hideTitle": false,
																"backgroundcolor": "color7",
																"labelsAlignment": "right",
																"label": "General_advanced_options_pane",
																"leftImageURL": "",
																"removeMargins": false,
																"version": 0,
																"hidden": true,
																"sameHeightAsSiblings": false
															},
															"stamp": 65,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"ForwardAdditionalAttach__": "LabelForwardAdditionalAttach__",
																			"LabelForwardAdditionalAttach__": "ForwardAdditionalAttach__",
																			"Billing_Info_Billing_Account__": "LabelBilling_Info_Billing_Account__",
																			"LabelBilling_Info_Billing_Account__": "Billing_Info_Billing_Account__",
																			"Billing_Info_Cost_Center__": "LabelBilling_Info_Cost_Center__",
																			"LabelBilling_Info_Cost_Center__": "Billing_Info_Cost_Center__",
																			"Redirect_sender_notifications__": "LabelRedirect_sender_notifications__",
																			"LabelRedirect_sender_notifications__": "Redirect_sender_notifications__",
																			"Redirect_recipient_notifications__": "LabelRedirect_recipient_notifications__",
																			"LabelRedirect_recipient_notifications__": "Redirect_recipient_notifications__",
																			"Simulate_delivery__": "LabelSimulate_delivery__",
																			"LabelSimulate_delivery__": "Simulate_delivery__",
																			"LabelConfiguration_template__": "Configuration_template__",
																			"Configuration_template__": "LabelConfiguration_template__",
																			"OutputFilenamePattern__": "LabelOutputFilenamePattern__",
																			"LabelOutputFilenamePattern__": "OutputFilenamePattern__",
																			"EndOfValidityNotification__": "LabelEndOfValidityNotification__",
																			"LabelEndOfValidityNotification__": "EndOfValidityNotification__"
																		},
																		"version": 0
																	},
																	"stamp": 66,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"Redirect_email_to__": {
																						"line": 1,
																						"column": 1
																					},
																					"Advanced__": {
																						"line": 7,
																						"column": 1
																					},
																					"ForwardAdditionalAttach__": {
																						"line": 9,
																						"column": 2
																					},
																					"LabelForwardAdditionalAttach__": {
																						"line": 9,
																						"column": 1
																					},
																					"Billing_Info__": {
																						"line": 4,
																						"column": 1
																					},
																					"Billing_Info_Billing_Account__": {
																						"line": 5,
																						"column": 2
																					},
																					"LabelBilling_Info_Billing_Account__": {
																						"line": 5,
																						"column": 1
																					},
																					"Billing_Info_Cost_Center__": {
																						"line": 6,
																						"column": 2
																					},
																					"LabelBilling_Info_Cost_Center__": {
																						"line": 6,
																						"column": 1
																					},
																					"Redirect_sender_notifications__": {
																						"line": 2,
																						"column": 2
																					},
																					"LabelRedirect_sender_notifications__": {
																						"line": 2,
																						"column": 1
																					},
																					"Redirect_recipient_notifications__": {
																						"line": 3,
																						"column": 2
																					},
																					"LabelRedirect_recipient_notifications__": {
																						"line": 3,
																						"column": 1
																					},
																					"Simulate_delivery__": {
																						"line": 8,
																						"column": 2
																					},
																					"LabelSimulate_delivery__": {
																						"line": 8,
																						"column": 1
																					},
																					"LabelConfiguration_template__": {
																						"line": 10,
																						"column": 1
																					},
																					"Configuration_template__": {
																						"line": 10,
																						"column": 2
																					},
																					"OutputFilenamePattern__": {
																						"line": 11,
																						"column": 2
																					},
																					"LabelOutputFilenamePattern__": {
																						"line": 11,
																						"column": 1
																					},
																					"EndOfValidityNotification__": {
																						"line": 12,
																						"column": 2
																					},
																					"LabelEndOfValidityNotification__": {
																						"line": 12,
																						"column": 1
																					}
																				},
																				"lines": 12,
																				"columns": 2,
																				"colspans": [
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[],
																					[],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[],
																					[],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[],
																					[],
																					[],
																					[],
																					[]
																				],
																				"version": 0
																			},
																			"stamp": 67,
																			"*": {
																				"Configuration_template__": {
																					"type": "CheckBox",
																					"data": [
																						"Configuration_template__"
																					],
																					"options": {
																						"label": "Configuration_template__",
																						"activable": true,
																						"width": 230,
																						"version": 0,
																						"helpText": "_Select this option to specify that this configuration is used as a base to create a new auto learning configuration",
																						"helpURL": "7009",
																						"readonly": true,
																						"helpFormat": "HTML Format"
																					},
																					"stamp": 1769
																				},
																				"LabelConfiguration_template__": {
																					"type": "Label",
																					"data": [
																						"Configuration_template__"
																					],
																					"options": {
																						"label": "Configuration_template__",
																						"version": 0
																					},
																					"stamp": 1768
																				},
																				"Redirect_email_to__": {
																					"type": "Title",
																					"data": false,
																					"options": {
																						"marge": 0,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"backgroundcolor": "default",
																						"label": "Redirect_email_to_part",
																						"version": 0,
																						"image_url": "",
																						"iconClass": ""
																					},
																					"stamp": 72
																				},
																				"LabelRedirect_sender_notifications__": {
																					"type": "Label",
																					"data": [
																						"Redirect_sender_notifications__"
																					],
																					"options": {
																						"label": "Redirect_sender_notifications__",
																						"version": 0
																					},
																					"stamp": 73
																				},
																				"Redirect_sender_notifications__": {
																					"type": "ShortText",
																					"data": [
																						"Redirect_sender_notifications__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"label": "Redirect_sender_notifications__",
																						"activable": true,
																						"width": 230,
																						"version": 0,
																						"length": 250,
																						"browsable": false,
																						"autocompletable": false,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format"
																					},
																					"stamp": 74
																				},
																				"Redirect_recipient_notifications__": {
																					"type": "ShortText",
																					"data": [
																						"Redirect_recipient_notifications__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"label": "Redirect_recipient_notifications__",
																						"activable": true,
																						"width": 230,
																						"version": 0,
																						"length": 250,
																						"browsable": false,
																						"autocompletable": false,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format"
																					},
																					"stamp": 86
																				},
																				"LabelRedirect_recipient_notifications__": {
																					"type": "Label",
																					"data": [
																						"Redirect_recipient_notifications__"
																					],
																					"options": {
																						"label": "Redirect_recipient_notifications__",
																						"version": 0
																					},
																					"stamp": 87
																				},
																				"Billing_Info__": {
																					"type": "Title",
																					"data": false,
																					"options": {
																						"marge": 0,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"backgroundcolor": "default",
																						"label": "Billing_Info_part",
																						"version": 0,
																						"image_url": "",
																						"iconClass": ""
																					},
																					"stamp": 769
																				},
																				"LabelBilling_Info_Billing_Account__": {
																					"type": "Label",
																					"data": [
																						"Billing_Info_Billing_Account__"
																					],
																					"options": {
																						"label": "Billing_Info_Billing_Account__",
																						"version": 0
																					},
																					"stamp": 770
																				},
																				"Billing_Info_Billing_Account__": {
																					"type": "ShortText",
																					"data": [
																						"Billing_Info_Billing_Account__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"label": "Billing_Info_Billing_Account__",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"browsable": false,
																						"autocompletable": false,
																						"version": 0
																					},
																					"stamp": 771
																				},
																				"LabelBilling_Info_Cost_Center__": {
																					"type": "Label",
																					"data": [
																						"Billing_Info_Cost_Center__"
																					],
																					"options": {
																						"label": "Billing_Info_Cost_Center__",
																						"version": 0
																					},
																					"stamp": 772
																				},
																				"Billing_Info_Cost_Center__": {
																					"type": "ShortText",
																					"data": [
																						"Billing_Info_Cost_Center__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"label": "Billing_Info_Cost_Center__",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"browsable": false,
																						"autocompletable": false,
																						"version": 0
																					},
																					"stamp": 773
																				},
																				"Advanced__": {
																					"type": "Title",
																					"data": false,
																					"options": {
																						"marge": 0,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"backgroundcolor": "default",
																						"label": "Advanced_part",
																						"version": 0,
																						"image_url": "",
																						"iconClass": ""
																					},
																					"stamp": 105
																				},
																				"LabelSimulate_delivery__": {
																					"type": "Label",
																					"data": [
																						"Simulate_delivery__"
																					],
																					"options": {
																						"label": "Simulate_delivery__",
																						"version": 0
																					},
																					"stamp": 108
																				},
																				"Simulate_delivery__": {
																					"type": "CheckBox",
																					"data": [
																						"Simulate_delivery__"
																					],
																					"options": {
																						"label": "Simulate_delivery__",
																						"activable": true,
																						"version": 0,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format"
																					},
																					"stamp": 109
																				},
																				"LabelForwardAdditionalAttach__": {
																					"type": "Label",
																					"data": [
																						"ForwardAdditionalAttach__"
																					],
																					"options": {
																						"label": "ForwardAdditionalAttach__",
																						"version": 0
																					},
																					"stamp": 112
																				},
																				"ForwardAdditionalAttach__": {
																					"type": "CheckBox",
																					"data": [
																						"ForwardAdditionalAttach__"
																					],
																					"options": {
																						"label": "ForwardAdditionalAttach__",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"version": 0
																					},
																					"stamp": 113
																				},
																				"LabelOutputFilenamePattern__": {
																					"type": "Label",
																					"data": [
																						"OutputFilenamePattern__"
																					],
																					"options": {
																						"label": "OutputFilenamePattern__",
																						"version": 0
																					},
																					"stamp": 1843
																				},
																				"OutputFilenamePattern__": {
																					"type": "ShortText",
																					"data": [
																						"OutputFilenamePattern__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"label": "OutputFilenamePattern__",
																						"activable": true,
																						"width": 230,
																						"helpText": "Output_FileName_Pattern_Tooltip",
																						"helpURL": "7010",
																						"helpFormat": "HTML Format",
																						"browsable": false,
																						"length": 200,
																						"defaultValue": "%[Document_Type__] %[Document_ID__]"
																					},
																					"stamp": 1844
																				},
																				"LabelEndOfValidityNotification__": {
																					"type": "Label",
																					"data": [
																						"EndOfValidityNotification__"
																					],
																					"options": {
																						"label": "EndOfValidityNotification__",
																						"version": 0
																					},
																					"stamp": 1954
																				},
																				"EndOfValidityNotification__": {
																					"type": "CheckBox",
																					"data": [
																						"EndOfValidityNotification__"
																					],
																					"options": {
																						"label": "EndOfValidityNotification__",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"version": 0
																					},
																					"stamp": 1955
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-center-5": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 116,
													"*": {
														"Splitting_pane": {
															"type": "PanelData",
															"options": {
																"border": {
																	"collapsed": false
																},
																"version": 0,
																"labelLength": "300",
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": true,
																"hideTitleBar": false,
																"hideTitle": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "Splitting_pane",
																"leftImageURL": "",
																"removeMargins": false,
																"hidden": false,
																"sameHeightAsSiblings": false
															},
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"options": {
																		"controlsAssociation": {
																			"Split_Location__": "LabelSplit_Location__",
																			"LabelSplit_Location__": "Split_Location__",
																			"Split_Offset__": "LabelSplit_Offset__",
																			"LabelSplit_Offset__": "Split_Offset__",
																			"Split_DivisionMethod__": "LabelSplit_DivisionMethod__",
																			"LabelSplit_DivisionMethod__": "Split_DivisionMethod__",
																			"Split_Area__": "LabelSplit_Area__",
																			"LabelSplit_Area__": "Split_Area__",
																			"Split_NumberPages__": "LabelSplit_NumberPages__",
																			"LabelSplit_NumberPages__": "Split_NumberPages__",
																			"Split_String__": "LabelSplit_String__",
																			"LabelSplit_String__": "Split_String__",
																			"Split_UseRegex__": "LabelSplit_UseRegex__",
																			"LabelSplit_UseRegex__": "Split_UseRegex__",
																			"Split_CaseSensitive__": "LabelSplit_CaseSensitive__",
																			"LabelSplit_CaseSensitive__": "Split_CaseSensitive__",
																			"Split_AreaRegex__": "LabelSplit_AreaRegex__",
																			"LabelSplit_AreaRegex__": "Split_AreaRegex__",
																			"Split_AreaMustBeFilled__": "LabelSplit_AreaMustBeFilled__",
																			"LabelSplit_AreaMustBeFilled__": "Split_AreaMustBeFilled__"
																		},
																		"version": 0
																	},
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"options": {
																				"lines": 13,
																				"columns": 2,
																				"version": 0,
																				"ctrlsPos": {
																					"Split_Location__": {
																						"line": 8,
																						"column": 2
																					},
																					"LabelSplit_Location__": {
																						"line": 8,
																						"column": 1
																					},
																					"Split_Offset__": {
																						"line": 3,
																						"column": 2
																					},
																					"LabelSplit_Offset__": {
																						"line": 3,
																						"column": 1
																					},
																					"Split_DivisionMethod__": {
																						"line": 5,
																						"column": 2
																					},
																					"LabelSplit_DivisionMethod__": {
																						"line": 5,
																						"column": 1
																					},
																					"Split_Area__": {
																						"line": 9,
																						"column": 2
																					},
																					"LabelSplit_Area__": {
																						"line": 9,
																						"column": 1
																					},
																					"Split_NumberPages__": {
																						"line": 6,
																						"column": 2
																					},
																					"LabelSplit_NumberPages__": {
																						"line": 6,
																						"column": 1
																					},
																					"Split_String__": {
																						"line": 7,
																						"column": 2
																					},
																					"LabelSplit_String__": {
																						"line": 7,
																						"column": 1
																					},
																					"Split_UseRegex__": {
																						"line": 10,
																						"column": 2
																					},
																					"LabelSplit_UseRegex__": {
																						"line": 10,
																						"column": 1
																					},
																					"Split_CaseSensitive__": {
																						"line": 12,
																						"column": 2
																					},
																					"LabelSplit_CaseSensitive__": {
																						"line": 12,
																						"column": 1
																					},
																					"HTMLSplittingPreview__": {
																						"line": 1,
																						"column": 1
																					},
																					"Spacer_line3__": {
																						"line": 2,
																						"column": 1
																					},
																					"SplittingRestrictionDescription__": {
																						"line": 4,
																						"column": 1
																					},
																					"Split_AreaRegex__": {
																						"line": 11,
																						"column": 2
																					},
																					"LabelSplit_AreaRegex__": {
																						"line": 11,
																						"column": 1
																					},
																					"Split_AreaMustBeFilled__": {
																						"line": 13,
																						"column": 2
																					},
																					"LabelSplit_AreaMustBeFilled__": {
																						"line": 13,
																						"column": 1
																					}
																				},
																				"colspans": [
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[]
																				]
																			},
																			"data": [
																				"fields"
																			],
																			"*": {
																				"HTMLSplittingPreview__": {
																					"type": "HTML",
																					"data": false,
																					"options": {
																						"label": "Splitting_Preview",
																						"css": "@ .preview {\nposition: absolute;\nleft: 220px;\ntop: 0px;\n}@ \n.preview td {\n\ttext-align: left;\n\tfont-size: 20pt;\nheight: 20px;\n}@ \n.preview td.empty {\nwidth: 42px;\n}@ \n.preview td.split {\n\tfont-size: 10pt;\n\tcolor: red;\n}@ \n.dots {\n\tposition: relative;\n\tleft: 7px;\n\ttop: 2px;\n\tborder-left: dashed 1px red;\n\theight: 25px;\n\twidth: 2px;\n}@ \n\n.key {\n\tposition: relative;\n\tfloat: left;\n\tleft: 7px;\n\ttop: 15px;\n\tborder-left: solid 1px black;\n\theight: 15px;\n\twidth: 2px;\n}@ \n.key.right {\n\tfloat: right;\n\tleft: inherit;\n\tright: 10px;\n}@ \n.preview td.keystring {\n\ttext-align: left;\n\tfont-size: 10pt;\n}@ \n.preview td.keystring.right {\n\ttext-align: right;\n}@ \n.elli {\n\tfont-size: 10pt;\n}@ \nspan {\n\tpadding: 3px;\n}@ \n.preview td.docbordernone {\n\theight: 7px;\n}@ \n.preview td.docborderend {\n\tborder-bottom: solid 2px LightGray;\n\tborder-left: solid 2px LightGray;\n\theight: 7px;\n}@ \n.preview td.docborder {\n\tborder-bottom: solid 2px LightGray;\n\tborder-left: solid 2px LightGray;\n\tborder-right: solid 2px LightGray;\n\theight: 7px;\n}@ \n\n.preview td.doc {\n\tfont-size: 10pt;\n\ttext-align: center;\n}@ ",
																						"width": 962,
																						"version": 0
																					},
																					"stamp": 117
																				},
																				"Spacer_line3__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "100",
																						"width": "",
																						"color": "default",
																						"label": "Spacer line3",
																						"version": 0
																					},
																					"stamp": 118
																				},
																				"LabelSplit_Offset__": {
																					"type": "Label",
																					"data": [
																						"Split_Offset__"
																					],
																					"options": {
																						"label": "Split_Offset__",
																						"version": 0
																					},
																					"stamp": 119
																				},
																				"Split_Offset__": {
																					"type": "Integer",
																					"data": [
																						"Split_Offset__"
																					],
																					"options": {
																						"version": 1,
																						"textSize": "S",
																						"textAlignment": "right",
																						"textStyle": "default",
																						"textColor": "default",
																						"integer": true,
																						"label": "Split_Offset__",
																						"precision_internal": 0,
																						"precision_current": 0,
																						"activable": true,
																						"width": "250",
																						"browsable": false,
																						"autocompletable": false
																					},
																					"stamp": 120
																				},
																				"SplittingRestrictionDescription__": {
																					"type": "Text",
																					"data": false,
																					"options": {
																						"marge": 316,
																						"textSize": "MS",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"backgroundcolor": "default",
																						"label": "_Splitting is restricted to PDF-only extractions",
																						"version": 0,
																						"image_url": "",
																						"iconClass": ""
																					},
																					"stamp": 751
																				},
																				"LabelSplit_DivisionMethod__": {
																					"type": "Label",
																					"data": [
																						"Split_DivisionMethod__"
																					],
																					"options": {
																						"label": "Split_DivisionMethod__",
																						"version": 0
																					},
																					"stamp": 121
																				},
																				"Split_DivisionMethod__": {
																					"type": "ComboBox",
																					"data": [
																						"Split_DivisionMethod__"
																					],
																					"options": {
																						"possibleValues": {
																							"0": "_Split_DivisionMethod_SIMPLE",
																							"1": "_Split_DivisionMethod_NPAGES",
																							"2": "_Split_DivisionMethod_ONSTRING",
																							"3": "_Split_DivisionMethod_ONAREA"
																						},
																						"version": 1,
																						"keyValueMode": true,
																						"possibleKeys": {
																							"0": "SIMPLE",
																							"1": "NPAGES",
																							"2": "ONSTRING",
																							"3": "ONAREA"
																						},
																						"label": "Split_DivisionMethod__",
																						"activable": true,
																						"width": "250",
																						"maxNbLinesToDisplay": 10
																					},
																					"stamp": 122
																				},
																				"LabelSplit_NumberPages__": {
																					"type": "Label",
																					"data": [
																						"Split_NumberPages__"
																					],
																					"options": {
																						"label": "Split_NumberPages__",
																						"version": 0
																					},
																					"stamp": 123
																				},
																				"Split_NumberPages__": {
																					"type": "Integer",
																					"data": [
																						"Split_NumberPages__"
																					],
																					"options": {
																						"version": 1,
																						"textSize": "S",
																						"textAlignment": "right",
																						"textStyle": "default",
																						"textColor": "default",
																						"integer": true,
																						"label": "Split_NumberPages__",
																						"precision_internal": 0,
																						"precision_current": 0,
																						"activable": true,
																						"width": "250",
																						"defaultValue": "",
																						"autocompletable": false,
																						"browsable": false
																					},
																					"stamp": 124
																				},
																				"LabelSplit_String__": {
																					"type": "Label",
																					"data": [
																						"Split_String__"
																					],
																					"options": {
																						"label": "Split_String__",
																						"version": 0
																					},
																					"stamp": 125
																				},
																				"Split_String__": {
																					"type": "ShortText",
																					"data": [
																						"Split_String__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"label": "Split_String__",
																						"activable": true,
																						"width": "250",
																						"browsable": false,
																						"version": 0,
																						"autocompletable": true
																					},
																					"stamp": 126
																				},
																				"LabelSplit_Location__": {
																					"type": "Label",
																					"data": [
																						"Split_Location__"
																					],
																					"options": {
																						"label": "Split_Location__",
																						"version": 0
																					},
																					"stamp": 127
																				},
																				"Split_Location__": {
																					"type": "ComboBox",
																					"data": [
																						"Split_Location__"
																					],
																					"options": {
																						"possibleValues": {
																							"0": "_Split_Location_Before",
																							"1": "_Split_Location_After"
																						},
																						"version": 1,
																						"keyValueMode": true,
																						"possibleKeys": {
																							"0": "0",
																							"1": "1"
																						},
																						"label": "Split_Location__",
																						"activable": true,
																						"width": "250",
																						"maxNbLinesToDisplay": 10
																					},
																					"stamp": 128
																				},
																				"LabelSplit_Area__": {
																					"type": "Label",
																					"data": [
																						"Split_Area__"
																					],
																					"options": {
																						"label": "Split_Area__",
																						"version": 0
																					},
																					"stamp": 129
																				},
																				"Split_Area__": {
																					"type": "ShortText",
																					"data": [
																						"Split_Area__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"label": "Split_Area__",
																						"activable": true,
																						"width": "250",
																						"browsable": false,
																						"version": 0,
																						"autocompletable": false
																					},
																					"stamp": 130
																				},
																				"LabelSplit_UseRegex__": {
																					"type": "Label",
																					"data": [
																						"Split_UseRegex__"
																					],
																					"options": {
																						"label": "Split_UseRegex__",
																						"version": 0
																					},
																					"stamp": 131
																				},
																				"Split_UseRegex__": {
																					"type": "CheckBox",
																					"data": [
																						"Split_UseRegex__"
																					],
																					"options": {
																						"label": "Split_UseRegex__",
																						"activable": true,
																						"version": 0
																					},
																					"stamp": 132
																				},
																				"LabelSplit_AreaRegex__": {
																					"type": "Label",
																					"data": [
																						"Split_String__"
																					],
																					"options": {
																						"label": "Split_AreaRegex__",
																						"version": 0,
																						"dataSource": 1
																					},
																					"stamp": 1940
																				},
																				"Split_AreaRegex__": {
																					"type": "ShortText",
																					"data": [
																						"Split_String__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"label": "Split_AreaRegex__",
																						"activable": true,
																						"width": 250,
																						"helpText": "_Filter matching areas",
																						"helpURL": "7014",
																						"helpFormat": "HTML Format",
																						"dataSource": 1,
																						"readonly": false,
																						"notInDB": true,
																						"dataType": "String",
																						"browsable": false,
																						"autocompletable": false
																					},
																					"stamp": 1941
																				},
																				"LabelSplit_CaseSensitive__": {
																					"type": "Label",
																					"data": [
																						"Split_CaseSensitive__"
																					],
																					"options": {
																						"label": "MatchCase__",
																						"version": 0
																					},
																					"stamp": 133
																				},
																				"Split_CaseSensitive__": {
																					"type": "CheckBox",
																					"data": [
																						"Split_CaseSensitive__"
																					],
																					"options": {
																						"label": "MatchCase__",
																						"activable": true,
																						"version": 0
																					},
																					"stamp": 134
																				},
																				"LabelSplit_AreaMustBeFilled__": {
																					"type": "Label",
																					"data": [
																						"Split_AreaMustBeFilled__"
																					],
																					"options": {
																						"label": "Split_AreaMustBeFilled__",
																						"version": 0
																					},
																					"stamp": 1952
																				},
																				"Split_AreaMustBeFilled__": {
																					"type": "CheckBox",
																					"data": [
																						"Split_AreaMustBeFilled__"
																					],
																					"options": {
																						"label": "Split_AreaMustBeFilled__",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"version": 0
																					},
																					"stamp": 1953
																				}
																			},
																			"stamp": 135
																		}
																	},
																	"stamp": 136,
																	"data": []
																}
															},
															"stamp": 137,
															"data": []
														}
													}
												},
												"form-content-left-16": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 1077,
													"*": {
														"Splitting_Buttons_Pane": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": "300",
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": false,
																"hideTitleBar": true,
																"hideTitle": false,
																"backgroundcolor": "default",
																"labelsAlignment": "left",
																"label": "Splitting_Buttons_Pane",
																"leftImageURL": "",
																"removeMargins": true,
																"version": 0,
																"hidden": false,
																"sameHeightAsSiblings": false
															},
															"stamp": 423,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"Split_ShowPreview__": "LabelSplit_ShowPreview__",
																			"LabelSplit_ShowPreview__": "Split_ShowPreview__"
																		},
																		"version": 0
																	},
																	"stamp": 424,
																	"*": {
																		"Grid": {
																			"type": "HorizontalLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"Split_ShowPreview__": {
																						"line": 1,
																						"column": 2
																					},
																					"LabelSplit_ShowPreview__": {
																						"line": 1,
																						"column": 1
																					},
																					"Split_ResetPreview__": {
																						"line": 2,
																						"column": 1
																					}
																				},
																				"lines": 2,
																				"colspans": [
																					[],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					]
																				],
																				"version": 0
																			},
																			"stamp": 425,
																			"*": {
																				"LabelSplit_ShowPreview__": {
																					"type": "Label",
																					"data": false,
																					"options": {
																						"label": "",
																						"version": 0
																					},
																					"stamp": 426
																				},
																				"Split_ShowPreview__": {
																					"type": "FormButton",
																					"data": false,
																					"options": {
																						"buttonLabel": "Split_ShowPreview__",
																						"label": "",
																						"textPosition": "text-right",
																						"textSize": "MS",
																						"textColor": "color1",
																						"action": "none",
																						"url": "",
																						"width": "",
																						"version": 0,
																						"style": 1,
																						"textStyle": "default"
																					},
																					"stamp": 427
																				},
																				"Split_ResetPreview__": {
																					"type": "FormButton",
																					"data": false,
																					"options": {
																						"buttonLabel": "Split_ResetPreview__",
																						"label": "Button2",
																						"textPosition": "text-right",
																						"textSize": "MS",
																						"textColor": "color1",
																						"width": "",
																						"action": "none",
																						"url": "",
																						"version": 0
																					},
																					"stamp": 428
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-center-6": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 138,
													"*": {
														"Validation_pane": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": "300",
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": true,
																"hideTitleBar": false,
																"hideTitle": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "Validation_pane",
																"leftImageURL": "",
																"removeMargins": false,
																"version": 0,
																"hidden": false,
																"sameHeightAsSiblings": false
															},
															"stamp": 139,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"LabelEnableErrorNotification__": "EnableErrorNotification__",
																			"EnableErrorNotification__": "LabelEnableErrorNotification__",
																			"ForceValidation__": "LabelForceValidation__",
																			"LabelForceValidation__": "ForceValidation__",
																			"LabelDisplayRejectButton__": "DisplayRejectButton__",
																			"MoreValidationOptions__": "LabelMoreValidationOptions__",
																			"LabelMoreValidationOptions__": "MoreValidationOptions__",
																			"Enable_conversation__": "LabelEnable_conversation__",
																			"LabelEnable_conversation__": "Enable_conversation__"
																		},
																		"version": 0
																	},
																	"stamp": 140,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"ForceValidation__": {
																						"line": 1,
																						"column": 2
																					},
																					"LabelForceValidation__": {
																						"line": 1,
																						"column": 1
																					},
																					"LabelEnableErrorNotification__": {
																						"line": 2,
																						"column": 1
																					},
																					"EnableErrorNotification__": {
																						"line": 2,
																						"column": 2
																					},
																					"MoreValidationOptions__": {
																						"line": 3,
																						"column": 2
																					},
																					"LabelMoreValidationOptions__": {
																						"line": 3,
																						"column": 1
																					},
																					"Enable_conversation__": {
																						"line": 4,
																						"column": 2
																					},
																					"LabelEnable_conversation__": {
																						"line": 4,
																						"column": 1
																					}
																				},
																				"lines": 4,
																				"columns": 2,
																				"colspans": [
																					[],
																					[],
																					[],
																					[]
																				],
																				"version": 0
																			},
																			"stamp": 141,
																			"*": {
																				"LabelForceValidation__": {
																					"type": "Label",
																					"data": [
																						"ForceValidation__"
																					],
																					"options": {
																						"label": "ForceValidation__",
																						"version": 0
																					},
																					"stamp": 144
																				},
																				"ForceValidation__": {
																					"type": "CheckBox",
																					"data": [
																						"ForceValidation__"
																					],
																					"options": {
																						"label": "ForceValidation__",
																						"activable": true,
																						"version": 0,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format"
																					},
																					"stamp": 145
																				},
																				"LabelEnableErrorNotification__": {
																					"type": "Label",
																					"data": [
																						"EnableErrorNotification__"
																					],
																					"options": {
																						"label": "EnableErrorNotification__",
																						"version": 0
																					},
																					"stamp": 143
																				},
																				"EnableErrorNotification__": {
																					"type": "CheckBox",
																					"data": [
																						"EnableErrorNotification__"
																					],
																					"options": {
																						"label": "EnableErrorNotification__",
																						"activable": true,
																						"defaultValue": true,
																						"version": 0
																					},
																					"stamp": 142
																				},
																				"LabelMoreValidationOptions__": {
																					"type": "Label",
																					"data": false,
																					"options": {
																						"label": "",
																						"version": 0
																					},
																					"stamp": 1312
																				},
																				"MoreValidationOptions__": {
																					"type": "HTML",
																					"data": false,
																					"options": {
																						"label": "",
																						"css": "@ .adv { cursor: pointer; }@ \n.adv:hover { text-decoration:underline; }@ ",
																						"htmlContent": "<span class=\"adv\"><i class=\"fa fa-plus-square\"></i> $TITLE$</span>",
																						"version": 0
																					},
																					"stamp": 1313
																				},
																				"LabelEnable_conversation__": {
																					"type": "Label",
																					"data": [
																						"Enable_conversation__"
																					],
																					"options": {
																						"label": "Enable  conversation",
																						"version": 0
																					},
																					"stamp": 2160
																				},
																				"Enable_conversation__": {
																					"type": "CheckBox",
																					"data": [
																						"Enable_conversation__"
																					],
																					"options": {
																						"label": "Enable  conversation",
																						"activable": true,
																						"width": 230,
																						"version": 0
																					},
																					"stamp": 2161
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-17": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 1322,
													"*": {
														"Advanced_validation_pane": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": "300",
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": true,
																"hideTitleBar": true,
																"hideTitle": false,
																"backgroundcolor": "color7",
																"labelsAlignment": "right",
																"label": "Advanced_validation_pane",
																"leftImageURL": "",
																"removeMargins": false,
																"elementsAlignment": "left",
																"version": 0,
																"sameHeightAsSiblings": false
															},
															"stamp": 1315,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"LabelForceMessageValidation_MOD__": "ForceMessageValidation_MOD__",
																			"ForceMessageValidation_MOD__": "LabelForceMessageValidation_MOD__",
																			"LabelForceMessageValidation_EMAIL__": "ForceMessageValidation_EMAIL__",
																			"ForceMessageValidation_EMAIL__": "LabelForceMessageValidation_EMAIL__",
																			"LabelForceMessageValidation_NOTIF_EMAIL__": "ForceMessageValidation_NOTIF_EMAIL__",
																			"ForceMessageValidation_NOTIF_EMAIL__": "LabelForceMessageValidation_NOTIF_EMAIL__",
																			"LabelForceMessageValidation_WELCOME_EMAIL__": "ForceMessageValidation_WELCOME_EMAIL__",
																			"ForceMessageValidation_WELCOME_EMAIL__": "LabelForceMessageValidation_WELCOME_EMAIL__",
																			"LabelForceMessageValidation_FAX__": "ForceMessageValidation_FAX__",
																			"ForceMessageValidation_FAX__": "LabelForceMessageValidation_FAX__"
																		},
																		"version": 0
																	},
																	"stamp": 1316,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"LabelForceMessageValidation_MOD__": {
																						"line": 2,
																						"column": 1
																					},
																					"ForceMessageValidation_MOD__": {
																						"line": 2,
																						"column": 2
																					},
																					"LabelForceMessageValidation_EMAIL__": {
																						"line": 1,
																						"column": 1
																					},
																					"ForceMessageValidation_EMAIL__": {
																						"line": 1,
																						"column": 2
																					},
																					"LabelForceMessageValidation_NOTIF_EMAIL__": {
																						"line": 3,
																						"column": 1
																					},
																					"ForceMessageValidation_NOTIF_EMAIL__": {
																						"line": 3,
																						"column": 2
																					},
																					"LabelForceMessageValidation_WELCOME_EMAIL__": {
																						"line": 4,
																						"column": 1
																					},
																					"ForceMessageValidation_WELCOME_EMAIL__": {
																						"line": 4,
																						"column": 2
																					},
																					"LabelForceMessageValidation_FAX__": {
																						"line": 5,
																						"column": 1
																					},
																					"ForceMessageValidation_FAX__": {
																						"line": 5,
																						"column": 2
																					}
																				},
																				"lines": 5,
																				"columns": 2,
																				"colspans": [
																					[],
																					[],
																					[],
																					[],
																					[]
																				],
																				"version": 0
																			},
																			"stamp": 1317,
																			"*": {
																				"ForceMessageValidation_FAX__": {
																					"type": "CheckBox",
																					"data": [
																						"ForceMessageValidation_FAX__"
																					],
																					"options": {
																						"label": "ForceMessageValidation_FAX__",
																						"activable": true,
																						"version": 0
																					},
																					"stamp": 70
																				},
																				"LabelForceMessageValidation_FAX__": {
																					"type": "Label",
																					"data": [
																						"ForceMessageValidation_FAX__"
																					],
																					"options": {
																						"label": "ForceMessageValidation_FAX__",
																						"version": 0
																					},
																					"stamp": 71
																				},
																				"ForceMessageValidation_WELCOME_EMAIL__": {
																					"type": "CheckBox",
																					"data": [
																						"ForceMessageValidation_WELCOME_EMAIL__"
																					],
																					"options": {
																						"label": "ForceMessageValidation_WELCOME_EMAIL__",
																						"activable": true,
																						"version": 0
																					},
																					"stamp": 77
																				},
																				"LabelForceMessageValidation_WELCOME_EMAIL__": {
																					"type": "Label",
																					"data": [
																						"ForceMessageValidation_WELCOME_EMAIL__"
																					],
																					"options": {
																						"label": "ForceMessageValidation_WELCOME_EMAIL__",
																						"version": 0
																					},
																					"stamp": 79
																				},
																				"ForceMessageValidation_NOTIF_EMAIL__": {
																					"type": "CheckBox",
																					"data": [
																						"ForceMessageValidation_NOTIF_EMAIL__"
																					],
																					"options": {
																						"label": "ForceMessageValidation_NOTIF_EMAIL__",
																						"activable": true,
																						"version": 0
																					},
																					"stamp": 82
																				},
																				"LabelForceMessageValidation_NOTIF_EMAIL__": {
																					"type": "Label",
																					"data": [
																						"ForceMessageValidation_NOTIF_EMAIL__"
																					],
																					"options": {
																						"label": "ForceMessageValidation_NOTIF_EMAIL__",
																						"version": 0
																					},
																					"stamp": 83
																				},
																				"LabelForceMessageValidation_EMAIL__": {
																					"type": "Label",
																					"data": [
																						"ForceMessageValidation_EMAIL__"
																					],
																					"options": {
																						"label": "ForceMessageValidation_EMAIL__",
																						"version": 0
																					},
																					"stamp": 76
																				},
																				"ForceMessageValidation_EMAIL__": {
																					"type": "CheckBox",
																					"data": [
																						"ForceMessageValidation_EMAIL__"
																					],
																					"options": {
																						"label": "ForceMessageValidation_EMAIL__",
																						"activable": true,
																						"version": 0,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format"
																					},
																					"stamp": 75
																				},
																				"ForceMessageValidation_MOD__": {
																					"type": "CheckBox",
																					"data": [
																						"ForceMessageValidation_MOD__"
																					],
																					"options": {
																						"label": "ForceMessageValidation_MOD__",
																						"activable": true,
																						"version": 0
																					},
																					"stamp": 80
																				},
																				"LabelForceMessageValidation_MOD__": {
																					"type": "Label",
																					"data": [
																						"ForceMessageValidation_MOD__"
																					],
																					"options": {
																						"label": "ForceMessageValidation_MOD__",
																						"version": 0
																					},
																					"stamp": 78
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-center-26": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 150,
													"*": {
														"Formatting_pane": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": "300",
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": true,
																"hideTitleBar": false,
																"hideTitle": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "Formatting_pane",
																"leftImageURL": "",
																"removeMargins": false,
																"version": 0,
																"sameHeightAsSiblings": false
															},
															"stamp": 151,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"Add_backgrounds__": "LabelAdd_backgrounds__",
																			"LabelAdd_backgrounds__": "Add_backgrounds__",
																			"Use_a_specific_background_for_the_first_page__": "LabelUse_a_specific_background_for_the_first_page__",
																			"LabelUse_a_specific_background_for_the_first_page__": "Use_a_specific_background_for_the_first_page__",
																			"Alternate_background_on_front_and_back_pages__": "LabelAlternate_background_on_front_and_back_pages__",
																			"LabelAlternate_background_on_front_and_back_pages__": "Alternate_background_on_front_and_back_pages__",
																			"BackPage__": "LabelBackPage__",
																			"LabelBackPage__": "BackPage__",
																			"FrontPage__": "LabelFrontPage__",
																			"LabelFrontPage__": "FrontPage__",
																			"Add_T_C__": "LabelAdd_T_C__",
																			"LabelAdd_T_C__": "Add_T_C__",
																			"Terms___conditions_file__": "LabelTerms___conditions_file__",
																			"LabelTerms___conditions_file__": "Terms___conditions_file__",
																			"Terms___conditions_position__": "LabelTerms___conditions_position__",
																			"LabelTerms___conditions_position__": "Terms___conditions_position__",
																			"MoreFormattingOptions__": "LabelMoreFormattingOptions__",
																			"LabelMoreFormattingOptions__": "MoreFormattingOptions__",
																			"FirstPage__": "LabelFirstPage__",
																			"LabelFirstPage__": "FirstPage__",
																			"Document_template__": "LabelDocument_template__",
																			"LabelDocument_template__": "Document_template__"
																		},
																		"version": 0
																	},
																	"stamp": 152,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"Add_backgrounds__": {
																						"line": 3,
																						"column": 2
																					},
																					"LabelAdd_backgrounds__": {
																						"line": 3,
																						"column": 1
																					},
																					"Use_a_specific_background_for_the_first_page__": {
																						"line": 4,
																						"column": 2
																					},
																					"LabelUse_a_specific_background_for_the_first_page__": {
																						"line": 4,
																						"column": 1
																					},
																					"HTML_FirstPage__": {
																						"line": 6,
																						"column": 1
																					},
																					"Alternate_background_on_front_and_back_pages__": {
																						"line": 5,
																						"column": 2
																					},
																					"LabelAlternate_background_on_front_and_back_pages__": {
																						"line": 5,
																						"column": 1
																					},
																					"BackPage__": {
																						"line": 11,
																						"column": 2
																					},
																					"LabelBackPage__": {
																						"line": 11,
																						"column": 1
																					},
																					"FrontPage__": {
																						"line": 9,
																						"column": 2
																					},
																					"LabelFrontPage__": {
																						"line": 9,
																						"column": 1
																					},
																					"HTML_FrontPage__": {
																						"line": 8,
																						"column": 1
																					},
																					"HTML_BackPage__": {
																						"line": 10,
																						"column": 1
																					},
																					"HTML_BackgroundPreview__": {
																						"line": 1,
																						"column": 1
																					},
																					"Add_T_C__": {
																						"line": 12,
																						"column": 2
																					},
																					"LabelAdd_T_C__": {
																						"line": 12,
																						"column": 1
																					},
																					"Terms___conditions_file__": {
																						"line": 15,
																						"column": 2
																					},
																					"LabelTerms___conditions_file__": {
																						"line": 15,
																						"column": 1
																					},
																					"HTML_TC__": {
																						"line": 14,
																						"column": 1
																					},
																					"Terms___conditions_position__": {
																						"line": 13,
																						"column": 2
																					},
																					"LabelTerms___conditions_position__": {
																						"line": 13,
																						"column": 1
																					},
																					"MoreFormattingOptions__": {
																						"line": 16,
																						"column": 2
																					},
																					"LabelMoreFormattingOptions__": {
																						"line": 16,
																						"column": 1
																					},
																					"FirstPage__": {
																						"line": 7,
																						"column": 2
																					},
																					"LabelFirstPage__": {
																						"line": 7,
																						"column": 1
																					},
																					"Document_template__": {
																						"line": 2,
																						"column": 2
																					},
																					"LabelDocument_template__": {
																						"line": 2,
																						"column": 1
																					}
																				},
																				"lines": 16,
																				"columns": 2,
																				"colspans": [
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[],
																					[],
																					[],
																					[],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[],
																					[],
																					[],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[],
																					[]
																				],
																				"version": 0
																			},
																			"stamp": 153,
																			"*": {
																				"HTML_BackgroundPreview__": {
																					"type": "HTML",
																					"data": false,
																					"options": {
																						"label": "HTML_BackgroundPreview__",
																						"htmlContent": "<div class=\"preview\">\n<span class=\"first\"><i class=\"fa fa-file-picture-o\"></i></span>\n<span class=\"front\"><i class=\"fa fa-file-text-o\"></i></span>\n<span class=\"back\"><i class=\"fa fa-file-text-o\"></i></span>\n<span class=\"front\"><i class=\"fa fa-file-text-o\"></i></span>\n<span class=\"back\"><i class=\"fa fa-file-text-o\"></i></span>\n</div>",
																						"css": "@  .preview {\nfont-size: 20pt;\nposition: absolute;\nright: 10px;\ntop: 30px;\nborder-radius: 3px;\n}@ \n.preview span { padding-right: 2px; }@ \n.front { color: #6699FF; }@ \n.back { color: #FFCC99; }@ \n.tc { color: #00CC99; }@ ",
																						"width": 762,
																						"version": 0
																					},
																					"stamp": 154
																				},
																				"LabelDocument_template__": {
																					"type": "Label",
																					"data": [
																						"Document_template__"
																					],
																					"options": {
																						"label": "Document_template__",
																						"version": 0
																					},
																					"stamp": 752
																				},
																				"Document_template__": {
																					"type": "ResourceComboBox",
																					"data": [
																						"Document_template__"
																					],
																					"options": {
																						"version": 1,
																						"storeValueAndType": true,
																						"defaultResourceType": "TYPE_TEMPLATE",
																						"showOnlyDefaultType": true,
																						"allowedExtensions": [
																							"rpt"
																						],
																						"label": "Document_template__",
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"browsable": true
																					},
																					"stamp": 753
																				},
																				"LabelAdd_backgrounds__": {
																					"type": "Label",
																					"data": [
																						"Add_backgrounds__"
																					],
																					"options": {
																						"label": "Add_backgrounds__",
																						"version": 0
																					},
																					"stamp": 155
																				},
																				"Add_backgrounds__": {
																					"type": "CheckBox",
																					"data": [
																						"Add_backgrounds__"
																					],
																					"options": {
																						"label": "Add_backgrounds__",
																						"activable": true,
																						"version": 0
																					},
																					"stamp": 156
																				},
																				"LabelUse_a_specific_background_for_the_first_page__": {
																					"type": "Label",
																					"data": [
																						"Use_a_specific_background_for_the_first_page__"
																					],
																					"options": {
																						"label": "Use_a_specific_background_for_the_first_page__",
																						"version": 0
																					},
																					"stamp": 157
																				},
																				"Use_a_specific_background_for_the_first_page__": {
																					"type": "CheckBox",
																					"data": [
																						"Use_a_specific_background_for_the_first_page__"
																					],
																					"options": {
																						"label": "Use_a_specific_background_for_the_first_page__",
																						"activable": true,
																						"version": 0
																					},
																					"stamp": 158
																				},
																				"LabelAlternate_background_on_front_and_back_pages__": {
																					"type": "Label",
																					"data": [
																						"Alternate_background_on_front_and_back_pages__"
																					],
																					"options": {
																						"label": "Alternate_background_on_front_and_back_pages__",
																						"version": 0
																					},
																					"stamp": 159
																				},
																				"Alternate_background_on_front_and_back_pages__": {
																					"type": "CheckBox",
																					"data": [
																						"Alternate_background_on_front_and_back_pages__"
																					],
																					"options": {
																						"label": "Alternate_background_on_front_and_back_pages__",
																						"activable": true,
																						"version": 0
																					},
																					"stamp": 160
																				},
																				"HTML_FirstPage__": {
																					"type": "HTML",
																					"data": false,
																					"options": {
																						"label": "HTML FirstPage",
																						"version": 0,
																						"htmlContent": "<div class=\"img\">\n<span class=\"first\"><i class=\"fa fa-file-picture-o\"></i></span>\n</div>",
																						"css": "@ .img {\nfont-size: 16pt;\nposition: absolute;\nleft: 278px;\nmargin-top: 0px;\n}@ \n.front { color: #6699FF; }@ \n.back { color: #FFCC99; }@ ",
																						"width": 762
																					},
																					"stamp": 161
																				},
																				"LabelFirstPage__": {
																					"type": "Label",
																					"data": [
																						"FirstPage__"
																					],
																					"options": {
																						"label": " ",
																						"version": 0
																					},
																					"stamp": 162
																				},
																				"FirstPage__": {
																					"type": "ResourceComboBox",
																					"data": [
																						"FirstPage__"
																					],
																					"options": {
																						"storeValueAndType": true,
																						"defaultResourceType": "TYPE_IMAGE",
																						"showOnlyDefaultType": "true",
																						"label": " ",
																						"activable": true,
																						"width": 230,
																						"browsable": true,
																						"allowedExtensions": [
																							"jpg"
																						],
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"version": 0
																					},
																					"stamp": 163
																				},
																				"HTML_FrontPage__": {
																					"type": "HTML",
																					"data": false,
																					"options": {
																						"label": "HTML_FrontPage__",
																						"htmlContent": "<div class=\"img\">\n<span class=\"front\"><i class=\"fa fa-file-text-o\"></i></span>\n</div>",
																						"css": "@ .img {\nfont-size: 16pt;\nposition: absolute;\nleft: 278px;\nmargin-top: 0px;\n}@ \n.front { color: #6699FF; }@ \n.back { color: #FFCC99; }@ ",
																						"width": 745,
																						"version": 0
																					},
																					"stamp": 164
																				},
																				"LabelFrontPage__": {
																					"type": "Label",
																					"data": [
																						"FrontPage__"
																					],
																					"options": {
																						"label": " ",
																						"version": 0
																					},
																					"stamp": 165
																				},
																				"FrontPage__": {
																					"type": "ResourceComboBox",
																					"data": [
																						"FrontPage__"
																					],
																					"options": {
																						"storeValueAndType": true,
																						"defaultResourceType": "TYPE_IMAGE",
																						"showOnlyDefaultType": "true",
																						"label": " ",
																						"activable": true,
																						"width": 230,
																						"browsable": true,
																						"allowedExtensions": [
																							"jpg"
																						],
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"version": 0
																					},
																					"stamp": 166
																				},
																				"HTML_BackPage__": {
																					"type": "HTML",
																					"data": false,
																					"options": {
																						"label": "HTML_BackPage__",
																						"htmlContent": "<div class=\"img\">\n<span class=\"back\"><i class=\"fa fa-file-text-o\"></i></span>\n</div>",
																						"css": "@ .img {\nfont-size: 16pt;\nposition: absolute;\nleft: 278px;\nmargin-top: 0px;\n}@ \n.front { color: #6699FF; }@ \n.back { color: #FFCC99; }@ ",
																						"width": 745,
																						"version": 0
																					},
																					"stamp": 167
																				},
																				"LabelBackPage__": {
																					"type": "Label",
																					"data": [
																						"BackPage__"
																					],
																					"options": {
																						"label": " ",
																						"version": 0
																					},
																					"stamp": 168
																				},
																				"BackPage__": {
																					"type": "ResourceComboBox",
																					"data": [
																						"BackPage__"
																					],
																					"options": {
																						"storeValueAndType": true,
																						"defaultResourceType": "TYPE_IMAGE",
																						"showOnlyDefaultType": "true",
																						"label": " ",
																						"width": 230,
																						"browsable": true,
																						"allowedExtensions": [
																							"jpg"
																						],
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"version": 0
																					},
																					"stamp": 169
																				},
																				"LabelAdd_T_C__": {
																					"type": "Label",
																					"data": [
																						"Add_T_C__"
																					],
																					"options": {
																						"label": "Add_T_C__",
																						"version": 0
																					},
																					"stamp": 170
																				},
																				"Add_T_C__": {
																					"type": "CheckBox",
																					"data": [
																						"Add_T_C__"
																					],
																					"options": {
																						"label": "Add_T_C__",
																						"activable": true,
																						"version": 0
																					},
																					"stamp": 171
																				},
																				"LabelTerms___conditions_position__": {
																					"type": "Label",
																					"data": [
																						"Select_from_a_combo_box2__"
																					],
																					"options": {
																						"label": "Terms___conditions_position__",
																						"version": 0
																					},
																					"stamp": 172
																				},
																				"Terms___conditions_position__": {
																					"type": "ComboBox",
																					"data": [
																						"Terms___conditions_position__"
																					],
																					"options": {
																						"possibleValues": {
																							"0": "_Terms___conditions_position_FIRST",
																							"1": "_Terms___conditions_position_EACH",
																							"2": "_Terms___conditions_position_LAST"
																						},
																						"version": 1,
																						"keyValueMode": true,
																						"possibleKeys": {
																							"0": "FIRST",
																							"1": "EACH",
																							"2": "LAST"
																						},
																						"label": "Terms___conditions_position__",
																						"activable": true,
																						"width": 230,
																						"maxNbLinesToDisplay": 10
																					},
																					"stamp": 173
																				},
																				"HTML_TC__": {
																					"type": "HTML",
																					"data": false,
																					"options": {
																						"label": "HTML_TC__",
																						"htmlContent": "<div class=\"img\">\n<span class=\"tc\"><i class=\"fa fa-file-text-o\"></i></span>\n</div>",
																						"css": "@ .img {\nfont-size: 16pt;\nposition: absolute;\nleft: 278px;\nmargin-top: 0px;\n}@ \n.front { color: #6699FF; }@ \n.back { color: #FFCC99; }@ \n.tc { color: #00CC99; }@ ",
																						"width": 745,
																						"version": 0
																					},
																					"stamp": 174
																				},
																				"LabelTerms___conditions_file__": {
																					"type": "Label",
																					"data": [
																						"Terms___conditions_file__"
																					],
																					"options": {
																						"label": " ",
																						"version": 0
																					},
																					"stamp": 175
																				},
																				"Terms___conditions_file__": {
																					"type": "ResourceComboBox",
																					"data": [
																						"Terms___conditions_file__"
																					],
																					"options": {
																						"storeValueAndType": true,
																						"defaultResourceType": "TYPE_MISC",
																						"showOnlyDefaultType": "false",
																						"label": " ",
																						"width": 230,
																						"browsable": true,
																						"allowedExtensions": [
																							"pdf"
																						],
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"version": 1
																					},
																					"stamp": 176
																				},
																				"LabelMoreFormattingOptions__": {
																					"type": "Label",
																					"data": false,
																					"options": {
																						"label": " ",
																						"version": 0
																					},
																					"stamp": 177
																				},
																				"MoreFormattingOptions__": {
																					"type": "HTML",
																					"data": false,
																					"options": {
																						"label": " ",
																						"htmlContent": "<span class=\"adv\"><i class=\"fa fa-plus-square\"></i> $TITLE$</span>",
																						"css": "@ .adv { cursor: pointer; }@ \n.adv:hover { text-decoration:underline; }@ ",
																						"version": 0
																					},
																					"stamp": 178
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-center-28": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 179,
													"*": {
														"Advanced_formatting_pane": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": "300",
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": true,
																"hideTitleBar": true,
																"hideTitle": false,
																"backgroundcolor": "color7",
																"labelsAlignment": "right",
																"label": "Advanced_formatting_pane",
																"leftImageURL": "",
																"removeMargins": false,
																"version": 0,
																"sameHeightAsSiblings": false
															},
															"stamp": 180,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"Document_Option_PDFCommand__": "LabelDocument_Option_PDFCommand__",
																			"LabelDocument_Option_PDFCommand__": "Document_Option_PDFCommand__"
																		},
																		"version": 0
																	},
																	"stamp": 181,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"DescPDFCommand__2__": {
																						"line": 2,
																						"column": 1
																					},
																					"Spacer_line20__": {
																						"line": 1,
																						"column": 1
																					},
																					"Spacer_line21__": {
																						"line": 4,
																						"column": 1
																					},
																					"Document_Option_PDFCommand__": {
																						"line": 3,
																						"column": 2
																					},
																					"LabelDocument_Option_PDFCommand__": {
																						"line": 3,
																						"column": 1
																					}
																				},
																				"lines": 4,
																				"columns": 2,
																				"colspans": [
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					]
																				],
																				"version": 0
																			},
																			"stamp": 182,
																			"*": {
																				"Spacer_line20__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "",
																						"width": "",
																						"color": "default",
																						"label": "Spacer line20",
																						"version": 0
																					},
																					"stamp": 183
																				},
																				"DescPDFCommand__2__": {
																					"type": "Text",
																					"data": false,
																					"options": {
																						"marge": 0,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"backgroundcolor": "default",
																						"label": "DescPDFCommand__",
																						"version": 0,
																						"image_url": "",
																						"iconClass": ""
																					},
																					"stamp": 184
																				},
																				"LabelDocument_Option_PDFCommand__": {
																					"type": "Label",
																					"data": [
																						"Document_Option_PDFCommand__"
																					],
																					"options": {
																						"label": "Document_Option_PDFCommand__",
																						"version": 0
																					},
																					"stamp": 185
																				},
																				"Document_Option_PDFCommand__": {
																					"type": "LongText",
																					"data": [
																						"Document_Option_PDFCommand__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"label": "Document_Option_PDFCommand__",
																						"activable": true,
																						"width": "100%",
																						"browsable": false,
																						"autocompletable": false,
																						"version": 0,
																						"length": 1024,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"minNbLines": 3
																					},
																					"stamp": 186
																				},
																				"Spacer_line21__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "",
																						"width": "",
																						"color": "default",
																						"label": "Spacer line21",
																						"version": 0
																					},
																					"stamp": 187
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-center-10": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 224,
													"*": {
														"Routing_pane": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": "300",
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": true,
																"hideTitleBar": false,
																"hideTitle": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "Routing_pane",
																"leftImageURL": "",
																"removeMargins": false,
																"version": 0,
																"hidden": true,
																"sameHeightAsSiblings": false
															},
															"stamp": 225,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"AutoCreateRecipients__": "LabelAutoCreateRecipients__",
																			"LabelAutoCreateRecipients__": "AutoCreateRecipients__",
																			"LabelBounceBacks_Enable__": "BounceBacks_Enable__",
																			"BounceBacks_Enable__": "LabelBounceBacks_Enable__",
																			"DefaultDelivery__": "LabelDefaultDelivery__",
																			"LabelDefaultDelivery__": "DefaultDelivery__",
																			"DeliveryMethodToUse__": "LabelDeliveryMethodToUse__",
																			"LabelDeliveryMethodToUse__": "DeliveryMethodToUse__",
																			"ProcessRelatedConfiguration__": "LabelProcessRelatedConfiguration__",
																			"LabelProcessRelatedConfiguration__": "ProcessRelatedConfiguration__",
																			"IsCOPAllowed__": "LabelIsCOPAllowed__",
																			"LabelIsCOPAllowed__": "IsCOPAllowed__",
																			"IsEmailAllowed__": "LabelIsEmailAllowed__",
																			"LabelIsEmailAllowed__": "IsEmailAllowed__",
																			"IsArchiveAllowed__": "LabelIsArchiveAllowed__",
																			"LabelIsArchiveAllowed__": "IsArchiveAllowed__",
																			"IsMODAllowed__": "LabelIsMODAllowed__",
																			"LabelIsMODAllowed__": "IsMODAllowed__",
																			"IsFaxAllowed__": "LabelIsFaxAllowed__",
																			"LabelIsFaxAllowed__": "IsFaxAllowed__",
																			"IsOtherAllowed__": "LabelIsOtherAllowed__",
																			"LabelIsOtherAllowed__": "IsOtherAllowed__",
																			"IsPortalAllowed__": "LabelIsPortalAllowed__",
																			"LabelIsPortalAllowed__": "IsPortalAllowed__",
																			"DefaultDeliveryEmail__": "Label_DefaultDeliveryEmail",
																			"Label_DefaultDeliveryEmail": "DefaultDeliveryEmail__",
																			"IsConversationAllowed__": "LabelIsConversationAllowed__",
																			"LabelIsConversationAllowed__": "IsConversationAllowed__",
																			"IsApplicationProcessAllowed__": "LabelIsApplicationProcessAllowed__",
																			"LabelIsApplicationProcessAllowed__": "IsApplicationProcessAllowed__",
																			"ProcessSelected__": "LabelProcessSelected__",
																			"LabelProcessSelected__": "ProcessSelected__",
																			"Target_configuration__": "LabelTarget_configuration__",
																			"LabelTarget_configuration__": "Target_configuration__"
																		},
																		"version": 0
																	},
																	"stamp": 226,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"AutoCreateRecipients__": {
																						"line": 20,
																						"column": 2
																					},
																					"LabelAutoCreateRecipients__": {
																						"line": 20,
																						"column": 1
																					},
																					"LabelBounceBacks_Enable__": {
																						"line": 19,
																						"column": 1
																					},
																					"BounceBacks_Enable__": {
																						"line": 19,
																						"column": 2
																					},
																					"DefaultDelivery__": {
																						"line": 15,
																						"column": 2
																					},
																					"LabelDefaultDelivery__": {
																						"line": 15,
																						"column": 1
																					},
																					"DeliveryMethodToUse__": {
																						"line": 1,
																						"column": 2
																					},
																					"LabelDeliveryMethodToUse__": {
																						"line": 1,
																						"column": 1
																					},
																					"ProcessRelatedConfiguration__": {
																						"line": 16,
																						"column": 2
																					},
																					"LabelProcessRelatedConfiguration__": {
																						"line": 16,
																						"column": 1
																					},
																					"AllowedRoutingMethodsTitle__": {
																						"line": 2,
																						"column": 1
																					},
																					"Spacer__": {
																						"line": 13,
																						"column": 1
																					},
																					"Spacer2__": {
																						"line": 21,
																						"column": 1
																					},
																					"IsCOPAllowed__": {
																						"line": 9,
																						"column": 2
																					},
																					"LabelIsCOPAllowed__": {
																						"line": 9,
																						"column": 1
																					},
																					"IsEmailAllowed__": {
																						"line": 5,
																						"column": 2
																					},
																					"LabelIsEmailAllowed__": {
																						"line": 5,
																						"column": 1
																					},
																					"IsArchiveAllowed__": {
																						"line": 8,
																						"column": 2
																					},
																					"LabelIsArchiveAllowed__": {
																						"line": 8,
																						"column": 1
																					},
																					"IsMODAllowed__": {
																						"line": 3,
																						"column": 2
																					},
																					"LabelIsMODAllowed__": {
																						"line": 3,
																						"column": 1
																					},
																					"IsFaxAllowed__": {
																						"line": 6,
																						"column": 2
																					},
																					"LabelIsFaxAllowed__": {
																						"line": 6,
																						"column": 1
																					},
																					"IsOtherAllowed__": {
																						"line": 7,
																						"column": 2
																					},
																					"LabelIsOtherAllowed__": {
																						"line": 7,
																						"column": 1
																					},
																					"IsPortalAllowed__": {
																						"line": 4,
																						"column": 2
																					},
																					"LabelIsPortalAllowed__": {
																						"line": 4,
																						"column": 1
																					},
																					"Label_DefaultDeliveryEmail": {
																						"line": 17,
																						"column": 1
																					},
																					"DefaultDeliveryEmail__": {
																						"line": 17,
																						"column": 2
																					},
																					"IsConversationAllowed__": {
																						"line": 10,
																						"column": 2
																					},
																					"LabelIsConversationAllowed__": {
																						"line": 10,
																						"column": 1
																					},
																					"Label_ShortText": {
																						"line": 17,
																						"column": 1
																					},
																					"IsApplicationProcessAllowed__": {
																						"line": 11,
																						"column": 2
																					},
																					"LabelIsApplicationProcessAllowed__": {
																						"line": 11,
																						"column": 1
																					},
																					"ProcessSelected__": {
																						"line": 12,
																						"column": 2
																					},
																					"LabelProcessSelected__": {
																						"line": 12,
																						"column": 1
																					},
																					"DefaultRoutingMethodsTitle__": {
																						"line": 14,
																						"column": 1
																					},
																					"Target_configuration__": {
																						"line": 18,
																						"column": 2
																					},
																					"LabelTarget_configuration__": {
																						"line": 18,
																						"column": 1
																					}
																				},
																				"lines": 21,
																				"columns": 2,
																				"colspans": [
																					[],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					]
																				],
																				"version": 0
																			},
																			"stamp": 227,
																			"*": {
																				"LabelDeliveryMethodToUse__": {
																					"type": "Label",
																					"data": [
																						"DeliveryMethodToUse__"
																					],
																					"options": {
																						"label": "DeliveryMethodToUse__",
																						"version": 0
																					},
																					"stamp": 1736
																				},
																				"DeliveryMethodToUse__": {
																					"type": "RadioButton",
																					"data": [
																						"DeliveryMethodToUse__"
																					],
																					"options": {
																						"keys": [
																							"DeliveryMethodToUse_CUSTOMER",
																							"DeliveryMethodToUse_DEFAULT"
																						],
																						"values": [
																							"DeliveryMethodToUse_CUSTOMER",
																							"DeliveryMethodToUse_DEFAULT"
																						],
																						"label": "DeliveryMethodToUse__",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"version": 0
																					},
																					"stamp": 1737
																				},
																				"AllowedRoutingMethodsTitle__": {
																					"type": "Text",
																					"data": false,
																					"options": {
																						"marge": 0,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "bold",
																						"textColor": "color1",
																						"backgroundcolor": "default",
																						"iconClass": "",
																						"label": "AllowedRoutingMethodsTitle",
																						"version": 0
																					},
																					"stamp": 2002
																				},
																				"LabelIsMODAllowed__": {
																					"type": "Label",
																					"data": [
																						"IsMODAllowed__"
																					],
																					"options": {
																						"label": "Default_MOD",
																						"version": 0
																					},
																					"stamp": 2013
																				},
																				"IsMODAllowed__": {
																					"type": "CheckBox",
																					"data": [
																						"IsMODAllowed__"
																					],
																					"options": {
																						"label": "Default_MOD",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"defaultValue": true,
																						"version": 0
																					},
																					"stamp": 2014
																				},
																				"LabelIsPortalAllowed__": {
																					"type": "Label",
																					"data": [
																						"IsPortalAllowed__"
																					],
																					"options": {
																						"label": "Default_PORTAL",
																						"version": 0
																					},
																					"stamp": 2019
																				},
																				"IsPortalAllowed__": {
																					"type": "CheckBox",
																					"data": [
																						"IsPortalAllowed__"
																					],
																					"options": {
																						"label": "Default_PORTAL",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"defaultValue": true,
																						"version": 0
																					},
																					"stamp": 2020
																				},
																				"LabelIsEmailAllowed__": {
																					"type": "Label",
																					"data": [
																						"IsEmailAllowed__"
																					],
																					"options": {
																						"label": "Default_EMAIL",
																						"version": 0
																					},
																					"stamp": 2009
																				},
																				"IsEmailAllowed__": {
																					"type": "CheckBox",
																					"data": [
																						"IsEmailAllowed__"
																					],
																					"options": {
																						"label": "Default_EMAIL",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"defaultValue": true,
																						"version": 0
																					},
																					"stamp": 2010
																				},
																				"LabelIsFaxAllowed__": {
																					"type": "Label",
																					"data": [
																						"IsFaxAllowed__"
																					],
																					"options": {
																						"label": "Default_FAX",
																						"version": 0
																					},
																					"stamp": 2015
																				},
																				"IsFaxAllowed__": {
																					"type": "CheckBox",
																					"data": [
																						"IsFaxAllowed__"
																					],
																					"options": {
																						"label": "Default_FAX",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"defaultValue": true,
																						"version": 0
																					},
																					"stamp": 2016
																				},
																				"LabelIsOtherAllowed__": {
																					"type": "Label",
																					"data": [
																						"IsOtherAllowed__"
																					],
																					"options": {
																						"label": "Default_OTHER",
																						"version": 0
																					},
																					"stamp": 2017
																				},
																				"IsOtherAllowed__": {
																					"type": "CheckBox",
																					"data": [
																						"IsOtherAllowed__"
																					],
																					"options": {
																						"label": "Default_OTHER",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"defaultValue": true,
																						"version": 0
																					},
																					"stamp": 2018
																				},
																				"LabelIsArchiveAllowed__": {
																					"type": "Label",
																					"data": [
																						"IsArchiveAllowed__"
																					],
																					"options": {
																						"label": "Default_NONE",
																						"version": 0
																					},
																					"stamp": 2011
																				},
																				"IsArchiveAllowed__": {
																					"type": "CheckBox",
																					"data": [
																						"IsArchiveAllowed__"
																					],
																					"options": {
																						"label": "Default_NONE",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"defaultValue": true,
																						"version": 0
																					},
																					"stamp": 2012
																				},
																				"LabelIsCOPAllowed__": {
																					"type": "Label",
																					"data": [
																						"IsCOPAllowed__"
																					],
																					"options": {
																						"label": "CustomerOrderProcessing",
																						"version": 0
																					},
																					"stamp": 2007
																				},
																				"IsCOPAllowed__": {
																					"type": "CheckBox",
																					"data": [
																						"IsCOPAllowed__"
																					],
																					"options": {
																						"label": "CustomerOrderProcessing",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"defaultValue": true,
																						"version": 0
																					},
																					"stamp": 2008
																				},
																				"LabelIsConversationAllowed__": {
																					"type": "Label",
																					"data": [
																						"IsConversationAllowed__"
																					],
																					"options": {
																						"label": "IsConversationAllowed",
																						"version": 0
																					},
																					"stamp": 2147
																				},
																				"IsConversationAllowed__": {
																					"type": "CheckBox",
																					"data": [
																						"IsConversationAllowed__"
																					],
																					"options": {
																						"label": "IsConversationAllowed",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"version": 0
																					},
																					"stamp": 2148
																				},
																				"LabelIsApplicationProcessAllowed__": {
																					"type": "Label",
																					"data": [
																						"IsApplicationProcessAllowed__"
																					],
																					"options": {
																						"label": "Default_APPLICATION_PROCESS",
																						"version": 0
																					},
																					"stamp": 2164
																				},
																				"IsApplicationProcessAllowed__": {
																					"type": "CheckBox",
																					"data": [
																						"IsApplicationProcessAllowed__"
																					],
																					"options": {
																						"label": "Default_APPLICATION_PROCESS",
																						"activable": true,
																						"width": 230,
																						"version": 0
																					},
																					"stamp": 2165
																				},
																				"Spacer__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "",
																						"width": "",
																						"color": "default",
																						"label": "_Spacer",
																						"version": 0
																					},
																					"stamp": 2003
																				},
																				"DefaultRoutingMethodsTitle__": {
																					"type": "Text",
																					"data": false,
																					"options": {
																						"marge": 0,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "bold",
																						"textColor": "color1",
																						"backgroundcolor": "default",
																						"iconClass": "",
																						"label": "_Default routing method title",
																						"version": 0
																					},
																					"stamp": 2170
																				},
																				"LabelDefaultDelivery__": {
																					"type": "Label",
																					"data": [
																						"DefaultDelivery__"
																					],
																					"options": {
																						"label": "DefaultDelivery__",
																						"version": 0
																					},
																					"stamp": 1278
																				},
																				"DefaultDelivery__": {
																					"type": "ComboBox",
																					"data": [
																						"DefaultDelivery__"
																					],
																					"options": {
																						"possibleKeys": {
																							"0": "MOD",
																							"1": "PORTAL",
																							"2": "SM",
																							"3": "FGFAXOUT",
																							"4": "OTHER",
																							"5": "NONE",
																							"6": "APPLICATION_PROCESS"
																						},
																						"version": 1,
																						"keyValueMode": true,
																						"possibleValues": {
																							"0": "Default_MOD",
																							"1": "Default_PORTAL",
																							"2": "Default_EMAIL",
																							"3": "Default_FAX",
																							"4": "Default_OTHER",
																							"5": "Default_NONE",
																							"6": "Default_APPLICATION_PROCESS"
																						},
																						"label": "DefaultDelivery__",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"maxNbLinesToDisplay": 10
																					},
																					"stamp": 1279
																				},
																				"LabelProcessSelected__": {
																					"type": "Label",
																					"data": [
																						"ComboBox__"
																					],
																					"options": {
																						"label": "_Process selected",
																						"hidden": true,
																						"version": 0
																					},
																					"stamp": 2166
																				},
																				"ProcessSelected__": {
																					"type": "ComboBox",
																					"data": [
																						"ProcessSelected__"
																					],
																					"options": {
																						"possibleValues": {},
																						"maxNbLinesToDisplay": 10,
																						"version": 1,
																						"keyValueMode": true,
																						"possibleKeys": {},
																						"label": "_Process selected",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"helpIconPosition": "Right",
																						"DataSourceOrigin": "Standalone",
																						"DisplayedColumns": ""
																					},
																					"stamp": 2167
																				},
																				"LabelProcessRelatedConfiguration__": {
																					"type": "Label",
																					"data": [
																						"Select_from_a_combo_box__"
																					],
																					"options": {
																						"label": "ProcessRelatedConfiguration__",
																						"version": 0
																					},
																					"stamp": 1936
																				},
																				"ProcessRelatedConfiguration__": {
																					"type": "ComboBox",
																					"data": [
																						"ProcessRelatedConfiguration__"
																					],
																					"options": {
																						"possibleValues": {},
																						"version": 1,
																						"keyValueMode": false,
																						"possibleKeys": {},
																						"label": "ProcessRelatedConfiguration__",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"DataSourceOrigin": "Standalone",
																						"DisplayedColumns": "",
																						"maxNbLinesToDisplay": 10
																					},
																					"stamp": 1937
																				},
																				"Label_ShortText": {
																					"type": "Label",
																					"data": [
																						"DefaultDeliveryEmail__"
																					],
																					"options": {
																						"label": "DefaultDeliveryEmail__",
																						"version": 0
																					},
																					"stamp": 2124
																				},
																				"DefaultDeliveryEmail__": {
																					"type": "ShortText",
																					"data": [
																						"DefaultDeliveryEmail__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"label": "DefaultDeliveryEmail__",
																						"activable": true,
																						"width": 230
																					},
																					"stamp": 2125
																				},
																				"LabelTarget_configuration__": {
																					"type": "Label",
																					"data": [
																						"ComboBox__"
																					],
																					"options": {
																						"label": "_Target configuration",
																						"version": 0
																					},
																					"stamp": 2171
																				},
																				"Target_configuration__": {
																					"type": "ComboBox",
																					"data": [
																						"Target_configuration__"
																					],
																					"options": {
																						"possibleValues": {},
																						"maxNbLinesToDisplay": 10,
																						"version": 1,
																						"keyValueMode": false,
																						"possibleKeys": {},
																						"label": "_Target configuration",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"helpIconPosition": "Right",
																						"DataSourceOrigin": "Standalone",
																						"DisplayedColumns": ""
																					},
																					"stamp": 2172
																				},
																				"LabelBounceBacks_Enable__": {
																					"type": "Label",
																					"data": [
																						"BounceBacks_Enable__"
																					],
																					"options": {
																						"label": "BounceBacks_Enable__",
																						"version": 0
																					},
																					"stamp": 52
																				},
																				"BounceBacks_Enable__": {
																					"type": "CheckBox",
																					"data": [
																						"BounceBacks_Enable__"
																					],
																					"options": {
																						"label": "BounceBacks_Enable__",
																						"activable": true,
																						"version": 0
																					},
																					"stamp": 54
																				},
																				"LabelAutoCreateRecipients__": {
																					"type": "Label",
																					"data": [
																						"AutoCreateRecipients__"
																					],
																					"options": {
																						"label": "AutoCreateRecipients__",
																						"version": 0
																					},
																					"stamp": 232
																				},
																				"AutoCreateRecipients__": {
																					"type": "CheckBox",
																					"data": [
																						"AutoCreateRecipients__"
																					],
																					"options": {
																						"label": "AutoCreateRecipients__",
																						"activable": true,
																						"version": 0
																					},
																					"stamp": 233
																				},
																				"Spacer2__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "",
																						"width": "",
																						"color": "default",
																						"label": "_Spacer2",
																						"version": 0
																					},
																					"stamp": 2004
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-center-22": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 234,
													"*": {
														"Routing_advanced_pane": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": "300",
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": false,
																"hideTitleBar": true,
																"hideTitle": false,
																"backgroundcolor": "color7",
																"labelsAlignment": "right",
																"label": "Routing_advanced_pane",
																"leftImageURL": "",
																"removeMargins": false,
																"version": 0,
																"hidden": false,
																"sameHeightAsSiblings": false
															},
															"stamp": 235,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"DefaultDeliveryMethod__": "LabelDefaultDeliveryMethod__",
																			"LabelDefaultDeliveryMethod__": "DefaultDeliveryMethod__",
																			"AllowMethodMOD__": "LabelAllowMethodMOD__",
																			"LabelAllowMethodMOD__": "AllowMethodMOD__",
																			"AllowMethodSM__": "LabelAllowMethodSM__",
																			"LabelAllowMethodSM__": "AllowMethodSM__",
																			"AllowMethodPORTAL__": "LabelAllowMethodPORTAL__",
																			"LabelAllowMethodPORTAL__": "AllowMethodPORTAL__",
																			"AllowMethodFAX__": "LabelAllowMethodFAX__",
																			"LabelAllowMethodFAX__": "AllowMethodFAX__",
																			"NewRecipientAllowNoPwd__": "LabelNewRecipientAllowNoPwd__",
																			"LabelNewRecipientAllowNoPwd__": "NewRecipientAllowNoPwd__",
																			"AdditionalSettings__": "LabelAdditionalSettings__",
																			"LabelAdditionalSettings__": "AdditionalSettings__"
																		},
																		"version": 0
																	},
																	"stamp": 236,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"DefaultDeliveryMethod__": {
																						"line": 1,
																						"column": 2
																					},
																					"LabelDefaultDeliveryMethod__": {
																						"line": 1,
																						"column": 1
																					},
																					"Spacer_line16__": {
																						"line": 2,
																						"column": 1
																					},
																					"DescAllowedDelivery__": {
																						"line": 3,
																						"column": 1
																					},
																					"AllowMethodMOD__": {
																						"line": 4,
																						"column": 2
																					},
																					"LabelAllowMethodMOD__": {
																						"line": 4,
																						"column": 1
																					},
																					"AllowMethodSM__": {
																						"line": 5,
																						"column": 2
																					},
																					"LabelAllowMethodSM__": {
																						"line": 5,
																						"column": 1
																					},
																					"AllowMethodPORTAL__": {
																						"line": 6,
																						"column": 2
																					},
																					"LabelAllowMethodPORTAL__": {
																						"line": 6,
																						"column": 1
																					},
																					"AllowMethodFAX__": {
																						"line": 7,
																						"column": 2
																					},
																					"LabelAllowMethodFAX__": {
																						"line": 7,
																						"column": 1
																					},
																					"Spacer_line22__": {
																						"line": 8,
																						"column": 1
																					},
																					"SecurityOptions__": {
																						"line": 9,
																						"column": 1
																					},
																					"NewRecipientAllowNoPwd__": {
																						"line": 10,
																						"column": 2
																					},
																					"LabelNewRecipientAllowNoPwd__": {
																						"line": 10,
																						"column": 1
																					},
																					"AdditionalSettings__": {
																						"line": 11,
																						"column": 2
																					},
																					"LabelAdditionalSettings__": {
																						"line": 11,
																						"column": 1
																					}
																				},
																				"lines": 11,
																				"columns": 2,
																				"colspans": [
																					[],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[],
																					[],
																					[],
																					[],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[],
																					[]
																				],
																				"version": 0
																			},
																			"stamp": 237,
																			"*": {
																				"LabelDefaultDeliveryMethod__": {
																					"type": "Label",
																					"data": [
																						"DefaultDeliveryMethod__"
																					],
																					"options": {
																						"label": "DefaultDeliveryMethod__",
																						"version": 0
																					},
																					"stamp": 238
																				},
																				"DefaultDeliveryMethod__": {
																					"type": "RadioButton",
																					"data": [
																						"DefaultDeliveryMethod__"
																					],
																					"options": {
																						"keys": [
																							"DefaultDelivery_MOD",
																							"DefaultDelivery_PORTAL",
																							"DefaultDelivery_EMAIL",
																							"DefaultDelivery_FAX"
																						],
																						"values": [
																							"DefaultDelivery_MOD",
																							"DefaultDelivery_PORTAL",
																							"DefaultDelivery_EMAIL",
																							"DefaultDelivery_FAX"
																						],
																						"label": "DefaultDeliveryMethod__",
																						"activable": true,
																						"version": 0
																					},
																					"stamp": 239
																				},
																				"Spacer_line16__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "",
																						"width": "",
																						"color": "default",
																						"label": "Spacer line16",
																						"version": 0
																					},
																					"stamp": 240
																				},
																				"DescAllowedDelivery__": {
																					"type": "Text",
																					"data": false,
																					"options": {
																						"marge": 0,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"backgroundcolor": "default",
																						"label": "DescAllowedDelivery__",
																						"version": 0,
																						"image_url": "",
																						"iconClass": ""
																					},
																					"stamp": 241
																				},
																				"LabelAllowMethodMOD__": {
																					"type": "Label",
																					"data": [
																						"AllowMethodMOD__"
																					],
																					"options": {
																						"label": "AllowMethodMOD__",
																						"version": 0
																					},
																					"stamp": 242
																				},
																				"AllowMethodMOD__": {
																					"type": "CheckBox",
																					"data": [
																						"AllowMethodMOD__"
																					],
																					"options": {
																						"label": "AllowMethodMOD__",
																						"activable": true,
																						"version": 0
																					},
																					"stamp": 243
																				},
																				"LabelAllowMethodSM__": {
																					"type": "Label",
																					"data": [
																						"AllowMethodSM__"
																					],
																					"options": {
																						"label": "AllowMethodSM__",
																						"version": 0
																					},
																					"stamp": 244
																				},
																				"AllowMethodSM__": {
																					"type": "CheckBox",
																					"data": [
																						"AllowMethodSM__"
																					],
																					"options": {
																						"label": "AllowMethodSM__",
																						"activable": true,
																						"version": 0
																					},
																					"stamp": 245
																				},
																				"LabelAllowMethodPORTAL__": {
																					"type": "Label",
																					"data": [
																						"AllowMethodPORTAL__"
																					],
																					"options": {
																						"label": "AllowMethodPORTAL__",
																						"version": 0
																					},
																					"stamp": 246
																				},
																				"AllowMethodPORTAL__": {
																					"type": "CheckBox",
																					"data": [
																						"AllowMethodPORTAL__"
																					],
																					"options": {
																						"label": "AllowMethodPORTAL__",
																						"activable": true,
																						"version": 0
																					},
																					"stamp": 247
																				},
																				"LabelAllowMethodFAX__": {
																					"type": "Label",
																					"data": [
																						"AllowMethodFAX__"
																					],
																					"options": {
																						"label": "AllowMethodFAX__",
																						"version": 0
																					},
																					"stamp": 248
																				},
																				"AllowMethodFAX__": {
																					"type": "CheckBox",
																					"data": [
																						"AllowMethodFAX__"
																					],
																					"options": {
																						"label": "AllowMethodFAX__",
																						"activable": true,
																						"version": 0
																					},
																					"stamp": 249
																				},
																				"Spacer_line22__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "",
																						"width": "",
																						"color": "default",
																						"label": "Spacer line22",
																						"version": 0
																					},
																					"stamp": 250
																				},
																				"SecurityOptions__": {
																					"type": "Text",
																					"data": false,
																					"options": {
																						"marge": 0,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"backgroundcolor": "default",
																						"label": "SecurityOptions__",
																						"version": 0,
																						"image_url": "",
																						"iconClass": ""
																					},
																					"stamp": 251
																				},
																				"LabelNewRecipientAllowNoPwd__": {
																					"type": "Label",
																					"data": [
																						"NewRecipientAllowNoPwd__"
																					],
																					"options": {
																						"label": "NewRecipientAllowNoPwd__",
																						"version": 0
																					},
																					"stamp": 252
																				},
																				"NewRecipientAllowNoPwd__": {
																					"type": "CheckBox",
																					"data": [
																						"NewRecipientAllowNoPwd__"
																					],
																					"options": {
																						"label": "NewRecipientAllowNoPwd__",
																						"activable": true,
																						"width": "",
																						"helpText": "",
																						"version": 0
																					},
																					"stamp": 253
																				},
																				"LabelAdditionalSettings__": {
																					"type": "Label",
																					"data": [
																						"AdditionalSettings__"
																					],
																					"options": {
																						"label": "AdditionalSettings",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 2149
																				},
																				"AdditionalSettings__": {
																					"type": "LongText",
																					"data": [
																						"AdditionalSettings__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"minNbLines": 3,
																						"label": "AdditionalSettings",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"browsable": false,
																						"hidden": true
																					},
																					"stamp": 2150
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-15": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 1746,
													"*": {
														"Related_Document_pane": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": "300",
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": true,
																"hideTitleBar": false,
																"hideTitle": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "Related_Document_pane",
																"leftImageURL": "",
																"removeMargins": false,
																"elementsAlignment": "left",
																"version": 0,
																"sameHeightAsSiblings": false
															},
															"stamp": 1739,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"LinkToAnotherDocument__": "LabelLinkToAnotherDocument__",
																			"LabelLinkToAnotherDocument__": "LinkToAnotherDocument__",
																			"BehaviorOnExpiration__": "LabelBehaviorOnExpiration__",
																			"LabelBehaviorOnExpiration__": "BehaviorOnExpiration__",
																			"ArchivedAsLongAsMasterDocument__": "LabelArchivedAsLongAsMasterDocument__",
																			"LabelArchivedAsLongAsMasterDocument__": "ArchivedAsLongAsMasterDocument__",
																			"MasterDocumentExpirationTime__": "LabelMasterDocumentExpirationTime__",
																			"LabelMasterDocumentExpirationTime__": "MasterDocumentExpirationTime__"
																		},
																		"version": 0
																	},
																	"stamp": 1740,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"LinkToAnotherDocument__": {
																						"line": 1,
																						"column": 2
																					},
																					"LabelLinkToAnotherDocument__": {
																						"line": 1,
																						"column": 1
																					},
																					"BehaviorOnExpiration__": {
																						"line": 3,
																						"column": 2
																					},
																					"LabelBehaviorOnExpiration__": {
																						"line": 3,
																						"column": 1
																					},
																					"ArchivedAsLongAsMasterDocument__": {
																						"line": 4,
																						"column": 2
																					},
																					"LabelArchivedAsLongAsMasterDocument__": {
																						"line": 4,
																						"column": 1
																					},
																					"MasterDocumentExpirationTime__": {
																						"line": 2,
																						"column": 2
																					},
																					"LabelMasterDocumentExpirationTime__": {
																						"line": 2,
																						"column": 1
																					}
																				},
																				"lines": 4,
																				"columns": 2,
																				"colspans": [
																					[],
																					[],
																					[],
																					[]
																				],
																				"version": 0
																			},
																			"stamp": 1741,
																			"*": {
																				"LabelLinkToAnotherDocument__": {
																					"type": "Label",
																					"data": [
																						"LinkToAnotherDocument__"
																					],
																					"options": {
																						"label": "_LinkToAnotherDocument",
																						"version": 0
																					},
																					"stamp": 1752
																				},
																				"LinkToAnotherDocument__": {
																					"type": "CheckBox",
																					"data": [
																						"LinkToAnotherDocument__"
																					],
																					"options": {
																						"label": "_LinkToAnotherDocument",
																						"activable": true,
																						"width": 230,
																						"helpText": "_Help tool tip about related documents",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"version": 0
																					},
																					"stamp": 1753
																				},
																				"LabelMasterDocumentExpirationTime__": {
																					"type": "Label",
																					"data": [
																						"MasterDocumentExpirationTime__"
																					],
																					"options": {
																						"label": "_MasterDocumentExpirationTime",
																						"version": 0
																					},
																					"stamp": 1762
																				},
																				"MasterDocumentExpirationTime__": {
																					"type": "Decimal",
																					"data": [
																						"MasterDocumentExpirationTime__"
																					],
																					"options": {
																						"version": 2,
																						"textSize": "S",
																						"textAlignment": "right",
																						"textStyle": "default",
																						"textColor": "default",
																						"integer": false,
																						"label": "_MasterDocumentExpirationTime",
																						"precision_internal": 0,
																						"precision_current": 0,
																						"activable": true,
																						"width": "80",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"autocompletable": false,
																						"defaultValue": "72"
																					},
																					"stamp": 1763
																				},
																				"LabelBehaviorOnExpiration__": {
																					"type": "Label",
																					"data": [
																						"BehaviorOnExpiration__"
																					],
																					"options": {
																						"label": "_BehaviorOnExpiration",
																						"version": 0
																					},
																					"stamp": 1754
																				},
																				"BehaviorOnExpiration__": {
																					"type": "RadioButton",
																					"data": [
																						"BehaviorOnExpiration__"
																					],
																					"options": {
																						"keys": [
																							"ERROR",
																							"SEND"
																						],
																						"values": [
																							"_BehaviorOnExpiration_SetOnError",
																							"_BehaviorOnExpiration_Send"
																						],
																						"label": "_BehaviorOnExpiration",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"version": 0
																					},
																					"stamp": 1755
																				},
																				"LabelArchivedAsLongAsMasterDocument__": {
																					"type": "Label",
																					"data": [
																						"ArchivedAsLongAsMasterDocument__"
																					],
																					"options": {
																						"label": "_ArchivedAsLongAsMasterDocument",
																						"version": 0
																					},
																					"stamp": 1760
																				},
																				"ArchivedAsLongAsMasterDocument__": {
																					"type": "CheckBox",
																					"data": [
																						"ArchivedAsLongAsMasterDocument__"
																					],
																					"options": {
																						"label": "_ArchivedAsLongAsMasterDocument",
																						"activable": true,
																						"width": 230,
																						"version": 0
																					},
																					"stamp": 1761
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-center-12": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 254,
													"*": {
														"MOD_delivery_pane": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": "300",
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": true,
																"hideTitleBar": false,
																"hideTitle": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "MOD_delivery_pane",
																"leftImageURL": "",
																"removeMargins": false,
																"version": 0,
																"hidden": false,
																"sameHeightAsSiblings": false
															},
															"stamp": 255,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"MOD_Option_Color__": "LabelMOD_Option_Color__",
																			"LabelMOD_Option_Color__": "MOD_Option_Color__",
																			"MOD_Option_Bothsided__": "LabelMOD_Option_Bothsided__",
																			"LabelMOD_Option_Bothsided__": "MOD_Option_Bothsided__",
																			"MOD_Option_Cover__": "LabelMOD_Option_Cover__",
																			"LabelMOD_Option_Cover__": "MOD_Option_Cover__",
																			"MoreMODOptions__": "LabelMoreMODOptions__",
																			"LabelMoreMODOptions__": "MoreMODOptions__",
																			"MOD_Option_Grouping__": "LabelMOD_Option_Grouping__",
																			"LabelMOD_Option_Grouping__": "MOD_Option_Grouping__",
																			"DeferredTimeAfterValidation__": "LabelDeferredTimeAfterValidation__",
																			"LabelDeferredTimeAfterValidation__": "DeferredTimeAfterValidation__",
																			"MOD_Option_CoverOnC4__": "LabelMOD_Option_CoverOnC4__",
																			"LabelMOD_Option_CoverOnC4__": "MOD_Option_CoverOnC4__",
																			"MOD_Option_FirstPageOnOddPage__": "LabelMOD_Option_FirstPageOnOddPage__",
																			"LabelMOD_Option_FirstPageOnOddPage__": "MOD_Option_FirstPageOnOddPage__",
																			"MOD_Option_MailService__": "LabelMOD_Option_MailService__",
																			"LabelMOD_Option_MailService__": "MOD_Option_MailService__"
																		},
																		"version": 0
																	},
																	"stamp": 256,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"MOD_Option_Color__": {
																						"line": 3,
																						"column": 2
																					},
																					"LabelMOD_Option_Color__": {
																						"line": 3,
																						"column": 1
																					},
																					"MOD_Option_Bothsided__": {
																						"line": 2,
																						"column": 2
																					},
																					"LabelMOD_Option_Bothsided__": {
																						"line": 2,
																						"column": 1
																					},
																					"MOD_Option_Cover__": {
																						"line": 1,
																						"column": 2
																					},
																					"LabelMOD_Option_Cover__": {
																						"line": 1,
																						"column": 1
																					},
																					"MoreMODOptions__": {
																						"line": 10,
																						"column": 2
																					},
																					"LabelMoreMODOptions__": {
																						"line": 10,
																						"column": 1
																					},
																					"DescMODGrouping__": {
																						"line": 6,
																						"column": 1
																					},
																					"MOD_Option_Grouping__": {
																						"line": 4,
																						"column": 2
																					},
																					"LabelMOD_Option_Grouping__": {
																						"line": 4,
																						"column": 1
																					},
																					"DeferredTimeAfterValidation__": {
																						"line": 5,
																						"column": 2
																					},
																					"LabelDeferredTimeAfterValidation__": {
																						"line": 5,
																						"column": 1
																					},
																					"MOD_Option_CoverOnC4__": {
																						"line": 7,
																						"column": 2
																					},
																					"LabelMOD_Option_CoverOnC4__": {
																						"line": 7,
																						"column": 1
																					},
																					"MOD_Option_FirstPageOnOddPage__": {
																						"line": 8,
																						"column": 2
																					},
																					"LabelMOD_Option_FirstPageOnOddPage__": {
																						"line": 8,
																						"column": 1
																					},
																					"MOD_Option_MailService__": {
																						"line": 9,
																						"column": 2
																					},
																					"LabelMOD_Option_MailService__": {
																						"line": 9,
																						"column": 1
																					}
																				},
																				"lines": 10,
																				"columns": 2,
																				"colspans": [
																					[],
																					[],
																					[],
																					[],
																					[],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[],
																					[],
																					[],
																					[]
																				],
																				"version": 0
																			},
																			"stamp": 257,
																			"*": {
																				"LabelMOD_Option_Cover__": {
																					"type": "Label",
																					"data": [
																						"MOD_Option_Cover__"
																					],
																					"options": {
																						"label": "MOD_Option_Cover__",
																						"version": 0
																					},
																					"stamp": 258
																				},
																				"MOD_Option_Cover__": {
																					"type": "CheckBox",
																					"data": [
																						"MOD_Option_Cover__"
																					],
																					"options": {
																						"label": "MOD_Option_Cover__",
																						"activable": true,
																						"version": 0
																					},
																					"stamp": 259
																				},
																				"LabelMOD_Option_Bothsided__": {
																					"type": "Label",
																					"data": [
																						"MOD_Option_Bothsided__"
																					],
																					"options": {
																						"label": "MOD_Option_Bothsided__",
																						"version": 0
																					},
																					"stamp": 260
																				},
																				"MOD_Option_Bothsided__": {
																					"type": "CheckBox",
																					"data": [
																						"MOD_Option_Bothsided__"
																					],
																					"options": {
																						"label": "MOD_Option_Bothsided__",
																						"activable": true,
																						"version": 0
																					},
																					"stamp": 261
																				},
																				"LabelMOD_Option_Color__": {
																					"type": "Label",
																					"data": [
																						"MOD_Option_Color__"
																					],
																					"options": {
																						"label": "MOD_Option_Color__",
																						"version": 0
																					},
																					"stamp": 262
																				},
																				"LabelMOD_Option_Grouping__": {
																					"type": "Label",
																					"data": [
																						"MOD_Option_Grouping__"
																					],
																					"options": {
																						"label": "MOD_Option_Grouping__",
																						"version": 0
																					},
																					"stamp": 263
																				},
																				"MOD_Option_Color__": {
																					"type": "CheckBox",
																					"data": [
																						"MOD_Option_Color__"
																					],
																					"options": {
																						"label": "MOD_Option_Color__",
																						"activable": true,
																						"defaultValue": true,
																						"version": 0
																					},
																					"stamp": 264
																				},
																				"MOD_Option_Grouping__": {
																					"type": "CheckBox",
																					"data": [
																						"MOD_Option_Grouping__"
																					],
																					"options": {
																						"label": "MOD_Option_Grouping__",
																						"activable": true,
																						"version": 0
																					},
																					"stamp": 265
																				},
																				"LabelDeferredTimeAfterValidation__": {
																					"type": "Label",
																					"data": [
																						"DeferredTimeAfterValidation__"
																					],
																					"options": {
																						"label": "DeferredTimeAfterValidation__",
																						"version": 0
																					},
																					"stamp": 266
																				},
																				"DeferredTimeAfterValidation__": {
																					"type": "CheckBox",
																					"data": [
																						"DeferredTimeAfterValidation__"
																					],
																					"options": {
																						"label": "DeferredTimeAfterValidation__",
																						"activable": true,
																						"defaultValue": true,
																						"version": 0
																					},
																					"stamp": 267
																				},
																				"DescMODGrouping__": {
																					"type": "Text",
																					"data": false,
																					"options": {
																						"marge": 310,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "italic",
																						"textColor": "default",
																						"backgroundcolor": "default",
																						"label": "DescMODGrouping__",
																						"version": 0,
																						"image_url": "",
																						"iconClass": ""
																					},
																					"stamp": 268
																				},
																				"LabelMOD_Option_CoverOnC4__": {
																					"type": "Label",
																					"data": [
																						"MOD_Option_CoverOnC4__"
																					],
																					"options": {
																						"label": "MOD_Option_CoverOnC4__",
																						"version": 0
																					},
																					"stamp": 269
																				},
																				"MOD_Option_CoverOnC4__": {
																					"type": "CheckBox",
																					"data": [
																						"MOD_Option_CoverOnC4__"
																					],
																					"options": {
																						"label": "MOD_Option_CoverOnC4__",
																						"activable": true,
																						"version": 0
																					},
																					"stamp": 270
																				},
																				"LabelMOD_Option_FirstPageOnOddPage__": {
																					"type": "Label",
																					"data": [
																						"MOD_Option_FirstPageOnOddPage__"
																					],
																					"options": {
																						"label": "MOD_Option_FirstPageOnOddPage__",
																						"version": 0
																					},
																					"stamp": 271
																				},
																				"MOD_Option_FirstPageOnOddPage__": {
																					"type": "CheckBox",
																					"data": [
																						"MOD_Option_FirstPageOnOddPage__"
																					],
																					"options": {
																						"label": "MOD_Option_FirstPageOnOddPage__",
																						"activable": true,
																						"version": 0
																					},
																					"stamp": 272
																				},
																				"LabelMOD_Option_MailService__": {
																					"type": "Label",
																					"data": [
																						"MOD_Option_MailService__"
																					],
																					"options": {
																						"label": "MOD_Option_MailService__",
																						"version": 0
																					},
																					"stamp": 1942
																				},
																				"MOD_Option_MailService__": {
																					"type": "ComboBox",
																					"data": [
																						"MOD_Option_MailService__"
																					],
																					"options": {
																						"possibleValues": {
																							"0": "_MOD_Option_StampType_Standard",
																							"1": "_MOD_Option_StampType_EconomyIfApplicable"
																						},
																						"version": 1,
																						"keyValueMode": true,
																						"possibleKeys": {
																							"0": "STANDARD",
																							"1": "ECONOMY_IF_APPLICABLE"
																						},
																						"label": "MOD_Option_MailService__",
																						"activable": true,
																						"width": 230,
																						"helpText": "_MOD_Option_MailService_tooltip",
																						"helpURL": "1750",
																						"helpFormat": "HTML Format",
																						"DataSourceOrigin": "Standalone",
																						"DisplayedColumns": "",
																						"maxNbLinesToDisplay": 10
																					},
																					"stamp": 1943
																				},
																				"LabelMoreMODOptions__": {
																					"type": "Label",
																					"data": false,
																					"options": {
																						"label": " ",
																						"version": 0
																					},
																					"stamp": 273
																				},
																				"MoreMODOptions__": {
																					"type": "HTML",
																					"data": false,
																					"options": {
																						"label": " ",
																						"htmlContent": "<span class=\"adv\"><i class=\"fa fa-plus-square\"></i> $TITLE$</span>",
																						"css": "@ .adv {\ncursor: pointer;\n}@ \n.adv:hover {\ntext-decoration:underline;\n}@ ",
																						"version": 0
																					},
																					"stamp": 274
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-center-17": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 275,
													"*": {
														"Advanced_mod_options": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {
																	"collapsed": false
																},
																"labelLength": "300",
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": false,
																"hideTitleBar": true,
																"hideTitle": false,
																"backgroundcolor": "color7",
																"labelsAlignment": "right",
																"label": "Advanced_mod_options",
																"leftImageURL": "",
																"removeMargins": false,
																"version": 0,
																"hidden": true,
																"sameHeightAsSiblings": false
															},
															"stamp": 276,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"LabelMOD_Option_PDFCommand__": "MOD_Option_PDFCommand__",
																			"MOD_Option_PDFCommand__": "LabelMOD_Option_PDFCommand__",
																			"LabelMOD_Option_Resize__": "MOD_Option_Resize__",
																			"MOD_Option_Resize__": "LabelMOD_Option_Resize__",
																			"MOD_Option_Scale__": "LabelMOD_Option_Scale__",
																			"LabelMOD_Option_Scale__": "MOD_Option_Scale__",
																			"MOD_Option_OffsetX__": "LabelMOD_Option_OffsetX__",
																			"LabelMOD_Option_OffsetX__": "MOD_Option_OffsetX__",
																			"MOD_Option_OffsetY__": "LabelMOD_Option_OffsetY__",
																			"LabelMOD_Option_OffsetY__": "MOD_Option_OffsetY__"
																		},
																		"version": 0
																	},
																	"stamp": 277,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"LabelMOD_Option_PDFCommand__": {
																						"line": 9,
																						"column": 1
																					},
																					"MOD_Option_PDFCommand__": {
																						"line": 9,
																						"column": 2
																					},
																					"LabelMOD_Option_Resize__": {
																						"line": 3,
																						"column": 1
																					},
																					"MOD_Option_Resize__": {
																						"line": 3,
																						"column": 2
																					},
																					"Spacer_line11__": {
																						"line": 1,
																						"column": 1
																					},
																					"Spacer_line12__": {
																						"line": 10,
																						"column": 1
																					},
																					"MOD_Option_Scale__": {
																						"line": 4,
																						"column": 2
																					},
																					"LabelMOD_Option_Scale__": {
																						"line": 4,
																						"column": 1
																					},
																					"MOD_Option_OffsetX__": {
																						"line": 5,
																						"column": 2
																					},
																					"LabelMOD_Option_OffsetX__": {
																						"line": 5,
																						"column": 1
																					},
																					"MOD_Option_OffsetY__": {
																						"line": 6,
																						"column": 2
																					},
																					"LabelMOD_Option_OffsetY__": {
																						"line": 6,
																						"column": 1
																					},
																					"DescPDFCommand__": {
																						"line": 8,
																						"column": 1
																					},
																					"Spacer_line13__": {
																						"line": 7,
																						"column": 1
																					},
																					"DescResize__": {
																						"line": 2,
																						"column": 1
																					}
																				},
																				"lines": 10,
																				"columns": 2,
																				"colspans": [
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[],
																					[],
																					[],
																					[],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					]
																				],
																				"version": 0
																			},
																			"stamp": 278,
																			"*": {
																				"Spacer_line11__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "",
																						"width": "",
																						"color": "default",
																						"label": "Spacer line11",
																						"version": 0
																					},
																					"stamp": 279
																				},
																				"DescResize__": {
																					"type": "Text",
																					"data": false,
																					"options": {
																						"marge": 0,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"backgroundcolor": "default",
																						"label": "DescResize__",
																						"version": 0,
																						"image_url": "",
																						"iconClass": ""
																					},
																					"stamp": 280
																				},
																				"MOD_Option_Resize__": {
																					"type": "CheckBox",
																					"data": [
																						"MOD_Option_Resize__"
																					],
																					"options": {
																						"label": "MOD_Option_Resize__",
																						"activable": true,
																						"version": 0
																					},
																					"stamp": 281
																				},
																				"LabelMOD_Option_Resize__": {
																					"type": "Label",
																					"data": [
																						"MOD_Option_Resize__"
																					],
																					"options": {
																						"label": "MOD_Option_Resize__",
																						"version": 0
																					},
																					"stamp": 282
																				},
																				"LabelMOD_Option_Scale__": {
																					"type": "Label",
																					"data": [
																						"MOD_Option_Scale__"
																					],
																					"options": {
																						"label": "MOD_Option_Scale__",
																						"version": 0
																					},
																					"stamp": 283
																				},
																				"MOD_Option_Scale__": {
																					"type": "Integer",
																					"data": [
																						"MOD_Option_Scale__"
																					],
																					"options": {
																						"version": 1,
																						"textSize": "S",
																						"textAlignment": "right",
																						"textStyle": "default",
																						"textColor": "default",
																						"integer": true,
																						"label": "MOD_Option_Scale__",
																						"precision_internal": 0,
																						"precision_current": 0,
																						"activable": true,
																						"width": "50",
																						"browsable": false,
																						"autocompletable": false
																					},
																					"stamp": 284
																				},
																				"LabelMOD_Option_OffsetX__": {
																					"type": "Label",
																					"data": [
																						"MOD_Option_OffsetX__"
																					],
																					"options": {
																						"label": "MOD_Option_OffsetX__",
																						"version": 0
																					},
																					"stamp": 285
																				},
																				"MOD_Option_OffsetX__": {
																					"type": "Integer",
																					"data": [
																						"MOD_Option_OffsetX__"
																					],
																					"options": {
																						"version": 1,
																						"textSize": "S",
																						"textAlignment": "right",
																						"textStyle": "default",
																						"textColor": "default",
																						"integer": true,
																						"label": "MOD_Option_OffsetX__",
																						"precision_internal": 0,
																						"precision_current": 0,
																						"activable": true,
																						"width": "50",
																						"browsable": false,
																						"autocompletable": false
																					},
																					"stamp": 286
																				},
																				"LabelMOD_Option_OffsetY__": {
																					"type": "Label",
																					"data": [
																						"MOD_Option_OffsetY__"
																					],
																					"options": {
																						"label": "MOD_Option_OffsetY__",
																						"version": 0
																					},
																					"stamp": 287
																				},
																				"MOD_Option_OffsetY__": {
																					"type": "Integer",
																					"data": [
																						"MOD_Option_OffsetY__"
																					],
																					"options": {
																						"version": 1,
																						"textSize": "S",
																						"textAlignment": "right",
																						"textStyle": "default",
																						"textColor": "default",
																						"integer": true,
																						"label": "MOD_Option_OffsetY__",
																						"precision_internal": 0,
																						"precision_current": 0,
																						"activable": true,
																						"width": "50",
																						"browsable": false,
																						"autocompletable": false
																					},
																					"stamp": 288
																				},
																				"Spacer_line13__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "",
																						"width": "",
																						"color": "default",
																						"label": "Spacer line13",
																						"version": 0
																					},
																					"stamp": 289
																				},
																				"DescPDFCommand__": {
																					"type": "Text",
																					"data": false,
																					"options": {
																						"marge": 0,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"backgroundcolor": "default",
																						"label": "DescPDFCommand__",
																						"version": 0,
																						"image_url": "",
																						"iconClass": ""
																					},
																					"stamp": 290
																				},
																				"LabelMOD_Option_PDFCommand__": {
																					"type": "Label",
																					"data": [
																						"MOD_Option_PDFCommand__"
																					],
																					"options": {
																						"label": "MOD_Option_PDFCommand__",
																						"version": 0
																					},
																					"stamp": 291
																				},
																				"MOD_Option_PDFCommand__": {
																					"type": "LongText",
																					"data": [
																						"MOD_Option_PDFCommand__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"label": "MOD_Option_PDFCommand__",
																						"activable": true,
																						"width": "100%",
																						"length": 1024,
																						"browsable": false,
																						"autocompletable": false,
																						"version": 0,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"minNbLines": 3
																					},
																					"stamp": 292
																				},
																				"Spacer_line12__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "",
																						"width": "",
																						"color": "default",
																						"label": "Spacer line12",
																						"version": 0
																					},
																					"stamp": 293
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-center-15": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 294,
													"*": {
														"Portal_delivery_pane": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": "300",
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": true,
																"hideTitleBar": false,
																"hideTitle": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "Portal_delivery_pane",
																"leftImageURL": "",
																"removeMargins": false,
																"version": 0,
																"hidden": false,
																"sameHeightAsSiblings": false
															},
															"stamp": 295,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"LabelWhenSendWelcomeEmail__": "WhenSendWelcomeEmail__",
																			"WhenSendWelcomeEmail__": "LabelWhenSendWelcomeEmail__",
																			"LabelConcatWelcomeEmail__": "ConcatWelcomeEmail__",
																			"ConcatWelcomeEmail__": "LabelConcatWelcomeEmail__",
																			"LabelMorePortalOptions__": "MorePortalOptions__",
																			"MorePortalOptions__": "LabelMorePortalOptions__",
																			"GroupEmailNotifications__": "LabelGroupEmailNotifications__",
																			"LabelGroupEmailNotifications__": "GroupEmailNotifications__"
																		},
																		"version": 0
																	},
																	"stamp": 296,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"Spacer_line4__": {
																						"line": 1,
																						"column": 1
																					},
																					"DescWelcomeEmail__": {
																						"line": 2,
																						"column": 1
																					},
																					"LabelWhenSendWelcomeEmail__": {
																						"line": 3,
																						"column": 1
																					},
																					"WhenSendWelcomeEmail__": {
																						"line": 3,
																						"column": 2
																					},
																					"LabelConcatWelcomeEmail__": {
																						"line": 4,
																						"column": 1
																					},
																					"ConcatWelcomeEmail__": {
																						"line": 4,
																						"column": 2
																					},
																					"LabelMorePortalOptions__": {
																						"line": 6,
																						"column": 1
																					},
																					"MorePortalOptions__": {
																						"line": 6,
																						"column": 2
																					},
																					"GroupEmailNotifications__": {
																						"line": 5,
																						"column": 2
																					},
																					"LabelGroupEmailNotifications__": {
																						"line": 5,
																						"column": 1
																					}
																				},
																				"lines": 6,
																				"columns": 2,
																				"colspans": [
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[],
																					[],
																					[],
																					[]
																				],
																				"version": 0
																			},
																			"stamp": 297,
																			"*": {
																				"MorePortalOptions__": {
																					"type": "HTML",
																					"data": false,
																					"options": {
																						"label": "",
																						"htmlContent": "<span class=\"adv\"><i class=\"fa fa-plus-square\"></i> $TITLE$</span>",
																						"version": 0,
																						"css": "@ .adv {\ncursor: pointer;\n}@ \n.adv:hover {\ntext-decoration:underline;\n}@ "
																					},
																					"stamp": 298
																				},
																				"LabelMorePortalOptions__": {
																					"type": "Label",
																					"data": false,
																					"options": {
																						"label": "",
																						"version": 0
																					},
																					"stamp": 299
																				},
																				"ConcatWelcomeEmail__": {
																					"type": "CheckBox",
																					"data": [
																						"ConcatWelcomeEmail__"
																					],
																					"options": {
																						"label": "ConcatWelcomeEmail__",
																						"activable": true,
																						"version": 0
																					},
																					"stamp": 300
																				},
																				"LabelConcatWelcomeEmail__": {
																					"type": "Label",
																					"data": [
																						"ConcatWelcomeEmail__"
																					],
																					"options": {
																						"label": "ConcatWelcomeEmail__",
																						"version": 0
																					},
																					"stamp": 301
																				},
																				"WhenSendWelcomeEmail__": {
																					"type": "ComboBox",
																					"data": [
																						"WhenSendWelcomeEmail__"
																					],
																					"options": {
																						"possibleValues": {
																							"0": "_NEVER",
																							"1": "_ONNEVERLOGGED",
																							"2": "_ONFIRSTDOCUMENT"
																						},
																						"version": 1,
																						"keyValueMode": true,
																						"possibleKeys": {
																							"0": "NEVER",
																							"1": "ONNEVERLOGGED",
																							"2": "ONFIRSTDOCUMENT"
																						},
																						"label": "WhenSendWelcomeEmail__",
																						"activable": true,
																						"width": 230,
																						"maxNbLinesToDisplay": 10
																					},
																					"stamp": 302
																				},
																				"LabelWhenSendWelcomeEmail__": {
																					"type": "Label",
																					"data": [
																						"WhenSendWelcomeEmail__"
																					],
																					"options": {
																						"label": "WhenSendWelcomeEmail__",
																						"version": 0
																					},
																					"stamp": 303
																				},
																				"LabelGroupEmailNotifications__": {
																					"type": "Label",
																					"data": [
																						"GroupEmailNotifications__"
																					],
																					"options": {
																						"label": "GroupEmailNotifications__",
																						"version": 0
																					},
																					"stamp": 1185
																				},
																				"GroupEmailNotifications__": {
																					"type": "CheckBox",
																					"data": [
																						"GroupEmailNotifications__"
																					],
																					"options": {
																						"label": "GroupEmailNotifications__",
																						"activable": true,
																						"width": 230,
																						"version": 0
																					},
																					"stamp": 1186
																				},
																				"DescWelcomeEmail__": {
																					"type": "Text",
																					"data": false,
																					"options": {
																						"marge": 0,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"backgroundcolor": "default",
																						"label": "DescWelcomeEmail__",
																						"version": 0,
																						"image_url": "",
																						"iconClass": ""
																					},
																					"stamp": 306
																				},
																				"Spacer_line4__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "",
																						"width": "",
																						"color": "default",
																						"label": "Spacer line4",
																						"version": 0
																					},
																					"stamp": 307
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-center-18": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 308,
													"*": {
														"Advanced_portal_options": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": "300",
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": false,
																"hideTitleBar": true,
																"hideTitle": false,
																"backgroundcolor": "color7",
																"labelsAlignment": "right",
																"label": "Advanced_portal_options",
																"leftImageURL": "",
																"removeMargins": false,
																"version": 0,
																"hidden": true,
																"sameHeightAsSiblings": false
															},
															"stamp": 309,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"LabelAliasPortalUrl__": "AliasPortalUrl__",
																			"AliasPortalUrl__": "LabelAliasPortalUrl__",
																			"LabelAlwaysSendNotification__": "AlwaysSendNotification__",
																			"AlwaysSendNotification__": "LabelAlwaysSendNotification__",
																			"LabelWhenSendWelcomeLetter__": "WhenSendWelcomeLetter__",
																			"WhenSendWelcomeLetter__": "LabelWhenSendWelcomeLetter__"
																		},
																		"version": 0
																	},
																	"stamp": 310,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"Spacer_line6__": {
																						"line": 1,
																						"column": 1
																					},
																					"DescAliasURL__": {
																						"line": 5,
																						"column": 1
																					},
																					"LabelAliasPortalUrl__": {
																						"line": 6,
																						"column": 1
																					},
																					"AliasPortalUrl__": {
																						"line": 6,
																						"column": 2
																					},
																					"Spacer_line7__": {
																						"line": 7,
																						"column": 1
																					},
																					"DescAlwaysSendNotification__": {
																						"line": 8,
																						"column": 1
																					},
																					"LabelAlwaysSendNotification__": {
																						"line": 9,
																						"column": 1
																					},
																					"AlwaysSendNotification__": {
																						"line": 9,
																						"column": 2
																					},
																					"Spacer_line8__": {
																						"line": 4,
																						"column": 1
																					},
																					"LabelWhenSendWelcomeLetter__": {
																						"line": 3,
																						"column": 1
																					},
																					"WhenSendWelcomeLetter__": {
																						"line": 3,
																						"column": 2
																					},
																					"DescWelcomeLetter__": {
																						"line": 2,
																						"column": 1
																					}
																				},
																				"lines": 9,
																				"columns": 2,
																				"colspans": [
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[]
																				],
																				"version": 0
																			},
																			"stamp": 311,
																			"*": {
																				"DescWelcomeLetter__": {
																					"type": "Text",
																					"data": false,
																					"options": {
																						"marge": 0,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"backgroundcolor": "default",
																						"label": "DescWelcomeLetter__",
																						"version": 0,
																						"image_url": "",
																						"iconClass": ""
																					},
																					"stamp": 313
																				},
																				"LabelWhenSendWelcomeLetter__": {
																					"type": "Label",
																					"data": [
																						"WhenSendWelcomeLetter__"
																					],
																					"options": {
																						"label": "WhenSendWelcomeLetter__",
																						"version": 0
																					},
																					"stamp": 314
																				},
																				"AlwaysSendNotification__": {
																					"type": "CheckBox",
																					"data": [
																						"AlwaysSendNotification__"
																					],
																					"options": {
																						"label": "AlwaysSendNotification__",
																						"activable": true,
																						"version": 0
																					},
																					"stamp": 315
																				},
																				"Spacer_line8__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "",
																						"width": "",
																						"color": "default",
																						"label": "Spacer line8",
																						"version": 0
																					},
																					"stamp": 316
																				},
																				"WhenSendWelcomeLetter__": {
																					"type": "ComboBox",
																					"data": [
																						"WhenSendWelcomeLetter__"
																					],
																					"options": {
																						"possibleValues": {
																							"0": "_NEVER",
																							"1": "_ONNEVERLOGGED",
																							"2": "_ONFIRSTDOCUMENT"
																						},
																						"version": 1,
																						"keyValueMode": true,
																						"possibleKeys": {
																							"0": "NEVER",
																							"1": "ONNEVERLOGGED",
																							"2": "ONFIRSTDOCUMENT"
																						},
																						"label": "WhenSendWelcomeLetter__",
																						"activable": true,
																						"width": 230,
																						"maxNbLinesToDisplay": 10
																					},
																					"stamp": 317
																				},
																				"LabelAlwaysSendNotification__": {
																					"type": "Label",
																					"data": [
																						"AlwaysSendNotification__"
																					],
																					"options": {
																						"label": "AlwaysSendNotification__",
																						"version": 0
																					},
																					"stamp": 318
																				},
																				"DescAlwaysSendNotification__": {
																					"type": "Text",
																					"data": false,
																					"options": {
																						"marge": 0,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"backgroundcolor": "default",
																						"label": "DescAlwaysSendNotification__",
																						"version": 0,
																						"image_url": "",
																						"iconClass": ""
																					},
																					"stamp": 319
																				},
																				"Spacer_line7__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "",
																						"width": "",
																						"color": "default",
																						"label": "Spacer line7",
																						"version": 0
																					},
																					"stamp": 320
																				},
																				"AliasPortalUrl__": {
																					"type": "ShortText",
																					"data": [
																						"AliasPortalUrl__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"label": "AliasPortalUrl__",
																						"activable": true,
																						"autocompletable": false,
																						"width": 230,
																						"version": 0
																					},
																					"stamp": 321
																				},
																				"LabelAliasPortalUrl__": {
																					"type": "Label",
																					"data": [
																						"AliasPortalUrl__"
																					],
																					"options": {
																						"label": "AliasPortalUrl__",
																						"version": 0
																					},
																					"stamp": 322
																				},
																				"DescAliasURL__": {
																					"type": "Text",
																					"data": false,
																					"options": {
																						"marge": 0,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"backgroundcolor": "default",
																						"label": "DescAliasURL__",
																						"version": 0,
																						"image_url": "",
																						"iconClass": ""
																					},
																					"stamp": 323
																				},
																				"Spacer_line6__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "",
																						"width": "",
																						"color": "default",
																						"label": "Spacer line6",
																						"version": 0
																					},
																					"stamp": 324
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-center-19": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 325,
													"*": {
														"Email_delivery_pane": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": "300",
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": true,
																"hideTitleBar": false,
																"hideTitle": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "Email_delivery_pane",
																"leftImageURL": "",
																"removeMargins": false,
																"version": 0,
																"hidden": true,
																"sameHeightAsSiblings": false
															},
															"stamp": 326,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"AddAttachmentsWithEmail__": "LabelAddAttachmentsWithEmail__",
																			"LabelAddAttachmentsWithEmail__": "AddAttachmentsWithEmail__",
																			"GroupEmailMaxAttachSize__": "LabelGroupEmailMaxAttachSize__",
																			"LabelGroupEmailMaxAttachSize__": "GroupEmailMaxAttachSize__",
																			"GroupEmailWithAttach__": "LabelGroupEmailWithAttach__",
																			"LabelGroupEmailWithAttach__": "GroupEmailWithAttach__"
																		},
																		"version": 0
																	},
																	"stamp": 327,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"AddAttachmentsWithEmail__": {
																						"line": 3,
																						"column": 2
																					},
																					"LabelAddAttachmentsWithEmail__": {
																						"line": 3,
																						"column": 1
																					},
																					"GroupEmailMaxAttachSize__": {
																						"line": 2,
																						"column": 2
																					},
																					"LabelGroupEmailMaxAttachSize__": {
																						"line": 2,
																						"column": 1
																					},
																					"GroupEmailWithAttach__": {
																						"line": 1,
																						"column": 2
																					},
																					"LabelGroupEmailWithAttach__": {
																						"line": 1,
																						"column": 1
																					}
																				},
																				"lines": 3,
																				"columns": 2,
																				"colspans": [
																					[],
																					[],
																					[]
																				],
																				"version": 0
																			},
																			"stamp": 328,
																			"*": {
																				"LabelGroupEmailWithAttach__": {
																					"type": "Label",
																					"data": [
																						"GroupEmailWithAttach__"
																					],
																					"options": {
																						"label": "GroupEmailWithAttach__",
																						"version": 0
																					},
																					"stamp": 1183
																				},
																				"GroupEmailWithAttach__": {
																					"type": "CheckBox",
																					"data": [
																						"GroupEmailWithAttach__"
																					],
																					"options": {
																						"label": "GroupEmailWithAttach__",
																						"activable": true,
																						"width": 230,
																						"version": 0
																					},
																					"stamp": 1184
																				},
																				"LabelGroupEmailMaxAttachSize__": {
																					"type": "Label",
																					"data": [
																						"GroupEmailMaxAttachSize__"
																					],
																					"options": {
																						"label": "GroupEmailMaxAttachSize__",
																						"version": 0
																					},
																					"stamp": 1181
																				},
																				"GroupEmailMaxAttachSize__": {
																					"type": "Integer",
																					"data": [
																						"GroupEmailMaxAttachSize__"
																					],
																					"options": {
																						"version": 2,
																						"textSize": "S",
																						"textAlignment": "right",
																						"textStyle": "default",
																						"textColor": "default",
																						"integer": true,
																						"label": "GroupEmailMaxAttachSize__",
																						"precision_internal": 0,
																						"precision_current": 0,
																						"activable": true,
																						"width": "50",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"defaultValue": "6",
																						"browsable": false,
																						"autocompletable": false
																					},
																					"stamp": 1182
																				},
																				"LabelAddAttachmentsWithEmail__": {
																					"type": "Label",
																					"data": [
																						"AddAttachmentsWithEmail__"
																					],
																					"options": {
																						"label": "AddAttachmentsWithEmail__",
																						"version": 0
																					},
																					"stamp": 333
																				},
																				"AddAttachmentsWithEmail__": {
																					"type": "CheckBox",
																					"data": [
																						"AddAttachmentsWithEmail__"
																					],
																					"options": {
																						"label": "AddAttachmentsWithEmail__",
																						"activable": true,
																						"width": 230,
																						"version": 0,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format"
																					},
																					"stamp": 334
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-11": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 363,
													"*": {
														"Archiving_msg_pane": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": false,
																"hideTitleBar": true,
																"hideTitle": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "Archiving_msg_pane",
																"leftImageURL": "warning_yellow.png",
																"removeMargins": false,
																"version": 0,
																"sameHeightAsSiblings": false
															},
															"stamp": 364,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {},
																		"version": 0
																	},
																	"stamp": 365,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"Spacer_line17__": {
																						"line": 1,
																						"column": 1
																					},
																					"Archiving_warning__message__": {
																						"line": 2,
																						"column": 1
																					}
																				},
																				"lines": 2,
																				"columns": 2,
																				"colspans": [
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					]
																				],
																				"version": 0
																			},
																			"stamp": 366,
																			"*": {
																				"Spacer_line17__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "",
																						"width": "",
																						"color": "default",
																						"label": "Spacer line17",
																						"version": 0
																					},
																					"stamp": 367
																				},
																				"Archiving_warning__message__": {
																					"type": "Text",
																					"data": false,
																					"options": {
																						"marge": 0,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "color3",
																						"backgroundcolor": "default",
																						"label": "Archiving_warning__message__",
																						"version": 0,
																						"image_url": "",
																						"iconClass": ""
																					},
																					"stamp": 368
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-center-11": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 369,
													"*": {
														"ArchivingSender_pane": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {
																	"collapsed": false
																},
																"labelLength": "300",
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": true,
																"hideTitleBar": false,
																"hideTitle": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "ArchivingSender_pane",
																"leftImageURL": "",
																"removeMargins": false,
																"version": 0,
																"hidden": true,
																"elementsAlignment": "left",
																"sameHeightAsSiblings": false
															},
															"stamp": 370,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"Document_ArchiveDuration_MOD__": "LabelDocument_ArchiveDuration_MOD__",
																			"LabelDocument_ArchiveDuration_MOD__": "Document_ArchiveDuration_MOD__",
																			"Document_ArchiveDuration_SM__": "LabelDocument_ArchiveDuration_SM__",
																			"LabelDocument_ArchiveDuration_SM__": "Document_ArchiveDuration_SM__",
																			"Document_ArchiveDuration_PORTAL__": "LabelDocument_ArchiveDuration_PORTAL__",
																			"LabelDocument_ArchiveDuration_PORTAL__": "Document_ArchiveDuration_PORTAL__",
																			"Document_ArchiveDuration_FGFAXOUT__": "LabelDocument_ArchiveDuration_FGFAXOUT__",
																			"LabelDocument_ArchiveDuration_FGFAXOUT__": "Document_ArchiveDuration_FGFAXOUT__",
																			"Document_ArchiveDuration_NONE__": "LabelDocument_ArchiveDuration_NONE__",
																			"LabelDocument_ArchiveDuration_NONE__": "Document_ArchiveDuration_NONE__",
																			"Document_ArchiveDuration_OTHER__": "LabelDocument_ArchiveDuration_OTHER__",
																			"LabelDocument_ArchiveDuration_OTHER__": "Document_ArchiveDuration_OTHER__"
																		},
																		"version": 0
																	},
																	"stamp": 371,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"Document_ArchiveDuration_MOD__": {
																						"line": 1,
																						"column": 2
																					},
																					"LabelDocument_ArchiveDuration_MOD__": {
																						"line": 1,
																						"column": 1
																					},
																					"Document_ArchiveDuration_SM__": {
																						"line": 2,
																						"column": 2
																					},
																					"LabelDocument_ArchiveDuration_SM__": {
																						"line": 2,
																						"column": 1
																					},
																					"Document_ArchiveDuration_PORTAL__": {
																						"line": 3,
																						"column": 2
																					},
																					"LabelDocument_ArchiveDuration_PORTAL__": {
																						"line": 3,
																						"column": 1
																					},
																					"Document_ArchiveDuration_FGFAXOUT__": {
																						"line": 4,
																						"column": 2
																					},
																					"LabelDocument_ArchiveDuration_FGFAXOUT__": {
																						"line": 4,
																						"column": 1
																					},
																					"Document_ArchiveDuration_NONE__": {
																						"line": 5,
																						"column": 2
																					},
																					"LabelDocument_ArchiveDuration_NONE__": {
																						"line": 5,
																						"column": 1
																					},
																					"Document_ArchiveDuration_OTHER__": {
																						"line": 6,
																						"column": 2
																					},
																					"LabelDocument_ArchiveDuration_OTHER__": {
																						"line": 6,
																						"column": 1
																					}
																				},
																				"lines": 6,
																				"columns": 2,
																				"colspans": [
																					[],
																					[],
																					[],
																					[],
																					[],
																					[]
																				],
																				"version": 0
																			},
																			"stamp": 372,
																			"*": {
																				"LabelDocument_ArchiveDuration_MOD__": {
																					"type": "Label",
																					"data": [
																						"Document_ArchiveDuration_MOD__"
																					],
																					"options": {
																						"label": "Document_ArchiveDuration_MOD__",
																						"version": 0
																					},
																					"stamp": 373
																				},
																				"Document_ArchiveDuration_MOD__": {
																					"type": "ComboBox",
																					"data": [
																						"Document_ArchiveDuration_MOD__"
																					],
																					"options": {
																						"possibleValues": {
																							"0": "_ARC_FREE",
																							"1": "_ARC_1YEAR",
																							"2": "_ARC_2YEAR",
																							"3": "_ARC_3YEARS",
																							"4": "_ARC_4YEARS",
																							"5": "_ARC_5YEARS",
																							"6": "_ARC_6YEARS",
																							"7": "_ARC_7YEARS",
																							"8": "_ARC_8YEARS",
																							"9": "_ARC_9YEARS",
																							"10": "_ARC_10YEARS",
																							"11": "_ARC_11YEARS"
																						},
																						"version": 1,
																						"keyValueMode": true,
																						"possibleKeys": {
																							"0": "0",
																							"1": "12",
																							"2": "24",
																							"3": "36",
																							"4": "48",
																							"5": "60",
																							"6": "72",
																							"7": "84",
																							"8": "96",
																							"9": "108",
																							"10": "120",
																							"11": "132"
																						},
																						"label": "Document_ArchiveDuration_MOD__",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"DataSourceOrigin": "Standalone",
																						"DisplayedColumns": "",
																						"maxNbLinesToDisplay": 10
																					},
																					"stamp": 374
																				},
																				"LabelDocument_ArchiveDuration_SM__": {
																					"type": "Label",
																					"data": [
																						"Document_ArchiveDuration_SM__"
																					],
																					"options": {
																						"label": "Document_ArchiveDuration_SM__",
																						"version": 0
																					},
																					"stamp": 375
																				},
																				"Document_ArchiveDuration_SM__": {
																					"type": "ComboBox",
																					"data": [
																						"Document_ArchiveDuration_SM__"
																					],
																					"options": {
																						"possibleValues": {
																							"0": "_ARC_FREE",
																							"1": "_ARC_1YEAR",
																							"2": "_ARC_2YEAR",
																							"3": "_ARC_3YEARS",
																							"4": "_ARC_4YEARS",
																							"5": "_ARC_5YEARS",
																							"6": "_ARC_6YEARS",
																							"7": "_ARC_7YEARS",
																							"8": "_ARC_8YEARS",
																							"9": "_ARC_9YEARS",
																							"10": "_ARC_10YEARS",
																							"11": "_ARC_11YEARS"
																						},
																						"version": 1,
																						"keyValueMode": true,
																						"possibleKeys": {
																							"0": "0",
																							"1": "12",
																							"2": "24",
																							"3": "36",
																							"4": "48",
																							"5": "60",
																							"6": "72",
																							"7": "84",
																							"8": "96",
																							"9": "108",
																							"10": "120",
																							"11": "132"
																						},
																						"label": "Document_ArchiveDuration_SM__",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"DataSourceOrigin": "Standalone",
																						"DisplayedColumns": "",
																						"maxNbLinesToDisplay": 10
																					},
																					"stamp": 376
																				},
																				"LabelDocument_ArchiveDuration_PORTAL__": {
																					"type": "Label",
																					"data": [
																						"Document_ArchiveDuration_PORTAL__"
																					],
																					"options": {
																						"label": "Document_ArchiveDuration_PORTAL__",
																						"version": 0
																					},
																					"stamp": 377
																				},
																				"Document_ArchiveDuration_PORTAL__": {
																					"type": "ComboBox",
																					"data": [
																						"Document_ArchiveDuration_PORTAL__"
																					],
																					"options": {
																						"possibleValues": {
																							"0": "_ARC_FREE",
																							"1": "_ARC_1YEAR",
																							"2": "_ARC_2YEAR",
																							"3": "_ARC_3YEARS",
																							"4": "_ARC_4YEARS",
																							"5": "_ARC_5YEARS",
																							"6": "_ARC_6YEARS",
																							"7": "_ARC_7YEARS",
																							"8": "_ARC_8YEARS",
																							"9": "_ARC_9YEARS",
																							"10": "_ARC_10YEARS",
																							"11": "_ARC_11YEARS"
																						},
																						"version": 1,
																						"keyValueMode": true,
																						"possibleKeys": {
																							"0": "0",
																							"1": "12",
																							"2": "24",
																							"3": "36",
																							"4": "48",
																							"5": "60",
																							"6": "72",
																							"7": "84",
																							"8": "96",
																							"9": "108",
																							"10": "120",
																							"11": "132"
																						},
																						"label": "Document_ArchiveDuration_PORTAL__",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"DataSourceOrigin": "Standalone",
																						"DisplayedColumns": "",
																						"maxNbLinesToDisplay": 10
																					},
																					"stamp": 378
																				},
																				"LabelDocument_ArchiveDuration_FGFAXOUT__": {
																					"type": "Label",
																					"data": [
																						"Document_ArchiveDuration_FGFAXOUT__"
																					],
																					"options": {
																						"label": "Document_ArchiveDuration_FGFAXOUT__",
																						"version": 0
																					},
																					"stamp": 381
																				},
																				"Document_ArchiveDuration_FGFAXOUT__": {
																					"type": "ComboBox",
																					"data": [
																						"Document_ArchiveDuration_FGFAXOUT__"
																					],
																					"options": {
																						"possibleValues": {
																							"0": "_ARC_FREE",
																							"1": "_ARC_1YEAR",
																							"2": "_ARC_2YEAR",
																							"3": "_ARC_3YEARS",
																							"4": "_ARC_4YEARS",
																							"5": "_ARC_5YEARS",
																							"6": "_ARC_6YEARS",
																							"7": "_ARC_7YEARS",
																							"8": "_ARC_8YEARS",
																							"9": "_ARC_9YEARS",
																							"10": "_ARC_10YEARS",
																							"11": "_ARC_11YEARS"
																						},
																						"version": 1,
																						"keyValueMode": true,
																						"possibleKeys": {
																							"0": "0",
																							"1": "12",
																							"2": "24",
																							"3": "36",
																							"4": "48",
																							"5": "60",
																							"6": "72",
																							"7": "84",
																							"8": "96",
																							"9": "108",
																							"10": "120",
																							"11": "132"
																						},
																						"label": "Document_ArchiveDuration_FGFAXOUT__",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"DataSourceOrigin": "Standalone",
																						"DisplayedColumns": "",
																						"maxNbLinesToDisplay": 10
																					},
																					"stamp": 382
																				},
																				"LabelDocument_ArchiveDuration_NONE__": {
																					"type": "Label",
																					"data": [
																						"Document_ArchiveDuration_NONE__"
																					],
																					"options": {
																						"label": "Document_ArchiveDuration_NONE__",
																						"version": 0
																					},
																					"stamp": 755
																				},
																				"Document_ArchiveDuration_NONE__": {
																					"type": "ComboBox",
																					"data": [
																						"Document_ArchiveDuration_NONE__"
																					],
																					"options": {
																						"possibleValues": {
																							"0": "_ARC_FREE",
																							"1": "_ARC_1YEAR",
																							"2": "_ARC_2YEAR",
																							"3": "_ARC_3YEARS",
																							"4": "_ARC_4YEARS",
																							"5": "_ARC_5YEARS",
																							"6": "_ARC_6YEARS",
																							"7": "_ARC_7YEARS",
																							"8": "_ARC_8YEARS",
																							"9": "_ARC_9YEARS",
																							"10": "_ARC_10YEARS",
																							"11": "_ARC_11YEARS"
																						},
																						"version": 1,
																						"keyValueMode": true,
																						"possibleKeys": {
																							"0": "0",
																							"1": "12",
																							"2": "24",
																							"3": "36",
																							"4": "48",
																							"5": "60",
																							"6": "72",
																							"7": "84",
																							"8": "96",
																							"9": "108",
																							"10": "120",
																							"11": "132"
																						},
																						"label": "Document_ArchiveDuration_NONE__",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"DataSourceOrigin": "Standalone",
																						"DisplayedColumns": "",
																						"maxNbLinesToDisplay": 10
																					},
																					"stamp": 756
																				},
																				"LabelDocument_ArchiveDuration_OTHER__": {
																					"type": "Label",
																					"data": [
																						"Document_ArchiveDuration_OTHER__"
																					],
																					"options": {
																						"label": "Document_ArchiveDuration_OTHER__",
																						"version": 0
																					},
																					"stamp": 759
																				},
																				"Document_ArchiveDuration_OTHER__": {
																					"type": "ComboBox",
																					"data": [
																						"Document_ArchiveDuration_OTHER__"
																					],
																					"options": {
																						"possibleValues": {
																							"0": "_ARC_FREE",
																							"1": "_ARC_1YEAR",
																							"2": "_ARC_2YEAR",
																							"3": "_ARC_3YEARS",
																							"4": "_ARC_4YEARS",
																							"5": "_ARC_5YEARS",
																							"6": "_ARC_6YEARS",
																							"7": "_ARC_7YEARS",
																							"8": "_ARC_8YEARS",
																							"9": "_ARC_9YEARS",
																							"10": "_ARC_10YEARS",
																							"11": "_ARC_11YEARS"
																						},
																						"version": 1,
																						"keyValueMode": true,
																						"possibleKeys": {
																							"0": "0",
																							"1": "12",
																							"2": "24",
																							"3": "36",
																							"4": "48",
																							"5": "60",
																							"6": "72",
																							"7": "84",
																							"8": "96",
																							"9": "108",
																							"10": "120",
																							"11": "132"
																						},
																						"label": "Document_ArchiveDuration_OTHER__",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"DataSourceOrigin": "Standalone",
																						"DisplayedColumns": "",
																						"maxNbLinesToDisplay": 10
																					},
																					"stamp": 760
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-2": {
													"type": "FlexibleFormLine",
													"options": {
														"version": 0
													},
													"*": {
														"SystemData": {
															"type": "PanelSystemData",
															"options": {
																"multicolumn": false,
																"version": 0,
																"border": {},
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": false,
																"hideTitleBar": false,
																"hideTitle": false,
																"label": "_System data",
																"hidden": true,
																"sameHeightAsSiblings": false
															},
															"stamp": 418,
															"data": []
														}
													},
													"stamp": 419,
													"data": []
												},
												"form-content-left-3": {
													"type": "FlexibleFormLine",
													"options": {
														"version": 0
													},
													"*": {
														"NextProcess": {
															"type": "PanelNextProcess",
															"options": {
																"hidden": true,
																"version": 0,
																"border": {},
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": false,
																"hideTitleBar": false,
																"hideTitle": false,
																"label": "_Next Process",
																"sameHeightAsSiblings": false
															},
															"stamp": 420,
															"data": []
														}
													},
													"stamp": 421,
													"data": []
												},
												"form-content-left-10": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 429,
													"*": {
														"Split_Information_pane": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": "300",
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": true,
																"hideTitleBar": false,
																"hideTitle": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "Split_Information_pane",
																"leftImageURL": "",
																"removeMargins": false,
																"version": 0,
																"hidden": true,
																"elementsAlignment": "left",
																"sameHeightAsSiblings": false
															},
															"stamp": 430,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"LabelSplit_Pages_Count__": "Split_Pages_Count__",
																			"Split_Pages_Count__": "LabelSplit_Pages_Count__",
																			"Split_Documents_Count__": "LabelSplit_Documents_Count__",
																			"LabelSplit_Documents_Count__": "Split_Documents_Count__"
																		},
																		"version": 0
																	},
																	"stamp": 431,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"LabelSplit_Pages_Count__": {
																						"line": 2,
																						"column": 1
																					},
																					"Split_Pages_Count__": {
																						"line": 2,
																						"column": 2
																					},
																					"Split_preview_table__": {
																						"line": 3,
																						"column": 1
																					},
																					"Split_Documents_Count__": {
																						"line": 1,
																						"column": 2
																					},
																					"LabelSplit_Documents_Count__": {
																						"line": 1,
																						"column": 1
																					}
																				},
																				"lines": 3,
																				"columns": 2,
																				"colspans": [
																					[],
																					[],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					]
																				],
																				"version": 0
																			},
																			"stamp": 432,
																			"*": {
																				"LabelSplit_Documents_Count__": {
																					"type": "Label",
																					"data": [
																						"Split_Documents_Count__"
																					],
																					"options": {
																						"label": "Split_Documents_Count__",
																						"version": 0
																					},
																					"stamp": 433
																				},
																				"Split_Documents_Count__": {
																					"type": "Integer",
																					"data": [
																						"Split_Documents_Count__"
																					],
																					"options": {
																						"version": 1,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"integer": true,
																						"label": "Split_Documents_Count__",
																						"precision_internal": 0,
																						"precision_current": 0,
																						"activable": true,
																						"width": "50",
																						"readonly": true,
																						"browsable": false,
																						"autocompletable": false,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format"
																					},
																					"stamp": 434
																				},
																				"Split_preview_table__": {
																					"type": "Table",
																					"data": [
																						"Split_preview_table__"
																					],
																					"options": {
																						"version": 5,
																						"lines": 10,
																						"columns": 4,
																						"displayEmptyLines": false,
																						"alwaysOneLine": false,
																						"menuOptions": {
																							"disableInsertLine": true,
																							"disableAddLine": true,
																							"hideTopNavigation": true,
																							"hideBottomNavigation": false,
																							"hideTableRowMenu": true,
																							"hideTableRowDelete": true,
																							"hideTableRowAdd": true,
																							"addButtonShouldInsert": true,
																							"hideTableRowDuplicate": true,
																							"hideTableRowButtonDuplicate": true
																						},
																						"item": {
																							"display": null
																						},
																						"group": {
																							"name": "GDR\nPage"
																						},
																						"label": "_Split_preview_table",
																						"readonly": false,
																						"subsection": null
																					},
																					"stamp": 435,
																					"*": {
																						"Actions": {
																							"type": "$TableActions",
																							"subtype": "Actions",
																							"options": {
																								"version": 0
																							},
																							"stamp": 436,
																							"*": {
																								"Top": {
																									"type": "$TableActions",
																									"subtype": "Top",
																									"options": {
																										"version": 0
																									},
																									"stamp": 437
																								},
																								"Bottom": {
																									"type": "$TableActions",
																									"subtype": "Top",
																									"options": {
																										"version": 0
																									},
																									"stamp": 438
																								}
																							}
																						},
																						"Header": {
																							"type": "$TableRow",
																							"subtype": "Header",
																							"options": {
																								"version": 0
																							},
																							"stamp": 439,
																							"data": [
																								null
																							],
																							"*": {
																								"Cell1": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 440,
																									"data": [],
																									"*": {
																										"Arrow__": {
																											"type": "Label",
																											"data": [
																												"Arrow__"
																											],
																											"options": {
																												"label": "",
																												"version": 0
																											},
																											"stamp": 441,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell2": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 442,
																									"data": [],
																									"*": {
																										"Split_ViewAction__": {
																											"type": "Label",
																											"data": [
																												"Split_ViewAction__"
																											],
																											"options": {
																												"label": "Split_ViewAction__",
																												"version": 0
																											},
																											"stamp": 443,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell3": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 444,
																									"data": [],
																									"*": {
																										"Split_Pages_Count__": {
																											"type": "Label",
																											"data": [
																												"Split_Pages_Count__"
																											],
																											"options": {
																												"label": "Split_Pages_Count__",
																												"version": 0
																											},
																											"stamp": 445,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell4": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 446,
																									"data": [],
																									"*": {
																										"Split_PageRange__": {
																											"type": "Label",
																											"data": [
																												"Split_PageRange__"
																											],
																											"options": {
																												"label": "Split_PageRange__",
																												"version": 0
																											},
																											"stamp": 447,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								}
																							}
																						},
																						"Clipboard": {
																							"type": "$TableRow",
																							"subtype": "Clipboard",
																							"options": {
																								"version": 0
																							},
																							"stamp": 448,
																							"data": [
																								null
																							],
																							"*": {
																								"Cell1": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 449,
																									"data": [],
																									"*": {
																										"Arrow__": {
																											"type": "ShortText",
																											"data": [
																												"Arrow__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"label": "",
																												"activable": true,
																												"width": "1",
																												"browsable": false,
																												"autocompletable": false,
																												"readonly": true,
																												"version": 0
																											},
																											"stamp": 450,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell2": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 451,
																									"data": [],
																									"*": {
																										"Split_ViewAction__": {
																											"type": "ShortText",
																											"data": [
																												"Split_ViewAction__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"label": "Split_ViewAction__",
																												"activable": true,
																												"width": "70",
																												"browsable": false,
																												"autocompletable": false,
																												"version": 0,
																												"readonly": true,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format"
																											},
																											"stamp": 452,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell3": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 453,
																									"data": [],
																									"*": {
																										"Split_Pages_Count__": {
																											"type": "ShortText",
																											"data": [
																												"Split_Pages_Count__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"label": "Split_Pages_Count__",
																												"activable": true,
																												"width": "60",
																												"browsable": false,
																												"autocompletable": false,
																												"version": 0,
																												"readonly": true,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format"
																											},
																											"stamp": 454,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell4": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 455,
																									"data": [],
																									"*": {
																										"Split_PageRange__": {
																											"type": "ShortText",
																											"data": [
																												"Split_PageRange__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"label": "Split_PageRange__",
																												"activable": true,
																												"width": 140,
																												"browsable": false,
																												"autocompletable": false,
																												"version": 0,
																												"readonly": true,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format"
																											},
																											"stamp": 456,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								}
																							}
																						}
																					}
																				},
																				"Split_Pages_Count__": {
																					"type": "Integer",
																					"data": [
																						"Split_Pages_Count__"
																					],
																					"options": {
																						"version": 1,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"integer": true,
																						"label": "Split_Pages_Count__",
																						"precision_internal": 0,
																						"precision_current": 0,
																						"activable": true,
																						"width": "50",
																						"readonly": true,
																						"browsable": false,
																						"autocompletable": false,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format"
																					},
																					"stamp": 457
																				},
																				"LabelSplit_Pages_Count__": {
																					"type": "Label",
																					"data": [
																						"Split_Pages_Count__"
																					],
																					"options": {
																						"label": "Split_Pages_Count__",
																						"version": 0
																					},
																					"stamp": 458
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-6": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 1169,
													"*": {
														"Document_data_pane": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": "300",
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": true,
																"hideTitleBar": false,
																"hideTitle": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "Document_data_pane",
																"leftImageURL": "",
																"removeMargins": false,
																"elementsAlignment": "left",
																"version": 0,
																"sameHeightAsSiblings": false
															},
															"stamp": 1152,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"Document_ID__": "LabelDocument_ID__",
																			"LabelDocument_ID__": "Document_ID__",
																			"LabelFieldsOrder__": "FieldsOrder__",
																			"FieldsOrder__": "LabelFieldsOrder__",
																			"Document_Type_SDAForm__": "LabelDocument_Type_SDAForm__",
																			"LabelDocument_Type_SDAForm__": "Document_Type_SDAForm__",
																			"Comment__": "LabelComment__",
																			"LabelComment__": "Comment__"
																		},
																		"version": 0
																	},
																	"stamp": 1153,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"Document_ID__": {
																						"line": 1,
																						"column": 2
																					},
																					"LabelDocument_ID__": {
																						"line": 1,
																						"column": 1
																					},
																					"LabelFieldsOrder__": {
																						"line": 5,
																						"column": 1
																					},
																					"FieldsOrder__": {
																						"line": 5,
																						"column": 2
																					},
																					"Document_Type_SDAForm__": {
																						"line": 2,
																						"column": 2
																					},
																					"LabelDocument_Type_SDAForm__": {
																						"line": 2,
																						"column": 1
																					},
																					"Routing_fields__": {
																						"line": 3,
																						"column": 1
																					},
																					"Comment__": {
																						"line": 4,
																						"column": 2
																					},
																					"LabelComment__": {
																						"line": 4,
																						"column": 1
																					}
																				},
																				"lines": 5,
																				"columns": 2,
																				"colspans": [
																					[],
																					[],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[],
																					[]
																				],
																				"version": 0
																			},
																			"stamp": 1154,
																			"*": {
																				"FieldsOrder__": {
																					"type": "ShortText",
																					"data": [
																						"FieldsOrder__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"label": "FieldsOrder",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"length": 2000,
																						"hidden": true,
																						"browsable": false
																					},
																					"stamp": 1959
																				},
																				"LabelFieldsOrder__": {
																					"type": "Label",
																					"data": [
																						"FieldsOrder__"
																					],
																					"options": {
																						"label": "FieldsOrder",
																						"hidden": true,
																						"version": 0
																					},
																					"stamp": 1958
																				},
																				"LabelDocument_Type_SDAForm__": {
																					"type": "Label",
																					"data": [
																						"Document_Type__"
																					],
																					"options": {
																						"label": "Document_Type__",
																						"version": 0,
																						"dataSource": 1
																					},
																					"stamp": 1960
																				},
																				"Document_Type_SDAForm__": {
																					"type": "ShortText",
																					"data": [
																						"Document_Type__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"label": "Document_Type__",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"browsable": false,
																						"autocompletable": false,
																						"version": 0,
																						"dataSource": 1,
																						"readonly": true,
																						"notInDB": true,
																						"dataType": "String"
																					},
																					"stamp": 1961
																				},
																				"Routing_fields__": {
																					"type": "HTML",
																					"data": false,
																					"options": {
																						"label": "Routing_fields__",
																						"htmlContent": "<div id=\"Routing_Fields_label\" onclick=\"setTranslatedLabel(event)\"></div> \n \n<script>function setTranslatedLabel(event){\nif(event.translatedLabel){\ndocument.querySelector(\"#Routing_Fields_label\").textContent=event.translatedLabel;\n}\n}\n</script>",
																						"css": "@ #Routing_Fields_label {\ntext-align: center;\ncolor: #008998;\noutline: 2px dashed #92b0b3;\nbackground: rgba(200, 218, 223, 0.50);\npadding: 10px;\nmargin: 5px 0;\n}@ ",
																						"width": "100%",
																						"version": 0
																					},
																					"stamp": 2127
																				},
																				"LabelComment__": {
																					"type": "Label",
																					"data": [
																						"Comment__"
																					],
																					"options": {
																						"label": "Comment__",
																						"version": 0
																					},
																					"stamp": 1964
																				},
																				"Comment__": {
																					"type": "ShortText",
																					"data": [
																						"Comment__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 0,
																						"label": "Comment__",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"browsable": false
																					},
																					"stamp": 1965
																				},
																				"LabelDocument_ID__": {
																					"type": "Label",
																					"data": [
																						"Document_ID__"
																					],
																					"options": {
																						"label": "Document_ID__",
																						"version": 0
																					},
																					"stamp": 1179
																				},
																				"Document_ID__": {
																					"type": "ShortText",
																					"data": [
																						"Document_ID__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"label": "Document_ID__",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"browsable": false,
																						"autocompletable": false,
																						"enableRuntimeExtraction": true,
																						"version": 0
																					},
																					"stamp": 1180
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-24": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 1981,
													"*": {
														"Additional_fields_control_pane": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": "280px",
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": true,
																"hideTitleBar": true,
																"hideTitle": true,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "Additional_fields_control_pane",
																"hidden": true,
																"leftImageURL": "",
																"removeMargins": false,
																"elementsAlignment": "left",
																"version": 0,
																"sameHeightAsSiblings": false
															},
															"stamp": 1972,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"Button_AddField__": "LabelButton_AddField__",
																			"LabelButton_AddField__": "Button_AddField__",
																			"Button_ModifyField__": "LabelButton_ModifyField__",
																			"LabelButton_ModifyField__": "Button_ModifyField__",
																			"Button_RemoveField__": "LabelButton_RemoveField__",
																			"LabelButton_RemoveField__": "Button_RemoveField__",
																			"Button_ReorderFields__": "LabelButton_ReorderFields__",
																			"LabelButton_ReorderFields__": "Button_ReorderFields__"
																		},
																		"version": 0
																	},
																	"stamp": 1973,
																	"*": {
																		"Grid": {
																			"type": "HorizontalLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"Button_AddField__": {
																						"line": 1,
																						"column": 1
																					},
																					"Button_ModifyField__": {
																						"line": 2,
																						"column": 1
																					},
																					"Button_RemoveField__": {
																						"line": 3,
																						"column": 1
																					},
																					"Button_ReorderFields__": {
																						"line": 4,
																						"column": 1
																					}
																				},
																				"lines": 4,
																				"colspans": [
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					]
																				],
																				"version": 0
																			},
																			"stamp": 1975,
																			"*": {
																				"Button_AddField__": {
																					"type": "FormButton",
																					"data": false,
																					"options": {
																						"buttonLabel": "Button_AddField__",
																						"label": "",
																						"textPosition": "text-right",
																						"textSize": "MS",
																						"textStyle": "default",
																						"textColor": "color1",
																						"urlImageOverlay": "",
																						"style": 1,
																						"width": "",
																						"action": "none",
																						"url": "",
																						"version": 0
																					},
																					"stamp": 1988
																				},
																				"Button_ModifyField__": {
																					"type": "FormButton",
																					"data": false,
																					"options": {
																						"buttonLabel": "Button_ModifyField__",
																						"label": "",
																						"textPosition": "text-right",
																						"textSize": "MS",
																						"textStyle": "default",
																						"textColor": "color1",
																						"urlImageOverlay": "",
																						"width": "",
																						"action": "none",
																						"url": "",
																						"version": 0
																					},
																					"stamp": 2153
																				},
																				"Button_RemoveField__": {
																					"type": "FormButton",
																					"data": false,
																					"options": {
																						"buttonLabel": "Button_RemoveField__",
																						"label": "",
																						"textPosition": "text-right",
																						"textSize": "MS",
																						"textStyle": "default",
																						"textColor": "color1",
																						"urlImageOverlay": "",
																						"width": "",
																						"action": "none",
																						"url": "",
																						"version": 0
																					},
																					"stamp": 1990
																				},
																				"Button_ReorderFields__": {
																					"type": "FormButton",
																					"data": false,
																					"options": {
																						"buttonLabel": "Button_ReorderFields__",
																						"label": "_Button2",
																						"textPosition": "text-right",
																						"textSize": "MS",
																						"textStyle": "default",
																						"textColor": "color1",
																						"urlImageOverlay": "",
																						"width": "",
																						"action": "none",
																						"url": "",
																						"version": 0
																					},
																					"stamp": 1992
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-22": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 2111,
													"*": {
														"extraction_feature_pane": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": "300px",
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": true,
																"hideTitleBar": true,
																"hideTitle": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "extraction_feature_pane",
																"hidden": false,
																"leftImageURL": "",
																"removeMargins": false,
																"elementsAlignment": "left",
																"version": 0,
																"sameHeightAsSiblings": false
															},
															"stamp": 2089,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"MoreExtractionOptions__": "LabelMoreExtractionOptions__",
																			"LabelMoreExtractionOptions__": "MoreExtractionOptions__"
																		},
																		"version": 0
																	},
																	"stamp": 2090,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"MoreExtractionOptions__": {
																						"line": 1,
																						"column": 2
																					},
																					"LabelMoreExtractionOptions__": {
																						"line": 1,
																						"column": 1
																					}
																				},
																				"lines": 1,
																				"columns": 2,
																				"colspans": [
																					[]
																				],
																				"version": 0
																			},
																			"stamp": 2091,
																			"*": {
																				"LabelMoreExtractionOptions__": {
																					"type": "Label",
																					"data": false,
																					"options": {
																						"label": "",
																						"version": 0
																					},
																					"stamp": 2118
																				},
																				"MoreExtractionOptions__": {
																					"type": "HTML",
																					"data": false,
																					"options": {
																						"label": "",
																						"htmlContent": "<span class=\"adv\"><i class=\"fa fa-plus-square\"></i> $TITLE$</span>",
																						"css": "@ .adv { cursor: pointer; }@ \n.adv:hover { text-decoration:underline; }@ ",
																						"version": 0
																					},
																					"stamp": 2119
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-25": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 2074,
													"*": {
														"Advanced_extraction_pane": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {
																	"collapsed": false
																},
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": false,
																"hideTitleBar": true,
																"hideTitle": false,
																"backgroundcolor": "color7",
																"labelsAlignment": "right",
																"label": "Advanced_extraction_pane",
																"hidden": false,
																"leftImageURL": "",
																"removeMargins": false,
																"elementsAlignment": "left",
																"version": 0,
																"labelLength": 0,
																"sameHeightAsSiblings": false
															},
															"stamp": 2065,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"LabelShow_recipient_id__": "Show_recipient_id__",
																			"Show_recipient_id__": "LabelShow_recipient_id__",
																			"LabelShow_comment__": "Show_comment__",
																			"Show_comment__": "LabelShow_comment__",
																			"LabelShow_document_number__": "Show_document_number__",
																			"Show_document_number__": "LabelShow_document_number__"
																		},
																		"version": 0
																	},
																	"stamp": 2066,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"Fields_part__": {
																						"line": 1,
																						"column": 1
																					},
																					"LabelShow_recipient_id__": {
																						"line": 2,
																						"column": 1
																					},
																					"Show_recipient_id__": {
																						"line": 2,
																						"column": 2
																					},
																					"LabelShow_comment__": {
																						"line": 3,
																						"column": 1
																					},
																					"Show_comment__": {
																						"line": 3,
																						"column": 2
																					},
																					"LabelShow_document_number__": {
																						"line": 4,
																						"column": 1
																					},
																					"Show_document_number__": {
																						"line": 4,
																						"column": 2
																					}
																				},
																				"lines": 4,
																				"columns": 2,
																				"colspans": [
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[],
																					[],
																					[]
																				],
																				"version": 0
																			},
																			"stamp": 2067,
																			"*": {
																				"Show_document_number__": {
																					"type": "CheckBox",
																					"data": [
																						"Show_document_number__"
																					],
																					"options": {
																						"label": "Show_document_number",
																						"activable": true,
																						"width": 230,
																						"version": 0
																					},
																					"stamp": 2031
																				},
																				"LabelShow_document_number__": {
																					"type": "Label",
																					"data": [
																						"Show_document_number__"
																					],
																					"options": {
																						"label": "Show_document_number",
																						"version": 0
																					},
																					"stamp": 2030
																				},
																				"Show_comment__": {
																					"type": "CheckBox",
																					"data": [
																						"Show_comment__"
																					],
																					"options": {
																						"label": "Show_comment",
																						"activable": true,
																						"width": 230,
																						"version": 0
																					},
																					"stamp": 2029
																				},
																				"LabelShow_comment__": {
																					"type": "Label",
																					"data": [
																						"Show_comment__"
																					],
																					"options": {
																						"label": "Show_comment",
																						"version": 0
																					},
																					"stamp": 2028
																				},
																				"Show_recipient_id__": {
																					"type": "CheckBox",
																					"data": [
																						"Show_recipient_id__"
																					],
																					"options": {
																						"label": "Show_recipient_id",
																						"activable": true,
																						"width": 230,
																						"helpText": "_showRecipientIDTooltip",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"version": 0
																					},
																					"stamp": 2027
																				},
																				"LabelShow_recipient_id__": {
																					"type": "Label",
																					"data": [
																						"Show_recipient_id__"
																					],
																					"options": {
																						"label": "Show_recipient_id",
																						"version": 0
																					},
																					"stamp": 2026
																				},
																				"Fields_part__": {
																					"type": "Title",
																					"data": false,
																					"options": {
																						"marge": 0,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"backgroundcolor": "default",
																						"iconClass": "",
																						"label": "Fields_part",
																						"version": 0
																					},
																					"stamp": 2059
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-9": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 459,
													"*": {
														"Recipient_pane": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {
																	"collapsed": false
																},
																"labelLength": "300",
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": true,
																"hideTitleBar": false,
																"hideTitle": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "Recipient_pane",
																"leftImageURL": "",
																"removeMargins": false,
																"version": 0,
																"hidden": false,
																"elementsAlignment": "left",
																"sameHeightAsSiblings": false
															},
															"stamp": 460,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"Recipient_ID__": "LabelRecipient_ID__",
																			"LabelRecipient_ID__": "Recipient_ID__",
																			"Recipient_name__": "LabelRecipient_name__",
																			"LabelRecipient_name__": "Recipient_name__",
																			"Recipient_address__": "LabelRecipient_address__",
																			"LabelRecipient_address__": "Recipient_address__",
																			"Recipient_phone__": "LabelRecipient_phone__",
																			"LabelRecipient_phone__": "Recipient_phone__",
																			"Recipient_fax__": "LabelRecipient_fax__",
																			"LabelRecipient_fax__": "Recipient_fax__",
																			"Recipient_email__": "LabelRecipient_email__",
																			"LabelRecipient_email__": "Recipient_email__",
																			"Recipient_copyemail__": "LabelRecipient_copyemail__",
																			"LabelRecipient_copyemail__": "Recipient_copyemail__",
																			"Recipient_country__": "LabelRecipient_country__",
																			"LabelRecipient_country__": "Recipient_country__"
																		},
																		"version": 0
																	},
																	"stamp": 461,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"Recipient_ID__": {
																						"line": 1,
																						"column": 2
																					},
																					"LabelRecipient_ID__": {
																						"line": 1,
																						"column": 1
																					},
																					"Recipient_name__": {
																						"line": 2,
																						"column": 2
																					},
																					"LabelRecipient_name__": {
																						"line": 2,
																						"column": 1
																					},
																					"Recipient_address__": {
																						"line": 3,
																						"column": 2
																					},
																					"LabelRecipient_address__": {
																						"line": 3,
																						"column": 1
																					},
																					"Recipient_phone__": {
																						"line": 5,
																						"column": 2
																					},
																					"LabelRecipient_phone__": {
																						"line": 5,
																						"column": 1
																					},
																					"Recipient_fax__": {
																						"line": 6,
																						"column": 2
																					},
																					"LabelRecipient_fax__": {
																						"line": 6,
																						"column": 1
																					},
																					"Recipient_email__": {
																						"line": 7,
																						"column": 2
																					},
																					"LabelRecipient_email__": {
																						"line": 7,
																						"column": 1
																					},
																					"Recipient_copyemail__": {
																						"line": 8,
																						"column": 2
																					},
																					"LabelRecipient_copyemail__": {
																						"line": 8,
																						"column": 1
																					},
																					"Recipient_country__": {
																						"line": 4,
																						"column": 2
																					},
																					"LabelRecipient_country__": {
																						"line": 4,
																						"column": 1
																					}
																				},
																				"lines": 8,
																				"columns": 2,
																				"colspans": [
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[]
																				],
																				"version": 0
																			},
																			"stamp": 462,
																			"*": {
																				"LabelRecipient_ID__": {
																					"type": "Label",
																					"data": [
																						"Recipient_ID__"
																					],
																					"options": {
																						"label": "Recipient_ID__",
																						"version": 0
																					},
																					"stamp": 463
																				},
																				"Recipient_ID__": {
																					"type": "ShortText",
																					"data": [
																						"Recipient_ID__"
																					],
																					"options": {
																						"enableRuntimeExtraction": true,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"label": "Recipient_ID__",
																						"activable": true,
																						"width": 230,
																						"browsable": false,
																						"version": 0,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"autocompletable": false
																					},
																					"stamp": 464
																				},
																				"LabelRecipient_name__": {
																					"type": "Label",
																					"data": [
																						"Recipient_name__"
																					],
																					"options": {
																						"label": "Recipient_name__",
																						"version": 0
																					},
																					"stamp": 465
																				},
																				"Recipient_name__": {
																					"type": "ShortText",
																					"data": [
																						"Recipient_name__"
																					],
																					"options": {
																						"enableRuntimeExtraction": true,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"label": "Recipient_name__",
																						"activable": true,
																						"width": 230,
																						"browsable": false,
																						"version": 0,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"autocompletable": false
																					},
																					"stamp": 466
																				},
																				"LabelRecipient_address__": {
																					"type": "Label",
																					"data": [
																						"Recipient_address__"
																					],
																					"options": {
																						"label": "Recipient_address__",
																						"version": 0
																					},
																					"stamp": 467
																				},
																				"Recipient_address__": {
																					"type": "LongText",
																					"data": [
																						"Recipient_address__"
																					],
																					"options": {
																						"enableRuntimeExtraction": true,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"label": "Recipient_address__",
																						"activable": true,
																						"width": 230,
																						"numberOfLines": 6,
																						"browsable": false,
																						"version": 0,
																						"resizable": true,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"minNbLines": 1
																					},
																					"stamp": 468
																				},
																				"LabelRecipient_country__": {
																					"type": "Label",
																					"data": [
																						"Recipient_country__"
																					],
																					"options": {
																						"label": "Recipient_country__",
																						"version": 0
																					},
																					"stamp": 1841
																				},
																				"Recipient_country__": {
																					"type": "ShortText",
																					"data": [
																						"Recipient_country__"
																					],
																					"options": {
																						"enableRuntimeExtraction": true,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"label": "Recipient_country__",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"browsable": false,
																						"autocompletable": false
																					},
																					"stamp": 1842
																				},
																				"LabelRecipient_phone__": {
																					"type": "Label",
																					"data": [
																						"Recipient_phone__"
																					],
																					"options": {
																						"label": "Recipient_phone__",
																						"version": 0
																					},
																					"stamp": 469
																				},
																				"Recipient_phone__": {
																					"type": "ShortText",
																					"data": [
																						"Recipient_phone__"
																					],
																					"options": {
																						"enableRuntimeExtraction": true,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"label": "Recipient_phone__",
																						"activable": true,
																						"width": 230,
																						"length": 30,
																						"browsable": false,
																						"version": 0,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"autocompletable": false
																					},
																					"stamp": 470
																				},
																				"LabelRecipient_fax__": {
																					"type": "Label",
																					"data": [
																						"Recipient_fax__"
																					],
																					"options": {
																						"label": "Recipient_fax__",
																						"version": 0
																					},
																					"stamp": 471
																				},
																				"Recipient_fax__": {
																					"type": "ShortText",
																					"data": [
																						"Recipient_fax__"
																					],
																					"options": {
																						"enableRuntimeExtraction": true,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"label": "Recipient_fax__",
																						"activable": true,
																						"width": 230,
																						"browsable": false,
																						"version": 0,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"autocompletable": false
																					},
																					"stamp": 472
																				},
																				"LabelRecipient_email__": {
																					"type": "Label",
																					"data": [
																						"Recipient_email__"
																					],
																					"options": {
																						"label": "Recipient_email__",
																						"version": 0
																					},
																					"stamp": 473
																				},
																				"Recipient_email__": {
																					"type": "ShortText",
																					"data": [
																						"Recipient_email__"
																					],
																					"options": {
																						"enableRuntimeExtraction": true,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"label": "Recipient_email__",
																						"activable": true,
																						"width": 230,
																						"length": 255,
																						"browsable": false,
																						"version": 0,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"autocompletable": false
																					},
																					"stamp": 474
																				},
																				"LabelRecipient_copyemail__": {
																					"type": "Label",
																					"data": [
																						"Recipient_copyemail__"
																					],
																					"options": {
																						"label": "Recipient_copyemail__",
																						"version": 0
																					},
																					"stamp": 475
																				},
																				"Recipient_copyemail__": {
																					"type": "ShortText",
																					"data": [
																						"Recipient_copyemail__"
																					],
																					"options": {
																						"enableRuntimeExtraction": true,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"label": "Recipient_copyemail__",
																						"activable": true,
																						"width": 230,
																						"length": 255,
																						"browsable": false,
																						"version": 0,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"autocompletable": false
																					},
																					"stamp": 476
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-13": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 596,
													"*": {
														"Sender_pane": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {
																	"collapsed": false
																},
																"labelLength": "300",
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": true,
																"hideTitleBar": false,
																"hideTitle": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "Sender_pane",
																"leftImageURL": "",
																"removeMargins": false,
																"version": 0,
																"hidden": false,
																"elementsAlignment": "left",
																"sameHeightAsSiblings": false
															},
															"stamp": 597,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"Sender_address__": "LabelSender_address__",
																			"LabelSender_address__": "Sender_address__",
																			"Sender_country__": "LabelSender_country__",
																			"LabelSender_country__": "Sender_country__",
																			"Sender_company__": "LabelSender_company__",
																			"LabelSender_company__": "Sender_company__",
																			"Sender_name__": "LabelSender_name__",
																			"LabelSender_name__": "Sender_name__",
																			"Sender_phone__": "LabelSender_phone__",
																			"LabelSender_phone__": "Sender_phone__",
																			"Sender_fax__": "LabelSender_fax__",
																			"LabelSender_fax__": "Sender_fax__",
																			"Sender_email__": "LabelSender_email__",
																			"LabelSender_email__": "Sender_email__"
																		},
																		"version": 0
																	},
																	"stamp": 598,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"Sender_address__": {
																						"line": 2,
																						"column": 2
																					},
																					"LabelSender_address__": {
																						"line": 2,
																						"column": 1
																					},
																					"Sender_country__": {
																						"line": 3,
																						"column": 2
																					},
																					"LabelSender_country__": {
																						"line": 3,
																						"column": 1
																					},
																					"Sender_company__": {
																						"line": 1,
																						"column": 2
																					},
																					"LabelSender_company__": {
																						"line": 1,
																						"column": 1
																					},
																					"Sender_name__": {
																						"line": 4,
																						"column": 2
																					},
																					"LabelSender_name__": {
																						"line": 4,
																						"column": 1
																					},
																					"Sender_phone__": {
																						"line": 5,
																						"column": 2
																					},
																					"LabelSender_phone__": {
																						"line": 5,
																						"column": 1
																					},
																					"Sender_fax__": {
																						"line": 6,
																						"column": 2
																					},
																					"LabelSender_fax__": {
																						"line": 6,
																						"column": 1
																					},
																					"Sender_email__": {
																						"line": 7,
																						"column": 2
																					},
																					"LabelSender_email__": {
																						"line": 7,
																						"column": 1
																					}
																				},
																				"lines": 7,
																				"columns": 2,
																				"colspans": [
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[]
																				],
																				"version": 0
																			},
																			"stamp": 599,
																			"*": {
																				"LabelSender_company__": {
																					"type": "Label",
																					"data": [
																						"Sender_company__"
																					],
																					"options": {
																						"label": "Sender_company__",
																						"version": 0
																					},
																					"stamp": 600
																				},
																				"Sender_company__": {
																					"type": "ShortText",
																					"data": [
																						"Sender_company__"
																					],
																					"options": {
																						"enableRuntimeExtraction": true,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"label": "Sender_company__",
																						"activable": true,
																						"width": 230,
																						"length": 100,
																						"browsable": false,
																						"version": 0,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"autocompletable": false
																					},
																					"stamp": 601
																				},
																				"LabelSender_address__": {
																					"type": "Label",
																					"data": [
																						"Sender_address__"
																					],
																					"options": {
																						"label": "Sender_address__",
																						"version": 0
																					},
																					"stamp": 610
																				},
																				"Sender_address__": {
																					"type": "LongText",
																					"data": [
																						"Sender_address__"
																					],
																					"options": {
																						"enableRuntimeExtraction": true,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"label": "Sender_address__",
																						"activable": true,
																						"width": 230,
																						"numberOfLines": 6,
																						"browsable": false,
																						"version": 0,
																						"resizable": true,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"minNbLines": 1
																					},
																					"stamp": 611
																				},
																				"LabelSender_country__": {
																					"type": "Label",
																					"data": [
																						"Sender_country__"
																					],
																					"options": {
																						"label": "Sender_country__",
																						"version": 0
																					},
																					"stamp": 855
																				},
																				"Sender_country__": {
																					"type": "ShortText",
																					"data": [
																						"Sender_country__"
																					],
																					"options": {
																						"enableRuntimeExtraction": true,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"label": "Sender_country__",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"browsable": false,
																						"version": 0,
																						"autocompletable": false
																					},
																					"stamp": 856
																				},
																				"LabelSender_name__": {
																					"type": "Label",
																					"data": [
																						"Sender_name__"
																					],
																					"options": {
																						"label": "Sender_name__",
																						"version": 0
																					},
																					"stamp": 612
																				},
																				"Sender_name__": {
																					"type": "ShortText",
																					"data": [
																						"Sender_name__"
																					],
																					"options": {
																						"enableRuntimeExtraction": true,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"label": "Sender_name__",
																						"activable": true,
																						"width": 230,
																						"length": 100,
																						"browsable": false,
																						"version": 0,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"autocompletable": false
																					},
																					"stamp": 613
																				},
																				"LabelSender_phone__": {
																					"type": "Label",
																					"data": [
																						"Sender_phone__"
																					],
																					"options": {
																						"label": "Sender_phone__",
																						"version": 0
																					},
																					"stamp": 614
																				},
																				"Sender_phone__": {
																					"type": "ShortText",
																					"data": [
																						"Sender_phone__"
																					],
																					"options": {
																						"enableRuntimeExtraction": true,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"label": "Sender_phone__",
																						"activable": true,
																						"width": 230,
																						"browsable": false,
																						"version": 0,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"autocompletable": false
																					},
																					"stamp": 615
																				},
																				"LabelSender_fax__": {
																					"type": "Label",
																					"data": [
																						"Sender_fax__"
																					],
																					"options": {
																						"label": "Sender_fax__",
																						"version": 0
																					},
																					"stamp": 616
																				},
																				"Sender_fax__": {
																					"type": "ShortText",
																					"data": [
																						"Sender_fax__"
																					],
																					"options": {
																						"enableRuntimeExtraction": true,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"label": "Sender_fax__",
																						"activable": true,
																						"width": 230,
																						"browsable": false,
																						"version": 0,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"autocompletable": false
																					},
																					"stamp": 617
																				},
																				"LabelSender_email__": {
																					"type": "Label",
																					"data": [
																						"Sender_email__"
																					],
																					"options": {
																						"label": "Sender_email__",
																						"version": 0
																					},
																					"stamp": 618
																				},
																				"Sender_email__": {
																					"type": "ShortText",
																					"data": [
																						"Sender_email__"
																					],
																					"options": {
																						"enableRuntimeExtraction": true,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"label": "Sender_email__",
																						"activable": true,
																						"width": 230,
																						"browsable": false,
																						"version": 0,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"autocompletable": false
																					},
																					"stamp": 619
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-26": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 787,
													"*": {
														"Billing_info_extraction_pane": {
															"type": "PanelData",
															"data": [],
															"options": {
																"labelLength": "300",
																"border": {},
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": true,
																"hideTitleBar": false,
																"hideTitle": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "Billing_info_extraction_pane",
																"leftImageURL": "",
																"removeMargins": false,
																"version": 0,
																"elementsAlignment": "left",
																"sameHeightAsSiblings": false
															},
															"stamp": 775,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"Billing_Info_Billing_Account_Extraction__": "LabelBilling_Info_Billing_Account_Extraction__",
																			"LabelBilling_Info_Billing_Account_Extraction__": "Billing_Info_Billing_Account_Extraction__",
																			"Billing_Info_Cost_Center_Extraction__": "LabelBilling_Info_Cost_Center_Extraction__",
																			"LabelBilling_Info_Cost_Center_Extraction__": "Billing_Info_Cost_Center_Extraction__"
																		},
																		"version": 0
																	},
																	"stamp": 776,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"Billing_Info_Billing_Account_Extraction__": {
																						"line": 1,
																						"column": 2
																					},
																					"LabelBilling_Info_Billing_Account_Extraction__": {
																						"line": 1,
																						"column": 1
																					},
																					"Billing_Info_Cost_Center_Extraction__": {
																						"line": 2,
																						"column": 2
																					},
																					"LabelBilling_Info_Cost_Center_Extraction__": {
																						"line": 2,
																						"column": 1
																					}
																				},
																				"lines": 2,
																				"columns": 2,
																				"colspans": [
																					[],
																					[]
																				],
																				"version": 0
																			},
																			"stamp": 777,
																			"*": {
																				"LabelBilling_Info_Billing_Account_Extraction__": {
																					"type": "Label",
																					"data": [
																						"Billing_Info_Billing_Account_Extraction__"
																					],
																					"options": {
																						"label": "Billing_Info_Billing_Account_Extraction__",
																						"version": 0
																					},
																					"stamp": 792
																				},
																				"Billing_Info_Billing_Account_Extraction__": {
																					"type": "ShortText",
																					"data": [
																						"Billing_Info_Billing_Account_Extraction__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"label": "Billing_Info_Billing_Account_Extraction__",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"enableRuntimeExtraction": true,
																						"browsable": false,
																						"autocompletable": false,
																						"version": 0
																					},
																					"stamp": 793
																				},
																				"LabelBilling_Info_Cost_Center_Extraction__": {
																					"type": "Label",
																					"data": [
																						"Billing_Info_Cost_Center_Extraction__"
																					],
																					"options": {
																						"label": "Billing_Info_Cost_Center_Extraction__",
																						"version": 0
																					},
																					"stamp": 794
																				},
																				"Billing_Info_Cost_Center_Extraction__": {
																					"type": "ShortText",
																					"data": [
																						"Billing_Info_Cost_Center_Extraction__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"label": "Billing_Info_Cost_Center_Extraction__",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"enableRuntimeExtraction": true,
																						"browsable": false,
																						"autocompletable": false,
																						"version": 0
																					},
																					"stamp": 795
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-1": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 1925,
													"*": {
														"Recognition_Match_String_pane": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": "300",
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": true,
																"hideTitleBar": true,
																"hideTitle": true,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "Recognition_Match_String_pane",
																"leftImageURL": "",
																"removeMargins": false,
																"elementsAlignment": "left",
																"version": 0,
																"sameHeightAsSiblings": false
															},
															"stamp": 1926,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"LabelUseTextInside__": "MatchStringWithinDocument__",
																			"MatchStringWithinDocument__": "LabelUseTextInside__"
																		},
																		"version": 0
																	},
																	"stamp": 1927,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"LabelUseTextInside__": {
																						"line": 1,
																						"column": 1
																					},
																					"MatchStringWithinDocument__": {
																						"line": 1,
																						"column": 2
																					}
																				},
																				"lines": 1,
																				"columns": 2,
																				"colspans": [
																					[]
																				],
																				"version": 0
																			},
																			"stamp": 1928,
																			"*": {
																				"MatchStringWithinDocument__": {
																					"type": "CheckBox",
																					"data": [
																						"MatchStringWithinDocument__"
																					],
																					"options": {
																						"label": "_Match string within document",
																						"activable": true,
																						"width": 230,
																						"helpText": "_Select this option to apply this configuration according to a string in the document",
																						"helpURL": "7008",
																						"helpFormat": "HTML Format",
																						"defaultValue": true,
																						"version": 0
																					},
																					"stamp": 1784
																				},
																				"LabelUseTextInside__": {
																					"type": "Label",
																					"data": [
																						"MatchStringWithinDocument__"
																					],
																					"options": {
																						"label": "_Match string within document",
																						"version": 0
																					},
																					"stamp": 1783
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-4": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 1929,
													"*": {
														"Recognition_Match_String_Data_pane": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": "300",
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": true,
																"hideTitleBar": true,
																"hideTitle": true,
																"backgroundcolor": "color7",
																"labelsAlignment": "right",
																"label": "Recognition_Match_String_Data_pane",
																"leftImageURL": "",
																"removeMargins": false,
																"elementsAlignment": "left",
																"version": 0,
																"hidden": true,
																"sameHeightAsSiblings": false
															},
															"stamp": 1930,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"LabelConfigurationSelection_Area__": "ConfigurationSelection_Area__",
																			"ConfigurationSelection_Area__": "LabelConfigurationSelection_Area__",
																			"LabelConfigurationSelection_Path__": "ConfigurationSelection_Path__",
																			"ConfigurationSelection_Path__": "LabelConfigurationSelection_Path__",
																			"LabelConfigurationSelection_Criteria__": "ConfigurationSelection_Criteria__",
																			"ConfigurationSelection_Criteria__": "LabelConfigurationSelection_Criteria__",
																			"LabelConfigurationSelection_CriteriaIsRegex__": "ConfigurationSelection_CriteriaIsRegex__",
																			"ConfigurationSelection_CriteriaIsRegex__": "LabelConfigurationSelection_CriteriaIsRegex__",
																			"LabelConfigurationSelection_CriteriaMatchCase__": "ConfigurationSelection_CriteriaMatchCase__",
																			"ConfigurationSelection_CriteriaMatchCase__": "LabelConfigurationSelection_CriteriaMatchCase__"
																		},
																		"version": 0
																	},
																	"stamp": 1931,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"LabelConfigurationSelection_Area__": {
																						"line": 1,
																						"column": 1
																					},
																					"ConfigurationSelection_Area__": {
																						"line": 1,
																						"column": 2
																					},
																					"LabelConfigurationSelection_Path__": {
																						"line": 2,
																						"column": 1
																					},
																					"ConfigurationSelection_Path__": {
																						"line": 2,
																						"column": 2
																					},
																					"LabelConfigurationSelection_Criteria__": {
																						"line": 3,
																						"column": 1
																					},
																					"ConfigurationSelection_Criteria__": {
																						"line": 3,
																						"column": 2
																					},
																					"LabelConfigurationSelection_CriteriaIsRegex__": {
																						"line": 4,
																						"column": 1
																					},
																					"ConfigurationSelection_CriteriaIsRegex__": {
																						"line": 4,
																						"column": 2
																					},
																					"LabelConfigurationSelection_CriteriaMatchCase__": {
																						"line": 5,
																						"column": 1
																					},
																					"ConfigurationSelection_CriteriaMatchCase__": {
																						"line": 5,
																						"column": 2
																					}
																				},
																				"lines": 5,
																				"columns": 2,
																				"colspans": [
																					[],
																					[],
																					[],
																					[],
																					[]
																				],
																				"version": 0
																			},
																			"stamp": 1932,
																			"*": {
																				"ConfigurationSelection_CriteriaMatchCase__": {
																					"type": "CheckBox",
																					"data": [
																						"ConfigurationSelection_CriteriaMatchCase__"
																					],
																					"options": {
																						"label": "MatchCase__",
																						"activable": true,
																						"version": 0,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"width": "230"
																					},
																					"stamp": 41
																				},
																				"LabelConfigurationSelection_CriteriaMatchCase__": {
																					"type": "Label",
																					"data": [
																						"ConfigurationSelection_CriteriaMatchCase__"
																					],
																					"options": {
																						"label": "MatchCase__",
																						"version": 0
																					},
																					"stamp": 40
																				},
																				"ConfigurationSelection_CriteriaIsRegex__": {
																					"type": "CheckBox",
																					"data": [
																						"ConfigurationSelection_CriteriaIsRegex__"
																					],
																					"options": {
																						"label": "ConfigurationSelection_CriteriaIsRegex__",
																						"activable": true,
																						"version": 0,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format"
																					},
																					"stamp": 39
																				},
																				"LabelConfigurationSelection_CriteriaIsRegex__": {
																					"type": "Label",
																					"data": [
																						"ConfigurationSelection_CriteriaIsRegex__"
																					],
																					"options": {
																						"label": "ConfigurationSelection_CriteriaIsRegex__",
																						"version": 0
																					},
																					"stamp": 38
																				},
																				"ConfigurationSelection_Criteria__": {
																					"type": "ShortText",
																					"data": [
																						"ConfigurationSelection_Criteria__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"label": "ConfigurationSelection_Criteria__",
																						"activable": true,
																						"width": 230,
																						"browsable": false,
																						"autocompletable": false,
																						"version": 0,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format"
																					},
																					"stamp": 37
																				},
																				"LabelConfigurationSelection_Criteria__": {
																					"type": "Label",
																					"data": [
																						"ConfigurationSelection_Criteria__"
																					],
																					"options": {
																						"label": "ConfigurationSelection_Criteria__",
																						"version": 0
																					},
																					"stamp": 36
																				},
																				"ConfigurationSelection_Path__": {
																					"type": "ShortText",
																					"data": [
																						"ConfigurationSelection_Path__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"label": "ConfigurationSelection_Path__",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"length": 100,
																						"browsable": false,
																						"version": 0,
																						"autocompletable": false
																					},
																					"stamp": 750
																				},
																				"LabelConfigurationSelection_Path__": {
																					"type": "Label",
																					"data": [
																						"ConfigurationSelection_Path__"
																					],
																					"options": {
																						"label": "ConfigurationSelection_Path__",
																						"version": 0
																					},
																					"stamp": 749
																				},
																				"ConfigurationSelection_Area__": {
																					"type": "ShortText",
																					"data": [
																						"ConfigurationSelection_Area__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"label": "ConfigurationSelection_Area__",
																						"activable": true,
																						"width": 230,
																						"browsable": false,
																						"autocompletable": false,
																						"version": 0,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format"
																					},
																					"stamp": 35
																				},
																				"LabelConfigurationSelection_Area__": {
																					"type": "Label",
																					"data": [
																						"ConfigurationSelection_Area__"
																					],
																					"options": {
																						"label": "ConfigurationSelection_Area__",
																						"version": 0
																					},
																					"stamp": 34
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-5": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 2135,
													"*": {
														"ButtonBar_pane": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": true,
																"hideTitleBar": false,
																"hideTitle": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "ButtonBar_pane",
																"hidden": false,
																"leftImageURL": "",
																"removeMargins": false,
																"elementsAlignment": "left",
																"version": 0,
																"labelLength": "300px",
																"sameHeightAsSiblings": false
															},
															"stamp": 2136,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"LabelDisplaySaveButton__": "DisplaySaveButton__",
																			"DisplaySaveButton__": "LabelDisplaySaveButton__",
																			"LabelDisplayResubmitButton__": "DisplayResubmitButton__",
																			"DisplayResubmitButton__": "LabelDisplayResubmitButton__",
																			"LabelDisplayRejectButton__": "DisplayRejectButton__",
																			"DisplayRejectButton__": "LabelDisplayRejectButton__",
																			"LabelDisplaySaveAndQuitButton__": "DisplaySaveAndQuitButton__",
																			"DisplaySaveAndQuitButton__": "LabelDisplaySaveAndQuitButton__",
																			"LabelDisplaySetAsideButton__": "DisplaySetAsideButton__",
																			"DisplaySetAsideButton__": "LabelDisplaySetAsideButton__"
																		},
																		"version": 0
																	},
																	"stamp": 2137,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"LabelDisplaySaveButton__": {
																						"line": 2,
																						"column": 1
																					},
																					"DisplaySaveButton__": {
																						"line": 2,
																						"column": 2
																					},
																					"LabelDisplayResubmitButton__": {
																						"line": 4,
																						"column": 1
																					},
																					"DisplayResubmitButton__": {
																						"line": 4,
																						"column": 2
																					},
																					"LabelDisplayRejectButton__": {
																						"line": 1,
																						"column": 1
																					},
																					"DisplayRejectButton__": {
																						"line": 1,
																						"column": 2
																					},
																					"LabelDisplaySaveAndQuitButton__": {
																						"line": 3,
																						"column": 1
																					},
																					"DisplaySaveAndQuitButton__": {
																						"line": 3,
																						"column": 2
																					},
																					"LabelDisplaySetAsideButton__": {
																						"line": 5,
																						"column": 1
																					},
																					"DisplaySetAsideButton__": {
																						"line": 5,
																						"column": 2
																					}
																				},
																				"lines": 5,
																				"columns": 2,
																				"colspans": [
																					[],
																					[],
																					[],
																					[],
																					[]
																				],
																				"version": 0
																			},
																			"stamp": 2138,
																			"*": {
																				"LabelDisplayRejectButton__": {
																					"type": "Label",
																					"data": [
																						"DisplayRejectButton__"
																					],
																					"options": {
																						"label": "DisplayRejectButton__",
																						"version": 0
																					},
																					"stamp": 2139
																				},
																				"DisplayRejectButton__": {
																					"type": "CheckBox",
																					"data": [
																						"DisplayRejectButton__"
																					],
																					"options": {
																						"label": "DisplayRejectButton__",
																						"activable": true,
																						"width": 230,
																						"version": 0
																					},
																					"stamp": 2140
																				},
																				"DisplaySaveAndQuitButton__": {
																					"type": "CheckBox",
																					"data": [
																						"DisplaySaveAndQuitButton__"
																					],
																					"options": {
																						"label": "DisplaySaveAndQuitButton__",
																						"activable": true,
																						"width": 230,
																						"version": 0
																					},
																					"stamp": 2141
																				},
																				"LabelDisplaySaveAndQuitButton__": {
																					"type": "Label",
																					"data": [
																						"DisplaySaveAndQuitButton__"
																					],
																					"options": {
																						"label": "DisplaySaveAndQuitButton__",
																						"version": 0
																					},
																					"stamp": 2142
																				},
																				"DisplaySaveButton__": {
																					"type": "CheckBox",
																					"data": [
																						"DisplaySaveButton__"
																					],
																					"options": {
																						"label": "DisplaySaveButton__",
																						"activable": true,
																						"width": 230,
																						"version": 0
																					},
																					"stamp": 2143
																				},
																				"LabelDisplaySaveButton__": {
																					"type": "Label",
																					"data": [
																						"DisplaySaveButton__"
																					],
																					"options": {
																						"label": "DisplaySaveButton__",
																						"version": 0
																					},
																					"stamp": 2144
																				},
																				"LabelDisplayResubmitButton__": {
																					"type": "Label",
																					"data": [
																						"DisplayResubmitButton__"
																					],
																					"options": {
																						"label": "DisplayResubmitButton__",
																						"version": 0
																					},
																					"stamp": 2145
																				},
																				"DisplayResubmitButton__": {
																					"type": "CheckBox",
																					"data": [
																						"DisplayResubmitButton__"
																					],
																					"options": {
																						"label": "DisplayResubmitButton__",
																						"activable": true,
																						"width": 230,
																						"version": 0
																					},
																					"stamp": 2146
																				},
																				"LabelDisplaySetAsideButton__": {
																					"type": "Label",
																					"data": [
																						"DisplaySetAsideButton__"
																					],
																					"options": {
																						"label": "DisplaySetAsideButton__",
																						"version": 0
																					},
																					"stamp": 2173
																				},
																				"DisplaySetAsideButton__": {
																					"type": "CheckBox",
																					"data": [
																						"DisplaySetAsideButton__"
																					],
																					"options": {
																						"label": "DisplaySetAsideButton__",
																						"activable": true,
																						"width": 230,
																						"version": 0
																					},
																					"stamp": 2174
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												}
											},
											"stamp": 733,
											"data": []
										},
										"form-content-right": {
											"type": "FlexibleFormSplitter",
											"options": {
												"version": 1,
												"box": {
													"top": "0%",
													"left": "55%",
													"width": "45%",
													"height": null
												},
												"hidden": false,
												"hideSeparator": true
											},
											"*": {
												"form-content-right-1": {
													"type": "FlexibleFormLine",
													"options": {
														"version": 0
													},
													"*": {
														"PreviewPanel": {
															"type": "PanelPreview",
															"options": {
																"border": {},
																"version": 0,
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": false,
																"hideTitleBar": true,
																"hideTitle": false,
																"label": "_Document Preview",
																"sameHeightAsSiblings": false
															},
															"*": {
																"Preview": {
																	"data": [
																		"previewdata"
																	],
																	"type": "Preview",
																	"options": {
																		"version": 0
																	},
																	"stamp": 734
																}
															},
															"stamp": 735,
															"data": []
														}
													},
													"stamp": 736,
													"data": []
												}
											},
											"stamp": 737,
											"data": []
										}
									},
									"stamp": 738,
									"data": []
								}
							},
							"stamp": 739,
							"data": []
						}
					},
					"stamp": 740,
					"data": []
				},
				"form-footer": {
					"type": "FlexibleFormFooter",
					"options": {
						"splitterType": "simple-h",
						"version": 0,
						"maxwidth": 1440
					},
					"*": {
						"Previous": {
							"type": "SubmitButton",
							"data": false,
							"options": {
								"label": "Previous__",
								"textPosition": "text-right",
								"textSize": "MS",
								"textColor": "color1",
								"url": "",
								"action": "none",
								"version": 0
							},
							"stamp": 741
						},
						"Next": {
							"type": "SubmitButton",
							"data": false,
							"options": {
								"label": "Next__",
								"textPosition": "text-right",
								"textSize": "MS",
								"textColor": "color1",
								"url": "",
								"action": "none",
								"version": 0,
								"textStyle": "default"
							},
							"stamp": 742
						},
						"Save": {
							"type": "SubmitButton",
							"data": false,
							"options": {
								"label": "_Save",
								"textPosition": "text-right",
								"textSize": "MS",
								"textStyle": "default",
								"textColor": "color1",
								"automaticSave": true,
								"style": 1,
								"urlImageOverlay": "",
								"url": "",
								"position": null,
								"action": "none",
								"version": 0
							},
							"stamp": 2021
						},
						"Close": {
							"type": "SubmitButton",
							"options": {
								"label": "_Close",
								"action": "cancel",
								"submit": true,
								"version": 0,
								"textPosition": "text-right",
								"textSize": "MS",
								"textColor": "color1"
							},
							"stamp": 744,
							"data": []
						}
					},
					"stamp": 745,
					"data": []
				}
			},
			"stamp": 746,
			"data": []
		}
	},
	"stamps": 2174,
	"data": []
}