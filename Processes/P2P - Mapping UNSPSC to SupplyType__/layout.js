{
	"*": {
		"Screen": {
			"type": "FlexibleFormLayout",
			"options": {
				"splitterType": "simple-v",
				"version": 0
			},
			"*": {
				"form-header": {
					"type": "FlexibleFormHeader",
					"options": {
						"hidePanelsMenu": false,
						"version": 0
					},
					"stamp": 2,
					"data": []
				},
				"form-center": {
					"type": "FlexibleFormCenter",
					"options": {
						"version": 0
					},
					"*": {
						"form-content": {
							"type": "FlexibleFormContent",
							"options": {
								"version": 0
							},
							"*": {
								"form-content-center": {
									"type": "FlexibleFormSplitter",
									"options": {
										"version": 1,
										"box": {
											"top": "0%",
											"left": "0%",
											"width": "100%"
										},
										"hidden": false
									},
									"*": {
										"form-content-center-1": {
											"type": "FlexibleFormLine",
											"data": [],
											"options": {},
											"stamp": 9,
											"*": {
												"DataPanel": {
													"type": "PanelData",
													"options": {
														"border": {},
														"version": 0,
														"labelLength": 0,
														"iconURL": "",
														"TitleFontSize": "S",
														"hideActionButtons": false,
														"hideTitleBar": false,
														"hideTitle": false,
														"backgroundcolor": "default",
														"labelsAlignment": "right",
														"label": "_Document data"
													},
													"*": {
														"FieldsManager": {
															"type": "FieldsManager",
															"options": {
																"controlsAssociation": {
																	"CompanyCode__": "LabelCompanyCode__",
																	"LabelCompanyCode__": "CompanyCode__",
																	"UNSPSC__": "LabelUNSPSC__",
																	"LabelUNSPSC__": "UNSPSC__",
																	"SupplyTypeId__": "LabelSupplyTypeId__",
																	"LabelSupplyTypeId__": "SupplyTypeId__",
																	"SupplyType__": "LabelSupplyType__",
																	"LabelSupplyType__": "SupplyType__"
																},
																"version": 0
															},
															"*": {
																"Grid": {
																	"type": "GridLayout",
																	"options": {
																		"lines": 4,
																		"columns": 2,
																		"version": 0,
																		"ctrlsPos": {
																			"CompanyCode__": {
																				"line": 1,
																				"column": 2
																			},
																			"LabelCompanyCode__": {
																				"line": 1,
																				"column": 1
																			},
																			"UNSPSC__": {
																				"line": 2,
																				"column": 2
																			},
																			"LabelUNSPSC__": {
																				"line": 2,
																				"column": 1
																			},
																			"SupplyTypeId__": {
																				"line": 4,
																				"column": 2
																			},
																			"LabelSupplyTypeId__": {
																				"line": 4,
																				"column": 1
																			},
																			"SupplyType__": {
																				"line": 3,
																				"column": 2
																			},
																			"LabelSupplyType__": {
																				"line": 3,
																				"column": 1
																			}
																		},
																		"colspans": [
																			[],
																			[],
																			[],
																			[]
																		]
																	},
																	"data": [
																		"fields"
																	],
																	"*": {
																		"LabelCompanyCode__": {
																			"type": "Label",
																			"data": [
																				"CompanyCode__"
																			],
																			"options": {
																				"label": "_CompanyCode"
																			},
																			"stamp": 14
																		},
																		"CompanyCode__": {
																			"type": "DatabaseComboBox",
																			"data": [
																				"CompanyCode__"
																			],
																			"options": {
																				"version": 1,
																				"label": "_CompanyCode",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"minNbLines": 1,
																				"maxNbLines": 1,
																				"PreFillFTS": true,
																				"fTSmaxRecords": "20",
																				"browsable": true
																			},
																			"stamp": 15
																		},
																		"LabelUNSPSC__": {
																			"type": "Label",
																			"data": [
																				"UNSPSC__"
																			],
																			"options": {
																				"label": "_UNSPSC"
																			},
																			"stamp": 16
																		},
																		"UNSPSC__": {
																			"type": "DatabaseComboBox",
																			"data": [
																				"UNSPSC__"
																			],
																			"options": {
																				"version": 1,
																				"label": "_UNSPSC",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"minNbLines": 1,
																				"maxNbLines": 1,
																				"fTSmaxRecords": "20",
																				"browsable": false
																			},
																			"stamp": 17
																		},
																		"LabelSupplyType__": {
																			"type": "Label",
																			"data": [
																				"SupplyType__"
																			],
																			"options": {
																				"label": "_SupplyType"
																			},
																			"stamp": 22
																		},
																		"SupplyType__": {
																			"type": "DatabaseComboBox",
																			"data": [
																				"SupplyType__"
																			],
																			"options": {
																				"version": 1,
																				"label": "_SupplyType",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"minNbLines": 1,
																				"maxNbLines": 1,
																				"PreFillFTS": true,
																				"fTSmaxRecords": "20",
																				"browsable": true
																			},
																			"stamp": 23
																		},
																		"LabelSupplyTypeId__": {
																			"type": "Label",
																			"data": [
																				"Select_from_a_table__"
																			],
																			"options": {
																				"label": "_SupplyTypeId"
																			},
																			"stamp": 20
																		},
																		"SupplyTypeId__": {
																			"type": "DatabaseComboBox",
																			"data": [
																				"SupplyTypeId__"
																			],
																			"options": {
																				"version": 1,
																				"label": "_SupplyTypeId",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"minNbLines": 1,
																				"maxNbLines": 1,
																				"PreFillFTS": true,
																				"fTSmaxRecords": "20",
																				"browsable": true,
																				"readonly": true
																			},
																			"stamp": 21
																		}
																	},
																	"stamp": 8
																}
															},
															"stamp": 7,
															"data": []
														}
													},
													"stamp": 6,
													"data": []
												}
											}
										}
									},
									"stamp": 5,
									"data": []
								}
							},
							"stamp": 4,
							"data": []
						}
					},
					"stamp": 3,
					"data": []
				},
				"form-footer": {
					"type": "FlexibleFormFooter",
					"options": {
						"splitterType": "simple-h",
						"version": 0
					},
					"*": {
						"Save": {
							"type": "SubmitButton",
							"options": {
								"label": "_Save",
								"action": "savequit",
								"submit": true,
								"version": 0,
								"shortCut": "S"
							},
							"stamp": 11,
							"data": []
						},
						"Close": {
							"type": "SubmitButton",
							"options": {
								"label": "_Close",
								"action": "cancel",
								"submit": true,
								"version": 0,
								"shortCut": "Q"
							},
							"stamp": 12,
							"data": []
						},
						"Delete": {
							"type": "SubmitButton",
							"options": {
								"label": "_Delete",
								"action": "delete",
								"submit": true,
								"version": 0,
								"disabled": true
							},
							"stamp": 13,
							"data": []
						}
					},
					"stamp": 10,
					"data": []
				}
			},
			"stamp": 1,
			"data": []
		}
	},
	"stamps": 23,
	"data": []
}