{
	"*": {
		"Screen": {
			"type": "FlexibleFormLayout",
			"options": {
				"splitterType": "simple-v",
				"version": 0,
				"backColor": "text-backgroundcolor-default",
				"style": null
			},
			"*": {
				"form-header": {
					"type": "FlexibleFormHeader",
					"options": {
						"hidePanelsMenu": false,
						"version": 0
					},
					"stamp": 2,
					"data": []
				},
				"form-center": {
					"type": "FlexibleFormCenter",
					"options": {
						"version": 0
					},
					"*": {
						"form-content": {
							"type": "FlexibleFormContent",
							"options": {
								"version": 0
							},
							"*": {
								"form-content-center": {
									"type": "FlexibleFormSplitter",
									"options": {
										"version": 1,
										"box": {
											"top": "0%",
											"left": "0%",
											"width": "100%"
										},
										"hidden": false
									},
									"*": {
										"form-content-center-1": {
											"type": "FlexibleFormLine",
											"data": [],
											"options": {
												"version": 0
											},
											"stamp": 9,
											"*": {
												"DataPanel": {
													"type": "PanelData",
													"options": {
														"border": {},
														"version": 0,
														"iconURL": "",
														"TitleFontSize": "S",
														"hideActionButtons": true,
														"hideTitleBar": false,
														"hideTitle": false,
														"sameHeightAsSiblings": false,
														"backgroundcolor": "default",
														"labelsAlignment": "right",
														"label": "_KeywordPane",
														"leftImageURL": "",
														"removeMargins": false,
														"elementsAlignment": "left",
														"labelLength": 0
													},
													"*": {
														"FieldsManager": {
															"type": "FieldsManager",
															"options": {
																"controlsAssociation": {
																	"CompanyCode__": "LabelCompanyCode__",
																	"LabelCompanyCode__": "CompanyCode__",
																	"VendorNumber__": "LabelVendorNumber__",
																	"LabelVendorNumber__": "VendorNumber__",
																	"Keyword__": "LabelKeyword__",
																	"LabelKeyword__": "Keyword__",
																	"Description__": "LabelDescription__",
																	"LabelDescription__": "Description__",
																	"MaximumAmount__": "LabelMaximumAmount__",
																	"LabelMaximumAmount__": "MaximumAmount__",
																	"MaximumInvoicePercentage__": "LabelMaximumInvoicePercentage__",
																	"LabelMaximumInvoicePercentage__": "MaximumInvoicePercentage__"
																},
																"version": 0
															},
															"*": {
																"Grid": {
																	"type": "GridLayout",
																	"options": {
																		"lines": 6,
																		"columns": 2,
																		"version": 0,
																		"ctrlsPos": {
																			"CompanyCode__": {
																				"line": 1,
																				"column": 2
																			},
																			"LabelCompanyCode__": {
																				"line": 1,
																				"column": 1
																			},
																			"VendorNumber__": {
																				"line": 2,
																				"column": 2
																			},
																			"LabelVendorNumber__": {
																				"line": 2,
																				"column": 1
																			},
																			"Keyword__": {
																				"line": 3,
																				"column": 2
																			},
																			"LabelKeyword__": {
																				"line": 3,
																				"column": 1
																			},
																			"Description__": {
																				"line": 4,
																				"column": 2
																			},
																			"LabelDescription__": {
																				"line": 4,
																				"column": 1
																			},
																			"MaximumAmount__": {
																				"line": 5,
																				"column": 2
																			},
																			"LabelMaximumAmount__": {
																				"line": 5,
																				"column": 1
																			},
																			"MaximumInvoicePercentage__": {
																				"line": 6,
																				"column": 2
																			},
																			"LabelMaximumInvoicePercentage__": {
																				"line": 6,
																				"column": 1
																			}
																		},
																		"colspans": [
																			[],
																			[],
																			[],
																			[],
																			[],
																			[]
																		]
																	},
																	"data": [
																		"fields"
																	],
																	"*": {
																		"LabelCompanyCode__": {
																			"type": "Label",
																			"data": [
																				"CompanyCode__"
																			],
																			"options": {
																				"label": "_CompanyCode",
																				"version": 0
																			},
																			"stamp": 15
																		},
																		"CompanyCode__": {
																			"type": "DatabaseComboBox",
																			"data": [
																				"CompanyCode__"
																			],
																			"options": {
																				"version": 1,
																				"autoCompleteSearchMode": "startswith",
																				"searchMode": "matches",
																				"label": "_CompanyCode",
																				"activable": true,
																				"width": "500",
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"helpIconPosition": "Right",
																				"minNbLines": 1,
																				"maxNbLines": 1,
																				"fTSmaxRecords": "20",
																				"browsable": true,
																				"PreFillFTS": true
																			},
																			"stamp": 16
																		},
																		"LabelVendorNumber__": {
																			"type": "Label",
																			"data": [
																				"VendorNumber__"
																			],
																			"options": {
																				"label": "_VendorNumber",
																				"version": 0
																			},
																			"stamp": 17
																		},
																		"VendorNumber__": {
																			"type": "DatabaseComboBox",
																			"data": [
																				"VendorNumber__"
																			],
																			"options": {
																				"version": 1,
																				"autoCompleteSearchMode": "startswith",
																				"searchMode": "matches",
																				"label": "_VendorNumber",
																				"activable": true,
																				"width": "500",
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"helpIconPosition": "Right",
																				"minNbLines": 1,
																				"maxNbLines": 1,
																				"fTSmaxRecords": "20",
																				"browsable": true
																			},
																			"stamp": 18
																		},
																		"LabelKeyword__": {
																			"type": "Label",
																			"data": [
																				"Keyword__"
																			],
																			"options": {
																				"label": "_Keyword",
																				"version": 0
																			},
																			"stamp": 20
																		},
																		"Keyword__": {
																			"type": "ShortText",
																			"data": [
																				"Keyword__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"version": 1,
																				"label": "_Keyword",
																				"activable": true,
																				"width": "500",
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"helpIconPosition": "Right",
																				"browsable": false
																			},
																			"stamp": 21
																		},
																		"LabelDescription__": {
																			"type": "Label",
																			"data": [
																				"Description__"
																			],
																			"options": {
																				"label": "_Description",
																				"version": 0
																			},
																			"stamp": 24
																		},
																		"Description__": {
																			"type": "ShortText",
																			"data": [
																				"Description__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"version": 1,
																				"label": "_Description",
																				"activable": true,
																				"width": "500",
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"helpIconPosition": "Right",
																				"browsable": false
																			},
																			"stamp": 25
																		},
																		"LabelMaximumAmount__": {
																			"type": "Label",
																			"data": [
																				"MaximumAmount__"
																			],
																			"options": {
																				"label": "_MaximumAmount",
																				"version": 0
																			},
																			"stamp": 68
																		},
																		"MaximumAmount__": {
																			"type": "Decimal",
																			"data": [
																				"MaximumAmount__"
																			],
																			"options": {
																				"version": 2,
																				"textSize": "S",
																				"textAlignment": "right",
																				"textStyle": "default",
																				"textColor": "default",
																				"integer": false,
																				"label": "_MaximumAmount",
																				"precision_internal": 2,
																				"precision_current": 2,
																				"activable": true,
																				"width": 500,
																				"helpText": "_MaximumAmount_Help",
																				"helpURL": "2604",
																				"helpFormat": "HTML Format",
																				"helpIconPosition": "Right",
																				"enablePlusMinus": false
																			},
																			"stamp": 69
																		},
																		"LabelMaximumInvoicePercentage__": {
																			"type": "Label",
																			"data": [
																				"MaximumInvoicePercentage__"
																			],
																			"options": {
																				"label": "_MaximumInvoicePercentage",
																				"version": 0
																			},
																			"stamp": 72
																		},
																		"MaximumInvoicePercentage__": {
																			"type": "Decimal",
																			"data": [
																				"MaximumInvoicePercentage__"
																			],
																			"options": {
																				"version": 2,
																				"textSize": "S",
																				"textAlignment": "right",
																				"textStyle": "default",
																				"textColor": "default",
																				"integer": false,
																				"label": "_MaximumInvoicePercentage",
																				"precision_internal": 4,
																				"precision_current": 4,
																				"activable": true,
																				"width": 500,
																				"helpText": "_MaximumInvoicePercentage_Help",
																				"helpURL": "2605",
																				"helpFormat": "HTML Format",
																				"helpIconPosition": "Right",
																				"enablePlusMinus": false
																			},
																			"stamp": 73
																		}
																	},
																	"stamp": 8
																}
															},
															"stamp": 7,
															"data": []
														}
													},
													"stamp": 6,
													"data": []
												},
												"Codings": {
													"type": "PanelData",
													"data": [],
													"options": {
														"border": {},
														"iconURL": "",
														"TitleFontSize": "S",
														"hideActionButtons": true,
														"hideTitleBar": false,
														"hideTitle": false,
														"sameHeightAsSiblings": false,
														"backgroundcolor": "default",
														"labelsAlignment": "right",
														"label": "_Codings",
														"hidden": false,
														"leftImageURL": "",
														"removeMargins": false,
														"elementsAlignment": "left",
														"version": 0,
														"labelLength": 0
													},
													"stamp": 56,
													"*": {
														"FieldsManager": {
															"type": "FieldsManager",
															"data": [],
															"options": {
																"controlsAssociation": {
																	"LabelCostCenter__": "CostCenter__",
																	"CostCenter__": "LabelCostCenter__",
																	"LabelGLAccount__": "GLAccount__",
																	"GLAccount__": "LabelGLAccount__",
																	"LabelTaxCode__": "TaxCode__",
																	"TaxCode__": "LabelTaxCode__",
																	"LabelProjectCode__": "ProjectCode__",
																	"ProjectCode__": "LabelProjectCode__",
																	"LabelTaxJurisdiction__": "TaxJurisdiction__",
																	"TaxJurisdiction__": "LabelTaxJurisdiction__",
																	"LabelWBSElement__": "WBSElement__",
																	"WBSElement__": "LabelWBSElement__",
																	"LabelBusinessArea__": "BusinessArea__",
																	"BusinessArea__": "LabelBusinessArea__",
																	"LabelAssignment__": "Assignment__",
																	"Assignment__": "LabelAssignment__",
																	"LabelInternalOrder__": "InternalOrder__",
																	"InternalOrder__": "LabelInternalOrder__",
																	"LabelTradingPartner__": "TradingPartner__",
																	"TradingPartner__": "LabelTradingPartner__"
																},
																"version": 0
															},
															"stamp": 57,
															"*": {
																"Grid": {
																	"type": "GridLayout",
																	"data": [
																		"fields"
																	],
																	"options": {
																		"ctrlsPos": {
																			"Codings_Common__": {
																				"line": 1,
																				"column": 1
																			},
																			"LabelCostCenter__": {
																				"line": 2,
																				"column": 1
																			},
																			"CostCenter__": {
																				"line": 2,
																				"column": 2
																			},
																			"LabelGLAccount__": {
																				"line": 3,
																				"column": 1
																			},
																			"GLAccount__": {
																				"line": 3,
																				"column": 2
																			},
																			"LabelTaxCode__": {
																				"line": 4,
																				"column": 1
																			},
																			"TaxCode__": {
																				"line": 4,
																				"column": 2
																			},
																			"Codings_Generic__": {
																				"line": 5,
																				"column": 1
																			},
																			"LabelProjectCode__": {
																				"line": 6,
																				"column": 1
																			},
																			"ProjectCode__": {
																				"line": 6,
																				"column": 2
																			},
																			"Codings_SAP__": {
																				"line": 7,
																				"column": 1
																			},
																			"LabelTaxJurisdiction__": {
																				"line": 8,
																				"column": 1
																			},
																			"TaxJurisdiction__": {
																				"line": 8,
																				"column": 2
																			},
																			"LabelWBSElement__": {
																				"line": 9,
																				"column": 1
																			},
																			"WBSElement__": {
																				"line": 9,
																				"column": 2
																			},
																			"LabelBusinessArea__": {
																				"line": 10,
																				"column": 1
																			},
																			"BusinessArea__": {
																				"line": 10,
																				"column": 2
																			},
																			"LabelAssignment__": {
																				"line": 11,
																				"column": 1
																			},
																			"Assignment__": {
																				"line": 11,
																				"column": 2
																			},
																			"LabelInternalOrder__": {
																				"line": 12,
																				"column": 1
																			},
																			"InternalOrder__": {
																				"line": 12,
																				"column": 2
																			},
																			"LabelTradingPartner__": {
																				"line": 13,
																				"column": 1
																			},
																			"TradingPartner__": {
																				"line": 13,
																				"column": 2
																			}
																		},
																		"lines": 13,
																		"columns": 2,
																		"colspans": [
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			],
																			[],
																			[],
																			[],
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			],
																			[],
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[]
																		],
																		"version": 0
																	},
																	"stamp": 58,
																	"*": {
																		"TradingPartner__": {
																			"type": "ShortText",
																			"data": [
																				"TradingPartner__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"version": 1,
																				"label": "_TradingPartner",
																				"activable": true,
																				"width": "500",
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"helpIconPosition": "Right",
																				"browsable": false
																			},
																			"stamp": 48
																		},
																		"LabelTradingPartner__": {
																			"type": "Label",
																			"data": [
																				"TradingPartner__"
																			],
																			"options": {
																				"label": "_TradingPartner",
																				"version": 0
																			},
																			"stamp": 47
																		},
																		"InternalOrder__": {
																			"type": "ShortText",
																			"data": [
																				"InternalOrder__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"version": 1,
																				"label": "_InternalOrder",
																				"activable": true,
																				"width": "500",
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"helpIconPosition": "Right",
																				"browsable": false
																			},
																			"stamp": 46
																		},
																		"LabelInternalOrder__": {
																			"type": "Label",
																			"data": [
																				"InternalOrder__"
																			],
																			"options": {
																				"label": "_InternalOrder",
																				"version": 0
																			},
																			"stamp": 45
																		},
																		"Assignment__": {
																			"type": "ShortText",
																			"data": [
																				"Assignment__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"version": 1,
																				"label": "_Assignment",
																				"activable": true,
																				"width": "500",
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"helpIconPosition": "Right",
																				"browsable": false
																			},
																			"stamp": 44
																		},
																		"LabelAssignment__": {
																			"type": "Label",
																			"data": [
																				"Assignment__"
																			],
																			"options": {
																				"label": "_Assignment",
																				"version": 0
																			},
																			"stamp": 43
																		},
																		"BusinessArea__": {
																			"type": "ShortText",
																			"data": [
																				"BusinessArea__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"version": 1,
																				"label": "_BusinessArea",
																				"activable": true,
																				"width": "500",
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"helpIconPosition": "Right",
																				"browsable": false
																			},
																			"stamp": 42
																		},
																		"LabelBusinessArea__": {
																			"type": "Label",
																			"data": [
																				"BusinessArea__"
																			],
																			"options": {
																				"label": "_BusinessArea",
																				"version": 0
																			},
																			"stamp": 41
																		},
																		"WBSElement__": {
																			"type": "ShortText",
																			"data": [
																				"WBSElement__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"version": 1,
																				"label": "_WBSElement",
																				"activable": true,
																				"width": "500",
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"helpIconPosition": "Right",
																				"browsable": false
																			},
																			"stamp": 40
																		},
																		"LabelWBSElement__": {
																			"type": "Label",
																			"data": [
																				"WBSElement__"
																			],
																			"options": {
																				"label": "_WBSElement",
																				"version": 0
																			},
																			"stamp": 39
																		},
																		"TaxJurisdiction__": {
																			"type": "ShortText",
																			"data": [
																				"TaxJurisdiction__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"version": 1,
																				"label": "_TaxJurisdiction",
																				"activable": true,
																				"width": "500",
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"helpIconPosition": "Right",
																				"browsable": false
																			},
																			"stamp": 50
																		},
																		"LabelTaxJurisdiction__": {
																			"type": "Label",
																			"data": [
																				"TaxJurisdiction__"
																			],
																			"options": {
																				"label": "_TaxJurisdiction",
																				"version": 0
																			},
																			"stamp": 49
																		},
																		"ProjectCode__": {
																			"type": "DatabaseComboBox",
																			"data": [
																				"ProjectCode__"
																			],
																			"options": {
																				"version": 1,
																				"autoCompleteSearchMode": "startswith",
																				"searchMode": "matches",
																				"label": "_ProjectCode",
																				"activable": true,
																				"width": "500",
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"helpIconPosition": "Right",
																				"minNbLines": 1,
																				"maxNbLines": 1,
																				"PreFillFTS": true,
																				"fTSmaxRecords": "20",
																				"browsable": true
																			},
																			"stamp": 36
																		},
																		"LabelProjectCode__": {
																			"type": "Label",
																			"data": [
																				"ProjectCode__"
																			],
																			"options": {
																				"label": "_ProjectCode",
																				"version": 0
																			},
																			"stamp": 35
																		},
																		"TaxCode__": {
																			"type": "DatabaseComboBox",
																			"data": [
																				"TaxCode__"
																			],
																			"options": {
																				"version": 1,
																				"autoCompleteSearchMode": "startswith",
																				"searchMode": "matches",
																				"label": "_TaxCode",
																				"activable": true,
																				"width": "500",
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"helpIconPosition": "Right",
																				"minNbLines": 1,
																				"maxNbLines": 1,
																				"PreFillFTS": true,
																				"fTSmaxRecords": "20",
																				"browsable": true
																			},
																			"stamp": 54
																		},
																		"LabelTaxCode__": {
																			"type": "Label",
																			"data": [
																				"TaxCode__"
																			],
																			"options": {
																				"label": "_TaxCode",
																				"version": 0
																			},
																			"stamp": 53
																		},
																		"GLAccount__": {
																			"type": "DatabaseComboBox",
																			"data": [
																				"GLAccount__"
																			],
																			"options": {
																				"version": 1,
																				"autoCompleteSearchMode": "startswith",
																				"searchMode": "matches",
																				"label": "_GLAccount",
																				"activable": true,
																				"width": "500",
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"helpIconPosition": "Right",
																				"minNbLines": 1,
																				"maxNbLines": 1,
																				"PreFillFTS": true,
																				"fTSmaxRecords": "20",
																				"browsable": true
																			},
																			"stamp": 32
																		},
																		"LabelGLAccount__": {
																			"type": "Label",
																			"data": [
																				"DatabaseComboBox__"
																			],
																			"options": {
																				"label": "_GLAccount",
																				"version": 0
																			},
																			"stamp": 31
																		},
																		"CostCenter__": {
																			"type": "DatabaseComboBox",
																			"data": [
																				"CostCenter__"
																			],
																			"options": {
																				"version": 1,
																				"autoCompleteSearchMode": "startswith",
																				"searchMode": "matches",
																				"label": "_CostCenter",
																				"activable": true,
																				"width": "500",
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"helpIconPosition": "Right",
																				"minNbLines": 1,
																				"maxNbLines": 1,
																				"fTSmaxRecords": "20",
																				"browsable": true,
																				"PreFillFTS": true
																			},
																			"stamp": 30
																		},
																		"LabelCostCenter__": {
																			"type": "Label",
																			"data": [
																				"CostCenter__"
																			],
																			"options": {
																				"label": "_CostCenter",
																				"version": 0
																			},
																			"stamp": 29
																		},
																		"Codings_Common__": {
																			"type": "Title",
																			"data": false,
																			"options": {
																				"marge": 0,
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"backgroundcolor": "default",
																				"iconClass": "",
																				"label": "_Codings_Common",
																				"version": 0
																			},
																			"stamp": 59
																		},
																		"Codings_Generic__": {
																			"type": "Title",
																			"data": false,
																			"options": {
																				"marge": 0,
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"backgroundcolor": "default",
																				"iconClass": "",
																				"label": "_Codings_Generic",
																				"version": 0
																			},
																			"stamp": 62
																		},
																		"Codings_SAP__": {
																			"type": "Title",
																			"data": false,
																			"options": {
																				"marge": 0,
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"backgroundcolor": "default",
																				"iconClass": "",
																				"label": "_Codings_SAP",
																				"version": 0
																			},
																			"stamp": 63
																		}
																	}
																}
															}
														}
													}
												}
											}
										}
									},
									"stamp": 5,
									"data": []
								}
							},
							"stamp": 4,
							"data": []
						}
					},
					"stamp": 3,
					"data": []
				},
				"form-footer": {
					"type": "FlexibleFormFooter",
					"options": {
						"splitterType": "simple-h",
						"version": 0
					},
					"*": {
						"Save": {
							"type": "SubmitButton",
							"options": {
								"label": "_Save",
								"action": "savequit",
								"submit": true,
								"version": 0,
								"shortCut": "S"
							},
							"stamp": 11,
							"data": []
						},
						"Close": {
							"type": "SubmitButton",
							"options": {
								"label": "_Close",
								"action": "cancel",
								"submit": true,
								"version": 0,
								"shortCut": "Q"
							},
							"stamp": 12,
							"data": []
						},
						"Delete": {
							"type": "SubmitButton",
							"options": {
								"label": "_Delete",
								"action": "delete",
								"submit": true,
								"version": 0,
								"disabled": true
							},
							"stamp": 13,
							"data": []
						}
					},
					"stamp": 10,
					"data": []
				}
			},
			"stamp": 1,
			"data": []
		}
	},
	"stamps": 73,
	"data": []
}