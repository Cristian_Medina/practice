///#GLOBALS Lib
Lib.CommonDialog.NextAlert.Reset();
var options = { orderByClause: "LineNumber__ ASC" };
Log.Info("Filling Edit PO Request Form with items from PO");
Lib.Purchasing.EditPORequest.FillForm(options)
    .Then(function () {
    Lib.Purchasing.InitTechnicalFields();
})
    .Catch(function (e) {
    Lib.CommonDialog.NextAlert.Define("_Edit PO Request creation error", e, { isError: true, behaviorName: "EditPORequestInitError" });
});
