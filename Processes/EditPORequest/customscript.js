///#GLOBALS Lib
var TableControl = Controls.LineItems__;
var currentStatus = Data.GetValue("Status__");
var readOnly = Controls.Status__.GetValue() !== "Draft" || !(User.isVendor || Lib.P2P.IsAdmin());
var filterChangedItems = false;
Log.Time("CustomScript");
function UpdateTotalAmountLabel() {
    var currency = Controls.Currency__.GetValue();
    Controls.InfoTotalNetAmount__.SetLabel(Language.Translate("_Info total net amount", false, currency));
}
function UpdateTableLayout() {
    var numberOfQuantityAmountChange = 0;
    var numberOfDeliveryDateChange = 0;
    var numberOfOtherChange = 0;
    var numberOfNoLongerAvailableChange = 0;
    var numberOfNoChange = 0;
    for (var index = 0; index < TableControl.GetItemCount(); index++) {
        var item = TableControl.GetItem(index);
        switch (item.GetValue("RequestedModification__")) {
            case "QuantityAmountChange":
                numberOfQuantityAmountChange++;
                break;
            case "DeliveryDateChange":
                numberOfDeliveryDateChange++;
                break;
            case "NoLongerAvailableChange":
                numberOfNoLongerAvailableChange++;
                break;
            case "Other":
                numberOfOtherChange++;
                break;
            case "NoChange":
            default:
                numberOfNoChange++;
                break;
        }
    }
    TableControl.ProposedDeliveryDate__.Hide(numberOfDeliveryDateChange === 0);
    TableControl.ProposedDeliveryDate__.SetRequired(numberOfDeliveryDateChange > 0);
    TableControl.ProposedQuantity__.Hide(numberOfQuantityAmountChange === 0);
    TableControl.ProposedQuantity__.SetRequired(numberOfQuantityAmountChange > 0);
    TableControl.ProposedUnitPrice__.Hide(numberOfQuantityAmountChange === 0);
    TableControl.ProposedUnitPrice__.SetRequired(numberOfQuantityAmountChange > 0);
    TableControl.ItemComment__.Hide(numberOfNoChange === TableControl.GetItemCount());
    Controls.FilterChangedItems__.Hide(currentStatus === "Draft" || numberOfNoChange === 0);
    CalculateNextTotalNetAmount();
}
function OnRefreshRow(index) {
    var row = TableControl.GetRow(index);
    row.ProposedDeliveryDate__.Hide(true);
    row.ProposedDeliveryDate__.SetReadOnly(true);
    row.ProposedDeliveryDate__.SetRequired(false);
    row.ProposedQuantity__.Hide(true);
    row.ProposedQuantity__.SetReadOnly(true);
    row.ProposedQuantity__.SetRequired(false);
    row.ProposedUnitPrice__.Hide(true);
    row.ProposedUnitPrice__.SetReadOnly(true);
    row.ProposedUnitPrice__.SetRequired(false);
    row.ItemComment__.SetReadOnly(readOnly);
    row.ItemComment__.SetRequired(false);
    row.RemoveRowStyle("background-red");
    switch (row.RequestedModification__.GetValue()) {
        case "QuantityAmountChange":
            row.ProposedQuantity__.Hide(false);
            row.ProposedQuantity__.SetReadOnly(readOnly);
            row.ProposedQuantity__.SetRequired(true);
            row.ProposedUnitPrice__.Hide(false);
            row.ProposedUnitPrice__.SetReadOnly(readOnly);
            row.ProposedUnitPrice__.SetRequired(true);
            break;
        case "Other":
            row.ItemComment__.SetRequired(true);
            break;
        case "DeliveryDateChange":
            row.ProposedDeliveryDate__.Hide(false);
            row.ProposedDeliveryDate__.SetReadOnly(readOnly);
            row.ProposedDeliveryDate__.SetRequired(true);
            break;
        case "NoLongerAvailableChange":
            row.AddRowStyle("background-red");
            break;
        case "NoChange":
        default:
            row.ItemComment__.SetReadOnly(true);
            if (filterChangedItems) {
                row.AddRowStyle("display-none");
            }
            else {
                row.RemoveRowStyle("display-none");
            }
            break;
    }
}
function CheckRequiredFields() {
    Sys.Helpers.Controls.ForEachTableRow(TableControl, function (row) {
        switch (row.RequestedModification__.GetValue()) {
            case "QuantityAmountChange":
                row.ProposedQuantity__.Focus();
                row.ProposedUnitPrice__.Focus();
                row.RequestedModification__.Focus();
                break;
            case "Other":
                row.ItemComment__.Focus();
                row.RequestedModification__.Focus();
                break;
            case "DeliveryDateChange":
                row.ProposedDeliveryDate__.Focus();
                row.RequestedModification__.Focus();
                break;
            case "NoLongerAvailableChange":
            case "NoChange":
            default:
                break;
        }
    });
}
function OnRequestedModificationChange() {
    var row = this.GetRow();
    row.ProposedDeliveryDate__.SetValue(null);
    row.ProposedQuantity__.SetValue(null);
    row.ProposedUnitPrice__.SetValue(null);
    OnRefreshRow(row.GetLineNumber() - 1);
    UpdateTableLayout();
}
function CalculateNextTotalNetAmount() {
    var currency = Data.GetValue("Currency__");
    var nextTotalNetAmount = new Sys.Decimal(0), net, quantity, unitPrice;
    for (var index = 0; index < TableControl.GetItemCount(); index++) {
        var item = TableControl.GetItem(index);
        switch (item.GetValue("RequestedModification__")) {
            case "NoLongerAvailableChange":
                break;
            case "QuantityAmountChange":
                if (currency === item.GetValue("ItemCurrency__")) {
                    quantity = item.IsNullOrEmpty("ProposedQuantity__") ? item.GetValue("ItemQuantity__") : item.GetValue("ProposedQuantity__");
                    unitPrice = item.IsNullOrEmpty("ProposedUnitPrice__") ? item.GetValue("ItemUnitPrice__") : item.GetValue("ProposedUnitPrice__");
                    net = new Sys.Decimal(quantity).mul(unitPrice);
                    if (!Sys.Helpers.IsEmpty(net)) {
                        nextTotalNetAmount = nextTotalNetAmount.add(net);
                    }
                }
                break;
            case "Other":
            case "NoChange":
            case "DeliveryDateChange":
            default:
                if (currency === item.GetValue("ItemCurrency__")) {
                    net = item.GetValue("ItemNetAmount__");
                    if (!Sys.Helpers.IsEmpty(net)) {
                        nextTotalNetAmount = nextTotalNetAmount.add(net);
                    }
                }
                break;
        }
    }
    Controls.NextTotalNetAmount__.SetValue(nextTotalNetAmount.toNumber());
    Controls.NextTotalNetAmount__.Hide(nextTotalNetAmount.toNumber() === Data.GetValue("TotalNetAmount__"));
}
function InitLayout() {
    Sys.Helpers.Banner.SetMainTitle("_Edit PO Request");
    Sys.Helpers.Banner.SetSubTitleAligned(true);
    Sys.Helpers.Banner.SetCentered(false);
    Sys.Helpers.Banner.SetHTMLBanner(Controls.HTMLBanner__);
    Controls.BuyerName__.Hide(!(User.isVendor || Lib.P2P.IsAdmin()));
    Controls.VendorName__.Hide(User.isVendor);
    TableControl.SetWidth("100%");
    TableControl.SetExtendableColumn("ItemDescription__");
    TableControl.OnRefreshRow = OnRefreshRow;
    TableControl.ProposedUnitPrice__.OnChange = CalculateNextTotalNetAmount;
    TableControl.ProposedQuantity__.OnChange = CalculateNextTotalNetAmount;
    TableControl.ItemComment__.SetReadOnly(readOnly);
    TableControl.RequestedModification__.SetReadOnly(readOnly);
    TableControl.RequestedModification__.OnChange = OnRequestedModificationChange;
    Controls.FilterChangedItems__.OnClick = function () {
        filterChangedItems = !filterChangedItems;
        var icon = filterChangedItems ? "fa fa-toggle-on fa-2x" : "fa fa-toggle-off fa-2x";
        Controls.FilterChangedItems__.SetAwesomeClasses(icon);
        Sys.Helpers.Controls.ForEachTableRow(TableControl, function (row, rowIndex) {
            OnRefreshRow(rowIndex);
        });
    };
    Sys.Helpers.Controls.ForEachTableRow(TableControl, function (row, rowIndex) {
        OnRefreshRow(rowIndex);
    });
    switch (currentStatus) {
        case "ToApprove":
            InitApproveStep();
            break;
        case "Approved":
        case "Accepted":
        case "Rejected":
            InitResultStep();
            break;
        case "Draft":
        default:
            InitRequestStep();
            break;
    }
    setTimeout(function () {
        Controls.PreviewPanel.Hide(false);
        Controls.form_content_left.Hide(false);
        Controls.form_content_left.SetSize(40);
    }, 1000);
}
function InitRequestStep() {
    Controls.EditRequestStepButton__.SetTextColor("color8");
    Controls.ApprovalStepButton__.SetTextColor("color9");
    Controls.ResultStepButton__.SetTextColor("color9");
    Controls.SubmitApprove__.SetButtonLabel(Language.Translate("_Submit", true));
    Controls.Cancel__.Hide(false);
    Controls.Quit__.Hide(true);
    Controls.Save__.Hide(false);
    Controls.HelpDescription__.Hide(false);
    Sys.Helpers.Banner.SetSubTitle("_Draft");
}
function InitApproveStep() {
    Controls.EditRequestStepButton__.SetTextColor("color9");
    Controls.ApprovalStepButton__.SetTextColor("color8");
    Controls.ResultStepButton__.SetTextColor("color9");
    Controls.HelpPane.Hide(true);
    Controls.SubmitApprove__.SetButtonLabel(Language.Translate("_Approve", true));
    Controls.Reject__.Hide(false);
    Sys.Helpers.Banner.SetSubTitle("_ToApprove");
}
function InitResultStep() {
    Controls.EditRequestStepButton__.SetTextColor("color9");
    Controls.ApprovalStepButton__.SetTextColor("color9");
    Controls.ResultStepButton__.SetTextColor("color8");
    Controls.HelpPane.Hide(true);
    Controls.SubmitApprove__.Hide(true);
    Controls.HTML__.Hide(true);
    Sys.Helpers.Banner.SetSubTitle(currentStatus === "Approved" ? "_Approved" : "_Rejected");
}
function UpdateLayout() {
    UpdateTotalAmountLabel();
    UpdateTableLayout();
}
function RegisterHandlers() {
    Controls.SubmitApprove__.OnClick = function () {
        var action = currentStatus === "ToApprove" ? "Approve" : "Submit";
        CheckRequiredFields();
        if (Process.ShowFirstError() === null) {
            TableControl.ProposedDeliveryDate__.SetRequired(false);
            TableControl.ProposedQuantity__.SetRequired(false);
            TableControl.ProposedUnitPrice__.SetRequired(false);
            ProcessInstance.Approve(action);
        }
        return false;
    };
    Controls.Reject__.OnClick = function () {
        TableControl.ProposedDeliveryDate__.SetRequired(false);
        TableControl.ProposedQuantity__.SetRequired(false);
        TableControl.ProposedUnitPrice__.SetRequired(false);
        ProcessInstance.Approve("Reject");
        return false;
    };
    Controls.Quit__.OnClick = function () {
        ProcessInstance.Quit("Quit");
        return false;
    };
    Controls.Cancel__.OnClick = function () {
        ProcessInstance.Quit("Cancel");
        return false;
    };
    Controls.Save__.OnClick = function () {
        ProcessInstance.Save("Save");
        return false;
    };
}
InitLayout();
UpdateLayout();
RegisterHandlers();
Log.TimeEnd("CustomScript");
