///#GLOBALS Lib
//#endregion
/** ****************** **/
/** INVOICE COMPLIANCE **/
/** ****************** **/
//#region
var InvoiceCompliance;
(function (InvoiceCompliance)
{
	var complianceInfo = null;

	function Init()
	{
		complianceInfo = Sys.Helpers.TryCallFunction("Lib.AP.Customization.Validation.GetInvoiceComplianceInformation") ||
		{
			reject: false
		};
	}
	InvoiceCompliance.Init = Init;

	function IsCompliant()
	{
		if (!complianceInfo)
		{
			Init();
		}
		return !complianceInfo.reject;
	}
	InvoiceCompliance.IsCompliant = IsCompliant;

	function RejectInvoiceIfNotCompliant()
	{
		var actionName = getActionName();
		var actionType = Data.GetActionType();
		if (!IsCompliant())
		{
			// exclude actionType = reprocess unless the user exit activate the auto-reject for all action types
			if (actionName === "" && (actionType === "" || complianceInfo.forAllActionType))
			{
				Data.SetValue("RejectReason__", complianceInfo.reason ? complianceInfo.reason : "Invoice is not compliant");
				Data.SetValue("Comment__", complianceInfo.message ? complianceInfo.message : Language.Translate("Invoice doesn't match requirement", false));
				actionAutoReject();
				return true;
			}
			Log.Warn("Invoice compliance info is marked as 'reject' but this has been ignored for actionName '" + actionName + "' and actionType '" + actionType + "'");
			TouchlessManager.SetWarning("Disable touchless as invoice is not compliant");
		}
		return false;
	}
	InvoiceCompliance.RejectInvoiceIfNotCompliant = RejectInvoiceIfNotCompliant;
})(InvoiceCompliance || (InvoiceCompliance = {}));
//#endregion
/** ***************** **/
/** TOUCHLESS MANAGER **/
/** ***************** **/
//#region
var TouchlessManager = {
	formInWarning: false,
	SetWarning: function (reason)
	{
		this.formInWarning = true;
		var msg = "Set form in warning";
		if (reason)
		{
			msg += ": ";
			msg += reason;
		}
		Log.Info(msg);
	},
	/** perform touchless post only if touchless is enabled */
	DoTouchlessIfEnabled: function ()
	{
		var touchlessEnabled = Data.GetValue("TouchlessEnabled__");
		if (touchlessEnabled)
		{
			Data.SetValue("EnableTouchless", true);
			// Only execute the touchless in the sendCD action
			if (Data.GetValue("State") === 50)
			{
				// Remove empty lines
				var lineItems = Data.GetTable("LineItems__");
				var itemCount = lineItems.GetItemCount();
				var lineIdx = 0;
				while (lineIdx < itemCount)
				{
					var item = lineItems.GetItem(lineIdx);
					if (!item.GetValue("Amount__") && !item.GetValue("Quantity__"))
					{
						Log.Info("Removing empty line item #" + lineIdx + " before performing touchless");
						itemCount = item.RemoveItem();
					}
					else
					{
						lineIdx++;
					}
				}
				Log.Info("Touchless : Automatic posting");
				Data.SetValue("TouchlessDone__", true);
				actionPost(true);
			}
			return true;
		}
		return false;
	},
	IsDataValid: function ()
	{
		var valid = false;
		var lineItems = Data.GetTable("LineItems__");
		var nbItems = lineItems.GetItemCount();
		if (this.formInWarning)
		{
			Log.Info("Cannot perform touchless because some fields are in warning");
		}
		else if (Data.GetValue("InvoiceAmount__") === 0)
		{
			Log.Info("Cannot perform touchless because invoice amount equals to 0");
		}
		else if (nbItems === 0)
		{
			Log.Info("Cannot perform touchless because there are no line items");
		}
		else
		{
			valid = true;
		}
		return valid;
	},
	/** return true is touchless is possible (doesn't check if touchless is enabled or not) */
	TouchlessIsPossible: function ()
	{
		var invoiceType = Data.GetValue("InvoiceType__");
		if (Data.GetValue("InvoiceStatus__") === Lib.AP.InvoiceStatus.ToVerify &&
			(invoiceType === Lib.AP.InvoiceType.PO_INVOICE ||
				invoiceType === Lib.AP.InvoiceType.PO_INVOICE_AS_FI ||
				(invoiceType === Lib.AP.InvoiceType.NON_PO_INVOICE && Sys.Parameters.GetInstance("AP").GetParameter("EnableTouchlessForNonPoInvoice") === "1")))
		{
			return this.IsDataValid();
		}
		return false;
	},
	TryTouchless: function (actionType)
	{
		if ((actionType === "" || actionType === "reprocess") && this.TouchlessIsPossible())
		{
			Data.SetValue("TouchlessPossible__", true);
			var needValidation = Data.GetValue("NeedValidation");
			if (needValidation)
			{
				Log.Info("NeedValidation=1: touchless is disabled");
			}
			else
			{
				return this.DoTouchlessIfEnabled();
			}
		}
		return false;
	},
	TryAutoPostAfterReview: function ()
	{
		if (Data.GetValue("InvoiceStatus__") === Lib.AP.InvoiceStatus.ToPost && this.IsDataValid())
		{
			Lib.AP.GetInvoiceDocument().Create(true);
			doAutoApprovalOfRecurringInvoiceIfPossible();
			return true;
		}
		return false;
	},
	TryAutoApproveRecurringInvoice: function ()
	{
		if (Data.GetValue("InvoiceStatus__") === Lib.AP.InvoiceStatus.ToApprove && this.IsDataValid())
		{
			Lib.AP.GetInvoiceDocument().Create(true);
			doAutoApprovalOfRecurringInvoiceIfPossible();
			return true;
		}
		return false;
	}
};
//#endregion
/** ***************** **/
/**  BUDGET MANAGER   **/
/** ***************** **/
//#region
var BudgetManager = {
	IsBudgetEnable: Lib.Budget.IsEnabled,
	Updater: Lib.AP.Budget
};
//#endregion
/** *************** **/
/** DATA VALIDATION **/
/** *************** **/
//#region
/**
 * @class An helper class to manage the several kinds of line item
 */
var InvoiceLineItem = {
	/**
	 * Check the data and set warning if they are not valid
	 * @param {Data} item The data of the line to check
	 * @param {integer} index The index of the data
	 */
	CheckWarning: function (item, index, invoiceType)
	{
		if (Lib.P2P.InvoiceLineItem.IsPOLineItem(item) || Lib.P2P.InvoiceLineItem.IsPOGLLineItem(item))
		{
			this.CheckWarningPO(item, index);
		}
		else if (Lib.P2P.InvoiceLineItem.IsUDCLineItem(item))
		{
			this.CheckWarningUDC(item, index);
		}
		if (invoiceType === Lib.AP.InvoiceType.CONSIGNMENT)
		{
			this.CheckWarningConsignmentVendor(item, index);
		}
	},
	/**
	 * Check the data of a vendor on line and set warning if they are not valid
	 * @param {Data} item The data of the line to check
	 * @param {integer} index The index of the data
	 */
	CheckWarningConsignmentVendor: function (item, index)
	{
		if (!Lib.AP.SAP.CheckVendorNumber(item.GetValue("VendorNumber__")))
		{
			TouchlessManager.SetWarning("Vendor number on header doesn't match the line " + index);
		}
	},
	/**
	 * Check the data of an UDC (Unplanned Delivery Cost) line and set warning if a warning is already here (threshold set in extraction script)
	 * Or if a keyword is found without amount.
	 * @param {Data} item The data of the line to check
	 * @param {integer} index The index of the data
	 */
	CheckWarningUDC: function (item, index)
	{
		var Amount = item.GetValue("Amount__");
		if (!Amount)
		{
			TouchlessManager.SetWarning("Extracted UDC Area for line with index " + index + " not defined.");
		}
		else if (item.GetWarning("Amount__"))
		{
			TouchlessManager.SetWarning(item.GetWarning("Amount__") + ". UDC line : " + index + ".");
		}
	},
	/**
	 * Check the data of a PO line and set warning if they are not valid
	 * @param {Data} item The data of the line to check
	 * @param {integer} index The index of the data
	 */
	CheckWarningPO: function (item, index)
	{
		var netAmount = item.GetValue("Amount__");
		//Check warnings: amount > expected or amount > open
		if (item.GetValue("OrderNumber__") && netAmount > 0)
		{
			var openAmount = item.GetValue("OpenAmount__");
			var expectedAmount = item.GetValue("ExpectedAmount__");
			if (netAmount > expectedAmount)
			{
				TouchlessManager.SetWarning("Amount exceeds expected amount line " + index);
			}
			else if (netAmount > openAmount)
			{
				TouchlessManager.SetWarning("Amount exceeds open amount line " + index);
			}
			else
			{
				item.SetWarning("Amount__", "");
			}
		}
		//Check warnings: quantity > expected or quantity > open
		if (item.GetValue("OrderNumber__"))
		{
			var openQuantity = item.GetValue("OpenQuantity__");
			var quantity = item.GetValue("Quantity__");
			var expectedQuantity = item.GetValue("ExpectedQuantity__");
			if (quantity > expectedQuantity)
			{
				TouchlessManager.SetWarning("Quantity exceeds expected amount line " + index);
			}
			else if (quantity > openQuantity)
			{
				TouchlessManager.SetWarning("Quantity exceeds open amount line " + index);
			}
			else
			{
				item.SetWarning("Quantity__", "");
			}
		}
	}
};

function manageStoredinLocalTableFields(storedInLocalTableFields)
{
	for (var field in storedInLocalTableFields.Header)
	{
		if (!storedInLocalTableFields.isStoredInLocalTable(storedInLocalTableFields.Header[field]))
		{
			Data.SetAllowTableValuesOnly(field, false);
		}
	}
	var lineItems = Data.GetTable("LineItems__");
	var nbItems = lineItems.GetItemCount();
	for (var i = 0; i < nbItems; i++)
	{
		var item = lineItems.GetItem(i);
		for (var field in storedInLocalTableFields.LineItems__)
		{
			if (!storedInLocalTableFields.isStoredInLocalTable(storedInLocalTableFields.LineItems__[field]))
			{
				item.SetAllowTableValuesOnly(field, false);
			}
		}
	}
}

function validateBalance()
{
	var balance = Data.GetValue("Balance__");
	var threshold = parseFloat(Variable.GetValueAsString("BalanceThreshold"));
	if (balance === 0)
	{
		Data.SetError("Balance__", "");
	}
	else if (Math.abs(balance) <= threshold)
	{
		TouchlessManager.SetWarning("Balance not null (" + balance + ") but in tolerance.");
	}
	else
	{
		Data.SetError("Balance__", "_Balance is not null");
		Log.Info("Balance is not valid");
		return false;
	}
	Log.Info("Balance is valid");
	return true;
}

function validateHeader(requiredFields)
{
	var isValid = true;
	for (var field in requiredFields.Header)
	{
		if (requiredFields.isRequired(requiredFields.Header[field]) && !Data.GetValue(field) && Data.GetValue(field) !== 0)
		{
			Data.SetError(field, "This field is required!");
			isValid = false;
		}
	}
	if (Data.GetWarning("ExtractedIBAN__"))
	{
		// warning on ExtractedIBAN__ should prevent touchless
		TouchlessManager.SetWarning("Extracted IBAN does not match any of vendor's bank detail");
	}
	if (!isValid)
	{
		Log.Info("Header is not valid");
	}
	return isValid;
}

function validateLineItems(requiredFields)
{
	var isValid = true;
	var invoiceType = Data.GetValue("InvoiceType__");
	var currentRole = Lib.AP.WorkflowCtrl.GetCurrentStepRole();
	// Check if all GL Account are filled
	var lineItems = Data.GetTable("LineItems__");
	var nbItems = lineItems.GetItemCount();
	if (nbItems <= 0)
	{
		isValid = false;
	}
	else if (currentRole === Lib.AP.WorkflowCtrl.roles.apStart &&
		!Data.GetValue("ERPPostingDate__") &&
		invoiceType !== Lib.AP.InvoiceType.NON_PO_INVOICE &&
		(!Lib.ERP.IsSAP() || invoiceType === Lib.AP.InvoiceType.PO_INVOICE_AS_FI))
	{
		// Update PO lines data and check if AP should revalidate them
		isValid = !Lib.AP.PurchaseOrder.UpdateExpectedValuesOnPOLine();
		if (!isValid)
		{
			Log.Info("Line items have been updated from master data, revalidation required.");
		}
	}
	for (var i = 0; i < nbItems; i++)
	{
		var item = lineItems.GetItem(i);
		isValid = validateLineItemsItem(requiredFields, item, i, invoiceType) && isValid;
	}
	Log.Info("LineItems are" + (isValid ? "" : " not") + " valid");
	return isValid;
}

function validateLineItemsItem(requiredFields, item, index, invoiceType)
{
	var isValid = true;
	// Validate all the required fields
	for (var field in requiredFields.LineItems__)
	{
		if (requiredFields.isRequired(requiredFields.LineItems__[field], item) && !item.GetValue(field))
		{
			item.SetError(field, "This field is required!");
			isValid = false;
		}
	}
	// Validate all others fields with more complex validation
	switch (invoiceType)
	{
	case Lib.AP.InvoiceType.NON_PO_INVOICE:
		isValid = Lib.AP.GetInvoiceDocument().ValidateLineItem(item) && isValid;
		break;
	case Lib.AP.InvoiceType.CONSIGNMENT:
	case Lib.AP.InvoiceType.PO_INVOICE:
	case Lib.AP.InvoiceType.PO_INVOICE_AS_FI:
		isValid = Lib.AP.GetInvoiceDocument().ValidateLineItem(item) && isValid;
		InvoiceLineItem.CheckWarning(item, index, invoiceType);
		break;
	default:
		Log.Warn("Unsupported invoice type '" + invoiceType + "'");
		isValid = false;
		break;
	}
	return isValid;
}

function isFormInError(actionName)
{
	if (actionName === "Post")
	{
		// Reset the duplicate check results (the user may have changed values that invalidates the duplicates)
		Variable.SetValueAsString("DuplicateCheckResult", "");
	}
	if (Data.IsFormInError())
	{
		Log.Info("Form is in error");
		return true;
	}
	Log.Info("Form is not in error");
	return false;
}
/** Check if any duplicate invoices exist
 * Fill the variable DuplicateCheckResult with all the duplicates found
 * @return true if no duplicate, false if at least one duplicate found
 */
function duplicateCheck(actionType)
{
	if (actionType !== "" && actionType !== "reprocess")
	{
		/** Check for duplicates only on submission or reprocess - done client side in other cases **/
		return true;
	}
	// Disable DuplicateCheck if invoice was already posted
	if (Data.GetValue("ERPPostingDate__"))
	{
		Log.Info("No duplicate check after posting invoice");
		return true;
	}
	var duplicateKeyControls = ["CompanyCode__", "VendorNumber__"];
	var controlsToCheck = ["InvoiceNumber__", "InvoiceDate__", "InvoiceAmount__"];
	var queryOptions = {
		// 16 months to cover a fiscal year
		MaxDateRangeInDays: 480,
		DateRangeFieldName: "InvoiceDate__",
		SortOnControlName: "InvoiceDate__",
		SearchInArchive: true,
		AdditionalAttributes: ["InvoiceStatus__", "OwnerId"],
		CustomFilter: ""
	};
	var invoiceRef = Lib.AP.ParseInvoiceDocumentNumber(Data.GetValue("InvoiceReferenceNumber__"), true);
	if (invoiceRef && invoiceRef.documentNumber)
	{
		if (!invoiceRef.isFI)
		{
			Log.Info("PO Invoice reference detected: ignore " + Data.GetValue("InvoiceReferenceNumber__") + " from duplicate check");
			queryOptions.CustomFilter += "(!(ERPMMInvoiceNumber__=" + Data.GetValue("InvoiceReferenceNumber__") + "))";
		}
		else
		{
			Log.Info("Non-PO Invoice reference detected: ignore " + Data.GetValue("InvoiceReferenceNumber__") + " from duplicate check");
			queryOptions.CustomFilter += "(!(ERPInvoiceNumber__=" + Data.GetValue("InvoiceReferenceNumber__") + "))";
		}
	}
	var duplicates = Lib.DuplicateCheck.CheckDuplicate(duplicateKeyControls, controlsToCheck, queryOptions, parseInt(Data.GetValue("DuplicateCheckAlertLevel__"), 10));
	if (duplicates.length > 0)
	{
		// Add login of the OwnerId to the AdditionalAttributes
		var cacheOwnerLogin = {};
		for (var _i = 0, duplicates_1 = duplicates; _i < duplicates_1.length; _i++)
		{
			var duplicateItem = duplicates_1[_i];
			var login = cacheOwnerLogin[duplicateItem.additionalAttributes.OwnerId];
			if (!login)
			{
				var owner = Users.GetUser(duplicateItem.additionalAttributes.OwnerId);
				if (owner)
				{
					var ownerVars = owner.GetVars();
					login = ownerVars.GetValue_String("Login", 0);
				}
				else
				{
					Log.Warn("User not found: " + duplicateItem.additionalAttributes.OwnerId);
					login = Language.Translate("User not found", false);
				}
			}
			duplicateItem.additionalAttributes.OwnerLogin = login;
			Log.Info("DuplicateCheckResult : " + JSON.stringify(duplicates));
		}
		Variable.SetValueAsString("DuplicateCheckResult", JSON.stringify(duplicates));
	}
	else
	{
		Variable.SetValueAsString("DuplicateCheckResult", "");
	}
	return duplicates.length === 0;
}

function disableTableValuesOnlyInApprovalWkf(force)
{
	if (force === void 0)
	{
		force = false;
	}
	// No need to check database combo boxes values when data was already posted successfully or when action does not require validation
	if (force || Lib.AP.WorkflowCtrl.GetCurrentStepRole() === Lib.AP.WorkflowCtrl.roles.approver || Data.GetValue("ERPPostingDate__"))
	{
		Sys.Helpers.Data.SetAllowTableValuesOnlyForFields(Lib.AP.UncheckedHeaderFieldsDuringApproval, false, Data);
		Sys.Helpers.Data.SetAllowTableValuesOnlyForColumns("LineItems__", Lib.AP.UncheckedLineItemsFieldsDuringApproval, false);
	}
}
/** Check all Data on the form so that the user can see all errors on post back
 * @param {string} actionName The name of the action the user requested
 * @param {string} actionType The type of button the user clicked
 * @return {Sys.Helpers.IPromise<boolean>} resolve to true if the form is valid (i.e. ready to post)
 */
function validateData(actionName, actionType)
{
	var requiredFields = Lib.AP.GetInvoiceDocument().GetRequiredFields(Sys.Helpers.TryGetFunction("Lib.AP.Customization.Common.GetRequiredFields"));
	manageStoredinLocalTableFields(Lib.AP.GetInvoiceDocument().GetStoredInLocalTableFields());
	var isValid = Data.GetValue("ManualLink__");
	if (!isValid)
	{
		isValid = duplicateCheck(actionType);
		isValid = validateBalance() && isValid;
		isValid = validateHeader(requiredFields) && isValid;
		isValid = validateLineItems(requiredFields) && isValid;
	}
	if (isValid)
	{
		disableTableValuesOnlyInApprovalWkf(false);
		isValid = !isFormInError(actionName);
	}
	var wkfExceptionError = Data.GetError("CurrentException__");
	var wkfException = Data.GetValue("CurrentException__");
	var nextRole = Lib.AP.WorkflowCtrl.GetNextStepRole();
	if (!isValid && !wkfExceptionError && wkfException && nextRole && nextRole !== Lib.AP.WorkflowCtrl.roles.approver)
	{
		// A valid exception is set, and there is someone next in the workflow
		Log.Warn("In exception workflow: ignore errors and continue with the action");
		isValid = true;
	}
	if (!Lib.AP.WorkflowCtrl.workflowUI.ValidateContributors())
	{
		Log.Warn("Invalid contributors detected in workflow");
		isValid = false;
	}
	var invoiceDocument = Lib.AP.GetInvoiceDocument();
	isValid = invoiceDocument.ValidateData(actionName, actionType) && isValid;
	var customIsValid = Sys.Helpers.TryCallFunction("Lib.AP.Customization.Common.OnValidateForm", isValid);
	var promisifiedIsValid;
	if (typeof customIsValid === "boolean")
	{
		promisifiedIsValid = Sys.Helpers.Promise.Resolve(customIsValid);
	}
	else if (Sys.Helpers.Promise.IsPromise(customIsValid))
	{
		promisifiedIsValid = customIsValid;
	}
	else
	{
		promisifiedIsValid = Sys.Helpers.Promise.Resolve(isValid);
	}
	return promisifiedIsValid;
}
//#endregion
/** ****************** 	**/
/** AL FOR LINE ITEMS 	**/
/** ****************** 	**/
//#region
function autoLearnLineItems()
{
	var itemsTable = Data.GetTable("LineItems__");
	var count = itemsTable.GetItemCount();
	var fields = ["OrderNumber__", "DeliveryNote__", "PartNumber__", "UnitPrice__", "Amount__", "Quantity__"];
	// Get Line Items lines
	var lines = getLines(itemsTable, count, fields);
	// Sort by area top to bottom
	lines.sort(function (line1, line2)
	{
		return line1.top - line2.top;
	});
	// Feed the Extracted Line Items table
	var _a = feedExtractedLineItemsTable(lines, fields, count),
		extractedDNArea = _a.extractedDNArea,
		extractedDNValue = _a.extractedDNValue;
	// Feed the Extracted Header Fields
	if (extractedDNArea)
	{
		Data.SetValue("HeaderDNExtracted__", extractedDNArea, extractedDNValue, true);
	}
	else
	{
		Data.SetValue("HeaderDNExtracted__", extractedDNValue, true);
	}
}

function feedExtractedLineItemsTable(lines, fields, count)
{
	var extractedDNArea = null;
	var extractedDNValue = null;
	var extractedLineItems = Data.GetTable("ExtractedLineItems__");
	extractedLineItems.SetItemCount(count);
	for (var i = 0; i < count; i++)
	{
		for (var _i = 0, fields_1 = fields; _i < fields_1.length; _i++)
		{
			var field = fields_1[_i];
			var area = lines[i][field].area;
			var value = lines[i][field].value;
			if (area)
			{
				extractedLineItems.GetItem(i).SetValue(field.replace("__", "Extracted__"), area, value, true);
			}
			else
			{
				extractedLineItems.GetItem(i).SetValue(field.replace("__", "Extracted__"), value, true);
			}
			if (field === "DeliveryNote__")
			{
				if (extractedDNValue === null)
				{
					extractedDNValue = value;
					extractedDNArea = area;
				}
				else if (extractedDNValue !== value)
				{
					extractedDNValue = "";
					extractedDNArea = null;
				}
			}
		}
	}
	return {
		extractedDNArea: extractedDNArea,
		extractedDNValue: extractedDNValue
	};
}

function getLines(table, itemCount, fields)
{
	var lines = [];
	for (var i = 0; i < itemCount; i++)
	{
		var item = table.GetItem(i);
		var line = {
			top: 0
		};
		for (var _i = 0, fields_2 = fields; _i < fields_2.length; _i++)
		{
			var field = fields_2[_i];
			line[field] = {};
			line[field].value = item.GetValue(field);
			line[field].area = item.GetArea(field);
			if (!line.top && line[field].area)
			{
				line.top = line[field].area.y;
			}
		}
		lines.push(line);
	}
	return lines;
}

function saveUDCPositions()
{
	var itemsTable = Data.GetTable("LineItems__"),
		nbItems = itemsTable.GetItemCount();
	var keywordsUpdated = [];
	for (var i = 0; i < nbItems; i++)
	{
		var item = itemsTable.GetItem(i);
		var keyword = item.GetValue("Keyword__");
		if (keyword && Lib.P2P.InvoiceLineItem.IsGLLineItem(item))
		{
			var finalArea = {
					xDiff: 0,
					yDiff: 0,
					width: 0,
					height: 0,
					page: 0
				},
				amountArea = item.GetArea("Amount__"),
				keywordArea = item.GetArea("Keyword__");
			if (amountArea && keywordArea)
			{
				finalArea.xDiff = amountArea.x - keywordArea.x;
				finalArea.yDiff = amountArea.y - keywordArea.y;
				finalArea.width = amountArea.width;
				finalArea.height = amountArea.height;
				finalArea.page = amountArea.page;
				if (!item.IsComputed("Amount__") && keywordsUpdated.indexOf(keyword) === -1)
				{
					var tableName = "P2P - UDC area__",
						companyCode = Data.GetValue("CompanyCode__"),
						vendorNumber = Data.GetValue("VendorNumber__"),
						filter = Sys.Helpers.LdapUtil.FilterAnd(Sys.Helpers.LdapUtil.FilterEqual("CompanyCode__", companyCode), Sys.Helpers.LdapUtil.FilterEqual("VendorNumber__", vendorNumber), Sys.Helpers.LdapUtil.FilterEqual("Keyword__", keyword)).toString(),
						attributes = [
						{
							name: "CompanyCode__",
							value: companyCode
						},
						{
							name: "VendorNumber__",
							value: vendorNumber
						},
						{
							name: "Keyword__",
							value: keyword
						},
						{
							name: "Area__",
							value: JSON.stringify(finalArea)
						}];
					keywordsUpdated.push(keyword);
					Sys.Helpers.Database.AddOrModifyTableRecord(tableName, filter, attributes);
				}
			}
		}
	}
}
//#endregion
/** *************** **/
/** SAVE PARAMETERS **/
/** *************** **/
//#region
function saveTemplate()
{
	Process.PreventApproval();
	Log.Info("Saving coding template '" + Data.GetValue("CodingTemplate__") + "'");
	var invoiceDoc = Lib.AP.GetInvoiceDocument();
	invoiceDoc.SaveTemplate(Lib.AP.Parameters.LineItemsPatternTable.CodingTemplate, Data, Sys.Helpers.Database);
}
//#endregion
/** *************** **/
/** Proof of concept region **/
/** *************** **/
//#region
// FYI : We add a cache to skip query with variable IsARecurringInvoice
// But if you implement the query as owner API to make feature work by using approvers context
// You have to remove this cache to make query every time needed
// And you have to replace QueryAsProcessAdmin with the API implemented QueryAsOwner
function isARecurringInvoice()
{
	var _isARecurringInvoice = Variable.GetValueAsString("IsARecurringInvoice") === "1";
	if (_isARecurringInvoice)
	{
		return true;
	}
	var invoiceDate = Data.GetValue("InvoiceDate__");
	// Compute invoice date to deacrease amount of month
	// Return date computed to local date string
	function computePreviousDate(currentDate, monthToDecrease)
	{
		var resultDate = new Date(currentDate);
		var day = resultDate.getDate();
		resultDate.setDate(1);
		resultDate.setMonth(resultDate.getMonth() + 1 - monthToDecrease);
		resultDate.setDate(0);
		var maxMonthDay = resultDate.getDate();
		if (day < maxMonthDay)
		{
			resultDate.setDate(day);
		}
		return resultDate.toLocaleDateString();
	}
	var invoiceDateArrayToCheck = [
		computePreviousDate(invoiceDate, 2),
		computePreviousDate(invoiceDate, 1)
	];

	function checkAndCleanInvoiceDate(_invoiceDate)
	{
		var indexToRemove = -1;
		for (var i = 0; i < invoiceDateArrayToCheck.length; i++)
		{
			if (invoiceDateArrayToCheck[i] === _invoiceDate.toLocaleDateString())
			{
				indexToRemove = i;
				break;
			}
		}
		if (indexToRemove >= 0)
		{
			invoiceDateArrayToCheck.splice(indexToRemove, 1);
			if (invoiceDateArrayToCheck.length === 0)
			{
				_isARecurringInvoice = true;
			}
		}
	}
	// Build query
	var DBFastAccess = 0x00210000;
	var DBDirtyRead = 0x00020000;
	var query = Process.CreateQueryAsProcessAdmin();
	query.Reset();
	query.SetSpecificTable("CDNAME#Vendor invoice");
	var ldapFilter = Sys.Helpers.LdapUtil.FilterAnd(Sys.Helpers.LdapUtil.FilterEqual("State", "100"), Sys.Helpers.LdapUtil.FilterEqual("VendorNumber__", Data.GetValue("VendorNumber__")), Sys.Helpers.LdapUtil.FilterEqual("InvoiceAmount__", Data.GetValue("InvoiceAmount__")), Sys.Helpers.LdapUtil.FilterEqual("InvoiceCurrency__", Data.GetValue("InvoiceCurrency__")));
	query.SetFilter(ldapFilter.toString());
	query.SetAttributesList("RUIDEX,InvoiceDate__");
	query.SetSortOrder("InvoiceDate__ DESC");
	query.SetOptionEx("Limit=2");
	query.SetOptionEx("FastSearch=1");
	query.SetOptions(DBFastAccess | DBDirtyRead);
	query.SetSearchInArchive(false);
	if (query.MoveFirst())
	{
		var record = query.MoveNext();
		while (record)
		{
			var vars = record.GetUninheritedVars();
			var recordInvoiceDateObj = vars.GetValue_Date("InvoiceDate__", 0);
			checkAndCleanInvoiceDate(recordInvoiceDateObj);
			record = query.MoveNext();
		}
	}
	Variable.SetValueAsString("IsARecurringInvoice", _isARecurringInvoice ? "1" : "0");
	return _isARecurringInvoice;
}
//#endregion
/** ******* **/
/** ACTIONS **/
/** ******* **/
//#region
var headerFieldsListForRecognitionStatistics = [
	"CompanyCode__",
	"InvoiceAmount__",
	"InvoiceCurrency__",
	"InvoiceDate__",
	"InvoiceNumber__",
	"OrderNumber__",
	"VendorName__",
	"VendorNumber__"
];
var lineItemsFieldsListForRecognitionStatistics = [
	"Amount__",
	"Quantity__",
	"TaxAmount__",
	"TaxCode__"
];

function actionByDefault()
{
	if (!InvoiceCompliance.RejectInvoiceIfNotCompliant())
	{
		actionTryTouchless(false);
	}
}

function actionRequestTeaching()
{
	Process.PreventApproval();
	Process.SendToTeaching();
}

function actionReject()
{
	Lib.AP.WorkflowCtrl.DoAction("reject");
}

function actionAutoReject()
{
	Lib.AP.WorkflowCtrl.DoAction("autoReject");
}

function actionSetAside()
{
	Lib.AP.WorkflowCtrl.DoAction("setAside");
	if (Data.GetValue("AsideReason__") === "Waiting for goods receipt")
	{
		checkGoodsReceipt(true);
	}
}

function actionSetOnHold()
{
	Lib.AP.WorkflowCtrl.DoAction("onHold");
}

function doAutoPostAfterReviewIfPossible(roleBeforePost)
{
	if (Sys.Parameters.GetInstance("AP").GetParameter("WorkflowAutoPostAfterReview", "0") === "1" &&
		roleBeforePost === Lib.AP.WorkflowCtrl.roles.controller &&
		Data.GetValue("InvoiceStatus__") === Lib.AP.InvoiceStatus.ToPost)
	{
		Process.RecallScript("autoPostAfterReview");
	}
}

function getRecurrentInvoiceForContributorFilter()
{
	return Sys.Helpers.LdapUtil.FilterAnd(Sys.Helpers.LdapUtil.FilterEqual("CompanyCode__", Data.GetValue("CompanyCode__")), Sys.Helpers.LdapUtil.FilterEqual("VendorNumber__", Data.GetValue("VendorNumber__")), Sys.Helpers.LdapUtil.FilterEqual("InvoiceAmount__", Data.GetValue("InvoiceAmount__")), Sys.Helpers.LdapUtil.FilterEqual("InvoiceCurrency__", Data.GetValue("InvoiceCurrency__")), Sys.Helpers.LdapUtil.FilterEqual("User__", Data.GetValue("OwnerID")), Sys.Helpers.LdapUtil.FilterEqual("NextInvoiceDate__", Sys.Helpers.Date.Date2DBDate(Data.GetValue("InvoiceDate__")))).toString();
}

function updateRecurrentInvoiceNextDate()
{
	var nextDate = Data.GetValue("InvoiceDate__");
	nextDate.setMonth(nextDate.getMonth() + 1);
	Sys.Helpers.Database.ModifyTableRecord("P2P - Recurring Invoice Approval__", getRecurrentInvoiceForContributorFilter(), [
	{
		name: "NextInvoiceDate__",
		value: Sys.Helpers.Date.Date2DBDate(nextDate)
	}]);
}

function isRecurrentInvoiceForCurrentContributor()
{
	var result = {
		isRecurrentInvoice: false,
		shouldAutoApprove: false
	};
	var recordVars = Sys.Helpers.Database.GetFirstRecordResult("P2P - Recurring Invoice Approval__", getRecurrentInvoiceForContributorFilter(), "*");
	if (recordVars)
	{
		result.isRecurrentInvoice = true;
		result.shouldAutoApprove = recordVars.GetValue_Long("AutomaticProcessing__", 0) === 1;
	}
	return result;
}

function doAutoApprovalOfRecurringInvoiceIfPossible()
{
	var currentRole = Lib.AP.WorkflowCtrl.GetCurrentStepRole();
	if (Sys.Parameters.GetInstance("AP").GetParameter("RecurringInvoiceDetection", "0") === "1" &&
		currentRole === Lib.AP.WorkflowCtrl.roles.approver &&
		Data.GetValue("InvoiceStatus__") === Lib.AP.InvoiceStatus.ToApprove)
	{
		var recurrenceInfos = isRecurrentInvoiceForCurrentContributor();
		if (recurrenceInfos.isRecurrentInvoice)
		{
			// invoice is already tagged as recurrent
			// update next invoice date to keep the recuurence mecanism even if not auto approval should be performed
			updateRecurrentInvoiceNextDate();
			if (recurrenceInfos.shouldAutoApprove)
			{
				Process.RecallScript("autoApproveRecurringInvoice");
			}
		}
		else
		{
			// invoice is not tagged as recurrent in table, verify against previous matching invoice
			var isRecurrent = isARecurringInvoice();
			if (isRecurrent)
			{
				Variable.SetValueAsString("RecurringInvoiceAutoValidation", "prompt");
			}
		}
	}
}

function actionPost(touchless)
{
	var roleBeforePost = Lib.AP.WorkflowCtrl.GetCurrentStepRole();
	if (Sys.Parameters.GetInstance("AP").GetParameter("AutolearningOnPOLines") === "1" &&
		roleBeforePost === Lib.AP.WorkflowCtrl.roles.apStart)
	{
		if (Lib.AP.InvoiceType.isPOInvoice())
		{
			autoLearnLineItems();
		}
		else
		{
			// Do not autolearn Non-PO Invoice line items
			Data.GetTable("ExtractedLineItems__").SetItemCount(0);
		}
	}
	saveUDCPositions();
	var erpInvoice = Lib.AP.GetInvoiceDocument();
	var shouldUpdateTables = (Sys.Helpers.TryCallFunction("Lib.AP.Customization.Common.DeactivateLocalPOTableUpdates") !== true) &&
		erpInvoice.ShouldUpdateLocalPOTable() &&
		!Data.GetValue("ERPPostingDate__") &&
		roleBeforePost === Lib.AP.WorkflowCtrl.roles.apStart &&
		Lib.AP.WorkflowCtrl.GetNextStepRole() !== Lib.AP.WorkflowCtrl.roles.controller &&
		Lib.AP.WorkflowCtrl.GetNextStepRole() !== Lib.AP.WorkflowCtrl.roles.apEnd &&
		Lib.AP.InvoiceType.isPOInvoice();
	if (Lib.AP.InvoiceType.isGLInvoice() && Sys.Parameters.GetInstance("AP").GetParameter("APCodingsPrediction") === "1")
	{
		var formDataToWS = Lib.AP.PredictionAPI.GetFormData();
		var options = {
			CompanyCode: Data.GetValue("CompanyCode__"),
			PartnerId: Data.GetValue("VendorNumber__"),
			ValidateData: JSON.stringify(formDataToWS)
		};
		try
		{
			Process.CreateMLDataTableRecord(options);
		}
		catch (e)
		{
			Log.Error("[CreateMLDataTableRecord] " + e);
		}
	}
	erpInvoice.Create(touchless);
	// update local master data as soon as possible when AP post in generic mode
	// In SAP (connected) mode, the ERPPostingError is set in real time, if ERPPostingError is set, an error occured
	// In Generic (not connected) mode, the ERPPostringError is update on ERP Ack and can show a previous error
	if (shouldUpdateTables && (!erpInvoice.IsPostConnected() || !Data.GetValue("ERPPostingError__")))
	{
		Lib.AP.TablesUpdater.Update(false, erpInvoice.ShouldUpdateVendorNumberOnPOHeaderAndItems());
	}
	saveInvoiceAsRecurrent(roleBeforePost);
	doAutoPostAfterReviewIfPossible(roleBeforePost);
	doAutoApprovalOfRecurringInvoiceIfPossible();
	if (roleBeforePost === Lib.AP.WorkflowCtrl.roles.apStart)
	{
		FillModifiedFieldsCounters();
	}
}

function saveInvoiceAsRecurrent(role)
{
	if (Sys.Parameters.GetInstance("AP").GetParameter("RecurringInvoiceDetection", "0") === "1" &&
		role === Lib.AP.WorkflowCtrl.roles.approver)
	{
		var autoValidateRecurrentInvoice = Variable.GetValueAsString("RecurringInvoiceAutoValidation");
		if (autoValidateRecurrentInvoice === "true" || autoValidateRecurrentInvoice === "false")
		{
			var automaticProcessing = autoValidateRecurrentInvoice == "true";
			var nextDate = Data.GetValue("InvoiceDate__");
			nextDate.setMonth(nextDate.getMonth() + 1);
			var filter = Sys.Helpers.LdapUtil.FilterAnd(Sys.Helpers.LdapUtil.FilterEqual("CompanyCode__", Data.GetValue("CompanyCode__")), Sys.Helpers.LdapUtil.FilterEqual("VendorNumber__", Data.GetValue("VendorNumber__")), Sys.Helpers.LdapUtil.FilterEqual("InvoiceAmount__", Data.GetValue("InvoiceAmount__")), Sys.Helpers.LdapUtil.FilterEqual("InvoiceCurrency__", Data.GetValue("InvoiceCurrency__")), Sys.Helpers.LdapUtil.FilterEqual("User__", Data.GetValue("ValidationOwnerID"))).toString();
			Sys.Helpers.Database.AddOrModifyTableRecord("P2P - Recurring Invoice Approval__", filter, [
			{
				name: "CompanyCode__",
				value: Data.GetValue("CompanyCode__")
			},
			{
				name: "VendorNumber__",
				value: Data.GetValue("VendorNumber__")
			},
			{
				name: "InvoiceAmount__",
				value: Data.GetValue("InvoiceAmount__")
			},
			{
				name: "InvoiceCurrency__",
				value: Data.GetValue("InvoiceCurrency__")
			},
			{
				name: "AutomaticProcessing__",
				value: automaticProcessing
			},
			{
				name: "NextInvoiceDate__",
				value: Sys.Helpers.Date.Date2DBDate(nextDate)
			},
			{
				name: "User__",
				value: Data.GetValue("ValidationOwnerID")
			}]);
			Variable.SetValueAsString("RecurringInvoiceAutoValidation", "");
		}
	}
}

function FillModifiedFieldsCounters()
{
	// Fill the fields that counts the fields modifications
	var headerStatistics = Lib.AP.getRecognitionStatisticsFromFieldsList(headerFieldsListForRecognitionStatistics);
	var lineItemsStatistics = Lib.AP.getRecognitionStatisticsFromFieldsListInTable("LineItems__", lineItemsFieldsListForRecognitionStatistics);
	Data.SetValue("ManuallyModifiedFieldsCount__", headerStatistics.ManuallyModifiedFields + lineItemsStatistics.ManuallyModifiedFields);
	Data.SetValue("AutomaticallyModifiedFieldsCount__", headerStatistics.AutomaticallyModifiedFields + lineItemsStatistics.AutomaticallyModifiedFields);
}

function actionAdminListApprove()
{
	var roleBeforePost = Lib.AP.WorkflowCtrl.GetCurrentStepRole();
	var validator = Users.GetUser(Data.GetValue("ValidationOwnerID"));
	var validatorVars = validator.GetVars();
	Data.SetValue("LastValidatorName__", validatorVars.GetValue_String("DisplayName", 0));
	Data.SetValue("LastValidatorUserId__", validatorVars.GetValue_String("Login", 0));
	Data.SetValue("Comment__", Data.GetValue("ValidationMessage"));
	Lib.AP.GetInvoiceDocument().Create(false);
	doAutoPostAfterReviewIfPossible(roleBeforePost);
	doAutoApprovalOfRecurringInvoiceIfPossible();
}

function actionBackToPrevious()
{
	if (Lib.AP.WorkflowCtrl.BackToPreviousPossible())
	{
		Data.SetValue("CurrentAttachmentFlag__", Attach.GetNbAttach());
		Lib.AP.WorkflowCtrl.DoAction("backToPrevious");
	}
	else
	{
		Process.PreventApproval();
	}
}

function actionBackToAP()
{
	var currentStep = Lib.AP.WorkflowCtrl.GetCurrentStepRole();
	if (currentStep === Lib.AP.WorkflowCtrl.roles.controller || currentStep === Lib.AP.WorkflowCtrl.roles.approver)
	{
		Data.SetValue("CurrentAttachmentFlag__", Attach.GetNbAttach());
		Lib.AP.WorkflowCtrl.DoAction("backToAP");
	}
	else
	{
		Process.PreventApproval();
	}
}

function actionAddApprover()
{
	var currentStep = Lib.AP.WorkflowCtrl.GetCurrentStepRole();
	if (currentStep === Lib.AP.WorkflowCtrl.roles.approver || currentStep === Lib.AP.WorkflowCtrl.roles.controller)
	{
		Data.SetValue("CurrentAttachmentFlag__", Attach.GetNbAttach());
		Lib.AP.WorkflowCtrl.DoAction("requestFurtherApproval");
	}
	else
	{
		Process.PreventApproval();
	}
}

function checkGoodsReceipt(keepSavedDateTime)
{
	// Use a critical section in case of several invoice to the same vendor (to check for overinvoicing).
	// Set critical section timeout to a random time between 30 and 120 seconds
	var criticalSectionTimeout = parseInt(Variable.GetValueAsString("updatePOLockTimeout"), 10);
	if (!criticalSectionTimeout)
	{
		criticalSectionTimeout = Math.floor((Math.random() * 90) + 30);
	}
	var criticalSectionID = Data.GetValue("CompanyCode__") + "-" + Data.GetValue("VendorNumber__");
	var success = Process.PreventConcurrentAccess(criticalSectionID, function ()
		{
			// Ensure Elastic Search is up to date for any query
			Process.Sleep(1);
			if (!keepSavedDateTime)
			{
				Data.SetValue("LastSavedDateTime", new Date());
			}
			Lib.AP.WorkflowCtrl.DoAction("checkGoodsReceipt");
			var invoiceDoc = Lib.AP.GetInvoiceDocument();
			invoiceDoc.ReconcileInvoice();
			invoiceDoc.UpdateBalance();
			validateData("approve", "checkgoodsreceipt")
				.Then(function (isValid)
				{
					if (isValid)
					{
						actionTryTouchless(true);
					}
					else
					{
						Log.Info("Error(s) detected in form");
						Process.PreventApproval();
					}
				})
				.Catch(function (error)
				{
					Log.Error("Unhandled promise: " + error + ". Prevent posting.");
					Process.PreventApproval();
				});
		},
		// no recovery function as master data should be synchronized regularly
		null, criticalSectionTimeout);
	if (!success)
	{
		// If there was an error in the process, throw an error to retry this action (if a retry is allowed)
		var errorMsg = "Failed to lock Purchase order items because they are momentarily held by another invoice";
		if (Process.GetScriptRetryCount() < Process.GetScriptMaxRetryCount() - 1)
		{
			var retryMsg = errorMsg + ". Retrying...";
			Log.Warn(retryMsg);
			// Generate an exception for script to retry
			throw retryMsg;
		}
		else
		{
			// The critical section could not be created within the criticalSectionTimeout. Keep the invoice asleep
			Log.Info(errorMsg);
			Process.PreventApproval();
		}
	}
}

function tryForwardUDCExceptionToReviewer()
{
	var nbRemainingControllers = Lib.AP.WorkflowCtrl.GetNbRemainingControllers();
	if (nbRemainingControllers > 0)
	{
		actionPost(true);
		return true;
	}
	return false;
}

function tryForwardNonPoInvoiceToReviewer()
{
	var forwardAutoToReviewerEnabled = Sys.Parameters.GetInstance("AP").GetParameter("AutomaticallyForwardNonPoInvoiceToReviewer");
	if (forwardAutoToReviewerEnabled === "1")
	{
		var nbRemainingControllers = Lib.AP.WorkflowCtrl.GetNbRemainingControllers();
		if (nbRemainingControllers > 0)
		{
			actionPost(true);
			return true;
		}
	}
	return false;
}

function actionTryTouchless(fromCheckGR)
{
	if (Lib.AP.InvoiceType.isGLInvoice() && Sys.Parameters.GetInstance("AP").GetParameter("EnableTouchlessForNonPoInvoice") !== "1")
	{
		if (!tryForwardNonPoInvoiceToReviewer())
		{
			Process.PreventApproval();
		}
	}
	else if (Data.GetValue("CurrentException__") === "UDC over tolerance")
	{
		if (!tryForwardUDCExceptionToReviewer())
		{
			Process.PreventApproval();
		}
	}
	else if (!Data.GetValue("TouchlessDone__") && !TouchlessManager.TryTouchless(fromCheckGR ? "" : Data.GetActionType()))
	{
		Process.PreventApproval();
	}
}

function actionAutoPostAfterReview()
{
	if (Lib.AP.WorkflowCtrl.GetNbRemainingContributorWithRole(Lib.AP.WorkflowCtrl.roles.controller) !== 0 ||
		!TouchlessManager.TryAutoPostAfterReview())
	{
		Process.PreventApproval();
	}
}

function actionAutoApproveRecurringInvoice()
{
	if (!TouchlessManager.TryAutoApproveRecurringInvoice())
	{
		Process.PreventApproval();
	}
}

function continueAfterERPAck()
{
	Lib.AP.WorkflowCtrl.DoAction("continueAfterERPAck");
	doAutoApprovalOfRecurringInvoiceIfPossible();
}

function onHoldExpiration()
{
	Lib.AP.WorkflowCtrl.DoAction("onHoldExpiration");
}
/**
 * Action to call when the invoice expire
 */
function onExpiration()
{
	Lib.AP.WorkflowCtrl.DoAction("onExpiration");
}

function manualUnblockPayment()
{
	Lib.AP.GetInvoiceDocument().SAPUnblockPayment("manual");
}

function retryUnblockPayment()
{
	Lib.AP.GetInvoiceDocument().SAPUnblockPayment("retry");
}

function updateHolds()
{
	Lib.AP.WorkflowCtrl.manageHolds();
	if (!Lib.AP.WorkflowCtrl.IsEnded())
	{
		if (!Data.IsNullOrEmpty("ERPPostingDate__") && Data.IsNullOrEmpty("ERPInvoiceNumber__"))
		{
			Log.Info("Invoice is still waiting for ERP ack");
			Process.WaitForUpdate();
		}
		else
		{
			Process.PreventApproval();
		}
	}
}

function clearingDone()
{
	var invoiceStatus = Data.GetValue("InvoiceStatus__");
	if (invoiceStatus === Lib.AP.InvoiceStatus.WaitForClearing)
	{
		Lib.AP.WorkflowCtrl.DoAction("clearingDone");
	}
	else
	{
		Process.PreventApproval();
	}
}

function clearingTimeout()
{
	var invoiceStatus = Data.GetValue("InvoiceStatus__");
	if (invoiceStatus === Lib.AP.InvoiceStatus.WaitForClearing)
	{
		Lib.AP.WorkflowCtrl.DoAction("clearingTimeout");
	}
	else
	{
		Process.PreventApproval();
	}
}

function contactVendor()
{
	if (Sys.Helpers.IsEmpty(Data.GetValue("PortalRuidEx__")))
	{
		var vendorPortalParams = Lib.AP.VendorPortal.GetParametersFromDataInvoice(Data);
		var fieldsToUpdate = Lib.AP.VendorPortal.GetFieldsToUpdate(vendorPortalParams);
		Lib.AP.VendorPortal.CreateCIAndNotifyVendor(vendorPortalParams, fieldsToUpdate);
	}
	Process.PreventApproval();
	Lib.CommonDialog.NextAlert.Define("ContactVendorDone", null,
	{
		behaviorName: "contactVendor"
	});
}

function actionFullBudgetRecovery()
{
	Lib.AP.Budget.DoFullRecovery();
}

function renewValidityPeriod()
{
	if (Variable.GetValueAsString("IsExtendedValidityPeriod") === "true")
	{
		// Reset the expiration timeout and get out of the "grace period"
		Lib.AP.WorkflowCtrl.ExpirationHelper.ResetValidity(new Date());
		Variable.SetValueAsString("IsExtendedValidityPeriod", "false");
	}
}
//#endregion
/** ******** **/
/** RUN PART **/
/** ******** **/
//#region
function SetTransportSubject()
{
	var subject = Data.GetValue("CompanyCode__") + "/" + Data.GetValue("VendorNumber__") + "/" + Data.GetValue("InvoiceNumber__");
	subject = Sys.Helpers.TryCallFunction("Lib.AP.Customization.Validation.SetTransportSubject", subject) || subject;
	Data.SetValue("Subject", subject);
}

function getActionName()
{
	if (!Process.AutoValidatingOnExpiration())
	{
		return Data.GetActionName();
	}
	if (Data.GetValue("InvoiceStatus__") === Lib.AP.InvoiceStatus.WaitForClearing)
	{
		return "clearingTimeout";
	}
	return "onexpiration";
}

function executeRequestedAction()
{
	var vendorPortalParams;
	var currentName = getActionName();
	var currentNameLowerCase = currentName.toLowerCase();
	var currentAction = Data.GetActionType();
	Data.SetValue("EnableTouchless", false);
	Log.Info("Action: " + (currentAction ? currentAction : "<empty>") + ", Name: " + (currentName ? currentName : "<empty>") + ", Device: " + Data.GetActionDevice());
	// Initialize WorkflowCtrl
	Lib.AP.WorkflowCtrl.Init();
	Lib.AP.WorkflowCtrl.SetObject("allowApprovers", Lib.AP.GetInvoiceDocument().AllowApprovers());
	Lib.AP.WorkflowCtrl.AllowRebuild(false);
	Lib.AP.WorkflowCtrl.SetObject("usersObject", Users);
	Lib.AP.WorkflowCtrl.SetObject("budgetManager", BudgetManager);
	/* Action map to specify what to do according to the Action Name. The validation, if needed, will always be executed synchronously.
	    The action in itself can be executed asynchronously.
	key: action name given by Data.GetActionName(). The empty string key means that no user action was ever performed on this form (submission)
	value : object
	- execute: function to call
	- requireValidation: set to true if you want the action to validate data
	- isAsync: set to true if the action should be executing asynchronously
	- leaveForm (only if isAsync is false): set to true if the form should be closed at the end of the action
	- keepScheduledActionParameters : set to true if you want to keep the values of the fields ScheduledActionDate and ScheduledAction after the validation action.
	- allowRetry: set to true to enable script retries on this specific action
	- renewValidityPeriod: set to true if you want the action to reset the validity date time when invoice is in extended validation period
	*/
	var approveActionMap = {};
	approveActionMap.post = {
		"execute": actionPost,
		"requireValidation": true,
		"InWorkflowLeaveForm": true,
		"allowRetry": true,
		"renewValidityPeriod": true
	};
	approveActionMap.adminlist = {
		"execute": actionAdminListApprove,
		"requireValidation": true,
		"renewValidityPeriod": true
	};
	approveActionMap.backtoprevious = {
		"execute": actionBackToPrevious,
		"requireValidation": false,
		"renewValidityPeriod": true
	};
	approveActionMap.backtoap = {
		"execute": actionBackToAP,
		"requireValidation": false,
		"renewValidityPeriod": true
	};
	approveActionMap.addapprover = {
		"execute": actionAddApprover,
		"requireValidation": false
	};
	approveActionMap.recalltoap = {
		"execute": actionBackToAP,
		"requireValidation": false,
		"renewValidityPeriod": true
	};
	approveActionMap.set_aside = {
		"execute": actionSetAside,
		"requireValidation": false,
		"leaveForm": true,
		"renewValidityPeriod": true
	};
	approveActionMap.onhold = {
		"execute": actionSetOnHold,
		"requireValidation": false,
		"leaveForm": true,
		"keepScheduledActionParameters": true,
		"renewValidityPeriod": true
	};
	approveActionMap.reject = {
		"execute": actionReject,
		"requireValidation": false,
		"leaveForm": true
	};
	approveActionMap.request_teaching = {
		"execute": actionRequestTeaching,
		"requireValidation": false,
		"leaveForm": false,
		"renewValidityPeriod": true
	};
	approveActionMap.continueaftererpack = {
		"execute": continueAfterERPAck,
		"requireValidation": false,
		"leaveForm": false,
		"allowRetry": true
	};
	approveActionMap.onexpiration = {
		"execute": onExpiration,
		"requireValidation": false,
		"leaveForm": false
	};
	approveActionMap.savetemplate = {
		"execute": saveTemplate,
		"requireValidation": false,
		"leaveForm": false
	};
	approveActionMap.checkgoodsreceipt = {
		"execute": checkGoodsReceipt,
		"requireValidation": false,
		"leaveForm": false,
		"allowRetry": true
	};
	approveActionMap.retryunblockpayment = {
		"execute": retryUnblockPayment,
		"requireValidation": false,
		"leaveForm": false
	};
	approveActionMap.manualunblockpayment = {
		"execute": manualUnblockPayment,
		"requireValidation": false,
		"leaveForm": false
	};
	approveActionMap.contactvendor = {
		"execute": contactVendor,
		"requireValidation": false,
		"leaveForm": false
	};
	approveActionMap.autopostafterreview = {
		"execute": actionAutoPostAfterReview,
		"requireValidation": true,
		"leaveForm": false
	};
	approveActionMap.autoapproverecurringinvoice = {
		"execute": actionAutoApproveRecurringInvoice,
		"requireValidation": true,
		"leaveForm": false
	};
	approveActionMap.onholdexpiration = {
		"execute": onHoldExpiration,
		"requireValidation": false,
		"leaveForm": false
	};
	approveActionMap.updateholds = {
		"execute": updateHolds,
		"requireValidation": false,
		"leaveForm": false
	};
	approveActionMap.clearingdone = {
		"execute": clearingDone,
		"requireValidation": false,
		"leaveForm": false
	};
	approveActionMap.clearingTimeout = {
		"execute": clearingTimeout,
		"requireValidation": false,
		"leaveForm": false
	};
	approveActionMap.fullbudgetrecovery = {
		"execute": actionFullBudgetRecovery,
		"requireValidation": false,
		"leaveForm": true
	};
	approveActionMap[""] = {
		"execute": actionByDefault,
		"requireValidation": true,
		"leaveForm": false
	};
	// Add customized actions to approveActionMap if not using the same actionName
	Sys.Helpers.TryCallFunction("Lib.AP.Customization.Validation.ExtendActionMap", approveActionMap);
	var action = approveActionMap[currentNameLowerCase];
	if (!action || currentNameLowerCase === "")
	{
		action = approveActionMap[""];
	}
	var ok = true;
	var autoReject = currentNameLowerCase === "" && !InvoiceCompliance.IsCompliant();
	// First time in validation script
	if (action.requireValidation && !autoReject)
	{
		validateData(currentName, currentAction)
			.Then(function (isValid)
			{
				if (!isValid)
				{
					Log.Info("Error(s) detected in form");
					ok = false;
				}
				else
				{
					SetTransportSubject();
					Lib.P2P.SetBillingInfo(Variable.GetValueAsString("ProcessingLabel"));
					ok = Sys.Helpers.TryCallFunction("Lib.AP.Customization.Validation.OnBilling");
					ok = ok || ok === null;
				}
			})
			.Catch(function (error)
			{
				Log.Error("Unhandled promise: " + error + ". Prevent posting.");
				ok = false;
			});
	}
	else if (currentNameLowerCase === "reject" || autoReject)
	{
		// Set right contract to bill even for rejected invoices
		Sys.Helpers.TryCallFunction("Lib.AP.Customization.Validation.OnBilling");
	}
	else
	{
		disableTableValuesOnlyInApprovalWkf(true);
	}
	if (ok || autoReject)
	{
		if (!action.keepScheduledActionParameters)
		{
			Data.SetValue("ScheduledActionDate__", "");
			Data.SetValue("ScheduledAction__", "");
		}
		if (action.renewValidityPeriod)
		{
			renewValidityPeriod();
		}
		// Execute action now
		vendorPortalParams = Lib.AP.VendorPortal.GetParametersFromDataInvoice(Data);
		Lib.AP.VendorPortal.ValidationScriptBegins(vendorPortalParams);
		var actionContributorIndex = Lib.AP.WorkflowCtrl.workflowUI.GetContributorIndex();
		var actionContributor = null;
		Process.AllowScriptRetries(Boolean(action.allowRetry));
		// The false parameter means this is not a touchless action and is only used for actionPost
		action.execute(false);
		vendorPortalParams = Lib.AP.VendorPortal.GetParametersFromDataInvoice(Data, Variable);
		Lib.AP.VendorPortal.ValidationScriptEnds(vendorPortalParams);
		if (actionContributorIndex >= 0 && actionContributorIndex < Lib.AP.WorkflowCtrl.workflowUI.GetNbContributors())
		{
			actionContributor = Lib.AP.WorkflowCtrl.workflowUI.GetContributorAt(actionContributorIndex);
		}
		var invoiceDoc = Lib.AP.GetInvoiceDocument();
		invoiceDoc.OnValidationActionEnd(currentAction, action ? currentName.toLowerCase() : "", actionContributor);
		Sys.Helpers.TryCallFunction("Lib.AP.Customization.Validation.onActionEnd", currentAction, action ? currentNameLowerCase : "", actionContributor);
		var InWorkflowLeaveFormWithoutPrevent = action.InWorkflowLeaveForm && !Lib.ERP.IsSAP();
		if (action.leaveForm || InWorkflowLeaveFormWithoutPrevent)
		{
			Process.LeaveForm();
		}
	}
	else
	{
		// Either validateData failed or OnBilling did not return true
		// In any case, we have to prevent the approval as the action was not performed
		Process.PreventApproval();
	}
	// Finalize SAP connection
	if (Lib.ERP.IsSAP())
	{
		Lib.AP.SAP.PurchaseOrder.FinalizeBapiParameters();
	}
}

function run()
{
	Lib.AP.InitArchiveDuration();
	//Serialze serializable parmeters on the process so they can be retrieve by the Mobile App
	Sys.Parameters.GetInstance("P2P").Serialize();
	Sys.Parameters.GetInstance("AP").Serialize();
	executeRequestedAction();
	Lib.AP.ResetERPManager();
}
run();
//#endregion