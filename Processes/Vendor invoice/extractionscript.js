///#GLOBALS Lib
//#endregion
/** ***** **/
/** DEBUG **/
/** ***** **/
//#region
// Enable/disable debug mode
var g_debug = false;

function traceDebug(txt)
{
	if (g_debug)
	{
		Log.Info(txt);
	}
}
//#endregion
/** **************** **/
/** Global variables **/
/** **************** **/
//#region
// Mapping between keyword and currency defined in the field InvoiceCurrency__
var g_currencies = {
	"EUR": ["EUR", "€"],
	"USD": ["USD", "$"],
	"GBP": ["GBP", "£"]
};
var g_integerCurrencies = ["JPY"];
//Flags
var g_vendorPreferredInvoiceType = null;
//Areas
var g_CompanyCodeArea = null;
//#endregion
/** ***************** **/
/** CURRENCY HELPERS  **/
/** ***************** **/
//#region
function GetDefaultCurrency()
{
	var companyCode = Data.GetValue("CompanyCode__");
	if (companyCode)
	{
		return Lib.P2P.ExchangeRate.GetCompanyCodeCurrency(companyCode);
	}
	return "";
}

function CurrencyHasDecimal(currency)
{
	return g_integerCurrencies.indexOf(currency) === -1;
}
//#endregion
/** *********************** **/
/** RECOGNITION ENGINE INIT **/
/** *********************** **/
//#region
function GetPatterns(fieldPatterns)
{
	var patterns = Sys.Parameters.GetInstance("AP").GetParameter(fieldPatterns, "");
	if (patterns)
	{
		return patterns.split(";");
	}
	return [];
}

function initAPFirstRecoEngine()
{
	Log.Info("Using AP extraction engine");
	var dateRange = parseFloat(Variable.GetValueAsString("DateRangeFromToday"));
	if (!dateRange)
	{
		dateRange = 3;
	}
	var today = new Date(),
		lowerBound = new Date(),
		upperBound = new Date();
	var dateRangeInDays = dateRange * 365;
	lowerBound.setDate(today.getDate() - dateRangeInDays);
	upperBound.setDate(today.getDate());
	Lib.FirstTimeRecognition.Register("AP", Lib.FirstTimeRecognition.EngineAP, true);
	Lib.FirstTimeRecognition.EngineAP.ActivateLog(g_debug);
	var inv = Lib.FirstTimeRecognition.EngineAP.GetNewDocument();
	var pagesToIgnore = {};
	// Hack to ignore email covers (can happen in some implementations)
	var emailArea = Document.SearchString("Message Body (text):", 0, false, false);
	if (emailArea && emailArea.length > 0)
	{
		pagesToIgnore["0"] = true;
	}
	var orderNumberExtractionSettings = {
		ranges: GetPatterns("OrderNumberPatterns"),
		allPages: true,
		exclusionRanges: null,
		mandatoryKeywords: null,
		exclusionKeywords: ["fax", "phone", "^(?!best)tel"],
		preferredKeywords: null,
		value: null,
		values: []
	};
	var goodIssueExtractionSettings = {
		ranges: GetPatterns("GoodIssueNumberPatterns"),
		allPages: true,
		exclusionRanges: null,
		mandatoryKeywords: null,
		exclusionKeywords: ["fax", "phone", "^(?!best)tel"],
		preferredKeywords: null,
		value: null,
		values: []
	};
	var params = {
		pagesToBeIgnored: pagesToIgnore,
		pageIgnoreThreshold: 6500,
		enableFuzzySearch: false,
		spaceTolerance: 0.9,
		headerFieldsAlignmentTolerance: 0.3,
		amountScopeTolerance: 3,
		amountAlternateScopeWidthTolerance: 0.5,
		amountAlternateScopeHeightTolerance: 13,
		amountAlternateScopeEnableSubsequentPagesSearch: true,
		amountHeaderMargin: 1,
		amountFooterMargin: 0,
		amountAlignmentTolerance: 0.5,
		amountConstraintGrossAmountLocation: true,
		amountPreventGrossAboveTax: false,
		amountCurrencyTolerance: 0.05,
		amountExpectedTaxRates: [2.1, 2.5, 3.8, 5, 5.5, 6, 7, 8, 8.25, 9, 9.5, 10, 12, 13, 13.5, 14, 15, 16, 18, 19, 19.6, 20, 21, 22, 23, 24, 25, 27],
		amountAnchorKeywords: [
			"total a payer", "montant a payer", "net à payer", "net a payer", "netapayer", "total facture", "total t.t.c", "total ttc",
			"montant t.t.c", "montant ttc", "montant eur ttc", "total dû", "net ttc", "total facture ttc", "total net", "t.t.c", "ttc",
			"total amount due", "invoice total", "total amount", "amount due", "total due", "gross value", "amount to pay", "total amt",
			"balance due", "total value", "grand total", "due", "total", "totaal", "amount", "montant", "betrag", "bedrag", "importe",
			"wert", "summe", "gross", "bezahlen", "betalen", "pagar", "payer", "cástka", "wartosc", "zaplaty", "razem", "credit", "debit",
			"payment", "invoice value", "합계금액", "인보이스 금액", "청구금액", "금액", "지급", "차변", "만기", "공급가액", "관세", "세액",
			"請求額", "ご請求額", "御請求額", "合計請求金額", "ご請求金額合計", "合計ご請求金額", "請求総計", "請求総額", "御請求総額", "ご請求総額",
			"御請求金額", "請求金額合計", "合計金額", "価格", "請求金額", "ご請求金額", "ご請求書金額", "支払金額", "合計", "お支払い", "借方", "消費税", "税抜金額", "御売上額",
			"当月請求額", "当月ご請求額", "当月御請求額", "当月請求金額", "当月ご請求金額", "当月御請求金額", "当月御請求残高", "差引御請求額", "今回請求額", "今回御請求額"
		],
		amountExpectedCultures: null,
		amountEnableIntegerExtraction: false,
		amountIntegerValidityThreshold: 1000,
		amountMaximumNumberOfDecimals: 2,
		amountSearchOnCoverPageFirst: false,
		amountGetLastCandidatesFirst: true,
		amountAllowUnplannedCostsDetection: false,
		amountSortTWTByAmountFirst: true,
		amountExtractionMode: Lib.FirstTimeRecognition.EngineAP.ExtractionMode.DEFAULT_EXTRACTION_MODE,
		expectedCurrencies: [
		{
			"US$": "USD"
		},
		{
			"USD": "USD"
		},
		{
			"U.S.D": "USD"
		},
		{
			"$": "USD"
		},
		{
			"EUR": "EUR"
		},
		{
			"€": "EUR"
		},
		{
			"EURO": "EUR"
		},
		{
			"EUROS": "EUR"
		},
		{
			"CAD": "CAD"
		},
		{
			"GBP": "GBP"
		},
		{
			"£": "GBP"
		},
		{
			"SGD": "SGD"
		},
		{
			"SG$": "SGD"
		},
		{
			"S.G.D": "SGD"
		},
		{
			"RMB": "RMB"
		},
		{
			"CNY": "RMB"
		},
		{
			"RM": "MYR"
		},
		{
			"MYR": "MYR"
		},
		{
			"JPY": "JPY"
		},
		{
			"¥": "JPY"
		},
		{
			"円": "JPY"
		},
		{
			"圓": "JPY"
		},
		{
			"￥": "JPY"
		},
		{
			"YEN": "JPY"
		},
		{
			"NT$": "TWD"
		},
		{
			"NTD": "TWD"
		},
		{
			"元": "TWD"
		},
		{
			"KRW": "KRW"
		},
		{
			"₩": "KRW"
		},
		{
			"￦": "KRW"
		}],
		integerCurrencies: g_integerCurrencies,
		// No default currency, AP must choose one
		//"defaultCurrency": Lib.P2P.FirstTimeRecognition_Vendor.cultureManager.GetDefaultCurrency(),
		documentDateHeaderMargin: 0.5,
		documentDatePreferredKeywords: ["請求書の日付", "請求日付", "発行", "発行日", "発行年月日", "請求年月日", "請求日"],
		documentDateExclusionKeywords: ["ste[i1]nga", "order", "due", "pay by", "received", "verval", "echeance", "PO ", "ship", "abgang", "departure", "fulfilment", "zahlungsfrist", "liefer",
			"주문", "만기", "지급일", "수취", "선박", "출발", "이행", "支払日", "振替日", "口座引落日", "注文番号", "お支払い期日", "支払期日", "受取", "船舶", "出発", "易行"
		],
		documentDateExpectedCultures: null,
		documentDateExpectMonthBeforeDate: Data.GetValue("VendorCountry__") === "US",
		documentDateValidityRangeLowerBound: lowerBound,
		documentDateValidityRangeUpperBound: upperBound,
		otherDocumentTypes:
		{
			"KG": [
				"credit note", "gutschrift", "nota di credito", "nota credito", "note de credit", "creditnota", "credit memo", "abono", "avoir n°", "수정세금계산서", "AP Credit"
			]
		},
		defaultDocumentType: "KR",
		documentNumberMandatoryKeywords:
		{
			"KG": [],
			"KR": []
		},
		documentNumberPreferredKeywords:
		{
			"KG": [
				"credit note", "credit memo", "gutschrift", "nota di credito", "nota credito", "note de credit", "creditnota",
				"verg[üu]tung", "abono", "avoir n°", "numéro", " n[°o] ?:?.?$", "수정세금계산서", "AP Credit"
			],
			"KR": [
				"n° de document", "inv number", " n[°o] ?:?$", "invoice(?! date)", "rechnung", "fattura", "facture", "factura(?!tion)",
				"factuur?", "rechn\\.nr", "numéro", "일련번호", "伝票番号", "請求書No.", "請求書コード", "請求番号", "請求書番号", "伝票No."
			]
		},
		documentNumberExclusionKeywords:
		{
			"KG": [
				"commercial", "zahlungsbed", "order", "commande", "account", "vendor", "cust",
				"^(?!invoice) ref", "a[ \\.]?b[ \\.]?n", "v[ \\.]?a[ \\.]?t", "b[ \\.]?t[ \\.]?w",
				"location", "deliv", " po ", "kunden", "adr", "klant", "empf[aä]nger", "client", "date", "payer", "id.?$",
				"주문", "계정", "상호", "주소", "배송", "고객사", "작성일자", "상업",
				"注文番号", "口座", "仕入先", "ベンダー", "住所", "納期", "お客様", "伝票日付", "商業", "〒",
				"電話番号", "お問い合わせ先", "口座番号", "店番号", "普通", "当座. (普)", "(当)"
			],
			"KR": [
				"commercial", "zahlungsbed", "order", "commande", "account", "vendor", "cust",
				"^(?!invoice) ref", "a[ \\.]?b[ \\.]?n", "v[ \\.]?a[ \\.]?t", "b[ \\.]?t[ \\.]?w",
				"location", "deliv", " po ", "kunden", "adr", "klant", "empf[aä]nger", "client", "date", "payer", "id.?$",
				"주문", "계정", "상호", "주소", "배송", "고객사", "작성일자", "상업",
				"注文番号", "口座", "仕入先", "ベンダー", "住所", "納期", "お客様", "伝票日付", "商業", "〒",
				"電話番号", "お問い合わせ先", "口座番号", "店番号", "普通", "当座. (普)", "(当)"
			]
		},
		documentNumberPreventSpaceExtraction: true,
		headerFields:
		{
			"OrderNumber": orderNumberExtractionSettings
		},
		enableLineItemsRecognition: false
	};
	params.computedCurrency = Lib.AP.Extraction.GetComputedTextValue("InvoiceCurrency__");
	params.computedDocumentDate = Lib.AP.Extraction.GetComputedDateValue("InvoiceDate__");
	params.computedGrossAmount = Lib.AP.Extraction.GetComputedAmountValue("InvoiceAmount__");
	params.computedNetAmount = Lib.AP.Extraction.GetComputedAmountValue("ExtractedNetAmount__");
	params.computedDocumentNumber = Lib.AP.Extraction.GetComputedTextValue("InvoiceNumber__");
	if (Sys.Parameters.GetInstance("AP").GetParameter("enableconsignmentstock") === "1")
	{
		params.headerFields.GoodIssueNumber = goodIssueExtractionSettings;
	}
	if (Sys.Parameters.GetInstance("P2P").GetParameter("EnableContractGlobalSetting", "") === "1")
	{
		var contractNumberExtractionSettings = {
			ranges: GetPatterns("ContractNumberPatterns"),
			allPages: true,
			exclusionRanges: null,
			mandatoryKeywords: ["Contract", "Contract No", "Contrat", "No contrat"],
			exclusionKeywords: [],
			preferredKeywords: null,
			value: null,
			values: []
		};
		params.headerFields.ContractNumber = contractNumberExtractionSettings;
	}
	var customParams = Sys.Helpers.TryCallFunction("Lib.AP.Customization.Extraction.GetInvoiceFTRParameters", params);
	inv.InitParameters(customParams ? customParams : params);
	return inv;
}

function initAPv1FirstRecoEngine()
{
	Log.Info("Using APv1 extraction engine");
	Lib.FirstTimeRecognition.Register("APv1", Lib.FirstTimeRecognition.EngineAPv1, true);
	Lib.FirstTimeRecognition.EngineAPv1.ActivateLog(g_debug);
	var inv = Lib.FirstTimeRecognition.EngineAPv1.GetNewDocument();
	// For compatibility with APV1 engine, culture must be set
	var cultureManager = Lib.P2P.FirstTimeRecognition_Vendor.cultureManager;
	var externalCulture = Variable.GetValueAsString("DocumentCulture");
	if (externalCulture)
	{
		cultureManager.SetCurrentDocumentCulture(externalCulture);
	}
	else
	{
		switch (Data.GetValue("CompanyCode__"))
		{
		case "FR01":
			cultureManager.SetCurrentDocumentCulture("fr-FR");
			break;
		case "UK01":
			cultureManager.SetCurrentDocumentCulture("en-GB");
			break;
		case "US01":
			cultureManager.SetCurrentDocumentCulture("en-US");
			break;
		default:
			cultureManager.SetCurrentDocumentCulture("en-US");
			break;
		}
	}
	var params = {
		"currentDocumentCulture": cultureManager.GetCurrentDocumentCulture(),
		"defaultCurrency": cultureManager.GetDefaultCurrency(),
		"expectedCurrencies": g_currencies,
		"totalKeywordsFirstTry": cultureManager.GetTotalKeywordsFirstTry(),
		"totalKeywordsSecondTry": cultureManager.GetTotalKeywordsSecondTry(),
		"invoiceNumberKeywordsFirstTry": cultureManager.GetInvoiceNumberKeywordsFirstTry(),
		"invoiceNumberKeywordsSecondTry": cultureManager.GetInvoiceNumberKeywordsSecondTry(),
		"invoiceNumberRegExp": cultureManager.GetInvoiceNumberRegExp(),
		"invoicePORegExp": cultureManager.GetInvoicePORegExp()
	};
	var customParams = Sys.Helpers.TryCallFunction("Lib.AP.Customization.Extraction.GetInvoiceFTRParameters", params);
	inv.InitParameters(customParams ? customParams : params);
	return inv;
}

function initFirstRecoEngine(engine)
{
	if (!engine)
	{
		engine = "DEFAULT";
	}
	switch (engine.toUpperCase())
	{
	case "APV1":
		return initAPv1FirstRecoEngine();
	case "NONE":
		return null;
		//case "AP":
	default:
		return initAPFirstRecoEngine();
	}
}
//#endregion
/** ******************* **/
/** PREVIEW INTERACTION **/
/** ******************* **/
//#region
function highlightVendorArea(area)
{
	var highlightColorBorder = 0xFFFFFF;
	var highlightColorBackground = 0xFFCC00;
	if (area)
	{
		area.zone.Highlight(true, highlightColorBackground, highlightColorBorder, "VendorName__");
		area.zone.Highlight(true, highlightColorBackground, highlightColorBorder, "VendorNumber__");
		area.zone.Highlight(true, highlightColorBackground, highlightColorBorder, "ExtractedIBAN__");
	}
}

function highlightCompanyCodeArea(area)
{
	var highlightColorBorder = 0xFFFFFF;
	var highlightColorBackground = 0xFFCC00;
	if (area)
	{
		area.Highlight(true, highlightColorBackground, highlightColorBorder, "CompanyCode__");
		g_CompanyCodeArea = area;
	}
}
//#endregion
/** ********************* **/
/** FTS AND QUERY HELPERS **/
/** ********************* **/
//#region
var QueryHelper = {
	/**
	 *	Execute a Query to find all matching records
	 *	@param {string[]} ArrayWordToFind words list
	 *	@param {string} sTableName Table to look into
	 *	@param {string} column column to look into
	 *	@param {callback} validateFunc Check if the record is valid regarding to the text found
	 *		Parameters -> Current record to evaluate, block array
	 *	@param {boolean} useMultipleRecords If true, return every matching record. Otherwise return first match
	 *	@param {string} companyCode If specified a filter on companyCode__ is added to the request
	 *	@param {boolean} ERPConnected define if the ERP is connected or not
	 */
	DoQuery: function (arrayWordToFind, sTableName, column, validateFunc, useMultipleRecords, companyCode, ERPConnected)
	{
		return QueryHelper.BuildAndExecuteQuery(arrayWordToFind, sTableName, column, validateFunc, useMultipleRecords, companyCode, ERPConnected);
	},
	/**
	 *	Execute a FTS query or a simple Query to find all matching records
	 */
	BuildAndExecuteQuery: function (arrayWordToFind, sTableName, column, validateFunc, useMultipleRecords, companyCode, ERPConnected)
	{
		// Validate parameters
		if (!arrayWordToFind || arrayWordToFind.length === 0)
		{
			return null;
		}
		var sWordsToSearch = QueryHelper.EscapeWordToFind(arrayWordToFind);
		if (sWordsToSearch === "")
		{
			Log.Warn("No word to search");
			return null;
		}
		var filter = QueryHelper.BuildFilter(column, arrayWordToFind, companyCode, ERPConnected);
		return QueryHelper.ExecuteQuery(sTableName, filter, useMultipleRecords, validateFunc);
	},
	ExecuteQueryLoopOnResult: function (validateFunc, useMultipleRecords)
	{
		var queryResult = Query.MoveNextRecord();
		var recordList = [];
		while (queryResult)
		{
			var record = QueryHelper.ValidateCurrentRecord(queryResult, validateFunc);
			if (record)
			{
				if (!useMultipleRecords)
				{
					return record;
				}
				recordList.push(record);
			}
			queryResult = Query.MoveNextRecord();
		}
		if (recordList.length > 0)
		{
			return recordList;
		}
		return null;
	},
	ExecuteQuery: function (sTableName, sFilter, useMultipleRecords, validateFunc)
	{
		Query.Reset();
		Query.SetAttributesList("*");
		Query.SetSpecificTable(sTableName);
		Query.SetFilter(sFilter);
		if (Query.MoveFirst())
		{
			var resultRecord = QueryHelper.ExecuteQueryLoopOnResult(validateFunc, useMultipleRecords);
			if (resultRecord && (typeof resultRecord.length === "undefined" || resultRecord.length > 0))
			{
				return resultRecord;
			}
		}
		return null;
	},
	ValidateCurrentRecord: function (queryResult, validateFunc)
	{
		var result = null;
		var record = queryResult.GetVars();
		if (record)
		{
			// Validate this matching record
			if (!validateFunc || validateFunc(record))
			{
				result = record;
			}
			else
			{
				traceDebug("Discarding invalid value");
			}
		}
		return result;
	},
	EscapeWordToFind: function (arrayWordsToFind)
	{
		var sWordsToSearch = "";
		for (var _i = 0, arrayWordsToFind_1 = arrayWordsToFind; _i < arrayWordsToFind_1.length; _i++)
		{
			var wordToFind = arrayWordsToFind_1[_i];
			if (wordToFind.length >= 2 && sWordsToSearch.indexOf("\"" + wordToFind + "\"") === -1)
			{
				var wordEscaped = wordToFind
					.replace("(", "\\(")
					.replace(")", "\\)")
					.replace("*", "\\*");
				if (sWordsToSearch[0] === '"')
				{
					sWordsToSearch += ", ";
				}
				sWordsToSearch += "\"" + wordEscaped + "\"";
			}
			else
			{
				traceDebug("Discarding duplicate or small value:" + wordToFind);
			}
		}
		return sWordsToSearch;
	},
	CheckFilterOptions: function (arrayWordToFind, companyCode)
	{
		var checkOptions = false;
		if (arrayWordToFind)
		{
			checkOptions = arrayWordToFind.length > 0 && arrayWordToFind instanceof Array;
		}
		if (checkOptions && companyCode)
		{
			checkOptions = !(companyCode instanceof Array) && companyCode.length > 0;
		}
		return checkOptions;
	},
	AppendToFilter: function (filter, key, value)
	{
		if (key.length > 0 && value.length > 0)
		{
			return filter + "(" + key + "=" + value + ")";
		}
		return "invalid filter";
	},
	/**
	 * Construct the filter for the Query
	 */
	BuildFilter: function (column, arrayWordToFind, companyCode, ERPConnected)
	{
		var filter = "";
		if (QueryHelper.CheckFilterOptions(arrayWordToFind, companyCode))
		{
			var maxWord = arrayWordToFind.length;
			for (var iWord = 0; iWord < maxWord && filter !== "invalid filter"; iWord++)
			{
				filter = QueryHelper.AppendToFilter(filter, column, arrayWordToFind[iWord]);
			}
			if (filter && filter !== "invalid filter")
			{
				if (maxWord > 1)
				{
					filter = "(|" + filter + ")";
				}
				if (ERPConnected)
				{
					filter = filter.AddNotCreatedInERPFilter();
				}
				if (companyCode && companyCode.length > 0)
				{
					filter = filter.AddCompanyCodeFilter(companyCode);
				}
			}
		}
		else
		{
			filter = "invalid filter";
			Log.Warn("QueryHelper.BuildFilter function call with wrong arguments");
		}
		if (filter === "invalid filter")
		{
			Log.Warn("QueryHelper.BuildFilter function call with wrong arguments");
		}
		traceDebug("Query Filter = " + filter);
		return filter;
	}
};
//#endregion
/** ****************** **/
/** VENDOR RECOGNITION **/
/** ****************** **/
//#region
function searchVendorNumber()
{
	var sVendorNumber = Data.GetValue("VendorNumber__");
	var sVendorName = Data.GetValue("VendorName__");
	var sCompanyCode = Data.GetValue("CompanyCode__");
	if (sVendorNumber || sVendorName)
	{
		var invoiceDoc = Lib.AP.GetInvoiceDocument();
		if (!invoiceDoc.SearchVendorFromPO(sCompanyCode, sVendorNumber, sVendorName, fillVendorFieldsFromQueryResult))
		{
			Data.SetValue("VendorNumber__", "");
			Data.SetValue("VendorName__", "");
		}
	}
}

function demoSampleSetInvoiceNumberFromCCNumber(ccNumber)
{
	var areas = [];
	if (ccNumber === "TH20000")
	{
		areas = Document.SearchString("TRC.PR.2423/2352/02", 0, 1811, 884, 461, 34, false, false);
	}
	else if (ccNumber === "SG10000")
	{
		areas = Document.SearchString("980004652", 0, 1299, 1358, 205, 30, false, false);
	}
	if (areas.length > 0)
	{
		Data.SetValue("InvoiceNumber__", areas[0]);
	}
}

function demoSampleForceCompanyCodeFromFoundVendor(queryResult)
{
	//////////////////////////////
	// DEMO PART
	// Allow override of the CompanyCode from the default configuration in generic ERP
	// but always keep CompanyCode defined as an External Variable
	var companyCodeVariable = Variable.GetValueAsString("CompanyCode");
	var portalCompanyCode = Variable.GetValueAsString("customerInvoiceCompanyCode");
	if (!companyCodeVariable && !portalCompanyCode && Lib.ERP.GetERPName("AP") === "generic")
	{
		// define Demo company codes
		var CompanyCodesList = {
			"SAP":
			{
				"8004":
				{
					"MJ": "1000"
				},
				"8005":
				{
					"London Postmaster": "1000"
				}
			},
			"*":
			{
				"MJ1186":
				{
					"MJ": "US01"
				},
				"DUR0005":
				{
					"Durial Bureautique": "FR01"
				},
				"10000":
				{
					"London Postmaster": "UK01"
				},
				"TH10000":
				{
					"*": "TH01"
				},
				"TH20000":
				{
					"*": "TH01"
				},
				"MY10000":
				{
					"*": "MY01"
				},
				"MY20000":
				{
					"*": "MY01"
				},
				"SG10000":
				{
					"*": "SG01"
				},
				"SG20000":
				{
					"*": "SG01"
				}
			}
		};
		var ccNumber = queryResult.GetValue_String("Number__", 0);
		var ccName = queryResult.GetValue_String("Name__", 0);
		var erpKey = Lib.ERP.IsSAP() ? "SAP" : "*";
		var companyCode = null;
		if (CompanyCodesList[erpKey][ccNumber])
		{
			companyCode = CompanyCodesList[erpKey][ccNumber][ccName] || CompanyCodesList[erpKey][ccNumber]["*"] || null;
		}
		if (!Lib.ERP.IsSAP())
		{
			demoSampleSetInvoiceNumberFromCCNumber(ccNumber);
		}
		if (companyCode)
		{
			Log.Info("Demo sample : Force company code to " + companyCode);
			Data.SetValue("CompanyCode__", companyCode);
			Data.SetWarning("CompanyCode__", "");
			if (g_CompanyCodeArea !== null)
			{
				g_CompanyCodeArea.Highlight(false, 0, 0, "CompanyCode__");
			}
		}
	}
	//////////////////////////////
}

function fillVendorFieldsFromQueryResult(queryResult, lookupValue, desc)
{
	demoSampleForceCompanyCodeFromFoundVendor(queryResult);
	var vendorNumber = queryResult.GetValue_String("Number__", 0);
	Data.SetValue("VendorNumber__", vendorNumber);
	Data.SetValue("VendorName__", queryResult.GetValue_String("Name__", 0));
	Data.SetValue("VendorStreet__", queryResult.GetValue_String("Street__", 0));
	Data.SetValue("VendorPOBox__", queryResult.GetValue_String("PostOfficeBox__", 0));
	Data.SetValue("VendorCity__", queryResult.GetValue_String("City__", 0));
	Data.SetValue("VendorZipCode__", queryResult.GetValue_String("PostalCode__", 0));
	Data.SetValue("VendorRegion__", queryResult.GetValue_String("Region__", 0));
	Data.SetValue("VendorCountry__", queryResult.GetValue_String("Country__", 0));
	var customFields = Sys.Helpers.TryCallFunction("Lib.AP.Customization.Common.GetVendorCustomFields");
	if (customFields)
	{
		Sys.Helpers.Array.ForEach(customFields, function (field)
		{
			Data.SetValue(field.nameInForm, queryResult.GetValue_String(field.nameInTable, 0));
		});
	}
	// Set preferred invoice type only when invoice type has not been taught or autolearned
	if (!Data.IsComputed("InvoiceType__"))
	{
		var preferredInvoiceType = queryResult.GetValue_String("PreferredInvoiceType__", 0);
		if (preferredInvoiceType && preferredInvoiceType.length > 0)
		{
			g_vendorPreferredInvoiceType = preferredInvoiceType;
		}
	}
	// Set payment terms
	var paymentTerms = queryResult.GetValue_String("PaymentTermCode__", 0);
	Data.SetValue("PaymentTerms__", paymentTerms);
	if (Lib.ERP.IsSAP())
	{
		var withholdingTax = Sys.Parameters.GetInstance("AP").GetParameter("TaxesWithholdingTax", "");
		if (withholdingTax === "Basic" || withholdingTax === "Extended" || !paymentTerms)
		{
			// Retrieve all vendor details from SAP
			Log.Info("Vendor data incomplete in P2P - Vendors table, retrieve vendor data from ESK.SAP.");
			Lib.AP.SAP.GetVendorDetailsFromSAP(Data.GetValue("CompanyCode__"), vendorNumber, "");
		}
	}
	Lib.AP.Extraction.FillVendorContactEmail();
	// Set extracted IBAN
	selectBankDetailsFromIBAN(lookupValue, desc);
}

function selectBankDetailsFromIBAN(lookupValue, desc)
{
	var lookupResults = Lib.P2P.FirstTimeRecognition_Vendor.GetLookupResults();
	//is vendor identified by IBAN
	if (desc === "by IBAN" && lookupValue)
	{
		Data.SetValue("ExtractedIBAN__", lookupValue);
		// Try to select bank account against extracted IBAN
		var params = {
			companyCode: Data.GetValue("CompanyCode__"),
			vendorNumber: Data.GetValue("VendorNumber__"),
			iban: lookupValue
		};
		Lib.AP.GetInvoiceDocument().SelectBankDetailsFromIBAN(params, function (result)
		{
			if (!result && !Sys.Helpers.TryCallFunction("Lib.AP.Customization.Common.SetAlertWhenExtractedIBANDoesNotMatch"))
			{
				Data.SetWarning("ExtractedIBAN__", "_Extracted IBAN does not match");
			}
		});
	}
	// vendor identified without IBAN, does IBAN has been found on document ?
	else if (lookupResults["by IBAN"])
	{
		var highlightColor_Border = 0xFFFFFF;
		var highlightColor_Background = 0xFFCC00;
		Data.SetValue("ExtractedIBAN__", lookupResults["by IBAN"].value);
		if (lookupResults["by IBAN"].zone)
		{
			lookupResults["by IBAN"].zone.Highlight(true, highlightColor_Background, highlightColor_Border, "ExtractedIBAN__");
		}
		if (!Sys.Helpers.TryCallFunction("Lib.AP.Customization.Common.SetAlertWhenExtractedIBANDoesNotMatch"))
		{
			Data.SetWarning("ExtractedIBAN__", "_Extracted IBAN does not match");
		}
	}
}
//#endregion
/** ******************** **/
/** HEADER FIELDS SEARCH **/
/** ******************** **/
//#region
function readFirstRecoResults(inv)
{
	Lib.AP.Extraction.SetFirstRecoValue("InvoiceDate__", inv.GetDocumentDate());
	Lib.AP.Extraction.SetFirstRecoValue("InvoiceAmount__", inv.GetGrossAmount(), "0.00");
	Lib.AP.Extraction.SetFirstRecoValue("ExtractedNetAmount__", inv.GetNetAmount(), inv.GetGrossAmount());
	Lib.AP.Extraction.SetFirstRecoValue("InvoiceNumber__", inv.GetDocumentNumber());
	Lib.AP.Extraction.SetFirstRecoValue("InvoiceCurrency__", inv.GetCurrency());
	if (hasMapperDocument())
	{
		Lib.AP.Extraction.SetFirstRecoValue("DueDate__", inv.GetDueDate());
	}
	var invoiceDoc = Lib.AP.GetInvoiceDocument();
	validateInvoiceCurrency();
	updateExchangeRate();
	var orderNumberCandidates = invoiceDoc.FormatOrderNumberCandidates(inv.GetHeaderFieldCandidates("OrderNumber"));
	if (Lib.ERP.IsSAP())
	{
		if (!searchGoodIssueNumbersAndFillLines(inv.GetHeaderFieldCandidates("GoodIssueNumber")))
		{
			searchOrderNumbersAndFillLines(orderNumberCandidates);
		}
		if (hasMapperDocument())
		{
			tryQueryBusinessValueFromMRU(Data.GetValue("CompanyCode__"), "SAPPaymentMethod__", inv.GetPaymentMethod());
		}
	}
	else
	{
		invoiceDoc.ValidateOrdersAndFillPO(orderNumberCandidates, QueryHelper, searchVendorNumber, null, false, reconcileInvoiceWithPO);
	}
	if (Sys.Parameters.GetInstance("P2P").GetParameter("EnableContractGlobalSetting", "") === "1")
	{
		if (Data.IsNullOrEmpty("ContractNumber__"))
		{
			var contractNumberCandidates = inv.GetHeaderFieldCandidates("ContractNumber");
			searchContractNumbersAndSetValue(contractNumberCandidates);
		}
	}
	if (Lib.AP.InvoiceType.isGLInvoice() && !Data.IsNullOrEmpty("ExtractedNetAmount__"))
	{
		invoiceDoc.FillGLLines();
	}
	if (hasMapperDocument() && inv.GetCalculateTax() === false)
	{
		Data.SetValue("CalculateTax__", false);
	}
	// Unsupported fields
	if (inv.GetUnplannedCosts() !== null)
	{
		traceDebug("FTR - UnplannedCosts found: " + JSON.stringify(inv.GetUnplannedCosts()));
	}
	if (inv.GetTaxAmount() !== null)
	{
		traceDebug("FTR - TaxAmount found: " + JSON.stringify(inv.GetTaxAmount()));
	}
	if (inv.GetTaxRate() !== null)
	{
		traceDebug("FTR - TaxRate found: " + JSON.stringify(inv.GetTaxRate()));
	}
	if (inv.GetDocumentType() !== null)
	{
		adaptSignByInvoiceType(inv.GetDocumentType() === "KG");
	}
}

function searchHeaderFields()
{
	traceDebug("Search header fields");
	// Call Synergy Neural Network API
	if (Sys.Parameters.GetInstance("P2P").GetParameter("EnableSynergyNeuralNetworkGlobalSetting") === "1" && !hasMapperDocument())
	{
		Log.Info("Using Synergy Neural Network");
		var options = {
			GetDefaultCurrency: GetDefaultCurrency,
			CurrencyHasDecimal: CurrencyHasDecimal
		};
		var customOptions = Sys.Helpers.TryCallFunction("Lib.AP.Customization.Extraction.SetSynergyPostProcessingOptions", options);
		Lib.AP.SynergyNeuralNetwork.FillHeaderFields(customOptions ? customOptions : options);
	}
	// Init recognition engine
	if (Process.GetScriptRetryCount() > 1)
	{
		Variable.SetValueAsString("AlertExtractionScriptTimeout", "NoFTR");
		if (Data.GetValue("InvoiceCurrency__"))
		{
			// The invoice currency can be set by the teaching/autolearning, validate the value and
			// correctly set the local currency and the exchange rate.
			validateInvoiceCurrency();
			updateExchangeRate();
		}
	}
	else
	{
		var engine = null;
		if (hasMapperDocument())
		{
			engine = Lib.AP.MappingManager.CurrentMapperDocument;
		}
		else
		{
			engine = initFirstRecoEngine(Variable.GetValueAsString("ExtractionEngine"));
		}
		if (engine)
		{
			// Parse document
			engine.Run();
			// Read results
			readFirstRecoResults(engine);
			if (!Data.GetValue("RecognitionMethod"))
			{
				Data.SetValue("RecognitionMethod", "FTR");
			}
		}
	}
}

function getPONumbersFromDN(params, deliveryNotes, poNumbersFromDN, extactedItem, deliveryNote, poNumber)
{
	var poNumbers = [];
	try
	{
		poNumbers = Lib.AP.SAP.PurchaseOrder.GetPONumberFromDeliveryNote(params, deliveryNote);
	}
	catch (err)
	{
		Log.Error("Lib.AP.SAP.PurchaseOrder.GetPONumberFromDeliveryNote BAPI call error: " + err);
	}
	if (!poNumber && poNumbers.length === 1)
	{
		poNumber = poNumbers[0];
		extactedItem.SetValue("OrderNumberExtracted__", poNumber);
	}
	for (var _i = 0, poNumbers_1 = poNumbers; _i < poNumbers_1.length; _i++)
	{
		var poNumberItem = poNumbers_1[_i];
		if (poNumbersFromDN[poNumberItem])
		{
			poNumbersFromDN[poNumberItem].deliveryNotes.push(deliveryNote);
		}
		else
		{
			var poNumberFromDN = {
				"deliveryNotes": [deliveryNote]
			};
			poNumbersFromDN[poNumberItem] = poNumberFromDN;
		}
	}
	deliveryNotes[deliveryNote] = poNumber;
}

function searchPONumbersFromDN()
{
	var deliveryNotes = {};
	var poNumbersFromDN = {};
	var extractionTable = Data.GetTable("ExtractedLineItems__");
	for (var i = 0; i < extractionTable.GetItemCount(); i++)
	{
		var extactedItem = extractionTable.GetItem(i);
		var deliveryNote = extactedItem.GetValue("DeliveryNoteExtracted__");
		var poNumber = extactedItem.GetValue("OrderNumberExtracted__");
		if (deliveryNote)
		{
			if (!deliveryNotes[deliveryNote])
			{
				var params = Lib.AP.SAP.PurchaseOrder.GetBapiParameters();
				if (!params)
				{
					Log.Error("extractionscript searchPONumbersFromDN - Failed to initialize BAPI parameters");
					return {};
				}
				getPONumbersFromDN(params, deliveryNotes, poNumbersFromDN, extactedItem, deliveryNote, poNumber);
			}
			else if (!poNumber)
			{
				extactedItem.SetValue("OrderNumberExtracted__", deliveryNotes[deliveryNote]);
			}
		}
	}
	return poNumbersFromDN;
}

function addOrderNumberCandidate(extendedPOSearch, area, orderNumbersCandidates, orderNumber)
{
	if (Array.isArray(extendedPOSearch))
	{
		for (var _i = 0, extendedPOSearch_1 = extendedPOSearch; _i < extendedPOSearch_1.length; _i++)
		{
			var extendedPO = extendedPOSearch_1[_i];
			var extendedOrderNumberCandidate = {
				area: area,
				standardStringValue: extendedPO
			};
			orderNumbersCandidates.push(extendedOrderNumberCandidate);
		}
	}
	else if (extendedPOSearch !== orderNumber)
	{
		var extendedOrderNumberCandidate = {
			area: area,
			standardStringValue: extendedPOSearch
		};
		orderNumbersCandidates.push(extendedOrderNumberCandidate);
	}
}

function validateExtractedPO(poNumbersFromDN, orderNumbersCandidates)
{
	if (!orderNumbersCandidates)
	{
		return false;
	}
	var params;
	var POFound = false;
	var poDetails = null;
	var orderNumbers = [];
	var candidateIdx = 0;
	// loop through candidates remove duplicates
	var capturedOrderNumbersCandidatesNb = orderNumbersCandidates.length;
	while (candidateIdx < orderNumbersCandidates.length)
	{
		POFound = false;
		var orderNumberCandidate = orderNumbersCandidates[candidateIdx];
		var orderNumber = orderNumberCandidate.standardStringValue;
		if (orderNumber && candidateIdx < capturedOrderNumbersCandidatesNb)
		{
			var extendedPOSearch = Sys.Helpers.TryCallFunction("Lib.AP.Customization.Common.PurchaseOrder.GetExtendedPOSearchCriteria", orderNumber);
			if (extendedPOSearch)
			{
				addOrderNumberCandidate(extendedPOSearch, orderNumberCandidate.area, orderNumbersCandidates, orderNumber);
			}
		}
		if (orderNumbers.indexOf(orderNumber) === -1)
		{
			orderNumbers.push(orderNumber);
			params = Lib.AP.SAP.PurchaseOrder.GetBapiParameters();
			if (params)
			{
				try
				{
					poDetails = Lib.AP.SAP.PurchaseOrder.GetDetails(params, orderNumber);
				}
				catch (err)
				{
					Log.Error("Lib.AP.SAP.PurchaseOrder.GetDetails BAPI call error: " + err);
				}
			}
			else
			{
				Log.Error("extractionscript validateExtractedPO - Failed to initialize BAPI parameters");
			}
			if (poDetails)
			{
				POFound = true;
				orderNumberCandidate.poDetails = poDetails;
				orderNumberCandidate.deliveryNotes = [];
				if (poNumbersFromDN[orderNumber])
				{
					orderNumberCandidate.deliveryNotes = poNumbersFromDN[orderNumber].deliveryNotes;
					poNumbersFromDN[orderNumber].added = true;
				}
			}
			else
			{
				Log.Info("- discarded PO# " + orderNumber + " (not found in SAP).");
			}
		}
		if (POFound)
		{
			candidateIdx++;
		}
		else
		{
			orderNumbersCandidates.splice(candidateIdx, 1);
		}
	}
	if (orderNumbersCandidates.length > 0)
	{
		POFound = true;
		for (var _i = 0, orderNumbersCandidates_1 = orderNumbersCandidates; _i < orderNumbersCandidates_1.length; _i++)
		{
			var orderNumbersCandidate = orderNumbersCandidates_1[_i];
			var highlightColor_Border = 0xFFFFFF;
			var highlightColor_Background = 0xFFCC00;
			// Set order number
			Lib.AP.AddOrderNumber(orderNumbersCandidate.standardStringValue, orderNumbersCandidate.area);
			var vendorFoundOnThisPO = false;
			var poVendorNumber = orderNumbersCandidate.poDetails.PO_HEADER.GetValue("VENDOR");
			if (!Data.GetValue("VendorNumber__") && poVendorNumber)
			{
				Log.Info("Set vendor according to PO number: " + poVendorNumber);
				Data.SetValue("VendorNumber__", Sys.Helpers.String.SAP.TrimLeadingZeroFromID(poVendorNumber));
				// Call searchVendorNumber to fill vendor fields from number
				searchVendorNumber();
				vendorFoundOnThisPO = true;
			}
			// Add corresponding line items
			Lib.AP.SAP.PurchaseOrder.FillLinesFromPO(orderNumbersCandidate.standardStringValue, orderNumbersCandidate.poDetails, orderNumbersCandidate.deliveryNotes);
			// Highlight order numbers on the document
			if (orderNumbersCandidate.area && vendorFoundOnThisPO)
			{
				orderNumbersCandidate.area.Highlight(true, highlightColor_Background, highlightColor_Border, "VendorNumber__");
				orderNumbersCandidate.area.Highlight(true, highlightColor_Background, highlightColor_Border, "VendorName__");
			}
		}
	}
	else
	{
		Log.Info("No valid purchase order found in ESK.SAP...");
	}
	return POFound;
}

function searchInvoicePOFromTeaching(poNumbersFromDN)
{
	Log.Info("Order number was taught, fill in line items from reference table...");
	var params = Lib.AP.SAP.PurchaseOrder.GetBapiParameters();
	if (!params)
	{
		Log.Error("extractionscript searchInvoicePOFromTeaching - Failed to initialize BAPI parameters");
		return false;
	}
	var poDetails = null;
	var POFound = false;
	var orderNumber = Lib.AP.GetFirstOrderNumber();
	try
	{
		poDetails = Lib.AP.SAP.PurchaseOrder.GetDetails(params, orderNumber);
	}
	catch (err)
	{
		Log.Error("Lib.AP.SAP.PurchaseOrder.GetDetails BAPI call error: " + err);
		throw err;
	}
	if (poDetails)
	{
		var deliveryNotes = [];
		if (poNumbersFromDN[orderNumber])
		{
			deliveryNotes = poNumbersFromDN[orderNumber].deliveryNotes;
			poNumbersFromDN[orderNumber].added = true;
		}
		POFound = true;
		Lib.AP.SAP.PurchaseOrder.FillLinesFromPO(orderNumber, poDetails, deliveryNotes);
	}
	return POFound;
}

function searchInvoicePOFromDN(poNumbersFromDN)
{
	var params;
	var poDetails = null;
	var POFound = false;
	for (var orderNumber in poNumbersFromDN)
	{
		if (Object.hasOwnProperty.call(poNumbersFromDN, orderNumber) && !poNumbersFromDN[orderNumber].added)
		{
			POFound = true;
			params = Lib.AP.SAP.PurchaseOrder.GetBapiParameters();
			if (!params)
			{
				Log.Error("extractionscript searchInvoicePOFromDN - Failed to initialize BAPI parameters");
				return false;
			}
			try
			{
				poDetails = Lib.AP.SAP.PurchaseOrder.GetDetails(params, orderNumber);
			}
			catch (err)
			{
				Log.Error("Lib.AP.SAP.PurchaseOrder.GetDetails BAPI call error: " + err);
			}
			if (poDetails)
			{
				Lib.AP.SAP.PurchaseOrder.FillLinesFromPO(orderNumber, poDetails, poNumbersFromDN[orderNumber].deliveryNotes);
			}
			poDetails = null;
		}
	}
	return POFound;
}

function addGoodIssueIfNotComputed(goodIssueCandidates)
{
	if (!Data.IsComputed("GoodIssue__") && goodIssueCandidates)
	{
		// Warn user that we will not fetch the PO Lines
		Variable.SetValueAsString("AlertExtractionScriptTimeout", "NoPOLineDetails");
		for (var _i = 0, goodIssueCandidates_1 = goodIssueCandidates; _i < goodIssueCandidates_1.length; _i++)
		{
			var goodIssueCandidate = goodIssueCandidates_1[_i];
			Lib.AP.AddGoodIssue(goodIssueCandidate.standardStringValue, goodIssueCandidate.area);
		}
	}
}

function searchGoodIssueNumbersAndFillLines(goodIssueCandidates)
{
	var GIFound = false;
	if (Sys.Parameters.GetInstance("AP").GetParameter("enableconsignmentstock") === "1")
	{
		if (Process.GetScriptRetryCount() !== 0)
		{
			// Do not attempt to get PO from SAP if we are in retry mode
			Log.Warn("Script in retry: bypass GetDetails from PO");
			// Set order number to help the user in the manual reconciliation
			addGoodIssueIfNotComputed(goodIssueCandidates);
		}
		else
		{
			GIFound = Lib.AP.SAP.Consignment.ValidateExtractedGoodIssues(goodIssueCandidates, searchVendorNumber);
			if (GIFound)
			{
				Data.SetValue("InvoiceType__", Lib.AP.InvoiceType.CONSIGNMENT);
			}
		}
	}
	return GIFound;
}

function searchOrderNumbersAndFillLines(orderNumbersCandidates)
{
	//Read in local table before SAP
	var invoiceDoc = Lib.AP.GetInvoiceDocument();
	var POFound = invoiceDoc.ValidateOrdersAndFillPO(orderNumbersCandidates, QueryHelper, searchVendorNumber, "POGL", Lib.ERP.IsSAP());
	if (POFound)
	{
		var invoiceType = Lib.AP.InvoiceType.PO_INVOICE;
		if (Lib.ERP.IsSAP())
		{
			invoiceType = Lib.AP.InvoiceType.PO_INVOICE_AS_FI;
			convertLineTypeToPOGL();
		}
		Data.SetValue("InvoiceType__", invoiceType);
	}
	else if (Process.GetScriptRetryCount() !== 0)
	{
		// Do not attempt to get PO from SAP if we are in retry mode
		Log.Warn("Script in retry: bypass GetDetails from PO");
		// Set order number to help the user in the manual reconciliation
		if (!Data.IsComputed("OrderNumber__") && orderNumbersCandidates)
		{
			// Warn user that we will not fetch the PO Lines
			Variable.SetValueAsString("AlertExtractionScriptTimeout", "NoPOLineDetails");
			for (var _i = 0, orderNumbersCandidates_2 = orderNumbersCandidates; _i < orderNumbersCandidates_2.length; _i++)
			{
				var orderNumbersCandidate = orderNumbersCandidates_2[_i];
				Lib.AP.AddOrderNumber(orderNumbersCandidate.standardStringValue, orderNumbersCandidate.area);
			}
		}
	}
	else
	{
		var poNumbersFromDN = searchPONumbersFromDN();
		if (!Data.IsComputed("OrderNumber__"))
		{
			POFound = validateExtractedPO(poNumbersFromDN, orderNumbersCandidates);
		}
		else
		{
			POFound = searchInvoicePOFromTeaching(poNumbersFromDN);
		}
		POFound = POFound || searchInvoicePOFromDN(poNumbersFromDN);
		if (POFound)
		{
			Data.SetValue("InvoiceType__", Lib.AP.InvoiceType.PO_INVOICE);
		}
	}
	if (POFound)
	{
		reconcileInvoiceWithPO();
	}
}

function convertLineTypeToPOGL()
{
	var table = Data.GetTable("LineItems__");
	var itemCount = table.GetItemCount();
	//Look for existing line
	for (var i = 0; i < itemCount; i++)
	{
		var line = table.GetItem(i);
		if (line.GetValue("LineType__") === Lib.P2P.LineType.PO)
		{
			line.SetValue("LineType__", Lib.P2P.LineType.POGL);
		}
	}
}

function searchContractNumbersAndSetValue(candidates)
{
	if (candidates && candidates.length > 0)
	{
		Log.Info("FTR Contract Number candidates: " + JSON.stringify(candidates));
		var ldapFilter = Sys.Helpers.LdapUtil.FilterEqual("ReferenceNumber__", candidates[0].standardStringValue).toString();
		for (var i = 1; i < candidates.length; i++)
		{
			var item = candidates[i];
			var newFilter = Sys.Helpers.LdapUtil.FilterEqual("ReferenceNumber__", item.standardStringValue).toString();
			ldapFilter = Sys.Helpers.LdapUtil.FilterOr(ldapFilter, newFilter).toString();
		}
		var query = Process.CreateQueryAsProcessAdmin();
		query.Reset();
		query.SetSpecificTable("CDNAME#P2P - Contract");
		query.SetFilter(ldapFilter.toString());
		query.SetAttributesList("ReferenceNumber__");
		query.SetOptionEx("Limit=1");
		if (query.MoveFirst())
		{
			var record = query.MoveNext();
			if (record)
			{
				var vars = record.GetUninheritedVars();
				var contractNumber = vars.GetValue("ReferenceNumber__", 0);
				for (var i = 0; i < candidates.length; i++)
				{
					var item = candidates[i];
					if (item.standardStringValue === contractNumber)
					{
						Data.SetValue("ContractNumber__", candidates[i].area, candidates[i].standardStringValue);
						break;
					}
				}
			}
		}
	}
}

function handleContractValidity()
{
	if (Sys.Parameters.GetInstance("P2P").GetParameter("EnableContractGlobalSetting", "") === "1")
	{
		Lib.AP.Contract.HandleContractValidity();
	}
}
/**
 * This function will determinate the best way to reconcile the invoice
 * with the PO found (Teaching, extract data, perfect match, ...)
 */
function reconcileInvoiceWithPO()
{
	// If no teaching, or if nothing was found by teaching, try to extract data from invoice
	var extractionTable = Data.GetTable("ExtractedLineItems__");
	if (extractionTable && extractionTable.GetItemCount() === 0)
	{
		extractLineItemsFromInvoice();
	}
	Variable.SetValueAsString("ReconciledByHeader", "false");
	Lib.AP.Reconciliation.reconcileInvoice();
}
//#endregion
/** *************************** **/
/** RECOGNITION OF CREDIT NOTES **/
/** *************************** **/
//#region
function adaptFieldSignByInvoiceType(item, fieldName, creditNote)
{
	var fldValue = item.GetValue(fieldName);
	if (fldValue !== null && fldValue > 0 && creditNote)
	{
		var fldArea = item.GetArea(fieldName);
		if (fldArea)
		{
			item.SetValue(fieldName, fldArea, -fldValue);
		}
		else
		{
			item.SetValue(fieldName, -fldValue);
		}
	}
}

function adaptSignByInvoiceType(creditNote)
{
	adaptFieldSignByInvoiceType(Data, "InvoiceAmount__", creditNote);
	Lib.AP.ConvertInLocalCurrency("InvoiceAmount__");
	adaptFieldSignByInvoiceType(Data, "ExtractedNetAmount__", creditNote);
	var lineItems = Data.GetTable("LineItems__");
	for (var j = 0; j < lineItems.GetItemCount(); j++)
	{
		var item = lineItems.GetItem(j);
		adaptFieldSignByInvoiceType(item, "Amount__", creditNote);
		adaptFieldSignByInvoiceType(item, "TaxAmount__", creditNote);
	}
}
/**
 * Extract by script the invoice line items table
 */
function extractLineItemsFromInvoice()
{
	Log.Info("Starting to extract line items from document");
	Sys.Helpers.ExtractTable.TableName = "ExtractedLineItems__";
	if (Sys.Parameters.GetInstance("AP").GetParameter("AutolearningOnPOLines") !== "1")
	{
		if (Lib.ERP.IsSAP())
		{
			addLondonPostmasterTemplateSAP();
			addIDESTemplate();
		}
		else
		{
			addLondonPostmasterTemplateGeneric();
		}
		addEskerDemoTemplate();
	}
	Sys.Helpers.ExtractTable.Extract();
}
/**
 *	Declare the template for London Postmaster multiline template
 */
function addLondonPostmasterTemplateGeneric()
{
	Sys.Helpers.ExtractTable.AddDocumentType("LondonPostmaster-Multiline");
	Sys.Helpers.ExtractTable.AddRecognitionRule("LondonPostmaster-Multiline", "London Postmaster vendor", "London Postmaster", Document.GetArea(0, 76, 154, 360, 166), false, true);
	Sys.Helpers.ExtractTable.AddRecognitionRule("LondonPostmaster-Multiline", "Invoice N°", "109423", Document.GetArea(0, 540, 543, 305, 135), false, true);
	Sys.Helpers.ExtractTable.AddExtractionRule("LondonPostmaster-Multiline", "OrderNumber", null, null, /Your Order No.\s*(45000[0-9]{5})/gi, "OrderNumberExtracted__", true);
	Sys.Helpers.ExtractTable.AddExtractionRule("LondonPostmaster-Multiline", "PartNo", null, null, /(PD#[0-9]+)/gi, "PartNumberExtracted__");
	Sys.Helpers.ExtractTable.AddExtractionRule("LondonPostmaster-Multiline", "UnitPrice", 1200, 200, "Number", "UnitPriceExtracted__");
	Sys.Helpers.ExtractTable.AddExtractionRule("LondonPostmaster-Multiline", "Quantity", 960, 160, "Number", "QuantityExtracted__");
	Sys.Helpers.ExtractTable.AddExtractionRule("LondonPostmaster-Multiline", "Amount", 2000, 250, "Number", "AmountExtracted__");
	var multiLineOptions = {
		LineItemHeight: 170,
		TableTopPosition: 1430,
		TableBottomPosition: 3200,
		TableBottomDelimiter: "Total GBP Incl. VAT",
		ExactBoundingArea: true
	};
	Sys.Helpers.ExtractTable.SetOptions("LondonPostmaster-Multiline", multiLineOptions);
	Sys.Helpers.ExtractTable.AddDocumentType("LondonPostmaster-SingleLine");
	Sys.Helpers.ExtractTable.AddRecognitionRule("LondonPostmaster-SingleLine", "London Postmaster vendor", "London Postmaster", Document.GetArea(0, 76, 154, 360, 166), false, true);
	Sys.Helpers.ExtractTable.AddRecognitionRule("LondonPostmaster-SingleLine", "Ref. in table", "Ref.", Document.GetArea(0, 156, 1232, 113, 47), false, true);
	Sys.Helpers.ExtractTable.AddExtractionRule("LondonPostmaster-SingleLine", "OrderNo", null, null, /(45000[0-9]{5})/gi, "OrderNumberExtracted__");
	Sys.Helpers.ExtractTable.AddExtractionRule("LondonPostmaster-SingleLine", "PartNo", null, null, /(DPC[0-9]+)/gi, "PartNumberExtracted__");
	Sys.Helpers.ExtractTable.AddExtractionRule("LondonPostmaster-SingleLine", "UnitPrice", 1200, 200, "Number", "UnitPriceExtracted__");
	Sys.Helpers.ExtractTable.AddExtractionRule("LondonPostmaster-SingleLine", "Quantity", 900, 110, /([0-9]+)/gi, "QuantityExtracted__", true);
	Sys.Helpers.ExtractTable.AddExtractionRule("LondonPostmaster-SingleLine", "Amount", 2100, 220, "Number", "AmountExtracted__", true);
	var singleLineOptions = {
		LineItemHeight: 50,
		TableTopPosition: 1410,
		TableBottomPosition: 2180,
		TableBottomDelimiter: "Total GBP Incl. VAT",
		ExactBoundingArea: true
	};
	Sys.Helpers.ExtractTable.SetOptions("LondonPostmaster-SingleLine", singleLineOptions);
}

function addLondonPostmasterTemplateSAP()
{
	Sys.Helpers.ExtractTable.AddDocumentType("LondonPostmaster-Multiline");
	Sys.Helpers.ExtractTable.AddRecognitionRule("LondonPostmaster-Multiline", "London Postmaster vendor", "London Postmaster", Document.GetArea(0, 153, 220, 360, 300), false, true);
	Sys.Helpers.ExtractTable.AddRecognitionRule("LondonPostmaster-Multiline", "Your Order No in description", "Your Order No", Document.GetArea(0, 400, 1649, 638, 607), false, true);
	Sys.Helpers.ExtractTable.AddExtractionRule("LondonPostmaster-Multiline", "OrderNumber", null, null, /Your Order No.\s*(45000[0-9]{5})/gi, "OrderNumberExtracted__", true);
	Sys.Helpers.ExtractTable.AddExtractionRule("LondonPostmaster-Multiline", "PartNo", null, null, /(PD#[0-9]+)/gi, "PartNumberExtracted__");
	Sys.Helpers.ExtractTable.AddExtractionRule("LondonPostmaster-Multiline", "UnitPrice", 1300, 200, "Number", "UnitPriceExtracted__");
	Sys.Helpers.ExtractTable.AddExtractionRule("LondonPostmaster-Multiline", "Quantity", 940, 180, "Number", "QuantityExtracted__");
	Sys.Helpers.ExtractTable.AddExtractionRule("LondonPostmaster-Multiline", "Amount", 2170, 180, "Number", "AmountExtracted__");
	var multiLineOptions = {
		LineItemHeight: 100,
		TableTopPosition: 1400,
		TableBottomPosition: 3400,
		TableBottomDelimiter: "Total GBP Incl. VAT",
		ExactBoundingArea: true
	};
	Sys.Helpers.ExtractTable.SetOptions("LondonPostmaster-Multiline", multiLineOptions);
	Sys.Helpers.ExtractTable.AddDocumentType("LondonPostmaster-SingleLine");
	Sys.Helpers.ExtractTable.AddRecognitionRule("LondonPostmaster-SingleLine", "London Postmaster vendor", "London Postmaster", Document.GetArea(0, 140, 150, 360, 300), false, true);
	Sys.Helpers.ExtractTable.AddExtractionRule("LondonPostmaster-SingleLine", "OrderNo", null, null, /(45000[0-9]{5})/gi, "OrderNumberExtracted__");
	Sys.Helpers.ExtractTable.AddExtractionRule("LondonPostmaster-SingleLine", "PartNo", null, null, /(DPC[0-9]+)/gi, "PartNumberExtracted__");
	Sys.Helpers.ExtractTable.AddExtractionRule("LondonPostmaster-SingleLine", "UnitPrice", 1300, 300, "Number", "UnitPriceExtracted__");
	Sys.Helpers.ExtractTable.AddExtractionRule("LondonPostmaster-SingleLine", "Quantity", 900, 400, /([0-9]+)/gi, "QuantityExtracted__", true);
	Sys.Helpers.ExtractTable.AddExtractionRule("LondonPostmaster-SingleLine", "Amount", 2100, 250, "Number", "AmountExtracted__", true);
	var singleLineOptions = {
		LineItemHeight: 49,
		TableTopPosition: 1400,
		TableBottomPosition: 2300,
		TableBottomDelimiter: "Total GBP Incl. VAT",
		ExactBoundingArea: true
	};
	Sys.Helpers.ExtractTable.SetOptions("LondonPostmaster-SingleLine", singleLineOptions);
}
/**
 *	Declare the template for IDES templates
 */
function addIDESTemplate()
{
	Sys.Helpers.ExtractTable.AddDocumentType("IDES-SimpleLine");
	Sys.Helpers.ExtractTable.AddRecognitionRule("IDES-SimpleLine", "IDES invoice", "IDES AG", Document.GetArea(0, 2175, 310, 200, 100), false, true);
	Sys.Helpers.ExtractTable.AddExtractionRule("IDES-SimpleLine", "OrderNo", null, null, /(45000[0-9]{5})/gi, "OrderNumberExtracted__");
	Sys.Helpers.ExtractTable.AddExtractionRule("IDES-SimpleLine", "PartNo", null, null, /((DPC|T-|40-)[0-9]+[a-z]?)/gi, "PartNumberExtracted__");
	Sys.Helpers.ExtractTable.AddExtractionRule("IDES-SimpleLine", "UnitPrice", 1300, 300, "Number", "UnitPriceExtracted__");
	Sys.Helpers.ExtractTable.AddExtractionRule("IDES-SimpleLine", "Quantity", 900, 400, /([0-9]+)/gi, "QuantityExtracted__", true);
	Sys.Helpers.ExtractTable.AddExtractionRule("IDES-SimpleLine", "Amount", 2100, 250, "Number", "AmountExtracted__", true);
	var singleLineOptions = {
		LineItemHeight: 50,
		TableTopPosition: 1400,
		TableBottomPosition: 2500,
		TableBottomDelimiter: "Total EUR Incl. VAT",
		ExactBoundingArea: true
	};
	Sys.Helpers.ExtractTable.SetOptions("IDES-SimpleLine", singleLineOptions);
	Sys.Helpers.ExtractTable.AddDocumentType("MMGenerator-SimpleLine");
	Sys.Helpers.ExtractTable.AddRecognitionRule("MMGenerator-SimpleLine", "MMGenerator invoice", "Ides AG", Document.GetArea(0, 1240, 845, 1000, 250), false, true);
	Sys.Helpers.ExtractTable.AddExtractionRule("MMGenerator-SimpleLine", "Order", null, null, /(45000[0-9]{5})/gi, "OrderNumberExtracted__");
	Sys.Helpers.ExtractTable.AddExtractionRule("MMGenerator-SimpleLine", "Ref", null, null, /((DPC|T-|40-)[0-9]+[a-z]?)/gi, "PartNumberExtracted__");
	Sys.Helpers.ExtractTable.AddExtractionRule("MMGenerator-SimpleLine", "Unit Price", 1620, 225, "Number", "UnitPriceExtracted__");
	Sys.Helpers.ExtractTable.AddExtractionRule("MMGenerator-SimpleLine", "Quantity", 1370, 225, /([0-9]+)/gi, "QuantityExtracted__", true);
	Sys.Helpers.ExtractTable.AddExtractionRule("MMGenerator-SimpleLine", "Amount", 1875, 225, "Number", "AmountExtracted__", true);
	singleLineOptions = {
		LineItemHeight: 50,
		TableTopPosition: 1500,
		TableBottomPosition: 3000,
		TableBottomDelimiter: "AMOUNT DUE",
		ExactBoundingArea: true
	};
	Sys.Helpers.ExtractTable.SetOptions("MMGenerator-SimpleLine", singleLineOptions);
}
/**
 *	Declare the template for the demo multiline template
 */
function addEskerDemoTemplate()
{
	Sys.Helpers.ExtractTable.AddDocumentType("PAC-Multiline");
	// Use full document area since it only appears on last page
	Sys.Helpers.ExtractTable.AddRecognitionRule("PAC-Multiline", "Esker Demo", "Esker Demo", Document.GetArea(), false, true);
	var bGRIV = Lib.P2P.IsGRIVEnabledGlobally();
	if (bGRIV)
	{
		Sys.Helpers.ExtractTable.AddExtractionRule("PAC-Multiline", "DN", 149, 244, /([#0-9A-Za-z_-]+)/gi, "DeliveryNoteExtracted__");
		Sys.Helpers.ExtractTable.AddExtractionRule("PAC-Multiline", "PartNo", 400, 244, /([0-9A-Za-z_-]+)/gi, "PartNumberExtracted__", true);
	}
	else
	{
		Sys.Helpers.ExtractTable.AddExtractionRule("PAC-Multiline", "PartNo", 149, 244, /([0-9A-Za-z_-]+)/gi, "PartNumberExtracted__", true);
	}
	Sys.Helpers.ExtractTable.AddExtractionRule("PAC-Multiline", "UnitPrice", 1713, 208, "Number", "UnitPriceExtracted__");
	Sys.Helpers.ExtractTable.AddExtractionRule("PAC-Multiline", "Quantity", 1312, 219, "Number", "QuantityExtracted__", true);
	Sys.Helpers.ExtractTable.AddExtractionRule("PAC-Multiline", "Amount", 2190, 181, "Number", "AmountExtracted__", true);
	var multiLineOptions = {
		LineItemHeight: 48,
		TableTopPosition: 1650,
		TableBottomPosition: 2354,
		ExactBoundingArea: true
	};
	Sys.Helpers.ExtractTable.SetOptions("PAC-Multiline", multiLineOptions);
}
//#endregion
/** ************** **/
/** INIT FUNCTIONS **/
/** ************** **/
//#region
function initDefaultValues()
{
	clearUnwantedAutolearnedFields();
	initERP();
	var invoiceDocument = Lib.AP.GetInvoiceDocument();
	invoiceDocument.InitDefaultValues();
	if (hasMapperDocument())
	{
		fillVendorFromMapperDocument();
		fillExtractedLinesFromMapperDocument();
	}
	else
	{
		initVendor();
	}
	//////////////////////////////
	// DEMO PART
	if (!Data.GetValue("CompanyCode__"))
	{
		var companyCode = Lib.ERP.IsSAP() ? "1000" : "US01";
		Log.Info("CompanyCode not defined by process variable or vendor, set to default value " + companyCode);
		Data.SetValue("CompanyCode__", companyCode);
	}
	//////////////////////////////
	initInvoiceType();
	Data.SetValue("CurrentException__", "");
	Data.SetValue("SubsequentDocument__", false);
	if (!Lib.ERP.IsSAP())
	{
		Data.SetValue("GRIV__", Lib.P2P.IsGRIVEnabledGlobally());
	}
	//Serialze serializable parmeters on the process so they can be retrieve by the Mobile App
	Sys.Parameters.GetInstance("P2P").Serialize();
	Sys.Parameters.GetInstance("AP").Serialize();
	Lib.P2P.SetTablesToIndex(["LineItems__", "ApproversList__"]);
}
var initERPCalled = false;

function initERP()
{
	if (!initERPCalled)
	{
		initERPCalled = true;
		// first get ERP from document name
		var erpName_1 = getERPByDocumentName();
		// if not set or not supported, we retrieve the default ERP for company code (if exists)
		if (!erpName_1 && Data.GetValue("CompanyCode__"))
		{
			var filter = "CompanyCode__=" + Data.GetValue("CompanyCode__");
			Sys.GenericAPI.Query("PurchasingCompanycodes__", filter, ["ERP__"], function (result, error)
			{
				if (!error && result && result.length > 0)
				{
					erpName_1 = result[0].ERP__;
				}
			});
		}
		Lib.ERP.InitERPName(erpName_1, false, "AP");
	}
}
/**
 * Clears fields that could have been autolearned, but we don't want them to be autolearned
 */
function clearUnwantedAutolearnedFields()
{
	// We should disable autolearning DatabaseCombobox when the feature will be available
	Data.SetValue("ERPInvoiceNumber__", "");
	Data.SetValue("ERPMMInvoiceNumber__", "");
}
/**
 * Company code not fill by teach nor autolearn nor vendor preferred invoice type.
 * @returns false if we need to re-extract the document with a new configuration, true otherwise.
 */
function initCompanyCode()
{
	Data.SetValue("Configuration__", Variable.GetValueAsString("Configuration"));
	// Init Company code from MRU
	if (hasMapperDocument())
	{
		var mapperDoc = Lib.AP.MappingManager.CurrentMapperDocument;
		tryQueryBusinessValueFromMRU(null, "CompanyCode__", mapperDoc.GetCustomerVATNumber());
	}
	// Override company code when
	// - created from the portal
	// - demo invoices file names
	// - forced by external variable
	var companyCode = Variable.GetValueAsString("customerInvoiceCompanyCode") || getCompanyCodeByDocumentName() || Variable.GetValueAsString("CompanyCode");
	if (companyCode)
	{
		setCompanyCode(companyCode);
		return true;
	}
	// If no override yet, but CC was set from MRU, we stop here
	if (Data.GetValue("CompanyCode__"))
	{
		return true;
	}
	// Else try to determine the Company code based on the Keywords defined in P2P-Company Code table
	var matchingCompanyCodes = Lib.AP.CompanyCodeDetermination.DetermineCompanyCodeFromKeywords();
	var warningMessage = "";
	var companyCodeArea = null;
	if (matchingCompanyCodes && matchingCompanyCodes.length)
	{
		var firstMatch = matchingCompanyCodes[0];
		if (firstMatch.defaultConfiguration && firstMatch.defaultConfiguration !== Data.GetValue("Configuration__"))
		{
			Lib.AP.CompanyCodeDetermination.ReExtract(matchingCompanyCodes);
			return false;
		}
		companyCode = firstMatch.companyCode;
		companyCodeArea = firstMatch.area;
		warningMessage = getMultipleMatchesWarning(matchingCompanyCodes);
	}
	else
	{
		// Fallback on the configuration
		companyCode = Sys.Parameters.GetInstance("AP").GetParameter("CompanyCode");
		if (matchingCompanyCodes !== null)
		{
			// Automated company code determination option was enabled
			warningMessage = Language.Translate("_using default company code", false);
		}
	}
	if (companyCode)
	{
		setCompanyCode(companyCode, warningMessage, companyCodeArea);
	}
	return true;
}

function setCompanyCode(companyCode, warningMessage, companyCodeArea)
{
	traceDebug("Init company code :" + companyCode);
	Data.SetValue("CompanyCode__", companyCode);
	Data.SetWarning("CompanyCode__", warningMessage ? warningMessage : "");
	if (companyCodeArea)
	{
		highlightCompanyCodeArea(companyCodeArea);
	}
}

function getMultipleMatchesWarning(matchingCompanyCodes)
{
	var concatenatedCompanyCodes = "";
	for (var _i = 0, _a = matchingCompanyCodes.slice(1); _i < _a.length; _i++)
	{
		var matchingCC = _a[_i];
		concatenatedCompanyCodes += " " + matchingCC.companyCode;
	}
	return concatenatedCompanyCodes ? Language.Translate("_multiple matching company codes : {0}", false, concatenatedCompanyCodes) : concatenatedCompanyCodes;
}

function getDocumentOriginalName()
{
	// When attachment name contains "." (NAV demo data CompanyCode), it is replaced by "_" in nicename (Document.GetName)
	// --> use AttachOriginalName
	var doc = Attach.GetProcessedDocument();
	return (doc && doc.GetVars().GetValue_String("AttachOriginalName", 0)) || Document.GetName();
}

function getERPByDocumentName()
{
	var documentName = getDocumentOriginalName();
	// 02/05/2016: PAC generated filename: PO_INVOICE_<CompanyCode>_<ERP>_<InvoiceNumber>.pdf
	var exp = /INVOICE_(.[^_]+)_(.[^_]+)_/;
	var res = exp.exec(documentName);
	var erpName = res !== null ? res[2] : null;
	return Lib.ERP.IsSupportedManager(erpName) ? erpName : null;
}
/**
 * For samples invoices, override companycode from file name
 */
function getCompanyCodeByDocumentName()
{
	var documentName = getDocumentOriginalName();
	// 02/05/2016: PAC generated filename: PO_INVOICE_<CompanyCode>_<ERP>_<InvoiceNumber>.pdf
	var exp = /INVOICE_(.[^_]+)_(.[^_]+)_/;
	var res = exp.exec(documentName);
	var companyCode = res !== null ? res[1] : null;
	//////////////////////////////
	// DEMO PART
	// old sample invoice filename: PO_INVOICE_<CountryCode>_<Vendor>_<InvoiceNumber>.pdf
	if (companyCode === "FR" || companyCode === "US" || companyCode === "GB")
	{
		// anticipated call
		initERP();
		if (Lib.ERP.IsSAP())
		{
			companyCode = "1000";
		}
		else
		{
			if (companyCode === "GB")
			{
				companyCode = "UK";
			}
			companyCode += "01";
		}
	}
	//////////////////////////////
	return companyCode;
}

function initVendor()
{
	// Init vendor number if the invoice has been submitted from vendor portal
	var submitterVendorId = Variable.GetValueAsString("submission_vendorid");
	if (submitterVendorId)
	{
		Data.SetValue("VendorNumber__", submitterVendorId);
		traceDebug("Vendor '" + submitterVendorId + "' retrieved by submission on vendor portal.");
	}
	// If it is a retry, search vendor only if it has been set by teaching or autolearning (to fill vendor info from SAP)
	if (Process.GetScriptRetryCount() <= 1 || Data.GetValue("VendorNumber__") || Data.GetValue("VendorName__"))
	{
		var invoiceDocument = Lib.AP.GetInvoiceDocument();
		Lib.P2P.FirstTimeRecognition_Vendor.Recognize(Lib.AP.GetAvailableDocumentCultures(), Data.GetValue("CompanyCode__") || invoiceDocument.GetDemoCompanyCode(), Data.GetValue("VendorNumber__"), Data.GetValue("VendorName__"), fillVendorFieldsFromQueryResult, highlightVendorArea, invoiceDocument.SearchVendorNumber, invoiceDocument);
	}
}
/**
 * Invoice type not fill by teach nor autolearn nor vendor preferred invoice type
 */
function initInvoiceType()
{
	if (!Data.IsComputed("InvoiceType__"))
	{
		if (g_vendorPreferredInvoiceType)
		{
			traceDebug("Setting invoice type with preferred value from vendor (" + g_vendorPreferredInvoiceType + ")");
			Data.SetValue("InvoiceType__", g_vendorPreferredInvoiceType);
		}
		else
		{
			var defaultInvoiceType = Variable.GetValueAsString("InvoiceType");
			if (defaultInvoiceType)
			{
				Log.Info("Setting invoice type with external variable value (" + defaultInvoiceType + ")");
				Data.SetValue("InvoiceType__", defaultInvoiceType);
			}
		}
	}
}

function initParameters()
{
	var setParametersCallback = function (lastAlertLevel, lastTouchlessEnabled, templateName, lineItemsImporterMapping)
	{
		Data.SetValue("DuplicateCheckAlertLevel__", lastAlertLevel);
		Data.SetValue("TouchlessEnabled__", lastTouchlessEnabled);
		Data.SetValue("CodingTemplate__", templateName);
		Variable.SetValueAsString("LineItemsImporterMapping", lineItemsImporterMapping ? lineItemsImporterMapping : "");
	};
	var companyCode = Data.GetValue("CompanyCode__");
	var vendorNumber = Data.GetValue("VendorNumber__");
	Lib.AP.Parameters.GetParameters(companyCode, vendorNumber, setParametersCallback);
}

function validateInvoiceCurrency()
{
	var currencies = Lib.P2P.ExchangeRate.GetCompanyCodeCurrencies(Data.GetValue("CompanyCode__"));
	if (currencies.indexOf(Data.GetValue("InvoiceCurrency__")) === -1)
	{
		Data.SetValue("InvoiceCurrency__", Lib.P2P.ExchangeRate.GetCompanyCodeCurrency(Data.GetValue("CompanyCode__")));
	}
}

function updateExchangeRate()
{
	Data.SetValue("LocalCurrency__", Lib.P2P.ExchangeRate.GetCompanyCodeCurrency(Data.GetValue("CompanyCode__")));
	var exchangeRate = Lib.P2P.ExchangeRate.GetExchangeRate(Data.GetValue("CompanyCode__"), Data.GetValue("InvoiceCurrency__"));
	Data.SetValue("ExchangeRate__", exchangeRate.toString());
	Data.SetValue("LocalInvoiceAmount__", Data.GetValue("InvoiceAmount__") * exchangeRate);
	Data.SetValue("LocalNetAmount__", Data.GetValue("NetAmount__") * exchangeRate);
	Data.SetValue("LocalTaxAmount__", Data.GetValue("TaxAmount__") * exchangeRate);
}

function APCodingsPrediction()
{
	var lineItemsTable = Data.GetTable("LineItems__");
	var vendorNumber = Data.GetValue("VendorNumber__");
	var companyCode = Data.GetValue("CompanyCode__");
	var invoiceDoc = Lib.AP.GetInvoiceDocument();
	var isDataCreated = false;
	if (companyCode && vendorNumber)
	{
		var options = {
			CompanyCode: companyCode,
			VendorID: vendorNumber
		};
		try
		{
			var WSResult = Process.GetCodingsPrediction(options);
			if (WSResult && WSResult.status === 200)
			{
				var data = JSON.parse(WSResult.data);
				lineItemsTable.SetItemCount(0);
				for (var i = 0; i < data.length; i++)
				{
					var gline = data[i];
					var curItem = lineItemsTable.AddItem();
					curItem.SetValue("LineType__", Lib.P2P.LineType.GL);
					curItem.SetValue("Amount__", Number(gline.Amount) * Data.GetValue("InvoiceAmount__"));
					curItem.SetValue("GLAccount__", gline.GLAccount__);
					curItem.SetValue("CostCenter__", gline.CostCenter__);
					curItem.SetValue("TaxCode__", gline.TaxCode__);
					Lib.P2P.fillCostTypeFromGLAccount(curItem);
					invoiceDoc.UpdateGLLineFromMapperDocument(curItem);
					isDataCreated = true;
				}
			}
			else
			{
				Log.Error("Exception during webservice call : Status " + WSResult.status + ", Message " + WSResult.lastMessage);
			}
		}
		catch (e)
		{
			Log.Warn("Exception during webservice call : " + e);
		}
	}
	return isDataCreated;
}

function loadTemplate()
{
	if (!Lib.AP.InvoiceType.isGLInvoice())
	{
		return;
	}
	var vendorNumber = Data.GetValue("VendorNumber__");
	var companyCode = Data.GetValue("CompanyCode__");
	var lineItemsTable = Data.GetTable("LineItems__");
	var templateName = Data.GetValue("CodingTemplate__");
	var extractedNetAmount = Data.GetValue("ExtractedNetAmount__");
	var noResultCallback = function ()
	{
		if (Data.GetValue("CodingTemplate__"))
		{
			Data.SetWarning("CodingTemplate__", "_TemplateWarning");
		}
		inferGLAccounts();
	};
	var invoiceDoc = Lib.AP.GetInvoiceDocument();
	//try to call ML codings prevision WS
	var isDataCreated = false;
	if (companyCode && Sys.Parameters.GetInstance("AP").GetParameter("APCodingsPrediction") === "1")
	{
		isDataCreated = APCodingsPrediction();
	}
	if (!isDataCreated)
	{
		if (hasMapperDocument() && Lib.AP.MappingManager.CurrentMapperDocument.GetGlLines())
		{
			var glLineItems = Lib.AP.MappingManager.CurrentMapperDocument.GetGlLines();
			lineItemsTable.SetItemCount(0);
			var curItem = lineItemsTable.AddItem();
			var customDimensions = Sys.Helpers.TryCallFunction("Lib.P2P.Customization.Common.GetCustomDimensions");
			for (var i = 0; i < glLineItems.length; i++)
			{
				var gline = glLineItems[i];
				if (i > 0)
				{
					curItem = curItem.AddItem();
				}
				curItem.SetValue("LineType__", Lib.P2P.LineType.GL);
				curItem.SetValue("Description__", gline.Description);
				curItem.SetValue("Amount__", gline.Amount);
				curItem.SetValue("GLAccount__", gline.GLAccount);
				curItem.SetValue("CostCenter__", gline.CostCenter);
				curItem.SetValue("TaxCode__", gline.TaxCode);
				curItem.SetValue("TaxRate__", gline.TaxRate);
				curItem.SetValue("TaxAmount__", gline.TaxAmount);
				Lib.P2P.fillCostTypeFromGLAccount(curItem);
				if (customDimensions && customDimensions.codingTemplates)
				{
					for (var _i = 0, _a = customDimensions.codingTemplates; _i < _a.length; _i++)
					{
						var template = _a[_i];
						curItem.SetValue(template.nameInForm, gline[template.nameInTable]);
					}
				}
				invoiceDoc.UpdateGLLineFromMapperDocument(curItem);
			}
		}
		else
		{
			invoiceDoc.LoadTemplate(extractedNetAmount, companyCode, vendorNumber, templateName, lineItemsTable, Lib.AP.ConvertInLocalCurrency, noResultCallback);
		}
	}
}

function setGLAccountAndGLDescription(item, GLAccount)
{
	item.SetValue("GLAccount__", GLAccount);
	item.SetComputedValueSource("GLAccount__", "Synergy");
	if (!Lib.ERP.IsSAP())
	{
		Lib.AP.UpdateGLCCDescriptions(item);
	}
	else
	{
		var bapiParams = Lib.AP.SAP.PurchaseOrder.GetBapiParameters();
		var companyCode = Lib.AP.GetLineItemCompanyCode(item);
		var glAccountDescription = Lib.AP.SAP.GetGLAccountDescriptionServer(bapiParams.GetBapi("BAPI_GL_ACC_GETDETAIL"), companyCode, GLAccount, Lib.AP.SAP.GetCurrentUserLanguage());
		item.SetValue("GLDescription__", glAccountDescription);
	}
}

function inferGLAccounts()
{
	if (Sys.Parameters.GetInstance("AP").GetParameter("GLAccountDeterminationFrench") === "1" && Data.GetValue("InvoiceType__") === "Non-PO Invoice")
	{
		var jsonString = Process.GetSynergyExtraction("SynergyGLAccountFR");
		var glAccounts = {};
		try
		{
			glAccounts = JSON.parse(jsonString);
		}
		catch (e)
		{
			Log.Error("SynergyGLAccountFR : Failed to parse JSON");
			return;
		}
		var bestGLAccount = null;
		var bestScore = 0;
		for (var glAccount in glAccounts)
		{
			if (glAccounts[glAccount] > bestScore)
			{
				bestGLAccount = glAccount;
				bestScore = glAccounts[glAccount];
			}
		}
		var lineItems = Data.GetTable("LineItems__");
		var nbItems = lineItems.GetItemCount();
		if (nbItems > 0 && bestGLAccount)
		{
			for (var i = 0; i < nbItems; i++)
			{
				var item = lineItems.GetItem(i);
				if (!item.GetValue("GLAccount__"))
				{
					setGLAccountAndGLDescription(item, bestGLAccount);
				}
			}
		}
	}
}

function setDigitalSignatureField(CheckSignatureResult)
{
	switch (CheckSignatureResult)
	{
	case "NoSignature":
		Data.SetValue("DigitalSignature__", "Not signed");
		break;
	case "SignatureInvalid":
		Data.SetValue("DigitalSignature__", "Invalid Corrupted");
		break;
	case "SignatureOk":
		Data.SetValue("DigitalSignature__", "Verified");
		break;
	case "SignatureDone":
		Data.SetValue("DigitalSignature__", "Signed");
		break;
	case "SignatureError":
		Data.SetValue("DigitalSignature__", "Signature failed");
		break;
	case "InvalidSignatureParameters":
	case "InvalidParameters":
		Data.SetValue("DigitalSignature__", "Invalid parameters");
		break;
	case "SignatureNotVerified":
		Data.SetValue("DigitalSignature__", "SignatureNotVerified");
		break;
		//case 'VerificationError':
		//case 'InvalidFileExtension':
	default:
		Data.SetValue("DigitalSignature__", "Unknown");
		break;
	}
}

function getCleanDigitalSignatureDetail(CheckSignatureDetail)
{
	var result = CheckSignatureDetail;
	// check if starting with SingnerInvalid
	var signerInvalidIdx = result.indexOf("SignerInvalid");
	if (signerInvalidIdx > -1)
	{
		var errorDetailIdx = result.indexOf(": ", signerInvalidIdx);
		result = result.substring(errorDetailIdx + 1);
	}
	else
	{
		// try extraction extract between quote
		var firstQuoteIdx = result.indexOf('"');
		if (firstQuoteIdx > -1)
		{
			var secondQuoteIdx = result.indexOf('"', firstQuoteIdx + 1);
			result = result.substring(firstQuoteIdx + 1, secondQuoteIdx);
		}
	}
	return result;
}

function manageSignatureResult(result)
{
	var resultShortStatus = result;
	var resultLongStatus = "";
	var splitResultIdx = result.indexOf(": ");
	if (splitResultIdx > 0)
	{
		resultShortStatus = result.substr(0, splitResultIdx);
		Log.Warn(result.substr(splitResultIdx + 2));
		if (resultShortStatus !== "VerificationError")
		{
			resultLongStatus = getCleanDigitalSignatureDetail(result.substr(splitResultIdx + 2));
		}
	}
	setDigitalSignatureField(resultShortStatus);
	Variable.SetValueAsString("InvoiceSignature", resultShortStatus);
	Variable.SetValueAsString("InvoiceSignatureDetail", resultLongStatus);
}

function getProcessedDocumentIndex()
{
	var nbAttach = Attach.GetNbAttach();
	for (var i = 0; i < nbAttach; i++)
	{
		if (Attach.IsProcessedDocument(i))
		{
			return i;
		}
	}
	return -1;
}
/**
 * Return the FromCountry for the signature check.
 * The From can be the Country of the vendor portal user
 * else the Country of the vendor
 * else the default country provided
 * @param {string} defaultCountry The default country if no country was found
 * @return {string} The From country
 */
function getFromCountry(defaultCountry)
{
	var country = null;
	if (Data.GetValue("ReceptionMethod__") === "Vendor portal")
	{
		var json = Lib.AP.VendorPortal.GetParametersFromDataInvoice(Data);
		var vendorContact = Lib.AP.VendorPortal.GetVendorUser(json);
		if (vendorContact)
		{
			country = vendorContact.GetValue("Country");
		}
	}
	else
	{
		country = Data.GetValue("VendorCountry__");
	}
	return country ? country : defaultCountry;
}

function manageSignature()
{
	var companyCodeCountry = "";

	function CheckCompanyCountry(_CCValue, _IsSignature)
	{
		// Check if company code query return a company code
		if (Object.keys(_CCValue).length === 0)
		{
			Log.Info((_IsSignature ? "Signature" : "Validation") + " : Error on company code");
			manageSignatureResult(_IsSignature ? "InvalidSignatureParameters" : "InvalidParameters");
			return false;
		}
		companyCodeCountry = _CCValue.Country__;
		// Check if the country code linked to company is valid
		if (!Sys.Locale.Country.GetCountryName(companyCodeCountry))
		{
			Log.Info((_IsSignature ? "Signature" : "Validation") + " : Country code linked to company code is invalid : " + companyCodeCountry);
			manageSignatureResult(_IsSignature ? "InvalidSignatureParameters" : "InvalidParameters");
			return false;
		}
		return true;
	}
	// Request company code table to get company code data
	Lib.P2P.CompanyCodesValue.QueryValues(Data.GetValue("CompanyCode__")).Then(function (CCValues)
	{
		/**
		if document is sign 	-> check signature
		if document is not sign -> sign it
		**/
		var processDocumentIdx = getProcessedDocumentIndex();
		if (processDocumentIdx !== -1)
		{
			if (Sys.AP.Server.IsSigned(processDocumentIdx))
			{
				/** Check first attachment signature**/
				if (Sys.Parameters.GetInstance("AP").GetParameter("VerificationCheckAttachmentSignature") === "1")
				{
					if (!CheckCompanyCountry(CCValues, false))
					{
						return;
					}
					var fromCountry = getFromCountry(companyCodeCountry);
					var toCountry = companyCodeCountry;
					manageSignatureResult(Sys.AP.Server.CheckSignature("TrustWeaver", processDocumentIdx, fromCountry, toCountry));
				}
				else
				{
					manageSignatureResult("SignatureNotVerified");
				}
			}
			else if (Sys.Parameters.GetInstance("AP").GetParameter("SignInvoicesForArchiving") === "1")
			{
				if (!CheckCompanyCountry(CCValues, true))
				{
					return;
				}
				/** Sign first attachment **/
				manageSignatureResult(Sys.AP.Server.SignDocument("TrustWeaver", processDocumentIdx, companyCodeCountry, companyCodeCountry));
			}
			else
			{
				manageSignatureResult("NoSignature");
			}
		}
		else
		{
			Log.Info("Signature/Validation : No document to process defined.");
		}
	});
}

function generatePdf(jsonValue, templatePath, filename)
{
	var isAttached = false;
	var error = "";
	var reportTemplate = Process.GetResourceFile(templatePath);
	if (reportTemplate)
	{
		try
		{
			var jsonString = JSON.stringify(jsonValue);
			var jsonFile = TemporaryFile.CreateFile("VIP.JSON", "utf16");
			if (!jsonFile)
			{
				Log.Error("Temporaty file creating failed: vip.json");
				throw "Couldn't create the json file";
			}
			TemporaryFile.Append(jsonFile, jsonString);
			var cvtFile = jsonFile.ConvertFile(
			{
				conversionType: "crystal",
				report: reportTemplate
			});
			if (!cvtFile)
			{
				throw "Crystal report error";
			}
			var index = Attach.GetNbAttach();
			isAttached = Attach.AttachTemporaryFile(cvtFile,
			{
				name: filename,
				attachAsConverted: true,
				attachAsFirst: true
			});
			if (!isAttached)
			{
				throw "Couldn't attach the generated file";
			}
			Variable.SetValueAsString("HumanReadableAttachmentName", filename);
			Attach.SetValue(index, "AttachConvertedFiles", 0);
		}
		catch (ex)
		{
			error = "The document could not be formatted: " + ex;
		}
	}
	else
	{
		error = "The document could not be formatted: Template not found";
	}
	if (error)
	{
		Log.Error(error);
	}
	return isAttached;
}

function getAttachFileNameWithExtension(index, extension)
{
	var fileName = Attach.GetAttach(index).GetVars().GetValue_String("AttachOriginalName", 0);
	var lastDot = fileName.lastIndexOf(".");
	fileName = lastDot > 0 ? fileName.substr(0, lastDot) : fileName;
	fileName += extension;
	return fileName;
}

function getJsonAttachFile()
{
	// Start looking for the json file from the end
	var idx = Attach.GetNbAttach() - 1;
	while (idx > -1 && Attach.GetInputFile(idx).GetExtension().toLowerCase() !== ".json")
	{
		idx--;
	}
	Log.Info("JSON file is at index " + idx);
	return idx;
}

function initExtraction()
{
	var _a;
	var receptionMethod = Data.GetValue("ReceptionMethod__");
	var idx = 0;
	if (receptionMethod === "Claims" || receptionMethod === "Vendor portal")
	{
		// In Expense the file containing invoice data is not attached at idx = 0 (Also, it's a json file)
		idx = getJsonAttachFile();
	}
	var file = null;
	if (idx > -1)
	{
		file = Attach.GetInputFile(idx);
	}
	else
	{
		Log.Error("No file to process...");
	}
	// Check if the attachment was OCRed or not
	if (file && (!Document.IsLoaded() || file.GetExtension().toLowerCase() === ".txt" || file.GetExtension().toLowerCase() === ".json"))
	{
		Log.Info("Searching Mapping Lib for data extraction...");
		var mapperDocument = getMapperDocument(file);
		if (mapperDocument)
		{
			Log.Info("Using Mapping Lib for data extraction");
			Lib.AP.MappingManager.SetCurrentMapperDocument(mapperDocument);
			Attach.SetTechnical(idx, mapperDocument.IsProcessDocumentTechnical());
			mapperDocument.Run();
			Data.SetValue("ReceptionMethod__", mapperDocument.GetReceptionMethod());
			Data.SetValue("SourceDocument__", (_a = mapperDocument.GetSourceDocument()) !== null && _a !== void 0 ? _a : "");
		}
	}
}

function endExtraction()
{
	var mapperDoc = Lib.AP.MappingManager.CurrentMapperDocument;
	if (mapperDoc)
	{
		// Generate Human readable PDF
		var templatePath = mapperDoc.GetConversionTemplatePath();
		var convertedAdded = false;
		if (templatePath)
		{
			var fileName = getAttachFileNameWithExtension(0, ".pdf");
			var json = mapperDoc.toJson();
			if (json)
			{
				if (!json.error)
				{
					convertedAdded = generatePdf(json, templatePath, fileName);
				}
				else
				{
					Log.Error("Failed to generatePDF from json: " + json.error);
				}
			}
			else
			{
				Log.Error("Failed to generatePDF from json");
			}
		}
		mapperDoc.AttachEmbeddedDocuments(!convertedAdded, TemporaryFile);
		mapperDoc.SetProcessingLabel();
	}
	Lib.AP.GetInvoiceDocument().EndExtraction();
	handleContractValidity();
}

function tryQueryBusinessValueFromMRU(companyCode, fieldName, extractedValue)
{
	if (Sys.Helpers.IsEmpty(extractedValue))
	{
		return false;
	}
	var businessValue = Lib.AP.Extraction.CrossReference.GetERPValue(companyCode, fieldName, extractedValue);
	if (businessValue)
	{
		Log.Info("Setting " + fieldName + " with MRU value " + businessValue);
		Data.SetValue(fieldName, businessValue);
		return true;
	}
	return false;
}

function tryQueryVendorFromTable(mapperDoc, companyCode, fillVendorFunction)
{
	var queryResult;
	var filter;
	if (mapperDoc.GetVendorNumber()) // only documents coming from a procurement solution (i.e. expense reports) have this information
	{
		filter = Sys.Helpers.LdapUtil.FilterEqual("Number__", mapperDoc.GetVendorNumber()).toString();
		queryResult = Sys.Helpers.Database.GetFirstRecordResult("AP - Vendors__", filter.AddCompanyCodeFilter(companyCode));
	}
	if (!queryResult && mapperDoc.GetVendorVATNumber())
	{
		filter = Sys.Helpers.LdapUtil.FilterEqual("VATNumber__", mapperDoc.GetVendorVATNumber()).toString();
		queryResult = Sys.Helpers.Database.GetFirstRecordResult("AP - Vendors__", filter.AddCompanyCodeFilter(companyCode));
	}
	if (!queryResult && mapperDoc.GetVendorName())
	{
		filter = Sys.Helpers.LdapUtil.FilterEqual("Name__", mapperDoc.GetVendorName()).toString();
		queryResult = Sys.Helpers.Database.GetFirstRecordResult("AP - Vendors__", filter.AddCompanyCodeFilter(companyCode));
	}
	if (queryResult && fillVendorFunction)
	{
		fillVendorFunction(queryResult);
	}
}

function fillVendorFromMapperDocument()
{
	var mapperDoc = Lib.AP.MappingManager.CurrentMapperDocument;
	var companyCode = Data.GetValue("CompanyCode__");
	if (!tryQueryBusinessValueFromMRU(companyCode, "VendorNumber__", mapperDoc.GetVendorVATNumber()))
	{
		tryQueryVendorFromTable(mapperDoc, companyCode, fillVendorFieldsFromQueryResult);
	}
	else
	{
		var vendorNumber = Data.GetValue("VendorNumber__");
		if (vendorNumber)
		{
			var invoiceDocument = Lib.AP.GetInvoiceDocument();
			invoiceDocument.SearchVendorNumber(companyCode, vendorNumber, null, fillVendorFieldsFromQueryResult);
		}
	}
}

function fillExtractedLinesFromMapperDocument()
{
	var mapperDoc = Lib.AP.MappingManager.CurrentMapperDocument;
	var extractedLineItems = Data.GetTable("ExtractedLineItems__");
	var mapperLineItems = mapperDoc.GetLineItems();
	extractedLineItems.SetItemCount(mapperLineItems.length);
	for (var i = 0; i < mapperLineItems.length; i++)
	{
		var item = mapperLineItems[i];
		var line = extractedLineItems.GetItem(i);
		if (item.OrderNumber)
		{
			line.SetValue("OrderNumberExtracted__", item.OrderNumber);
		}
		if (item.Quantity)
		{
			line.SetValue("QuantityExtracted__", item.Quantity);
		}
		if (item.Amount)
		{
			line.SetValue("AmountExtracted__", item.Amount);
		}
		if (item.UnitPrice)
		{
			line.SetValue("UnitPriceExtracted__", item.UnitPrice);
		}
		if (item.DeliveryNote)
		{
			line.SetValue("DeliveryNoteExtracted__", item.DeliveryNote);
		}
		if (item.PartNumber)
		{
			line.SetValue("PartNumberExtracted__", item.PartNumber);
		}
	}
}

function getMapperDocument(inputFile)
{
	var mapper = Lib.AP.MappingManager.GetMapper(inputFile.GetContent());
	if (mapper)
	{
		return mapper.GetNewDocument(inputFile);
	}
	Log.Error("No registered mapper is able to extract informations from the attached file");
	return null;
}

function cleanExtractedLineItems()
{
	// Reset extracted line items to reduce the weight of the form data (even if they had been taught)
	var g_clearExtractedLineItems = Lib.AP.GetVariableAsBoolean("ClearExtractedLineItems", false);
	if (!g_debug && g_clearExtractedLineItems)
	{
		Data.GetTable("ExtractedLineItems__").SetItemCount(0);
		// no need to perform more cleaning action
		return;
	}
	// Clean error on extracted line has they are not relevant for processing
	var exLineItemsTable = Data.GetTable("ExtractedLineItems__");
	var fieldsInError = exLineItemsTable.GetItemCount() > 0 ? Data.GetFieldsInError() : null;
	if (fieldsInError && fieldsInError.nbErrors > 0)
	{
		var exLinesInError = fieldsInError.tables.ExtractedLineItems__;
		if (exLinesInError)
		{
			for (var row in exLinesInError.rows)
			{
				if (Object.hasOwnProperty.call(exLinesInError.rows, row))
				{
					var exItem = exLineItemsTable.GetItem(row);
					for (var _i = 0, _a = exLinesInError.rows[row]; _i < _a.length; _i++)
					{
						var fieldName = _a[_i];
						exItem.SetValue(fieldName, "");
						exItem.SetError(fieldName, "");
					}
				}
			}
		}
	}
}

function hasMapperDocument()
{
	if (Lib.AP.MappingManager.CurrentMapperDocument)
	{
		return true;
	}
	return false;
}

function continueExtraction()
{
	initDefaultValues();
	/** header gathering **/
	// Try to identify header fields
	searchHeaderFields();
	/** retrieve vendor parameters **/
	initParameters();
	manageSignature();
	/** retrieve last used values **/
	loadTemplate();
	/** load UnplannedDeliveryCosts **/
	if (Lib.AP.InvoiceType.isPOInvoice() || Lib.AP.InvoiceType.isPOGLInvoice())
	{
		Lib.AP.UnplannedDeliveryCosts.ExtractFromKeywords();
	}
	/** Document fields initialization **/
	// Update the balance
	Lib.AP.UpdateBalance(Lib.AP.TaxHelper.computeHeaderTaxAmount, g_debug);
	Lib.AP.GetInvoiceDocument().ComputePaymentAmountsAndDates(true, true);
	/** Anomaly detection **/
	Lib.AP.AnomalyDetection.DetectUnusualInvoiceAmount();
	// Set form validity date (to prevent expiration errors)
	Lib.AP.WorkflowCtrl.ExpirationHelper.ResetValidity();
	// set a flag to disable Request teaching button when no document is attached
	var refAttach = Attach.GetProcessedDocument();
	if (refAttach)
	{
		Variable.SetValueAsString("DocAttached", "1");
	}
	else
	{
		Variable.SetValueAsString("DocAttached", "0");
	}
	cleanExtractedLineItems();
	// Mark document as to verify
	Data.SetValue("InvoiceStatus__", Lib.AP.InvoiceStatus.ToVerify);
	if (!Lib.ERP.IsSAP())
	{
		Lib.Budget.IsEnabled();
	}
	Lib.AP.Extraction.DetermineSourceTypeFromOriginalJobID();
	Lib.AP.Extraction.DetermineOwner();
	// When submitted from the vendor portal, we need to initiat he conversation between VI and CI
	if (Data.GetValue("ReceptionMethod__") === "Vendor portal" && Data.GetValue("PortalRuidEx__ "))
	{
		Lib.AP.VendorPortal.JoinVIToCIChat(true);
	}
	var allowBuildOfApprovers = Sys.Helpers.TryCallFunction("Lib.AP.Customization.Common.Workflow.OnBuildOfApprovers");
	if (typeof allowBuildOfApprovers !== "boolean")
	{
		allowBuildOfApprovers = true;
	}
	// Build approvers list
	Lib.AP.WorkflowCtrl.Init();
	Lib.AP.WorkflowCtrl.SetObject("usersObject", Users);
	Lib.AP.WorkflowCtrl.SetObject("allowApprovers", Lib.AP.GetInvoiceDocument().AllowApprovers());
	Lib.AP.WorkflowCtrl.InitRolesSequence(allowBuildOfApprovers, true, true);
	Lib.AP.Extraction.CrossReference.SaveEDIValues();
	endExtraction();
	Sys.Helpers.TryCallFunction("Lib.AP.Customization.Extraction.OnExtractionScriptEnd");
	// Finalize SAP connection
	if (Lib.ERP.IsSAP())
	{
		Lib.AP.SAP.PurchaseOrder.FinalizeBapiParameters();
	}
	Lib.AP.ResetERPManager();
}
//#endregion
/** ******** **/
/** RUN PART **/
/** ******** **/
//#region
/** initialization **/
Sys.Helpers.TryCallFunction("Lib.AP.Customization.Extraction.OnExtractionScriptBegin");
Lib.AP.InitArchiveDuration();
// Read EDI information - Can force CompanyCode
initExtraction();
if (initCompanyCode())
{
	continueExtraction();
}
//#endregion