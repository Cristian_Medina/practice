///#GLOBALS Lib
Lib.AP.WorkflowCtrl.Init();

function executeFinalizationScript()
{
	if (Data.GetValue("State") <= 100)
	{
		Process.Forward(Lib.AP.WorkflowCtrl.GetWorkflowInitiator().login);
	}
}
executeFinalizationScript();