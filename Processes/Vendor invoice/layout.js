{
	"*": {
		"GDRData": {
			"type": "GDRData",
			"options": {
				"version": 0
			},
			"stamp": 1,
			"data": []
		},
		"OCRParams": {
			"type": "OCRParams",
			"options": {
				"version": 0
			},
			"stamp": 2,
			"data": []
		},
		"Screen": {
			"type": "FlexibleFormLayout",
			"options": {
				"splitterType": "complex",
				"version": 0,
				"maxwidth": "",
				"backColor": "text-backgroundcolor-default",
				"style": null
			},
			"*": {
				"form-header": {
					"type": "FlexibleFormHeader",
					"options": {
						"version": 0,
						"maxwidth": ""
					},
					"stamp": 3,
					"data": []
				},
				"form-center": {
					"type": "FlexibleFormCenter",
					"options": {
						"version": 0,
						"maxwidth": ""
					},
					"*": {
						"form-content": {
							"type": "FlexibleFormContent",
							"options": {
								"version": 0
							},
							"*": {
								"form-content-top": {
									"type": "FlexibleFormSplitter",
									"options": {
										"autosize": true,
										"version": 1,
										"box": {
											"top": "0%",
											"left": "0%",
											"width": null,
											"height": "18%"
										},
										"hidden": true
									},
									"*": {},
									"stamp": 4,
									"data": []
								},
								"form-content-bottom": {
									"type": "FlexibleFormSplitter",
									"options": {
										"version": 1,
										"box": {
											"top": "0%",
											"left": "0%",
											"width": null,
											"height": "100%"
										},
										"hidden": false,
										"hideSeparator": true,
										"stickypanel": "TopPaneWarning"
									},
									"*": {
										"form-content-left": {
											"type": "FlexibleFormSplitter",
											"options": {
												"version": 1,
												"box": {
													"top": "0%",
													"left": "0%",
													"width": "54%",
													"height": null
												},
												"hidden": false,
												"hideSeparator": true
											},
											"*": {
												"form-content-left-2": {
													"type": "FlexibleFormLine",
													"options": {
														"version": 0
													},
													"*": {
														"SystemData": {
															"type": "PanelSystemData",
															"options": {
																"multicolumn": false,
																"version": 0,
																"border": {},
																"labelLength": 0,
																"label": "_System data",
																"hidden": true,
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": false,
																"hideTitleBar": false,
																"hideTitle": false,
																"sameHeightAsSiblings": false
															},
															"stamp": 5,
															"data": []
														}
													},
													"stamp": 6,
													"data": []
												},
												"form-content-left-3": {
													"type": "FlexibleFormLine",
													"options": {
														"version": 0
													},
													"*": {
														"NextProcess": {
															"type": "PanelNextProcess",
															"options": {
																"hidden": true,
																"version": 0,
																"border": {},
																"labelLength": 0,
																"label": "_Next Process",
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": false,
																"hideTitleBar": false,
																"hideTitle": false,
																"sameHeightAsSiblings": false
															},
															"stamp": 7,
															"data": []
														}
													},
													"stamp": 8,
													"data": []
												},
												"form-content-left-9": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 9,
													"*": {
														"TopPaneWarning": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "M",
																"hideActionButtons": true,
																"hideTitleBar": true,
																"hideTitle": false,
																"backgroundcolor": "color3",
																"labelsAlignment": "right",
																"label": "TopPaneWarning",
																"leftImageURL": "",
																"removeMargins": false,
																"hidden": true,
																"version": 0,
																"sameHeightAsSiblings": false
															},
															"stamp": 10,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {},
																		"version": 0
																	},
																	"stamp": 11,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"TopMessageWarning__": {
																						"line": 1,
																						"column": 1
																					}
																				},
																				"lines": 1,
																				"columns": 2,
																				"colspans": [
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					]
																				],
																				"version": 0
																			},
																			"stamp": 12,
																			"*": {
																				"TopMessageWarning__": {
																					"type": "Text",
																					"data": false,
																					"options": {
																						"marge": 0,
																						"textSize": "M",
																						"textAlignment": "center",
																						"textStyle": "bold",
																						"textColor": "color8",
																						"backgroundcolor": "color3",
																						"label": "_TopMessageWarning",
																						"version": 0,
																						"iconClass": ""
																					},
																					"stamp": 13
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-7": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 14,
													"*": {
														"DocumentsPanel": {
															"type": "PanelDocument",
															"options": {
																"version": 0,
																"border": {},
																"labelLength": 0,
																"label": "_Documents",
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": false,
																"hideTitleBar": false,
																"hideTitle": false,
																"sameHeightAsSiblings": false
															},
															"*": {
																"Document": {
																	"data": [
																		"documentscontroldata"
																	],
																	"type": "Documents",
																	"options": {
																		"version": 1,
																		"previewButton": true,
																		"downloadButton": true
																	},
																	"stamp": 15
																}
															},
															"stamp": 16,
															"data": []
														}
													}
												},
												"form-content-left-11": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 17,
													"*": {
														"HeaderDataPanelForApproversLeft": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": "120",
																"iconURL": "AP_HeaderDataPanel.png",
																"TitleFontSize": "M",
																"hideActionButtons": true,
																"hideTitleBar": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "_Invoice Details for approvers (left)",
																"leftImageURL": "",
																"hidden": true,
																"version": 0,
																"hideTitle": false,
																"sameHeightAsSiblings": false
															},
															"stamp": 18,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"VendorNameForApprovers__": "LabelVendorNameForApprovers__",
																			"LabelVendorNameForApprovers__": "VendorNameForApprovers__",
																			"InvoiceNumberForApprovers__": "LabelInvoiceNumberForApprovers__",
																			"LabelInvoiceNumberForApprovers__": "InvoiceNumberForApprovers__",
																			"OrderNumberForApprovers__": "LabelOrderNumberForApprovers__",
																			"LabelOrderNumberForApprovers__": "OrderNumberForApprovers__",
																			"NetAmountForApprovers__": "LabelNetAmountForApprovers__",
																			"LabelNetAmountForApprovers__": "NetAmountForApprovers__",
																			"InvoiceCurrencyForApprovers__": "LabelInvoiceCurrencyForApprovers__",
																			"LabelInvoiceCurrencyForApprovers__": "InvoiceCurrencyForApprovers__",
																			"LabelInvoiceDateForApprovers__": "InvoiceDateForApprovers__",
																			"InvoiceDateForApprovers__": "LabelInvoiceDateForApprovers__",
																			"ContractNumberDetails__": "LabelContractNumberDetails__",
																			"LabelContractNumberDetails__": "ContractNumberDetails__"
																		},
																		"version": 0
																	},
																	"stamp": 19,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"VendorNameForApprovers__": {
																						"line": 1,
																						"column": 2
																					},
																					"LabelVendorNameForApprovers__": {
																						"line": 1,
																						"column": 1
																					},
																					"InvoiceNumberForApprovers__": {
																						"line": 2,
																						"column": 2
																					},
																					"LabelInvoiceNumberForApprovers__": {
																						"line": 2,
																						"column": 1
																					},
																					"OrderNumberForApprovers__": {
																						"line": 3,
																						"column": 2
																					},
																					"LabelOrderNumberForApprovers__": {
																						"line": 3,
																						"column": 1
																					},
																					"ContractNumberDetails__": {
																						"line": 4,
																						"column": 2
																					},
																					"LabelContractNumberDetails__": {
																						"line": 4,
																						"column": 1
																					},
																					"LabelInvoiceDateForApprovers__": {
																						"line": 5,
																						"column": 1
																					},
																					"InvoiceDateForApprovers__": {
																						"line": 5,
																						"column": 2
																					},
																					"NetAmountForApprovers__": {
																						"line": 6,
																						"column": 2
																					},
																					"LabelNetAmountForApprovers__": {
																						"line": 6,
																						"column": 1
																					},
																					"InvoiceCurrencyForApprovers__": {
																						"line": 7,
																						"column": 2
																					},
																					"LabelInvoiceCurrencyForApprovers__": {
																						"line": 7,
																						"column": 1
																					}
																				},
																				"lines": 7,
																				"columns": 2,
																				"colspans": [
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[]
																				],
																				"version": 0
																			},
																			"stamp": 20,
																			"*": {
																				"LabelVendorNameForApprovers__": {
																					"type": "Label",
																					"data": [
																						"VendorName__"
																					],
																					"options": {
																						"label": "_Vendor name",
																						"dataSource": 1,
																						"version": 0
																					},
																					"stamp": 21
																				},
																				"VendorNameForApprovers__": {
																					"type": "ShortText",
																					"data": [
																						"VendorName__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"label": "_Vendor name",
																						"activable": true,
																						"width": 230,
																						"dataSource": 1,
																						"readonly": true,
																						"browsable": false,
																						"autocompletable": false,
																						"version": 0
																					},
																					"stamp": 22
																				},
																				"LabelInvoiceNumberForApprovers__": {
																					"type": "Label",
																					"data": [
																						"InvoiceNumber__"
																					],
																					"options": {
																						"label": "_Invoice number",
																						"dataSource": 1,
																						"version": 0
																					},
																					"stamp": 23
																				},
																				"InvoiceNumberForApprovers__": {
																					"type": "ShortText",
																					"data": [
																						"InvoiceNumber__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"label": "_Invoice number",
																						"activable": true,
																						"width": 230,
																						"dataSource": 1,
																						"readonly": true,
																						"browsable": false,
																						"autocompletable": false,
																						"version": 0
																					},
																					"stamp": 24
																				},
																				"LabelOrderNumberForApprovers__": {
																					"type": "Label",
																					"data": [
																						"OrderNumber__"
																					],
																					"options": {
																						"label": "_Order number",
																						"dataSource": 1,
																						"version": 0
																					},
																					"stamp": 25
																				},
																				"LabelInvoiceDateForApprovers__": {
																					"type": "Label",
																					"data": [
																						"InvoiceDate__"
																					],
																					"options": {
																						"label": "_Invoice date",
																						"dataSource": 1,
																						"version": 0
																					},
																					"stamp": 26
																				},
																				"LabelContractNumberDetails__": {
																					"type": "Label",
																					"data": [
																						"ContractNumber__"
																					],
																					"options": {
																						"label": "_ContractNumber",
																						"hidden": true,
																						"version": 0
																					},
																					"stamp": 815
																				},
																				"ContractNumberDetails__": {
																					"type": "DatabaseComboBox",
																					"data": [
																						"ContractNumber__"
																					],
																					"options": {
																						"version": 1,
																						"autoCompleteSearchMode": "startswith",
																						"searchMode": "contains",
																						"label": "_ContractNumber",
																						"activable": true,
																						"width": "180",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"helpIconPosition": "Right",
																						"minNbLines": 1,
																						"maxNbLines": 1,
																						"fTSmaxRecords": "20",
																						"browsable": true,
																						"PreFillFTS": true,
																						"RestrictSearch": true,
																						"hidden": true,
																						"notInDB": true,
																						"dataType": "String"
																					},
																					"stamp": 816
																				},
																				"OrderNumberForApprovers__": {
																					"type": "ShortText",
																					"data": [
																						"OrderNumber__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"label": "_Order number",
																						"activable": true,
																						"width": 230,
																						"dataSource": 1,
																						"readonly": true,
																						"browsable": false,
																						"autocompletable": false,
																						"version": 0
																					},
																					"stamp": 27
																				},
																				"InvoiceDateForApprovers__": {
																					"type": "DateTime",
																					"data": [
																						"InvoiceDate__"
																					],
																					"options": {
																						"displayLongFormat": false,
																						"label": "_Invoice date",
																						"activable": true,
																						"width": 230,
																						"dataSource": 1,
																						"readonly": true,
																						"autocompletable": false,
																						"version": 0
																					},
																					"stamp": 28
																				},
																				"LabelNetAmountForApprovers__": {
																					"type": "Label",
																					"data": [
																						"NetAmount__"
																					],
																					"options": {
																						"label": "_Net amount",
																						"dataSource": 1,
																						"version": 0
																					},
																					"stamp": 29
																				},
																				"NetAmountForApprovers__": {
																					"type": "Decimal",
																					"data": [
																						"NetAmount__"
																					],
																					"options": {
																						"version": 2,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"integer": false,
																						"label": "_Net amount",
																						"precision_internal": 2,
																						"activable": true,
																						"width": 230,
																						"dataSource": 1,
																						"readonly": true,
																						"autocompletable": false,
																						"precision_current": 2
																					},
																					"stamp": 30
																				},
																				"LabelInvoiceCurrencyForApprovers__": {
																					"type": "Label",
																					"data": [
																						"InvoiceCurrency__"
																					],
																					"options": {
																						"label": "_Invoice currency",
																						"dataSource": 1,
																						"version": 0
																					},
																					"stamp": 31
																				},
																				"InvoiceCurrencyForApprovers__": {
																					"type": "ShortText",
																					"data": [
																						"InvoiceCurrency__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"label": "_Invoice currency",
																						"activable": true,
																						"width": 230,
																						"dataSource": 1,
																						"readonly": true,
																						"browsable": false,
																						"autocompletable": false,
																						"version": 0
																					},
																					"stamp": 32
																				}
																			}
																		}
																	}
																}
															}
														},
														"HeaderDataPanelForApproversRight": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": "120",
																"iconURL": "",
																"TitleFontSize": "M",
																"hideActionButtons": true,
																"hideTitleBar": false,
																"hideTitle": true,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "_Invoice Details for approvers (right)",
																"leftImageURL": "",
																"version": 0,
																"hidden": true,
																"sameHeightAsSiblings": false
															},
															"stamp": 33,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"AsideReasonForApprovers__": "LabelAsideReasonForApprovers__",
																			"LabelAsideReasonForApprovers__": "AsideReasonForApprovers__",
																			"InvoiceStatusForApprovers__": "LabelInvoiceStatusForApprovers__",
																			"LabelInvoiceStatusForApprovers__": "InvoiceStatusForApprovers__",
																			"DueDateForApprovers__": "LabelDueDateForApprovers__",
																			"LabelDueDateForApprovers__": "DueDateForApprovers__",
																			"DiscountLimitDate2__": "LabelDiscountLimitDate2__",
																			"LabelDiscountLimitDate2__": "DiscountLimitDate2__",
																			"EstimatedDiscountAmountForApprover__": "LabelEstimatedDiscountAmountForApprover__",
																			"LabelEstimatedDiscountAmountForApprover__": "EstimatedDiscountAmountForApprover__",
																			"EstimatedLatePaymentFeeForApprovers__": "LabelEstimatedLatePaymentFeeForApprovers",
																			"LabelEstimatedLatePaymentFeeForApprovers": "EstimatedLatePaymentFeeForApprovers__"
																		},
																		"version": 0
																	},
																	"stamp": 34,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"DiscountLimitDate2__": {
																						"line": 1,
																						"column": 2
																					},
																					"LabelDiscountLimitDate2__": {
																						"line": 1,
																						"column": 1
																					},
																					"EstimatedDiscountAmountForApprover__": {
																						"line": 2,
																						"column": 2
																					},
																					"LabelEstimatedDiscountAmountForApprover__": {
																						"line": 2,
																						"column": 1
																					},
																					"DueDateForApprovers__": {
																						"line": 3,
																						"column": 2
																					},
																					"LabelDueDateForApprovers__": {
																						"line": 3,
																						"column": 1
																					},
																					"LabelEstimatedLatePaymentFeeForApprovers": {
																						"line": 4,
																						"column": 1
																					},
																					"EstimatedLatePaymentFeeForApprovers__": {
																						"line": 4,
																						"column": 2
																					},
																					"InvoiceStatusForApprovers__": {
																						"line": 5,
																						"column": 2
																					},
																					"LabelInvoiceStatusForApprovers__": {
																						"line": 5,
																						"column": 1
																					},
																					"AsideReasonForApprovers__": {
																						"line": 6,
																						"column": 2
																					},
																					"LabelAsideReasonForApprovers__": {
																						"line": 6,
																						"column": 1
																					}
																				},
																				"lines": 6,
																				"columns": 2,
																				"colspans": [
																					[],
																					[],
																					[],
																					[],
																					[],
																					[]
																				],
																				"version": 0
																			},
																			"stamp": 35,
																			"*": {
																				"LabelDueDateForApprovers__": {
																					"type": "Label",
																					"data": [
																						"DueDate__"
																					],
																					"options": {
																						"label": "_Due date",
																						"dataSource": 1,
																						"version": 0
																					},
																					"stamp": 36
																				},
																				"DueDateForApprovers__": {
																					"type": "DateTime",
																					"data": [
																						"DueDate__"
																					],
																					"options": {
																						"displayLongFormat": false,
																						"label": "_Due date",
																						"activable": true,
																						"width": 230,
																						"dataSource": 1,
																						"readonly": true,
																						"autocompletable": false,
																						"version": 0
																					},
																					"stamp": 37
																				},
																				"LabelEstimatedLatePaymentFeeForApprovers": {
																					"type": "Label",
																					"data": [
																						"EstimatedLatePaymentFee__"
																					],
																					"options": {
																						"label": "_EstimatedLatePaymentFee",
																						"dataSource": 1,
																						"version": 0
																					},
																					"stamp": 38
																				},
																				"EstimatedLatePaymentFeeForApprovers__": {
																					"type": "Decimal",
																					"data": [
																						"EstimatedLatePaymentFee__"
																					],
																					"options": {
																						"version": 1,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"integer": false,
																						"label": "_EstimatedLatePaymentFee",
																						"precision_internal": 2,
																						"precision_current": 2,
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"dataSource": 1,
																						"readonly": true,
																						"notInDB": true,
																						"dataType": "Number",
																						"autocompletable": false
																					},
																					"stamp": 39
																				},
																				"LabelDiscountLimitDate2__": {
																					"type": "Label",
																					"data": [
																						"DiscountLimitDate__"
																					],
																					"options": {
																						"label": "_DiscountLimitDate",
																						"dataSource": 1,
																						"version": 0
																					},
																					"stamp": 40
																				},
																				"DiscountLimitDate2__": {
																					"type": "DateTime",
																					"data": [
																						"DiscountLimitDate__"
																					],
																					"options": {
																						"displayLongFormat": false,
																						"label": "_DiscountLimitDate",
																						"activable": true,
																						"width": "230",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"dataSource": 1,
																						"readonly": true,
																						"notInDB": true,
																						"dataType": "Date",
																						"autocompletable": false,
																						"version": 0
																					},
																					"stamp": 41
																				},
																				"LabelEstimatedDiscountAmountForApprover__": {
																					"type": "Label",
																					"data": [
																						"EstimatedDiscountAmount__"
																					],
																					"options": {
																						"label": "_EstimatedDiscountAmount",
																						"dataSource": 1,
																						"version": 0
																					},
																					"stamp": 42
																				},
																				"EstimatedDiscountAmountForApprover__": {
																					"type": "Decimal",
																					"data": [
																						"EstimatedDiscountAmount__"
																					],
																					"options": {
																						"version": 2,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"integer": false,
																						"label": "_EstimatedDiscountAmount",
																						"precision_internal": 2,
																						"precision_current": 2,
																						"activable": true,
																						"width": "230",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"dataSource": 1,
																						"readonly": true,
																						"notInDB": true,
																						"dataType": "Number",
																						"autocompletable": false
																					},
																					"stamp": 43
																				},
																				"LabelInvoiceStatusForApprovers__": {
																					"type": "Label",
																					"data": [
																						"InvoiceStatus__"
																					],
																					"options": {
																						"label": "_Invoice status",
																						"dataSource": 1,
																						"version": 0
																					},
																					"stamp": 44
																				},
																				"InvoiceStatusForApprovers__": {
																					"type": "ComboBox",
																					"data": [
																						"InvoiceStatus__"
																					],
																					"options": {
																						"possibleValues": {
																							"0": "Received",
																							"1": "To verify",
																							"2": "To approve",
																							"3": "To post",
																							"4": "On hold",
																							"5": "To pay",
																							"6": "Rejected",
																							"7": "Set aside",
																							"8": "Paid",
																							"9": "Reversed",
																							"10": "Expired"
																						},
																						"version": 1,
																						"keyValueMode": true,
																						"possibleKeys": {
																							"0": "Received",
																							"1": "To verify",
																							"2": "To approve",
																							"3": "To post",
																							"4": "On hold",
																							"5": "To pay",
																							"6": "Rejected",
																							"7": "Set aside",
																							"8": "Paid",
																							"9": "Reversed",
																							"10": "Expired"
																						},
																						"label": "_Invoice status",
																						"activable": true,
																						"width": 230,
																						"dataSource": 1,
																						"readonly": true,
																						"maxNbLinesToDisplay": 10
																					},
																					"stamp": 45
																				},
																				"LabelAsideReasonForApprovers__": {
																					"type": "Label",
																					"data": [
																						"AsideReason__"
																					],
																					"options": {
																						"label": "_AsideReason",
																						"dataSource": 1,
																						"version": 0
																					},
																					"stamp": 46
																				},
																				"AsideReasonForApprovers__": {
																					"type": "ComboBox",
																					"data": [
																						"AsideReason__"
																					],
																					"options": {
																						"possibleValues": {
																							"0": "",
																							"1": "Waiting for goods receipt",
																							"2": "Quantity variance",
																							"3": "Price variance",
																							"4": "Schedule variance",
																							"5": "Quality inspection",
																							"6": "Waiting for credit memo",
																							"7": "Other reason"
																						},
																						"version": 1,
																						"keyValueMode": true,
																						"possibleKeys": {
																							"0": "-- none --",
																							"1": "Waiting for goods receipt",
																							"2": "Quantity variance",
																							"3": "Price variance",
																							"4": "Schedule variance",
																							"5": "Quality inspection",
																							"6": "Waiting for credit memo",
																							"7": "Other reason"
																						},
																						"label": "_AsideReason",
																						"activable": true,
																						"width": 230,
																						"dataSource": 1,
																						"readonly": true,
																						"maxNbLinesToDisplay": 10
																					},
																					"stamp": 47
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-8": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 48,
													"*": {
														"Invoice_Processing": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {
																	"maximized": false
																},
																"labelLength": 120,
																"label": "_Invoice Processing",
																"version": 0,
																"hideTitleBar": false,
																"iconURL": "AP_Invoice_Processing.png",
																"TitleFontSize": "M",
																"hideActionButtons": true,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"hideTitle": false,
																"sameHeightAsSiblings": false
															},
															"stamp": 49,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"LabelInvoiceType__": "InvoiceType__",
																			"InvoiceType__": "LabelInvoiceType__",
																			"LabelInvoiceStatus__": "InvoiceStatus__",
																			"InvoiceStatus__": "LabelInvoiceStatus__",
																			"CompanyCode__": "LabelCompanyCode__",
																			"LabelCompanyCode__": "CompanyCode__",
																			"AsideReason__": "LabelAsideReason__",
																			"LabelAsideReason__": "AsideReason__",
																			"VerificationDate__": "LabelVerificationDate__",
																			"LabelVerificationDate__": "VerificationDate__",
																			"ReceptionMethod__": "LabelReceptionMethod__",
																			"LabelReceptionMethod__": "ReceptionMethod__",
																			"BackToAPReason__": "LabelBackToAPReason__",
																			"LabelBackToAPReason__": "BackToAPReason__",
																			"RejectReason__": "LabelRejectReason__",
																			"LabelRejectReason__": "RejectReason__",
																			"ERP__": "LabelERP__",
																			"LabelERP__": "ERP__",
																			"DigitalSignature__": "LabelDigitalSignature__",
																			"LabelDigitalSignature__": "DigitalSignature__",
																			"Configuration__": "LabelConfiguration__",
																			"LabelConfiguration__": "Configuration__",
																			"ArchiveRuidEx__": "LabelArchiveRuidEx__",
																			"LabelArchiveRuidEx__": "ArchiveRuidEx__",
																			"ArchiveProcessLink__": "LabelArchiveProcessLink__",
																			"LabelArchiveProcessLink__": "ArchiveProcessLink__",
																			"ArchiveProcessLinkGenerated__": "LabelArchiveProcessLinkGenerated__",
																			"LabelArchiveProcessLinkGenerated__": "ArchiveProcessLinkGenerated__",
																			"ScheduledActionDate__": "LabelScheduledActionDate__",
																			"LabelScheduledActionDate__": "ScheduledActionDate__",
																			"ScheduledAction__": "LabelScheduledAction__",
																			"LabelScheduledAction__": "ScheduledAction__",
																			"SourceDocument__": "LabelSourceDocument__",
																			"LabelSourceDocument__": "SourceDocument__",
																			"ManuallyModifiedFieldsCount__": "LabelManuallyModifiedFieldsCount__",
																			"LabelManuallyModifiedFieldsCount__": "ManuallyModifiedFieldsCount__",
																			"AutomaticallyModifiedFieldsCount__": "LabelAutomaticallyModifiedFieldsCount__",
																			"LabelAutomaticallyModifiedFieldsCount__": "AutomaticallyModifiedFieldsCount__",
																			"PaidOnTime__": "LabelPaidOnTime__",
																			"LabelPaidOnTime__": "PaidOnTime__",
																			"HasBeenOnHold__": "LabelHasBeenOnHold__",
																			"LabelHasBeenOnHold__": "HasBeenOnHold__"
																		},
																		"version": 0
																	},
																	"stamp": 50,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"CompanyCode__": {
																						"line": 1,
																						"column": 2
																					},
																					"LabelCompanyCode__": {
																						"line": 1,
																						"column": 1
																					},
																					"LabelInvoiceType__": {
																						"line": 3,
																						"column": 1
																					},
																					"InvoiceType__": {
																						"line": 3,
																						"column": 2
																					},
																					"LabelInvoiceStatus__": {
																						"line": 4,
																						"column": 1
																					},
																					"InvoiceStatus__": {
																						"line": 4,
																						"column": 2
																					},
																					"AsideReason__": {
																						"line": 5,
																						"column": 2
																					},
																					"LabelAsideReason__": {
																						"line": 5,
																						"column": 1
																					},
																					"VerificationDate__": {
																						"line": 6,
																						"column": 2
																					},
																					"LabelVerificationDate__": {
																						"line": 6,
																						"column": 1
																					},
																					"ReceptionMethod__": {
																						"line": 9,
																						"column": 2
																					},
																					"LabelReceptionMethod__": {
																						"line": 9,
																						"column": 1
																					},
																					"BackToAPReason__": {
																						"line": 11,
																						"column": 2
																					},
																					"LabelBackToAPReason__": {
																						"line": 11,
																						"column": 1
																					},
																					"RejectReason__": {
																						"line": 12,
																						"column": 2
																					},
																					"LabelRejectReason__": {
																						"line": 12,
																						"column": 1
																					},
																					"ERP__": {
																						"line": 2,
																						"column": 2
																					},
																					"LabelERP__": {
																						"line": 2,
																						"column": 1
																					},
																					"DigitalSignature__": {
																						"line": 13,
																						"column": 2
																					},
																					"LabelDigitalSignature__": {
																						"line": 13,
																						"column": 1
																					},
																					"Configuration__": {
																						"line": 14,
																						"column": 2
																					},
																					"LabelConfiguration__": {
																						"line": 14,
																						"column": 1
																					},
																					"ArchiveRuidEx__": {
																						"line": 15,
																						"column": 2
																					},
																					"LabelArchiveRuidEx__": {
																						"line": 15,
																						"column": 1
																					},
																					"ArchiveProcessLink__": {
																						"line": 16,
																						"column": 2
																					},
																					"LabelArchiveProcessLink__": {
																						"line": 16,
																						"column": 1
																					},
																					"ArchiveProcessLinkGenerated__": {
																						"line": 17,
																						"column": 2
																					},
																					"LabelArchiveProcessLinkGenerated__": {
																						"line": 17,
																						"column": 1
																					},
																					"ScheduledActionDate__": {
																						"line": 7,
																						"column": 2
																					},
																					"LabelScheduledActionDate__": {
																						"line": 7,
																						"column": 1
																					},
																					"ScheduledAction__": {
																						"line": 8,
																						"column": 2
																					},
																					"LabelScheduledAction__": {
																						"line": 8,
																						"column": 1
																					},
																					"SourceDocument__": {
																						"line": 10,
																						"column": 2
																					},
																					"LabelSourceDocument__": {
																						"line": 10,
																						"column": 1
																					},
																					"ManuallyModifiedFieldsCount__": {
																						"line": 18,
																						"column": 2
																					},
																					"LabelManuallyModifiedFieldsCount__": {
																						"line": 18,
																						"column": 1
																					},
																					"AutomaticallyModifiedFieldsCount__": {
																						"line": 19,
																						"column": 2
																					},
																					"LabelAutomaticallyModifiedFieldsCount__": {
																						"line": 19,
																						"column": 1
																					},
																					"PaidOnTime__": {
																						"line": 20,
																						"column": 2
																					},
																					"LabelPaidOnTime__": {
																						"line": 20,
																						"column": 1
																					},
																					"HasBeenOnHold__": {
																						"line": 21,
																						"column": 2
																					},
																					"LabelHasBeenOnHold__": {
																						"line": 21,
																						"column": 1
																					}
																				},
																				"lines": 21,
																				"columns": 2,
																				"colspans": [
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[]
																				],
																				"version": 0
																			},
																			"stamp": 51,
																			"*": {
																				"LabelERP__": {
																					"type": "Label",
																					"data": [
																						"ERP__"
																					],
																					"options": {
																						"label": "_ERP",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 52
																				},
																				"ERP__": {
																					"type": "ComboBox",
																					"data": [
																						"ERP__"
																					],
																					"options": {
																						"possibleValues": {
																							"0": "_SAP",
																							"1": "_Generic",
																							"2": "_EBS",
																							"3": "_NAV",
																							"4": "_JDE",
																							"5": "_SAPS4CLOUD"
																						},
																						"version": 1,
																						"keyValueMode": true,
																						"possibleKeys": {
																							"0": "SAP",
																							"1": "generic",
																							"2": "EBS",
																							"3": "NAV",
																							"4": "JDE",
																							"5": "SAPS4CLOUD"
																						},
																						"label": "_ERP",
																						"activable": true,
																						"width": "180",
																						"hidden": true,
																						"maxNbLinesToDisplay": 10
																					},
																					"stamp": 53
																				},
																				"LabelCompanyCode__": {
																					"type": "Label",
																					"data": [
																						"CompanyCode__"
																					],
																					"options": {
																						"label": "_Company code",
																						"version": 0
																					},
																					"stamp": 54
																				},
																				"CompanyCode__": {
																					"type": "DatabaseComboBox",
																					"data": [
																						"CompanyCode__"
																					],
																					"options": {
																						"browsable": true,
																						"label": "_Company code",
																						"activable": true,
																						"width": "180",
																						"PreFillFTS": true,
																						"fTSmaxRecords": "20",
																						"version": 0,
																						"autoCompleteSearchMode": "startswith",
																						"searchMode": "matches"
																					},
																					"stamp": 55
																				},
																				"InvoiceStatus__": {
																					"type": "ComboBox",
																					"data": [
																						"InvoiceStatus__"
																					],
																					"options": {
																						"possibleValues": {
																							"0": "Received",
																							"1": "To verify",
																							"2": "To approve",
																							"3": "To post",
																							"4": "On hold",
																							"5": "To pay",
																							"6": "Rejected",
																							"7": "Set aside",
																							"8": "Paid",
																							"9": "Reversed",
																							"10": "Expired"
																						},
																						"keyValueMode": true,
																						"possibleKeys": {
																							"0": "Received",
																							"1": "To verify",
																							"2": "To approve",
																							"3": "To post",
																							"4": "On hold",
																							"5": "To pay",
																							"6": "Rejected",
																							"7": "Set aside",
																							"8": "Paid",
																							"9": "Reversed",
																							"10": "Expired"
																						},
																						"label": "_Invoice status",
																						"activable": true,
																						"width": "180",
																						"readonly": true,
																						"version": 1,
																						"readonlyIsText": false,
																						"maxNbLinesToDisplay": 10
																					},
																					"stamp": 56
																				},
																				"LabelInvoiceStatus__": {
																					"type": "Label",
																					"data": [
																						"InvoiceStatus__"
																					],
																					"options": {
																						"label": "_Invoice status",
																						"version": 0
																					},
																					"stamp": 57
																				},
																				"InvoiceType__": {
																					"type": "ComboBox",
																					"data": [
																						"InvoiceType__"
																					],
																					"options": {
																						"possibleValues": {
																							"0": "Non-PO Invoice",
																							"1": "PO Invoice",
																							"2": "PO Invoice (as FI)",
																							"3": "Consignment"
																						},
																						"keyValueMode": true,
																						"possibleKeys": {
																							"0": "Non-PO Invoice",
																							"1": "PO Invoice",
																							"2": "PO Invoice (as FI)",
																							"3": "Consignment"
																						},
																						"label": "_Invoice type",
																						"activable": true,
																						"width": "180",
																						"version": 1,
																						"readonlyIsText": false,
																						"maxNbLinesToDisplay": 10
																					},
																					"stamp": 58
																				},
																				"LabelInvoiceType__": {
																					"type": "Label",
																					"data": [
																						"InvoiceType__"
																					],
																					"options": {
																						"label": "_Invoice type",
																						"version": 0
																					},
																					"stamp": 59
																				},
																				"LabelAsideReason__": {
																					"type": "Label",
																					"data": [
																						"AsideReason__"
																					],
																					"options": {
																						"label": "_AsideReason",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 60
																				},
																				"AsideReason__": {
																					"type": "ComboBox",
																					"data": [
																						"AsideReason__"
																					],
																					"options": {
																						"possibleValues": {
																							"0": "",
																							"1": "Waiting for goods receipt",
																							"2": "Quantity variance",
																							"3": "Price variance",
																							"4": "Schedule variance",
																							"5": "Quality inspection",
																							"6": "Waiting for credit memo",
																							"7": "Other reason"
																						},
																						"keyValueMode": true,
																						"possibleKeys": {
																							"0": "-- none --",
																							"1": "Waiting for goods receipt",
																							"2": "Quantity variance",
																							"3": "Price variance",
																							"4": "Schedule variance",
																							"5": "Quality inspection",
																							"6": "Waiting for credit memo",
																							"7": "Other reason"
																						},
																						"label": "_AsideReason",
																						"activable": true,
																						"width": "180",
																						"readonly": true,
																						"version": 1,
																						"readonlyIsText": false,
																						"hidden": true,
																						"maxNbLinesToDisplay": 10
																					},
																					"stamp": 61
																				},
																				"LabelVerificationDate__": {
																					"type": "Label",
																					"data": [
																						"VerificationDate__"
																					],
																					"options": {
																						"label": "_ViewVerificationDate",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 62
																				},
																				"VerificationDate__": {
																					"type": "DateTime",
																					"data": [
																						"VerificationDate__"
																					],
																					"options": {
																						"displayLongFormat": false,
																						"label": "_VerificationDate",
																						"activable": true,
																						"width": 230,
																						"readonly": true,
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 63
																				},
																				"LabelScheduledActionDate__": {
																					"type": "Label",
																					"data": [
																						"ScheduledActionDate__"
																					],
																					"options": {
																						"label": "_ScheduledActionDate",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 64
																				},
																				"ScheduledActionDate__": {
																					"type": "DateTime",
																					"data": [
																						"ScheduledActionDate__"
																					],
																					"options": {
																						"displayLongFormat": false,
																						"label": "_ScheduledActionDate",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"readonly": true,
																						"autocompletable": false,
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 65
																				},
																				"LabelScheduledAction__": {
																					"type": "Label",
																					"data": [
																						"ScheduledAction__"
																					],
																					"options": {
																						"label": "_ScheduledAction",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 66
																				},
																				"ScheduledAction__": {
																					"type": "ShortText",
																					"data": [
																						"ScheduledAction__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"label": "_ScheduledAction",
																						"activable": true,
																						"width": 230,
																						"version": 0,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"readonly": true,
																						"browsable": false,
																						"autocompletable": false,
																						"hidden": true
																					},
																					"stamp": 67
																				},
																				"LabelReceptionMethod__": {
																					"type": "Label",
																					"data": [
																						"ReceptionMethod__"
																					],
																					"options": {
																						"label": "_ReceptionMethod",
																						"version": 0
																					},
																					"stamp": 68
																				},
																				"ReceptionMethod__": {
																					"type": "ComboBox",
																					"data": [
																						"ReceptionMethod__"
																					],
																					"options": {
																						"possibleValues": {
																							"0": "Scan/Other",
																							"1": "Email",
																							"2": "Vendor portal",
																							"3": "EDI",
																							"4": "Claims"
																						},
																						"keyValueMode": true,
																						"possibleKeys": {
																							"0": "Scan/Other",
																							"1": "Email",
																							"2": "Vendor portal",
																							"3": "EDI",
																							"4": "Claims"
																						},
																						"label": "_ReceptionMethod",
																						"activable": true,
																						"width": "180",
																						"readonly": true,
																						"version": 1,
																						"maxNbLinesToDisplay": 10
																					},
																					"stamp": 69
																				},
																				"LabelSourceDocument__": {
																					"type": "Label",
																					"data": [
																						"SourceDocument__"
																					],
																					"options": {
																						"label": "_SourceDocument",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 70
																				},
																				"SourceDocument__": {
																					"type": "ShortText",
																					"data": [
																						"SourceDocument__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"label": "_SourceDocument",
																						"activable": true,
																						"width": "180",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"readonly": true,
																						"browsable": false,
																						"hidden": true
																					},
																					"stamp": 71
																				},
																				"LabelBackToAPReason__": {
																					"type": "Label",
																					"data": [
																						"BackToAPReason__"
																					],
																					"options": {
																						"label": "_BackToAPReason",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 72
																				},
																				"BackToAPReason__": {
																					"type": "ComboBox",
																					"data": [
																						"BackToAPReason__"
																					],
																					"options": {
																						"possibleValues": {
																							"0": "",
																							"1": "Incorrect data capture",
																							"2": "Wrong workflow: not in charge",
																							"3": "Wrong analytical assignment",
																							"4": "Cost center / GL account is missing",
																							"5": "Other reason"
																						},
																						"version": 1,
																						"keyValueMode": true,
																						"possibleKeys": {
																							"0": "-- none --",
																							"1": "Incorrect data capture",
																							"2": "Wrong workflow: not in charge",
																							"3": "Wrong analytical assignment",
																							"4": "Cost center / GL account is missing",
																							"5": "Other reason"
																						},
																						"label": "_BackToAPReason",
																						"activable": true,
																						"width": "180",
																						"readonly": true,
																						"hidden": true,
																						"maxNbLinesToDisplay": 10
																					},
																					"stamp": 73
																				},
																				"LabelRejectReason__": {
																					"type": "Label",
																					"data": [
																						"RejectReason__"
																					],
																					"options": {
																						"label": "_RejectReason",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 74
																				},
																				"RejectReason__": {
																					"type": "ComboBox",
																					"data": [
																						"RejectReason__"
																					],
																					"options": {
																						"possibleValues": {
																							"0": "",
																							"1": "Document already processed",
																							"2": "Document is not an invoice",
																							"3": "Invoice is not compliant",
																							"4": "Unreadable invoice / bad quality scan",
																							"5": "Invalid signature",
																							"6": "Other reason"
																						},
																						"version": 1,
																						"keyValueMode": true,
																						"possibleKeys": {
																							"0": "-- none --",
																							"1": "Document already processed",
																							"2": "Document is not an invoice",
																							"3": "Invoice is not compliant",
																							"4": "Unreadable invoice / bad quality scan",
																							"5": "Invalid signature",
																							"6": "Other reason"
																						},
																						"label": "_RejectReason",
																						"activable": true,
																						"width": 230,
																						"readonly": true,
																						"hidden": true,
																						"maxNbLinesToDisplay": 10
																					},
																					"stamp": 75
																				},
																				"LabelDigitalSignature__": {
																					"type": "Label",
																					"data": [
																						"DigitalSignature__"
																					],
																					"options": {
																						"label": "_DigitalSignature",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 76
																				},
																				"DigitalSignature__": {
																					"type": "ComboBox",
																					"data": [
																						"DigitalSignature__"
																					],
																					"options": {
																						"possibleValues": {
																							"0": "_Unknown",
																							"1": "_Not signed",
																							"2": "_SignatureVerified",
																							"3": "_Invalid Corrupted",
																							"4": "_Invalid parameters",
																							"5": "_Signed",
																							"6": "_Signature failed",
																							"7": "_SignatureNotVerified"
																						},
																						"version": 1,
																						"keyValueMode": true,
																						"possibleKeys": {
																							"0": "Unknown",
																							"1": "Not signed",
																							"2": "Verified",
																							"3": "Invalid Corrupted",
																							"4": "Invalid parameters",
																							"5": "Signed",
																							"6": "Signature failed",
																							"7": "SignatureNotVerified"
																						},
																						"label": "_DigitalSignature",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"hidden": true,
																						"maxNbLinesToDisplay": 10
																					},
																					"stamp": 77
																				},
																				"LabelConfiguration__": {
																					"type": "Label",
																					"data": [
																						"Configuration__"
																					],
																					"options": {
																						"label": "_Configuration",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 78
																				},
																				"Configuration__": {
																					"type": "ShortText",
																					"data": [
																						"Configuration__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"label": "_Configuration",
																						"activable": true,
																						"width": 230,
																						"version": 0,
																						"helpText": "",
																						"browsable": false,
																						"hidden": true
																					},
																					"stamp": 79
																				},
																				"LabelArchiveRuidEx__": {
																					"type": "Label",
																					"data": [
																						"ArchiveRuidEx__"
																					],
																					"options": {
																						"label": "_ArchiveRuidEx",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 80
																				},
																				"ArchiveRuidEx__": {
																					"type": "ShortText",
																					"data": [
																						"ArchiveRuidEx__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"label": "_ArchiveRuidEx",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"readonly": true,
																						"browsable": false,
																						"autocompletable": false,
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 81
																				},
																				"LabelArchiveProcessLink__": {
																					"type": "Label",
																					"data": [
																						"ArchiveProcessLink__"
																					],
																					"options": {
																						"label": "_ArchiveProcessLink",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 82
																				},
																				"ArchiveProcessLink__": {
																					"type": "ShortText",
																					"data": [
																						"ArchiveProcessLink__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"label": "_ArchiveProcessLink",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"readonly": true,
																						"browsable": false,
																						"autocompletable": false,
																						"version": 0,
																						"length": 200,
																						"hidden": true
																					},
																					"stamp": 83
																				},
																				"ArchiveProcessLinkGenerated__": {
																					"type": "Link",
																					"data": false,
																					"options": {
																						"version": 1,
																						"link": "",
																						"openInCurrentWindow": false,
																						"label": "_ArchiveProcessLink",
																						"text": "_ClickHere",
																						"width": "",
																						"hidden": true
																					},
																					"stamp": 84
																				},
																				"LabelArchiveProcessLinkGenerated__": {
																					"type": "Label",
																					"data": false,
																					"options": {
																						"label": "_ArchiveProcessLink",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 85
																				},
																				"LabelManuallyModifiedFieldsCount__": {
																					"type": "Label",
																					"data": [
																						"ManuallyModifiedFieldsCount__"
																					],
																					"options": {
																						"label": "_NumberOfManuallyModifiedFields",
																						"hidden": true,
																						"version": 0
																					},
																					"stamp": 797
																				},
																				"ManuallyModifiedFieldsCount__": {
																					"type": "Integer",
																					"data": [
																						"ManuallyModifiedFieldsCount__"
																					],
																					"options": {
																						"version": 2,
																						"textSize": "S",
																						"textAlignment": "right",
																						"textStyle": "default",
																						"textColor": "default",
																						"integer": true,
																						"label": "_NumberOfManuallyModifiedFields",
																						"precision_internal": 0,
																						"precision_current": 0,
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"helpIconPosition": "Right",
																						"enablePlusMinus": false,
																						"readonly": true,
																						"browsable": false,
																						"hidden": true
																					},
																					"stamp": 798
																				},
																				"LabelAutomaticallyModifiedFieldsCount__": {
																					"type": "Label",
																					"data": [
																						"AutomaticallyModifiedFieldsCount__"
																					],
																					"options": {
																						"label": "_NumberOfAutomaticallyModifiedFields",
																						"hidden": true,
																						"version": 0
																					},
																					"stamp": 799
																				},
																				"AutomaticallyModifiedFieldsCount__": {
																					"type": "Integer",
																					"data": [
																						"AutomaticallyModifiedFieldsCount__"
																					],
																					"options": {
																						"version": 2,
																						"textSize": "S",
																						"textAlignment": "right",
																						"textStyle": "default",
																						"textColor": "default",
																						"integer": true,
																						"label": "_NumberOfAutomaticallyModifiedFields",
																						"precision_internal": 0,
																						"precision_current": 0,
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"helpIconPosition": "Right",
																						"enablePlusMinus": false,
																						"browsable": false,
																						"hidden": true,
																						"readonly": true
																					},
																					"stamp": 800
																				},
																				"LabelPaidOnTime__": {
																					"type": "Label",
																					"data": [
																						"PaidOnTime__"
																					],
																					"options": {
																						"label": "_PaidOnTime",
																						"hidden": true,
																						"version": 0
																					},
																					"stamp": 801
																				},
																				"PaidOnTime__": {
																					"type": "CheckBox",
																					"data": [
																						"PaidOnTime__"
																					],
																					"options": {
																						"label": "_PaidOnTime",
																						"activable": true,
																						"width": 230,
																						"hidden": true,
																						"version": 0
																					},
																					"stamp": 802
																				},
																				"LabelHasBeenOnHold__": {
																					"type": "Label",
																					"data": [
																						"HasBeenOnHold__"
																					],
																					"options": {
																						"label": "_HasBeenOnHold",
																						"hidden": true,
																						"version": 0
																					},
																					"stamp": 803
																				},
																				"HasBeenOnHold__": {
																					"type": "CheckBox",
																					"data": [
																						"HasBeenOnHold__"
																					],
																					"options": {
																						"label": "_HasBeenOnHold",
																						"activable": true,
																						"width": 230,
																						"hidden": true,
																						"version": 0
																					},
																					"stamp": 804
																				}
																			}
																		}
																	}
																}
															}
														},
														"ERP_Details": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {
																	"collapsed": false
																},
																"labelLength": 120,
																"label": "_ERP Details",
																"version": 0,
																"hideTitleBar": false,
																"iconURL": "AP_ERP_Details.png",
																"TitleFontSize": "M",
																"hideActionButtons": true,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"hideTitle": false,
																"hidden": false,
																"sameHeightAsSiblings": false
															},
															"stamp": 86,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"ERPPostingDate__": "LabelERPPostingDate__",
																			"LabelERPPostingDate__": "ERPPostingDate__",
																			"SAP_Simulation_Result__": "LabelSAP_Simulation_Result__",
																			"LabelSAP_Simulation_Result__": "SAP_Simulation_Result__",
																			"ManualLink__": "LabelManualLink__",
																			"LabelManualLink__": "ManualLink__",
																			"ERPLinkingDate__": "LabelERPLinkingDate__",
																			"LabelERPLinkingDate__": "ERPLinkingDate__",
																			"GRIV__": "LabelGRIV__",
																			"LabelGRIV__": "GRIV__",
																			"ERPPaymentBlocked__": "LabelERPPaymentBlocked__",
																			"LabelERPPaymentBlocked__": "ERPPaymentBlocked__",
																			"ERPInvoiceNumber__": "LabelERPInvoiceNumber__",
																			"LabelERPInvoiceNumber__": "ERPInvoiceNumber__",
																			"ERPClearingDocumentNumber__": "LabelERPClearingDocumentNumber__",
																			"LabelERPClearingDocumentNumber__": "ERPClearingDocumentNumber__",
																			"ERPMMInvoiceNumber__": "LabelERPMMInvoiceNumber__",
																			"LabelERPMMInvoiceNumber__": "ERPMMInvoiceNumber__",
																			"BudgetExportStatus__": "LabelBudgetExportStatus__",
																			"LabelBudgetExportStatus__": "BudgetExportStatus__",
																			"ERPPostingError__": "LabelERPPostingError__",
																			"LabelERPPostingError__": "ERPPostingError__",
																			"ERPAckRuidEx__": "LabelERPAckRuidEx__",
																			"LabelERPAckRuidEx__": "ERPAckRuidEx__"
																		},
																		"version": 0
																	},
																	"stamp": 87,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"ERPPostingDate__": {
																						"line": 1,
																						"column": 2
																					},
																					"LabelERPPostingDate__": {
																						"line": 1,
																						"column": 1
																					},
																					"SAP_Simulation_Result__": {
																						"line": 4,
																						"column": 2
																					},
																					"LabelSAP_Simulation_Result__": {
																						"line": 4,
																						"column": 1
																					},
																					"ManualLink__": {
																						"line": 3,
																						"column": 2
																					},
																					"LabelManualLink__": {
																						"line": 3,
																						"column": 1
																					},
																					"ManualLinkExplanation__": {
																						"line": 5,
																						"column": 1
																					},
																					"ERPLinkingDate__": {
																						"line": 2,
																						"column": 2
																					},
																					"LabelERPLinkingDate__": {
																						"line": 2,
																						"column": 1
																					},
																					"GRIV__": {
																						"line": 6,
																						"column": 2
																					},
																					"LabelGRIV__": {
																						"line": 6,
																						"column": 1
																					},
																					"ERPPaymentBlocked__": {
																						"line": 7,
																						"column": 2
																					},
																					"LabelERPPaymentBlocked__": {
																						"line": 7,
																						"column": 1
																					},
																					"ERPInvoiceNumber__": {
																						"line": 8,
																						"column": 2
																					},
																					"LabelERPInvoiceNumber__": {
																						"line": 8,
																						"column": 1
																					},
																					"ERPClearingDocumentNumber__": {
																						"line": 9,
																						"column": 2
																					},
																					"LabelERPClearingDocumentNumber__": {
																						"line": 9,
																						"column": 1
																					},
																					"ERPMMInvoiceNumber__": {
																						"line": 10,
																						"column": 2
																					},
																					"LabelERPMMInvoiceNumber__": {
																						"line": 10,
																						"column": 1
																					},
																					"ERPPostingError__": {
																						"line": 11,
																						"column": 2
																					},
																					"LabelERPPostingError__": {
																						"line": 11,
																						"column": 1
																					},
																					"BudgetExportStatus__": {
																						"line": 12,
																						"column": 2
																					},
																					"LabelBudgetExportStatus__": {
																						"line": 12,
																						"column": 1
																					},
																					"ERPAckRuidEx__": {
																						"line": 13,
																						"column": 2
																					},
																					"LabelERPAckRuidEx__": {
																						"line": 13,
																						"column": 1
																					}
																				},
																				"lines": 13,
																				"columns": 2,
																				"colspans": [
																					[],
																					[],
																					[],
																					[],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[]
																				],
																				"version": 0
																			},
																			"stamp": 88,
																			"*": {
																				"LabelERPPostingDate__": {
																					"type": "Label",
																					"data": [
																						"ERPPostingDate__"
																					],
																					"options": {
																						"label": "_ERP Posting date",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 89
																				},
																				"ERPPostingDate__": {
																					"type": "RealDateTime",
																					"data": [
																						"ERPPostingDate__"
																					],
																					"options": {
																						"readonly": true,
																						"displayLongFormat": false,
																						"label": "_ERP Posting date",
																						"activable": true,
																						"width": 230,
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 90
																				},
																				"LabelERPLinkingDate__": {
																					"type": "Label",
																					"data": [
																						"ERPLinkingDate__"
																					],
																					"options": {
																						"label": "_ERP Linking date",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 91
																				},
																				"ERPLinkingDate__": {
																					"type": "RealDateTime",
																					"data": [
																						"ERPLinkingDate__"
																					],
																					"options": {
																						"readonly": true,
																						"displayLongFormat": false,
																						"label": "_ERP Linking date",
																						"activable": true,
																						"width": 230,
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 92
																				},
																				"LabelManualLink__": {
																					"type": "Label",
																					"data": [
																						"ManualLink__"
																					],
																					"options": {
																						"label": "_Manually Linked",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 93
																				},
																				"ManualLink__": {
																					"type": "CheckBox",
																					"data": [
																						"ManualLink__"
																					],
																					"options": {
																						"label": "_Manually Linked",
																						"activable": true,
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 94
																				},
																				"LabelSAP_Simulation_Result__": {
																					"type": "Label",
																					"data": false,
																					"options": {
																						"label": "_SAP_Simulation_Result",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 95
																				},
																				"SAP_Simulation_Result__": {
																					"type": "HTML",
																					"data": false,
																					"options": {
																						"label": "SAP_Simulation_Result__",
																						"css": "@ {\n\twhite-space: nowrap;\n}@ \nDIV.SAPError\n{\tcolor: #E42518;\n}\nDIV.TitleLevel2\n{\n\tborder-bottom: 1px solid #A0A5A8;\n\tmargin: 10px 0px 10px 0px;\n\tposition: relative;\n\tvertical-align: bottom;\n\twidth: 100%;\n}@ \nDIV.TitleLevel2 H2\n{\n\tpadding-left: 5px;\n\tpadding-top: 3px;\t\n\tbackground-color: #737A7E;\n\tdisplay: inline;\n\tcolor: white;\n\tfont-size: 11px;\n\tfont-weight: bold;\n\tpadding-right: 5px;\n}@ \nDIV.Panel_Tab_Content DIV.TitleLevel2 H2\n{\n\tbackground-color: #456b8c;\n\tcolor: white;\n}@ \nTR.CheckList_ListHeader,@  TR.list_header\n{\n\tbackground-color: #727a7e;\n\tcolor: White;\n\tborder-collapse: collapse;\n}@ \nTD.CheckList_ListHeader\n{\n\tpadding-left: 5px;\n}@ \nTD.list_header_text\n{\n\tborder: 1px solid White;\n\tpadding: 0px 2px 0px 2px;\n\tborder-collapse: collapse;\n}@ \nTD.list_header_text A\n{\n\tcolor: White;\n\ttext-decoration: none;\n}@ \nA.list_header_link\n{\n\tcolor: White;\n\ttext-decoration: none;\n}@ \nA.list_header_link:hover\n{\n\ttext-decoration: underline;\n}@ \nTR.CheckList_ListItem,@  TR.edr-L1\n{\n\tbackground-color: #E8E9EB !important;\n}@ \nTR.edr-L1,@  TR.edr-L2\n{\n\tpadding: 2px !important;\n}@ \nTR.edr-L1 TD A,@  TR.edr-L2 TD A\n{\n\tcolor: #000000;\n\ttext-decoration: none;\n}@ \nTR.edr-L1 TD SPAN.Browsebutton A,@  TR.edr-L2 TD SPAN.Browsebutton A\n{\n\tcolor:white; \n\ttext-decoration:none;\n}@ \nTD.list_text\n{\n\tborder: 1px solid White;\n\tpadding: 0px 2px 0px 2px;\n\theight: 25px;\n}@ \nTD.list_textInteger\n{\n\tborder: 1px solid White;\n\tpadding: 0px 2px 0px 2px;\n\theight: 25px;\n\ttext-align: right;\n}@ \nTD.list_textHidden\n{\n\tdisplay: none;\n}@ \nTR.list_textHidden\n{\n\tdisplay: none;\n\theight: 0px;\n}@ \nTR.CheckList_ListItemAlternate,@  TR.edr-L2\n{\n\tbackground-color: #E8E9EB !important;\n}@  \nTABLE.simulationTableSub{\n\tpadding-bottom: 10px;\n}@ \n",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 96
																				},
																				"ManualLinkExplanation__": {
																					"type": "Text",
																					"data": false,
																					"options": {
																						"marge": 20,
																						"label": "_Manual Link Explanation",
																						"version": 0,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"backgroundcolor": "default",
																						"iconClass": "",
																						"hidden": true
																					},
																					"stamp": 97
																				},
																				"LabelGRIV__": {
																					"type": "Label",
																					"data": [
																						"GRIV__"
																					],
																					"options": {
																						"label": "_GRIV",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 98
																				},
																				"GRIV__": {
																					"type": "CheckBox",
																					"data": [
																						"GRIV__"
																					],
																					"options": {
																						"label": "_GRIV",
																						"activable": true,
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 99
																				},
																				"LabelERPPaymentBlocked__": {
																					"type": "Label",
																					"data": [
																						"ERPPaymentBlocked__"
																					],
																					"options": {
																						"label": "_ERP payment blocked",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 100
																				},
																				"ERPPaymentBlocked__": {
																					"type": "CheckBox",
																					"data": [
																						"ERPPaymentBlocked__"
																					],
																					"options": {
																						"label": "_ERP payment blocked",
																						"activable": true,
																						"readonly": true,
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 101
																				},
																				"LabelERPInvoiceNumber__": {
																					"type": "Label",
																					"data": [
																						"ERPInvoiceNumber__"
																					],
																					"options": {
																						"label": "_ERP Invoice number",
																						"version": 0
																					},
																					"stamp": 102
																				},
																				"ERPInvoiceNumber__": {
																					"type": "DatabaseComboBox",
																					"data": [
																						"ERPInvoiceNumber__"
																					],
																					"options": {
																						"version": 1,
																						"label": "_ERP Invoice number",
																						"activable": true,
																						"width": "180",
																						"fTSmaxRecords": "20",
																						"browsable": false,
																						"readonly": true,
																						"autocompletable": false,
																						"autoCompleteSearchMode": "startswith",
																						"searchMode": "matches"
																					},
																					"stamp": 103
																				},
																				"LabelERPClearingDocumentNumber__": {
																					"type": "Label",
																					"data": [
																						"ERPClearingDocumentNumber__"
																					],
																					"options": {
																						"label": "_ERP Clearing document number",
																						"version": 0
																					},
																					"stamp": 104
																				},
																				"ERPClearingDocumentNumber__": {
																					"type": "DatabaseComboBox",
																					"data": [
																						"ERPClearingDocumentNumber__"
																					],
																					"options": {
																						"version": 1,
																						"label": "_ERP Clearing document number",
																						"activable": true,
																						"width": "180",
																						"fTSmaxRecords": "20",
																						"browsable": false,
																						"readonly": true,
																						"autocompletable": false,
																						"autoCompleteSearchMode": "startswith",
																						"searchMode": "matches"
																					},
																					"stamp": 105
																				},
																				"LabelERPMMInvoiceNumber__": {
																					"type": "Label",
																					"data": [
																						"ERPMMInvoiceNumber__"
																					],
																					"options": {
																						"label": "_ERP MM Invoice number",
																						"version": 0
																					},
																					"stamp": 106
																				},
																				"ERPMMInvoiceNumber__": {
																					"type": "DatabaseComboBox",
																					"data": [
																						"ERPMMInvoiceNumber__"
																					],
																					"options": {
																						"version": 1,
																						"label": "_ERP MM Invoice number",
																						"activable": true,
																						"width": "180",
																						"fTSmaxRecords": "20",
																						"browsable": false,
																						"readonly": true,
																						"autocompletable": false,
																						"autoCompleteSearchMode": "startswith",
																						"searchMode": "matches"
																					},
																					"stamp": 107
																				},
																				"LabelERPPostingError__": {
																					"type": "Label",
																					"data": [
																						"ERPPostingError__"
																					],
																					"options": {
																						"label": "_ERP posting error",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 108
																				},
																				"ERPPostingError__": {
																					"type": "ShortText",
																					"data": [
																						"ERPPostingError__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"label": "_ERP posting error",
																						"activable": true,
																						"width": 230,
																						"length": 1024,
																						"readonly": true,
																						"browsable": false,
																						"autocompletable": false,
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 109
																				},
																				"LabelBudgetExportStatus__": {
																					"type": "Label",
																					"data": [
																						"BudgetExportStatus__"
																					],
																					"options": {
																						"label": "_Budget Export Status",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 110
																				},
																				"BudgetExportStatus__": {
																					"type": "ShortText",
																					"data": [
																						"BudgetExportStatus__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"label": "_Budget Export Status",
																						"activable": true,
																						"width": 230,
																						"readonly": true,
																						"browsable": false,
																						"autocompletable": false,
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 111
																				},
																				"LabelERPAckRuidEx__": {
																					"type": "Label",
																					"data": [
																						"ERPAckRuidEx__"
																					],
																					"options": {
																						"label": "_ERPAckRUIDEX",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 112
																				},
																				"ERPAckRuidEx__": {
																					"type": "ShortText",
																					"data": [
																						"ERPAckRuidEx__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"label": "_ERPAckRUIDEX",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"browsable": false,
																						"readonly": true,
																						"autocompletable": false,
																						"hidden": true
																					},
																					"stamp": 113
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-1": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 114,
													"*": {
														"HeaderDataPanel": {
															"type": "PanelData",
															"options": {
																"border": {},
																"version": 0,
																"labelLength": 120,
																"label": "_Invoice Details",
																"hideTitleBar": false,
																"iconURL": "AP_HeaderDataPanel.png",
																"TitleFontSize": "M",
																"hideActionButtons": true,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"hideTitle": false,
																"sameHeightAsSiblings": false
															},
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"options": {
																		"controlsAssociation": {
																			"InvoiceNumber__": "LabelInvoiceNumber__",
																			"LabelInvoiceNumber__": "InvoiceNumber__",
																			"InvoiceDate__": "LabelInvoiceDate__",
																			"LabelInvoiceDate__": "InvoiceDate__",
																			"InvoiceAmount__": "LabelInvoiceAmount__",
																			"LabelInvoiceAmount__": "InvoiceAmount__",
																			"CurrentAttachmentFlag__": "LabelCurrentAttachmentFlag__",
																			"LabelCurrentAttachmentFlag__": "CurrentAttachmentFlag__",
																			"TaxAmount__": "LabelTaxAmount__",
																			"LabelTaxAmount__": "TaxAmount__",
																			"NetAmount__": "LabelNetAmount__",
																			"LabelNetAmount__": "NetAmount__",
																			"Balance__": "LabelBalance__",
																			"LabelBalance__": "Balance__",
																			"LabelOrderNumber__": "OrderNumber__",
																			"OrderNumber__": "LabelOrderNumber__",
																			"ExtractedNetAmount__": "LabelExtractedNetAmount__",
																			"LabelExtractedNetAmount__": "ExtractedNetAmount__",
																			"PostingDate__": "LabelPostingDate__",
																			"LabelPostingDate__": "PostingDate__",
																			"LocalInvoiceAmount__": "LabelLocalInvoiceAmount__",
																			"LabelLocalInvoiceAmount__": "LocalInvoiceAmount__",
																			"LocalNetAmount__": "LabelLocalNetAmount__",
																			"LabelLocalNetAmount__": "LocalNetAmount__",
																			"LocalTaxAmount__": "LabelLocalTaxAmount__",
																			"LabelLocalTaxAmount__": "LocalTaxAmount__",
																			"LocalCurrency__": "LabelLocalCurrency__",
																			"LabelLocalCurrency__": "LocalCurrency__",
																			"InvoiceCurrency__": "LabelInvoiceCurrency__",
																			"LabelInvoiceCurrency__": "InvoiceCurrency__",
																			"ExchangeRate__": "LabelExchangeRate__",
																			"LabelExchangeRate__": "ExchangeRate__",
																			"InvoiceDescription__": "LabelInvoiceDescription__",
																			"LabelInvoiceDescription__": "InvoiceDescription__",
																			"LastExportDate__": "LabelLastExportDate__",
																			"LabelLastExportDate__": "LastExportDate__",
																			"LastPaymentApprovalExportDate__": "LabelLastPaymentApprovalExportDate__",
																			"LabelLastPaymentApprovalExportDate__": "LastPaymentApprovalExportDate__",
																			"Investment__": "LabelInvestment__",
																			"LabelInvestment__": "Investment__",
																			"GoodIssue__": "LabelGoodIssue__",
																			"LabelGoodIssue__": "GoodIssue__",
																			"ContractNumber__": "LabelContractNumber__",
																			"LabelContractNumber__": "ContractNumber__"
																		},
																		"version": 0
																	},
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"options": {
																				"lines": 22,
																				"columns": 2,
																				"version": 0,
																				"ctrlsPos": {
																					"InvoiceNumber__": {
																						"line": 1,
																						"column": 2
																					},
																					"LabelInvoiceNumber__": {
																						"line": 1,
																						"column": 1
																					},
																					"LabelOrderNumber__": {
																						"line": 2,
																						"column": 1
																					},
																					"OrderNumber__": {
																						"line": 2,
																						"column": 2
																					},
																					"GoodIssue__": {
																						"line": 3,
																						"column": 2
																					},
																					"LabelGoodIssue__": {
																						"line": 3,
																						"column": 1
																					},
																					"ContractNumber__": {
																						"line": 4,
																						"column": 2
																					},
																					"LabelContractNumber__": {
																						"line": 4,
																						"column": 1
																					},
																					"InvoiceDate__": {
																						"line": 5,
																						"column": 2
																					},
																					"LabelInvoiceDate__": {
																						"line": 5,
																						"column": 1
																					},
																					"PostingDate__": {
																						"line": 6,
																						"column": 2
																					},
																					"LabelPostingDate__": {
																						"line": 6,
																						"column": 1
																					},
																					"InvoiceAmount__": {
																						"line": 7,
																						"column": 2
																					},
																					"LabelInvoiceAmount__": {
																						"line": 7,
																						"column": 1
																					},
																					"ExchangeRate__": {
																						"line": 8,
																						"column": 2
																					},
																					"LabelExchangeRate__": {
																						"line": 8,
																						"column": 1
																					},
																					"LocalInvoiceAmount__": {
																						"line": 9,
																						"column": 2
																					},
																					"LabelLocalInvoiceAmount__": {
																						"line": 9,
																						"column": 1
																					},
																					"NetAmount__": {
																						"line": 10,
																						"column": 2
																					},
																					"LabelNetAmount__": {
																						"line": 10,
																						"column": 1
																					},
																					"LocalNetAmount__": {
																						"line": 11,
																						"column": 2
																					},
																					"LabelLocalNetAmount__": {
																						"line": 11,
																						"column": 1
																					},
																					"TaxAmount__": {
																						"line": 12,
																						"column": 2
																					},
																					"LabelTaxAmount__": {
																						"line": 12,
																						"column": 1
																					},
																					"LocalTaxAmount__": {
																						"line": 13,
																						"column": 2
																					},
																					"LabelLocalTaxAmount__": {
																						"line": 13,
																						"column": 1
																					},
																					"Balance__": {
																						"line": 14,
																						"column": 2
																					},
																					"LabelBalance__": {
																						"line": 14,
																						"column": 1
																					},
																					"CurrentAttachmentFlag__": {
																						"line": 15,
																						"column": 2
																					},
																					"LabelCurrentAttachmentFlag__": {
																						"line": 15,
																						"column": 1
																					},
																					"ExtractedNetAmount__": {
																						"line": 16,
																						"column": 2
																					},
																					"LabelExtractedNetAmount__": {
																						"line": 16,
																						"column": 1
																					},
																					"LocalCurrency__": {
																						"line": 17,
																						"column": 2
																					},
																					"LabelLocalCurrency__": {
																						"line": 17,
																						"column": 1
																					},
																					"InvoiceCurrency__": {
																						"line": 18,
																						"column": 2
																					},
																					"LabelInvoiceCurrency__": {
																						"line": 18,
																						"column": 1
																					},
																					"InvoiceDescription__": {
																						"line": 19,
																						"column": 2
																					},
																					"LabelInvoiceDescription__": {
																						"line": 19,
																						"column": 1
																					},
																					"LastExportDate__": {
																						"line": 20,
																						"column": 2
																					},
																					"LabelLastExportDate__": {
																						"line": 20,
																						"column": 1
																					},
																					"LastPaymentApprovalExportDate__": {
																						"line": 21,
																						"column": 2
																					},
																					"LabelLastPaymentApprovalExportDate__": {
																						"line": 21,
																						"column": 1
																					},
																					"Investment__": {
																						"line": 22,
																						"column": 2
																					},
																					"LabelInvestment__": {
																						"line": 22,
																						"column": 1
																					}
																				},
																				"colspans": [
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[]
																				]
																			},
																			"data": [
																				"fields"
																			],
																			"*": {
																				"LabelInvoiceNumber__": {
																					"type": "Label",
																					"data": [
																						"InvoiceNumber__"
																					],
																					"options": {
																						"label": "_Invoice number",
																						"version": 0
																					},
																					"stamp": 115
																				},
																				"LabelOrderNumber__": {
																					"type": "Label",
																					"data": [
																						"OrderNumber__"
																					],
																					"options": {
																						"label": "_Order number",
																						"version": 0
																					},
																					"stamp": 116
																				},
																				"InvoiceNumber__": {
																					"type": "ShortText",
																					"data": [
																						"InvoiceNumber__"
																					],
																					"options": {
																						"label": "_Invoice number",
																						"activable": true,
																						"width": "180",
																						"version": 0,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default"
																					},
																					"stamp": 117
																				},
																				"OrderNumber__": {
																					"type": "ShortText",
																					"data": [
																						"OrderNumber__"
																					],
																					"options": {
																						"label": "_Order number",
																						"activable": true,
																						"width": "180",
																						"browsable": true,
																						"version": 0,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default"
																					},
																					"stamp": 118
																				},
																				"LabelGoodIssue__": {
																					"type": "Label",
																					"data": [
																						"GoodIssue__"
																					],
																					"options": {
																						"label": "_GoodIssue",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 119
																				},
																				"GoodIssue__": {
																					"type": "DatabaseComboBox",
																					"data": [
																						"GoodIssue__"
																					],
																					"options": {
																						"label": "_GoodIssue",
																						"activable": true,
																						"width": "180",
																						"fTSmaxRecords": "20",
																						"version": 0,
																						"browsable": true,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"hidden": true,
																						"autoCompleteSearchMode": "startswith",
																						"searchMode": "matches"
																					},
																					"stamp": 120
																				},
																				"LabelContractNumber__": {
																					"type": "Label",
																					"data": [
																						"ContractNumber__"
																					],
																					"options": {
																						"label": "_ContractNumber",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 813
																				},
																				"ContractNumber__": {
																					"type": "DatabaseComboBox",
																					"data": [
																						"ContractNumber__"
																					],
																					"options": {
																						"version": 1,
																						"autoCompleteSearchMode": "startswith",
																						"searchMode": "matches",
																						"label": "_ContractNumber",
																						"activable": true,
																						"width": "180",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"helpIconPosition": "Right",
																						"minNbLines": 1,
																						"maxNbLines": 1,
																						"fTSmaxRecords": "20",
																						"browsable": true,
																						"PreFillFTS": true,
																						"RestrictSearch": true,
																						"hidden": true
																					},
																					"stamp": 814
																				},
																				"LabelPostingDate__": {
																					"type": "Label",
																					"data": [
																						"PostingDate__"
																					],
																					"options": {
																						"label": "_Posting date",
																						"version": 0
																					},
																					"stamp": 121
																				},
																				"PostingDate__": {
																					"type": "DateTime",
																					"data": [
																						"PostingDate__"
																					],
																					"options": {
																						"displayLongFormat": false,
																						"label": "_Posting date",
																						"activable": true,
																						"width": "180",
																						"version": 0
																					},
																					"stamp": 122
																				},
																				"LabelInvoiceDate__": {
																					"type": "Label",
																					"data": [
																						"InvoiceDate__"
																					],
																					"options": {
																						"label": "_Invoice date",
																						"version": 0
																					},
																					"stamp": 123
																				},
																				"InvoiceDate__": {
																					"type": "DateTime",
																					"data": [
																						"InvoiceDate__"
																					],
																					"options": {
																						"label": "_Invoice date",
																						"activable": true,
																						"width": "180",
																						"version": 0,
																						"displayLongFormat": false
																					},
																					"stamp": 124
																				},
																				"LabelInvoiceAmount__": {
																					"type": "Label",
																					"data": [
																						"InvoiceAmount__"
																					],
																					"options": {
																						"label": "_Invoice amount",
																						"version": 0
																					},
																					"stamp": 125
																				},
																				"InvoiceAmount__": {
																					"type": "Decimal",
																					"data": [
																						"InvoiceAmount__"
																					],
																					"options": {
																						"integer": false,
																						"label": "_Invoice amount",
																						"precision_internal": 2,
																						"activable": true,
																						"width": "180",
																						"version": 2,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"precision_current": 2,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"triggerHalfWidth": true,
																						"currencySource": "InvoiceCurrency__"
																					},
																					"stamp": 126
																				},
																				"LabelExchangeRate__": {
																					"type": "Label",
																					"data": [
																						"ExchangeRate__"
																					],
																					"options": {
																						"label": "_Exchange rate",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 127
																				},
																				"ExchangeRate__": {
																					"type": "ShortText",
																					"data": [
																						"ExchangeRate__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"label": "_Exchange rate",
																						"activable": true,
																						"width": "180",
																						"readonly": true,
																						"browsable": false,
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 128
																				},
																				"LabelLocalInvoiceAmount__": {
																					"type": "Label",
																					"data": [
																						"LocalInvoiceAmount__"
																					],
																					"options": {
																						"label": "_Local invoice amount",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 129
																				},
																				"LocalInvoiceAmount__": {
																					"type": "Decimal",
																					"data": [
																						"LocalInvoiceAmount__"
																					],
																					"options": {
																						"version": 2,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"integer": false,
																						"label": "_Local invoice amount",
																						"precision_internal": 2,
																						"activable": true,
																						"width": "180",
																						"readonly": true,
																						"precision_current": 2,
																						"hidden": true
																					},
																					"stamp": 130
																				},
																				"LabelNetAmount__": {
																					"type": "Label",
																					"data": [
																						"NetAmount__"
																					],
																					"options": {
																						"label": "_Net amount",
																						"version": 0
																					},
																					"stamp": 131
																				},
																				"NetAmount__": {
																					"type": "Decimal",
																					"data": [
																						"NetAmount__"
																					],
																					"options": {
																						"integer": false,
																						"label": "_Net amount",
																						"precision_internal": 2,
																						"activable": true,
																						"width": "180",
																						"version": 2,
																						"readonly": true,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"precision_current": 2,
																						"triggerHalfWidth": true,
																						"currencySource": "InvoiceCurrency__"
																					},
																					"stamp": 132
																				},
																				"LabelLocalNetAmount__": {
																					"type": "Label",
																					"data": [
																						"LocalNetAmount__"
																					],
																					"options": {
																						"label": "_Local net amount",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 133
																				},
																				"LocalNetAmount__": {
																					"type": "Decimal",
																					"data": [
																						"LocalNetAmount__"
																					],
																					"options": {
																						"version": 2,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"integer": false,
																						"label": "_Local net amount",
																						"precision_internal": 2,
																						"activable": true,
																						"width": "180",
																						"readonly": true,
																						"precision_current": 2,
																						"hidden": true
																					},
																					"stamp": 134
																				},
																				"LabelTaxAmount__": {
																					"type": "Label",
																					"data": [
																						"TaxAmount__"
																					],
																					"options": {
																						"label": "_Tax amount",
																						"version": 0
																					},
																					"stamp": 135
																				},
																				"TaxAmount__": {
																					"type": "Decimal",
																					"data": [
																						"TaxAmount__"
																					],
																					"options": {
																						"integer": false,
																						"label": "_Tax amount",
																						"precision_internal": 2,
																						"activable": true,
																						"width": "180",
																						"version": 2,
																						"readonly": true,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"precision_current": 2,
																						"triggerHalfWidth": true,
																						"currencySource": "InvoiceCurrency__"
																					},
																					"stamp": 136
																				},
																				"LabelLocalTaxAmount__": {
																					"type": "Label",
																					"data": [
																						"LocalTaxAmount__"
																					],
																					"options": {
																						"label": "_Local tax amount",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 137
																				},
																				"LocalTaxAmount__": {
																					"type": "Decimal",
																					"data": [
																						"LocalTaxAmount__"
																					],
																					"options": {
																						"version": 2,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"integer": false,
																						"label": "_Local tax amount",
																						"precision_internal": 2,
																						"activable": true,
																						"width": "180",
																						"readonly": true,
																						"precision_current": 2,
																						"hidden": true
																					},
																					"stamp": 138
																				},
																				"LabelBalance__": {
																					"type": "Label",
																					"data": [
																						"Balance__"
																					],
																					"options": {
																						"label": "_Balance",
																						"version": 0
																					},
																					"stamp": 139
																				},
																				"Balance__": {
																					"type": "Decimal",
																					"data": [
																						"Balance__"
																					],
																					"options": {
																						"integer": false,
																						"label": "_Balance",
																						"precision_internal": 2,
																						"activable": true,
																						"width": "180",
																						"readonly": true,
																						"version": 2,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"precision_current": 2,
																						"triggerHalfWidth": true,
																						"currencySource": "InvoiceCurrency__"
																					},
																					"stamp": 140
																				},
																				"LabelCurrentAttachmentFlag__": {
																					"type": "Label",
																					"data": [
																						"CurrentAttachmentFlag__"
																					],
																					"options": {
																						"label": "_FirstAttachRemovabled",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 141
																				},
																				"CurrentAttachmentFlag__": {
																					"type": "Integer",
																					"data": [
																						"CurrentAttachmentFlag__"
																					],
																					"options": {
																						"integer": true,
																						"label": "_FirstAttachRemovabled",
																						"precision_internal": 0,
																						"activable": true,
																						"width": 230,
																						"readonly": true,
																						"version": 0,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"browsable": false,
																						"precision_current": 0,
																						"hidden": true
																					},
																					"stamp": 142
																				},
																				"LabelExtractedNetAmount__": {
																					"type": "Label",
																					"data": [
																						"ExtractedNetAmount__"
																					],
																					"options": {
																						"label": "_Extracted net amount",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 143
																				},
																				"ExtractedNetAmount__": {
																					"type": "Decimal",
																					"data": [
																						"ExtractedNetAmount__"
																					],
																					"options": {
																						"integer": false,
																						"label": "_Extracted net amount",
																						"precision_internal": 2,
																						"activable": true,
																						"width": "180",
																						"version": 2,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"readonly": true,
																						"precision_current": 2,
																						"hidden": true
																					},
																					"stamp": 144
																				},
																				"LabelLocalCurrency__": {
																					"type": "Label",
																					"data": [
																						"LocalCurrency__"
																					],
																					"options": {
																						"label": "_Local currency",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 145
																				},
																				"LocalCurrency__": {
																					"type": "ShortText",
																					"data": [
																						"LocalCurrency__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"label": "_Local currency",
																						"activable": true,
																						"width": "180",
																						"readonly": true,
																						"browsable": false,
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 146
																				},
																				"LabelInvoiceCurrency__": {
																					"type": "Label",
																					"data": [
																						"InvoiceCurrency__"
																					],
																					"options": {
																						"label": "_Invoice currency",
																						"version": 0
																					},
																					"stamp": 147
																				},
																				"InvoiceCurrency__": {
																					"type": "DatabaseComboBox",
																					"data": [
																						"InvoiceCurrency__"
																					],
																					"options": {
																						"label": "_Invoice currency",
																						"activable": true,
																						"width": "180",
																						"PreFillFTS": true,
																						"fTSmaxRecords": "20",
																						"readonly": false,
																						"browsable": true,
																						"version": 0,
																						"autoCompleteSearchMode": "startswith",
																						"searchMode": "matches"
																					},
																					"stamp": 148
																				},
																				"LabelInvoiceDescription__": {
																					"type": "Label",
																					"data": [
																						"InvoiceDescription__"
																					],
																					"options": {
																						"label": "_Invoice description",
																						"version": 0
																					},
																					"stamp": 149
																				},
																				"InvoiceDescription__": {
																					"type": "ShortText",
																					"data": [
																						"InvoiceDescription__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"label": "_Invoice description",
																						"activable": true,
																						"width": "100%",
																						"browsable": false,
																						"autocompletable": false,
																						"version": 0
																					},
																					"stamp": 150
																				},
																				"LabelLastExportDate__": {
																					"type": "Label",
																					"data": [
																						"LastExportDate__"
																					],
																					"options": {
																						"label": "_Date Last Export",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 151
																				},
																				"LastExportDate__": {
																					"type": "DateTime",
																					"data": [
																						"LastExportDate__"
																					],
																					"options": {
																						"displayLongFormat": false,
																						"label": "_Date Last Export",
																						"activable": true,
																						"width": "180",
																						"readonly": true,
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 152
																				},
																				"LabelLastPaymentApprovalExportDate__": {
																					"type": "Label",
																					"data": [
																						"LastPaymentApprovalExportDate__"
																					],
																					"options": {
																						"label": "_Date Last Payment Approval Export",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 153
																				},
																				"LastPaymentApprovalExportDate__": {
																					"type": "DateTime",
																					"data": [
																						"LastPaymentApprovalExportDate__"
																					],
																					"options": {
																						"displayLongFormat": false,
																						"label": "_Date Last Payment Approval Export",
																						"activable": true,
																						"width": "180",
																						"readonly": true,
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 154
																				},
																				"LabelInvestment__": {
																					"type": "Label",
																					"data": [
																						"Investment__"
																					],
																					"options": {
																						"label": "_Investment",
																						"version": 0
																					},
																					"stamp": 155
																				},
																				"Investment__": {
																					"type": "CheckBox",
																					"data": [
																						"Investment__"
																					],
																					"options": {
																						"label": "_Investment",
																						"activable": true,
																						"version": 0
																					},
																					"stamp": 156
																				}
																			},
																			"stamp": 157
																		}
																	},
																	"stamp": 158,
																	"data": []
																}
															},
															"stamp": 159,
															"data": []
														},
														"VendorInformation": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": 120,
																"label": "_Vendor Information",
																"version": 0,
																"hideTitleBar": false,
																"iconURL": "AP_VendorInformation.png",
																"TitleFontSize": "M",
																"hideActionButtons": true,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"hideTitle": false,
																"sameHeightAsSiblings": false
															},
															"stamp": 160,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"VendorName__": "LabelVendorName__",
																			"LabelVendorName__": "VendorName__",
																			"VendorStreet__": "LabelVendorStreet__",
																			"LabelVendorStreet__": "VendorStreet__",
																			"VendorCity__": "LabelVendorCity__",
																			"LabelVendorCity__": "VendorCity__",
																			"VendorZipCode__": "LabelVendorZipCode__",
																			"LabelVendorZipCode__": "VendorZipCode__",
																			"VendorRegion__": "LabelVendorRegion__",
																			"LabelVendorRegion__": "VendorRegion__",
																			"VendorCountry__": "LabelVendorCountry__",
																			"LabelVendorCountry__": "VendorCountry__",
																			"VendorNumber__": "LabelVendorNumber__",
																			"LabelVendorNumber__": "VendorNumber__",
																			"LabelPortalRuidEx__": "PortalRuidEx__",
																			"PortalRuidEx__": "LabelPortalRuidEx__",
																			"VendorContactEmail__": "LabelVendorContactEmail__",
																			"LabelVendorContactEmail__": "VendorContactEmail__",
																			"VendorPOBox__": "LabelVendorPOBox__",
																			"LabelVendorPOBox__": "VendorPOBox__",
																			"WithholdingTax__": "LabelWithholdingTax__",
																			"LabelWithholdingTax__": "WithholdingTax__",
																			"SelectedBankAccountID__": "LabelSelectedBankAccountID__",
																			"LabelSelectedBankAccountID__": "SelectedBankAccountID__",
																			"ExtractedIBAN__": "LabelExtractedIBAN__",
																			"LabelExtractedIBAN__": "ExtractedIBAN__"
																		},
																		"version": 0
																	},
																	"stamp": 161,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"VendorName__": {
																						"line": 2,
																						"column": 2
																					},
																					"LabelVendorName__": {
																						"line": 2,
																						"column": 1
																					},
																					"VendorStreet__": {
																						"line": 3,
																						"column": 2
																					},
																					"LabelVendorStreet__": {
																						"line": 3,
																						"column": 1
																					},
																					"VendorCity__": {
																						"line": 5,
																						"column": 2
																					},
																					"LabelVendorCity__": {
																						"line": 5,
																						"column": 1
																					},
																					"VendorZipCode__": {
																						"line": 6,
																						"column": 2
																					},
																					"LabelVendorZipCode__": {
																						"line": 6,
																						"column": 1
																					},
																					"VendorRegion__": {
																						"line": 7,
																						"column": 2
																					},
																					"LabelVendorRegion__": {
																						"line": 7,
																						"column": 1
																					},
																					"VendorCountry__": {
																						"line": 8,
																						"column": 2
																					},
																					"LabelVendorCountry__": {
																						"line": 8,
																						"column": 1
																					},
																					"VendorNumber__": {
																						"line": 1,
																						"column": 2
																					},
																					"LabelVendorNumber__": {
																						"line": 1,
																						"column": 1
																					},
																					"LabelPortalRuidEx__": {
																						"line": 9,
																						"column": 1
																					},
																					"PortalRuidEx__": {
																						"line": 9,
																						"column": 2
																					},
																					"VendorContactEmail__": {
																						"line": 10,
																						"column": 2
																					},
																					"LabelVendorContactEmail__": {
																						"line": 10,
																						"column": 1
																					},
																					"VendorPOBox__": {
																						"line": 4,
																						"column": 2
																					},
																					"LabelVendorPOBox__": {
																						"line": 4,
																						"column": 1
																					},
																					"WithholdingTax__": {
																						"line": 11,
																						"column": 2
																					},
																					"LabelWithholdingTax__": {
																						"line": 11,
																						"column": 1
																					},
																					"LabelSelectedBankAccountID__": {
																						"line": 12,
																						"column": 1
																					},
																					"SelectedBankAccountID__": {
																						"line": 12,
																						"column": 2
																					},
																					"LabelExtractedIBAN__": {
																						"line": 13,
																						"column": 1
																					},
																					"ExtractedIBAN__": {
																						"line": 13,
																						"column": 2
																					}
																				},
																				"lines": 13,
																				"columns": 2,
																				"colspans": [
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[]
																				],
																				"version": 0
																			},
																			"stamp": 162,
																			"*": {
																				"PortalRuidEx__": {
																					"type": "ShortText",
																					"data": [
																						"PortalRuidEx__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"label": "_Portal Ruidex",
																						"activable": true,
																						"width": 230,
																						"readonly": true,
																						"browsable": false,
																						"autocompletable": false,
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 163
																				},
																				"LabelPortalRuidEx__": {
																					"type": "Label",
																					"data": [
																						"PortalRuidEx__"
																					],
																					"options": {
																						"label": "_Portal Ruidex",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 164
																				},
																				"LabelVendorNumber__": {
																					"type": "Label",
																					"data": [
																						"VendorNumber__"
																					],
																					"options": {
																						"label": "_Number",
																						"version": 0
																					},
																					"stamp": 165
																				},
																				"VendorNumber__": {
																					"type": "DatabaseComboBox",
																					"data": [
																						"VendorNumber__"
																					],
																					"options": {
																						"label": "_Number",
																						"activable": true,
																						"width": "180",
																						"fTSmaxRecords": "20",
																						"version": 0,
																						"PreFillFTS": true,
																						"browsable": true,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"autoCompleteSearchMode": "startswith",
																						"searchMode": "matches",
																						"helpIconPosition": "Right",
																						"minNbLines": 1,
																						"maxNbLines": 1
																					},
																					"stamp": 166
																				},
																				"LabelVendorName__": {
																					"type": "Label",
																					"data": [
																						"VendorName__"
																					],
																					"options": {
																						"label": "_Name",
																						"version": 0
																					},
																					"stamp": 167
																				},
																				"VendorName__": {
																					"type": "DatabaseComboBox",
																					"data": [
																						"VendorName__"
																					],
																					"options": {
																						"label": "_Name",
																						"activable": true,
																						"width": "180",
																						"fTSmaxRecords": "20",
																						"version": 0,
																						"PreFillFTS": true,
																						"browsable": true,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"autoCompleteSearchMode": "startswith",
																						"searchMode": "matches"
																					},
																					"stamp": 168
																				},
																				"LabelVendorPOBox__": {
																					"type": "Label",
																					"data": [
																						"VendorPOBox__"
																					],
																					"options": {
																						"label": "_POBox",
																						"version": 0
																					},
																					"stamp": 169
																				},
																				"VendorPOBox__": {
																					"type": "ShortText",
																					"data": [
																						"VendorPOBox__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"label": "_POBox",
																						"activable": true,
																						"width": "180",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"browsable": false,
																						"length": 20,
																						"readonly": true,
																						"version": 0
																					},
																					"stamp": 170
																				},
																				"LabelVendorStreet__": {
																					"type": "Label",
																					"data": [
																						"VendorStreet__"
																					],
																					"options": {
																						"label": "_Street",
																						"version": 0
																					},
																					"stamp": 171
																				},
																				"VendorStreet__": {
																					"type": "ShortText",
																					"data": [
																						"VendorStreet__"
																					],
																					"options": {
																						"label": "_Street",
																						"activable": true,
																						"width": "180",
																						"readonly": true,
																						"version": 0,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default"
																					},
																					"stamp": 172
																				},
																				"LabelVendorCity__": {
																					"type": "Label",
																					"data": [
																						"VendorCity__"
																					],
																					"options": {
																						"label": "_City",
																						"version": 0
																					},
																					"stamp": 173
																				},
																				"VendorCity__": {
																					"type": "ShortText",
																					"data": [
																						"VendorCity__"
																					],
																					"options": {
																						"label": "_City",
																						"activable": true,
																						"width": "180",
																						"readonly": true,
																						"length": 50,
																						"version": 0,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"browsable": false
																					},
																					"stamp": 174
																				},
																				"LabelVendorZipCode__": {
																					"type": "Label",
																					"data": [
																						"VendorZipCode__"
																					],
																					"options": {
																						"label": "_Zip code",
																						"version": 0
																					},
																					"stamp": 175
																				},
																				"VendorZipCode__": {
																					"type": "ShortText",
																					"data": [
																						"VendorZipCode__"
																					],
																					"options": {
																						"label": "_Zip code",
																						"activable": true,
																						"width": "180",
																						"readonly": true,
																						"length": 50,
																						"version": 0,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default"
																					},
																					"stamp": 176
																				},
																				"LabelVendorRegion__": {
																					"type": "Label",
																					"data": [
																						"VendorRegion__"
																					],
																					"options": {
																						"label": "_Region",
																						"version": 0
																					},
																					"stamp": 177
																				},
																				"VendorRegion__": {
																					"type": "ShortText",
																					"data": [
																						"VendorRegion__"
																					],
																					"options": {
																						"label": "_Region",
																						"activable": true,
																						"width": "180",
																						"readonly": true,
																						"length": 50,
																						"version": 0,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default"
																					},
																					"stamp": 178
																				},
																				"LabelVendorCountry__": {
																					"type": "Label",
																					"data": [
																						"VendorCountry__"
																					],
																					"options": {
																						"label": "_Country",
																						"version": 0
																					},
																					"stamp": 179
																				},
																				"VendorCountry__": {
																					"type": "ShortText",
																					"data": [
																						"VendorCountry__"
																					],
																					"options": {
																						"label": "_Country",
																						"activable": true,
																						"width": "180",
																						"length": 50,
																						"readonly": true,
																						"version": 0,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default"
																					},
																					"stamp": 180
																				},
																				"LabelVendorContactEmail__": {
																					"type": "Label",
																					"data": [
																						"VendorContactEmail__"
																					],
																					"options": {
																						"label": "_VendorContactEmail",
																						"version": 0
																					},
																					"stamp": 181
																				},
																				"VendorContactEmail__": {
																					"type": "ShortText",
																					"data": [
																						"VendorContactEmail__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"label": "_VendorContactEmail",
																						"activable": true,
																						"width": 230,
																						"readonly": true,
																						"version": 0,
																						"browsable": false,
																						"autocompletable": false
																					},
																					"stamp": 182
																				},
																				"LabelWithholdingTax__": {
																					"type": "Label",
																					"data": [
																						"WithholdingTax__"
																					],
																					"options": {
																						"label": "_WithholdingTax",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 183
																				},
																				"WithholdingTax__": {
																					"type": "ShortText",
																					"data": [
																						"WithholdingTax__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"label": "_WithholdingTax",
																						"activable": true,
																						"width": "180",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"length": 2,
																						"browsable": false,
																						"autocompletable": false,
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 184
																				},
																				"SelectedBankAccountID__": {
																					"type": "ShortText",
																					"data": [
																						"SelectedBankAccountID__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"label": "_SelectedBankAccountID",
																						"activable": true,
																						"width": 230,
																						"readonly": true,
																						"browsable": false,
																						"autocompletable": false,
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 185
																				},
																				"LabelSelectedBankAccountID__": {
																					"type": "Label",
																					"data": [
																						"SelectedBankAccountID__"
																					],
																					"options": {
																						"label": "_SelectedBankAccountID",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 186
																				},
																				"LabelExtractedIBAN__": {
																					"type": "Label",
																					"data": [
																						"ExtractedIBAN__"
																					],
																					"options": {
																						"label": "_ExtractedIBAN",
																						"version": 0
																					},
																					"stamp": 187
																				},
																				"ExtractedIBAN__": {
																					"type": "ShortText",
																					"data": [
																						"ExtractedIBAN__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"label": "_ExtractedIBAN",
																						"activable": true,
																						"readonly": true,
																						"width": "240",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"browsable": false
																					},
																					"stamp": 188
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-21": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 189,
													"*": {
														"VendorInformationButtons": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": true,
																"hideTitleBar": true,
																"hideTitle": true,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "_Vendor Information Actions",
																"leftImageURL": "",
																"removeMargins": true,
																"elementsAlignment": "center",
																"version": 0,
																"sameHeightAsSiblings": false
															},
															"stamp": 190,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"ContactVendor__": "LabelContactVendor__",
																			"LabelContactVendor__": "ContactVendor__",
																			"ShowBankDetails__": "LabelShowBankDetails__",
																			"LabelShowBankDetails__": "ShowBankDetails__",
																			"ToggleHistoryView__": "LabelToggleHistoryView__",
																			"LabelToggleHistoryView__": "ToggleHistoryView__",
																			"ToggleVendorRelatedContractsView__": "LabelToggleVendorRelatedContractsView__",
																			"LabelToggleVendorRelatedContractsView__": "ToggleVendorRelatedContractsView__",
																			"SupplierInformationManagementLink__": "LabelSupplierInformationManagementLink__",
																			"LabelSupplierInformationManagementLink__": "SupplierInformationManagementLink__"
																		},
																		"version": 0
																	},
																	"stamp": 191,
																	"*": {
																		"Grid": {
																			"type": "HorizontalLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"ContactVendor__": {
																						"line": 1,
																						"column": 2
																					},
																					"LabelContactVendor__": {
																						"line": 1,
																						"column": 1
																					},
																					"ShowBankDetails__": {
																						"line": 2,
																						"column": 2
																					},
																					"LabelShowBankDetails__": {
																						"line": 2,
																						"column": 1
																					},
																					"ToggleHistoryView__": {
																						"line": 3,
																						"column": 2
																					},
																					"LabelToggleHistoryView__": {
																						"line": 3,
																						"column": 1
																					},
																					"ToggleVendorRelatedContractsView__": {
																						"line": 4,
																						"column": 2
																					},
																					"LabelToggleVendorRelatedContractsView__": {
																						"line": 4,
																						"column": 1
																					},
																					"SupplierInformationManagementLink__": {
																						"line": 5,
																						"column": 2
																					},
																					"LabelSupplierInformationManagementLink__": {
																						"line": 5,
																						"column": 1
																					}
																				},
																				"lines": 5,
																				"colspans": [
																					[],
																					[],
																					[],
																					[],
																					[]
																				],
																				"version": 0
																			},
																			"stamp": 192,
																			"*": {
																				"LabelContactVendor__": {
																					"type": "Label",
																					"data": false,
																					"options": {
																						"label": "",
																						"version": 0
																					},
																					"stamp": 193
																				},
																				"ContactVendor__": {
																					"type": "FormButton",
																					"data": false,
																					"options": {
																						"buttonLabel": "_Contact Vendor",
																						"label": "",
																						"version": 0,
																						"textPosition": "text-right",
																						"textSize": "MS",
																						"textColor": "default",
																						"nextprocess": {
																							"processName": "AP - Application Settings Wizard",
																							"attachmentsMode": "all",
																							"willBeChild": true
																						},
																						"urlImage": "AP_Contact_Vendor_boutton.png",
																						"style": 4,
																						"width": "",
																						"action": "none",
																						"url": ""
																					},
																					"stamp": 194
																				},
																				"LabelShowBankDetails__": {
																					"type": "Label",
																					"data": false,
																					"options": {
																						"label": "",
																						"version": 0
																					},
																					"stamp": 195
																				},
																				"ShowBankDetails__": {
																					"type": "FormButton",
																					"data": false,
																					"options": {
																						"buttonLabel": "_ShowBankDetails",
																						"label": "",
																						"textPosition": "text-right",
																						"textSize": "MS",
																						"textStyle": "default",
																						"textColor": "default",
																						"nextprocess": {
																							"processName": "AP - Application Settings Wizard",
																							"attachmentsMode": "all",
																							"willBeChild": true,
																							"returnToOriginalUrl": false
																						},
																						"urlImage": "AP_Bank_information_Button.png",
																						"style": 4,
																						"action": "none",
																						"url": "",
																						"version": 0
																					},
																					"stamp": 196
																				},
																				"LabelToggleHistoryView__": {
																					"type": "Label",
																					"data": false,
																					"options": {
																						"label": "",
																						"version": 0
																					},
																					"stamp": 197
																				},
																				"ToggleHistoryView__": {
																					"type": "FormButton",
																					"data": false,
																					"options": {
																						"buttonLabel": "_ShowHistoryView",
																						"label": "",
																						"textPosition": "text-right",
																						"textSize": "MS",
																						"textStyle": "default",
																						"textColor": "default",
																						"nextprocess": {
																							"processName": "AP - Application Settings Wizard",
																							"attachmentsMode": "all",
																							"willBeChild": true,
																							"returnToOriginalUrl": false
																						},
																						"action": "none",
																						"url": "",
																						"urlImage": "AP_history_boutton.png",
																						"style": 4,
																						"version": 0
																					},
																					"stamp": 198
																				},
																				"LabelToggleVendorRelatedContractsView__": {
																					"type": "Label",
																					"data": false,
																					"options": {
																						"label": "",
																						"version": 0
																					},
																					"stamp": 199
																				},
																				"ToggleVendorRelatedContractsView__": {
																					"type": "FormButton",
																					"data": false,
																					"options": {
																						"buttonLabel": "_ShowVendorRelatedContractsView",
																						"label": "",
																						"textPosition": "text-right",
																						"textSize": "MS",
																						"textStyle": "default",
																						"textColor": "default",
																						"nextprocess": {
																							"processName": "AP - Application Settings Wizard",
																							"attachmentsMode": "all",
																							"willBeChild": true,
																							"returnToOriginalUrl": false
																						},
																						"urlImage": "AP_Contract_button.png",
																						"style": 4,
																						"action": "none",
																						"url": "",
																						"version": 0
																					},
																					"stamp": 200
																				},
																				"LabelSupplierInformationManagementLink__": {
																					"type": "Label",
																					"data": false,
																					"options": {
																						"label": "",
																						"version": 0
																					},
																					"stamp": 201
																				},
																				"SupplierInformationManagementLink__": {
																					"type": "FormButton",
																					"data": false,
																					"options": {
																						"buttonLabel": "_SupplierInformationManagementLink",
																						"label": "",
																						"textPosition": "text-right",
																						"textSize": "MS",
																						"textStyle": "default",
																						"textColor": "default",
																						"nextprocess": {
																							"processName": "AP - Application Settings Wizard",
																							"attachmentsMode": "all",
																							"willBeChild": true,
																							"returnToOriginalUrl": false
																						},
																						"urlImage": "AP_supplier_info.png",
																						"urlImageOverlay": "",
																						"style": 4,
																						"action": "none",
																						"url": "",
																						"version": 0
																					},
																					"stamp": 202
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-20": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 203,
													"*": {
														"BankDetailsPane": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": 0,
																"iconURL": "AP_Bank_information.png",
																"TitleFontSize": "M",
																"hideActionButtons": true,
																"hideTitleBar": false,
																"hideTitle": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "_BankDetailsPane",
																"leftImageURL": "",
																"removeMargins": false,
																"hidden": false,
																"version": 0,
																"sameHeightAsSiblings": false
															},
															"stamp": 204,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"ComputingBankDetailsWaiting__": "LabelComputingBankDetailsWaiting__",
																			"LabelComputingBankDetailsWaiting__": "ComputingBankDetailsWaiting__"
																		},
																		"version": 0
																	},
																	"stamp": 205,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"BankDetails__": {
																						"line": 1,
																						"column": 1
																					},
																					"ComputingBankDetailsWaiting__": {
																						"line": 2,
																						"column": 2
																					},
																					"LabelComputingBankDetailsWaiting__": {
																						"line": 2,
																						"column": 1
																					},
																					"Spacer_line__": {
																						"line": 3,
																						"column": 1
																					},
																					"BankDetailsNoItem__": {
																						"line": 4,
																						"column": 1
																					},
																					"ExtractedIBAN__": {
																						"line": 5,
																						"column": 2
																					},
																					"LabelExtractedIBAN__": {
																						"line": 5,
																						"column": 1
																					}
																				},
																				"lines": 5,
																				"columns": 2,
																				"colspans": [
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[]
																				],
																				"version": 0
																			},
																			"stamp": 206,
																			"*": {
																				"BankDetails__": {
																					"type": "Table",
																					"data": [
																						"BankDetails__"
																					],
																					"options": {
																						"version": 5,
																						"lines": 3,
																						"columns": 9,
																						"displayEmptyLines": false,
																						"alwaysOneLine": true,
																						"menuOptions": {
																							"disableInsertLine": true,
																							"disableAddLine": true,
																							"hideTopNavigation": true,
																							"hideBottomNavigation": true,
																							"hideTableRowMenu": true,
																							"hideTableRowDelete": true,
																							"hideTableRowAdd": true,
																							"addButtonShouldInsert": false,
																							"hideTableRowDuplicate": true,
																							"hideTableRowButtonDuplicate": true
																						},
																						"item": {
																							"display": null
																						},
																						"group": {
																							"name": "GDR\nPage"
																						},
																						"label": "_BankDetails",
																						"readonly": false,
																						"subsection": null
																					},
																					"stamp": 207,
																					"*": {
																						"Actions": {
																							"type": "$TableActions",
																							"subtype": "Actions",
																							"options": {
																								"version": 0
																							},
																							"stamp": 208,
																							"*": {
																								"Top": {
																									"type": "$TableActions",
																									"subtype": "Top",
																									"options": {
																										"version": 0
																									},
																									"stamp": 209
																								},
																								"Bottom": {
																									"type": "$TableActions",
																									"subtype": "Top",
																									"options": {
																										"version": 0
																									},
																									"stamp": 210
																								}
																							}
																						},
																						"Header": {
																							"type": "$TableRow",
																							"subtype": "Header",
																							"options": {
																								"version": 0
																							},
																							"stamp": 211,
																							"data": [
																								null
																							],
																							"*": {
																								"Cell1": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 212,
																									"data": [],
																									"*": {
																										"BankDetails_Select__": {
																											"type": "Label",
																											"data": [
																												"BankDetails_Select__"
																											],
																											"options": {
																												"label": "_BankDetails_Select",
																												"version": 0
																											},
																											"stamp": 213,
																											"position": [
																												"Check box"
																											]
																										}
																									}
																								},
																								"Cell2": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 214,
																									"data": [],
																									"*": {
																										"BankDetails_Country__": {
																											"type": "Label",
																											"data": [
																												"BankDetails_Country__"
																											],
																											"options": {
																												"label": "_BankDetails_Country",
																												"version": 0
																											},
																											"stamp": 215,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell3": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 216,
																									"data": [],
																									"*": {
																										"BankDetails_Key__": {
																											"type": "Label",
																											"data": [
																												"BankDetails_Key__"
																											],
																											"options": {
																												"label": "_BankDetails_Key",
																												"version": 0
																											},
																											"stamp": 217,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell4": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 218,
																									"data": [],
																									"*": {
																										"BankDetails_Name__": {
																											"type": "Label",
																											"data": [
																												"BankDetails_Name__"
																											],
																											"options": {
																												"label": "_BankDetails_Name",
																												"version": 0
																											},
																											"stamp": 219,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell5": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 220,
																									"data": [],
																									"*": {
																										"BankDetails_Account__": {
																											"type": "Label",
																											"data": [
																												"BankDetails_Account__"
																											],
																											"options": {
																												"label": "_BankDetails_Account",
																												"version": 0
																											},
																											"stamp": 221,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell6": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 222,
																									"data": [],
																									"*": {
																										"BankDetails_Holder__": {
																											"type": "Label",
																											"data": [
																												"BankDetails_Holder__"
																											],
																											"options": {
																												"label": "_BankDetails_Holder",
																												"version": 0
																											},
																											"stamp": 223,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell7": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 224,
																									"data": [],
																									"*": {
																										"BankDetails_IBAN__": {
																											"type": "Label",
																											"data": [
																												"BankDetails_IBAN__"
																											],
																											"options": {
																												"label": "_BankDetails_IBAN",
																												"version": 0
																											},
																											"stamp": 225,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell8": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 226,
																									"data": [],
																									"*": {
																										"BankDetails_Type__": {
																											"type": "Label",
																											"data": [
																												"BankDetails_Type__"
																											],
																											"options": {
																												"label": "_BankDetails_Type",
																												"version": 0
																											},
																											"stamp": 227,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell9": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 228,
																									"data": [],
																									"*": {
																										"BankDetails_ID__": {
																											"type": "Label",
																											"data": [
																												"BankDetails_ID__"
																											],
																											"options": {
																												"label": "_BankDetails_ID",
																												"version": 0
																											},
																											"stamp": 229,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								}
																							}
																						},
																						"Clipboard": {
																							"type": "$TableRow",
																							"subtype": "Clipboard",
																							"options": {
																								"version": 0
																							},
																							"stamp": 230,
																							"data": [
																								null
																							],
																							"*": {
																								"Cell1": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 231,
																									"data": [],
																									"*": {
																										"BankDetails_Select__": {
																											"type": "CheckBox",
																											"data": [
																												"BankDetails_Select__"
																											],
																											"options": {
																												"label": "_BankDetails_Select",
																												"activable": true,
																												"width": 50,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"version": 0
																											},
																											"stamp": 232,
																											"position": [
																												"Check box"
																											]
																										}
																									}
																								},
																								"Cell2": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 233,
																									"data": [],
																									"*": {
																										"BankDetails_Country__": {
																											"type": "ShortText",
																											"data": [
																												"BankDetails_Country__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"label": "_BankDetails_Country",
																												"activable": true,
																												"width": "50",
																												"version": 0,
																												"helpText": "",
																												"browsable": false,
																												"length": 50,
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"readonly": true
																											},
																											"stamp": 234,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell3": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 235,
																									"data": [],
																									"*": {
																										"BankDetails_Key__": {
																											"type": "ShortText",
																											"data": [
																												"BankDetails_Key__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"label": "_BankDetails_Key",
																												"activable": true,
																												"width": "100",
																												"helpText": "",
																												"browsable": false,
																												"length": 15,
																												"version": 0,
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"readonly": true
																											},
																											"stamp": 236,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell4": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 237,
																									"data": [],
																									"*": {
																										"BankDetails_Name__": {
																											"type": "ShortText",
																											"data": [
																												"BankDetails_Name__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"label": "_BankDetails_Name",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"browsable": false,
																												"version": 0,
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"readonly": true
																											},
																											"stamp": 238,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell5": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 239,
																									"data": [],
																									"*": {
																										"BankDetails_Account__": {
																											"type": "ShortText",
																											"data": [
																												"BankDetails_Account__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"label": "_BankDetails_Account",
																												"activable": true,
																												"width": 130,
																												"helpText": "",
																												"browsable": false,
																												"version": 0,
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"readonly": true
																											},
																											"stamp": 240,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell6": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 241,
																									"data": [],
																									"*": {
																										"BankDetails_Holder__": {
																											"type": "ShortText",
																											"data": [
																												"BankDetails_Holder__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"label": "_BankDetails_Holder",
																												"activable": true,
																												"width": "200",
																												"helpText": "",
																												"length": 80,
																												"browsable": false,
																												"version": 0,
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"readonly": true
																											},
																											"stamp": 242,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell7": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 243,
																									"data": [],
																									"*": {
																										"BankDetails_IBAN__": {
																											"type": "ShortText",
																											"data": [
																												"BankDetails_IBAN__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"label": "_BankDetails_IBAN",
																												"activable": true,
																												"width": "240",
																												"helpText": "",
																												"browsable": false,
																												"version": 0,
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"readonly": true
																											},
																											"stamp": 244,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell8": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 245,
																									"data": [],
																									"*": {
																										"BankDetails_Type__": {
																											"type": "ShortText",
																											"data": [
																												"BankDetails_Type__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"label": "_BankDetails_Type",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"length": 4,
																												"browsable": false,
																												"version": 0,
																												"readonly": true
																											},
																											"stamp": 246,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell9": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 247,
																									"data": [],
																									"*": {
																										"BankDetails_ID__": {
																											"type": "ShortText",
																											"data": [
																												"BankDetails_ID__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"label": "_BankDetails_ID",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"length": 50,
																												"browsable": false,
																												"version": 0,
																												"readonly": true
																											},
																											"stamp": 248,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								}
																							}
																						}
																					}
																				},
																				"LabelComputingBankDetailsWaiting__": {
																					"type": "Label",
																					"data": false,
																					"options": {
																						"label": "_ComputingBankDetailsWaiting",
																						"version": 0
																					},
																					"stamp": 249
																				},
																				"ComputingBankDetailsWaiting__": {
																					"type": "HTML",
																					"data": false,
																					"options": {
																						"label": "_ComputingBankDetailsWaiting",
																						"htmlContent": "<div class=\"WaitingIcon-Small\"></div>",
																						"version": 0
																					},
																					"stamp": 250
																				},
																				"BankDetailsNoItem__": {
																					"type": "Text",
																					"data": false,
																					"options": {
																						"marge": 0,
																						"textSize": "S",
																						"textAlignment": "center",
																						"textStyle": "italic",
																						"textColor": "default",
																						"backgroundcolor": "default",
																						"label": "_BankDetailsNoItem",
																						"version": 0,
																						"iconClass": ""
																					},
																					"stamp": 251
																				},
																				"Spacer_line__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "",
																						"width": "",
																						"color": "default",
																						"label": "BankDetailsTableSpacer",
																						"version": 0
																					},
																					"stamp": 252
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-12": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 253,
													"*": {
														"HistoryViewPanel": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": 0,
																"iconURL": "AP_history.png",
																"TitleFontSize": "M",
																"hideActionButtons": true,
																"hideTitleBar": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "_HistoryViewPanelDefault",
																"leftImageURL": "",
																"version": 0,
																"hidden": false,
																"hideTitle": false,
																"sameHeightAsSiblings": false
															},
															"stamp": 254,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {},
																		"version": 0
																	},
																	"stamp": 255,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"HistoryView__": {
																						"line": 1,
																						"column": 1
																					},
																					"HistoryViewNoItem__": {
																						"line": 3,
																						"column": 1
																					},
																					"Spacer_line2__": {
																						"line": 2,
																						"column": 1
																					}
																				},
																				"lines": 3,
																				"columns": 2,
																				"colspans": [
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					]
																				],
																				"version": 0
																			},
																			"stamp": 256,
																			"*": {
																				"HistoryView__": {
																					"type": "AdminList",
																					"data": false,
																					"options": {
																						"width": "100%",
																						"restrictToCurrentJobId": false,
																						"showActions": false,
																						"openInNewTab": true,
																						"label": "_HistoryView",
																						"version": 0,
																						"visible": false,
																						"openInReadOnlyMode": true
																					},
																					"stamp": 257
																				},
																				"Spacer_line2__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "",
																						"width": "",
																						"color": "default",
																						"label": "Spacer line2",
																						"version": 0
																					},
																					"stamp": 258
																				},
																				"HistoryViewNoItem__": {
																					"type": "Text",
																					"data": false,
																					"options": {
																						"marge": 0,
																						"textSize": "S",
																						"textAlignment": "center",
																						"textStyle": "italic",
																						"textColor": "default",
																						"backgroundcolor": "default",
																						"label": "_HistoryViewNoItem",
																						"visible": false,
																						"version": 0,
																						"iconClass": ""
																					},
																					"stamp": 259
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-25": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 260,
													"*": {
														"VendorRelatedContractsViewPanel": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": 0,
																"iconURL": "AP_Contract.png",
																"TitleFontSize": "M",
																"hideActionButtons": true,
																"hideTitleBar": false,
																"hideTitle": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "_VendorRelatedContractsPanelDefault",
																"leftImageURL": "",
																"removeMargins": false,
																"elementsAlignment": "left",
																"version": 0,
																"sameHeightAsSiblings": false
															},
															"stamp": 261,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {},
																		"version": 0
																	},
																	"stamp": 262,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"VendorRelatedContractsView__": {
																						"line": 1,
																						"column": 1
																					},
																					"VendorRelatedContractsNoItem__": {
																						"line": 3,
																						"column": 1
																					},
																					"Spacer_line6__": {
																						"line": 2,
																						"column": 1
																					}
																				},
																				"lines": 3,
																				"columns": 2,
																				"colspans": [
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					]
																				],
																				"version": 0
																			},
																			"stamp": 263,
																			"*": {
																				"VendorRelatedContractsView__": {
																					"type": "AdminList",
																					"data": false,
																					"options": {
																						"width": "100%",
																						"restrictToCurrentJobId": false,
																						"showActions": false,
																						"openInNewTab": true,
																						"openInReadOnlyMode": true,
																						"label": "_VendorRelatedContractsView",
																						"visible": false,
																						"version": 0
																					},
																					"stamp": 264
																				},
																				"Spacer_line6__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "",
																						"width": "",
																						"color": "default",
																						"label": "Spacer line6",
																						"version": 0
																					},
																					"stamp": 265
																				},
																				"VendorRelatedContractsNoItem__": {
																					"type": "Text",
																					"data": false,
																					"options": {
																						"marge": 0,
																						"textSize": "S",
																						"textAlignment": "center",
																						"textStyle": "italic",
																						"textColor": "default",
																						"backgroundcolor": "default",
																						"label": "_VendorRelatedContractsNoItem",
																						"version": 0,
																						"iconClass": ""
																					},
																					"stamp": 266
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-14": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 267,
													"*": {
														"Payment": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": "120",
																"iconURL": "",
																"TitleFontSize": "M",
																"hideActionButtons": true,
																"hideTitleBar": false,
																"label": "_Payment",
																"version": 0,
																"hidden": true,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"hideTitle": false,
																"sameHeightAsSiblings": false
															},
															"stamp": 268,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"BaselineDate__": "LabelBaselineDate__",
																			"LabelBaselineDate__": "BaselineDate__",
																			"SAPPaymentMethod__": "LabelSAPPaymentMethod__",
																			"LabelSAPPaymentMethod__": "SAPPaymentMethod__",
																			"AlternativePayee__": "LabelAlternativePayee__",
																			"LabelAlternativePayee__": "AlternativePayee__",
																			"PaymentTerms__": "LabelPaymentTerms__",
																			"LabelPaymentTerms__": "PaymentTerms__",
																			"InvoiceReferenceNumber__": "LabelInvoiceReferenceNumber__",
																			"LabelInvoiceReferenceNumber__": "InvoiceReferenceNumber__",
																			"LabelDueDate__": "DueDate__",
																			"DueDate__": "LabelDueDate__",
																			"LabelDiscountLimitDate__": "DiscountLimitDate__",
																			"DiscountLimitDate__": "LabelDiscountLimitDate__",
																			"EstimatedDiscountAmount__": "LabelEstimatedDiscountAmount__",
																			"LabelEstimatedDiscountAmount__": "EstimatedDiscountAmount__",
																			"LocalEstimatedDiscountAmount__": "LabelLocalEstimatedDiscountAmount__",
																			"LabelLocalEstimatedDiscountAmount__": "LocalEstimatedDiscountAmount__",
																			"EstimatedLatePaymentFee__": "LabelEstimatedLatePaymentFee__",
																			"LabelEstimatedLatePaymentFee__": "EstimatedLatePaymentFee__",
																			"LocalEstimatedLatePaymentFee__": "LabelLocalEstimatedLatePaymentFee__",
																			"LabelLocalEstimatedLatePaymentFee__": "LocalEstimatedLatePaymentFee__"
																		},
																		"version": 0
																	},
																	"stamp": 269,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"LabelPaymentTerms__": {
																						"line": 1,
																						"column": 1
																					},
																					"PaymentTerms__": {
																						"line": 1,
																						"column": 2
																					},
																					"LabelDiscountLimitDate__": {
																						"line": 2,
																						"column": 1
																					},
																					"DiscountLimitDate__": {
																						"line": 2,
																						"column": 2
																					},
																					"LabelEstimatedDiscountAmount__": {
																						"line": 3,
																						"column": 1
																					},
																					"EstimatedDiscountAmount__": {
																						"line": 3,
																						"column": 2
																					},
																					"LabelLocalEstimatedDiscountAmount__": {
																						"line": 4,
																						"column": 1
																					},
																					"LocalEstimatedDiscountAmount__": {
																						"line": 4,
																						"column": 2
																					},
																					"LabelDueDate__": {
																						"line": 5,
																						"column": 1
																					},
																					"DueDate__": {
																						"line": 5,
																						"column": 2
																					},
																					"LabelEstimatedLatePaymentFee__": {
																						"line": 6,
																						"column": 1
																					},
																					"EstimatedLatePaymentFee__": {
																						"line": 6,
																						"column": 2
																					},
																					"LabelLocalEstimatedLatePaymentFee__": {
																						"line": 7,
																						"column": 1
																					},
																					"LocalEstimatedLatePaymentFee__": {
																						"line": 7,
																						"column": 2
																					},
																					"LabelBaselineDate__": {
																						"line": 8,
																						"column": 1
																					},
																					"BaselineDate__": {
																						"line": 8,
																						"column": 2
																					},
																					"LabelSAPPaymentMethod__": {
																						"line": 9,
																						"column": 1
																					},
																					"SAPPaymentMethod__": {
																						"line": 9,
																						"column": 2
																					},
																					"LabelAlternativePayee__": {
																						"line": 10,
																						"column": 1
																					},
																					"AlternativePayee__": {
																						"line": 10,
																						"column": 2
																					},
																					"LabelInvoiceReferenceNumber__": {
																						"line": 11,
																						"column": 1
																					},
																					"InvoiceReferenceNumber__": {
																						"line": 11,
																						"column": 2
																					}
																				},
																				"lines": 11,
																				"columns": 2,
																				"colspans": [
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[]
																				],
																				"version": 0
																			},
																			"stamp": 270,
																			"*": {
																				"LabelPaymentTerms__": {
																					"type": "Label",
																					"data": [
																						"PaymentTerms__"
																					],
																					"options": {
																						"label": "_Payment terms",
																						"version": 0
																					},
																					"stamp": 271
																				},
																				"PaymentTerms__": {
																					"type": "DatabaseComboBox",
																					"data": [
																						"PaymentTerms__"
																					],
																					"options": {
																						"version": 1,
																						"label": "_Payment terms",
																						"activable": true,
																						"width": "180",
																						"PreFillFTS": true,
																						"fTSmaxRecords": "20",
																						"browsable": true,
																						"autoCompleteSearchMode": "startswith",
																						"searchMode": "matches"
																					},
																					"stamp": 272
																				},
																				"LabelDueDate__": {
																					"type": "Label",
																					"data": [
																						"DueDate__"
																					],
																					"options": {
																						"label": "_Due date",
																						"version": 0
																					},
																					"stamp": 273
																				},
																				"DueDate__": {
																					"type": "DateTime",
																					"data": [
																						"DueDate__"
																					],
																					"options": {
																						"displayLongFormat": false,
																						"label": "_Due date",
																						"activable": true,
																						"width": "180",
																						"readonly": false,
																						"autocompletable": false,
																						"version": 0
																					},
																					"stamp": 274
																				},
																				"LabelDiscountLimitDate__": {
																					"type": "Label",
																					"data": [
																						"DiscountLimitDate__"
																					],
																					"options": {
																						"label": "_DiscountLimitDate",
																						"version": 0
																					},
																					"stamp": 275
																				},
																				"DiscountLimitDate__": {
																					"type": "DateTime",
																					"data": [
																						"DiscountLimitDate__"
																					],
																					"options": {
																						"displayLongFormat": false,
																						"label": "_DiscountLimitDate",
																						"activable": true,
																						"width": "180",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"autocompletable": false,
																						"version": 0
																					},
																					"stamp": 276
																				},
																				"LabelEstimatedDiscountAmount__": {
																					"type": "Label",
																					"data": [
																						"EstimatedDiscountAmount__"
																					],
																					"options": {
																						"label": "_EstimatedDiscountAmount",
																						"version": 0
																					},
																					"stamp": 277
																				},
																				"EstimatedDiscountAmount__": {
																					"type": "Decimal",
																					"data": [
																						"EstimatedDiscountAmount__"
																					],
																					"options": {
																						"version": 2,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"integer": false,
																						"label": "_EstimatedDiscountAmount",
																						"precision_internal": 2,
																						"precision_current": 2,
																						"activable": true,
																						"width": "180",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"autocompletable": false,
																						"readonly": true
																					},
																					"stamp": 278
																				},
																				"LabelLocalEstimatedDiscountAmount__": {
																					"type": "Label",
																					"data": [
																						"LocalEstimatedDiscountAmount__"
																					],
																					"options": {
																						"label": "_Local Estimated Discount Amount",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 279
																				},
																				"LocalEstimatedDiscountAmount__": {
																					"type": "Decimal",
																					"data": [
																						"LocalEstimatedDiscountAmount__"
																					],
																					"options": {
																						"version": 2,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"integer": false,
																						"label": "_Local Estimated Discount Amount",
																						"precision_internal": 2,
																						"precision_current": 2,
																						"activable": true,
																						"width": "180",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"readonly": true,
																						"autocompletable": false,
																						"hidden": true
																					},
																					"stamp": 280
																				},
																				"LabelBaselineDate__": {
																					"type": "Label",
																					"data": [],
																					"options": {
																						"label": "_Baseline date",
																						"version": 0
																					},
																					"stamp": 281
																				},
																				"BaselineDate__": {
																					"type": "DateTime",
																					"data": [
																						"BaselineDate__"
																					],
																					"options": {
																						"displayLongFormat": false,
																						"label": "_Baseline date",
																						"activable": true,
																						"width": "180",
																						"version": 0
																					},
																					"stamp": 282
																				},
																				"LabelSAPPaymentMethod__": {
																					"type": "Label",
																					"data": [
																						"SAPPaymentMethod__"
																					],
																					"options": {
																						"label": "_SAP Payment method",
																						"version": 0
																					},
																					"stamp": 283
																				},
																				"SAPPaymentMethod__": {
																					"type": "DatabaseComboBox",
																					"data": [
																						"SAPPaymentMethod__"
																					],
																					"options": {
																						"version": 1,
																						"label": "_SAP Payment method",
																						"activable": true,
																						"width": "180",
																						"fTSmaxRecords": "20",
																						"browsable": true,
																						"autoCompleteSearchMode": "startswith",
																						"searchMode": "matches"
																					},
																					"stamp": 284
																				},
																				"LabelAlternativePayee__": {
																					"type": "Label",
																					"data": [
																						"AlternativePayee__"
																					],
																					"options": {
																						"label": "_Alternative payee",
																						"version": 0
																					},
																					"stamp": 285
																				},
																				"AlternativePayee__": {
																					"type": "DatabaseComboBox",
																					"data": [
																						"AlternativePayee__"
																					],
																					"options": {
																						"version": 1,
																						"label": "_Alternative payee",
																						"activable": true,
																						"width": "180",
																						"fTSmaxRecords": "20",
																						"browsable": true,
																						"PreFillFTS": true,
																						"autocompletable": false,
																						"autoCompleteSearchMode": "startswith",
																						"searchMode": "matches"
																					},
																					"stamp": 286
																				},
																				"LabelInvoiceReferenceNumber__": {
																					"type": "Label",
																					"data": [
																						"InvoiceReferenceNumber__"
																					],
																					"options": {
																						"label": "_Invoice reference",
																						"version": 0
																					},
																					"stamp": 287
																				},
																				"InvoiceReferenceNumber__": {
																					"type": "DatabaseComboBox",
																					"data": [
																						"InvoiceReferenceNumber__"
																					],
																					"options": {
																						"version": 1,
																						"label": "_Invoice reference",
																						"activable": true,
																						"width": "180",
																						"fTSmaxRecords": "20",
																						"browsable": true,
																						"autocompletable": false,
																						"autoCompleteSearchMode": "startswith",
																						"searchMode": "matches"
																					},
																					"stamp": 288
																				},
																				"LabelEstimatedLatePaymentFee__": {
																					"type": "Label",
																					"data": [
																						"EstimatedLatePaymentFee__"
																					],
																					"options": {
																						"label": "_EstimatedLatePaymentFee",
																						"version": 0
																					},
																					"stamp": 289
																				},
																				"EstimatedLatePaymentFee__": {
																					"type": "Decimal",
																					"data": [
																						"EstimatedLatePaymentFee__"
																					],
																					"options": {
																						"version": 1,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"integer": false,
																						"label": "_EstimatedLatePaymentFee",
																						"precision_internal": 2,
																						"precision_current": 2,
																						"activable": true,
																						"width": "180",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"readonly": true
																					},
																					"stamp": 290
																				},
																				"LabelLocalEstimatedLatePaymentFee__": {
																					"type": "Label",
																					"data": [
																						"LocalEstimatedLatePaymentFee__"
																					],
																					"options": {
																						"label": "_LocalEstimatedLatePaymentFee",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 291
																				},
																				"LocalEstimatedLatePaymentFee__": {
																					"type": "Decimal",
																					"data": [
																						"LocalEstimatedLatePaymentFee__"
																					],
																					"options": {
																						"version": 1,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"integer": false,
																						"label": "_LocalEstimatedLatePaymentFee",
																						"precision_internal": 2,
																						"precision_current": 2,
																						"activable": true,
																						"width": "180",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"readonly": true,
																						"hidden": true
																					},
																					"stamp": 292
																				}
																			}
																		}
																	}
																}
															}
														},
														"Details": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": 120,
																"iconURL": "",
																"TitleFontSize": "M",
																"hideActionButtons": true,
																"hideTitleBar": false,
																"label": "_Details",
																"hidden": true,
																"version": 0,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"hideTitle": false,
																"sameHeightAsSiblings": false
															},
															"stamp": 293,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"Assignment__": "LabelAssignment__",
																			"LabelAssignment__": "Assignment__",
																			"HeaderText__": "LabelHeaderText__",
																			"LabelHeaderText__": "HeaderText__",
																			"UnplannedDeliveryCosts__": "LabelUnplannedDeliveryCosts__",
																			"LabelUnplannedDeliveryCosts__": "UnplannedDeliveryCosts__",
																			"BusinessArea__": "LabelBusinessArea__",
																			"LabelBusinessArea__": "BusinessArea__"
																		},
																		"version": 0
																	},
																	"stamp": 294,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"Assignment__": {
																						"line": 1,
																						"column": 2
																					},
																					"LabelAssignment__": {
																						"line": 1,
																						"column": 1
																					},
																					"HeaderText__": {
																						"line": 2,
																						"column": 2
																					},
																					"LabelHeaderText__": {
																						"line": 2,
																						"column": 1
																					},
																					"UnplannedDeliveryCosts__": {
																						"line": 3,
																						"column": 2
																					},
																					"LabelUnplannedDeliveryCosts__": {
																						"line": 3,
																						"column": 1
																					},
																					"BusinessArea__": {
																						"line": 4,
																						"column": 2
																					},
																					"LabelBusinessArea__": {
																						"line": 4,
																						"column": 1
																					}
																				},
																				"lines": 4,
																				"columns": 2,
																				"colspans": [
																					[],
																					[],
																					[],
																					[]
																				],
																				"version": 0
																			},
																			"stamp": 295,
																			"*": {
																				"LabelAssignment__": {
																					"type": "Label",
																					"data": [
																						"Assignment__"
																					],
																					"options": {
																						"label": "_Assignment",
																						"version": 0
																					},
																					"stamp": 296
																				},
																				"Assignment__": {
																					"type": "ShortText",
																					"data": [
																						"Assignment__"
																					],
																					"options": {
																						"label": "_Assignment",
																						"activable": true,
																						"width": "180",
																						"browsable": false,
																						"version": 0,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default"
																					},
																					"stamp": 297
																				},
																				"LabelHeaderText__": {
																					"type": "Label",
																					"data": [
																						"HeaderText__"
																					],
																					"options": {
																						"label": "_Header text",
																						"version": 0
																					},
																					"stamp": 298
																				},
																				"HeaderText__": {
																					"type": "ShortText",
																					"data": [
																						"HeaderText__"
																					],
																					"options": {
																						"label": "_Header text",
																						"activable": true,
																						"width": "180",
																						"browsable": false,
																						"version": 0,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default"
																					},
																					"stamp": 299
																				},
																				"LabelUnplannedDeliveryCosts__": {
																					"type": "Label",
																					"data": [
																						"UnplannedDeliveryCosts__"
																					],
																					"options": {
																						"label": "_Unplanned delivery costs",
																						"version": 0
																					},
																					"stamp": 300
																				},
																				"UnplannedDeliveryCosts__": {
																					"type": "Decimal",
																					"data": [
																						"UnplannedDeliveryCosts__"
																					],
																					"options": {
																						"version": 2,
																						"integer": false,
																						"label": "_Unplanned delivery costs",
																						"precision_internal": 2,
																						"activable": true,
																						"width": "180",
																						"textSize": "S",
																						"textAlignment": "right",
																						"textStyle": "default",
																						"textColor": "default",
																						"precision_current": 2
																					},
																					"stamp": 301
																				},
																				"LabelBusinessArea__": {
																					"type": "Label",
																					"data": [
																						"BusinessArea__"
																					],
																					"options": {
																						"label": "_Business area",
																						"version": 0
																					},
																					"stamp": 302
																				},
																				"BusinessArea__": {
																					"type": "DatabaseComboBox",
																					"data": [
																						"BusinessArea__"
																					],
																					"options": {
																						"version": 1,
																						"label": "_Business area",
																						"activable": true,
																						"width": "180",
																						"fTSmaxRecords": "20",
																						"length": 4,
																						"browsable": true,
																						"PreFillFTS": true,
																						"autoCompleteSearchMode": "startswith",
																						"searchMode": "matches"
																					},
																					"stamp": 303
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-17": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 304,
													"*": {
														"PaymentDetails": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": 0,
																"iconURL": "AP_PaymentDetailsPanel.png",
																"TitleFontSize": "M",
																"hideActionButtons": true,
																"hideTitleBar": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "_Payment Details",
																"leftImageURL": "",
																"version": 0,
																"hidden": false,
																"hideTitle": false,
																"sameHeightAsSiblings": false
															},
															"stamp": 305,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"PaymentDate__": "LabelPaymentDate__",
																			"LabelPaymentDate__": "PaymentDate__",
																			"PaymentMethod__": "LabelPaymentMethod__",
																			"LabelPaymentMethod__": "PaymentMethod__",
																			"PaymentReference__": "LabelPaymentReference__",
																			"LabelPaymentReference__": "PaymentReference__"
																		},
																		"version": 0
																	},
																	"stamp": 306,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"PaymentDate__": {
																						"line": 1,
																						"column": 2
																					},
																					"LabelPaymentDate__": {
																						"line": 1,
																						"column": 1
																					},
																					"PaymentMethod__": {
																						"line": 2,
																						"column": 2
																					},
																					"LabelPaymentMethod__": {
																						"line": 2,
																						"column": 1
																					},
																					"PaymentReference__": {
																						"line": 3,
																						"column": 2
																					},
																					"LabelPaymentReference__": {
																						"line": 3,
																						"column": 1
																					}
																				},
																				"lines": 3,
																				"columns": 2,
																				"colspans": [
																					[],
																					[],
																					[]
																				],
																				"version": 0
																			},
																			"stamp": 307,
																			"*": {
																				"LabelPaymentDate__": {
																					"type": "Label",
																					"data": [
																						"PaymentDate__"
																					],
																					"options": {
																						"label": "_Payment Date",
																						"version": 0
																					},
																					"stamp": 308
																				},
																				"PaymentDate__": {
																					"type": "DateTime",
																					"data": [
																						"PaymentDate__"
																					],
																					"options": {
																						"displayLongFormat": false,
																						"label": "_Payment Date",
																						"activable": true,
																						"width": "180",
																						"readonly": true,
																						"autocompletable": false,
																						"version": 0
																					},
																					"stamp": 309
																				},
																				"LabelPaymentMethod__": {
																					"type": "Label",
																					"data": [
																						"PaymentMethod__"
																					],
																					"options": {
																						"label": "_Payment Method",
																						"version": 0
																					},
																					"stamp": 310
																				},
																				"PaymentMethod__": {
																					"type": "ComboBox",
																					"data": [
																						"PaymentMethod__"
																					],
																					"options": {
																						"possibleValues": {
																							"0": "",
																							"1": "Cash",
																							"2": "Check",
																							"3": "Credit card",
																							"4": "EFT",
																							"5": "Other"
																						},
																						"keyValueMode": true,
																						"possibleKeys": {
																							"0": "0",
																							"1": "Cash",
																							"2": "Check",
																							"3": "Credit card",
																							"4": "EFT",
																							"5": "Other"
																						},
																						"label": "_Payment Method",
																						"activable": true,
																						"width": "180",
																						"readonly": true,
																						"version": 1,
																						"maxNbLinesToDisplay": 10
																					},
																					"stamp": 311
																				},
																				"LabelPaymentReference__": {
																					"type": "Label",
																					"data": [
																						"PaymentReference__"
																					],
																					"options": {
																						"label": "_Payment Reference",
																						"version": 0
																					},
																					"stamp": 312
																				},
																				"PaymentReference__": {
																					"type": "ShortText",
																					"data": [
																						"PaymentReference__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"label": "_Payment Reference",
																						"activable": true,
																						"width": "180",
																						"readonly": true,
																						"browsable": false,
																						"autocompletable": false,
																						"version": 0
																					},
																					"stamp": 313
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-22": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 314,
													"*": {
														"ExtendedWithholdingTaxPane": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": 0,
																"iconURL": "AP_WitholdingTax.png",
																"TitleFontSize": "M",
																"hideActionButtons": true,
																"hideTitleBar": false,
																"hideTitle": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "_Extended Withholding Tax Pane",
																"leftImageURL": "",
																"removeMargins": false,
																"version": 0,
																"sameHeightAsSiblings": false
															},
															"stamp": 315,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {},
																		"version": 0
																	},
																	"stamp": 316,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"ExtendedWithholdingTax__": {
																						"line": 1,
																						"column": 1
																					},
																					"Spacer_line3__": {
																						"line": 2,
																						"column": 1
																					}
																				},
																				"lines": 2,
																				"columns": 2,
																				"colspans": [
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					]
																				],
																				"version": 0
																			},
																			"stamp": 317,
																			"*": {
																				"ExtendedWithholdingTax__": {
																					"type": "Table",
																					"data": [
																						"ExtendedWithholdingTax__"
																					],
																					"options": {
																						"version": 5,
																						"lines": 10,
																						"columns": 6,
																						"displayEmptyLines": false,
																						"alwaysOneLine": true,
																						"menuOptions": {
																							"disableInsertLine": true,
																							"disableAddLine": true,
																							"hideTopNavigation": true,
																							"hideBottomNavigation": true,
																							"hideTableRowMenu": true,
																							"hideTableRowDelete": false,
																							"hideTableRowAdd": false,
																							"addButtonShouldInsert": false,
																							"hideTableRowDuplicate": true,
																							"hideTableRowButtonDuplicate": true
																						},
																						"item": {
																							"display": null
																						},
																						"group": {
																							"name": "GDR\nPage"
																						},
																						"label": "_ExtendedWithholdingTaxTable",
																						"subsection": null
																					},
																					"stamp": 318,
																					"*": {
																						"Actions": {
																							"type": "$TableActions",
																							"subtype": "Actions",
																							"options": {
																								"version": 0
																							},
																							"stamp": 319,
																							"*": {
																								"Top": {
																									"type": "$TableActions",
																									"subtype": "Top",
																									"options": {
																										"version": 0
																									},
																									"stamp": 320
																								},
																								"Bottom": {
																									"type": "$TableActions",
																									"subtype": "Top",
																									"options": {
																										"version": 0
																									},
																									"stamp": 321
																								}
																							}
																						},
																						"Header": {
																							"type": "$TableRow",
																							"subtype": "Header",
																							"options": {
																								"version": 0
																							},
																							"stamp": 322,
																							"data": [
																								null
																							],
																							"*": {
																								"Cell1": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 323,
																									"data": [],
																									"*": {
																										"WHTType__": {
																											"type": "Label",
																											"data": [
																												"WHTType__"
																											],
																											"options": {
																												"label": "_WHT Type",
																												"version": 0
																											},
																											"stamp": 324,
																											"position": [
																												"Select from a table"
																											]
																										}
																									}
																								},
																								"Cell2": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 325,
																									"data": [],
																									"*": {
																										"WHTCode__": {
																											"type": "Label",
																											"data": [
																												"WHTCode__"
																											],
																											"options": {
																												"label": "_WHT Code",
																												"version": 0
																											},
																											"stamp": 326,
																											"position": [
																												"Select from a table"
																											]
																										}
																									}
																								},
																								"Cell3": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 327,
																									"data": [],
																									"*": {
																										"WHTDescription__": {
																											"type": "Label",
																											"data": [
																												"WHTDescription__"
																											],
																											"options": {
																												"label": "_WHT Description",
																												"width": 240,
																												"version": 0
																											},
																											"stamp": 328,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell4": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 329,
																									"data": [],
																									"*": {
																										"WHTBaseAmount__": {
																											"type": "Label",
																											"data": [
																												"WHTBaseAmount__"
																											],
																											"options": {
																												"label": "_WHT Base Amount",
																												"width": 80,
																												"version": 0
																											},
																											"stamp": 330,
																											"position": [
																												"Decimal number"
																											]
																										}
																									}
																								},
																								"Cell5": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 331,
																									"data": [],
																									"*": {
																										"WHTTaxAmount__": {
																											"type": "Label",
																											"data": [
																												"WHTTaxAmount__"
																											],
																											"options": {
																												"label": "_WHT Tax Amount",
																												"width": 80,
																												"version": 0
																											},
																											"stamp": 332,
																											"position": [
																												"Decimal number"
																											]
																										}
																									}
																								},
																								"Cell6": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 333,
																									"data": [],
																									"*": {
																										"WHTTaxAmountAuto__": {
																											"type": "Label",
																											"data": [
																												"WHTTaxAmountAuto__"
																											],
																											"options": {
																												"label": "_WHT automatic tax amount",
																												"version": 0
																											},
																											"stamp": 334,
																											"position": [
																												"Checkbox"
																											]
																										}
																									}
																								}
																							}
																						},
																						"Clipboard": {
																							"type": "$TableRow",
																							"subtype": "Clipboard",
																							"options": {
																								"version": 0
																							},
																							"stamp": 335,
																							"data": [
																								null
																							],
																							"*": {
																								"Cell1": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 336,
																									"data": [],
																									"*": {
																										"WHTType__": {
																											"type": "DatabaseComboBox",
																											"data": [
																												"WHTType__"
																											],
																											"options": {
																												"version": 1,
																												"label": "_WHT Type",
																												"activable": true,
																												"width": "65",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"fTSmaxRecords": "20",
																												"PreFillFTS": true,
																												"browsable": true,
																												"autoCompleteSearchMode": "startswith",
																												"searchMode": "matches"
																											},
																											"stamp": 337,
																											"position": [
																												"Select from a table"
																											]
																										}
																									}
																								},
																								"Cell2": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 338,
																									"data": [],
																									"*": {
																										"WHTCode__": {
																											"type": "DatabaseComboBox",
																											"data": [
																												"WHTCode__"
																											],
																											"options": {
																												"version": 1,
																												"label": "_WHT Code",
																												"activable": true,
																												"width": "65",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"fTSmaxRecords": "20",
																												"PreFillFTS": true,
																												"browsable": true,
																												"autoCompleteSearchMode": "startswith",
																												"searchMode": "matches"
																											},
																											"stamp": 339,
																											"position": [
																												"Select from a table"
																											]
																										}
																									}
																								},
																								"Cell3": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 340,
																									"data": [],
																									"*": {
																										"WHTDescription__": {
																											"type": "ShortText",
																											"data": [
																												"WHTDescription__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"label": "_WHT Description",
																												"activable": true,
																												"width": 260,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"readonly": true,
																												"browsable": false,
																												"version": 0
																											},
																											"stamp": 341,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell4": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 342,
																									"data": [],
																									"*": {
																										"WHTBaseAmount__": {
																											"type": "Decimal",
																											"data": [
																												"WHTBaseAmount__"
																											],
																											"options": {
																												"version": 2,
																												"textSize": "S",
																												"textAlignment": "right",
																												"textStyle": "default",
																												"textColor": "default",
																												"integer": false,
																												"label": "_WHT Base Amount",
																												"precision_internal": 2,
																												"precision_current": 2,
																												"activable": true,
																												"width": 80,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format"
																											},
																											"stamp": 343,
																											"position": [
																												"Decimal number"
																											]
																										}
																									}
																								},
																								"Cell5": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 344,
																									"data": [],
																									"*": {
																										"WHTTaxAmount__": {
																											"type": "Decimal",
																											"data": [
																												"WHTTaxAmount__"
																											],
																											"options": {
																												"version": 2,
																												"textSize": "S",
																												"textAlignment": "right",
																												"textStyle": "default",
																												"textColor": "default",
																												"integer": false,
																												"label": "_WHT Tax Amount",
																												"precision_internal": 2,
																												"precision_current": 2,
																												"activable": true,
																												"width": 80,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format"
																											},
																											"stamp": 345,
																											"position": [
																												"Decimal number"
																											]
																										}
																									}
																								},
																								"Cell6": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 346,
																									"data": [],
																									"*": {
																										"WHTTaxAmountAuto__": {
																											"type": "CheckBox",
																											"data": [],
																											"options": {
																												"label": "_WHT automatic tax amount",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"defaultValue": true,
																												"readonly": true,
																												"dataType": "Boolean",
																												"version": 0
																											},
																											"stamp": 347,
																											"position": [
																												"Checkbox"
																											]
																										}
																									}
																								}
																							}
																						}
																					}
																				},
																				"Spacer_line3__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "",
																						"width": "",
																						"color": "default",
																						"label": "Spacer line3",
																						"version": 0
																					},
																					"stamp": 348
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-15": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 349,
													"*": {
														"LineItemsRecognition": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": 0,
																"label": "_Line Items Recognition",
																"version": 0,
																"hidden": true,
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": false,
																"hideTitleBar": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"hideTitle": false,
																"sameHeightAsSiblings": false
															},
															"stamp": 350,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"HeaderDNExtracted__": "LabelHeaderDNExtracted__",
																			"LabelHeaderDNExtracted__": "HeaderDNExtracted__"
																		},
																		"version": 0
																	},
																	"stamp": 351,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"HeaderDNExtracted__": {
																						"line": 1,
																						"column": 2
																					},
																					"LabelHeaderDNExtracted__": {
																						"line": 1,
																						"column": 1
																					},
																					"Spacer_line4__": {
																						"line": 2,
																						"column": 1
																					},
																					"ExtractedLineItems__": {
																						"line": 3,
																						"column": 1
																					}
																				},
																				"lines": 3,
																				"columns": 2,
																				"colspans": [
																					[],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					]
																				],
																				"version": 0
																			},
																			"stamp": 352,
																			"*": {
																				"LabelHeaderDNExtracted__": {
																					"type": "Label",
																					"data": [
																						"HeaderDNExtracted__"
																					],
																					"options": {
																						"label": "_Header DeliveryNote Extracted",
																						"version": 0
																					},
																					"stamp": 353
																				},
																				"HeaderDNExtracted__": {
																					"type": "ShortText",
																					"data": [
																						"HeaderDNExtracted__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"label": "_Header DeliveryNote Extracted",
																						"activable": true,
																						"width": "180",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"readonly": true,
																						"browsable": false,
																						"autocompletable": false
																					},
																					"stamp": 354
																				},
																				"Spacer_line4__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "",
																						"width": "",
																						"color": "default",
																						"label": "Spacer line4",
																						"version": 0
																					},
																					"stamp": 355
																				},
																				"ExtractedLineItems__": {
																					"type": "Table",
																					"data": [
																						"ExtractedLineItems__"
																					],
																					"options": {
																						"version": 5,
																						"lines": 5,
																						"columns": 6,
																						"displayEmptyLines": false,
																						"alwaysOneLine": false,
																						"menuOptions": {
																							"disableInsertLine": false,
																							"disableAddLine": false,
																							"hideTopNavigation": false,
																							"hideBottomNavigation": false,
																							"hideTableRowMenu": false,
																							"hideTableRowDelete": true,
																							"hideTableRowAdd": true,
																							"addButtonShouldInsert": false,
																							"hideTableRowDuplicate": true,
																							"hideTableRowButtonDuplicate": true
																						},
																						"item": {
																							"display": null
																						},
																						"group": {
																							"name": "GDR\nPage"
																						},
																						"label": "Table with auto-learning"
																					},
																					"stamp": 356,
																					"*": {
																						"Actions": {
																							"type": "$TableActions",
																							"subtype": "Actions",
																							"options": {
																								"version": 0
																							},
																							"stamp": 357,
																							"*": {
																								"Top": {
																									"type": "$TableActions",
																									"subtype": "Top",
																									"options": {
																										"version": 0
																									},
																									"stamp": 358,
																									"*": {
																										"Autocomplete": {
																											"type": "ToolButton",
																											"data": false,
																											"options": {
																												"action": "Autocomplete",
																												"help": "_AutoComplete",
																												"version": 0
																											},
																											"stamp": 359
																										}
																									},
																									"data": []
																								},
																								"Bottom": {
																									"type": "$TableActions",
																									"subtype": "Top",
																									"options": {
																										"version": 0
																									},
																									"stamp": 360,
																									"*": {
																										"Autocomplete": {
																											"type": "ToolButton",
																											"data": false,
																											"options": {
																												"action": "Autocomplete",
																												"help": "_AutoComplete",
																												"version": 0
																											},
																											"stamp": 361
																										}
																									},
																									"data": []
																								}
																							},
																							"data": []
																						},
																						"Header": {
																							"type": "$TableRow",
																							"subtype": "Header",
																							"options": {
																								"version": 0
																							},
																							"stamp": 362,
																							"data": [
																								null
																							],
																							"*": {
																								"Cell1": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 363,
																									"data": [],
																									"*": {
																										"OrderNumberExtracted__": {
																											"type": "Label",
																											"data": [
																												"OrderNumberExtracted__"
																											],
																											"options": {
																												"label": "_Order number",
																												"version": 0
																											},
																											"stamp": 364
																										}
																									}
																								},
																								"Cell2": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 365,
																									"data": [],
																									"*": {
																										"DeliveryNoteExtracted__": {
																											"type": "Label",
																											"data": [
																												"DeliveryNoteExtracted__"
																											],
																											"options": {
																												"label": "_Delivery note",
																												"version": 0
																											},
																											"stamp": 366
																										}
																									},
																									"position": [
																										{}
																									]
																								},
																								"Cell3": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 367,
																									"data": [],
																									"*": {
																										"PartNumberExtracted__": {
																											"type": "Label",
																											"data": [
																												"PartNumberExtracted__"
																											],
																											"options": {
																												"label": "_Part number",
																												"version": 0
																											},
																											"stamp": 368
																										}
																									}
																								},
																								"Cell4": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 369,
																									"data": [],
																									"*": {
																										"UnitPriceExtracted__": {
																											"type": "Label",
																											"data": [
																												"UnitPriceExtracted__"
																											],
																											"options": {
																												"label": "_Unit price",
																												"version": 0
																											},
																											"stamp": 370
																										}
																									}
																								},
																								"Cell5": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 371,
																									"data": [],
																									"*": {
																										"QuantityExtracted__": {
																											"type": "Label",
																											"data": [
																												"QuantityExtracted__"
																											],
																											"options": {
																												"label": "_Quantity",
																												"version": 0
																											},
																											"stamp": 372
																										}
																									}
																								},
																								"Cell6": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 373,
																									"data": [],
																									"*": {
																										"AmountExtracted__": {
																											"type": "Label",
																											"data": [
																												"AmountExtracted__"
																											],
																											"options": {
																												"label": "_Amount",
																												"version": 0
																											},
																											"stamp": 374
																										}
																									}
																								}
																							}
																						},
																						"Clipboard": {
																							"type": "$TableRow",
																							"subtype": "Clipboard",
																							"options": {
																								"version": 0
																							},
																							"stamp": 375,
																							"data": [
																								null
																							],
																							"*": {
																								"Cell1": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 376,
																									"data": [],
																									"*": {
																										"OrderNumberExtracted__": {
																											"type": "ShortText",
																											"data": [
																												"OrderNumberExtracted__"
																											],
																											"options": {
																												"label": "_Order number",
																												"activable": true,
																												"width": "100",
																												"autocompletable": true,
																												"readonly": true,
																												"version": 0,
																												"browsable": false,
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default"
																											},
																											"stamp": 377
																										}
																									}
																								},
																								"Cell2": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 378,
																									"data": [],
																									"*": {
																										"DeliveryNoteExtracted__": {
																											"type": "ShortText",
																											"data": [
																												"DeliveryNoteExtracted__"
																											],
																											"options": {
																												"label": "_Delivery note",
																												"activable": true,
																												"width": "100",
																												"browsable": false,
																												"readonly": true,
																												"version": 0,
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default"
																											},
																											"stamp": 379
																										}
																									},
																									"position": [
																										{}
																									]
																								},
																								"Cell3": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 380,
																									"data": [],
																									"*": {
																										"PartNumberExtracted__": {
																											"type": "ShortText",
																											"data": [
																												"PartNumberExtracted__"
																											],
																											"options": {
																												"label": "_Part number",
																												"activable": true,
																												"width": "100",
																												"autocompletable": true,
																												"readonly": true,
																												"version": 0,
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default"
																											},
																											"stamp": 381
																										}
																									}
																								},
																								"Cell4": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 382,
																									"data": [],
																									"*": {
																										"UnitPriceExtracted__": {
																											"type": "Decimal",
																											"data": [
																												"UnitPriceExtracted__"
																											],
																											"options": {
																												"integer": false,
																												"precision_internal": 4,
																												"label": "_Unit price",
																												"activable": true,
																												"width": "100",
																												"readonly": true,
																												"autocompletable": true,
																												"version": 2,
																												"textSize": "S",
																												"textAlignment": "right",
																												"textStyle": "default",
																												"textColor": "default",
																												"precision_current": 4
																											},
																											"stamp": 383
																										}
																									}
																								},
																								"Cell5": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 384,
																									"data": [],
																									"*": {
																										"QuantityExtracted__": {
																											"type": "Decimal",
																											"data": [
																												"QuantityExtracted__"
																											],
																											"options": {
																												"integer": false,
																												"precision_internal": 2,
																												"label": "_Quantity",
																												"activable": true,
																												"width": 140,
																												"autocompletable": true,
																												"readonly": true,
																												"version": 2,
																												"textSize": "S",
																												"textAlignment": "right",
																												"textStyle": "default",
																												"textColor": "default",
																												"precision_current": 2
																											},
																											"stamp": 385
																										}
																									}
																								},
																								"Cell6": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 386,
																									"data": [],
																									"*": {
																										"AmountExtracted__": {
																											"type": "Decimal",
																											"data": [
																												"AmountExtracted__"
																											],
																											"options": {
																												"integer": false,
																												"precision_internal": 2,
																												"label": "_Amount",
																												"activable": true,
																												"width": 140,
																												"readonly": true,
																												"autocompletable": true,
																												"version": 2,
																												"textSize": "S",
																												"textAlignment": "right",
																												"textStyle": "default",
																												"textColor": "default",
																												"precision_current": 2
																											},
																											"stamp": 387
																										}
																									}
																								}
																							}
																						}
																					}
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-16": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 388,
													"*": {
														"Line_items": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": 0,
																"label": "_Line Items",
																"version": 0,
																"hideTitleBar": false,
																"iconURL": "AP_Line_items.png",
																"TitleFontSize": "M",
																"hideActionButtons": true,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"hideTitle": false,
																"sameHeightAsSiblings": false
															},
															"stamp": 389,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {},
																		"version": 0
																	},
																	"stamp": 390,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"ReconcileWarning__": {
																						"line": 1,
																						"column": 1
																					},
																					"LineItems__": {
																						"line": 2,
																						"column": 1
																					}
																				},
																				"lines": 2,
																				"columns": 2,
																				"colspans": [
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					]
																				],
																				"version": 0
																			},
																			"stamp": 391,
																			"*": {
																				"ReconcileWarning__": {
																					"type": "Text",
																					"data": false,
																					"options": {
																						"marge": 0,
																						"label": "_ReconcileWarning",
																						"version": 0,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"backgroundcolor": "default",
																						"iconClass": "",
																						"hidden": true
																					},
																					"stamp": 392
																				},
																				"LineItems__": {
																					"type": "Table",
																					"data": [
																						"LineItems__"
																					],
																					"options": {
																						"version": 4,
																						"lines": 20,
																						"columns": 51,
																						"displayEmptyLines": false,
																						"alwaysOneLine": true,
																						"menuOptions": {
																							"disableInsertLine": false,
																							"disableAddLine": false,
																							"hideTopNavigation": true,
																							"hideBottomNavigation": false,
																							"hideTableRowMenu": false,
																							"hideTableRowDelete": true,
																							"hideTableRowAdd": true,
																							"addButtonShouldInsert": false,
																							"hideTableRowDuplicate": false,
																							"hideTableRowButtonDuplicate": true
																						},
																						"item": {
																							"display": null
																						},
																						"group": {
																							"name": "GDR\nPage"
																						},
																						"label": "_Line items",
																						"subsection": null
																					},
																					"stamp": 393,
																					"*": {
																						"Actions": {
																							"type": "$TableActions",
																							"subtype": "Actions",
																							"options": {
																								"version": 0
																							},
																							"stamp": 394,
																							"*": {
																								"Top": {
																									"type": "$TableActions",
																									"subtype": "Top",
																									"options": {
																										"version": 0
																									},
																									"stamp": 395,
																									"data": []
																								},
																								"Bottom": {
																									"type": "$TableActions",
																									"subtype": "Top",
																									"options": {
																										"version": 0
																									},
																									"stamp": 396,
																									"data": []
																								}
																							},
																							"data": []
																						},
																						"Header": {
																							"type": "$TableRow",
																							"subtype": "Header",
																							"options": {
																								"version": 0
																							},
																							"stamp": 397,
																							"data": [
																								null
																							],
																							"*": {
																								"Cell1": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 398,
																									"data": [],
																									"*": {
																										"LineType__": {
																											"type": "Label",
																											"data": [
																												"LineType__"
																											],
																											"options": {
																												"label": "LineType",
																												"version": 0
																											},
																											"stamp": 399
																										}
																									},
																									"position": [
																										{}
																									]
																								},
																								"Cell2": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 400,
																									"data": [],
																									"*": {
																										"OrderNumber__": {
																											"type": "Label",
																											"data": [
																												"OrderNumber__"
																											],
																											"options": {
																												"label": "_Order number",
																												"version": 0
																											},
																											"stamp": 401
																										}
																									},
																									"position": [
																										{}
																									]
																								},
																								"Cell3": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 402,
																									"data": [],
																									"*": {
																										"GoodIssue__": {
																											"type": "Label",
																											"data": [
																												"GoodIssue__"
																											],
																											"options": {
																												"label": "_Line item good issue number",
																												"version": 0
																											},
																											"stamp": 403,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell4": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 404,
																									"data": [],
																									"*": {
																										"DeliveryNote__": {
																											"type": "Label",
																											"data": [
																												"DeliveryNote__"
																											],
																											"options": {
																												"label": "_Delivery note",
																												"version": 0
																											},
																											"stamp": 405
																										}
																									},
																									"position": [
																										{}
																									]
																								},
																								"Cell5": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 406,
																									"data": [],
																									"*": {
																										"GoodsReceipt__": {
																											"type": "Label",
																											"data": [
																												"GoodsReceipt__"
																											],
																											"options": {
																												"label": "_Goods receipt",
																												"version": 0
																											},
																											"stamp": 407
																										}
																									}
																								},
																								"Cell6": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 408,
																									"data": [],
																									"*": {
																										"ItemNumber__": {
																											"type": "Label",
																											"data": [
																												"ItemNumber__"
																											],
																											"options": {
																												"label": "_Item number",
																												"version": 0
																											},
																											"stamp": 409
																										}
																									},
																									"position": [
																										{}
																									]
																								},
																								"Cell7": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": 50,
																										"version": 0
																									},
																									"stamp": 410,
																									"data": [],
																									"*": {
																										"PriceCondition__": {
																											"type": "Label",
																											"data": [
																												"PriceCondition__"
																											],
																											"options": {
																												"label": "_PriceCondition",
																												"minwidth": 50,
																												"version": 0
																											},
																											"stamp": 411,
																											"position": [
																												"_ShortText"
																											]
																										}
																									}
																								},
																								"Cell8": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 412,
																									"data": [],
																									"*": {
																										"PartNumber__": {
																											"type": "Label",
																											"data": [
																												"PartNumber__"
																											],
																											"options": {
																												"label": "_Part number",
																												"version": 0,
																												"minwidth": 140
																											},
																											"stamp": 413
																										}
																									}
																								},
																								"Cell9": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": "0",
																										"version": 0
																									},
																									"stamp": 794,
																									"data": [],
																									"*": {
																										"Keyword__": {
																											"type": "Label",
																											"data": [
																												"Keyword__"
																											],
																											"options": {
																												"label": "_Keyword",
																												"minwidth": 140,
																												"version": 0
																											},
																											"stamp": 795,
																											"position": [
																												{}
																											]
																										}
																									}
																								},
																								"Cell10": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 414,
																									"data": [],
																									"*": {
																										"Description__": {
																											"type": "Label",
																											"data": [
																												"Description__"
																											],
																											"options": {
																												"label": "_Description",
																												"version": 0
																											},
																											"stamp": 415
																										}
																									},
																									"position": [
																										{}
																									]
																								},
																								"Cell11": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": "90",
																										"hidden": false,
																										"version": 0
																									},
																									"stamp": 780,
																									"data": [],
																									"*": {
																										"ItemType__": {
																											"type": "Label",
																											"data": [
																												"ItemType__"
																											],
																											"options": {
																												"label": "_ItemType",
																												"minwidth": "90",
																												"hidden": false,
																												"version": 0
																											},
																											"stamp": 781,
																											"position": [
																												"_ComboBox"
																											]
																										}
																									}
																								},
																								"Cell12": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 416,
																									"data": [],
																									"*": {
																										"Quantity__": {
																											"type": "Label",
																											"data": [
																												"Quantity__"
																											],
																											"options": {
																												"label": "_Quantity",
																												"version": 0
																											},
																											"stamp": 417
																										}
																									},
																									"position": [
																										{}
																									]
																								},
																								"Cell13": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 418,
																									"data": [],
																									"*": {
																										"Amount__": {
																											"type": "Label",
																											"data": [
																												"Amount__"
																											],
																											"options": {
																												"label": "_Amount",
																												"version": 0
																											},
																											"stamp": 419
																										}
																									}
																								},
																								"Cell14": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 420,
																									"data": [],
																									"*": {
																										"CompanyCode__": {
																											"type": "Label",
																											"data": [
																												"CompanyCode__"
																											],
																											"options": {
																												"label": "_LineCompanyCode",
																												"version": 0
																											},
																											"stamp": 421,
																											"position": [
																												"Select from a table"
																											]
																										}
																									}
																								},
																								"Cell15": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 422,
																									"data": [],
																									"*": {
																										"GLAccount__": {
																											"type": "Label",
																											"data": [
																												"GLAccount__"
																											],
																											"options": {
																												"label": "_G/L account",
																												"version": 0
																											},
																											"stamp": 423
																										}
																									},
																									"position": [
																										{}
																									]
																								},
																								"Cell16": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 424,
																									"data": [],
																									"*": {
																										"GLDescription__": {
																											"type": "Label",
																											"data": [
																												"GLDescription__"
																											],
																											"options": {
																												"label": "_G/L account description",
																												"version": 0
																											},
																											"stamp": 425
																										}
																									},
																									"position": [
																										{}
																									]
																								},
																								"Cell17": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 426,
																									"data": [],
																									"*": {
																										"Group__": {
																											"type": "Label",
																											"data": [
																												"Group__"
																											],
																											"options": {
																												"label": "_Group",
																												"version": 0
																											},
																											"stamp": 427,
																											"position": [
																												"Texte"
																											]
																										}
																									}
																								},
																								"Cell18": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": 140,
																										"hidden": false,
																										"version": 0
																									},
																									"stamp": 428,
																									"data": [],
																									"*": {
																										"CostType__": {
																											"type": "Label",
																											"data": [
																												"CostType__"
																											],
																											"options": {
																												"label": "_CostType",
																												"minwidth": 80,
																												"hidden": false,
																												"version": 0
																											},
																											"stamp": 429,
																											"position": [
																												"_ComboBox"
																											]
																										}
																									}
																								},
																								"Cell19": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 430,
																									"data": [],
																									"*": {
																										"BusinessArea__": {
																											"type": "Label",
																											"data": [
																												"BusinessArea__"
																											],
																											"options": {
																												"label": "_Business area",
																												"version": 0
																											},
																											"stamp": 431,
																											"position": [
																												"Select from a table"
																											]
																										}
																									}
																								},
																								"Cell20": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 432,
																									"data": [],
																									"*": {
																										"CostCenter__": {
																											"type": "Label",
																											"data": [
																												"CostCenter__"
																											],
																											"options": {
																												"label": "_Cost center",
																												"version": 0
																											},
																											"stamp": 433
																										}
																									},
																									"position": [
																										{}
																									]
																								},
																								"Cell21": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 434,
																									"data": [],
																									"*": {
																										"CCDescription__": {
																											"type": "Label",
																											"data": [
																												"CCDescription__"
																											],
																											"options": {
																												"label": "_Cost center description",
																												"version": 0
																											},
																											"stamp": 435
																										}
																									},
																									"position": [
																										{}
																									]
																								},
																								"Cell22": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": "100",
																										"version": 0
																									},
																									"stamp": 436,
																									"data": [],
																									"*": {
																										"ProjectCode__": {
																											"type": "Label",
																											"data": [
																												"ProjectCode__"
																											],
																											"options": {
																												"label": "_Project code",
																												"minwidth": "100",
																												"version": 0
																											},
																											"stamp": 437,
																											"position": [
																												"_DatabaseComboBox"
																											]
																										}
																									}
																								},
																								"Cell23": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": 140,
																										"version": 0
																									},
																									"stamp": 438,
																									"data": [],
																									"*": {
																										"ProjectCodeDescription__": {
																											"type": "Label",
																											"data": [
																												"ProjectCodeDescription__"
																											],
																											"options": {
																												"label": "_Project code description",
																												"minwidth": 140,
																												"version": 0
																											},
																											"stamp": 439,
																											"position": [
																												"_ShortText"
																											]
																										}
																									}
																								},
																								"Cell24": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": "100",
																										"hidden": false,
																										"version": 0
																									},
																									"stamp": 440,
																									"data": [],
																									"*": {
																										"PreviousBudgetID__": {
																											"type": "Label",
																											"data": [
																												"PreviousBudgetID__"
																											],
																											"options": {
																												"label": "_PreviousBudgetID",
																												"minwidth": "100",
																												"hidden": false,
																												"version": 0
																											},
																											"stamp": 441,
																											"position": [
																												"_ShortText"
																											]
																										}
																									}
																								},
																								"Cell25": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 442,
																									"data": [],
																									"*": {
																										"BudgetID__": {
																											"type": "Label",
																											"data": [
																												"BudgetID__"
																											],
																											"options": {
																												"label": "_BudgetID",
																												"version": 0
																											},
																											"stamp": 443,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell26": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 444,
																									"data": [],
																									"*": {
																										"Assignment__": {
																											"type": "Label",
																											"data": [
																												"Assignment__"
																											],
																											"options": {
																												"label": "_Assignment",
																												"version": 0
																											},
																											"stamp": 445,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell27": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 446,
																									"data": [],
																									"*": {
																										"TradingPartner__": {
																											"type": "Label",
																											"data": [
																												"TradingPartner__"
																											],
																											"options": {
																												"label": "_Trading partner",
																												"version": 0
																											},
																											"stamp": 447,
																											"position": [
																												"Select from a table"
																											]
																										}
																									}
																								},
																								"Cell28": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 448,
																									"data": [],
																									"*": {
																										"InternalOrder__": {
																											"type": "Label",
																											"data": [
																												"InternalOrder__"
																											],
																											"options": {
																												"label": "_InternalOrder",
																												"version": 0
																											},
																											"stamp": 449,
																											"position": [
																												"Select from a table"
																											]
																										}
																									}
																								},
																								"Cell29": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 450,
																									"data": [],
																									"*": {
																										"WBSElement__": {
																											"type": "Label",
																											"data": [
																												"WBSElement__"
																											],
																											"options": {
																												"label": "_WBSElement",
																												"version": 0
																											},
																											"stamp": 451,
																											"position": [
																												"Select from a table"
																											]
																										}
																									}
																								},
																								"Cell30": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 452,
																									"data": [],
																									"*": {
																										"WBSElementID__": {
																											"type": "Label",
																											"data": [
																												"WBSElementID__"
																											],
																											"options": {
																												"label": "_WBSElementID",
																												"version": 0
																											},
																											"stamp": 453,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell31": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": "0",
																										"hidden": false,
																										"version": 0
																									},
																									"stamp": 804,
																									"data": [],
																									"*": {
																										"FreeDimension1__": {
																											"type": "Label",
																											"data": [
																												"FreeDimension1__"
																											],
																											"options": {
																												"label": "_FreeDimension1",
																												"minwidth": 140,
																												"version": 0
																											},
																											"stamp": 805,
																											"position": [
																												"_DatabaseComboBox"
																											]
																										}
																									}
																								},
																								"Cell32": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": "0",
																										"hidden": false,
																										"version": 0
																									},
																									"stamp": 810,
																									"data": [],
																									"*": {
																										"FreeDimension1ID__": {
																											"type": "Label",
																											"data": [
																												"FreeDimension1ID__"
																											],
																											"options": {
																												"label": "_FreeDimension1ID",
																												"minwidth": 140,
																												"version": 0
																											},
																											"stamp": 811,
																											"position": [
																												"_DatabaseComboBox"
																											]
																										}
																									}
																								},
																								"Cell33": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 454,
																									"data": [],
																									"*": {
																										"TaxCode__": {
																											"type": "Label",
																											"data": [
																												"TaxCode__"
																											],
																											"options": {
																												"label": "_Tax code",
																												"version": 0
																											},
																											"stamp": 455
																										}
																									},
																									"position": [
																										{}
																									]
																								},
																								"Cell34": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 456,
																									"data": [],
																									"*": {
																										"TaxJurisdiction__": {
																											"type": "Label",
																											"data": [
																												"TaxJurisdiction__"
																											],
																											"options": {
																												"label": "_Jurisdiction code",
																												"version": 0
																											},
																											"stamp": 457
																										}
																									},
																									"position": [
																										{}
																									]
																								},
																								"Cell35": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 458,
																									"data": [],
																									"*": {
																										"TaxAmount__": {
																											"type": "Label",
																											"data": [
																												"TaxAmount__"
																											],
																											"options": {
																												"label": "_Tax amount",
																												"version": 0
																											},
																											"stamp": 459
																										}
																									},
																									"position": [
																										{}
																									]
																								},
																								"Cell36": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 460,
																									"data": [],
																									"*": {
																										"TaxRate__": {
																											"type": "Label",
																											"data": [
																												"TaxRate__"
																											],
																											"options": {
																												"label": "_Tax rate",
																												"version": 0
																											},
																											"stamp": 461
																										}
																									}
																								},
																								"Cell37": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 783,
																									"data": [],
																									"*": {
																										"MultiTaxRates__": {
																											"type": "Label",
																											"data": [
																												"MultiTaxRates__"
																											],
																											"options": {
																												"label": "_MultiTaxRates",
																												"minwidth": 140,
																												"version": 0
																											},
																											"stamp": 784,
																											"position": [
																												"_LongText"
																											]
																										}
																									}
																								},
																								"Cell38": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 462,
																									"data": [],
																									"*": {
																										"VendorNumber__": {
																											"type": "Label",
																											"data": [
																												"VendorNumber__"
																											],
																											"options": {
																												"label": "_Vendor number",
																												"version": 0
																											},
																											"stamp": 463
																										}
																									}
																								},
																								"Cell39": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 464,
																									"data": [],
																									"*": {
																										"ExpectedQuantity__": {
																											"type": "Label",
																											"data": [
																												"ExpectedQuantity__"
																											],
																											"options": {
																												"label": "_Expected quantity",
																												"version": 0
																											},
																											"stamp": 465
																										}
																									},
																									"position": [
																										{}
																									]
																								},
																								"Cell40": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 466,
																									"data": [],
																									"*": {
																										"ExpectedAmount__": {
																											"type": "Label",
																											"data": [
																												"ExpectedAmount__"
																											],
																											"options": {
																												"label": "_Expected amount",
																												"version": 0
																											},
																											"stamp": 467
																										}
																									},
																									"position": [
																										{}
																									]
																								},
																								"Cell41": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 468,
																									"data": [],
																									"*": {
																										"OpenQuantity__": {
																											"type": "Label",
																											"data": [
																												"OpenQuantity__"
																											],
																											"options": {
																												"label": "_Open quantity",
																												"version": 0
																											},
																											"stamp": 469
																										}
																									}
																								},
																								"Cell42": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 470,
																									"data": [],
																									"*": {
																										"OpenAmount__": {
																											"type": "Label",
																											"data": [
																												"OpenAmount__"
																											],
																											"options": {
																												"label": "_Open amount",
																												"version": 0
																											},
																											"stamp": 471
																										}
																									}
																								},
																								"Cell43": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 472,
																									"data": [],
																									"*": {
																										"UnitPrice__": {
																											"type": "Label",
																											"data": [
																												"UnitPrice__"
																											],
																											"options": {
																												"label": "_Unit price",
																												"version": 0
																											},
																											"stamp": 473
																										}
																									}
																								},
																								"Cell44": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 474,
																									"data": [],
																									"*": {
																										"Buyer__": {
																											"type": "Label",
																											"data": [
																												"Buyer__"
																											],
																											"options": {
																												"label": "_Buyer",
																												"version": 0
																											},
																											"stamp": 475,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell45": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 476,
																									"data": [],
																									"*": {
																										"Receiver__": {
																											"type": "Label",
																											"data": [
																												"Receiver__"
																											],
																											"options": {
																												"label": "_Receiver",
																												"version": 0
																											},
																											"stamp": 477,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell46": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 478,
																									"data": [],
																									"*": {
																										"IsLocalPO__": {
																											"type": "Label",
																											"data": [
																												"IsLocalPO__"
																											],
																											"options": {
																												"label": "_IsLocalPO_V2",
																												"version": 0
																											},
																											"stamp": 479,
																											"position": [
																												"Check box"
																											]
																										}
																									}
																								},
																								"Cell47": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 480,
																									"data": [],
																									"*": {
																										"DifferentInvoicingParty__": {
																											"type": "Label",
																											"data": [
																												"DifferentInvoicingParty__"
																											],
																											"options": {
																												"label": "_DifferentInvoicingParty",
																												"version": 0
																											},
																											"stamp": 481,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell48": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 482,
																									"data": [],
																									"*": {
																										"AcctAssCat__": {
																											"type": "Label",
																											"data": [
																												"AcctAssCat__"
																											],
																											"options": {
																												"label": "_Account assignment category",
																												"version": 0
																											},
																											"stamp": 483,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell49": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 484,
																									"data": [],
																									"*": {
																										"NoGoodsReceipt__": {
																											"type": "Label",
																											"data": [
																												"NoGoodsReceipt__"
																											],
																											"options": {
																												"label": "_PO_NoGoodsReceipt",
																												"version": 0
																											},
																											"stamp": 485,
																											"position": [
																												"Check box"
																											]
																										}
																									}
																								},
																								"Cell50": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 486,
																									"data": [],
																									"*": {
																										"UnitOfMeasureCode__": {
																											"type": "Label",
																											"data": [
																												"UnitOfMeasureCode__"
																											],
																											"options": {
																												"label": "_UnitOfMeasureCode",
																												"version": 0
																											},
																											"stamp": 487,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell51": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": 140,
																										"version": 0
																									},
																									"stamp": 806,
																									"data": [],
																									"*": {
																										"TechnicalDetails__": {
																											"type": "Label",
																											"data": [
																												"TechnicalDetails__"
																											],
																											"options": {
																												"label": "_TechnicalDetails",
																												"minwidth": 140,
																												"version": 0
																											},
																											"stamp": 807,
																											"position": [
																												"_LongText"
																											]
																										}
																									}
																								}
																							}
																						},
																						"Clipboard": {
																							"type": "$TableRow",
																							"subtype": "Clipboard",
																							"options": {
																								"version": 0
																							},
																							"stamp": 490,
																							"data": [
																								null
																							],
																							"*": {
																								"Cell1": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 491,
																									"data": [],
																									"*": {
																										"LineType__": {
																											"type": "ShortText",
																											"data": [
																												"LineType__"
																											],
																											"options": {
																												"label": "LineType",
																												"activable": true,
																												"width": 140,
																												"autocompletable": true,
																												"version": 0,
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default"
																											},
																											"stamp": 492
																										}
																									},
																									"position": [
																										{}
																									]
																								},
																								"Cell2": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 493,
																									"data": [],
																									"*": {
																										"OrderNumber__": {
																											"type": "ShortText",
																											"data": [
																												"OrderNumber__"
																											],
																											"options": {
																												"label": "_Order number",
																												"activable": true,
																												"width": "100",
																												"readonly": true,
																												"version": 0,
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default"
																											},
																											"stamp": 494
																										}
																									},
																									"position": [
																										{}
																									]
																								},
																								"Cell3": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 495,
																									"data": [],
																									"*": {
																										"GoodIssue__": {
																											"type": "ShortText",
																											"data": [
																												"GoodIssue__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"label": "_Line item good issue number",
																												"activable": true,
																												"width": "100",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"readonly": true,
																												"browsable": false
																											},
																											"stamp": 496,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell4": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 497,
																									"data": [],
																									"*": {
																										"DeliveryNote__": {
																											"type": "ShortText",
																											"data": [
																												"DeliveryNote__"
																											],
																											"options": {
																												"label": "_Delivery note",
																												"activable": true,
																												"width": 140,
																												"readonly": true,
																												"browsable": false,
																												"version": 0,
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default"
																											},
																											"stamp": 498
																										}
																									},
																									"position": [
																										{}
																									]
																								},
																								"Cell5": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 499,
																									"data": [],
																									"*": {
																										"GoodsReceipt__": {
																											"type": "ShortText",
																											"data": [
																												"GoodsReceipt__"
																											],
																											"options": {
																												"label": "_Goods receipt",
																												"activable": true,
																												"width": 140,
																												"readonly": true,
																												"browsable": false,
																												"version": 0,
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default"
																											},
																											"stamp": 500
																										}
																									}
																								},
																								"Cell6": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 501,
																									"data": [],
																									"*": {
																										"ItemNumber__": {
																											"type": "ShortText",
																											"data": [
																												"ItemNumber__"
																											],
																											"options": {
																												"label": "_Item number",
																												"activable": true,
																												"width": "80",
																												"readonly": true,
																												"version": 0,
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default"
																											},
																											"stamp": 502
																										}
																									},
																									"position": [
																										{}
																									]
																								},
																								"Cell7": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"hidden": false,
																										"version": 0
																									},
																									"stamp": 503,
																									"data": [],
																									"*": {
																										"PriceCondition__": {
																											"type": "ShortText",
																											"data": [
																												"PriceCondition__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"label": "_PriceCondition",
																												"activable": true,
																												"width": 50,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"readonly": true,
																												"browsable": false
																											},
																											"stamp": 504,
																											"position": [
																												"_ShortText"
																											]
																										}
																									}
																								},
																								"Cell8": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 505,
																									"data": [],
																									"*": {
																										"PartNumber__": {
																											"type": "ShortText",
																											"data": [
																												"PartNumber__"
																											],
																											"options": {
																												"label": "_Part number",
																												"activable": true,
																												"width": 140,
																												"autocompletable": true,
																												"readonly": true,
																												"version": 0,
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default"
																											},
																											"stamp": 506
																										}
																									}
																								},
																								"Cell9": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 793,
																									"data": [],
																									"*": {
																										"Keyword__": {
																											"type": "ShortText",
																											"data": [
																												"Keyword__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"label": "_Keyword",
																												"activable": true,
																												"width": 140,
																												"readonly": true
																											},
																											"stamp": 796,
																											"position": [
																												{}
																											]
																										}
																									}
																								},
																								"Cell10": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 507,
																									"data": [],
																									"*": {
																										"Description__": {
																											"type": "ShortText",
																											"data": [
																												"Description__"
																											],
																											"options": {
																												"label": "_Description",
																												"activable": true,
																												"width": 140,
																												"version": 0,
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default"
																											},
																											"stamp": 508
																										}
																									},
																									"position": [
																										{}
																									]
																								},
																								"Cell11": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"hidden": false,
																										"version": 0
																									},
																									"stamp": 779,
																									"data": [],
																									"*": {
																										"ItemType__": {
																											"type": "ComboBox",
																											"data": [
																												"ItemType__"
																											],
																											"options": {
																												"possibleValues": {
																													"0": "_QuantityBased",
																													"1": "_AmountBased"
																												},
																												"version": 1,
																												"keyValueMode": true,
																												"possibleKeys": {
																													"0": "QuantityBased",
																													"1": "AmountBased"
																												},
																												"label": "_ItemType",
																												"activable": true,
																												"width": "90",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"DataSourceOrigin": "Standalone",
																												"DisplayedColumns": "",
																												"hidden": false,
																												"readonly": true,
																												"maxNbLinesToDisplay": 10
																											},
																											"stamp": 782,
																											"position": [
																												"_ComboBox"
																											]
																										}
																									}
																								},
																								"Cell12": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 509,
																									"data": [],
																									"*": {
																										"Quantity__": {
																											"type": "Decimal",
																											"data": [
																												"Quantity__"
																											],
																											"options": {
																												"integer": false,
																												"precision_internal": 2,
																												"label": "_Quantity",
																												"activable": true,
																												"width": "80",
																												"version": 2,
																												"textSize": "S",
																												"textAlignment": "right",
																												"textStyle": "default",
																												"textColor": "default",
																												"precision_current": 2
																											},
																											"stamp": 510
																										}
																									},
																									"position": [
																										{}
																									]
																								},
																								"Cell13": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 511,
																									"data": [],
																									"*": {
																										"Amount__": {
																											"type": "Decimal",
																											"data": [
																												"Amount__"
																											],
																											"options": {
																												"integer": false,
																												"precision_internal": 2,
																												"label": "_Amount",
																												"activable": true,
																												"width": "80",
																												"version": 2,
																												"textSize": "S",
																												"textAlignment": "right",
																												"textStyle": "default",
																												"textColor": "default",
																												"precision_current": 2,
																												"triggerHalfWidth": true,
																												"currencySource": "InvoiceCurrency__"
																											},
																											"stamp": 512
																										}
																									}
																								},
																								"Cell14": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 513,
																									"data": [],
																									"*": {
																										"CompanyCode__": {
																											"type": "DatabaseComboBox",
																											"data": [
																												"CompanyCode__"
																											],
																											"options": {
																												"version": 1,
																												"label": "_LineCompanyCode",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"fTSmaxRecords": "20",
																												"browsable": true,
																												"PreFillFTS": true,
																												"autoCompleteSearchMode": "startswith",
																												"searchMode": "matches"
																											},
																											"stamp": 514,
																											"position": [
																												"Select from a table"
																											]
																										}
																									}
																								},
																								"Cell15": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 515,
																									"data": [],
																									"*": {
																										"GLAccount__": {
																											"type": "DatabaseComboBox",
																											"data": [
																												"GLAccount__"
																											],
																											"options": {
																												"label": "_G/L account",
																												"activable": true,
																												"width": "80",
																												"fTSmaxRecords": "20",
																												"version": 0,
																												"PreFillFTS": true,
																												"browsable": true,
																												"RestrictSearch": true,
																												"autoCompleteSearchMode": "startswith",
																												"searchMode": "contains",
																												"triggerHalfWidth": true
																											},
																											"stamp": 516
																										}
																									},
																									"position": [
																										{}
																									]
																								},
																								"Cell16": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 517,
																									"data": [],
																									"*": {
																										"GLDescription__": {
																											"type": "ShortText",
																											"data": [
																												"GLDescription__"
																											],
																											"options": {
																												"label": "_G/L account description",
																												"activable": true,
																												"width": 140,
																												"readonly": true,
																												"browsable": false,
																												"version": 0,
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default"
																											},
																											"stamp": 518
																										}
																									},
																									"position": [
																										{}
																									]
																								},
																								"Cell17": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 519,
																									"data": [],
																									"*": {
																										"Group__": {
																											"type": "ShortText",
																											"data": [
																												"Group__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"label": "_Group",
																												"activable": true,
																												"width": 140,
																												"readonly": true,
																												"browsable": false,
																												"autocompletable": false,
																												"length": 64,
																												"version": 0
																											},
																											"stamp": 520,
																											"position": [
																												"Texte"
																											]
																										}
																									}
																								},
																								"Cell18": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"hidden": false,
																										"version": 0
																									},
																									"stamp": 521,
																									"data": [],
																									"*": {
																										"CostType__": {
																											"type": "ComboBox",
																											"data": [
																												"CostType__"
																											],
																											"options": {
																												"possibleValues": {
																													"0": "",
																													"1": "_OpEx",
																													"2": "_CapEx"
																												},
																												"version": 1,
																												"keyValueMode": true,
																												"possibleKeys": {
																													"0": "",
																													"1": "OpEx",
																													"2": "CapEx"
																												},
																												"label": "_CostType",
																												"activable": true,
																												"width": 80,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"DataSourceOrigin": "Standalone",
																												"DisplayedColumns": "",
																												"hidden": false,
																												"readonly": false,
																												"maxNbLinesToDisplay": 10
																											},
																											"stamp": 522,
																											"position": [
																												"_ComboBox"
																											]
																										}
																									}
																								},
																								"Cell19": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 523,
																									"data": [],
																									"*": {
																										"BusinessArea__": {
																											"type": "DatabaseComboBox",
																											"data": [
																												"BusinessArea__"
																											],
																											"options": {
																												"version": 1,
																												"label": "_Business area",
																												"activable": true,
																												"width": "80",
																												"fTSmaxRecords": "20",
																												"browsable": true,
																												"PreFillFTS": true,
																												"autoCompleteSearchMode": "startswith",
																												"searchMode": "matches"
																											},
																											"stamp": 524,
																											"position": [
																												"Select from a table"
																											]
																										}
																									}
																								},
																								"Cell20": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 525,
																									"data": [],
																									"*": {
																										"CostCenter__": {
																											"type": "DatabaseComboBox",
																											"data": [
																												"CostCenter__"
																											],
																											"options": {
																												"label": "_Cost center",
																												"activable": true,
																												"width": "100",
																												"PreFillFTS": true,
																												"fTSmaxRecords": "20",
																												"version": 0,
																												"browsable": true,
																												"RestrictSearch": true,
																												"autoCompleteSearchMode": "startswith",
																												"searchMode": "contains",
																												"triggerHalfWidth": true
																											},
																											"stamp": 526
																										}
																									},
																									"position": [
																										{}
																									]
																								},
																								"Cell21": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 527,
																									"data": [],
																									"*": {
																										"CCDescription__": {
																											"type": "ShortText",
																											"data": [
																												"CCDescription__"
																											],
																											"options": {
																												"label": "_Cost center description",
																												"activable": true,
																												"width": 140,
																												"readonly": true,
																												"browsable": false,
																												"version": 0,
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default"
																											},
																											"stamp": 528
																										}
																									},
																									"position": [
																										{}
																									]
																								},
																								"Cell22": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 529,
																									"data": [],
																									"*": {
																										"ProjectCode__": {
																											"type": "DatabaseComboBox",
																											"data": [
																												"ProjectCode__"
																											],
																											"options": {
																												"version": 1,
																												"autoCompleteSearchMode": "startswith",
																												"searchMode": "contains",
																												"label": "_Project code",
																												"activable": true,
																												"width": "100",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"minNbLines": 1,
																												"maxNbLines": 1,
																												"PreFillFTS": true,
																												"RestrictSearch": true,
																												"fTSmaxRecords": "20",
																												"browsable": true
																											},
																											"stamp": 530,
																											"position": [
																												"_DatabaseComboBox"
																											]
																										}
																									}
																								},
																								"Cell23": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 531,
																									"data": [],
																									"*": {
																										"ProjectCodeDescription__": {
																											"type": "ShortText",
																											"data": [
																												"ProjectCodeDescription__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"label": "_Project code description",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"readonly": true,
																												"browsable": false
																											},
																											"stamp": 532,
																											"position": [
																												"_ShortText"
																											]
																										}
																									}
																								},
																								"Cell24": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"hidden": false,
																										"version": 0
																									},
																									"stamp": 533,
																									"data": [],
																									"*": {
																										"PreviousBudgetID__": {
																											"type": "ShortText",
																											"data": [
																												"PreviousBudgetID__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"label": "_PreviousBudgetID",
																												"activable": true,
																												"width": "100",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"hidden": false,
																												"readonly": true,
																												"browsable": false,
																												"autocompletable": false
																											},
																											"stamp": 534,
																											"position": [
																												"_ShortText"
																											]
																										}
																									}
																								},
																								"Cell25": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 535,
																									"data": [],
																									"*": {
																										"BudgetID__": {
																											"type": "ShortText",
																											"data": [
																												"BudgetID__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"label": "_BudgetID",
																												"activable": true,
																												"width": "100",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"readonly": true,
																												"browsable": false,
																												"version": 0
																											},
																											"stamp": 536,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell26": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 537,
																									"data": [],
																									"*": {
																										"Assignment__": {
																											"type": "ShortText",
																											"data": [
																												"Assignment__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"label": "_Assignment",
																												"activable": true,
																												"width": "180",
																												"browsable": false,
																												"version": 0
																											},
																											"stamp": 538,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell27": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 539,
																									"data": [],
																									"*": {
																										"TradingPartner__": {
																											"type": "DatabaseComboBox",
																											"data": [
																												"TradingPartner__"
																											],
																											"options": {
																												"version": 1,
																												"label": "_Trading partner",
																												"activable": true,
																												"width": "80",
																												"helpText": "",
																												"fTSmaxRecords": "20",
																												"browsable": true,
																												"autocompletable": false,
																												"autoCompleteSearchMode": "startswith",
																												"searchMode": "matches"
																											},
																											"stamp": 540,
																											"position": [
																												"Select from a table"
																											]
																										}
																									}
																								},
																								"Cell28": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 541,
																									"data": [],
																									"*": {
																										"InternalOrder__": {
																											"type": "DatabaseComboBox",
																											"data": [
																												"InternalOrder__"
																											],
																											"options": {
																												"version": 1,
																												"label": "_InternalOrder",
																												"activable": true,
																												"width": 140,
																												"fTSmaxRecords": "20",
																												"length": 12,
																												"browsable": true,
																												"autocompletable": false,
																												"helpText": "",
																												"autoCompleteSearchMode": "startswith",
																												"searchMode": "matches"
																											},
																											"stamp": 542,
																											"position": [
																												"Select from a table"
																											]
																										}
																									}
																								},
																								"Cell29": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 543,
																									"data": [],
																									"*": {
																										"WBSElement__": {
																											"type": "DatabaseComboBox",
																											"data": [
																												"WBSElement__"
																											],
																											"options": {
																												"version": 1,
																												"label": "_WBSElement",
																												"activable": true,
																												"width": 140,
																												"fTSmaxRecords": "20",
																												"browsable": true,
																												"autocompletable": false,
																												"length": 24,
																												"helpText": "",
																												"autoCompleteSearchMode": "startswith",
																												"searchMode": "matches"
																											},
																											"stamp": 544,
																											"position": [
																												"Select from a table"
																											]
																										}
																									}
																								},
																								"Cell30": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 545,
																									"data": [],
																									"*": {
																										"WBSElementID__": {
																											"type": "ShortText",
																											"data": [
																												"WBSElementID__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"label": "_WBSElementID",
																												"activable": true,
																												"width": 140,
																												"version": 0,
																												"helpText": "",
																												"readonly": true,
																												"browsable": false,
																												"autocompletable": false
																											},
																											"stamp": 546,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell31": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"hidden": false,
																										"version": 0
																									},
																									"stamp": 803,
																									"data": [],
																									"*": {
																										"FreeDimension1__": {
																											"type": "DatabaseComboBox",
																											"data": [
																												"FreeDimension1__"
																											],
																											"options": {
																												"version": 1,
																												"autoCompleteSearchMode": "contains",
																												"searchMode": "contains",
																												"label": "_FreeDimension1",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"helpIconPosition": "Right",
																												"minNbLines": 1,
																												"maxNbLines": 1,
																												"PreFillFTS": true,
																												"RestrictSearch": true,
																												"fTSmaxRecords": "20",
																												"browsable": true,
																												"length": 500
																											},
																											"stamp": 806,
																											"position": [
																												"_DatabaseComboBox"
																											]
																										}
																									}
																								},
																								"Cell32": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"hidden": false,
																										"version": 0
																									},
																									"stamp": 809,
																									"data": [],
																									"*": {
																										"FreeDimension1ID__": {
																											"type": "DatabaseComboBox",
																											"data": [
																												"FreeDimension1ID__"
																											],
																											"options": {
																												"version": 1,
																												"autoCompleteSearchMode": "startswith",
																												"searchMode": "matches",
																												"label": "_FreeDimension1ID",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"helpIconPosition": "Right",
																												"minNbLines": 1,
																												"maxNbLines": 1,
																												"length": 500,
																												"PreFillFTS": true,
																												"RestrictSearch": true,
																												"fTSmaxRecords": "20",
																												"browsable": true
																											},
																											"stamp": 812,
																											"position": [
																												"_DatabaseComboBox"
																											]
																										}
																									}
																								},
																								"Cell33": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 547,
																									"data": [],
																									"*": {
																										"TaxCode__": {
																											"type": "DatabaseComboBox",
																											"data": [
																												"TaxCode__"
																											],
																											"options": {
																												"label": "_Tax code",
																												"activable": true,
																												"width": "65",
																												"PreFillFTS": true,
																												"fTSmaxRecords": "20",
																												"version": 0,
																												"browsable": true,
																												"autoCompleteSearchMode": "startswith",
																												"searchMode": "matches",
																												"triggerHalfWidth": true,
																												"ftsMaxRecordsOnPages": 15,
																												"MultipleSelection": true
																											},
																											"stamp": 548
																										}
																									},
																									"position": [
																										{}
																									]
																								},
																								"Cell34": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 549,
																									"data": [],
																									"*": {
																										"TaxJurisdiction__": {
																											"type": "ShortText",
																											"data": [
																												"TaxJurisdiction__"
																											],
																											"options": {
																												"label": "_Jurisdiction code",
																												"activable": true,
																												"width": "100",
																												"browsable": true,
																												"version": 0,
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default"
																											},
																											"stamp": 550
																										}
																									},
																									"position": [
																										{}
																									]
																								},
																								"Cell35": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 551,
																									"data": [],
																									"*": {
																										"TaxAmount__": {
																											"type": "Decimal",
																											"data": [
																												"TaxAmount__"
																											],
																											"options": {
																												"integer": false,
																												"precision_internal": 2,
																												"label": "_Tax amount",
																												"activable": true,
																												"width": "80",
																												"readonly": true,
																												"version": 2,
																												"textSize": "S",
																												"textAlignment": "right",
																												"textStyle": "default",
																												"textColor": "default",
																												"precision_current": 2,
																												"triggerHalfWidth": true,
																												"currencySource": "InvoiceCurrency__"
																											},
																											"stamp": 552
																										}
																									},
																									"position": [
																										{}
																									]
																								},
																								"Cell36": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 553,
																									"data": [],
																									"*": {
																										"TaxRate__": {
																											"type": "Decimal",
																											"data": [
																												"TaxRate__"
																											],
																											"options": {
																												"integer": false,
																												"precision_internal": 3,
																												"label": "_Tax rate",
																												"activable": true,
																												"width": 50,
																												"readonly": true,
																												"version": 2,
																												"textSize": "S",
																												"textAlignment": "right",
																												"textStyle": "default",
																												"textColor": "default",
																												"precision_current": 3,
																												"precision_min": 2
																											},
																											"stamp": 554
																										}
																									}
																								},
																								"Cell37": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 785,
																									"data": [],
																									"*": {
																										"MultiTaxRates__": {
																											"type": "LongText",
																											"data": [
																												"MultiTaxRates__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"minNbLines": 3,
																												"label": "_MultiTaxRates",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"browsable": false
																											},
																											"stamp": 786,
																											"position": [
																												"_LongText"
																											]
																										}
																									}
																								},
																								"Cell38": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 555,
																									"data": [],
																									"*": {
																										"VendorNumber__": {
																											"type": "ShortText",
																											"data": [
																												"VendorNumber__"
																											],
																											"options": {
																												"label": "_Vendor number",
																												"activable": true,
																												"width": 140,
																												"readonly": true,
																												"version": 0,
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default"
																											},
																											"stamp": 556
																										}
																									}
																								},
																								"Cell39": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 557,
																									"data": [],
																									"*": {
																										"ExpectedQuantity__": {
																											"type": "Decimal",
																											"data": [
																												"ExpectedQuantity__"
																											],
																											"options": {
																												"integer": false,
																												"precision_internal": 2,
																												"label": "_Expected quantity",
																												"activable": true,
																												"width": "80",
																												"readonly": true,
																												"version": 2,
																												"textSize": "S",
																												"textAlignment": "right",
																												"textStyle": "default",
																												"textColor": "default",
																												"precision_current": 2
																											},
																											"stamp": 558
																										}
																									},
																									"position": [
																										{}
																									]
																								},
																								"Cell40": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 559,
																									"data": [],
																									"*": {
																										"ExpectedAmount__": {
																											"type": "Decimal",
																											"data": [
																												"ExpectedAmount__"
																											],
																											"options": {
																												"integer": false,
																												"precision_internal": 2,
																												"label": "_Expected amount",
																												"activable": true,
																												"width": "80",
																												"readonly": true,
																												"version": 2,
																												"textSize": "S",
																												"textAlignment": "right",
																												"textStyle": "default",
																												"textColor": "default",
																												"precision_current": 2,
																												"currencySource": "InvoiceCurrency__"
																											},
																											"stamp": 560
																										}
																									},
																									"position": [
																										{}
																									]
																								},
																								"Cell41": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 561,
																									"data": [],
																									"*": {
																										"OpenQuantity__": {
																											"type": "Decimal",
																											"data": [
																												"OpenQuantity__"
																											],
																											"options": {
																												"integer": false,
																												"precision_internal": 2,
																												"label": "_Open quantity",
																												"activable": true,
																												"width": "80",
																												"readonly": true,
																												"version": 2,
																												"textSize": "S",
																												"textAlignment": "right",
																												"textStyle": "default",
																												"textColor": "default",
																												"precision_current": 2
																											},
																											"stamp": 562
																										}
																									}
																								},
																								"Cell42": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 563,
																									"data": [],
																									"*": {
																										"OpenAmount__": {
																											"type": "Decimal",
																											"data": [
																												"OpenAmount__"
																											],
																											"options": {
																												"integer": false,
																												"precision_internal": 2,
																												"label": "_Open amount",
																												"activable": true,
																												"width": "80",
																												"readonly": true,
																												"version": 2,
																												"textSize": "S",
																												"textAlignment": "right",
																												"textStyle": "default",
																												"textColor": "default",
																												"precision_current": 2,
																												"currencySource": "InvoiceCurrency__"
																											},
																											"stamp": 564
																										}
																									}
																								},
																								"Cell43": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 565,
																									"data": [],
																									"*": {
																										"UnitPrice__": {
																											"type": "Decimal",
																											"data": [
																												"UnitPrice__"
																											],
																											"options": {
																												"integer": false,
																												"precision_internal": 4,
																												"label": "_Unit price",
																												"activable": true,
																												"width": 140,
																												"readonly": true,
																												"autocompletable": true,
																												"version": 2,
																												"textSize": "S",
																												"textAlignment": "right",
																												"textStyle": "default",
																												"textColor": "default",
																												"precision_current": 4,
																												"currencySource": "InvoiceCurrency__"
																											},
																											"stamp": 566
																										}
																									}
																								},
																								"Cell44": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 567,
																									"data": [],
																									"*": {
																										"Buyer__": {
																											"type": "ShortText",
																											"data": [
																												"Buyer__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"label": "_Buyer",
																												"activable": true,
																												"width": 140,
																												"version": 0
																											},
																											"stamp": 568,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell45": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 569,
																									"data": [],
																									"*": {
																										"Receiver__": {
																											"type": "ShortText",
																											"data": [
																												"Receiver__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"label": "_Receiver",
																												"activable": true,
																												"width": 140,
																												"browsable": false,
																												"version": 0
																											},
																											"stamp": 570,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell46": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 571,
																									"data": [],
																									"*": {
																										"IsLocalPO__": {
																											"type": "CheckBox",
																											"data": [
																												"IsLocalPO__"
																											],
																											"options": {
																												"label": "_IsLocalPO_V2",
																												"activable": true,
																												"width": "20",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"readonly": true,
																												"version": 0
																											},
																											"stamp": 572,
																											"position": [
																												"Check box"
																											]
																										}
																									}
																								},
																								"Cell47": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 573,
																									"data": [],
																									"*": {
																										"DifferentInvoicingParty__": {
																											"type": "ShortText",
																											"data": [
																												"DifferentInvoicingParty__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"label": "_DifferentInvoicingParty",
																												"activable": true,
																												"width": 140,
																												"version": 0
																											},
																											"stamp": 574,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell48": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 575,
																									"data": [],
																									"*": {
																										"AcctAssCat__": {
																											"type": "ShortText",
																											"data": [
																												"AcctAssCat__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"label": "_Account assignment category",
																												"activable": true,
																												"readonly": true,
																												"width": 40,
																												"version": 0
																											},
																											"stamp": 576,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell49": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 577,
																									"data": [],
																									"*": {
																										"NoGoodsReceipt__": {
																											"type": "CheckBox",
																											"data": [
																												"NoGoodsReceipt__"
																											],
																											"options": {
																												"label": "_PO_NoGoodsReceipt",
																												"activable": true,
																												"width": "40",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"readonly": true,
																												"version": 0
																											},
																											"stamp": 578,
																											"position": [
																												"Check box"
																											]
																										}
																									}
																								},
																								"Cell50": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 579,
																									"data": [],
																									"*": {
																										"UnitOfMeasureCode__": {
																											"type": "ShortText",
																											"data": [
																												"UnitOfMeasureCode__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"label": "_UnitOfMeasureCode",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"length": 140,
																												"browsable": false
																											},
																											"stamp": 580,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell51": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 805,
																									"data": [],
																									"*": {
																										"TechnicalDetails__": {
																											"type": "LongText",
																											"data": [
																												"TechnicalDetails__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"minNbLines": 3,
																												"label": "_TechnicalDetails",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"helpIconPosition": "Right",
																												"readonly": true,
																												"browsable": false
																											},
																											"stamp": 808,
																											"position": [
																												"_LongText"
																											]
																										}
																									}
																								}
																							}
																						}
																					}
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-27": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 581,
													"*": {
														"Line_Items_Options": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": true,
																"hideTitleBar": true,
																"hideTitle": true,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "_Line Items Options",
																"leftImageURL": "",
																"removeMargins": false,
																"elementsAlignment": "left",
																"version": 0,
																"sameHeightAsSiblings": false
															},
															"stamp": 582,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"LabelCodingTemplate__": "CodingTemplate__",
																			"CodingTemplate__": "LabelCodingTemplate__",
																			"LabelCalculateTax__": "CalculateTax__",
																			"CalculateTax__": "LabelCalculateTax__",
																			"SubsequentDocument__": "LabelSubsequentDocument__",
																			"LabelSubsequentDocument__": "SubsequentDocument__"
																		},
																		"version": 0
																	},
																	"stamp": 583,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"LabelCodingTemplate__": {
																						"line": 3,
																						"column": 1
																					},
																					"CodingTemplate__": {
																						"line": 3,
																						"column": 2
																					},
																					"TemplateWarning__": {
																						"line": 2,
																						"column": 1
																					},
																					"LabelCalculateTax__": {
																						"line": 1,
																						"column": 1
																					},
																					"CalculateTax__": {
																						"line": 1,
																						"column": 2
																					},
																					"SubsequentDocument__": {
																						"line": 4,
																						"column": 2
																					},
																					"LabelSubsequentDocument__": {
																						"line": 4,
																						"column": 1
																					}
																				},
																				"lines": 4,
																				"columns": 2,
																				"colspans": [
																					[],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[],
																					[]
																				],
																				"version": 0
																			},
																			"stamp": 584,
																			"*": {
																				"LabelCalculateTax__": {
																					"type": "Label",
																					"data": [
																						"CalculateTax__"
																					],
																					"options": {
																						"label": "_Calculate tax",
																						"version": 0
																					},
																					"stamp": 585
																				},
																				"CalculateTax__": {
																					"type": "CheckBox",
																					"data": [
																						"CalculateTax__"
																					],
																					"options": {
																						"label": "_Calculate tax",
																						"activable": true,
																						"defaultValue": true,
																						"version": 0
																					},
																					"stamp": 586
																				},
																				"TemplateWarning__": {
																					"type": "Text",
																					"data": false,
																					"options": {
																						"marge": 0,
																						"label": "_TemplateWarning",
																						"version": 0,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "color3",
																						"backgroundcolor": "default",
																						"iconClass": "",
																						"hidden": true
																					},
																					"stamp": 587
																				},
																				"LabelCodingTemplate__": {
																					"type": "Label",
																					"data": [
																						"CodingTemplate__"
																					],
																					"options": {
																						"label": "_CodingTemplate",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 588
																				},
																				"CodingTemplate__": {
																					"type": "DatabaseComboBox",
																					"data": [
																						"CodingTemplate__"
																					],
																					"options": {
																						"label": "_CodingTemplate",
																						"activable": true,
																						"width": 230,
																						"fTSmaxRecords": "20",
																						"PreFillFTS": true,
																						"browsable": false,
																						"readonly": true,
																						"version": 0,
																						"hidden": true,
																						"autoCompleteSearchMode": "startswith",
																						"searchMode": "matches"
																					},
																					"stamp": 589
																				},
																				"LabelSubsequentDocument__": {
																					"type": "Label",
																					"data": [
																						"SubsequentDocument__"
																					],
																					"options": {
																						"label": "_SubsequentDocument",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 590
																				},
																				"SubsequentDocument__": {
																					"type": "CheckBox",
																					"data": [
																						"SubsequentDocument__"
																					],
																					"options": {
																						"label": "_SubsequentDocument",
																						"activable": true,
																						"width": "0",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 591
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-31": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 592,
													"*": {
														"Line_Items_Actions": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": true,
																"hideTitleBar": true,
																"hideTitle": true,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "_Line Items Actions",
																"leftImageURL": "",
																"removeMargins": true,
																"elementsAlignment": "center",
																"version": 0,
																"sameHeightAsSiblings": false
															},
															"stamp": 593,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {},
																		"version": 0
																	},
																	"stamp": 594,
																	"*": {
																		"Grid": {
																			"type": "HorizontalLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"ButtonLoadTemplate__": {
																						"line": 1,
																						"column": 1
																					},
																					"ButtonSaveTemplate__": {
																						"line": 2,
																						"column": 1
																					},
																					"ButtonLoadClipboard__": {
																						"line": 3,
																						"column": 1
																					}
																				},
																				"lines": 3,
																				"colspans": [
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					]
																				],
																				"version": 0
																			},
																			"stamp": 595,
																			"*": {
																				"ButtonLoadClipboard__": {
																					"type": "FormButton",
																					"data": false,
																					"options": {
																						"buttonLabel": "_Load clipboard",
																						"label": "",
																						"width": "",
																						"version": 0,
																						"urlImage": "AP_LoadClipboard.png",
																						"style": 4
																					},
																					"stamp": 596
																				},
																				"ButtonSaveTemplate__": {
																					"type": "FormButton",
																					"data": false,
																					"options": {
																						"buttonLabel": "_Save template",
																						"label": "",
																						"width": "",
																						"version": 0,
																						"urlImage": "AP_SaveTemplate.png",
																						"style": 4
																					},
																					"stamp": 597
																				},
																				"ButtonLoadTemplate__": {
																					"type": "FormButton",
																					"data": false,
																					"options": {
																						"buttonLabel": "_Load template",
																						"label": "",
																						"width": "",
																						"version": 0,
																						"urlImage": "AP_LoadTemplate.png",
																						"style": 4
																					},
																					"stamp": 598
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-30": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 599,
													"*": {
														"HoldsPane": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "M",
																"hideActionButtons": true,
																"hideTitleBar": false,
																"hideTitle": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "_HoldsPane",
																"leftImageURL": "",
																"removeMargins": false,
																"elementsAlignment": "left",
																"version": 0,
																"sameHeightAsSiblings": false
															},
															"stamp": 600,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"ActiveHoldsCount__": "LabelActiveHoldsCount__",
																			"LabelActiveHoldsCount__": "ActiveHoldsCount__",
																			"LastHoldReleaseDate__": "LabelLastHoldReleaseDate__",
																			"LabelLastHoldReleaseDate__": "LastHoldReleaseDate__"
																		},
																		"version": 0
																	},
																	"stamp": 601,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"Holds__": {
																						"line": 1,
																						"column": 1
																					},
																					"ActiveHoldsCount__": {
																						"line": 2,
																						"column": 2
																					},
																					"LabelActiveHoldsCount__": {
																						"line": 2,
																						"column": 1
																					},
																					"LastHoldReleaseDate__": {
																						"line": 3,
																						"column": 2
																					},
																					"LabelLastHoldReleaseDate__": {
																						"line": 3,
																						"column": 1
																					}
																				},
																				"lines": 3,
																				"columns": 2,
																				"colspans": [
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[],
																					[]
																				],
																				"version": 0
																			},
																			"stamp": 602,
																			"*": {
																				"Holds__": {
																					"type": "Table",
																					"data": [
																						"Holds__"
																					],
																					"options": {
																						"version": 5,
																						"lines": 10,
																						"columns": 7,
																						"displayEmptyLines": false,
																						"alwaysOneLine": true,
																						"menuOptions": {
																							"disableInsertLine": true,
																							"disableAddLine": true,
																							"hideTopNavigation": false,
																							"hideBottomNavigation": false,
																							"hideTableRowMenu": true,
																							"hideTableRowDelete": true,
																							"hideTableRowAdd": true,
																							"hideTableRowDuplicate": true,
																							"addButtonShouldInsert": false,
																							"hideTableRowButtonDuplicate": true
																						},
																						"item": {
																							"display": null
																						},
																						"group": {
																							"name": "GDR\nPage"
																						},
																						"label": "_Holds",
																						"subsection": null
																					},
																					"stamp": 603,
																					"*": {
																						"Actions": {
																							"type": "$TableActions",
																							"subtype": "Actions",
																							"options": {
																								"version": 0
																							},
																							"stamp": 604,
																							"*": {
																								"Top": {
																									"type": "$TableActions",
																									"subtype": "Top",
																									"options": {
																										"version": 0
																									},
																									"stamp": 605
																								},
																								"Bottom": {
																									"type": "$TableActions",
																									"subtype": "Top",
																									"options": {
																										"version": 0
																									},
																									"stamp": 606
																								}
																							}
																						},
																						"Header": {
																							"type": "$TableRow",
																							"subtype": "Header",
																							"options": {
																								"version": 0
																							},
																							"stamp": 607,
																							"data": [
																								null
																							],
																							"*": {
																								"Cell1": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 608,
																									"data": [],
																									"*": {
																										"HoldName__": {
																											"type": "Label",
																											"data": [
																												"HoldName__"
																											],
																											"options": {
																												"label": "_Hold Name",
																												"version": 0
																											},
																											"stamp": 609,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell2": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 610,
																									"data": [],
																									"*": {
																										"HoldReason__": {
																											"type": "Label",
																											"data": [
																												"HoldReason__"
																											],
																											"options": {
																												"label": "_Hold Reason",
																												"version": 0
																											},
																											"stamp": 611,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell3": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 612,
																									"data": [],
																									"*": {
																										"HoldDate__": {
																											"type": "Label",
																											"data": [
																												"HoldDate__"
																											],
																											"options": {
																												"label": "_Hold Date",
																												"version": 0
																											},
																											"stamp": 613,
																											"position": [
																												"Date"
																											]
																										}
																									}
																								},
																								"Cell4": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 614,
																									"data": [],
																									"*": {
																										"ReleaseName__": {
																											"type": "Label",
																											"data": [
																												"ReleaseName__"
																											],
																											"options": {
																												"label": "_Release Name",
																												"version": 0
																											},
																											"stamp": 615,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell5": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 616,
																									"data": [],
																									"*": {
																										"ReleaseReason__": {
																											"type": "Label",
																											"data": [
																												"ReleaseReason__"
																											],
																											"options": {
																												"label": "_Release Reason",
																												"version": 0
																											},
																											"stamp": 617,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell6": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 618,
																									"data": [],
																									"*": {
																										"ReleaseBy__": {
																											"type": "Label",
																											"data": [
																												"ReleaseBy__"
																											],
																											"options": {
																												"label": "_Release By",
																												"version": 0
																											},
																											"stamp": 619,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell7": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 620,
																									"data": [],
																									"*": {
																										"ReleaseDate__": {
																											"type": "Label",
																											"data": [
																												"ReleaseDate__"
																											],
																											"options": {
																												"label": "_Release Date",
																												"version": 0
																											},
																											"stamp": 621,
																											"position": [
																												"Date"
																											]
																										}
																									}
																								}
																							}
																						},
																						"Clipboard": {
																							"type": "$TableRow",
																							"subtype": "Clipboard",
																							"options": {
																								"version": 0
																							},
																							"stamp": 622,
																							"data": [
																								null
																							],
																							"*": {
																								"Cell1": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 623,
																									"data": [],
																									"*": {
																										"HoldName__": {
																											"type": "ShortText",
																											"data": [
																												"HoldName__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"label": "_Hold Name",
																												"activable": true,
																												"width": 100,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"readonly": true,
																												"browsable": false
																											},
																											"stamp": 624,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell2": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 625,
																									"data": [],
																									"*": {
																										"HoldReason__": {
																											"type": "ShortText",
																											"data": [
																												"HoldReason__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"label": "_Hold Reason",
																												"activable": true,
																												"width": 300,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"readonly": true,
																												"browsable": false
																											},
																											"stamp": 626,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell3": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 627,
																									"data": [],
																									"*": {
																										"HoldDate__": {
																											"type": "DateTime",
																											"data": [
																												"HoldDate__"
																											],
																											"options": {
																												"displayLongFormat": false,
																												"label": "_Hold Date",
																												"activable": true,
																												"width": 80,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"readonly": true,
																												"version": 0
																											},
																											"stamp": 628,
																											"position": [
																												"Date"
																											]
																										}
																									}
																								},
																								"Cell4": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 629,
																									"data": [],
																									"*": {
																										"ReleaseName__": {
																											"type": "ShortText",
																											"data": [
																												"ReleaseName__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"label": "_Release Name",
																												"activable": true,
																												"width": 100,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"readonly": true,
																												"browsable": false
																											},
																											"stamp": 630,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell5": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 631,
																									"data": [],
																									"*": {
																										"ReleaseReason__": {
																											"type": "ShortText",
																											"data": [
																												"ReleaseReason__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"label": "_Release Reason",
																												"activable": true,
																												"width": 200,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"readonly": true,
																												"browsable": false
																											},
																											"stamp": 632,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell6": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 633,
																									"data": [],
																									"*": {
																										"ReleaseBy__": {
																											"type": "ShortText",
																											"data": [
																												"ReleaseBy__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"label": "_Release By",
																												"activable": true,
																												"width": 100,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"readonly": true,
																												"browsable": false
																											},
																											"stamp": 634,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell7": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 635,
																									"data": [],
																									"*": {
																										"ReleaseDate__": {
																											"type": "DateTime",
																											"data": [
																												"ReleaseDate__"
																											],
																											"options": {
																												"displayLongFormat": false,
																												"label": "_Release Date",
																												"activable": true,
																												"width": 80,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"readonly": true,
																												"version": 0
																											},
																											"stamp": 636,
																											"position": [
																												"Date"
																											]
																										}
																									}
																								}
																							}
																						}
																					}
																				},
																				"LabelActiveHoldsCount__": {
																					"type": "Label",
																					"data": [
																						"ActiveHoldsCount__"
																					],
																					"options": {
																						"label": "_Active holds count",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 637
																				},
																				"ActiveHoldsCount__": {
																					"type": "Integer",
																					"data": [
																						"ActiveHoldsCount__"
																					],
																					"options": {
																						"version": 2,
																						"textSize": "S",
																						"textAlignment": "right",
																						"textStyle": "default",
																						"textColor": "default",
																						"integer": true,
																						"label": "_Active holds count",
																						"precision_internal": 0,
																						"precision_current": 0,
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"browsable": false,
																						"hidden": true
																					},
																					"stamp": 638
																				},
																				"LabelLastHoldReleaseDate__": {
																					"type": "Label",
																					"data": [
																						"LastHoldReleaseDate__"
																					],
																					"options": {
																						"label": "_LastHoldReleaseDate",
																						"version": 0
																					},
																					"stamp": 787
																				},
																				"LastHoldReleaseDate__": {
																					"type": "RealDateTime",
																					"data": [
																						"LastHoldReleaseDate__"
																					],
																					"options": {
																						"readonly": true,
																						"displayLongFormat": false,
																						"label": "_LastHoldReleaseDate",
																						"activable": true,
																						"width": 230,
																						"version": 0,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"helpIconPosition": "Right"
																					},
																					"stamp": 788
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-19": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 639,
													"*": {
														"ExceptionsType": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": 0,
																"iconURL": "AP_ExceptionsType.png",
																"TitleFontSize": "M",
																"hideActionButtons": true,
																"hideTitleBar": false,
																"hideTitle": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "_ExceptionsType",
																"leftImageURL": "",
																"removeMargins": false,
																"version": 0,
																"sameHeightAsSiblings": false
															},
															"stamp": 640,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"CurrentException__": "LabelCurrentException__",
																			"LabelCurrentException__": "CurrentException__"
																		},
																		"version": 0
																	},
																	"stamp": 641,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"CurrentException__": {
																						"line": 1,
																						"column": 2
																					},
																					"LabelCurrentException__": {
																						"line": 1,
																						"column": 1
																					}
																				},
																				"lines": 1,
																				"columns": 2,
																				"colspans": [
																					[]
																				],
																				"version": 0
																			},
																			"stamp": 642,
																			"*": {
																				"LabelCurrentException__": {
																					"type": "Label",
																					"data": [
																						"CurrentException__"
																					],
																					"options": {
																						"label": "_Current exception",
																						"version": 0
																					},
																					"stamp": 643
																				},
																				"CurrentException__": {
																					"type": "DatabaseComboBox",
																					"data": [
																						"CurrentException__"
																					],
																					"options": {
																						"version": 1,
																						"label": "_Current exception",
																						"activable": true,
																						"width": 230,
																						"fTSmaxRecords": "20",
																						"browsable": true,
																						"RestrictSearch": true,
																						"PreFillFTS": true,
																						"autoCompleteSearchMode": "startswith",
																						"searchMode": "contains"
																					},
																					"stamp": 644
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-4": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 645,
													"*": {
														"WorkflowDataPanel": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": "160",
																"label": "_Payment Approval",
																"version": 0,
																"hideTitleBar": false,
																"iconURL": "AP_WorkflowDataPanel.png",
																"TitleFontSize": "M",
																"hideActionButtons": true,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"hideTitle": false,
																"sameHeightAsSiblings": false
															},
															"stamp": 646,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"PaymentApprovalStatus__": "LabelPaymentApprovalStatus__",
																			"LabelPaymentApprovalStatus__": "PaymentApprovalStatus__",
																			"ComputingWorkflow__": "LabelComputingWorkflow__",
																			"LabelComputingWorkflow__": "ComputingWorkflow__",
																			"LabelPaymentApprovalMode__": "PaymentApprovalMode__",
																			"PaymentApprovalMode__": "LabelPaymentApprovalMode__",
																			"WorkflowInitiator__": "LabelWorkflowInitiator__",
																			"LabelWorkflowInitiator__": "WorkflowInitiator__",
																			"PostedBy__": "LabelPostedBy__",
																			"LabelPostedBy__": "PostedBy__"
																		},
																		"version": 0
																	},
																	"stamp": 647,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"Comment__": {
																						"line": 2,
																						"column": 1
																					},
																					"SpacerComment__": {
																						"line": 3,
																						"column": 1
																					},
																					"ApproversList__": {
																						"line": 4,
																						"column": 1
																					},
																					"LabelPaymentApprovalStatus__": {
																						"line": 5,
																						"column": 1
																					},
																					"PaymentApprovalStatus__": {
																						"line": 5,
																						"column": 2
																					},
																					"ComputingWorkflow__": {
																						"line": 6,
																						"column": 2
																					},
																					"LabelComputingWorkflow__": {
																						"line": 6,
																						"column": 1
																					},
																					"LabelPaymentApprovalMode__": {
																						"line": 1,
																						"column": 1
																					},
																					"PaymentApprovalMode__": {
																						"line": 1,
																						"column": 2
																					},
																					"WorkflowInitiator__": {
																						"line": 7,
																						"column": 2
																					},
																					"LabelWorkflowInitiator__": {
																						"line": 7,
																						"column": 1
																					},
																					"PostedBy__": {
																						"line": 8,
																						"column": 2
																					},
																					"LabelPostedBy__": {
																						"line": 8,
																						"column": 1
																					}
																				},
																				"lines": 8,
																				"columns": 2,
																				"colspans": [
																					[],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[],
																					[],
																					[],
																					[]
																				],
																				"version": 0
																			},
																			"stamp": 648,
																			"*": {
																				"LabelPaymentApprovalMode__": {
																					"type": "Label",
																					"data": [
																						"PaymentApprovalMode__"
																					],
																					"options": {
																						"label": "_Payment approval mode",
																						"hidden": true,
																						"version": 0
																					},
																					"stamp": 649
																				},
																				"PaymentApprovalMode__": {
																					"type": "ComboBox",
																					"data": [
																						"PaymentApprovalMode__"
																					],
																					"options": {
																						"possibleValues": {
																							"0": "Approved for payment first, then posted",
																							"1": "Posted first, then sent for payment approval"
																						},
																						"version": 1,
																						"keyValueMode": true,
																						"possibleKeys": {
																							"0": "ApproveFirst",
																							"1": "PostFirst"
																						},
																						"label": "_Payment approval mode",
																						"activable": true,
																						"width": "300",
																						"hidden": true,
																						"maxNbLinesToDisplay": 10
																					},
																					"stamp": 650
																				},
																				"Comment__": {
																					"type": "LongText",
																					"data": [
																						"Comment__"
																					],
																					"options": {
																						"label": "_Comment",
																						"activable": true,
																						"width": "100%",
																						"numberOfLines": 3,
																						"version": 0,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"browsable": false,
																						"minNbLines": 3
																					},
																					"stamp": 651
																				},
																				"SpacerComment__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"label": "Spacer line",
																						"version": 0,
																						"height": "",
																						"width": "",
																						"color": "default"
																					},
																					"stamp": 652
																				},
																				"ApproversList__": {
																					"type": "Table",
																					"data": [
																						"ApproversList__"
																					],
																					"options": {
																						"version": 5,
																						"lines": 10,
																						"columns": 13,
																						"displayEmptyLines": false,
																						"alwaysOneLine": false,
																						"item": {
																							"display": null
																						},
																						"group": {
																							"name": "GDR\nPage"
																						},
																						"label": "_ApproversTable",
																						"menuOptions": {
																							"disableInsertLine": true,
																							"disableAddLine": true,
																							"hideTopNavigation": true,
																							"hideBottomNavigation": true,
																							"hideTableRowMenu": true,
																							"hideTableRowDelete": false,
																							"hideTableRowAdd": false,
																							"addButtonShouldInsert": false,
																							"hideTableRowDuplicate": true,
																							"hideTableRowButtonDuplicate": true
																						},
																						"readonly": true,
																						"subsection": null
																					},
																					"stamp": 653,
																					"*": {
																						"Actions": {
																							"type": "$TableActions",
																							"subtype": "Actions",
																							"options": {
																								"version": 0
																							},
																							"stamp": 654,
																							"*": {
																								"Top": {
																									"type": "$TableActions",
																									"subtype": "Top",
																									"options": {
																										"version": 0
																									},
																									"stamp": 655,
																									"data": []
																								},
																								"Bottom": {
																									"type": "$TableActions",
																									"subtype": "Top",
																									"options": {
																										"version": 0
																									},
																									"stamp": 656,
																									"data": []
																								}
																							},
																							"data": []
																						},
																						"Header": {
																							"type": "$TableRow",
																							"subtype": "Header",
																							"options": {
																								"version": 0
																							},
																							"stamp": 657,
																							"data": [
																								null
																							],
																							"*": {
																								"Cell1": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 658,
																									"data": [],
																									"*": {
																										"LineMarker__": {
																											"type": "Label",
																											"data": [
																												"LineMarker__"
																											],
																											"options": {
																												"label": "",
																												"version": 0
																											},
																											"stamp": 659
																										}
																									},
																									"position": [
																										{}
																									]
																								},
																								"Cell2": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 660,
																									"data": [],
																									"*": {
																										"Approver__": {
																											"type": "Label",
																											"data": [
																												"Approver__"
																											],
																											"options": {
																												"label": "_Approver",
																												"version": 0
																											},
																											"stamp": 661
																										}
																									},
																									"position": [
																										{}
																									]
																								},
																								"Cell3": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 662,
																									"data": [],
																									"*": {
																										"ApproverLabelRole__": {
																											"type": "Label",
																											"data": [
																												"ApproverLabelRole__"
																											],
																											"options": {
																												"label": "_ApproverLabelRole",
																												"version": 0
																											},
																											"stamp": 663
																										}
																									},
																									"position": [
																										{}
																									]
																								},
																								"Cell4": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 664,
																									"data": [],
																									"*": {
																										"ApprovalDate__": {
																											"type": "Label",
																											"data": [
																												"ApprovalDate__"
																											],
																											"options": {
																												"label": "_ApprovalDate",
																												"version": 0
																											},
																											"stamp": 665
																										}
																									},
																									"position": [
																										{}
																									]
																								},
																								"Cell5": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 666,
																									"data": [],
																									"*": {
																										"ApproverComment__": {
																											"type": "Label",
																											"data": [
																												"ApproverComment__"
																											],
																											"options": {
																												"label": "_ApproverComment",
																												"version": 0
																											},
																											"stamp": 667
																										}
																									},
																									"position": [
																										{}
																									]
																								},
																								"Cell6": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 668,
																									"data": [],
																									"*": {
																										"ApproverAction__": {
																											"type": "Label",
																											"data": [
																												"ApproverAction__"
																											],
																											"options": {
																												"label": "_ApproverAction",
																												"version": 0
																											},
																											"stamp": 669
																										}
																									},
																									"position": [
																										{}
																									]
																								},
																								"Cell7": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 670,
																									"data": [],
																									"*": {
																										"WorkflowIndex__": {
																											"type": "Label",
																											"data": [
																												"WorkflowIndex__"
																											],
																											"options": {
																												"label": "_WorkflowIndex",
																												"version": 0
																											},
																											"stamp": 671
																										}
																									},
																									"position": [
																										{}
																									]
																								},
																								"Cell8": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 672,
																									"data": [],
																									"*": {
																										"Approved__": {
																											"type": "Label",
																											"data": [
																												"Approved__"
																											],
																											"options": {
																												"label": "_Approved",
																												"version": 0
																											},
																											"stamp": 673
																										}
																									}
																								},
																								"Cell9": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 674,
																									"data": [],
																									"*": {
																										"ApproverID__": {
																											"type": "Label",
																											"data": [
																												"ApproverID__"
																											],
																											"options": {
																												"label": "_ApproverID",
																												"version": 0
																											},
																											"stamp": 675
																										}
																									},
																									"position": [
																										{}
																									]
																								},
																								"Cell10": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 676,
																									"data": [],
																									"*": {
																										"ApproverEmail__": {
																											"type": "Label",
																											"data": [
																												"ApproverEmail__"
																											],
																											"options": {
																												"label": "_ApproverEmail",
																												"version": 0
																											},
																											"stamp": 677
																										}
																									}
																								},
																								"Cell11": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 678,
																									"data": [],
																									"*": {
																										"WRKFIsGroup__": {
																											"type": "Label",
																											"data": [
																												"WRKFIsGroup__"
																											],
																											"options": {
																												"label": "_WRKFIsGroup",
																												"version": 0
																											},
																											"stamp": 679,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell12": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 680,
																									"data": [],
																									"*": {
																										"ApprovalRequestDate__": {
																											"type": "Label",
																											"data": [
																												"ApprovalRequestDate__"
																											],
																											"options": {
																												"label": "_ApprovalRequestDate",
																												"version": 0
																											},
																											"stamp": 681,
																											"position": [
																												"Date/Time"
																											]
																										}
																									}
																								},
																								"Cell13": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": 140,
																										"version": 0
																									},
																									"stamp": 789,
																									"data": [],
																									"*": {
																										"ActualApprover__": {
																											"type": "Label",
																											"data": [
																												"ActualApprover__"
																											],
																											"options": {
																												"label": "_ActualApprover",
																												"minwidth": 140,
																												"version": 0
																											},
																											"stamp": 790,
																											"position": [
																												"_ShortText"
																											]
																										}
																									}
																								}
																							}
																						},
																						"Clipboard": {
																							"type": "$TableRow",
																							"subtype": "Clipboard",
																							"options": {
																								"version": 0
																							},
																							"stamp": 682,
																							"data": [
																								null
																							],
																							"*": {
																								"Cell1": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 683,
																									"data": [],
																									"*": {
																										"LineMarker__": {
																											"type": "ShortText",
																											"data": [
																												"LineMarker__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"label": "",
																												"activable": true,
																												"width": "0",
																												"browsable": false,
																												"autocompletable": false,
																												"version": 0
																											},
																											"stamp": 684
																										}
																									},
																									"position": [
																										{}
																									]
																								},
																								"Cell2": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 685,
																									"data": [],
																									"*": {
																										"Approver__": {
																											"type": "ShortText",
																											"data": [
																												"Approver__"
																											],
																											"options": {
																												"label": "_Approver",
																												"activable": true,
																												"width": "200",
																												"length": 100,
																												"readonly": true,
																												"browsable": true,
																												"version": 0,
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default"
																											},
																											"stamp": 686
																										}
																									},
																									"position": [
																										{}
																									]
																								},
																								"Cell3": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 687,
																									"data": [],
																									"*": {
																										"ApproverLabelRole__": {
																											"type": "ShortText",
																											"data": [
																												"ApproverLabelRole__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"label": "_ApproverLabelRole",
																												"activable": true,
																												"width": "120",
																												"browsable": false,
																												"version": 0
																											},
																											"stamp": 688
																										}
																									},
																									"position": [
																										{}
																									]
																								},
																								"Cell4": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 689,
																									"data": [],
																									"*": {
																										"ApprovalDate__": {
																											"type": "RealDateTime",
																											"data": [
																												"ApprovalDate__"
																											],
																											"options": {
																												"readonly": true,
																												"displayLongFormat": false,
																												"label": "_ApprovalDate",
																												"activable": true,
																												"width": "140",
																												"version": 0
																											},
																											"stamp": 690
																										}
																									},
																									"position": [
																										{}
																									]
																								},
																								"Cell5": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 691,
																									"data": [],
																									"*": {
																										"ApproverComment__": {
																											"type": "LongText",
																											"data": [
																												"ApproverComment__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"label": "_ApproverComment",
																												"activable": true,
																												"width": "330",
																												"numberOfLines": 5,
																												"resizable": true,
																												"browsable": false,
																												"version": 0,
																												"minNbLines": 1
																											},
																											"stamp": 692
																										}
																									},
																									"position": [
																										{}
																									]
																								},
																								"Cell6": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 693,
																									"data": [],
																									"*": {
																										"ApproverAction__": {
																											"type": "ShortText",
																											"data": [
																												"ApproverAction__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"label": "_ApproverAction",
																												"activable": true,
																												"width": 140,
																												"browsable": false,
																												"version": 0
																											},
																											"stamp": 694
																										}
																									},
																									"position": [
																										{}
																									]
																								},
																								"Cell7": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 695,
																									"data": [],
																									"*": {
																										"WorkflowIndex__": {
																											"type": "ShortText",
																											"data": [
																												"WorkflowIndex__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"label": "_WorkflowIndex",
																												"activable": true,
																												"width": "90",
																												"browsable": false,
																												"version": 0
																											},
																											"stamp": 696
																										}
																									},
																									"position": [
																										{}
																									]
																								},
																								"Cell8": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 697,
																									"data": [],
																									"*": {
																										"Approved__": {
																											"type": "CheckBox",
																											"data": [
																												"Approved__"
																											],
																											"options": {
																												"label": "_Approved",
																												"activable": true,
																												"width": 140,
																												"readonly": true,
																												"version": 0
																											},
																											"stamp": 698
																										}
																									}
																								},
																								"Cell9": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 699,
																									"data": [],
																									"*": {
																										"ApproverID__": {
																											"type": "ShortText",
																											"data": [
																												"ApproverID__"
																											],
																											"options": {
																												"label": "_ApproverID",
																												"activable": true,
																												"readonly": true,
																												"width": "200",
																												"version": 0,
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default"
																											},
																											"stamp": 700
																										}
																									},
																									"position": [
																										{}
																									]
																								},
																								"Cell10": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 701,
																									"data": [],
																									"*": {
																										"ApproverEmail__": {
																											"type": "ShortText",
																											"data": [
																												"ApproverEmail__"
																											],
																											"options": {
																												"label": "_ApproverEmail",
																												"activable": true,
																												"width": 140,
																												"length": 100,
																												"version": 0,
																												"readonly": true,
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default"
																											},
																											"stamp": 702
																										}
																									}
																								},
																								"Cell11": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 703,
																									"data": [],
																									"*": {
																										"WRKFIsGroup__": {
																											"type": "ShortText",
																											"data": [
																												"WRKFIsGroup__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"label": "_WRKFIsGroup",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"browsable": false,
																												"autocompletable": false,
																												"version": 0
																											},
																											"stamp": 704,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell12": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 705,
																									"data": [],
																									"*": {
																										"ApprovalRequestDate__": {
																											"type": "RealDateTime",
																											"data": [
																												"ApprovalRequestDate__"
																											],
																											"options": {
																												"readonly": true,
																												"displayLongFormat": false,
																												"label": "_ApprovalRequestDate",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"version": 0
																											},
																											"stamp": 706,
																											"position": [
																												"Date/Time"
																											]
																										}
																									}
																								},
																								"Cell13": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 791,
																									"data": [],
																									"*": {
																										"ActualApprover__": {
																											"type": "ShortText",
																											"data": [
																												"ActualApprover__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"label": "_ActualApprover",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"helpIconPosition": "Right",
																												"length": 100,
																												"readonly": true,
																												"browsable": false
																											},
																											"stamp": 792,
																											"position": [
																												"_ShortText"
																											]
																										}
																									}
																								}
																							}
																						}
																					}
																				},
																				"LabelPaymentApprovalStatus__": {
																					"type": "Label",
																					"data": [
																						"PaymentApprovalStatus__"
																					],
																					"options": {
																						"label": "_Payment approval status",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 707
																				},
																				"PaymentApprovalStatus__": {
																					"type": "ComboBox",
																					"data": [
																						"PaymentApprovalStatus__"
																					],
																					"options": {
																						"possibleValues": {
																							"0": "Not requested",
																							"1": "Pending",
																							"2": "Approved"
																						},
																						"keyValueMode": true,
																						"possibleKeys": {
																							"0": "Not requested",
																							"1": "Pending",
																							"2": "Approved"
																						},
																						"label": "_Payment approval status",
																						"activable": true,
																						"width": 230,
																						"readonly": true,
																						"version": 1,
																						"readonlyIsText": false,
																						"hidden": true,
																						"maxNbLinesToDisplay": 10
																					},
																					"stamp": 708
																				},
																				"LabelComputingWorkflow__": {
																					"type": "Label",
																					"data": false,
																					"options": {
																						"label": "_ComputingWorkflow",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 709
																				},
																				"ComputingWorkflow__": {
																					"type": "HTML",
																					"data": false,
																					"options": {
																						"label": "_ComputingWorkflow",
																						"htmlContent": "<div class=\"WaitingIcon-Small\"></div>",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 710
																				},
																				"LabelWorkflowInitiator__": {
																					"type": "Label",
																					"data": [
																						"WorkflowInitiator__"
																					],
																					"options": {
																						"label": "_WorkflowInitiator",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 711
																				},
																				"WorkflowInitiator__": {
																					"type": "ShortText",
																					"data": [
																						"WorkflowInitiator__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"label": "_WorkflowInitiator",
																						"activable": true,
																						"width": 230,
																						"browsable": false,
																						"version": 0,
																						"readonly": true,
																						"hidden": true
																					},
																					"stamp": 712
																				},
																				"LabelPostedBy__": {
																					"type": "Label",
																					"data": [
																						"PostedBy__"
																					],
																					"options": {
																						"label": "_Posted by",
																						"hidden": true,
																						"version": 0
																					},
																					"stamp": 713
																				},
																				"PostedBy__": {
																					"type": "ShortText",
																					"data": [
																						"PostedBy__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"label": "_Posted by",
																						"activable": true,
																						"width": 230,
																						"version": 0,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"length": 80,
																						"readonly": true,
																						"browsable": false,
																						"hidden": true
																					},
																					"stamp": 714
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-5": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 715,
													"*": {
														"Comment": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": "120",
																"label": "_History",
																"version": 0,
																"hideTitleBar": false,
																"iconURL": "AP_Comment.png",
																"TitleFontSize": "M",
																"hideActionButtons": true,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"hideTitle": false,
																"sameHeightAsSiblings": false
															},
															"stamp": 716,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"LastValidatorName__": "LabelLastValidatorName__",
																			"LabelLastValidatorName__": "LastValidatorName__",
																			"LastValidatorUserId__": "LabelLastValidatorUserId__",
																			"LabelLastValidatorUserId__": "LastValidatorUserId__",
																			"HoldingComment__": "LabelHoldingComment__",
																			"LabelHoldingComment__": "HoldingComment__",
																			"LastArchiveEditor__": "LabelLastArchiveEditor__",
																			"LabelLastArchiveEditor__": "LastArchiveEditor__",
																			"LastArchiveEditionDate__": "LabelLastArchiveEditionDate__",
																			"LabelLastArchiveEditionDate__": "LastArchiveEditionDate__"
																		},
																		"version": 0
																	},
																	"stamp": 717,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"HoldingComment__": {
																						"line": 1,
																						"column": 2
																					},
																					"LabelHoldingComment__": {
																						"line": 1,
																						"column": 1
																					},
																					"History__": {
																						"line": 2,
																						"column": 1
																					},
																					"LastValidatorName__": {
																						"line": 3,
																						"column": 2
																					},
																					"LabelLastValidatorName__": {
																						"line": 3,
																						"column": 1
																					},
																					"LastValidatorUserId__": {
																						"line": 4,
																						"column": 2
																					},
																					"LabelLastValidatorUserId__": {
																						"line": 4,
																						"column": 1
																					},
																					"LastArchiveEditor__": {
																						"line": 5,
																						"column": 2
																					},
																					"LabelLastArchiveEditor__": {
																						"line": 5,
																						"column": 1
																					},
																					"LastArchiveEditionDate__": {
																						"line": 6,
																						"column": 2
																					},
																					"LabelLastArchiveEditionDate__": {
																						"line": 6,
																						"column": 1
																					}
																				},
																				"lines": 6,
																				"columns": 2,
																				"colspans": [
																					[],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[],
																					[],
																					[],
																					[]
																				],
																				"version": 0
																			},
																			"stamp": 718,
																			"*": {
																				"LabelHoldingComment__": {
																					"type": "Label",
																					"data": [
																						"HoldingComment__"
																					],
																					"options": {
																						"label": "_HoldingComment",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 719
																				},
																				"HoldingComment__": {
																					"type": "ShortText",
																					"data": [
																						"HoldingComment__"
																					],
																					"options": {
																						"label": "_HoldingComment",
																						"activable": true,
																						"width": 230,
																						"version": 0,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"browsable": false,
																						"hidden": true
																					},
																					"stamp": 720
																				},
																				"History__": {
																					"type": "LongText",
																					"data": [
																						"History__"
																					],
																					"options": {
																						"label": "_History",
																						"activable": true,
																						"width": "100%",
																						"numberOfLines": 50,
																						"readonly": true,
																						"version": 0,
																						"textSize": "S",
																						"resizable": true,
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"minNbLines": 1
																					},
																					"stamp": 721
																				},
																				"LabelLastValidatorName__": {
																					"type": "Label",
																					"data": [
																						"LastValidatorName__"
																					],
																					"options": {
																						"label": "_LastValidatorName",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 722
																				},
																				"LastValidatorName__": {
																					"type": "ShortText",
																					"data": [
																						"LastValidatorName__"
																					],
																					"options": {
																						"label": "_LastValidatorName",
																						"activable": true,
																						"width": 230,
																						"readonly": true,
																						"version": 0,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"browsable": false,
																						"hidden": true
																					},
																					"stamp": 723
																				},
																				"LabelLastValidatorUserId__": {
																					"type": "Label",
																					"data": [
																						"LastValidatorUserId__"
																					],
																					"options": {
																						"label": "_LastValidatorUserId",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 724
																				},
																				"LastValidatorUserId__": {
																					"type": "ShortText",
																					"data": [
																						"LastValidatorUserId__"
																					],
																					"options": {
																						"label": "_LastValidatorUserId",
																						"activable": true,
																						"width": 230,
																						"version": 0,
																						"readonly": true,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"browsable": false,
																						"hidden": true
																					},
																					"stamp": 725
																				},
																				"LabelLastArchiveEditor__": {
																					"type": "Label",
																					"data": [
																						"LastArchiveEditor__"
																					],
																					"options": {
																						"label": "_LastArchiveEditor",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 726
																				},
																				"LastArchiveEditor__": {
																					"type": "ShortText",
																					"data": [
																						"LastArchiveEditor__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"label": "_LastArchiveEditor",
																						"activable": true,
																						"width": 230,
																						"version": 0,
																						"browsable": false,
																						"autocompletable": false,
																						"readonly": true,
																						"hidden": true
																					},
																					"stamp": 727
																				},
																				"LabelLastArchiveEditionDate__": {
																					"type": "Label",
																					"data": [
																						"LastArchiveEditionDate__"
																					],
																					"options": {
																						"label": "_LastArchiveEditionDate",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 728
																				},
																				"LastArchiveEditionDate__": {
																					"type": "RealDateTime",
																					"data": [
																						"LastArchiveEditionDate__"
																					],
																					"options": {
																						"readonly": true,
																						"displayLongFormat": false,
																						"label": "_LastArchiveEditionDate",
																						"activable": true,
																						"width": 230,
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 729
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-24": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 730,
													"*": {
														"Event_history": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": 0,
																"iconURL": "AP_Comment.png",
																"TitleFontSize": "L",
																"hideActionButtons": true,
																"hideTitleBar": false,
																"hideTitle": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "_Events",
																"leftImageURL": "",
																"removeMargins": false,
																"version": 0,
																"hidden": true,
																"sameHeightAsSiblings": false
															},
															"stamp": 731,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {},
																		"version": 0
																	},
																	"stamp": 732,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"ConversationUI__": {
																						"line": 1,
																						"column": 1
																					}
																				},
																				"lines": 1,
																				"columns": 2,
																				"colspans": [
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					]
																				],
																				"version": 0
																			},
																			"stamp": 733,
																			"*": {
																				"ConversationUI__": {
																					"type": "ConversationUI",
																					"data": false,
																					"options": {
																						"label": "_ConversationUI",
																						"activable": true,
																						"width": 981,
																						"tableName": "Conversation__",
																						"version": 0,
																						"conversationOptions": {
																							"ignoreIfExists": false,
																							"notifyByEmail": true
																						}
																					},
																					"stamp": 734
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-6": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 735,
													"*": {
														"Parameters": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": "250",
																"label": "_APParameters",
																"version": 0,
																"iconURL": "AP_Parameters.png",
																"TitleFontSize": "M",
																"hideActionButtons": true,
																"hideTitleBar": true,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"hideTitle": true,
																"leftImageURL": "",
																"removeMargins": false,
																"elementsAlignment": "left",
																"sameHeightAsSiblings": false
															},
															"stamp": 736,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"LabelDuplicateCheckAlertLevel__": "DuplicateCheckAlertLevel__",
																			"DuplicateCheckAlertLevel__": "LabelDuplicateCheckAlertLevel__",
																			"TouchlessEnabled__": "LabelTouchlessEnabled__",
																			"LabelTouchlessEnabled__": "TouchlessEnabled__",
																			"TouchlessDone__": "LabelTouchlessDone__",
																			"LabelTouchlessDone__": "TouchlessDone__",
																			"TouchlessPossible__": "LabelTouchlessPossible__",
																			"LabelTouchlessPossible__": "TouchlessPossible__",
																			"ComputingParametersWaiting__": "LabelComputingParametersWaiting__",
																			"LabelComputingParametersWaiting__": "ComputingParametersWaiting__"
																		},
																		"version": 0
																	},
																	"stamp": 737,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"LabelDuplicateCheckAlertLevel__": {
																						"line": 4,
																						"column": 1
																					},
																					"DuplicateCheckAlertLevel__": {
																						"line": 4,
																						"column": 2
																					},
																					"Duplicates__": {
																						"line": 3,
																						"column": 1
																					},
																					"AutomaticProcessing__": {
																						"line": 5,
																						"column": 1
																					},
																					"TouchlessEnabled__": {
																						"line": 6,
																						"column": 2
																					},
																					"LabelTouchlessEnabled__": {
																						"line": 6,
																						"column": 1
																					},
																					"TouchlessPossible__": {
																						"line": 7,
																						"column": 2
																					},
																					"LabelTouchlessPossible__": {
																						"line": 7,
																						"column": 1
																					},
																					"TouchlessDone__": {
																						"line": 8,
																						"column": 2
																					},
																					"LabelTouchlessDone__": {
																						"line": 8,
																						"column": 1
																					},
																					"ShowParameters__": {
																						"line": 1,
																						"column": 1
																					},
																					"ParametersWarning__": {
																						"line": 2,
																						"column": 1
																					},
																					"ComputingParametersWaiting__": {
																						"line": 9,
																						"column": 2
																					},
																					"LabelComputingParametersWaiting__": {
																						"line": 9,
																						"column": 1
																					}
																				},
																				"lines": 9,
																				"columns": 2,
																				"colspans": [
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[],
																					[],
																					[],
																					[]
																				],
																				"version": 0
																			},
																			"stamp": 738,
																			"*": {
																				"ShowParameters__": {
																					"type": "FormButton",
																					"data": false,
																					"options": {
																						"buttonLabel": "Show Parameters",
																						"label": "Show Parameters",
																						"textPosition": "text-right",
																						"textSize": "MS",
																						"textStyle": "default",
																						"textColor": "default",
																						"nextprocess": {
																							"processName": "AP - Application Settings Wizard",
																							"attachmentsMode": "all",
																							"willBeChild": true,
																							"returnToOriginalUrl": false
																						},
																						"urlImage": "AP_Parameters.png",
																						"style": 4,
																						"action": "none",
																						"url": "",
																						"width": "100%",
																						"version": 0
																					},
																					"stamp": 739
																				},
																				"ParametersWarning__": {
																					"type": "Text",
																					"data": false,
																					"options": {
																						"marge": 0,
																						"textSize": "L",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "color3",
																						"backgroundcolor": "default",
																						"label": "",
																						"version": 0,
																						"iconClass": "",
																						"hidden": true
																					},
																					"stamp": 740
																				},
																				"Duplicates__": {
																					"type": "Title",
																					"data": false,
																					"options": {
																						"marge": 0,
																						"label": "_Duplicates",
																						"version": 0,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"backgroundcolor": "default",
																						"iconClass": "",
																						"hidden": true
																					},
																					"stamp": 741
																				},
																				"DuplicateCheckAlertLevel__": {
																					"type": "ComboBox",
																					"data": [
																						"DuplicateCheckAlertLevel__"
																					],
																					"options": {
																						"possibleValues": {
																							"0": "If at least 1 value is identical",
																							"1": "If at least 2 values are identical",
																							"2": "If at least 3 values are identical",
																							"3": "Do not alert on duplicates"
																						},
																						"keyValueMode": true,
																						"possibleKeys": {
																							"0": "1",
																							"1": "2",
																							"2": "3",
																							"3": "0"
																						},
																						"label": "Alert on duplicates",
																						"activable": true,
																						"width": "300",
																						"version": 1,
																						"readonlyIsText": false,
																						"hidden": true,
																						"maxNbLinesToDisplay": 10
																					},
																					"stamp": 742
																				},
																				"LabelDuplicateCheckAlertLevel__": {
																					"type": "Label",
																					"data": [
																						"DuplicateCheckAlertLevel__"
																					],
																					"options": {
																						"label": "Alert on duplicates",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 743
																				},
																				"AutomaticProcessing__": {
																					"type": "Title",
																					"data": false,
																					"options": {
																						"marge": 0,
																						"label": "_Automatic Processing",
																						"version": 0,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"backgroundcolor": "default",
																						"iconClass": "",
																						"hidden": true
																					},
																					"stamp": 744
																				},
																				"LabelTouchlessEnabled__": {
																					"type": "Label",
																					"data": [
																						"TouchlessEnabled__"
																					],
																					"options": {
																						"label": "_Enable automatic processing for this vendor's PO invoices",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 745
																				},
																				"TouchlessEnabled__": {
																					"type": "CheckBox",
																					"data": [
																						"TouchlessEnabled__"
																					],
																					"options": {
																						"label": "_Enable automatic processing for this vendor's PO invoices",
																						"activable": true,
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 746
																				},
																				"LabelTouchlessPossible__": {
																					"type": "Label",
																					"data": [
																						"TouchlessPossible__"
																					],
																					"options": {
																						"label": "_Automatic processing is possible",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 747
																				},
																				"TouchlessPossible__": {
																					"type": "CheckBox",
																					"data": [
																						"TouchlessPossible__"
																					],
																					"options": {
																						"label": "_Automatic processing is possible",
																						"activable": true,
																						"readonly": true,
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 748
																				},
																				"LabelTouchlessDone__": {
																					"type": "Label",
																					"data": [
																						"TouchlessDone__"
																					],
																					"options": {
																						"label": "_Document automatically processed",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 749
																				},
																				"TouchlessDone__": {
																					"type": "CheckBox",
																					"data": [
																						"TouchlessDone__"
																					],
																					"options": {
																						"label": "_Document automatically processed",
																						"activable": true,
																						"readonly": true,
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 750
																				},
																				"LabelComputingParametersWaiting__": {
																					"type": "Label",
																					"data": false,
																					"options": {
																						"label": "_ComputingParametersWaiting",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 751
																				},
																				"ComputingParametersWaiting__": {
																					"type": "HTML",
																					"data": false,
																					"options": {
																						"label": "_ComputingParametersWaiting",
																						"htmlContent": "<div class=\"WaitingIcon-Small\"></div>",
																						"width": "",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 752
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												}
											},
											"stamp": 753,
											"data": []
										},
										"form-content-right": {
											"type": "FlexibleFormSplitter",
											"options": {
												"version": 1,
												"box": {
													"top": "0%",
													"left": "54%",
													"width": "46%",
													"height": null
												},
												"hidden": false
											},
											"*": {
												"form-content-right-1": {
													"type": "FlexibleFormLine",
													"options": {
														"version": 0
													},
													"*": {
														"PreviewPanel": {
															"type": "PanelPreview",
															"options": {
																"border": {
																	"collapsed": false
																},
																"version": 0,
																"labelLength": 0,
																"label": "_Document Preview",
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": false,
																"hideTitleBar": false,
																"hideTitle": false,
																"sameHeightAsSiblings": false
															},
															"*": {
																"Preview": {
																	"data": [
																		"previewdata"
																	],
																	"type": "Preview",
																	"options": {
																		"version": 0
																	},
																	"stamp": 754
																}
															},
															"stamp": 755,
															"data": []
														}
													},
													"stamp": 756,
													"data": []
												}
											},
											"stamp": 757,
											"data": []
										}
									},
									"stamp": 758,
									"data": []
								}
							},
							"stamp": 759,
							"data": []
						}
					},
					"stamp": 760,
					"data": []
				},
				"form-footer": {
					"type": "FlexibleFormFooter",
					"options": {
						"splitterType": "simple-h",
						"version": 0,
						"maxwidth": ""
					},
					"*": {
						"Request_teaching": {
							"type": "SubmitButton",
							"data": false,
							"options": {
								"label": "_Request Teaching",
								"action": "none",
								"version": 1
							},
							"stamp": 761
						},
						"BackToAP": {
							"type": "SubmitButton",
							"data": false,
							"options": {
								"label": "_Back to AP",
								"action": "approve",
								"version": 1
							},
							"stamp": 762
						},
						"BackToPrevious": {
							"type": "SubmitButton",
							"data": false,
							"options": {
								"label": "_Back to previous",
								"action": "approve",
								"version": 1
							},
							"stamp": 763
						},
						"AddApprover": {
							"type": "SubmitButton",
							"data": false,
							"options": {
								"label": "_Add approver",
								"action": "none",
								"version": 1
							},
							"stamp": 764
						},
						"SetAside": {
							"type": "SubmitButton",
							"data": false,
							"options": {
								"label": "_Set aside",
								"action": "none",
								"version": 1
							},
							"stamp": 765
						},
						"OnHold": {
							"type": "SubmitButton",
							"data": false,
							"options": {
								"label": "_OnHold",
								"action": "none",
								"version": 1
							},
							"stamp": 766
						},
						"Simulate": {
							"type": "SubmitButton",
							"data": false,
							"options": {
								"label": "_Simulate",
								"action": "approve",
								"submit": true,
								"version": 1,
								"nextprocess": {
									"processName": "",
									"attachmentsMode": "all",
									"willBeChild": null
								}
							},
							"stamp": 767
						},
						"Post": {
							"type": "SubmitButton",
							"options": {
								"label": "_Post",
								"action": "approve",
								"submit": true,
								"version": 1,
								"style": 1
							},
							"stamp": 768,
							"data": []
						},
						"UpdatePayment": {
							"type": "SubmitButton",
							"data": false,
							"options": {
								"label": "_UpdatePayment",
								"textPosition": "text-right",
								"textSize": "MS",
								"textColor": "default",
								"nextprocess": {
									"processName": "Vendor invoice payment",
									"attachmentsMode": "all",
									"willBeChild": true
								},
								"style": 1,
								"url": "",
								"action": "openprocess",
								"version": 0
							},
							"stamp": 769
						},
						"Reject": {
							"type": "SubmitButton",
							"data": false,
							"options": {
								"label": "_Reject",
								"action": "approve",
								"version": 1,
								"style": 2
							},
							"stamp": 770
						},
						"Reprocess": {
							"type": "SubmitButton",
							"options": {
								"label": "_Reprocess",
								"action": "reprocess",
								"submit": true,
								"version": 1
							},
							"stamp": 771,
							"data": []
						},
						"Save": {
							"type": "SubmitButton",
							"options": {
								"label": "_Save",
								"action": "save",
								"submit": true,
								"version": 1
							},
							"stamp": 772,
							"data": []
						},
						"Edit_": {
							"type": "SubmitButton",
							"data": false,
							"options": {
								"label": "_Edit",
								"style": 2,
								"action": "unseal",
								"version": 0,
								"disabled": true
							},
							"stamp": 773
						},
						"SaveEditing_": {
							"type": "SubmitButton",
							"data": false,
							"options": {
								"label": "_Save",
								"style": 2,
								"action": "reseal",
								"version": 0,
								"disabled": true
							},
							"stamp": 774
						},
						"ReverseInvoice": {
							"type": "SubmitButton",
							"data": false,
							"options": {
								"label": "_Reverse invoice",
								"action": "none",
								"style": 3,
								"version": 0
							},
							"stamp": 775
						},
						"Close": {
							"type": "SubmitButton",
							"options": {
								"label": "_Close",
								"action": "cancel",
								"submit": true,
								"version": 1
							},
							"stamp": 776,
							"data": []
						}
					},
					"stamp": 777,
					"data": []
				}
			},
			"stamp": 778,
			"data": []
		}
	},
	"stamps": 816,
	"data": []
}