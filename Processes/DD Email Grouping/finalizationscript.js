///#GLOBALS Lib Sys
var DD;
(function (DD) {
    var EmailGrouping;
    (function (EmailGrouping) {
        var Finalization;
        (function (Finalization) {
            // Two cache global variables
            var _doesSMNeedToLoadRecipientLinks = null;
            var _doesPORTALNeedToLoadRecipientLinks = null;
            function Main() {
                var result;
                var allUpdateInSuccess = true;
                var index = 0;
                var emailSentCount = 0;
                var timeoutHelper = new Sys.Helpers.TimeoutHelper(400, 20 /* case RD00005764 was used to compute this value */);
                // To read the input file line by line, we use the CSVReader because it's fully
                // scalable. We consider that it's a single column CSV line without header.
                var csvHelper = Sys.Helpers.CSVReader.CreateInstance(0);
                csvHelper.SplitSeparator = "\x00";
                csvHelper.ReturnSeparator = "\n";
                csvHelper.RemoveDoubleQuotes = false;
                var lineContent = null;
                var documentStack = null;
                do {
                    lineContent = csvHelper.GetNextLine();
                    if (lineContent !== null) {
                        try {
                            documentStack = JSON.parse(lineContent);
                        }
                        catch (e) {
                            documentStack = null;
                        }
                        if (documentStack !== null) {
                            result = SendGroupedEmailAndUpdateDocuments(index, documentStack);
                            allUpdateInSuccess = allUpdateInSuccess && result;
                            if (allUpdateInSuccess) {
                                emailSentCount++;
                            }
                        }
                    }
                    index++;
                    timeoutHelper.NotifyIteration();
                } while (lineContent !== null && lineContent !== "" && documentStack !== null);
                Log.Info(emailSentCount + " email(s) were sent to recipient(s)");
                Data.SetValue("CounterSentEmail__", emailSentCount);
                if (!allUpdateInSuccess) {
                    Log.Error("Some documents couldn't be updated, this process has to be in error");
                    Data.SetValue("State", "200");
                }
            }
            Finalization.Main = Main;
            function SendGroupedEmailAndUpdateDocuments(index, documentStack) {
                var recipientID = documentStack[0].recipientID;
                var transactionKey = index + "-" + recipientID;
                var transactionValue = Transaction.Read(transactionKey);
                // In case of retry, we need to have the previously sent email's MSNEX so
                // that we can update documents. The emails transports are real childs so we
                // want to enter in the transport.process() again but the transaction is
                // here so that we can remember the correct MSNEX.
                // sentEmailMsnEx is empty when the email has been sent in a previous retry
                var sentEmailMsnEx = "";
                if (transactionValue === "") {
                    sentEmailMsnEx = SendGroupedEmail(documentStack);
                    Transaction.Write(transactionKey, sentEmailMsnEx);
                }
                else {
                    //If the updates has already been done, or that if we already have stored the EmailMsnEx, we create fake emails in order to bypass the first one in case of retry.
                    CreateFakeEmail();
                    if (transactionValue === "UPDATES_DONE") {
                        return true;
                    }
                    sentEmailMsnEx = transactionValue;
                    Log.Warn("A transaction for recipient with key \"" + transactionKey + "\" was read with MSNEX=" + sentEmailMsnEx + ", we may be currently in a retry");
                }
                // Should not happen except if redis do not give us back the msnex during a retry
                if (!sentEmailMsnEx) {
                    Log.Warn("We do not have the msnex of the grouped email with transaction key \"" + transactionKey + "\"");
                    return false;
                }
                // If the email was sent, we update the documents with the sent email MSNEX and grouping data
                if (UpdateDocuments(sentEmailMsnEx, documentStack)) {
                    Transaction.Write(transactionKey, "UPDATES_DONE");
                    return true;
                }
                return false;
            }
            function AttachDocumentsFilesToTransport(documents, transport) {
                for (var _i = 0, documents_1 = documents; _i < documents_1.length; _i++) {
                    var document = documents_1[_i];
                    for (var f = 0; f < document.files.length; f++) {
                        var file = document.files[f];
                        var outputName = file.outputName;
                        Sys.Helpers.Attach.AttachFileRef(transport, file.fileName, file.outputFormat, outputName);
                    }
                }
            }
            function AddDocumentUpdateNotifs(documents, email) {
                var senderFormRUIDEXList = [];
                for (var _i = 0, documents_2 = documents; _i < documents_2.length; _i++) {
                    var document = documents_2[_i];
                    senderFormRUIDEXList.push(document.senderFormRUIDEX);
                }
                var groupingEmailFailedNotifForCUSI = {
                    transport: email,
                    ruidExToUpdate: senderFormRUIDEXList,
                    notifyFilter: "(state>100)",
                    notifySubject: "Email Grouping notification",
                    notifyEachTime: true,
                    varsToUpdate: {
                        "State": { type: "long", value: 200 },
                        "CompletionDateTime": { type: "string", value: "NOW()" },
                        "RecomputeParentsState": { type: "string", value: "1" }
                    }
                };
                Lib.DD.Records.AddUpdateNotification(groupingEmailFailedNotifForCUSI);
                var groupingEmailSuccessNotifForCUSI = {
                    transport: email,
                    ruidExToUpdate: senderFormRUIDEXList,
                    notifyFilter: "(state=100)",
                    notifySubject: "Email Grouping notification",
                    notifyEachTime: true,
                    varsToUpdate: {
                        "State": { type: "long", value: 100 },
                        "CompletionDateTime": { type: "string", value: "NOW()" },
                        "RecomputeParentsState": { type: "string", value: "1" }
                    }
                };
                Lib.DD.Records.AddUpdateNotification(groupingEmailSuccessNotifForCUSI);
            }
            function CreateFakeEmail() {
                var email = Process.CreateTransport("mail", true);
                email.Process();
            }
            function SendGroupedEmail(documentStack) {
                var senderObj = GetCachedSenderObject();
                var sender = {
                    company: senderObj.GetValue("Company"),
                    logoPath: senderObj.GetLogoPath(),
                    accountID: senderObj.GetValue("AccountID"),
                    ownerID: documentStack[0].senderOwnerID
                };
                var recipientLogin = sender.accountID + "$" + documentStack[0].recipientID;
                var recipientObj = GetCachedRecipientObject(recipientLogin);
                if (!recipientObj) {
                    // We can enter here if the recipient has been deleted between submission and grouping
                    Log.Error("Unable to find recipient with login \"" + recipientLogin + "\"");
                    return null;
                }
                var recipient = {
                    identifier: documentStack[0].recipientID,
                    emailAddress: Sys.DD.ComputeRedirectEmailAddress("Redirect_recipient_notifications", documentStack[0].recipientEmailAddress),
                    culture: recipientObj.GetValue("Culture"),
                    language: recipientObj.GetValue("Language"),
                    portalUrl: recipientObj.GetPortalURL(true),
                    internalUser: recipientObj,
                    company: recipientObj.GetValue("Company")
                };
                var allRecipientsLinks = null;
                if (ShouldLoadEveryDocumentLinks(documentStack[0].recipientDeliveryMethod)) {
                    allRecipientsLinks = LoadEveryDocumentLinks(sender, recipient, documentStack);
                }
                var settings = documentStack[0].settings;
                var subjectKey = "_New document" + (documentStack.length > 1 ? "s" : "") + " from sender {0}";
                var subjectTranslated = Language.TranslateInto(subjectKey, recipient.language, true, sender.company);
                var senderLogo = "<img src=\"" + sender.logoPath + "\" >";
                var documentLinesHTML = GetDocumentLinesHtml(recipient, allRecipientsLinks, documentStack);
                Log.Info("Creating email groupment of " + documentStack.length + " document(s) for recipient " + recipient.identifier + " to address: " + recipient.emailAddress);
                var email = Process.CreateTransport("mail", true);
                var emailVars = email.GetUninheritedVars();
                emailVars.AddValue_String("OwnerID", sender.ownerID, true);
                emailVars.AddValue_String("EmailAddress", recipient.emailAddress, true);
                emailVars.AddValue_String("Subject", subjectTranslated, true);
                emailVars.AddValue_String("ToName", recipient.identifier, true);
                if (settings.forceEmailValidation) {
                    emailVars.AddValue_String("NeedValidation", "1", true);
                }
                var archiveDuration = documentStack[0].senderFormArchiveDuration;
                if (archiveDuration !== "0") {
                    emailVars.AddValue_String("ArchiveDuration", archiveDuration, true);
                    emailVars.AddValue_String("ArchiveBehavior", "0", true); //Do not archive attach for end messages
                    if (settings.archiveCondition) {
                        emailVars.AddValue_String("ArchiveCondition", settings.archiveCondition, true);
                    }
                }
                if (settings.bounceBackEnabled) {
                    Sys.Helpers.BounceBack.SetBounceBack(email, "ALWAYS", true, false);
                }
                var recipientDeliveryMethod = documentStack[0].recipientDeliveryMethod;
                var introSentenceKey = recipientDeliveryMethod === "SM" ? "_Grouped email intro with attach" : "_Grouped email intro portal";
                var introSentence = Language.TranslateInto(introSentenceKey, recipient.language, false);
                var template = recipient.internalUser.GetTemplateContent({
                    templateName: "DD_Grouped_Notification.htm",
                    replaceTags: true,
                    customTags: {
                        logo: senderLogo,
                        portalUrl: recipient.portalUrl,
                        welcomeInformations: ShouldOneDocumentSendWelcomeEMail(documentStack) ? GetWelcomeEmailLoginInfo(recipient) : "",
                        introSentence: introSentence,
                        documentLines: documentLinesHTML,
                        recipientName: recipient.company
                    }
                });
                Sys.Helpers.Attach.AttachHTML(email, "NewDocuments.htm", template.content);
                if (recipientDeliveryMethod === "SM") {
                    AttachDocumentsFilesToTransport(documentStack, email);
                    AddDocumentUpdateNotifs(documentStack, email);
                }
                var emailsProperties = Lib.DD.Emails.GetEmailsProperties("EmailGrouping");
                var toUser1 = {
                    "AR-transportType": "SM-GROUPED",
                    "AR-transportIsOriginal": recipientDeliveryMethod === "SM"
                };
                emailVars.AddValue_String("ToUser1", JSON.stringify(toUser1), true);
                Lib.DD.Emails.SetSender(email, emailsProperties);
                if (recipientDeliveryMethod === "SM") {
                    Sys.Helpers.TryCallFunction("Lib.DD.Customization.Deliveries.FinalizeGroupedEmailsTransport", email, GetUserExitContextParam(recipientObj), documentStack);
                }
                else {
                    Sys.Helpers.TryCallFunction("Lib.DD.Customization.Deliveries.FinalizeGroupedNotificationsTransport", email, GetUserExitContextParam(recipientObj), documentStack);
                }
                email.Process();
                // Let's return the MSNEX of created process so that we can make the checkpoint
                return email.GetUninheritedVars().GetValue_String("MSNEX", 0);
            }
            function ShouldOneDocumentSendWelcomeEMail(documentStack) {
                for (var _i = 0, documentStack_1 = documentStack; _i < documentStack_1.length; _i++) {
                    var document = documentStack_1[_i];
                    if (document.sendWelcomeEMail === "CONCAT" && document.passwordUrl) {
                        return true;
                    }
                }
                return false;
            }
            function GetDocumentLinesHtml(recipient, allRecipients, documentStack) {
                var res = "";
                var viewLinkStr = Language.TranslateInto("_View", recipient.language);
                for (var i = 0; i < documentStack.length; i++) {
                    var document = documentStack[i];
                    var documentURL = "";
                    if (allRecipients !== null) {
                        // We already loaded every links of the table
                        documentURL = allRecipients[document.senderFormRUIDEX];
                    }
                    if (!allRecipients) {
                        documentURL = recipient.internalUser.GetProcessURL(document.senderFormRUIDEX);
                    }
                    res += "<tr class=\"" + (i % 2 === 0 ? "odd" : "even") + "\">\r\n";
                    res += "\t<td>" + document.type + "</td>\r\n";
                    res += "\t<td>" + document.number + "</td>\r\n";
                    res += "\t<td><a href=\"" + documentURL + "\">" + viewLinkStr + "</a></td>\r\n";
                    res += "</tr>\r\n";
                }
                return res;
            }
            function GetWelcomeEmailLoginInfo(recipient) {
                var parametersObject = {
                    templateName: "DD_Welcome_PasswordUrl.txt",
                    replaceTags: true,
                    customTags: {
                        recipientID: recipient.identifier,
                        passwordUrl: recipient.internalUser.GeneratePasswordURL()
                    }
                };
                return recipient.internalUser.GetTemplateContent(parametersObject).content;
            }
            function UpdateDocuments(emailMsnEx, documentStack) {
                for (var _i = 0, documentStack_2 = documentStack; _i < documentStack_2.length; _i++) {
                    var document = documentStack_2[_i];
                    var updateProcess = Process.GetUpdatableTransport(document.senderFormRUIDEX);
                    var eddRecordVars = updateProcess.GetUninheritedVars();
                    eddRecordVars.AddValue_String("Email_grouping_state__", "GROUPABLE_GROUPED", true);
                    eddRecordVars.AddValue_Date("Email_grouping_datetime__", new Date(), true);
                    eddRecordVars.AddValue_String("Email_groupment_identifier__", emailMsnEx, true);
                    updateProcess.Process();
                }
                return true;
            }
            // Cache to query sender only once
            var _senderObj = null;
            function GetCachedSenderObject() {
                if (!_senderObj) {
                    _senderObj = Users.GetUserAsProcessAdmin(Data.GetValue("OwnerID"));
                }
                return _senderObj;
            }
            // Cache to query recipient only once
            var _recipientHash = {};
            function GetCachedRecipientObject(recipientLogin) {
                if (!(recipientLogin in _recipientHash)) {
                    _recipientHash[recipientLogin] = Users.GetUserAsProcessAdmin(recipientLogin);
                }
                return _recipientHash[recipientLogin];
            }
            // Object made to be given to the user exit to customize generated email
            function GetUserExitContextParam(recipient) {
                return {
                    GetSenderUser: function () {
                        return GetCachedSenderObject();
                    },
                    GetRecipientUser: function () {
                        return recipient;
                    }
                };
            }
            function ShouldLoadEveryDocumentLinks(deliveryMethod) {
                // Read values from cache if already computed
                if (deliveryMethod === "SM" && _doesSMNeedToLoadRecipientLinks !== null) {
                    return _doesSMNeedToLoadRecipientLinks;
                }
                else if (deliveryMethod === "PORTAL" && _doesPORTALNeedToLoadRecipientLinks !== null) {
                    return _doesPORTALNeedToLoadRecipientLinks;
                }
                var groupingKey = null;
                var groupingKeyStr = Variable.GetValueAsString("GroupingKey_" + deliveryMethod);
                try {
                    groupingKey = JSON.parse(groupingKeyStr);
                }
                catch (e) {
                    Log.Error("Unable to parse grouping key \"" + groupingKeyStr + "\", exception was: " + e);
                    return false;
                }
                if (!groupingKey || !Sys.Helpers.IsArray(groupingKey)) {
                    Log.Error("The parsed grouping key \"" + groupingKeyStr + "\" isn't a valid array");
                    return false;
                }
                // We need to load every recipients only if the Recipient_ID__ isn't in the grouping list
                var recipientIDFieldFound = false;
                for (var i = 0; i < groupingKey.length && !recipientIDFieldFound; i++) {
                    if (groupingKey[i].toLowerCase() === "recipient_id__") {
                        recipientIDFieldFound = true;
                    }
                }
                // Put result in cache
                if (deliveryMethod === "SM") {
                    _doesSMNeedToLoadRecipientLinks = !recipientIDFieldFound;
                }
                else {
                    _doesPORTALNeedToLoadRecipientLinks = !recipientIDFieldFound;
                }
                if (!recipientIDFieldFound) {
                    Log.Info("The grouping key for " + deliveryMethod + " doesn't contain the \"Recipient_ID__\" field, so we need to load every recipients in DB");
                    Log.Info("As an information, the grouping key is: " + groupingKeyStr);
                }
                return !recipientIDFieldFound;
            }
            function LoadEveryDocumentLinks(sender, recipient, documentStack) {
                var allRecipientObjects = {};
                var allDocumentLinks = {};
                // We passed the first recipient as a parameter because it's already fully loaded
                allRecipientObjects[recipient.identifier] = recipient.internalUser;
                allDocumentLinks[documentStack[0].senderFormRUIDEX] = recipient.internalUser.GetProcessURL(documentStack[0].senderFormRUIDEX);
                for (var i = 1; i < documentStack.length; i++) {
                    var recipientID = documentStack[i].recipientID;
                    var senderFormRUIDEX = documentStack[i].senderFormRUIDEX;
                    var recipientObj = null;
                    if (!allRecipientObjects[recipientID]) {
                        // The user isn't in cache yet: let's query it
                        var recipientLogin = sender.accountID + "$" + documentStack[i].recipientID;
                        recipientObj = GetCachedRecipientObject(recipientLogin);
                        if (!recipientObj) {
                            // We can enter here if the recipient has been deleted between submission and grouping
                            Log.Error("Unable to find recipient with login \"" + recipientLogin + "\"");
                            // We pass the first link as we can't do better, but it shouldn't happen as it's
                            // a limit case because recipient deletion is rare
                            allDocumentLinks[senderFormRUIDEX] = allDocumentLinks[documentStack[0].senderFormRUIDEX];
                            continue;
                        }
                        // Put the queried recipient in cache an continue
                        allRecipientObjects[recipientID] = recipientObj;
                    }
                    else {
                        // Get the recipient from the cache
                        recipientObj = allRecipientObjects[recipientID];
                    }
                    // We have the recipient object, we can finally get the link
                    allDocumentLinks[senderFormRUIDEX] = recipientObj.GetProcessURL(senderFormRUIDEX);
                }
                return allDocumentLinks;
            }
            Main();
        })(Finalization = EmailGrouping.Finalization || (EmailGrouping.Finalization = {}));
    })(EmailGrouping = DD.EmailGrouping || (DD.EmailGrouping = {}));
})(DD || (DD = {}));
