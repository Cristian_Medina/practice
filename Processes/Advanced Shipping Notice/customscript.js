///#GLOBALS Lib
var currentStatus = Data.GetValue("Status__");
// TODO
// const readOnly = Controls.Status__.GetValue() !== "Draft" || !(User.isVendor || Lib.P2P.IsAdmin());
Log.Time("CustomScript");
function InitLayout() {
    Sys.Helpers.Banner.SetMainTitle("_Advanced Shipping Notice");
    Sys.Helpers.Banner.SetSubTitleAligned(true);
    Sys.Helpers.Banner.SetCentered(false);
    Sys.Helpers.Banner.SetHTMLBanner(Controls.HTMLBanner__);
    Controls.LineItems__.ItemPONumber__.SetReadOnly(true);
    Controls.LineItems__.ItemDescription__.SetReadOnly(true);
    Controls.LineItems__.ItemPOLineQuantity__.SetReadOnly(true);
    var isUOMEnabled = Sys.Parameters.GetInstance("PAC").GetParameter("DisplayUnitOfMeasure");
    Controls.LineItems__.ItemUOM__.Hide(!isUOMEnabled);
    Controls.LineItems__.ItemUOM__.SetReadOnly(true);
    Controls.ASNNumber__.SetRequired(true);
    Controls.LineItems__.SetWidth("100%");
    Controls.LineItems__.SetExtendableColumn("ItemDescription__");
    switch (currentStatus) {
        case "Submitted":
            InitResultStep();
            Lib.Shipping.Client.InitControl(true);
            break;
        case "Draft":
        default:
            InitRequestStep();
            Lib.Shipping.Client.InitControl(false);
            break;
    }
}
function InitRequestStep() {
    Controls.Quit__.Hide(false);
    Controls.Save__.Hide(false);
    Sys.Helpers.Banner.SetSubTitle("_Draft");
}
function InitResultStep() {
    Controls.Quit__.Hide(false);
    Controls.SubmitApprove__.Hide(true);
    Controls.HTML__.Hide(true);
    Sys.Helpers.Banner.SetSubTitle("_Submitted");
}
function CheckAllLinesEmpty() {
    var hasAllEmptyLines = true;
    Sys.Helpers.Data.ForEachTableItem("LineItems__", function (lineItem) {
        if (!Sys.Helpers.IsEmpty(lineItem.GetValue("ItemQuantity__"))) {
            hasAllEmptyLines = false;
        }
    });
    if (hasAllEmptyLines) {
        Popup.Alert("_ASN without items line", true, null, "_ASN without items line title");
    }
    return hasAllEmptyLines;
}
function CheckRequiredFields() {
    if (Sys.Helpers.IsEmpty(Data.GetValue("ASNNumber__"))) {
        Controls.ASNNumber__.SetError("This field is required!");
    }
}
function RegisterHandlers() {
    Controls.SubmitApprove__.OnClick = function () {
        CheckRequiredFields();
        if (Process.ShowFirstError() === null && !CheckAllLinesEmpty()) {
            ProcessInstance.Approve("Submit");
        }
        return false;
    };
    Controls.Quit__.OnClick = function () {
        ProcessInstance.Quit("Quit");
        return false;
    };
    Controls.Save__.OnClick = function () {
        ProcessInstance.Save("Save");
        return false;
    };
}
InitLayout();
RegisterHandlers();
Log.TimeEnd("CustomScript");
