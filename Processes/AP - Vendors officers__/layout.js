{
	"*": {
		"Screen": {
			"type": "FlexibleFormLayout",
			"options": {
				"splitterType": "simple-v",
				"version": 0,
				"style": null
			},
			"*": {
				"form-header": {
					"type": "FlexibleFormHeader",
					"options": {
						"hidePanelsMenu": false,
						"version": 0
					},
					"stamp": 2,
					"data": []
				},
				"form-center": {
					"type": "FlexibleFormCenter",
					"options": {
						"version": 0
					},
					"*": {
						"form-content": {
							"type": "FlexibleFormContent",
							"options": {
								"version": 0
							},
							"*": {
								"form-content-center": {
									"type": "FlexibleFormSplitter",
									"options": {
										"version": 1,
										"box": {
											"top": "0%",
											"left": "0%",
											"width": "100%"
										},
										"hidden": false
									},
									"*": {
										"form-content-center-1": {
											"type": "FlexibleFormLine",
											"data": [],
											"options": {
												"version": 0
											},
											"stamp": 9,
											"*": {
												"DataPanel": {
													"type": "PanelData",
													"options": {
														"border": {},
														"version": 0,
														"labelLength": 0,
														"iconURL": "",
														"TitleFontSize": "S",
														"hideActionButtons": true,
														"hideTitleBar": false,
														"hideTitle": false,
														"backgroundcolor": "default",
														"labelsAlignment": "right",
														"label": "_VendorOfficersPane",
														"leftImageURL": "",
														"removeMargins": false,
														"elementsAlignment": "left"
													},
													"*": {
														"FieldsManager": {
															"type": "FieldsManager",
															"options": {
																"controlsAssociation": {
																	"CompanyCode__": "LabelCompanyCode__",
																	"LabelCompanyCode__": "CompanyCode__",
																	"VendorNumber__": "LabelVendorNumber__",
																	"LabelVendorNumber__": "VendorNumber__",
																	"Role__": "LabelRole__",
																	"LabelRole__": "Role__",
																	"FirstName__": "LabelFirstName__",
																	"LabelFirstName__": "FirstName__",
																	"LastName__": "LabelLastName__",
																	"LabelLastName__": "LastName__",
																	"Email__": "LabelEmail__",
																	"LabelEmail__": "Email__",
																	"TableIndex__": "LabelTableIndex__",
																	"LabelTableIndex__": "TableIndex__"
																},
																"version": 0
															},
															"*": {
																"Grid": {
																	"type": "GridLayout",
																	"options": {
																		"lines": 7,
																		"columns": 2,
																		"version": 0,
																		"ctrlsPos": {
																			"CompanyCode__": {
																				"line": 1,
																				"column": 2
																			},
																			"LabelCompanyCode__": {
																				"line": 1,
																				"column": 1
																			},
																			"VendorNumber__": {
																				"line": 2,
																				"column": 2
																			},
																			"LabelVendorNumber__": {
																				"line": 2,
																				"column": 1
																			},
																			"Role__": {
																				"line": 4,
																				"column": 2
																			},
																			"LabelRole__": {
																				"line": 4,
																				"column": 1
																			},
																			"FirstName__": {
																				"line": 5,
																				"column": 2
																			},
																			"LabelFirstName__": {
																				"line": 5,
																				"column": 1
																			},
																			"LastName__": {
																				"line": 6,
																				"column": 2
																			},
																			"LabelLastName__": {
																				"line": 6,
																				"column": 1
																			},
																			"Email__": {
																				"line": 7,
																				"column": 2
																			},
																			"LabelEmail__": {
																				"line": 7,
																				"column": 1
																			},
																			"TableIndex__": {
																				"line": 3,
																				"column": 2
																			},
																			"LabelTableIndex__": {
																				"line": 3,
																				"column": 1
																			}
																		},
																		"colspans": [
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[]
																		]
																	},
																	"data": [
																		"fields"
																	],
																	"*": {
																		"LabelCompanyCode__": {
																			"type": "Label",
																			"data": [
																				"CompanyCode__"
																			],
																			"options": {
																				"label": "_CompanyCode",
																				"version": 0
																			},
																			"stamp": 14
																		},
																		"CompanyCode__": {
																			"type": "DatabaseComboBox",
																			"data": [
																				"CompanyCode__"
																			],
																			"options": {
																				"version": 1,
																				"label": "_CompanyCode",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"minNbLines": 1,
																				"maxNbLines": 1,
																				"fTSmaxRecords": "20",
																				"browsable": true
																			},
																			"stamp": 15
																		},
																		"LabelVendorNumber__": {
																			"type": "Label",
																			"data": [
																				"VendorNumber__"
																			],
																			"options": {
																				"label": "_VendorNumber",
																				"version": 0
																			},
																			"stamp": 16
																		},
																		"VendorNumber__": {
																			"type": "DatabaseComboBox",
																			"data": [
																				"VendorNumber__"
																			],
																			"options": {
																				"version": 1,
																				"label": "_VendorNumber",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"minNbLines": 1,
																				"maxNbLines": 1,
																				"fTSmaxRecords": "20",
																				"browsable": true
																			},
																			"stamp": 17
																		},
																		"LabelTableIndex__": {
																			"type": "Label",
																			"data": [
																				"TableIndex__"
																			],
																			"options": {
																				"label": "_Table index"
																			},
																			"stamp": 26
																		},
																		"TableIndex__": {
																			"type": "Integer",
																			"data": [
																				"TableIndex__"
																			],
																			"options": {
																				"version": 2,
																				"textSize": "S",
																				"textAlignment": "right",
																				"textStyle": "default",
																				"textColor": "default",
																				"integer": true,
																				"label": "_Table index",
																				"precision_internal": 2,
																				"precision_current": 2,
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"defaultValue": "0",
																				"enablePlusMinus": false,
																				"browsable": false
																			},
																			"stamp": 27
																		},
																		"LabelRole__": {
																			"type": "Label",
																			"data": [
																				"Role__"
																			],
																			"options": {
																				"label": "_Role",
																				"version": 0
																			},
																			"stamp": 18
																		},
																		"Role__": {
																			"type": "ShortText",
																			"data": [
																				"Role__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"version": 1,
																				"label": "_Role",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"browsable": false
																			},
																			"stamp": 19
																		},
																		"LabelFirstName__": {
																			"type": "Label",
																			"data": [
																				"FirstName__"
																			],
																			"options": {
																				"label": "_FirstName",
																				"version": 0
																			},
																			"stamp": 20
																		},
																		"FirstName__": {
																			"type": "ShortText",
																			"data": [
																				"FirstName__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"version": 1,
																				"label": "_FirstName",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"browsable": false
																			},
																			"stamp": 21
																		},
																		"LabelLastName__": {
																			"type": "Label",
																			"data": [
																				"LastName__"
																			],
																			"options": {
																				"label": "_LastName",
																				"version": 0
																			},
																			"stamp": 22
																		},
																		"LastName__": {
																			"type": "ShortText",
																			"data": [
																				"LastName__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"version": 1,
																				"label": "_LastName",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"browsable": false
																			},
																			"stamp": 23
																		},
																		"LabelEmail__": {
																			"type": "Label",
																			"data": [
																				"Email__"
																			],
																			"options": {
																				"label": "_Email",
																				"version": 0
																			},
																			"stamp": 24
																		},
																		"Email__": {
																			"type": "ShortText",
																			"data": [
																				"Email__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"version": 1,
																				"label": "_Email",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"browsable": false
																			},
																			"stamp": 25
																		}
																	},
																	"stamp": 8
																}
															},
															"stamp": 7,
															"data": []
														}
													},
													"stamp": 6,
													"data": []
												}
											}
										}
									},
									"stamp": 5,
									"data": []
								}
							},
							"stamp": 4,
							"data": []
						}
					},
					"stamp": 3,
					"data": []
				},
				"form-footer": {
					"type": "FlexibleFormFooter",
					"options": {
						"splitterType": "simple-h",
						"version": 0
					},
					"*": {
						"Save": {
							"type": "SubmitButton",
							"options": {
								"label": "_Save",
								"action": "savequit",
								"submit": true,
								"version": 0,
								"shortCut": "S"
							},
							"stamp": 11,
							"data": []
						},
						"Close": {
							"type": "SubmitButton",
							"options": {
								"label": "_Close",
								"action": "cancel",
								"submit": true,
								"version": 0,
								"shortCut": "Q"
							},
							"stamp": 12,
							"data": []
						},
						"Delete": {
							"type": "SubmitButton",
							"options": {
								"label": "_Delete",
								"action": "delete",
								"submit": true,
								"version": 0,
								"disabled": true
							},
							"stamp": 13,
							"data": []
						}
					},
					"stamp": 10,
					"data": []
				}
			},
			"stamp": 1,
			"data": []
		}
	},
	"stamps": 27,
	"data": []
}