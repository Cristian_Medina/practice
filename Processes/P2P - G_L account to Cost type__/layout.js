{
	"*": {
		"Screen": {
			"type": "FlexibleFormLayout",
			"options": {
				"splitterType": "simple-v",
				"version": 0,
				"backColor": "text-backgroundcolor-default",
				"style": null
			},
			"*": {
				"form-header": {
					"type": "FlexibleFormHeader",
					"options": {
						"hidePanelsMenu": false,
						"version": 0
					},
					"stamp": 2,
					"data": []
				},
				"form-center": {
					"type": "FlexibleFormCenter",
					"options": {
						"version": 0
					},
					"*": {
						"form-content": {
							"type": "FlexibleFormContent",
							"options": {
								"version": 0
							},
							"*": {
								"form-content-center": {
									"type": "FlexibleFormSplitter",
									"options": {
										"version": 1,
										"box": {
											"top": "0%",
											"left": "0%",
											"width": "100%"
										},
										"hidden": false
									},
									"*": {
										"form-content-center-1": {
											"type": "FlexibleFormLine",
											"data": [],
											"options": {
												"version": 0
											},
											"stamp": 9,
											"*": {
												"DataPanel": {
													"type": "PanelData",
													"options": {
														"border": {},
														"version": 0,
														"labelLength": 0,
														"iconURL": "",
														"TitleFontSize": "S",
														"hideActionButtons": false,
														"hideTitleBar": false,
														"hideTitle": false,
														"backgroundcolor": "default",
														"labelsAlignment": "right",
														"label": "_Document data"
													},
													"*": {
														"FieldsManager": {
															"type": "FieldsManager",
															"options": {
																"controlsAssociation": {
																	"CompanyCode__": "LabelCompanyCode__",
																	"LabelCompanyCode__": "CompanyCode__",
																	"ToGLAccount__": "LabelToGLAccount__",
																	"LabelToGLAccount__": "ToGLAccount__",
																	"FromGLAccount__": "LabelFromGLAccount__",
																	"LabelFromGLAccount__": "FromGLAccount__",
																	"CostType__": "LabelCostType__",
																	"LabelCostType__": "CostType__"
																},
																"version": 0
															},
															"*": {
																"Grid": {
																	"type": "GridLayout",
																	"options": {
																		"lines": 4,
																		"columns": 2,
																		"version": 0,
																		"ctrlsPos": {
																			"CompanyCode__": {
																				"line": 1,
																				"column": 2
																			},
																			"LabelCompanyCode__": {
																				"line": 1,
																				"column": 1
																			},
																			"ToGLAccount__": {
																				"line": 3,
																				"column": 2
																			},
																			"LabelToGLAccount__": {
																				"line": 3,
																				"column": 1
																			},
																			"FromGLAccount__": {
																				"line": 2,
																				"column": 2
																			},
																			"LabelFromGLAccount__": {
																				"line": 2,
																				"column": 1
																			},
																			"CostType__": {
																				"line": 4,
																				"column": 2
																			},
																			"LabelCostType__": {
																				"line": 4,
																				"column": 1
																			}
																		},
																		"colspans": [
																			[],
																			[],
																			[],
																			[]
																		]
																	},
																	"data": [
																		"fields"
																	],
																	"*": {
																		"LabelCompanyCode__": {
																			"type": "Label",
																			"data": [
																				"CompanyCode__"
																			],
																			"options": {
																				"label": "_CompanyCode",
																				"version": 0
																			},
																			"stamp": 20
																		},
																		"CompanyCode__": {
																			"type": "DatabaseComboBox",
																			"data": [
																				"CompanyCode__"
																			],
																			"options": {
																				"version": 1,
																				"autoCompleteSearchMode": "startswith",
																				"searchMode": "matches",
																				"label": "_CompanyCode",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"minNbLines": 1,
																				"maxNbLines": 1,
																				"fTSmaxRecords": "20",
																				"browsable": true,
																				"PreFillFTS": true
																			},
																			"stamp": 21
																		},
																		"LabelFromGLAccount__": {
																			"type": "Label",
																			"data": [
																				"FromGLAccount__"
																			],
																			"options": {
																				"label": "_FromGLAccount",
																				"version": 0
																			},
																			"stamp": 18
																		},
																		"FromGLAccount__": {
																			"type": "DatabaseComboBox",
																			"data": [
																				"FromGLAccount__"
																			],
																			"options": {
																				"version": 1,
																				"autoCompleteSearchMode": "startswith",
																				"searchMode": "matches",
																				"label": "_FromGLAccount",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"minNbLines": 1,
																				"maxNbLines": 1,
																				"fTSmaxRecords": "20",
																				"browsable": true,
																				"PreFillFTS": true
																			},
																			"stamp": 19
																		},
																		"LabelToGLAccount__": {
																			"type": "Label",
																			"data": [
																				"ToGLAccount__"
																			],
																			"options": {
																				"label": "_ToGLAccount",
																				"version": 0
																			},
																			"stamp": 16
																		},
																		"ToGLAccount__": {
																			"type": "DatabaseComboBox",
																			"data": [
																				"ToGLAccount__"
																			],
																			"options": {
																				"version": 1,
																				"autoCompleteSearchMode": "startswith",
																				"searchMode": "matches",
																				"label": "_ToGLAccount",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"minNbLines": 1,
																				"maxNbLines": 1,
																				"fTSmaxRecords": "20",
																				"browsable": true,
																				"PreFillFTS": true
																			},
																			"stamp": 17
																		},
																		"LabelCostType__": {
																			"type": "Label",
																			"data": [
																				"CostType__"
																			],
																			"options": {
																				"label": "_CostType",
																				"version": 0
																			},
																			"stamp": 14
																		},
																		"CostType__": {
																			"type": "ComboBox",
																			"data": [
																				"CostType__"
																			],
																			"options": {
																				"possibleValues": {
																					"0": "",
																					"1": "_OpEx",
																					"2": "_CapEx"
																				},
																				"version": 1,
																				"keyValueMode": true,
																				"possibleKeys": {
																					"0": "",
																					"1": "OpEx",
																					"2": "CapEx"
																				},
																				"label": "_CostType",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"DataSourceOrigin": "Standalone",
																				"DisplayedColumns": ""
																			},
																			"stamp": 15
																		}
																	},
																	"stamp": 8
																}
															},
															"stamp": 7,
															"data": []
														}
													},
													"stamp": 6,
													"data": []
												}
											}
										}
									},
									"stamp": 5,
									"data": []
								}
							},
							"stamp": 4,
							"data": []
						}
					},
					"stamp": 3,
					"data": []
				},
				"form-footer": {
					"type": "FlexibleFormFooter",
					"options": {
						"splitterType": "simple-h",
						"version": 0
					},
					"*": {
						"Save": {
							"type": "SubmitButton",
							"options": {
								"label": "_Save",
								"action": "savequit",
								"submit": true,
								"version": 0,
								"shortCut": "S"
							},
							"stamp": 11,
							"data": []
						},
						"Close": {
							"type": "SubmitButton",
							"options": {
								"label": "_Close",
								"action": "cancel",
								"submit": true,
								"version": 0,
								"shortCut": "Q"
							},
							"stamp": 12,
							"data": []
						},
						"Delete": {
							"type": "SubmitButton",
							"options": {
								"label": "_Delete",
								"action": "delete",
								"submit": true,
								"version": 0,
								"disabled": true
							},
							"stamp": 13,
							"data": []
						}
					},
					"stamp": 10,
					"data": []
				}
			},
			"stamp": 1,
			"data": []
		}
	},
	"stamps": 21,
	"data": []
}