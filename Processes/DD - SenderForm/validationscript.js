///#GLOBALS Lib Sys

var Tools = {

	/*
	Function that will set a value in the field ShortStatus, and eventually into the EDI Status
	The value is an error message (reason it is in a "to validate" status)
	*/
	SetErrorShortStatus: function (errorMessage)
	{
		var translatedErrorMessage = '';
		translatedErrorMessage = Language.Translate(errorMessage);

		Data.SetValue("ShortStatus", translatedErrorMessage);
		Data.SetValue("ShortStatusTranslated", translatedErrorMessage);
		Log.Warn(translatedErrorMessage);

		// If ErrorLabelReason__ field will be displayed, we do not hide it
		if (Variable.GetValueAsString("Display_ShortStatus_As_Error__") !== "1")
		{
			// Display ShortStatus as error in the ErrorLabelReason__ field on validation form
			Variable.SetValueAsString("Display_ShortStatus_As_Error__", "0");
		}
	},

	SetStatusInError: function (errorTranslated)
	{
		// We set any value to avoid CustomDataConn to erase our status
		Data.SetValue("State", 200);
		Data.SetValue("StatusCode", 200);
		Data.SetValue("ShortStatusTranslated", errorTranslated);
		Data.SetValue("ShortStatus", errorTranslated);
		Data.SetValue("LongStatus", errorTranslated);
		Data.SetValue("CompletionDateTime", new Date());
		Process.PreventApproval();
	}
};

function Main()
{
	Sys.Helpers.TryCallFunction("Lib.DD.FeatureSetsValidation.OnValidationScriptBegin");

	if (Data.GetActionName() === "LinkDocument")
	{
		// This action is called by the SUPI when the document is attached.
		// That's why in this case we set up the variable to 1 so that in the finalization
		// script the document is only archived.
		Log.Info("This document has been linked to another");
		Variable.SetValueAsString("MasterDocumentLinked", "1");
	}
	else if (Data.GetActionName() === "RoutingSuccess")
	{
		Log.Info("Routing was marked as sucessfull by subsequent process.");
		Variable.SetValueAsString("RoutingSuccess", "true");		
		UpdateDataFromCreatedProcess(Variable.GetValueAsString("UpdatedDataFromCreatedProcess"));
	}
	else if (Data.GetActionName() === "RoutingFailed")
	{
		Log.Info("Routing was marked as failed by subsequent process.");
		ActionRoutingFailed();
	}
	else if (Data.GetActionName() === "setAside")
	{
		Log.Info("The form was set aside");
		Process.PreventApproval();
		Process.LeaveForm();
	}
	else if (Process.AutoValidatingOnExpiration())
	{
		Log.Info("This process has expired. the validation script is skipped");
	}
	else
	{
		if (Sys.DD.GetParameter("LinkToAnotherDocument"))
		{
			Process.SetAutoValidateOnExpiration(true);
		}

		Validate();

		var configurationName = Sys.DD.GetParameter("ConfigurationName");
		if (!configurationName)
		{
			Tools.SetErrorShortStatus("_Configuration not found");
		}
	}

	Sys.Helpers.TryCallFunction("Lib.DD.FeatureSetsValidation.OnValidationScriptEnd");
}

function ActionRoutingFailed()
{
	var SDARejectParams = null;
	try
	{
		SDARejectParams = JSON.parse(Variable.GetValueAsString("SDARejectParams"));
	}
	catch (exception)
	{
		Log.Info("Invalid JSON SDARejectParams");
		return;
	}

	var commentUserDisplayName = Users.GetUser(SDARejectParams.UserName).GetValue("DisplayName");
	Variable.SetValueAsString("CommentUserDisplayName", commentUserDisplayName);

	Data.SetValue("Document_type__", SDARejectParams.DocType);
	if (SDARejectParams.DocType && SDARejectParams.RelatedAutoLearningConfiguration)
	{
		Sys.DD.ClearConfigurationCache(true);
		Data.SetValue("ConfigurationName__", SDARejectParams.RelatedAutoLearningConfiguration);
		Sys.DD.LoadConfigurationByName(SDARejectParams.RelatedAutoLearningConfiguration, null, null, true);
		var deliveryMethodValue = Sys.DD.GetParameter("DefaultDelivery__");
		Data.SetValue("Delivery_method__", deliveryMethodValue);
		Variable.SetValueAsString("ConfigurationSetUserValidation__", "true");

		// Set email value and archive duration value (those values are set in customscript in other case)
		var defaultDeliveryEmail = Sys.DD.GetParameter("DefaultDeliveryEmail__");
		if (deliveryMethodValue === "SM" && defaultDeliveryEmail)
		{
			Data.SetValue("Recipient_email__", defaultDeliveryEmail);
		}

		if(Sys.DD.GetParameter("ForceValidation__"))
		{
			Process.PreventApproval();
		}
	}
	else
	{
		Data.SetValue("ConfigurationName__", "");
		Data.SetValue("Delivery_method__", "");
		Data.SetValue("ProcessRelatedConfiguration__", "");
		var configurationTemplate = Sys.DD.LoadConfigurationByFilter("(configuration_template__=1)", null, false);
		Sys.DD.UpdateParameters(configurationTemplate, false);
		Variable.SetValueAsString("ConfigurationSetUserValidation__", "");
		Process.PreventApproval();
	}

	Validate();
}

function Validate()
{
	var currentAction = Data.GetActionType();
	//If we are on touchless mode
	if (currentAction === "")
	{
		if (Sys.DD.GetParameter("ForceValidation"))
		{
			Tools.SetErrorShortStatus("_WarningValidation_Option forcing validation has been set");
		}
		if (Variable.GetValueAsString("Validation_requested_for_recipient") === "1")
		{
			Tools.SetErrorShortStatus("_WarningValidation_Validation required before sending to this recipient");
		}
	}
	else
	{
		Data.SetValue("ShortStatus", "");
	}

	if (!Data.GetValue("Subject"))
	{
		Data.SetValue("Subject", Language.Translate("_Subject text", true, Data.GetValue("Document_ID__"), Data.GetValue("Sender_company__")));
	}
	Data.SetValue("ToName", Data.GetValue("Recipient_ID__"));
	Data.SetValue("ToCompany", Data.GetValue("Recipient_name__"));
	Data.SetValue("ArchiveDuration", Sys.DD.ComputeArchiveDuration("", Data.GetValue("Delivery_method__")));
	Data.SetValue("ToCountry", Data.GetValue("Recipient_country__"));

	CheckApprovalPrevention();

	if (Variable.GetValueAsString("GeneratedPdfIndex") !== "" && (currentAction === "approve" || currentAction === "approve_asynchronous"))
	{
		Variable.SetValueAsString("GeneratedPdfIndex", "");
	}

	// Synchronize the ValidattionMessage and Validation_comment__ contents
	// (depending on whether the message has been entered in the form (Validation_comment__) or in a view (ValidationMessage)
	var validationMessage = Data.GetValue("ValidationMessage");
	var validationComment = Data.GetValue("Validation_comment__");
	if (!validationComment && validationMessage)
	{
		Data.SetValue("Validation_comment__", validationMessage);
	}
	else if (validationComment)
	{
		Data.SetValue("ValidationMessage", validationComment);
	}

	Sys.Helpers.TryCallFunction("Lib.DD.Customization.Validation.FinalizeSenderFormValidation");

	var submittedDocType = Sys.DD.GetParameter("SubmittedDocType__");
	var submittedDocTypeIsXMLandPDF = submittedDocType === "customxml_pdf";
	var submittedDocTypeIsXMLonly = submittedDocType === "customxml";

	if (submittedDocTypeIsXMLandPDF || submittedDocTypeIsXMLonly)
	{
		var templatePath = GetTemplatePath();
		if (templatePath && Variable.GetValueAsString("GeneratedPdfIndex") === "")
		{
			GeneratePdf(templatePath);
		}
		else if (!templatePath && submittedDocTypeIsXMLonly)
		{
			Tools.SetStatusInError(Language.Translate("_There is no PDF template defined whereas the input is XML only"));
		}
		else if (!templatePath && !Attach.GetProcessedDocument())
		{
			Tools.SetStatusInError(Language.Translate("_There is no pdf in submitted file"));
		}
	}
}

function CheckApprovalPrevention()
{
	Variable.SetValueAsString("FormHasErrors", "");
	if (!ValidateRecipientID() || !ValidateDeliveryMethod())
	{
		Variable.SetValueAsString("FormHasErrors", "true");
		Process.PreventApproval();
	}
}

function GetTemplatePath()
{
	var templatePath = Sys.Helpers.TryCallFunction("Lib.DD.Customization.Configuration.GetFormattingTemplate", Lib.DD.CreateCallbackParametersUsers(
		{ ownerID: Variable.GetValueAsString("Sender_ownerID__") },
		{ ownerID: Variable.GetValueAsString("Recipient_ownerId__") }));

	if (!templatePath)
	{
		templatePath = Sys.DD.GetParameter("Document_template");
	}

	return templatePath;
}

function GeneratePdf(templatePath)
{
	var reportTemplate = Process.GetResourceFile(templatePath);
	if (!reportTemplate)
	{
		var translationBuffer = Language.Translate("_Template not found");
		var errorTranslated = Language.Translate("_The document could not be formatted: {0}", false, translationBuffer);
		Tools.SetStatusInError(errorTranslated);
		return;
	}

	var xgzFile = Data.GetDataFile();
	var cvtFile = xgzFile.ConvertFile({
		conversionType: "crystal",
		report: reportTemplate
	});
	if (!cvtFile)
	{
		var errorCrystalReport = Language.Translate("_Crystal report error");
		throw Language.Translate("_The document could not be formatted: {0}", false, errorCrystalReport);
	}

	var index = Attach.GetNbAttach();
	var isAttached = Attach.AttachTemporaryFile(cvtFile, {
		name: "generatedPDF",
		attachAsConverted: true
	});

	if (!isAttached)
	{
		var errorAttach = Language.Translate("_Couldn't attach the generated file");
		throw Language.Translate("_The document could not be formatted: {0}", false, errorAttach);
	}

	Attach.SetValue(index, "AttachConvertedFiles", 0);
	Variable.SetValueAsString("GeneratedPdfIndex", index);
	Attach.SetValue(index, "DocumentType", Data.GetValue("Document_Type__"));
	Data.SetValue("AttachmentPreviewIndex", index);
}

// Check that delivery method corresponding recipient address is valid
// Returns: false if invalid
function ValidateDeliveryMethod()
{
	Data.SetError("Recipient_email__", "");
	Data.SetError("Recipient_address__", "");
	Data.SetError("Recipient_ID__", "");
	Data.SetError("Recipient_fax__", "");
	Data.SetError("ProcessRelatedConfiguration__", "");
	Data.SetError("Delivery_method__", "");
	Data.SetError("Recipient_email_Common__", "");
	var deliveryMethod = Data.GetValue("Delivery_method__");
	if (deliveryMethod)
	{
		deliveryMethod = deliveryMethod.toUpperCase();
	}

	if (deliveryMethod === "MOD" && !Data.GetValue("Recipient_address__"))
	{
		Tools.SetErrorShortStatus("_Delivery method address error");
		Data.SetError("Recipient_address__", "_Delivery method address error");
		return false;
	}
	else if (deliveryMethod === "SM" &&
		(!Sys.DD.ComputeRedirectEmailAddress("Redirect_recipient_notifications", Data.GetValue("Recipient_email__")) ||
		(Data.GetValue("Recipient_email__") && Data.GetValue("Recipient_email__").indexOf("@") === -1)))
	{
		Tools.SetErrorShortStatus("_Delivery method email error");
		Data.SetError("Recipient_email__", "_Delivery method email error");
		return false;
	}
	else if (deliveryMethod === "PORTAL" && !Data.GetValue("Recipient_ID__"))
	{
		Tools.SetErrorShortStatus("_Delivery method portal error");
		Data.SetError("Recipient_ID__", "_Delivery method portal error");
		return false;
	}
	else if (deliveryMethod === "FGFAXOUT" && !Data.GetValue("Recipient_fax__"))
	{
		Tools.SetErrorShortStatus("_Delivery method fax error");
		Data.SetError("Recipient_fax__", "_Delivery method fax error");
		return false;
	}
	else if (deliveryMethod === "COP" && !Data.GetValue("ProcessRelatedConfiguration__"))
	{
		Tools.SetErrorShortStatus("_Delivery method cop error");
		Data.SetError("ProcessRelatedConfiguration__", "_Delivery method cop error");
		return false;
	}
	else if(deliveryMethod === "CONVERSATION" && !Data.GetValue("FormRelatedToConversationMsnex__"))
	{
		Tools.SetErrorShortStatus("_Delivery method conversation error");
		Data.SetError("FormRelatedToConversationMsnex__", "_Delivery method conversation error");		
		return false;
	}
	else if (deliveryMethod === "APPLICATION_PROCESS")
	{
		var processSelected = Sys.DD.GetParameter("ProcessSelected__");
		if(!Process.GetProcessID(processSelected))
		{
			Data.SetError("Delivery_method__", "_Routing_Process_is_not_found");
			return false;
		}
	}
	else if (!deliveryMethod || deliveryMethod === "EMPTY")
	{
		Tools.SetErrorShortStatus("_Delivery method unknown error");
		Data.SetError("Delivery_method__", "_Delivery method unknown error");
		return false;
	}
	return true;
}

function ShouldPublishOnPortal()
{
	return Data.GetValue("Delivery_method__") === "PORTAL";
}

function ValidateRecipientID()
{
	var regex = new RegExp("^([[a-zA-Z0-9-'_.@]+)$");
	if (Data.GetValue("Recipient_ID__") && !Data.GetValue("Recipient_ID__").match(regex))
	{
		Tools.SetErrorShortStatus("_Recipient identifier invalid");
		Data.SetError("Recipient_ID__", "_Recipient identifier invalid");
		return false;
	}

	if (ShouldPublishOnPortal() && !Data.GetValue("Recipient_ID__"))
	{
		Tools.SetErrorShortStatus("_ErrorRefreshRecipient_Recipient ID can not be empty");
		Data.SetError("Recipient_ID__", "_ErrorRefreshRecipient_Recipient ID can not be empty");
		return false;
	}

	return true;
}

function UpdateDataFromCreatedProcess(strJson)
{
	if (strJson)
	{
		Log.Info("UpdateFromChildProcess", strJson);
		var json = JSON.parse(strJson);
		for(var key in json)
		{
			Data.SetValue(key, json[key]);
		}
	}
}

Main();
