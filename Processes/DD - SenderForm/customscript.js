///#GLOBALS Lib Sys

// --------------------
// Global variables
// --------------------

var g_errorClassCSS = "input text-size-S text-color-color2 readonly";

// State of panes
var Panes = {
	Header_title_pane: {
		control: Controls.Header_title_pane,
		headerPane: true
	},
	Being_process_pane: {
		control: Controls.Being_process_pane,
		headerPane: true,
		beingProcessPane: true
	},
	Main_pane: {
		control: Controls.Main_pane,
		headerPane: true
	},
	Error_pane: {
		control: Controls.Error_pane,
		headerPane: true,
		errorPane: true
	},
	Toggle_button_top_pane: {
		control: Controls.Toggle_button_top_pane,
		headerPane: true
	},
	Delivery_details_pane: {
		control: Controls.Delivery_details_pane,
		toggleable: true
	},
	Archiving_pane: {
		control: Controls.Archiving_pane,
		toggleable: true
	},
	Validation_pane: {
		control: Controls.Validation_pane,
		toggleable: true
	},
	Recipient_pane: {
		control: Controls.Recipient_pane,
		toggleable: true
	},
	Sender_pane: {
		control: Controls.Sender_pane,
		toggleable: true
	},
	Toggle_button_bottom_pane: {
		control: Controls.Toggle_button_bottom_pane,
		toggleable: true
	},
	Email_grouping_pane: {
		control: Controls.Email_grouping_pane,
		toggleable: true
	},
	History_pane: {
		control: Controls.History_pane,
		toggleable: true
	},
	Notification_pane: {
		control: Controls.Notification_pane
	}
};

var PaneTools = {
	ignoredControls: ["Document_ID__", "Document_type__", "Delivery_method__", "Comment__"],
	toggledPanesDisplayed: null,
	displayHeaderPane: true,
	errorControls: [
		Controls.ErrorLabelReason__,
		Controls.Error_spacer__,
		Controls.WarningLabel__
	],

	nonRecipientControls: [
		Controls.History_pane,
		Controls.Toggle_button_top_pane,
		Controls.Delivery_details_pane,
		Controls.Archiving_pane,
		Controls.Validation_pane,
		Controls.Toggle_button_bottom_pane,
		Controls.Recipient_exist__,
		Controls.Recipient_ID_link__,
		Controls.Delivery_method__,
		Controls.Email_grouping_pane
	],

	Init: function ()
	{
		if (PaneTools.displayHeaderPane)
		{
			for (var pane in Panes)
			{
				if (Panes[pane].headerPane && !Panes[pane].beingProcessPane)
				{
					Panes[pane].control.Hide(false);
				}
			}
		}

		var displayEmailGroupingPane = Data.GetValue("Email_grouping_state__");
		Panes.Email_grouping_pane.toggleable = displayEmailGroupingPane;
	},

	HideAll: function ()
	{
		for (var pane in Panes)
		{
			Panes[pane].control.Hide(true);
		}
	},

	HideErrorControls: function ()
	{
		for (var pane in Panes)
		{
			if (Panes[pane].errorPane)
			{
				Panes[pane].control.Hide(true);
			}
		}

		for (var i = 0; i < PaneTools.errorControls.length; i++)
		{
			PaneTools.errorControls[i].Hide(true);
		}
	},

	SwitchTogglePanes: function ()
	{
		if (PaneTools.toggledPanesDisplayed == null)
		{
			PaneTools.toggledPanesDisplayed = !AreRequiredFieldsValid(PaneTools.ignoredControls);
		}

		for (var pane in Panes)
		{
			if (Panes[pane].toggleable)
			{
				Panes[pane].control.Hide(PaneTools.toggledPanesDisplayed);
			}
		}

		PaneTools.toggledPanesDisplayed = !PaneTools.toggledPanesDisplayed;

		if (PaneTools.toggledPanesDisplayed)
		{
			// Expanded form
			var showLessTranslated = Language.Translate("_Show less details");
			Controls.Button_Show_More_Less__.SetIconURL("show_less.png");
			Controls.Button_Show_More_Less_bottom__.SetIconURL("show_less.png");
			Controls.Button_Show_More_Less__.SetButtonLabel(showLessTranslated);
			Controls.Button_Show_More_Less_bottom__.SetButtonLabel(showLessTranslated);

		}
		else
		{
			// Compacted form
			var showMoreTranslated = Language.Translate("_Show more details");
			Controls.Button_Show_More_Less__.SetIconURL("show_more.png");
			Controls.Button_Show_More_Less_bottom__.SetIconURL("show_more.png");
			Controls.Button_Show_More_Less__.SetButtonLabel(showMoreTranslated);
			Controls.Button_Show_More_Less_bottom__.SetButtonLabel(showMoreTranslated);
		}
	},

	ShowOnlyBeingProcessPane: function ()
	{
		for (var pane in Panes)
		{
			Panes[pane].control.Hide(!Panes[pane].beingProcessPane);
		}

		var newManualSubmission = ProcessInstance.state === null;
		Panes.Being_process_pane.control.Hide(newManualSubmission);
	}
};

var forceRecipientMsnRefresh = true;
var SDARejectParams = null;
var _oldDeliveryMethod = "";
var _deliveryMethodAssociatedFields =
{
	"MOD": [{ "fieldName": "Recipient_address__", "mandatory": true }, { "fieldName": "Recipient_ID__", "mandatory": false }],
	"SM": [{ "fieldName": "Recipient_email__", "mandatory": true }, { "fieldName": "Recipient_ID__", "mandatory": false }],
	"PORTAL": [{ "fieldName": "Recipient_ID__", "mandatory": true }, { "fieldName": "Recipient_email__", "mandatory": false }],
	"FGFAXOUT": [{ "fieldName": "Recipient_fax__", "mandatory": true }, { "fieldName": "Recipient_ID__", "mandatory": false }],
	"OTHER": [{ "fieldName": "Recipient_ID__", "mandatory": false }],
	"NONE": [{ "fieldName": "Recipient_ID__", "mandatory": false }],
	"COP": [{ "fieldName": "ProcessRelatedConfiguration__", "mandatory": true }],
	"APPLICATION_PROCESS": [{ "fieldName": "ProcessRelatedConfiguration__", "mandatory": false }],
	"CONVERSATION" : [{"fieldName" : "FormRelatedToConversationMsnex__", "mandatory": true}]
};

var _outboundDefaultDeliveryMethods = ["EMPTY=", "MOD=_DM Mail on demand", "PORTAL=_DM Portal", "SM=_DM Email", "FGFAXOUT=_DM Fax", "OTHER=_DM Other", "NONE=_DM None"];

var g_configurationTemplateCache = null;
var g_controlsToHideWhenEmpty = [];

// --------------------
// Entry point
// --------------------

function InitFormControls()
{
	PaneTools.Init();

	InitPreviewPane();
	InitPaneDemonstrationMode();
	InitMainPane();
	InitPaneDeliveries();
	InitPaneArchiving();
	InitPaneValidation();
	InitPaneRecipient();
	InitPaneHistory();
	InitButtons();
	InitToggleButtonPanes();
	InitChoosePane();
	UpdateNotificationPaneFromConfiguration();
}

// --------------------
// ReOrder Fields Panel
// --------------------
function ReOrderFields(docTypeConfig)
{
	var fieldsOrder = Lib.DD.DocumentTypeManager.GetDefaultFieldsOrder();

	if (docTypeConfig && docTypeConfig.fieldsOrder && Sys.Helpers.IsArray(docTypeConfig.fieldsOrder))
	{
		fieldsOrder = docTypeConfig.fieldsOrder;
		if (fieldsOrder.indexOf("Enable_touchless__") === -1)
		{
			fieldsOrder.push("Enable_touchless__");
		}
	}

	fieldsOrder = AddMissingReorderedFields(fieldsOrder);

	for (var j = 1; j < fieldsOrder.length; j++)
	{
		if (Controls[fieldsOrder[j]])
		{
			Controls[fieldsOrder[j]].InsertAfter(fieldsOrder[j - 1]);
		}

	}
}

/*Retrieve all custom fields which are not part of the current fields order
and add it at the end of the ordered list*/
function AddMissingReorderedFields(fieldsOrder)
{
	var customFieldsList = Lib.DD_Client.VirtualFieldsManager.Document.GetActiveCustomFieldsNames();
	for (var j = 0; j < customFieldsList.length; j++)
	{
		if (fieldsOrder.indexOf(customFieldsList[j]) === -1)
		{
			fieldsOrder.push(customFieldsList[j]);
		}
	}
	return fieldsOrder;
}

// --------------------
// Preview Panel
// --------------------

function InitPreviewPane()
{
	var documentAttached = Attach.GetNbAttach() > 0;
	var severalDocumentsAttached = Attach.GetNbAttach() > 1;

	if (severalDocumentsAttached || documentAttached)
	{
		Lib.DD_Client.DisplayPreviewPanel(true);
	}
	else
	{
		Lib.DD_Client.DisplayPreviewPanel(false);
	}

	Controls.PreviewPanel.OnPreviewError = OnPreviewError;
	Controls.PreviewPanel.OnPreviewComplete = OnPreviewComplete;
}

function OnPreviewError()
{
	var onlyOneDocumentsAttached = Attach.GetNbAttach() === 1;
	if (onlyOneDocumentsAttached)
	{
		Lib.DD_Client.DisplayPreviewPanel(false);
	}
}

function OnPreviewComplete()
{
	Lib.DD_Client.DisplayPreviewPanel(true);
}

// --------------------
// Two top panes initialization
// --------------------

function InitPaneDemonstrationMode()
{
	var hideDemontrationInfo = Sys.DD.GetParameter("Configuration_type__") !== "DEMO";
	var statement = "_This form is in";
	if (ProcessInstance.state >= 100)
	{
		statement = "_This form has been submitted in";
	}

	var message = Language.Translate(statement) + Language.Translate("_Demonstration mode");
	Controls.Demonstration_Info__.SetLabel(message);

	Controls.Demonstration_Info.Hide(hideDemontrationInfo);
	Controls.Demonstration_Info__.Hide(hideDemontrationInfo);
}

// --------------------
// Management of headers panes display
// --------------------

function InitHeaderPanes(statusLabel)
{
	Controls.ErrorLabelReason__.SetImageURL("warning_red.png", true);
	Controls.WarningLabel__.SetImageURL("warning_yellow.png", true);

	var state = ProcessInstance.state;

	if (state < 50)
	{
		PaneTools.ShowOnlyBeingProcessPane();
	}
	else if (state === 50)
	{
		PaneTools.ShowOnlyBeingProcessPane();
		PaneTools.HideErrorControls();
	}
	else
	{
		var generatedTransportCount = Data.GetValue("ForwardTotal");
		var generatedTransportInError = Data.GetValue("ForwardError");

		Controls.Being_process_pane.Hide(true);
		PaneTools.HideErrorControls();

		var errorOccured = state > 100 && (generatedTransportCount === 0 || generatedTransportInError === 0);
		if (errorOccured)
		{
			var mainLabel = Data.GetValue("StateTranslated");
			var secondLabel = "";

			if (state === 200)
			{
				secondLabel = Variable.GetValueAsString("ErrorSelectionConfiguration") || Data.GetValue("LongStatus");
			}
			else if (state === 400)
			{
				secondLabel = Data.GetValue("Validation_comment__");
			}

			var errorLabel = mainLabel;
			if (secondLabel)
			{
				errorLabel += ": " + secondLabel;
			}
			Controls.ErrorLabelReason__.SetLabel(errorLabel);

			var hideErrorLabel = errorLabel.toUpperCase() === statusLabel.toUpperCase();
			Controls.ErrorLabelReason__.Hide(hideErrorLabel);
			Controls.Error_pane.Hide(hideErrorLabel && !Controls.WarningLabel__.IsVisible());
			Controls.Error_spacer__.Hide(hideErrorLabel + !Controls.WarningLabel__.IsVisible());
		}
	}

	if (state === 70)
	{
		var isFixedLayout = Sys.DD.GetParameter("Routing__") !== "Routing message";
		// Validation required for this document due to the package option "ForceValidation"
		if (Sys.DD.GetParameter("ForceValidation") && isFixedLayout)
		{
			Lib.DD_Client.AddWarningMessageOnSenderForm("DOCUMENT_VALIDATION", 0, Language.Translate("_WarningValidation_Option forcing validation has been set"));
		}

		if (!Sys.DD.GetParameter("ConfigurationName") || Sys.Helpers.TryCallFunction("Lib.DD.Customization.HTMLScripts.HideTouchLessCheckbox", Data.GetValue("Document_Type__")))
		{
			if (!Sys.DD.GetParameter("ConfigurationName") && isFixedLayout)
			{
				Lib.DD_Client.AddWarningMessageOnSenderForm("NO_DOCUMENT_TYPRE", 1, Language.Translate("_No_Document_Type_Selected"));
			}
			Controls.Enable_touchless__.Hide(true);
		}
		else if (!Sys.DD.GetParameter("ForceValidation"))
		{
			Controls.Enable_touchless__.SetReadOnly(true);
		}

		// Validation required for this document according to recipient Advanced Delivery option "Request_validation_for_all_invoices__"
		if (Variable.GetValueAsString("Validation_requested_for_recipient") === "1")
		{
			Lib.DD_Client.AddWarningMessageOnSenderForm("RECIPIENT_VALIDATION", 5, Language.Translate("_WarningValidation_Validation required before sending to this recipient"));
		}

		if (Variable.GetValueAsString("Display_ShortStatus_As_Error__") === "1")
		{
			Lib.DD_Client.AddErrorMessageOnSenderForm("SHORT_STATUS_ERROR", 0, Data.GetValue("ShortStatus"));
		}

		var scriptErrorMessage = ProcessInstance.scriptExceptionMessage;
		var messageToFind = "Error:";
		var index = scriptErrorMessage.indexOf(messageToFind);
		if (index > -1)
		{
			scriptErrorMessage = scriptErrorMessage.substring(index + 6);
			scriptErrorMessage = scriptErrorMessage.substring(0, scriptErrorMessage.indexOf("(line"));
			Variable.SetValueAsString("FormHasErrors", "true");
			Controls.ErrorLabelReason__.SetLabel(scriptErrorMessage);
			Controls.ErrorLabelReason__.Hide(false);
		}
	}

	if (state === 90 && Sys.DD.GetParameter("LinkToAnotherDocument"))
	{
		var warningMessage = Language.Translate("_The current {0} is waiting to be linked to a main document", false, Data.GetValue("Document_Type__"));
		Lib.DD_Client.AddWarningMessageOnSenderForm("DOCUMENT_LINKING", 1, warningMessage);
	}
}

// --------------------
// Main pane initialization
// --------------------

function HideDeliveryMethodRelatedFields()
{
	for (var deliveryMethod in _deliveryMethodAssociatedFields)
	{
		var relatedFields = _deliveryMethodAssociatedFields[deliveryMethod];
		for (var i = 0; i < relatedFields.length; i++)
		{
			Controls[relatedFields[i].fieldName].Hide(true);
		}
	}
}

function RefreshMandatoryFields()
{
	var HideMandatoryFields = function (deliveryMethod, hide)
	{
		if (deliveryMethod && _deliveryMethodAssociatedFields[deliveryMethod])
		{
			var relatedFields = _deliveryMethodAssociatedFields[deliveryMethod];
			for (var i = 0; i < relatedFields.length; i++)
			{
				var field = Controls[relatedFields[i].fieldName];
				field.Hide(hide);
				field.SetRequired(relatedFields[i].mandatory && !hide && !field.IsReadOnly());
			}
		}
	};

	var newMethod = Controls.Delivery_method__.GetValue();

	Controls.Delivery_method__.SetRequired(newMethod === "EMPTY");

	HideMandatoryFields(_oldDeliveryMethod, true);
	HideMandatoryFields(newMethod, false);

	_oldDeliveryMethod = newMethod;
}

function UpdateRecipientEmail(deliveryMethodValue)
{
	var defaultDeliveryEmail = Sys.DD.GetParameter("DefaultDeliveryEmail__");
	if (deliveryMethodValue === "SM" && defaultDeliveryEmail)
	{
		Controls.Recipient_email__.SetValue(defaultDeliveryEmail);
	}
}

function InitMainPane()
{
	var $onDeliveryMethodChanged = function ()
	{
		UpdateArchiveDuration();
		RefreshMandatoryFields();
		var defaultDeliveryMethod = Controls.Delivery_method__.GetValue();
		Lib.DD.AppDeliveries.HandleConfigurationsComboForDelivery(defaultDeliveryMethod, Controls.ProcessRelatedConfiguration__, defaultDeliveryMethod === "APPLICATION_PROCESS");
		UpdateRecipientEmail(defaultDeliveryMethod);
		UpdateRecipientIDVisibility();
		Lib.DD.AppDeliveries.CheckDeliveryMethodError(Controls.Delivery_method__);
	};

	HideDeliveryMethodRelatedFields();
	RefreshMandatoryFields();

	var isVariableLayout = Sys.DD.GetParameter("Routing__") === "Routing message";
	InitHeaderTitle(isVariableLayout);
	InitMainPaneRecipientValues();

	// Put the completion date time into "Date/Time completed" field
	Controls.Delivery_date__.SetText(Data.GetValue("CompletionDateTime"));

	// Init the workflow display on the top right
	var isArchivedOnly = Data.GetValue("Delivery_method__") === "NONE";
	var statusLabel = InitMainPaneHTMLWorkflow(isArchivedOnly, isVariableLayout);

	InitHeaderPanes(statusLabel);

	Lib.DD.DocumentTypeManager.FillDocumentTypeControl(HandleWrongRoutedDocument, false);
	var documentTypePredicted = Variable.GetValueAsString("documentTypePredicted");
	if (documentTypePredicted && !Variable.GetValueAsString("SDADocumentFamily"))
	{
		Lib.DD.DocumentTypeManager.AddNewDocumentType(documentTypePredicted);
	}
	var defaultDeliveryMethod = Controls.Delivery_method__.GetValue();
	Lib.DD.AppDeliveries.HandleConfigurationsComboForDelivery(defaultDeliveryMethod, Controls.ProcessRelatedConfiguration__, defaultDeliveryMethod === "APPLICATION_PROCESS");
	Lib.DD.AppDeliveries.CheckDeliveryMethodError(Controls.Delivery_method__);
	Controls.Delivery_method__.OnChange = $onDeliveryMethodChanged;

	// Auto refresh data on recipient ID typed by the user
	Controls.Recipient_ID_Common__.OnChange = function ()
	{
		RefreshRecipient(null, InitPaneRecipientClearValues);
	};

}

function HandleWrongRoutedDocument()
{
	if (SDARejectParams)
	{
		Lib.DD.DocumentTypeManager.SetDocumentType(SDARejectParams.DocType);
		var commentUser = Variable.GetValueAsString("CommentUserDisplayName");

		var commentMessage;
		if (commentUser)
		{
			commentMessage = Language.Translate("_Wrong_Routing {0}", false, Variable.GetValueAsString("CommentUserDisplayName"));
		}
		else
		{
			commentMessage = Language.Translate("_Wrong_Routing", false);
		}

		if (SDARejectParams.DocType)
		{
			commentMessage += "\n" + Language.Translate("_Wrong_Routing_Comment_DocType {0}", false, SDARejectParams.DocType);
		}

		if (SDARejectParams.Comment)
		{
			commentMessage += "\n" + Language.Translate("_Wrong_Routing_Comment {0}", false, SDARejectParams.Comment);
		}

		Lib.DD_Client.HistoryTable.AddGenericLine("ROUTING_FAILED_COMMENT", new Date(), commentMessage, true);

		Lib.DD_Client.AddWarningMessageOnSenderForm("ROUTING_FAILED_COMMENT", 10, commentMessage);
	}

	if (Controls.Document_type__.GetValue())
	{
		OnDocumentTypeChanged(true);
	}
}

function UpdateControlsWithConfiguration(isFormBeingOpened)
{
	Controls.Comment__.Hide(Sys.DD.GetParameter("Show_comment__") !== "1");
	Controls.Document_ID__.Hide(Sys.DD.GetParameter("Show_document_number__") !== "1");
	if (ProcessInstance.state > 70)
	{
		UpdateRecipientIDVisibility();
		Controls.Recipient_ID_link__.Hide(!Data.GetValue("Recipient_ID__"));
		return;
	}
	UpdateAllowedRoutingMethods();

	var isFormInError = Variable.GetValueAsString("FormHasErrors");
	var hasBeenSaved = Data.GetActionName() === "Action_Save";

	if (!isFormBeingOpened || (isFormBeingOpened && !isFormInError && !hasBeenSaved))
	{
		if (UseDefaultDeliveryMethod())
		{
			SetDeliveryMethod(Sys.DD.GetParameter("DefaultDelivery__"));
		}
		else if (Variable.GetValueAsString("RecipientDeliveryMethod"))
		{
			SetDeliveryMethod(Variable.GetValueAsString("RecipientDeliveryMethod"));
		}
	}

	RefreshMandatoryFields();
	SetTouchlessFromConfiguration();
	UpdateNotificationPaneFromConfiguration();
	UpdateRecipientEmail(Controls.Delivery_method__.GetValue());
	UpdateRecipientIDVisibility();
}

function UpdateRecipientIDVisibility()
{
	Controls.Recipient_ID__.Hide(Data.GetValue("Delivery_Method__") !== "PORTAL" && Sys.DD.GetParameter("Show_recipient_id__") !== "1");
}

function SetTouchlessFromConfiguration()
{
	var touchlessActive = !Sys.DD.GetParameter("ForceValidation");
	Controls.Enable_touchless__.SetValue(touchlessActive);
	Controls.Enable_touchless__.SetReadOnly(touchlessActive);
}

function OnDocumentTypeChanged(isFormBeingOpened)
{
	if(isFormBeingOpened)
	{
		ProcessInstance.SetSilentChange(true);
	}
	var documentTypeSelected = Controls.Document_type__.GetValue();
	var relatedConfiguration = Lib.DD.DocumentTypeManager.GetConfiguration(documentTypeSelected);
	if (relatedConfiguration)
	{
		Controls.ConfigurationName__.SetValue(relatedConfiguration);
		Data.SetExtractionConfiguration(relatedConfiguration);
		Sys.DD.LoadConfigurationByName(relatedConfiguration,function(result){
			ProcessInstance.SetSilentChange(true);
			UpdateControlsWithConfiguration(isFormBeingOpened);
			Sys.Helpers.TryCallFunction("Lib.DD.FeatureSetsHTMLScripts.OnConfigurationLoaded",result);
			ProcessInstance.SetSilentChange(false);
		} , null, true);
		Variable.SetValueAsString("ConfigurationSetUserValidation__", "true");
	}
	else if (documentTypeSelected !== Lib.DD.DocumentTypeManager.AddNewDocumentTypeOption)
	{
		Controls.ConfigurationName__.SetValue("");
		Sys.DD.UpdateParameters(g_configurationTemplateCache, false);
		Variable.SetValueAsString("ConfigurationSetUserValidation__", "");
		if (!documentTypeSelected)
		{
			SetDeliveryMethod("EMPTY");
		}
	}

	var family = Lib.DD.DocumentTypeManager.GetFamilyFromDocumentType(documentTypeSelected);
	Controls.Family__.SetValue(family);
	Controls.form_header.SetProcessDisplayName(family || "Smart Doc");
	if (!Sys.Helpers.TryCallFunction("Lib.DD.Customization.HTMLScripts.HideTouchLessCheckbox", Data.GetValue("Document_Type__")))
	{
		Controls.Enable_touchless__.Hide(false);
		SetTouchlessFromConfiguration();
	}
	else
	{
		Controls.Enable_touchless__.Hide(true);
	}

	Variable.SetValueAsString("SDARejectParams", "");

	var docTypeConfig = Lib.DD.DocumentTypeManager.GetDocumentTypeConfig(Controls.Document_type__.GetValue());
	//By default, we're in a fixed layout for retrocompat with outbound
	var isVariableLayout = false;
	if (docTypeConfig)
	{
		isVariableLayout = docTypeConfig.routing === "Routing message";
		Data.SetValue("Routing__", docTypeConfig.routing);
	}


	Lib.DD_Client.VirtualFieldsManager.Document.Init({
		paneTargetControl: Controls.Main_pane,
		overwriteOutboundInit: isVariableLayout
	});

	ReOrderFields(docTypeConfig);

	ChangeButtonsVisibilityAccordingToConfig(docTypeConfig);

	if(isFormBeingOpened)
	{
		ProcessInstance.SetSilentChange(false);
	}
}

function UpdateAllowedRoutingMethods()
{
	if (Sys.DD.GetParameter("Routing__") !== "Routing message")
	{
		var routingMethodAvailableValuesRoutMessage = _outboundDefaultDeliveryMethods;
		UpdateApplicationProcessIfNeeded(routingMethodAvailableValuesRoutMessage);
		Controls.Delivery_method__.SetAvailableValues(routingMethodAvailableValuesRoutMessage);
		Lib.DD.AppDeliveries.FeedDeliveryCombo(Controls.Delivery_method__, ["COP"]);
		return;
	}
	var routingMethodAvailableValues = [];
	if (Sys.DD.GetParameter("IsMODAllowed__") === "1")
	{
		routingMethodAvailableValues.push("MOD=_DM Mail on demand");
	}
	if (Sys.DD.GetParameter("IsPortalAllowed__") === "1")
	{
		routingMethodAvailableValues.push("PORTAL=_DM Portal");
	}
	if (Sys.DD.GetParameter("IsEmailAllowed__") === "1")
	{
		routingMethodAvailableValues.push("SM=_DM Email");
	}
	if (Sys.DD.GetParameter("IsFaxAllowed__") === "1")
	{
		routingMethodAvailableValues.push("FGFAXOUT=_DM Fax");
	}
	if (Sys.DD.GetParameter("IsOtherAllowed__") === "1")
	{
		routingMethodAvailableValues.push("OTHER=_DM Other");
	}
	if (Sys.DD.GetParameter("IsArchiveAllowed__") === "1")
	{
		routingMethodAvailableValues.push("NONE=_DM None");
	}
	if (Sys.DD.GetParameter("IsCOPAllowed__") === "1" && Lib.DD.AppDeliveries.IsActive("COP"))
	{
		routingMethodAvailableValues.push("COP=_DM COP");
	}
	if (Sys.DD.GetParameter("IsConversationAllowed__") === "1" && Lib.DD.AppDeliveries.IsActive("COP"))
	{
		routingMethodAvailableValues.push("CONVERSATION=_DM Conversation");
	}
	UpdateApplicationProcessIfNeeded(routingMethodAvailableValues);
	Controls.Delivery_method__.SetAvailableValues(routingMethodAvailableValues);
}

//update for either routing message or the other one, previously, no process was allowed when routing == routing message since FT-021311_ability_to_route_a_document_to_a_remittance_for_fixed_layout_SDA_configuration it's possible
function UpdateApplicationProcessIfNeeded(routingMethodAvailableValues)
{
	if (Sys.DD.GetParameter("IsApplicationProcessAllowed__") === "1")
	{
		var processList = ProcessInstance.extendedProperties.accountProcesses;
		var processConfigTableName = Sys.DD.GetParameter("ProcessSelected__");
		var processConfigTableLabel = "";
		if (processList)
		{
			for (var i = 0; i < processList.length; i++)
			{
				if (processList[i].Key === processConfigTableName)
				{
					processConfigTableLabel = processList[i].Label;
				}
			}
		}
		routingMethodAvailableValues.push("APPLICATION_PROCESS=" + (processConfigTableLabel ? processConfigTableLabel : processConfigTableName));

		Lib.DD.AppDeliveries.HandleConfigurationsComboForDelivery(Controls.Delivery_method__.GetValue(), Controls.ProcessRelatedConfiguration__);
	}
}

function UpdateNotificationPaneFromConfiguration()
{
	Controls.Notification_pane.Hide(Sys.DD.GetParameter("EndOfValidityNotification__") === "0" || Sys.DD.GetParameter("EndOfValidityNotification__") === "");
	if (Controls.NotificationRecipient__.GetValue())
	{
		// Get the display name from the OwnerID using the Lib.DD_Client cached method
		Lib.DD_Client.GetCachedUser(
			Controls.NotificationRecipient__.GetValue(),
			function (userObj)
			{
				var userDisplayName = userObj !== null ? userObj.displayName : "";
				Controls.Notification_recipient_name__.SetText(userDisplayName);
			}
		);
	}
	else
	{
		Controls.Notification_recipient_name__.SetText("");
	}
}

function InitMainPaneHTMLWorkflow(isArchivedOnly, isVariableLayout)
{
	// Getting images URLs only once
	var imgGreenCheck = Process.GetImageURL("approval_green.png");
	var imgBlueRefresh = Process.GetImageURL("being_process.png");
	var imgRedCross = Process.GetImageURL("deny_red.png");
	var imgYellowWarning = Process.GetImageURL("warning_yellow.png");

	// Compute state
	var imgLabel = "";
	var mainLabel = "";

	switch (ProcessInstance.state)
	{
		case 70:
			// To validate
			imgLabel = imgYellowWarning;
			mainLabel = Language.Translate("_State_validation_pending");
			break;
		case 100:
			imgLabel = imgGreenCheck;
			mainLabel = isArchivedOnly ?
				Language.Translate("_Archived only") :
				Language.Translate("_Sent");
			break;
		case 400:
			// Rejected
			imgLabel = imgRedCross;
			mainLabel = Language.Translate("_State_400");
			break;
		case 300:
			// Cancelled
			imgLabel = imgRedCross;
			mainLabel = Language.Translate("_State_300");
			break;
		case 200:
			// Failure
			imgLabel = imgRedCross;
			mainLabel = Language.Translate("_State_200");
			break;
		default:
			//Being processed
			imgLabel = imgBlueRefresh;
			mainLabel = Language.Translate("_State_waiting");
			break;
	}

	if (User.isCustomer || isVariableLayout)
	{
		imgLabel = "";
		mainLabel = "";
	}
	RefreshHTMLTemplate(imgLabel, mainLabel);

	return mainLabel;
}

function RefreshHTMLTemplate(imgLabel, mainLabel)
{
	// Replace label with the corresponding elements
	var replacementPattern = new RegExp(/<div class="image" style=(?:"|')background-image: url\((?:"|')(.*)(?:"|')\);?(?:"|')>([^<]*)<\/div>/i);
	var template = Controls.HeaderTitle__.GetHTML();
	var matches = replacementPattern.exec(template);
	if (matches && matches.length === 3)
	{
		template = template.replace(matches[1], imgLabel);
		template = template.replace(matches[2], mainLabel);
		Controls.HeaderTitle__.SetHTML(template);
	}
	else
	{
		Log.Error('Unexpected contents in "HeaderTitle__" : could not perform replacement');
	}
}

function InitHeaderTitle(isVariableLayout)
{
	var headerTitle = Data.GetValue("Document_Type__");

	// Display header title and company name
	var recipientNameToDisplay = Data.GetValue("Recipient_name__");
	if (!recipientNameToDisplay)
	{
		recipientNameToDisplay = Data.GetValue("Recipient_ID__");
	}

	var template = Controls.HeaderTitle__.GetHTML();
	if (isVariableLayout && ProcessInstance.state === 70)
	{
		headerTitle = Language.Translate("_General informations", false);
	}

	template = template.replace("%TITLE%", headerTitle ? headerTitle : "Document");
	if (recipientNameToDisplay)
	{
		template = template.replace("%COMPANY%", recipientNameToDisplay);
	}
	else
	{
		template = template.replace(" - %COMPANY%", "");
	}

	Controls.HeaderTitle__.SetHTML(template);
}

function InitMainPaneRecipientValues()
{
	var recipientId = Data.GetValue("Recipient_ID__");
	Controls.Recipient_ID_link__.Hide(!recipientId);
	if (!recipientId && ProcessInstance.state >= 90)
	{
		return;
	}

	var recipientMsn = Variable.GetValueAsString("Recipient_msn__");
	if (recipientMsn && !forceRecipientMsnRefresh)
	{
		// The recipient exists, we set the url to the recipient page
		Controls.Recipient_ID_link__.SetURL(Sys.DD.Recipient.GetRecipientUrl(recipientMsn, true));
		Controls.Recipient_ID_link__.OnClick = null;
	}
	else
	{
		// The recipient doesn't exist, we display a popup message
		Controls.Recipient_ID_link__.SetURL('');
		Controls.Recipient_ID_link__.OnClick = RecipientDoesNotExistCallback;
	}
}

function RecipientDoesNotExistCallback()
{
	// disable the handlers to make sure we don't make multiple queries in case the user clicks multiple times
	Controls.Recipient_ID_link__.OnClick = null;

	RefreshRecipient(
		function ()
		{
			Controls.Recipient_ID_link__.Click();
		},
		function ()
		{
			var title = Language.Translate("_ErrorClickOnRecipientID_Recipient not found title");
			var message = Language.Translate("_ErrorClickOnRecipientID_Recipient not found");
			Popup.Alert(message, false, null, title);
		}
	);
}

function UpdateArchiveDuration()
{
	if (Data.GetValue("Delivery_method__") && Controls.ConfigurationName__.GetValue())
	{
		Data.SetValue("ArchiveDuration", Sys.DD.ComputeArchiveDuration("", Data.GetValue("Delivery_method__")));
		Lib.DD_Client.SetupArchiveValues();
	}
}

function SetDeliveryMethod(deliveryMethod)
{
	//Check if the default routing method is in the available routing methods for this document type
	var availableDeliveryMethods = Controls.Delivery_method__.GetAvailableValues();
	var routingMethodsList = availableDeliveryMethods.map(function(availableValue)
	{
		return availableValue.split("=")[0];
	});
	if (routingMethodsList.indexOf(deliveryMethod) === -1)
	{
		//Default routing method has not been allowed in the configuration
		Log.Warn("Default delivery method defined in configuration is not in the available routing methods. Did you forget to enable a package ?");
		return;
	}

	//Default routing method has been allowed in the configuration
	Data.SetValue("Delivery_method__", deliveryMethod);
	Lib.DD.AppDeliveries.CheckDeliveryMethodError(Controls.Delivery_method__);
	UpdateArchiveDuration();
	RefreshMandatoryFields();
	Lib.DD.AppDeliveries.HandleConfigurationsComboForDelivery(deliveryMethod, Controls.ProcessRelatedConfiguration__, deliveryMethod === "APPLICATION_PROCESS");
	if (Controls.ProcessRelatedConfiguration__.IsVisible())
	{
		Controls.ProcessRelatedConfiguration__.SetValue(Sys.DD.GetParameter("ProcessRelatedConfiguration__"));
	}
}

function UseDefaultDeliveryMethod()
{
	return Sys.DD.GetParameter("DeliveryMethodToUse__") === "DeliveryMethodToUse_DEFAULT";
}

// --------------------
// Deliveries pane initialization
// --------------------

function InitPaneDeliveries()
{
	// Avoid flicker while waiting query to check transports
	Controls.Generated_transports_pane.Hide(true);

	// Full management of delivery table: querying, enriching, formatting, sorting, displaying.
	Lib.DD_Client.DeliveryTable.Manage();


	var state = Data.GetValue("State");
	var translatedState = Data.GetValue("StateTranslated") ? Data.GetValue("StateTranslated") : "";
	if (state === "200")
	{
		Controls.Delivery_status__.AddStyle(g_errorClassCSS);
	}
	Controls.Delivery_status__.SetReadOnly(true);
	Controls.Delivery_status__.SetText(translatedState);

	var shortStatus = Data.GetValue("ShortStatusTranslated");
	var isInError = ProcessInstance.state === 200;
	if (isInError && shortStatus)
	{
		Controls.Short_Status__.SetText(shortStatus);
		Controls.Short_Status__.Hide(false);
	}
	else
	{
		Controls.Short_Status__.Hide(true);
	}
}

// --------------------
// History pane initialization
// --------------------

function InitPaneHistory()
{
	// Full management of delivery table: computing all lines, sorting, displaying.
	// Needs the reference to the duplicates lib to add the duplicates lines.
	Lib.DD_Client.HistoryTable.Manage();
}

function InitChoosePane()
{
	var isDocument = Attach.GetNbAttach() > 0;
	Controls.ChooseDocument.Hide(isDocument);
	if (!isDocument)
	{
		var html = "<img src=\"";
		html += Process.GetImageURL("arrow_splitting.png");
		html += "\">";
		Controls.Arrow__.SetHTML(html);
	}
}

// --------------------
// Validation pane initialization
// --------------------

function InitPaneValidation()
{
	var needValidation = Data.GetValue("NeedValidation");
	var touchLessDone = Data.GetValue("TouchLessDone") ? Data.GetValue("TouchLessDone") : "0";

	if (needValidation !== "1" || touchLessDone !== "0")
	{
		Controls.Validation_pane.Hide(true);
		Panes.Validation_pane.toggleable = false;
		return;
	}

	var preferredValidationOwnerId = Data.GetValue("PreferredValidationOwnerId");
	var validationStatus = Data.GetValue("ValidationStatus");
	var validationOwnerId = Data.GetValue("ValidationOwnerId");

	if (ProcessInstance.state === 70 || !validationOwnerId)
	{
		// The form is to validate, but there is no validation
		// Or nobody validated and the state is not 70
		Controls.Validation_pane.Hide(true);
		Panes.Validation_pane.toggleable = false;
	}
	else if (validationOwnerId)
	{
		// The form has been validated
		Controls.Validation_user_in_charge__.Hide(false);
		Controls.Validation_user_in_charge_name__.Hide(false);
		Controls.Validation_status__.Hide(false);
		Controls.Validation_user_name__.Hide(false);
		Controls.Validation_comment__.Hide(false);

		// The preferred validator is optional
		if (preferredValidationOwnerId)
		{
			Controls.Validation_user_in_charge__.SetText(preferredValidationOwnerId);
			InitPaneValidationSetControlWithDisplayName(Controls.Validation_user_in_charge_name__, preferredValidationOwnerId);
		}
		else
		{
			Controls.Validation_user_in_charge__.Hide(true);
			Controls.Validation_user_in_charge_name__.Hide(true);
		}

		Controls.Validation_status__.SetText(validationStatus);

		InitPaneValidationSetControlWithDisplayName(Controls.Validation_user_name__, validationOwnerId);

		if (Controls.Validation_comment__.GetText())
		{
			Data.SetValue("Validation_comment__", Controls.Validation_comment__.GetText());
		}
		else
		{
			Controls.Validation_comment__.Hide(true);
		}
	}
}

// Manage to put right DisplayName from given OwnerID in given Text control
function InitPaneValidationSetControlWithDisplayName(control, ownerId)
{
	// Get the display name from the OwnerID using the Lib.DD_Client cached method
	Lib.DD_Client.GetCachedUser(
		ownerId,
		function (userObj)
		{
			var userDisplayName = userObj !== null ? userObj.displayName : "";
			control.SetText(userDisplayName);
		}
	);
}


// --------------------
// Archiving pane initialization
// --------------------

function InitPaneArchiving()
{
	Lib.DD_Client.SetupArchiveValues();
}

// --------------------
// Recipient pane initialization
// --------------------

function RefreshRecipient(recipientFoundCallback, recipientNotFoundCallback)
{
	var recipientIdControl = Controls.Recipient_ID__;
	if (recipientIdControl.GetValue())
	{
		var extractedRecipientId = recipientIdControl.GetValue().toLowerCase();
		var senderAccountID = Variable.GetValueAsString("Sender_accountID__") || User.accountId;

		if (senderAccountID && extractedRecipientId)
		{
			var fields = "MSN|Login|OwnerID|Country|AllowedDeliveryMethods|DisplayName|Company|PreferredDeliveryMethod|EmailAddress|";
			fields += "PhoneNumber|FaxNumber|DeliveryCopyEmail|Language|TimeZoneIdentifier|Culture|LastConnectionDateTime|AlreadySentWelcomeEmail|AlreadySentWelcomeLetter|";
			fields += "CompanyFirstInAddress|Street|City";
			Query.DBQuery(
				function ()
				{
					InitPaneRecipientQueryCallback.call(this, recipientFoundCallback, recipientNotFoundCallback);
				},
				"ODUSER",
				fields,
				"(Login=" + senderAccountID + "$" + extractedRecipientId + ")"
			);
		}
		else
		{
			if (!senderAccountID)
			{
				recipientIdControl.SetError("_ErrorRefreshRecipient_Sender account not found");
			}
			if (!extractedRecipientId)
			{
				recipientIdControl.SetWarning("_ErrorRefreshRecipient_Recipient ID can not be empty");
			}
		}
	}
	else
	{
		recipientNotFoundCallback();
	}
}
function InitPaneRecipient()
{
	// Auto refresh data on recipient ID typed by the user
	Controls.Recipient_ID__.OnChange = function ()
	{
		RefreshRecipient(null, InitPaneRecipientClearValues);
	};
}


function InitPaneRecipientQueryCallback(recipientFoundCallback, recipientNotFoundCallback)
{
	var err = this.GetQueryError();
	if (err)
	{
		Popup.Alert(err);

		// in case of network error the user can retry by clicking again on the link
		Controls.Recipient_ID_link__.OnClick = RecipientDoesNotExistCallback;
		return;
	}

	if (this.GetRecordsCount() === 0)
	{
		if (recipientNotFoundCallback)
		{
			recipientNotFoundCallback();
		}
	}
	else
	{
		Variable.SetValueAsString("Recipient_msn__", this.GetQueryValue("Msn", 0));
		forceRecipientMsnRefresh = false;
		InitPaneRecipientSetValues(this);

		if (recipientFoundCallback)
		{
			recipientFoundCallback(this);
		}
	}
}

function InitPaneRecipientClearValues()
{
	var createRecipientsOnTheFly = Sys.DD.GetParameter("AutoCreateRecipients");
	Variable.SetValueAsString("RecipientDeliveryMethod", "");

	TryClearData("Recipient_country__", "", createRecipientsOnTheFly);
	TryClearData("Recipient_exist__", 0, createRecipientsOnTheFly);
	TryClearData("Recipient_email__", "", createRecipientsOnTheFly);
	TryClearData("Recipient_fax__", "", createRecipientsOnTheFly);
	TryClearData("Recipient_name__", "", createRecipientsOnTheFly);
	TryClearData("Recipient_phone__", "", createRecipientsOnTheFly);
	TryClearData("Recipient_copyemail__", "", createRecipientsOnTheFly);

	TryClearVariable("Recipient_login__", "", createRecipientsOnTheFly);
	TryClearVariable("Recipient_language__", "", createRecipientsOnTheFly);
	TryClearVariable("Recipient_culture__", "", createRecipientsOnTheFly);
	TryClearVariable("Recipient_timezone__", "", createRecipientsOnTheFly);
	TryClearVariable("Recipient_ownerId__", "", createRecipientsOnTheFly);
	TryClearVariable("Recipient_allowedDeliveryMethods__", "", createRecipientsOnTheFly);
	TryClearVariable("Recipient_firstPassword__", "", createRecipientsOnTheFly);
	TryClearVariable("Recipient_needToRefresh__", "0", createRecipientsOnTheFly);
	TryClearVariable("Recipient_sendWelcomeEmail__", "0", createRecipientsOnTheFly);
	TryClearVariable("Recipient_sendWelcomeLetter__", "0", createRecipientsOnTheFly);

	Controls.Recipient_ID_link__.Hide(true);
}

function TryClearData(fieldName, value, createRecipientsOnTheFly)
{
	if (!createRecipientsOnTheFly || !Data.GetValue(fieldName))
	{
		Data.SetValue(fieldName, value);
	}
}

function TryClearVariable(varName, value, createRecipientsOnTheFly)
{
	if (!createRecipientsOnTheFly || !Variable.GetValueAsString(varName))
	{
		Variable.SetValueAsString(varName, value);
	}
}

function SetFieldIfEmpty(fieldName, fieldValue)
{
	if (!Data.GetValue(fieldName))
	{
		Data.SetValue(fieldName, fieldValue);
	}
}

function InitPaneRecipientSetValues(query)
{
	var companyFirstInAddress = query.GetQueryValue("CompanyFirstInAddress", 0);
	var company = query.GetQueryValue("Company", 0);
	var street = query.GetQueryValue("Street", 0);
	var city = query.GetQueryValue("City", 0);
	var country = query.GetQueryValue("Country", 0);

	var $computeAddress = function ()
	{
		var address = "";

		address += companyFirstInAddress ? company + "\n" : "";
		address += street + "\n";
		address += city + "\n";
		address += country + "\n";

		return address;
	};

	Data.SetValue("Recipient_exist__", 1);

	var recipientDeliveryMethod = query.GetQueryValue("PreferredDeliveryMethod", 0) || Sys.DD.GetParameter("DefaultDelivery__");
	Variable.SetValueAsString("RecipientDeliveryMethod", recipientDeliveryMethod);
	if (!UseDefaultDeliveryMethod())
	{
		SetDeliveryMethod(recipientDeliveryMethod);
	}

	SetFieldIfEmpty("Recipient_country__", country);
	SetFieldIfEmpty("Recipient_name__", company);
	SetFieldIfEmpty("Recipient_email__", query.GetQueryValue("EmailAddress", 0));
	SetFieldIfEmpty("Recipient_phone__", query.GetQueryValue("PhoneNumber", 0));
	SetFieldIfEmpty("Recipient_fax__", query.GetQueryValue("FaxNumber", 0));
	SetFieldIfEmpty("Recipient_copyemail__", query.GetQueryValue("DeliveryCopyEmail", 0));
	SetFieldIfEmpty("Recipient_address__", $computeAddress());

	var lastConnectionDateTime = query.GetQueryValue("LastConnectionDateTime", 0);
	var alreadySentWelcomeEmail = query.GetQueryValue("AlreadySentWelcomeEmail", 0);
	var alreadySentWelcomeLetter = query.GetQueryValue("AlreadySentWelcomeLetter", 0);
	var whenSendWelcomeEmail = Sys.DD.GetParameter("WhenSendWelcomeEmail");
	var whenSendWelcomeLetter = Sys.DD.GetParameter("WhenSendWelcomeLetter");

	var shouldSendWelcomeEmail = !lastConnectionDateTime &&
		(whenSendWelcomeEmail === "ONNEVERLOGGED" || (whenSendWelcomeEmail === "ONFIRSTDOCUMENT" && alreadySentWelcomeEmail !== "1"));
	var shouldSendWelcomeLetter = !lastConnectionDateTime &&
		(whenSendWelcomeLetter === "ONNEVERLOGGED" || (whenSendWelcomeLetter === "ONFIRSTDOCUMENT" && alreadySentWelcomeLetter !== "1"));

	var recipientLogin = query.GetQueryValue("Login", 0);

	Variable.SetValueAsString("Recipient_login__", recipientLogin);
	Variable.SetValueAsString("Recipient_language__", query.GetQueryValue("language", 0));
	Variable.SetValueAsString("Recipient_culture__", query.GetQueryValue("Culture", 0));
	Variable.SetValueAsString("Recipient_timezone__", query.GetQueryValue("TimeZoneIndex", 0));
	Variable.SetValueAsString("Recipient_ownerId__", "cn=" + recipientLogin + "," + query.GetQueryValue("OwnerId", 0));
	Variable.SetValueAsString("Recipient_allowedDeliveryMethods__", query.GetQueryValue("AllowedDeliveryMethods", 0));
	Variable.SetValueAsString("Recipient_firstPassword__", "");
	Variable.SetValueAsString("Recipient_needToRefresh__", "1");
	Variable.SetValueAsString("Recipient_sendWelcomeEmail__", shouldSendWelcomeEmail ? "1" : "0");
	Variable.SetValueAsString("Recipient_sendWelcomeLetter__", shouldSendWelcomeLetter ? "1" : "0");

	InitMainPaneRecipientValues();
}

// --------------------
// Bottom buttons initialization
// --------------------

function InitButtons()
{
	ChangeButtonsVisibilityAccordingToConfig();

	//Hide old buttons if present after a upgrade if not hidden by the layout
	if (Controls.Actions_pane && Controls.Actions_pane.IsVisible())
	{
		Controls.Actions_pane.Hide(true);
	}

	Controls.Action_Approve.Hide(ProcessInstance.state !== 70 || User.isCustomer);

	// Update Validation_Comment__ when appoving/rejecting a document
	// The reject action is done in PostRejectCommentUpdateCallback if there is no error
	Controls.Action_Reject.OnClick = RejectCallback;
	Controls.Action_Approve.OnClick = ApproveCallback;
	Controls.Action_SetAside.OnClick = SetAsideService.SetAsideActionCallback;

	Controls.Action_Edit.Hide(true);


	// Hide edit button
	Controls.Action_Edit.Hide(true);
}

function ChangeButtonsVisibilityAccordingToConfig(docTypeConfig)
{
	if (!docTypeConfig)
	{
		docTypeConfig = {
			"DisplaySaveAndQuitButton": Sys.DD.GetParameter("DisplaySaveAndQuitButton__"),
			"DisplaySaveButton": Sys.DD.GetParameter("DisplaySaveButton__"),
			"DisplayRejectButton": Sys.DD.GetParameter("DisplayRejectButton__"),
			"DisplayResubmitButton": Sys.DD.GetParameter("DisplayResubmitButton__"),
			"DisplaySetAsideButton": "0"
		};
	}

	// If we are in the case where there is no configuration
	if (!Controls.Document_type__.GetValue())
	{
		docTypeConfig = {
			"DisplaySaveAndQuitButton": "1",
			"DisplaySaveButton": "1",
			"DisplayRejectButton": "1",
			"DisplayResubmitButton": "0",
			"DisplaySetAsideButton": "0"
		};
	}
	var mustHide = (ProcessInstance.state !== 70);
	if (docTypeConfig.DisplaySaveAndQuitButton === "1")
	{
		Controls.Action_SaveAndQuit.Hide(mustHide);
		Controls.Action_SaveAndQuit.SetDisabled(mustHide);
	}
	else
	{
		Controls.Action_SaveAndQuit.Hide(true);
	}
	if (docTypeConfig.DisplaySaveButton === "1")
	{
		Controls.Action_Save.Hide(mustHide);
		Controls.Action_Save.SetDisabled(mustHide);
	}
	else
	{
		Controls.Action_Save.Hide(true);
	}

	if (docTypeConfig.DisplayResubmitButton === "1")
	{
		// Resubmit visibility
		var configurationType = Sys.DD.GetParameter("Configuration_type__");
		var resubmitAvailable = (configurationType === "DEMO" || configurationType === "TEST") && (ProcessInstance.state === 70 || ProcessInstance.state >= 200);
		Controls.Action_Resubmit.Hide(!resubmitAvailable || User.isCustomer);
	}
	else
	{
		Controls.Action_Resubmit.Hide(true);
	}

	if (docTypeConfig.DisplayRejectButton === "1")
	{
		Controls.Action_Reject.Hide(mustHide || User.isCustomer);
	}
	else
	{
		Controls.Action_Reject.Hide(true);
	}

	var hideSetAsideButton = docTypeConfig.DisplaySetAsideButton !== "1" || mustHide || User.isCustomer;
	var hideSetAsideReason = docTypeConfig.DisplaySetAsideButton !== "1" || User.isCustomer || !Data.GetValue("SetAsideReason__");

	Controls.Action_SetAside.Hide(hideSetAsideButton);
	Controls.SetAsideReason__.Hide(hideSetAsideReason);
	Controls.SetAsideComment__.Hide(hideSetAsideReason);

	if(!hideSetAsideReason)
	{
		var value = Data.GetValue("SetAsideReason__");
		Controls.SetAsideReason__.SetAvailableValues(value + "=" + value);
	}
}

// --------------------
// Toggeled panes management
// --------------------

// Init the two "Show more/less" buttons panes
function InitToggleButtonPanes()
{
	Controls.Button_Show_More_Less__.OnClick = PaneTools.SwitchTogglePanes;
	Controls.Button_Show_More_Less_bottom__.OnClick = PaneTools.SwitchTogglePanes;
}

// --------------------
// Common script functions
// --------------------

function RejectCallback()
{
	ProcessInstance.Reject("reject");
}

function ApproveCallback()
{
	ProcessInstance.Approve("approve");
}

var SetAsideService =
{
	SetAsideActionCallback: function ()
	{
		function OrderKeysByTranslations(keys)
		{
			var orderedKeys = keys
				.map(function(key) { 
					return { 
						"key": key,
						"translation": Language.Translate(key, false)
					};
				})
				.sort(function(objectA, objectB){
					return objectA.translation.localeCompare(objectB.translation);
				})
				.map(function(object) { 
					return object.key;
				});

			return orderedKeys;
		}

		function GetAvailableValues(records)
		{
			if(records.length === 0)
			{
				return ["No reason specified=No reason specified"];
			}

			var setAsideReasons = [];
			for (var j = 0; j < records.length; j++)
			{
				if (records[j].Key__)
				{
					setAsideReasons.push(records[j].Key__);
				}
			}

			var orderedReasonsByTranslations = OrderKeysByTranslations(setAsideReasons);
			return orderedReasonsByTranslations.map(function(key) { return key + "=" + key;});
		}

		function HandleSetAsideReasonsQueryCallback (records)
		{
			var possibleValues = GetAvailableValues(records);
			var currentReason = Controls.SetAsideReason__.GetValue();
			if (!currentReason)
			{
				var firstValue = possibleValues[0];
				if (firstValue && firstValue.indexOf("=") != -1)
				{
					currentReason = firstValue.substring(0, firstValue.indexOf("="));
				}
			}

			Lib.CommonDialog.PopupReason({
				title: "_SetAsideTitle",
				reasonListLabel: "_Select set aside reason",
				possibleValues: possibleValues.join("\n"),
				currentReason: currentReason,
				currentComment: Controls.SetAsideComment__.GetValue(),
				ignoreNone: true,
				onClickOk: function (result)
				{
					Data.SetValue("SetAsideReason__", result.reason);
					Data.SetValue("SetAsideComment__", result.comment ? result.comment : "");
					ProcessInstance.Approve("setAside");
				}
			});
		}

		Sys.Helpers.TryCallFunction("Lib.DD.FeatureSetsHTMLScripts.InitTablePrefix");
		var tablePrefix = Variable.GetValueAsString("TablePrefix");
		if(tablePrefix)
		{
			var tableName = tablePrefix + " - Set aside reasons__";
			Sys.GenericAPI.Query(tableName, null, ["Key__"], HandleSetAsideReasonsQueryCallback, null, "NO_LIMIT");
		}
		else
		{
			Log.Warn("Implement the feature set function 'InitTablePrefix'");
			HandleSetAsideReasonsQueryCallback([]);
		}		
	}
};

function AreRequiredFieldsValid(ignoredControls)
{
	for (var control in Controls)
	{
		if (ignoredControls.indexOf(control) === -1 && typeof Controls[control].GetError === "function" && Controls[control].GetError())
		{
			return false;
		}
	}
	return true;
}

function DeserializeSDARejectParams()
{
	var SDARejectParamsJSON = Variable.GetValueAsString("SDARejectParams");
	if (SDARejectParamsJSON)
	{
		try
		{
			SDARejectParams = JSON.parse(SDARejectParamsJSON);
		}
		catch (exception)
		{
			Log.Info("Invalid JSON SDARejectParams");
		}
	}
}


// --------------------
// Entry point
// --------------------

function Main()
{
	Sys.Helpers.EnableSmartSilentChange();
	ProcessInstance.SetSilentChange(true);
	Sys.Helpers.TryCallFunction("Lib.DD.FeatureSetsHTMLScripts.OnHTMLScriptBegin");

	if (Variable.GetValueAsString("SDADocumentFamily"))
	{
		Controls.form_header.SetProcessDisplayName(Variable.GetValueAsString("SDADocumentFamily"));
		Log.Info("Forced document family (by external variable SDADocumentFamily): " + Variable.GetValueAsString("SDADocumentFamily"));
	}

	if (!Data.GetValue("ExtractionConfiguration"))
	{
		g_configurationTemplateCache = Sys.DD.LoadConfigurationByFilter("(configuration_template__=1)", null, false);
	}

	DeserializeSDARejectParams();

	if (SDARejectParams)
	{
		Controls.ConfigurationName__.SetValue("");
		Variable.SetValueAsString("documentTypePredicted", "");
		Variable.SetValueAsString("TemporaryDocumentType", "");
	}

	Lib.DD.DocumentTypeManager.Init(Controls.Document_type__, Controls.Document_type_autolearn__, Controls.ConfigurationName__.GetValue(), OnDocumentTypeChanged);
	Lib.DD.DocumentTypeManager.CheckRequired();

	InitFormControls();

	// When the document is in success, some controls are hidden if they are empty
	if (ProcessInstance.state === 100)
	{
		g_controlsToHideWhenEmpty.map(
			function (control)
			{
				control.Hide(control.GetValue() === null);
			}
		);
	}

	if (User.isCustomer)
	{
		PaneTools.nonRecipientControls.map(
			function (control)
			{
				control.Hide(true);
			}
		);
	}

	if (Variable.GetValueAsString("RoutingSuccess"))
	{
		Lib.DD_Client.HistoryTable.AddGenericLine("ROUTING_SUCCESS", new Date(), Language.Translate("_Routing_Success_{0}", false, Data.GetValue("Delivery_method__")), true);
	}
	if (Data.GetValue("Email_From__"))
	{
		Controls.Email_From__.Hide(false);
		Controls.Email_Subject__.Hide(false);
		Controls.Spacer_Source_Document__.Hide(false);
	}
	Sys.Helpers.TryCallFunction("Lib.DD.FeatureSetsHTMLScripts.OnHTMLScriptEnd");
	Sys.Helpers.TryCallFunction("Lib.DD.Customization.HTMLScripts.CustomizeSenderForm");
	ProcessInstance.SetSilentChange(false);
}

PaneTools.HideAll();

Sys.DD.Init(Main);

