/** *************** **/
/** CONTROL HELPERS **/
/** *************** **/
var EmailNotificationCtrl = {
	GetSubject: function (count)
	{
		if (count > 1)
		{
			return {
				key: "You have {0} invoices pending approval",
				parameters: [count]
			};
		}
		return {
			key: "You have an invoice pending approval"
		};
	},
	GetCustomTags: function (count, approver)
	{
		var approverVars = approver.GetVars();
		var baseUrl = Data.GetValue("ValidationUrl");
		baseUrl = baseUrl.substr(0, baseUrl.lastIndexOf("/"));
		var customTags = {
			ApproverDisplayName: approverVars.GetValue_String("DisplayName", 0),
			NumberOfInvoice: count,
			ValidationUrl: baseUrl + "/View.link?tabName=_My%20documents-AP_SAP&viewName=_AP_View%20-%20Assigned%20to%20me"
		};
		if (count > 1)
		{
			customTags.PlurialInvoices = true;
		}
		return customTags;
	},
	NotifyApprover: function (login, count)
	{
		var approver = Users.GetUser(login);
		if (approver)
		{
			var approverLanguage = approver.GetVars().GetValue_String("Language", 0);
			var email = Sys.EmailNotification.CreateEmailWithUser(
			{
				user: approver,
				subject: this.GetSubject(count),
				template: "AP-ReminderForPaymentApproval.htm",
				customTags: this.GetCustomTags(count, approver),
				escapeCustomTags: true,
				backupUserAsCC: true,
				sendToAllMembersIfGroup: Sys.Parameters.GetInstance("P2P").GetParameter("SendNotificationsToEachGroupMembers") === "1"
			});
			if (email)
			{
				Sys.EmailNotification.AddSender(email, "notification@eskerondemand.com", Language.TranslateInto("Esker Accounts payable", approverLanguage, false));
				Sys.EmailNotification.SendEmail(email);
			}
		}
		else
		{
			Log.Warn("approver with login '" + login + "' not found");
		}
	}
};
var ReadFromCSV = {
	hasHeader: true,
	/**
	 * Extract info from csv by block
	 */
	getUsers: function ()
	{
		var users = {};
		var reader = Sys.Helpers.Attach.getReader(0);
		var line;
		// Skip first line if CSV has header
		if (ReadFromCSV.hasHeader)
		{
			reader.getLine();
		}
		line = reader.getLine();
		while (line !== null)
		{
			ReadFromCSV.extractLoginFromLine(line, users);
			line = reader.getLine();
		}
		return users;
	},
	/**
	 * extract login on line
	 */
	extractLoginFromLine: function (line, users)
	{
		// remove " from line
		line = line.replace(/"/g, "");
		ReadFromCSV.updateLogin(users, line);
	},
	/**
	 * update count for a specific login
	 */
	updateLogin: function (users, login)
	{
		if (Object.prototype.hasOwnProperty.call(users, login))
		{
			users[login]++;
		}
		else
		{
			users[login] = 1;
		}
	}
};
/**
 * sent an email to all users specified as parameter
 */
function notifyUsers(users)
{
	for (var login in users)
	{
		if (Object.prototype.hasOwnProperty.call(users, login))
		{
			EmailNotificationCtrl.NotifyApprover(login, users[login]);
		}
	}
}
var usersToNotify = ReadFromCSV.getUsers();
notifyUsers(usersToNotify);