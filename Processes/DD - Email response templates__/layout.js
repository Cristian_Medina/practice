{
	"*": {
		"Screen": {
			"type": "FlexibleFormLayout",
			"options": {
				"splitterType": "simple-v",
				"version": 0,
				"backColor": "text-backgroundcolor-default",
				"style": null
			},
			"*": {
				"form-header": {
					"type": "FlexibleFormHeader",
					"options": {
						"hidePanelsMenu": false,
						"version": 0
					},
					"stamp": 2,
					"data": []
				},
				"form-center": {
					"type": "FlexibleFormCenter",
					"options": {
						"version": 0
					},
					"*": {
						"form-content": {
							"type": "FlexibleFormContent",
							"options": {
								"version": 0
							},
							"*": {
								"form-content-center": {
									"type": "FlexibleFormSplitter",
									"options": {
										"version": 1,
										"box": {
											"top": "0%",
											"left": "0%",
											"width": "100%"
										},
										"hidden": false,
										"hideSeparator": true
									},
									"*": {
										"form-content-center-1": {
											"type": "FlexibleFormLine",
											"data": [],
											"options": {},
											"stamp": 9,
											"*": {
												"DataPanel": {
													"type": "PanelData",
													"options": {
														"border": {},
														"version": 0,
														"labelLength": 0,
														"iconURL": "",
														"TitleFontSize": "S",
														"hideActionButtons": false,
														"hideTitleBar": false,
														"hideTitle": false,
														"sameHeightAsSiblings": false,
														"backgroundcolor": "default",
														"labelsAlignment": "right",
														"label": "_Document data"
													},
													"*": {
														"FieldsManager": {
															"type": "FieldsManager",
															"options": {
																"controlsAssociation": {
																	"Name__": "LabelName__",
																	"LabelName__": "Name__",
																	"Family__": "LabelFamily__",
																	"LabelFamily__": "Family__",
																	"Template__": "LabelTemplate__",
																	"LabelTemplate__": "Template__"
																},
																"version": 0
															},
															"*": {
																"Grid": {
																	"type": "GridLayout",
																	"options": {
																		"lines": 3,
																		"columns": 2,
																		"version": 0,
																		"ctrlsPos": {
																			"Name__": {
																				"line": 1,
																				"column": 2
																			},
																			"LabelName__": {
																				"line": 1,
																				"column": 1
																			},
																			"Family__": {
																				"line": 2,
																				"column": 2
																			},
																			"LabelFamily__": {
																				"line": 2,
																				"column": 1
																			},
																			"Template__": {
																				"line": 3,
																				"column": 2
																			},
																			"LabelTemplate__": {
																				"line": 3,
																				"column": 1
																			}
																		},
																		"colspans": [
																			[],
																			[],
																			[]
																		]
																	},
																	"data": [
																		"fields"
																	],
																	"*": {
																		"LabelName__": {
																			"type": "Label",
																			"data": [
																				"Name__"
																			],
																			"options": {
																				"label": "Name__"
																			},
																			"stamp": 14
																		},
																		"Name__": {
																			"type": "ShortText",
																			"data": [
																				"Name__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"version": 1,
																				"label": "Name__",
																				"activable": true,
																				"width": "500",
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"helpIconPosition": "Right",
																				"browsable": false,
																				"autocompletable": false
																			},
																			"stamp": 15
																		},
																		"LabelFamily__": {
																			"type": "Label",
																			"data": [
																				"Family__"
																			],
																			"options": {
																				"label": "Family__"
																			},
																			"stamp": 16
																		},
																		"Family__": {
																			"type": "ShortText",
																			"data": [
																				"Family__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"version": 1,
																				"label": "Family__",
																				"activable": true,
																				"width": "500",
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"helpIconPosition": "Right",
																				"browsable": false,
																				"autocompletable": false
																			},
																			"stamp": 17
																		},
																		"LabelTemplate__": {
																			"type": "Label",
																			"data": [
																				"Template__"
																			],
																			"options": {
																				"label": "Template__"
																			},
																			"stamp": 18
																		},
																		"Template__": {
																			"type": "LongText",
																			"data": [
																				"Template__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"version": 1,
																				"minNbLines": 20,
																				"label": "Template__",
																				"activable": true,
																				"width": "1000",
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"helpIconPosition": "Right",
																				"numberOfLines": 40,
																				"browsable": false
																			},
																			"stamp": 19
																		}
																	},
																	"stamp": 8
																}
															},
															"stamp": 7,
															"data": []
														}
													},
													"stamp": 6,
													"data": []
												}
											}
										}
									},
									"stamp": 5,
									"data": []
								}
							},
							"stamp": 4,
							"data": []
						}
					},
					"stamp": 3,
					"data": []
				},
				"form-footer": {
					"type": "FlexibleFormFooter",
					"options": {
						"splitterType": "simple-h",
						"version": 0
					},
					"*": {
						"Save": {
							"type": "SubmitButton",
							"options": {
								"label": "_Save",
								"action": "savequit",
								"submit": true,
								"version": 0,
								"shortCut": "S"
							},
							"stamp": 11,
							"data": []
						},
						"Close": {
							"type": "SubmitButton",
							"options": {
								"label": "_Close",
								"action": "cancel",
								"submit": true,
								"version": 0,
								"shortCut": "Q"
							},
							"stamp": 12,
							"data": []
						},
						"Delete": {
							"type": "SubmitButton",
							"options": {
								"label": "_Delete",
								"action": "delete",
								"submit": true,
								"version": 0,
								"disabled": true
							},
							"stamp": 13,
							"data": []
						}
					},
					"stamp": 10,
					"data": []
				}
			},
			"stamp": 1,
			"data": []
		}
	},
	"stamps": 19,
	"data": []
}