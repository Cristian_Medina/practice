{
	"*": {
		"Screen": {
			"type": "FlexibleFormLayout",
			"options": {
				"splitterType": "simple-v",
				"version": 0
			},
			"*": {
				"form-header": {
					"type": "FlexibleFormHeader",
					"options": {
						"hidePanelsMenu": false,
						"version": 0
					},
					"stamp": 2,
					"data": []
				},
				"form-center": {
					"type": "FlexibleFormCenter",
					"options": {
						"version": 0
					},
					"*": {
						"form-content": {
							"type": "FlexibleFormContent",
							"options": {
								"version": 0
							},
							"*": {
								"form-content-center": {
									"type": "FlexibleFormSplitter",
									"options": {
										"version": 1,
										"box": {
											"top": "0%",
											"left": "0%",
											"width": "100%"
										},
										"hidden": false
									},
									"*": {
										"form-content-center-1": {
											"type": "FlexibleFormLine",
											"data": [],
											"options": {
												"version": 0
											},
											"stamp": 9,
											"*": {
												"DataPanel": {
													"type": "PanelData",
													"options": {
														"border": {},
														"version": 0,
														"labelLength": 0,
														"iconURL": "",
														"TitleFontSize": "S",
														"hideActionButtons": false,
														"hideTitleBar": false,
														"hideTitle": false,
														"backgroundcolor": "default",
														"labelsAlignment": "right",
														"label": "_Document data"
													},
													"*": {
														"FieldsManager": {
															"type": "FieldsManager",
															"options": {
																"controlsAssociation": {
																	"WHTType__": "LabelWHTType__",
																	"LabelWHTType__": "WHTType__",
																	"WHTCode__": "LabelWHTCode__",
																	"LabelWHTCode__": "WHTCode__",
																	"WHTDescription__": "LabelWHTDescription__",
																	"LabelWHTDescription__": "WHTDescription__",
																	"CompanyCode__": "LabelCompanyCode__",
																	"LabelCompanyCode__": "CompanyCode__",
																	"VendorNumber__": "LabelVendorNumber__",
																	"LabelVendorNumber__": "VendorNumber__"
																},
																"version": 0
															},
															"*": {
																"Grid": {
																	"type": "GridLayout",
																	"options": {
																		"lines": 5,
																		"columns": 2,
																		"version": 0,
																		"ctrlsPos": {
																			"WHTType__": {
																				"line": 3,
																				"column": 2
																			},
																			"LabelWHTType__": {
																				"line": 3,
																				"column": 1
																			},
																			"WHTCode__": {
																				"line": 4,
																				"column": 2
																			},
																			"LabelWHTCode__": {
																				"line": 4,
																				"column": 1
																			},
																			"WHTDescription__": {
																				"line": 5,
																				"column": 2
																			},
																			"LabelWHTDescription__": {
																				"line": 5,
																				"column": 1
																			},
																			"CompanyCode__": {
																				"line": 1,
																				"column": 2
																			},
																			"LabelCompanyCode__": {
																				"line": 1,
																				"column": 1
																			},
																			"VendorNumber__": {
																				"line": 2,
																				"column": 2
																			},
																			"LabelVendorNumber__": {
																				"line": 2,
																				"column": 1
																			}
																		},
																		"colspans": [
																			[],
																			[],
																			[],
																			[],
																			[]
																		]
																	},
																	"data": [
																		"fields"
																	],
																	"*": {
																		"LabelCompanyCode__": {
																			"type": "Label",
																			"data": [
																				"CompanyCode__"
																			],
																			"options": {
																				"label": "_Company Code",
																				"version": 0
																			},
																			"stamp": 26
																		},
																		"CompanyCode__": {
																			"type": "DatabaseComboBox",
																			"data": [
																				"CompanyCode__"
																			],
																			"options": {
																				"version": 1,
																				"label": "_Company Code",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"fTSmaxRecords": "20",
																				"browsable": true,
																				"PreFillFTS": true
																			},
																			"stamp": 27
																		},
																		"LabelVendorNumber__": {
																			"type": "Label",
																			"data": [
																				"VendorNumber__"
																			],
																			"options": {
																				"label": "_Vendor Number",
																				"version": 0
																			},
																			"stamp": 28
																		},
																		"VendorNumber__": {
																			"type": "ShortText",
																			"data": [
																				"VendorNumber__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "_Vendor Number",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"browsable": false,
																				"version": 0
																			},
																			"stamp": 29
																		},
																		"LabelWHTType__": {
																			"type": "Label",
																			"data": [
																				"WHTType__"
																			],
																			"options": {
																				"label": "_WHT Type",
																				"version": 0
																			},
																			"stamp": 14
																		},
																		"WHTType__": {
																			"type": "ShortText",
																			"data": [
																				"WHTType__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "_WHT Type",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"browsable": false,
																				"version": 0
																			},
																			"stamp": 15
																		},
																		"LabelWHTCode__": {
																			"type": "Label",
																			"data": [
																				"WHTCode__"
																			],
																			"options": {
																				"label": "_WHT Code",
																				"version": 0
																			},
																			"stamp": 16
																		},
																		"WHTCode__": {
																			"type": "ShortText",
																			"data": [
																				"WHTCode__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "_WHT Code",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"browsable": false,
																				"version": 0
																			},
																			"stamp": 17
																		},
																		"LabelWHTDescription__": {
																			"type": "Label",
																			"data": [
																				"WHTDescription__"
																			],
																			"options": {
																				"label": "_WHT Description",
																				"version": 0
																			},
																			"stamp": 18
																		},
																		"WHTDescription__": {
																			"type": "ShortText",
																			"data": [
																				"WHTDescription__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "_WHT Description",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"browsable": false,
																				"version": 0
																			},
																			"stamp": 19
																		}
																	},
																	"stamp": 8
																}
															},
															"stamp": 7,
															"data": []
														}
													},
													"stamp": 6,
													"data": []
												}
											}
										}
									},
									"stamp": 5,
									"data": []
								}
							},
							"stamp": 4,
							"data": []
						}
					},
					"stamp": 3,
					"data": []
				},
				"form-footer": {
					"type": "FlexibleFormFooter",
					"options": {
						"splitterType": "simple-h",
						"version": 0
					},
					"*": {
						"Save": {
							"type": "SubmitButton",
							"options": {
								"label": "_Save",
								"action": "savequit",
								"submit": true,
								"version": 0,
								"shortCut": "S"
							},
							"stamp": 11,
							"data": []
						},
						"Close": {
							"type": "SubmitButton",
							"options": {
								"label": "_Close",
								"action": "cancel",
								"submit": true,
								"version": 0,
								"shortCut": "Q"
							},
							"stamp": 12,
							"data": []
						},
						"Delete": {
							"type": "SubmitButton",
							"options": {
								"label": "_Delete",
								"action": "delete",
								"submit": true,
								"version": 0,
								"disabled": true
							},
							"stamp": 13,
							"data": []
						}
					},
					"stamp": 10,
					"data": []
				}
			},
			"stamp": 1,
			"data": []
		}
	},
	"stamps": 29,
	"data": []
}