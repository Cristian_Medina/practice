///#GLOBALS Lib
var maxInvoicesPerRecall = 5000;
var maxInvoicesPerFlush = 10;
Process.SetTimeOut(2 * 60 * 60);
var date = new Date();
var ERROR = -1;
var NOT_FINISHED = 0;
var ALL_INVOICE_PROCESSED = 1;
/**
 * Determine the current export mode (all invoices, custom selection, differential export)
 * @return {string} A string containing the export mode
 **/
function getExportMode()
{
	if (Variable.GetValueAsString("AncestorsRuid"))
	{
		return "CustomSelectionFromAdminList";
	}
	return "NoRestrictions";
}
/**
 * Return the filter when a selection of invoices to export was done
 * @return {string} a string containing an LDAP filter based on selection
 **/
function getQueryFilterForCustomSelectionFromAdminList()
{
	var filter = "";
	var ancestorsRuid = Variable.GetValueAsString("AncestorsRuid");
	if (ancestorsRuid)
	{
		var list = ancestorsRuid.split("|");
		if (list.length === 1)
		{
			filter = "RUIDEX=" + ancestorsRuid;
		}
		else
		{
			filter = "|(RUIDEX=" + list.join(")(RUIDEX=") + ")";
		}
	}
	return filter;
}
/**
 * Build the filter of the query based on the invoice export mode and options
 **/
function getQueryFilter()
{
	var filter;
	if (getExportMode() === "CustomSelectionFromAdminList")
	{
		filter = getQueryFilterForCustomSelectionFromAdminList();
	}
	else
	{
		var DateOfStartExport = Data.GetValue("DateOfStartExport__");
		if (DateOfStartExport)
		{
			filter = "&(ERPPostingDate__=*)(ERPPostingDate__<" + Sys.Helpers.Date.Date2DBDateTime(DateOfStartExport) + ")";
		}
		else
		{
			filter = "ERPPostingDate__=*";
		}
		if (Data.GetValue("SkipInvoicesAlreadyExported__"))
		{
			filter = "|(&(" + filter + ")(LastExportDate__!=*))(&(LastPaymentApprovalExportDate__!=*)(|(InvoiceStatus__=Paid)(InvoiceStatus__=To pay)))";
		}
	}
	var lastMsnEx = Data.GetValue("LastMsnEx__");
	if (lastMsnEx)
	{
		filter = "&(" + filter + ")(msnex>" + lastMsnEx + ")";
	}
	return filter;
}

function PaymentAuthorization(formdata)
{
	var status = formdata.GetValue("InvoiceStatus__");
	if (status && (status === "To pay" || status === "Paid"))
	{
		return true;
	}
	return false;
}
/**
 * Update transport given: set LastExportDateed
 * @param {transport} the transport to update
 * @param {date} date to set
 **/
var updateInvoiceAndPaymentAuthorization = {
	UpdateInvoice: false,
	UpdatePaymentAuthorization: false,
	init: function (transport, exportDate)
	{
		this.UpdateInvoice = false;
		this.UpdatePaymentAuthorization = false;
		if (!transport.GetUninheritedVars().GetValue_Date("LastExportDate__", 0) || !Data.GetValue("SkipInvoicesAlreadyExported__"))
		{
			transport.GetUninheritedVars().AddValue_Date("LastExportDate__", exportDate, true);
			this.UpdateInvoice = true;
		}
		if (PaymentAuthorization(transport.GetFormData()) && (!transport.GetUninheritedVars().GetValue_Date("LastPaymentApprovalExportDate__", 0) || !Data.GetValue("SkipInvoicesAlreadyExported__")))
		{
			transport.GetUninheritedVars().AddValue_Date("LastPaymentApprovalExportDate__", exportDate, true);
			this.UpdatePaymentAuthorization = true;
		}
		transport.Process();
		var error = transport.GetLastError() === 0;
		if (!error)
		{
			Log.Warn("Invoice " + transport.ruid + ": " + transport.GetLastErrorMessage());
		}
		return error;
	}
};

function appendInTempFile(converterName, fieldName, content)
{
	var result = true;
	var tempFileName = Data.GetValue(fieldName);
	if (!tempFileName)
	{
		Lib.FormDataConverter.SetActiveConverter(converterName);
		tempFileName = TemporaryFile.Create(Lib.FormDataConverter.GetExtension(), Lib.FormDataConverter.GetEncoding());
		Data.SetValue(fieldName, tempFileName);
		// Add header
		result = TemporaryFile.Append(tempFileName, Lib.FormDataConverter.GetHeader(), Lib.FormDataConverter.GetEOLFormat());
	}
	result = result && TemporaryFile.Append(tempFileName, content, Lib.FormDataConverter.GetEOLFormat());
	if (!result)
	{
		Log.Error("Failed to append data in file (filesize limit is 100MB) - adjust parameters");
	}
	return result;
}

function convertFormData(transport, formData, counter, buffer)
{
	if (updateInvoiceAndPaymentAuthorization.init(transport, date))
	{
		if (updateInvoiceAndPaymentAuthorization.UpdateInvoice && Lib.FormDataConverter.SetActiveConverter(Data.GetValue("Converter__")))
		{
			buffer.exported.push(Lib.FormDataConverter.Convert(formData));
			counter.exported++;
		}
		if (updateInvoiceAndPaymentAuthorization.UpdatePaymentAuthorization && Lib.FormDataConverter.SetActiveConverter("CSV_PaymentApproval"))
		{
			buffer.exportedApproved.push(Lib.FormDataConverter.Convert(formData));
			counter.exportedApproved++;
		}
		return true;
	}
	return false;
}

function convertTransport(transport, counter, buffer)
{
	var success = true;
	var ownerShipToken = "INVEXP_" + Process.GetProcessID();
	transport.ruid = transport.GetUninheritedVars().GetValue_String("ruidex", 0);
	var formData = transport.GetFormData();
	// if ApproversList is null then another export is in progress
	if (formData && formData.GetTable("ApproversList__"))
	{
		if (Sys.Helpers.GetOwnershipIfNeeded(transport, ownerShipToken))
		{
			if (!convertFormData(transport, formData, counter, buffer))
			{
				counter.skipped++;
			}
			transport.ReleaseAsyncOwnership(ownerShipToken);
		}
		else
		{
			Log.Warn("Invoice " + transport.ruid + ": Unable to update form because it is currently used by another user.");
			counter.skipped++;
		}
	}
	else
	{
		Log.Warn("Invoice " + transport.ruid + ": Unable to get form data. Check that the attribute DataFile has been added in Query.SetAttributesList.");
		counter.skipped++;
	}
	// Flush if needed
	if (buffer.exported.length === maxInvoicesPerFlush)
	{
		success = success && appendInTempFile(Data.GetValue("Converter__"), "InvoicesToExportTempFile__", buffer.exported.join(""));
		buffer.exported = [];
	}
	if (buffer.exportedApproved.length === maxInvoicesPerFlush)
	{
		success = success && appendInTempFile("CSV_PaymentApproval", "InvoicesApprovedTempFile__", buffer.exportedApproved.join(""));
		buffer.exportedApproved = [];
	}
	return success;
}
// Main function
function queryAndConvert(nbInvoiceMax)
{
	Process.StateOnLastScriptFailure(200);
	var lastMsnEx = "";
	var success = true;
	var counter = {
		skipped: 0,
		exported: 0,
		exportedApproved: 0
	};
	var buffer = {
		exported: [],
		exportedApproved: []
	};
	var transport = null;
	Query.Reset();
	Query.SetSpecificTable("CDNAME#Vendor invoice");
	Query.SetAttributesList("DataFile,ValidationURL,RuidEx,msnex,State,LastExportDate__,LastPaymentApprovalExportDate__,ProcessID,InvoiceStatus__,ERPPostingDate__");
	Query.SetSortOrder("msnex ASC");
	Query.SetOptionEx("limit=" + (maxInvoicesPerRecall + 1));
	Query.SetFilter(getQueryFilter());
	try
	{
		// Parse invoices and build attachement
		if (Query.MoveFirst())
		{
			transport = Query.MoveNext();
			while (transport && nbInvoiceMax > 0)
			{
				success = success && convertTransport(transport, counter, buffer);
				nbInvoiceMax--;
				lastMsnEx = transport.GetUninheritedVars().GetValue_String("msnex", 0);
				transport = Query.MoveNext();
			}
		}
		else
		{
			Log.Error("Cannot read query result");
			success = false;
		}
		// Flush the rest
		if (buffer.exported.length > 0)
		{
			success = success && appendInTempFile(Data.GetValue("Converter__"), "InvoicesToExportTempFile__", buffer.exported.join(""));
		}
		if (buffer.exportedApproved.length > 0)
		{
			success = success && appendInTempFile("CSV_PaymentApproval", "InvoicesApprovedTempFile__", buffer.exportedApproved.join(""));
		}
		if (success)
		{
			Data.SetValue("NumberOfInvoicesInError__", Data.GetValue("NumberOfInvoicesInError__") + counter.skipped);
			Data.SetValue("NumberOfExportedInvoices__", Data.GetValue("NumberOfExportedInvoices__") + counter.exported);
			Data.SetValue("NumberOfExportedApprovedInvoices__", Data.GetValue("NumberOfExportedApprovedInvoices__") + counter.exportedApproved);
			Data.SetValue("LastMsnEx__", lastMsnEx);
		}
	}
	catch (e)
	{
		Log.Error("An exception occured : " + e);
		success = false;
	}
	if (!success)
	{
		return ERROR;
	}
	if (!transport)
	{
		return ALL_INVOICE_PROCESSED;
	}
	return NOT_FINISHED;
}

function initialize()
{
	if (Data.GetActionName() !== "export_in_progress")
	{
		Data.SetValue("NumberOfExportedInvoices__", 0);
		Data.SetValue("NumberOfExportedApprovedInvoices__", 0);
		Data.SetValue("NumberOfInvoicesInError__", 0);
		Data.SetValue("DateOfStartExport__", date);
	}
}

function endOfExport(status)
{
	Data.SetValue("ExportStatus__", status);
	Data.SetValue("CompletedDateTime__", new Date());
	Process.AllowScriptRetries(false);
	/** Clean technical fields */
	Data.SetValue("InvoicesToExportTempFile__", "");
	Data.SetValue("InvoicesApprovedTempFile__", "");
	Data.SetValue("DateOfStartExport__", "");
	Data.SetValue("LastMsnEx__", "");
}

function AddFooter(converter, fileName)
{
	Lib.FormDataConverter.SetActiveConverter(converter);
	if (!TemporaryFile.Append(fileName, Lib.FormDataConverter.GetFooter(), Lib.FormDataConverter.GetEOLFormat()))
	{
		Log.Error("Failed to append footer in file (filesize limit is 100MB) - adjust parameters");
	}
}
/**
 * Get temporaries files and save them as attachments
 */
function attachFiles()
{
	var dateISO = date.toISOString().substr(0, 10);
	if (Data.GetValue("NumberOfExportedInvoices__") > 0)
	{
		var tempFileName = Data.GetValue("InvoicesToExportTempFile__");
		AddFooter(Data.GetValue("Converter__"), tempFileName);
		var attachName = Language.Translate("Posted invoices", false) + " " + dateISO;
		Attach.AttachTemporaryFile(tempFileName, attachName);
	}
	if (Data.GetValue("NumberOfExportedApprovedInvoices__") > 0)
	{
		var invoicesApprovedTempFileName = Data.GetValue("InvoicesApprovedTempFile__");
		AddFooter("CSV_PaymentApproval", invoicesApprovedTempFileName);
		var attachNameApprovedInvoices = Language.Translate("Approved invoices", false) + " " + dateISO;
		Attach.AttachTemporaryFile(invoicesApprovedTempFileName, attachNameApprovedInvoices);
	}
}

function run()
{
	if (Data.GetValue("ExportStatus__") !== "Cancelled")
	{
		initialize();
		if (Process.GetScriptRetryCount() === 0)
		{
			var res = queryAndConvert(maxInvoicesPerRecall);
			if (res === NOT_FINISHED)
			{
				Process.RecallScript("export_in_progress");
			}
			else if (res === ALL_INVOICE_PROCESSED)
			{
				attachFiles();
				endOfExport("Completed");
			}
			else
			{
				Log.Error("Error during the export");
				endOfExport("Error");
			}
		}
		else
		{
			Log.Error("Script retry :" + Process.GetScriptRetryCount() + ". Set process in error");
			endOfExport("Error");
		}
	}
}
run();