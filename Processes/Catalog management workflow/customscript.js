///#GLOBALS Lib
ProcessInstance.SetSilentChange(true);
Process.SetHelpId("5024");
var isExternal = Variable.GetValueAsString("CatalogManagmentType");
if (!isExternal) {
    Variable.SetValueAsString("CatalogManagmentType", "internal");
}
var currentStatus = Data.GetValue("Status__");
var wkf = Lib.Purchasing.CM.Workflow;
var isInternalUpdateRequest = Lib.Purchasing.CM.Workflow.IsInternalUpdateRequest();
function DisplayParsingError() {
    var tableControl = Controls.ParsingErrorItems__;
    tableControl.SetItemCount(0);
    tableControl.SetWidth("100%");
    tableControl.SetExtendableColumn("CSVLine__");
    var extractedData = Lib.Purchasing.CM.GetExternalData();
    var errorCount = 0;
    if (extractedData.ParsingError) {
        extractedData.ParsingError.forEach(function (value) {
            var newErrorItem = tableControl.AddItem();
            newErrorItem.SetValue("CSVLineNumber__", value.LineNumber);
            newErrorItem.SetValue("CSVLine__", value.CSVLine);
            newErrorItem.SetValue("ErrorStatus__", Language.Translate(value.ErrorStatus));
            errorCount++;
        });
        setTimeout(function () {
            var lineItemsCount = Math.min(tableControl.GetItemCount(), tableControl.GetLineCount());
            for (var lineIdx = 0; lineIdx < lineItemsCount; lineIdx++) {
                var row = tableControl.GetRow(lineIdx);
                row.AddStyle("highlight-danger");
            }
        }, 100);
    }
    Controls.ParsingErrorsPane.Hide(errorCount === 0);
    Controls.ParsingErrorsPane.SetLabel(Language.Translate("_ParsingErrorsPane {0}", true, errorCount));
    return errorCount != 0;
}
var ApprovalCommentCheck = {
    GetCommentField: function () {
        return Controls.Comments__;
    },
    // Fill dialog callback: Design and instantiation of the controls
    fill_CB: function (dialogTexts) {
        return function (dialog /*, tabId, event, control*/) {
            var commentValue = ApprovalCommentCheck.GetCommentField().GetValue();
            var ctrl = dialog.AddDescription("ctrlDesc", null, 466);
            if (Sys.Helpers.IsEmpty(commentValue)) {
                ctrl.SetText(Language.Translate(dialogTexts.descriptionRequired));
            }
            else {
                ctrl.SetText(Language.Translate(dialogTexts.descriptionConfirmation));
            }
            var commentCtrl = dialog.AddMultilineText("ctrlComments", "_Comment", 400);
            dialog.RequireControl(commentCtrl);
            commentCtrl.SetValue(commentValue);
        };
    },
    // Validate dialog callback: checks if required fields are set
    validate_CB: function (dialog /*, tabId, event, control*/) {
        if (!dialog.GetControl("ctrlComments").GetValue()) {
            dialog.GetControl("ctrlComments").SetError("This field is required!");
            return false;
        }
        return true;
    },
    // Commit dialog callback: updates the process form with dialog results
    commit_CB: function (anAction, onCommitted) {
        var action = anAction;
        return function (dialog /*, tabId, event, control*/) {
            var newValue = dialog.GetControl("ctrlComments").GetValue();
            ApprovalCommentCheck.GetCommentField().SetValue(newValue);
            ApprovalCommentCheck.GetCommentField().SetError("");
            ProcessInstance.Approve(action);
            if (Sys.Helpers.IsFunction(onCommitted)) {
                onCommitted();
            }
        };
    },
    OnClick: function (action, dialogTexts, onCommitted) {
        var defaultDialogTexts = {
            titleConfirmation: "_Approval comments confirmation",
            titleRequired: "_Approval comments required",
            descriptionConfirmation: "_Please confirm your comment:",
            descriptionRequired: "_Please write your comment:"
        }, att;
        if (!dialogTexts) {
            dialogTexts = defaultDialogTexts;
        }
        else {
            for (att in defaultDialogTexts) {
                if (!(att in dialogTexts)) {
                    dialogTexts[att] = defaultDialogTexts[att];
                }
            }
        }
        var title = ApprovalCommentCheck.GetCommentField().GetValue() ? dialogTexts.titleConfirmation : dialogTexts.titleRequired;
        Popup.Dialog(title, null, ApprovalCommentCheck.fill_CB(dialogTexts), ApprovalCommentCheck.commit_CB(action, onCommitted), ApprovalCommentCheck.validate_CB);
        return false;
    }
};
function SetWizardStep(step) {
    var STEPS = {
        "import": "color9",
        "workflow": "color9",
        "summary": "color9"
    };
    STEPS[step] = "color8";
    Controls.ImportStepButton__.SetTextColor(STEPS["import"]);
    Controls.WorkflowStepButton__.SetTextColor(STEPS["workflow"]);
    Controls.SummaryStepButton__.SetTextColor(STEPS["summary"]);
}
function InitLayout() {
    Controls.Comments__.SetPlaceholder(Language.Translate("_Enter a comment"));
    Controls.ImportDescription__.Hide(false);
    Controls.RequesterName__.Hide(false);
    Controls.RequesterLogin__.Hide(true);
    Controls.Status__.Hide(false);
    Controls.CompanyCode__.Hide(isInternalUpdateRequest);
    Controls.VendorNumber__.Hide(isInternalUpdateRequest);
    Controls.VendorName__.Hide(isInternalUpdateRequest);
    Controls.AttachmentsPane.Hide(false);
    Controls.GeneralInformationsPane.Hide(false);
    Controls.WorkflowPane.Hide(wkf.controller.GetNbContributors() < 2);
    Controls.WorkflowStepButton__.Hide(wkf.controller.GetNbContributors() < 2);
    Controls.Spacer3__.Hide(wkf.controller.GetNbContributors() < 2);
}
function InitImportStep() {
    SetWizardStep("import");
    Controls.Reason__.Hide(true);
    Controls.Reject.Hide(true);
    Controls.Revert.Hide(true);
    Controls.SubmissionDateTime__.Hide(true);
    Controls.ValidationDateTime__.Hide(true);
    Controls.ImportDescription__.SetReadOnly(false);
    if (Sys.Helpers.IsEmpty(Data.GetValue("RequesterLogin__"))) {
        Data.SetValue("RequesterLogin__", User.loginId);
        Data.SetValue("RequesterName__", User.fullName);
    }
    var lineDisplayed = Lib.Purchasing.CM.DisplayPreviews(true);
    var CSVhasError = DisplayParsingError();
    Controls.AttachmentsPane.AllowChangeOrUploadReferenceDocument(true);
    Controls.Submit.SetLabel("_Send to approval");
    Controls.Submit.SetDisabled(CSVhasError || !Controls.AttachmentsPane.IsDocumentLoaded() || lineDisplayed === 0);
}
function InitWorkflowStep() {
    SetWizardStep("workflow");
    Controls.ValidationDateTime__.Hide(true);
    Controls.Reason__.Hide(true);
    Controls.SubmissionDateTime__.Hide(false);
    Controls.ParsingErrorsPane.Hide(true);
    Controls.AttachmentsPane.AllowChangeOrUploadReferenceDocument(false);
    Lib.Purchasing.CM.DisplayPreviews(true);
    Controls.Revert.Hide(true);
    if (Variable.GetValueAsString("CSVProcessingError") === "true") {
        Controls.Comments__.Hide(true);
        Controls.Submit.Hide(true);
        Controls.Reject.Hide(true);
        Lib.CommonDialog.PopupYesCancel(function (action) {
            if (action === "Yes") {
                Variable.SetValueAsString("CSVProcessingError", "false");
                ProcessInstance.ApproveAsynchronous("Submit");
            }
            else {
                ProcessInstance.Quit();
            }
        }, "_CSV Import Error", "_Some error occured, whould you like to retry ?", "_Retry", "_Quit");
    }
    else {
        Controls.Comments__.Hide(false);
        Controls.Submit.Hide(false);
        Controls.Reject.Hide(false);
    }
}
function InitResultStep(displayPreviews) {
    if (displayPreviews === void 0) { displayPreviews = true; }
    SetWizardStep("summary");
    Controls.ValidationDateTime__.Hide(false);
    Controls.Comments__.Hide(true);
    Controls.Reason__.Hide(Sys.Helpers.IsEmpty(Data.GetValue("Reason__")));
    Controls.SubmissionDateTime__.Hide(false);
    Controls.Submit.Hide(true);
    Controls.Reject.Hide(true);
    Controls.ParsingErrorsPane.Hide(true);
    Controls.AttachmentsPane.AllowChangeOrUploadReferenceDocument(false);
    Lib.Purchasing.CM.DisplayPreviews(displayPreviews);
    if (currentStatus === "Approved") {
        Controls.Revert.SetDisabled(true);
        Lib.Purchasing.CM.Workflow.GetLastApprovedUpdateRequest()
            .Then(function (lastCMW) {
            var isLastApprovedCMW = (Data.GetValue("RUIDEX") === lastCMW);
            Controls.Revert.SetDisabled(!isLastApprovedCMW);
        });
    }
    else {
        Controls.Revert.Hide(true);
    }
}
function HideTechnicalFields() {
    Controls.NextProcess.Hide(true);
    Controls.SystemData.Hide(true);
}
function HandleEvents() {
    Controls.Submit.OnClick = function () {
        ProcessInstance.ApproveAsynchronous("saveBeforeSubmit");
        return false;
    };
    Controls.Reject.OnClick = function () {
        ApprovalCommentCheck.OnClick("Reject", {
            titleConfirmation: "_Reject comments confirmation",
            titleRequired: "_Reject comments required",
            descriptionConfirmation: "_Please provide a reason for rejection:",
            descriptionRequired: "_Rejection reason is required:"
        });
        return false;
    };
    Controls.Revert.OnClick = function () {
        Lib.Purchasing.CM.Workflow.RevertManager();
        return false;
    };
    Controls.AttachmentsPane.OnDocumentDeleted = function () {
        Lib.Purchasing.CM.SetExternalData(null);
        InitImportStep();
    };
}
function InitWorkflow() {
    Log.Info("Initializing workflow");
    wkf.controller.Define(wkf.parameters);
    wkf.controller.AllowRebuild(false);
    Log.Info("Initialization of workflow done");
}
function Initform() {
    Controls.IsInternalUpdateRequest__.Check(isInternalUpdateRequest);
    InitWorkflow();
    InitLayout();
    Lib.Purchasing.CM.Workflow.InitWorkflowLayout();
    switch (currentStatus) {
        case "Approved":
        case "Reverted":
            InitResultStep();
            break;
        case "Rejected":
        case "Failed":
            InitResultStep(false);
            break;
        case "ToApprove":
            InitWorkflowStep();
            break;
        default:
            InitImportStep();
            break;
    }
    HideTechnicalFields();
    HandleEvents();
}
Initform();
