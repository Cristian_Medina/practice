///#GLOBALS Sys Lib
// TODO replace by language.translate
var COLUMNS = {
	MSNEX: 0,
	NAME__: 1,
	REFERENCENUMBER__: 2,
	ENDDATE__: 3,
	TACITRENEWAL__: 4,
	NOTIFICATIONDATE__: 5,
	INITIALLYEXPECTEDENDDATE__: 6,
	OWNERID: 7,
	CREATOROWNERID: 8,
	ORIGINALOWNERID: 9,
	CREATEONBEHALF: 10,
	SEND: 11
};
var contracts = [];
var ownerToNotify = [];

function ReadCSV()
{
	Log.Info("ReadCSV: START");
	var csvReader = Sys.Helpers.CSVReader.CreateInstance(0, "V2");
	csvReader.ReturnSeparator = "\n";
	csvReader.GuessSeparator(); // Warning: it reads the first line
	Log.Info("Guessed separator: " + csvReader.SplitSeparator);
	var count = 0;
	while (csvReader.GetNextLine())
	{
		try
		{
			count++;
			var parsedLine = csvReader.GetCurrentLineArray();
			Log.Info("Contract " + count + ": " + parsedLine);
			var ownerId = parsedLine[COLUMNS.OWNERID];
			var culture = Lib.P2P.GetValidatorOrOwner().GetValue("Culture");
			var endDate = Sys.Helpers.Date.ParseLocaleDate(parsedLine[COLUMNS.ENDDATE__], culture);
			var tacitRenewal = Sys.Helpers.String.ToBoolean(parsedLine[COLUMNS.TACITRENEWAL__]);
			var notifDate = Sys.Helpers.Date.ParseLocaleDate(parsedLine[COLUMNS.NOTIFICATIONDATE__], culture);
			var initiallyExpectedEndDate = !Sys.Helpers.IsEmpty(parsedLine[COLUMNS.INITIALLYEXPECTEDENDDATE__]) ? Sys.Helpers.Date.ParseLocaleDate(parsedLine[COLUMNS.INITIALLYEXPECTEDENDDATE__], culture) : null;
			contracts.push(
			{
				RuidEx: GetRuidEX(parsedLine[COLUMNS.MSNEX]),
				Name: parsedLine[COLUMNS.NAME__],
				OwnerId: ownerId,
				Culture: culture,
				EndDate: endDate,
				TacitRenewal: tacitRenewal,
				NotificationDate: notifDate,
				InitiallyExpectedEndDate: initiallyExpectedEndDate,
				Notifiable: true
			});
		}
		catch (e)
		{
			Log.Error("Error parsing CSV file. Ligne " + count + "\n" + e);
		}
	}
	Log.Info("ReadCSV: END. " + count + " contract(s) found.");
}

function GetRuidEX(msnEx)
{
	var processID = Process.GetProcessID("P2P - Contract");
	return "CD#" + processID + "." + msnEx;
}

function ManageContracts()
{
	Log.Info("ManageContracts: START");
	for (var _i = 0, contracts_1 = contracts; _i < contracts_1.length; _i++)
	{
		var contract = contracts_1[_i];
		var endDate = contract.EndDate ? Sys.Helpers.Date.NormalizeToEskerDate(contract.EndDate) : null;
		var notificationDate = contract.NotificationDate ? Sys.Helpers.Date.NormalizeToEskerDate(contract.NotificationDate) : null;
		if (Sys.Helpers.Date.CompareDateToToday(endDate) < 0)
		{
			Log.Info("Contract endDate passed for contract: " + contract.Name + "' , call UpdateContract.");
			var ctrTransport = Process.GetUpdatableTransportAsProcessAdmin(contract.RuidEx);
			//ResumeWithActionAsync works with record in state = 100 where ResumeWithAction does not. TODO : ResumeWithAction should work with record in state 100
			ctrTransport.ResumeWithActionAsync("UpdateContract");
			contract.Notifiable = false;
		}
		else if (Sys.Helpers.Date.CompareDateToToday(notificationDate) === 0)
		{
			// notif
			Log.Info("New contract close to end or renewal: '" + contract.Name + "'. " + contract.OwnerId + " will be notified");
			ownerToNotify.push(contract.OwnerId);
		}
		else
		{
			Log.Info("Contract close to end or renewal '" + contract.Name + "'. " + contract.OwnerId + " but already notified");
		}
	}
	Log.Info("ManageContracts: END.");
}

function SendNotifs()
{
	var contractNotifiable = contracts.filter(function (c)
	{
		return c.Notifiable !== false;
	});
	var groupedContractToNotify = {};
	for (var _i = 0, contractNotifiable_1 = contractNotifiable; _i < contractNotifiable_1.length; _i++)
	{
		var contract = contractNotifiable_1[_i];
		if (ownerToNotify.indexOf(contract.OwnerId) !== -1)
		{
			if (groupedContractToNotify[contract.OwnerId])
			{
				groupedContractToNotify[contract.OwnerId].push(contract);
			}
			else
			{
				groupedContractToNotify[contract.OwnerId] = [contract];
			}
		}
	}
	for (var ownerId in groupedContractToNotify)
	{
		if (groupedContractToNotify.hasOwnProperty(ownerId))
		{
			groupedContractToNotify[ownerId].sort(function (a, b)
			{
				return Sys.Helpers.Date.CompareDate(a.EndDate, b.EndDate);
			});
			var contractsCloseToEnd = 0;
			var contractsCloseToRenewal = 0;
			for (var _a = 0, _b = groupedContractToNotify[ownerId]; _a < _b.length; _a++)
			{
				var contract = _b[_a];
				if (contract.TacitRenewal && !Sys.Helpers.IsEmpty(contract.InitiallyExpectedEndDate))
				{
					contractsCloseToRenewal++;
				}
				else
				{
					contractsCloseToEnd++;
				}
			}
			Log.Info("Send notif to " + ownerId + " for " + contractsCloseToEnd + " contracts close to end and " + contractsCloseToRenewal + " contracts close to renewals.");
			var owner = Users.GetUser(ownerId);
			var ownerEmail = owner.GetValue("EmailAddress");
			if (Sys.Helpers.IsEmpty(ownerEmail))
			{
				Log.Error("No email address to notify user.");
				return;
			}
			var baseUrl = Data.GetValue("ValidationUrl");
			baseUrl = baseUrl.substr(0, baseUrl.lastIndexOf("/"));
			var customTags = {
				ViewUrl_CloseToEnd: baseUrl + "/View.link?tabName=_My documents-P2P%20Contract&viewName=_P2P_Views%20-%20My%20Contracts%20expiring%20soon",
				ViewUrl_CloseToRenewal: baseUrl + "/View.link?tabName=_My documents-P2P%20Contract&viewName=_P2P_Views%20-%20My%20Contracts%20upcoming%20renewals"
			};
			if (contractsCloseToEnd > 0)
			{
				if (contractsCloseToEnd > 1)
				{
					customTags.SeveralContractsCloseToEnd = "true";
					customTags.NumberOfContractsCloseToEnd = contractsCloseToEnd.toString();
				}
				else
				{
					customTags.OneContractCloseToEnd = "true";
				}
			}
			if (contractsCloseToRenewal > 0)
			{
				if (contractsCloseToRenewal > 1)
				{
					customTags.SeveralContractsCloseToRenewal = "true";
					customTags.NumberOfContractsCloseToRenewal = contractsCloseToRenewal.toString();
				}
				else
				{
					customTags.OneContractCloseToRenewal = "true";
				}
			}
			try
			{
				SendEmailNotification(ownerId, null, "Contract_Email_NotifReminder.htm", null, customTags);
			}
			catch (e)
			{
				Log.Error("Cannot send the notification. Details: " + e);
			}
		}
	}
}

function SendEmailNotification(userID, subject, template, backupUserAsCC, tags)
{
	var options = {
		userId: userID,
		subject: subject,
		template: template,
		customTags: tags,
		fromName: "Esker Contract",
		backupUserAsCC: !!backupUserAsCC
	};
	try
	{
		Sys.EmailNotification.SendEmailNotification(options);
	}
	catch (e)
	{
		Log.Error("Cannot send the notification. Details: " + e);
	}
}
ReadCSV();
ManageContracts();
SendNotifs();