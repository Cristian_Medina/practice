{
	"*": {
		"GDRData": {
			"type": "GDRData",
			"options": {
				"version": 0
			},
			"stamp": 1,
			"data": []
		},
		"OCRParams": {
			"type": "OCRParams",
			"options": {
				"version": 0
			},
			"stamp": 2,
			"data": []
		},
		"Screen": {
			"type": "FlexibleFormLayout",
			"options": {
				"splitterType": "complex",
				"version": 0,
				"maxwidth": 1024,
				"backColor": "text-backgroundcolor-default",
				"style": "FlexibleFormLight"
			},
			"*": {
				"form-header": {
					"type": "FlexibleFormHeader",
					"options": {
						"version": 0,
						"maxwidth": 1024
					},
					"stamp": 4,
					"data": []
				},
				"form-center": {
					"type": "FlexibleFormCenter",
					"options": {
						"version": 0,
						"maxwidth": 1024
					},
					"*": {
						"form-content": {
							"type": "FlexibleFormContent",
							"options": {
								"version": 0
							},
							"*": {
								"form-content-top": {
									"type": "FlexibleFormSplitter",
									"options": {
										"autosize": true,
										"version": 1,
										"box": {
											"top": 0,
											"left": 0,
											"width": 100,
											"height": 18
										},
										"hidden": true
									},
									"*": {
										"form-content-top-1": {
											"type": "FlexibleFormLine",
											"options": {
												"version": 0
											},
											"*": {
												"DocumentsPanel": {
													"type": "PanelDocument",
													"options": {
														"version": 0,
														"border": {},
														"labelLength": 0,
														"iconURL": "",
														"TitleFontSize": "S",
														"hideActionButtons": false,
														"hideTitleBar": false,
														"hideTitle": false,
														"sameHeightAsSiblings": false,
														"label": "_Documents"
													},
													"*": {
														"Document": {
															"data": [
																"documentscontroldata"
															],
															"type": "Documents",
															"options": {
																"version": 0
															},
															"stamp": 10
														}
													},
													"stamp": 9,
													"data": []
												}
											},
											"stamp": 8,
											"data": []
										}
									},
									"stamp": 7,
									"data": []
								},
								"form-content-bottom": {
									"type": "FlexibleFormSplitter",
									"options": {
										"version": 1,
										"box": {
											"top": 0,
											"left": 0,
											"width": 100,
											"height": 100
										},
										"hidden": false,
										"hideSeparator": true
									},
									"*": {
										"form-content-left": {
											"type": "FlexibleFormSplitter",
											"options": {
												"version": 1,
												"box": {
													"top": "0%",
													"left": "0%",
													"width": "100%",
													"height": null
												},
												"hidden": false
											},
											"*": {
												"form-content-left-1": {
													"type": "FlexibleFormLine",
													"options": {
														"version": 0
													},
													"*": {
														"PanelGeneral": {
															"type": "PanelData",
															"options": {
																"border": {},
																"version": 0,
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": true,
																"hideTitleBar": false,
																"hideTitle": false,
																"sameHeightAsSiblings": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "_PanelGeneral",
																"leftImageURL": "",
																"removeMargins": false,
																"elementsAlignment": "left",
																"labelLength": 0
															},
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"options": {
																		"controlsAssociation": {
																			"ConfigName__": "LabelConfigName__",
																			"LabelConfigName__": "ConfigName__",
																			"ConfigDescription__": "LabelConfigDescription__",
																			"LabelConfigDescription__": "ConfigDescription__"
																		},
																		"version": 0
																	},
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"options": {
																				"lines": 2,
																				"columns": 2,
																				"version": 0,
																				"ctrlsPos": {
																					"ConfigName__": {
																						"line": 1,
																						"column": 2
																					},
																					"LabelConfigName__": {
																						"line": 1,
																						"column": 1
																					},
																					"ConfigDescription__": {
																						"line": 2,
																						"column": 2
																					},
																					"LabelConfigDescription__": {
																						"line": 2,
																						"column": 1
																					}
																				},
																				"colspans": [
																					[],
																					[]
																				]
																			},
																			"data": [
																				"fields"
																			],
																			"*": {
																				"LabelConfigName__": {
																					"type": "Label",
																					"data": [
																						"ConfigName__"
																					],
																					"options": {
																						"label": "_ConfigName",
																						"version": 0
																					},
																					"stamp": 31
																				},
																				"ConfigName__": {
																					"type": "ShortText",
																					"data": [
																						"ConfigName__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"label": "_ConfigName",
																						"activable": true,
																						"width": "350",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"helpIconPosition": "Right",
																						"browsable": false,
																						"length": 100
																					},
																					"stamp": 32
																				},
																				"LabelConfigDescription__": {
																					"type": "Label",
																					"data": [
																						"ConfigDescription__"
																					],
																					"options": {
																						"label": "_ConfigDescription",
																						"version": 0
																					},
																					"stamp": 33
																				},
																				"ConfigDescription__": {
																					"type": "LongText",
																					"data": [
																						"ConfigDescription__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"minNbLines": 3,
																						"label": "_ConfigDescription",
																						"activable": true,
																						"width": "350",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"helpIconPosition": "Right",
																						"browsable": false
																					},
																					"stamp": 34
																				}
																			},
																			"stamp": 16
																		}
																	},
																	"stamp": 15,
																	"data": []
																}
															},
															"stamp": 14,
															"data": []
														},
														"PanelWorkingDays": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": true,
																"hideTitleBar": false,
																"hideTitle": false,
																"sameHeightAsSiblings": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "_PanelWorkingDays",
																"hidden": false,
																"leftImageURL": "",
																"removeMargins": false,
																"elementsAlignment": "left",
																"version": 0,
																"labelLength": "90px"
															},
															"stamp": 36,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"Weekday1__": "LabelWeekday1__",
																			"LabelWeekday1__": "Weekday1__",
																			"Weekday2__": "LabelWeekday2__",
																			"LabelWeekday2__": "Weekday2__",
																			"Weekday3__": "LabelWeekday3__",
																			"LabelWeekday3__": "Weekday3__",
																			"Weekday4__": "LabelWeekday4__",
																			"LabelWeekday4__": "Weekday4__",
																			"Weekday5__": "LabelWeekday5__",
																			"LabelWeekday5__": "Weekday5__",
																			"Weekday6__": "LabelWeekday6__",
																			"LabelWeekday6__": "Weekday6__",
																			"Weekday0__": "LabelWeekday0__",
																			"LabelWeekday0__": "Weekday0__"
																		},
																		"version": 0
																	},
																	"stamp": 37,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"Weekday1__": {
																						"line": 1,
																						"column": 2
																					},
																					"LabelWeekday1__": {
																						"line": 1,
																						"column": 1
																					},
																					"Weekday2__": {
																						"line": 2,
																						"column": 2
																					},
																					"LabelWeekday2__": {
																						"line": 2,
																						"column": 1
																					},
																					"Weekday3__": {
																						"line": 3,
																						"column": 2
																					},
																					"LabelWeekday3__": {
																						"line": 3,
																						"column": 1
																					},
																					"Weekday4__": {
																						"line": 4,
																						"column": 2
																					},
																					"LabelWeekday4__": {
																						"line": 4,
																						"column": 1
																					},
																					"Weekday5__": {
																						"line": 5,
																						"column": 2
																					},
																					"LabelWeekday5__": {
																						"line": 5,
																						"column": 1
																					},
																					"Weekday6__": {
																						"line": 6,
																						"column": 2
																					},
																					"LabelWeekday6__": {
																						"line": 6,
																						"column": 1
																					},
																					"Weekday0__": {
																						"line": 7,
																						"column": 2
																					},
																					"LabelWeekday0__": {
																						"line": 7,
																						"column": 1
																					}
																				},
																				"lines": 7,
																				"columns": 2,
																				"colspans": [
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[]
																				],
																				"version": 0
																			},
																			"stamp": 38,
																			"*": {
																				"LabelWeekday1__": {
																					"type": "Label",
																					"data": [
																						"Weekday1__"
																					],
																					"options": {
																						"label": "_Monday",
																						"version": 0
																					},
																					"stamp": 43
																				},
																				"Weekday1__": {
																					"type": "CheckBox",
																					"data": [
																						"Weekday1__"
																					],
																					"options": {
																						"label": "_Monday",
																						"activable": true,
																						"width": 230,
																						"version": 0,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"helpIconPosition": "Right",
																						"defaultValue": true
																					},
																					"stamp": 44
																				},
																				"LabelWeekday2__": {
																					"type": "Label",
																					"data": [
																						"Weekday2__"
																					],
																					"options": {
																						"label": "_Tuesday",
																						"version": 0
																					},
																					"stamp": 45
																				},
																				"Weekday2__": {
																					"type": "CheckBox",
																					"data": [
																						"Weekday2__"
																					],
																					"options": {
																						"label": "_Tuesday",
																						"activable": true,
																						"width": 230,
																						"version": 0,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"helpIconPosition": "Right",
																						"defaultValue": true
																					},
																					"stamp": 46
																				},
																				"LabelWeekday3__": {
																					"type": "Label",
																					"data": [
																						"Weekday3__"
																					],
																					"options": {
																						"label": "_Wednesday",
																						"version": 0
																					},
																					"stamp": 47
																				},
																				"Weekday3__": {
																					"type": "CheckBox",
																					"data": [
																						"Weekday3__"
																					],
																					"options": {
																						"label": "_Wednesday",
																						"activable": true,
																						"width": 230,
																						"version": 0,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"helpIconPosition": "Right",
																						"defaultValue": true
																					},
																					"stamp": 48
																				},
																				"LabelWeekday4__": {
																					"type": "Label",
																					"data": [
																						"Weekday4__"
																					],
																					"options": {
																						"label": "_Thursday",
																						"version": 0
																					},
																					"stamp": 49
																				},
																				"Weekday4__": {
																					"type": "CheckBox",
																					"data": [
																						"Weekday4__"
																					],
																					"options": {
																						"label": "_Thursday",
																						"activable": true,
																						"width": 230,
																						"version": 0,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"helpIconPosition": "Right",
																						"defaultValue": true
																					},
																					"stamp": 50
																				},
																				"LabelWeekday5__": {
																					"type": "Label",
																					"data": [
																						"Weekday5__"
																					],
																					"options": {
																						"label": "_Friday",
																						"version": 0
																					},
																					"stamp": 51
																				},
																				"Weekday5__": {
																					"type": "CheckBox",
																					"data": [
																						"Weekday5__"
																					],
																					"options": {
																						"label": "_Friday",
																						"activable": true,
																						"width": 230,
																						"version": 0,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"helpIconPosition": "Right",
																						"defaultValue": true
																					},
																					"stamp": 52
																				},
																				"LabelWeekday6__": {
																					"type": "Label",
																					"data": [
																						"Weekday6__"
																					],
																					"options": {
																						"label": "_Saturday",
																						"version": 0
																					},
																					"stamp": 53
																				},
																				"Weekday6__": {
																					"type": "CheckBox",
																					"data": [
																						"Weekday6__"
																					],
																					"options": {
																						"label": "_Saturday",
																						"activable": true,
																						"width": 230,
																						"version": 0,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"helpIconPosition": "Right"
																					},
																					"stamp": 54
																				},
																				"LabelWeekday0__": {
																					"type": "Label",
																					"data": [
																						"Weekday0__"
																					],
																					"options": {
																						"label": "_Sunday",
																						"version": 0
																					},
																					"stamp": 55
																				},
																				"Weekday0__": {
																					"type": "CheckBox",
																					"data": [
																						"Weekday0__"
																					],
																					"options": {
																						"label": "_Sunday",
																						"activable": true,
																						"width": 230,
																						"version": 0,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"helpIconPosition": "Right"
																					},
																					"stamp": 56
																				}
																			}
																		}
																	}
																}
															}
														}
													},
													"stamp": 13,
													"data": []
												},
												"form-content-left-2": {
													"type": "FlexibleFormLine",
													"options": {
														"version": 0
													},
													"*": {
														"SystemData": {
															"type": "PanelSystemData",
															"options": {
																"multicolumn": false,
																"version": 0,
																"border": {},
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": false,
																"hideTitleBar": false,
																"hideTitle": false,
																"sameHeightAsSiblings": false,
																"label": "_System data",
																"hidden": true
															},
															"stamp": 18,
															"data": []
														}
													},
													"stamp": 17,
													"data": []
												},
												"form-content-left-3": {
													"type": "FlexibleFormLine",
													"options": {
														"version": 0
													},
													"*": {
														"NextProcess": {
															"type": "PanelNextProcess",
															"options": {
																"hidden": true,
																"version": 0,
																"border": {},
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": false,
																"hideTitleBar": false,
																"hideTitle": false,
																"sameHeightAsSiblings": false,
																"label": "_Next Process"
															},
															"stamp": 20,
															"data": []
														}
													},
													"stamp": 19,
													"data": []
												},
												"form-content-left-4": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 57,
													"*": {
														"PanelDaysOff": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": true,
																"hideTitleBar": false,
																"hideTitle": false,
																"sameHeightAsSiblings": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "_PanelDaysOff",
																"hidden": false,
																"leftImageURL": "",
																"removeMargins": false,
																"elementsAlignment": "left",
																"version": 0,
																"labelLength": 0
															},
															"stamp": 58,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"jsonDaysOff__": "LabeljsonDaysOff__",
																			"LabeljsonDaysOff__": "jsonDaysOff__"
																		},
																		"version": 0
																	},
																	"stamp": 59,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"DaysOffList__": {
																						"line": 1,
																						"column": 1
																					},
																					"jsonDaysOff__": {
																						"line": 2,
																						"column": 2
																					},
																					"LabeljsonDaysOff__": {
																						"line": 2,
																						"column": 1
																					}
																				},
																				"lines": 2,
																				"columns": 2,
																				"colspans": [
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[]
																				],
																				"version": 0
																			},
																			"stamp": 60,
																			"*": {
																				"DaysOffList__": {
																					"type": "Table",
																					"data": [
																						"DaysOffList__"
																					],
																					"options": {
																						"version": 5,
																						"lines": 10,
																						"columns": 5,
																						"displayEmptyLines": false,
																						"alwaysOneLine": true,
																						"menuOptions": {
																							"disableInsertLine": false,
																							"disableAddLine": true,
																							"hideTopNavigation": false,
																							"hideBottomNavigation": false,
																							"hideTableRowMenu": false,
																							"hideTableRowDelete": false,
																							"hideTableRowAdd": false,
																							"hideTableRowDuplicate": true,
																							"hideTableRowButtonDuplicate": true,
																							"addButtonShouldInsert": false
																						},
																						"item": {
																							"display": null
																						},
																						"group": {
																							"name": "GDR\nPage"
																						},
																						"label": "_DaysOffList",
																						"subsection": null
																					},
																					"stamp": 61,
																					"*": {
																						"Actions": {
																							"type": "$TableActions",
																							"subtype": "Actions",
																							"options": {
																								"version": 0
																							},
																							"stamp": 62,
																							"*": {
																								"Top": {
																									"type": "$TableActions",
																									"subtype": "Top",
																									"options": {
																										"version": 0
																									},
																									"stamp": 63
																								},
																								"Bottom": {
																									"type": "$TableActions",
																									"subtype": "Top",
																									"options": {
																										"version": 0
																									},
																									"stamp": 64
																								}
																							}
																						},
																						"Header": {
																							"type": "$TableRow",
																							"subtype": "Header",
																							"options": {
																								"version": 0
																							},
																							"stamp": 65,
																							"data": [
																								null
																							],
																							"*": {
																								"Cell1": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": "350",
																										"version": 0
																									},
																									"stamp": 68,
																									"data": [],
																									"*": {
																										"DayOffName__": {
																											"type": "Label",
																											"data": [
																												"DayOffName__"
																											],
																											"options": {
																												"label": "_DayOffName",
																												"minwidth": "350",
																												"version": 0
																											},
																											"stamp": 69,
																											"position": [
																												"_ShortText"
																											]
																										}
																									}
																								},
																								"Cell2": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": "100",
																										"version": 0
																									},
																									"stamp": 72,
																									"data": [],
																									"*": {
																										"DayOffStart__": {
																											"type": "Label",
																											"data": [
																												"DayOffStart__"
																											],
																											"options": {
																												"label": "_DayOffStart",
																												"minwidth": "100",
																												"version": 0
																											},
																											"stamp": 73,
																											"position": [
																												"_DateTime"
																											]
																										}
																									}
																								},
																								"Cell3": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": "100",
																										"version": 0
																									},
																									"stamp": 76,
																									"data": [],
																									"*": {
																										"DayOffEnd__": {
																											"type": "Label",
																											"data": [
																												"DayOffEnd__"
																											],
																											"options": {
																												"label": "_DayOffEnd",
																												"minwidth": "100",
																												"version": 0
																											},
																											"stamp": 77,
																											"position": [
																												"_DateTime2"
																											]
																										}
																									}
																								},
																								"Cell4": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": 140,
																										"version": 0
																									},
																									"stamp": 84,
																									"data": [],
																									"*": {
																										"DayOffRecurrence__": {
																											"type": "Label",
																											"data": [
																												"DayOffRecurrence__"
																											],
																											"options": {
																												"label": "_DayOffRecurrence",
																												"minwidth": 140,
																												"version": 0
																											},
																											"stamp": 85,
																											"position": [
																												"_ComboBox"
																											]
																										}
																									}
																								},
																								"Cell5": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": "80",
																										"version": 0
																									},
																									"stamp": 88,
																									"data": [],
																									"*": {
																										"DayOffDetails__": {
																											"type": "Label",
																											"data": [
																												"Details__"
																											],
																											"options": {
																												"label": "",
																												"minwidth": "80",
																												"version": 0
																											},
																											"stamp": 89,
																											"position": [
																												"_Button"
																											]
																										}
																									}
																								}
																							}
																						},
																						"Clipboard": {
																							"type": "$TableRow",
																							"subtype": "Clipboard",
																							"options": {
																								"version": 0
																							},
																							"stamp": 66,
																							"data": [
																								null
																							],
																							"*": {
																								"Cell1": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 67,
																									"data": [],
																									"*": {
																										"DayOffName__": {
																											"type": "ShortText",
																											"data": [
																												"DayOffName__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"label": "_DayOffName",
																												"activable": true,
																												"width": "350",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"helpIconPosition": "Right",
																												"browsable": false
																											},
																											"stamp": 70,
																											"position": [
																												"_ShortText"
																											]
																										}
																									}
																								},
																								"Cell2": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 71,
																									"data": [],
																									"*": {
																										"DayOffStart__": {
																											"type": "DateTime",
																											"data": [
																												"DayOffStart__"
																											],
																											"options": {
																												"displayLongFormat": false,
																												"label": "_DayOffStart",
																												"activable": true,
																												"width": "100",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"helpIconPosition": "Right",
																												"version": 0
																											},
																											"stamp": 74,
																											"position": [
																												"_DateTime"
																											]
																										}
																									}
																								},
																								"Cell3": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 75,
																									"data": [],
																									"*": {
																										"DayOffEnd__": {
																											"type": "DateTime",
																											"data": [
																												"DayOffEnd__"
																											],
																											"options": {
																												"displayLongFormat": false,
																												"label": "_DayOffEnd",
																												"activable": true,
																												"width": "100",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"helpIconPosition": "Right",
																												"readonly": true,
																												"version": 0
																											},
																											"stamp": 78,
																											"position": [
																												"_DateTime2"
																											]
																										}
																									}
																								},
																								"Cell4": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 83,
																									"data": [],
																									"*": {
																										"DayOffRecurrence__": {
																											"type": "ComboBox",
																											"data": [
																												"DayOffRecurrence__"
																											],
																											"options": {
																												"possibleValues": {
																													"0": "_Daily",
																													"1": "_Weekly",
																													"2": "_Monthly",
																													"3": "_Yearly"
																												},
																												"maxNbLinesToDisplay": 10,
																												"version": 1,
																												"keyValueMode": true,
																												"possibleKeys": {
																													"0": "Daily",
																													"1": "Weekly",
																													"2": "Monthly",
																													"3": "Yearly"
																												},
																												"label": "_DayOffRecurrence",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"helpIconPosition": "Right",
																												"readonly": true,
																												"DataSourceOrigin": "Standalone",
																												"DisplayedColumns": ""
																											},
																											"stamp": 86,
																											"position": [
																												"_ComboBox"
																											]
																										}
																									}
																								},
																								"Cell5": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 87,
																									"data": [],
																									"*": {
																										"DayOffDetails__": {
																											"type": "FormButton",
																											"data": [
																												"Details__"
																											],
																											"options": {
																												"buttonLabel": "_Details",
																												"label": "",
																												"width": "80",
																												"textPosition": "text-right",
																												"textSize": "MS",
																												"textStyle": "default",
																												"textColor": "default",
																												"nextprocess": {
																													"processName": "AP - Application Settings Wizard",
																													"attachmentsMode": "all",
																													"willBeChild": true,
																													"returnToOriginalUrl": false
																												},
																												"urlImageOverlay": "",
																												"action": "none",
																												"url": "",
																												"version": 0
																											},
																											"stamp": 90,
																											"position": [
																												"_Button"
																											]
																										}
																									}
																								}
																							}
																						}
																					}
																				},
																				"LabeljsonDaysOff__": {
																					"type": "Label",
																					"data": [
																						"jsonDaysOff__"
																					],
																					"options": {
																						"label": "_jsonDaysOff",
																						"hidden": true,
																						"version": 0
																					},
																					"stamp": 229
																				},
																				"jsonDaysOff__": {
																					"type": "LongText",
																					"data": [
																						"jsonDaysOff__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"minNbLines": 5,
																						"label": "_jsonDaysOff",
																						"activable": true,
																						"width": "800",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"helpIconPosition": "Right",
																						"numberOfLines": 20,
																						"browsable": false,
																						"hidden": true
																					},
																					"stamp": 230
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												}
											},
											"stamp": 12,
											"data": []
										},
										"form-content-right": {
											"type": "FlexibleFormSplitter",
											"options": {
												"version": 1,
												"box": {
													"top": 0,
													"left": 45,
													"width": 55,
													"height": 100
												},
												"hidden": true,
												"hideSeparator": true
											},
											"*": {
												"form-content-right-1": {
													"type": "FlexibleFormLine",
													"options": {
														"version": 0
													},
													"*": {
														"PreviewPanel": {
															"type": "PanelPreview",
															"options": {
																"border": {},
																"version": 0,
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": false,
																"hideTitleBar": false,
																"hideTitle": false,
																"sameHeightAsSiblings": false,
																"label": "_Document Preview"
															},
															"*": {
																"Preview": {
																	"data": [
																		"previewdata"
																	],
																	"type": "Preview",
																	"options": {
																		"version": 0
																	},
																	"stamp": 24
																}
															},
															"stamp": 23,
															"data": []
														}
													},
													"stamp": 22,
													"data": []
												}
											},
											"stamp": 21,
											"data": []
										}
									},
									"stamp": 11,
									"data": []
								}
							},
							"stamp": 6,
							"data": []
						}
					},
					"stamp": 5,
					"data": []
				},
				"form-footer": {
					"type": "FlexibleFormFooter",
					"options": {
						"splitterType": "simple-h",
						"version": 0,
						"maxwidth": 1024
					},
					"*": {
						"Save": {
							"type": "SubmitButton",
							"options": {
								"label": "_Save",
								"action": "approve",
								"submit": true,
								"version": 0,
								"textPosition": "text-right",
								"textSize": "MS",
								"textStyle": "default",
								"textColor": "default",
								"nextprocess": {
									"processName": "AP - Application Settings Wizard",
									"attachmentsMode": "all",
									"willBeChild": true,
									"returnToOriginalUrl": false
								},
								"urlImageOverlay": "",
								"url": "",
								"position": null
							},
							"stamp": 26,
							"data": []
						},
						"Close": {
							"type": "SubmitButton",
							"options": {
								"label": "_Close",
								"action": "cancel",
								"submit": true,
								"version": 0
							},
							"stamp": 30,
							"data": []
						}
					},
					"stamp": 25,
					"data": []
				}
			},
			"stamp": 3,
			"data": []
		}
	},
	"stamps": 230,
	"data": []
}