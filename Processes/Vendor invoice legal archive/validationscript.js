function actionTryFiscalArchiving()
{
	Variable.SetValueAsString("ArchiveProvider", Data.GetValue("ArchiveProvider"));
	if (Sys.Parameters.GetInstance("AP").GetParameter("isArkhineoConf"))
	{
		//Do not execute old fiscal archiving action
		Data.SetValue("ArchiveProvider", "");
		Data.SetValue("ArchiveProviderSolution", "");
		Variable.SetValueAsString("ArchiveProvider", "");
		var vault = {
			user: Sys.Parameters.GetInstance("AP").GetParameter("userArkhineo"),
			password: Sys.Parameters.GetInstance("AP").GetParameter("passwordArkhineo"),
			cfeId: Sys.Parameters.GetInstance("AP").GetParameter("cfeId"),
			sectionId: Sys.Parameters.GetInstance("AP").GetParameter("sectionId")
		};
		var ArkhineoMetaData = {
			applicative: Variable.GetValueAsString("ArkhineoMetaDataApplicative") ? JSON.parse(Variable.GetValueAsString("ArkhineoMetaDataApplicative")) : null,
			descriptive: Variable.GetValueAsString("ArkhineoMetaDataDescriptive") ? JSON.parse(Variable.GetValueAsString("ArkhineoMetaDataDescriptive")) : null
		};
		var httpResult = Attach.ArchiveToArkhineo(0, vault, ArkhineoMetaData);
		var lastStatus = httpResult.status;
		var lastMessage = httpResult.lastMessage;
		Data.SetValue("ArchivingStatus__", lastMessage);
		if (lastStatus !== 201)
		{
			Data.SetValue("ArchiveUniqueIdentifier__", "");
			Data.SetValue("LinkToArchive__", "");
			var errorMessage = "Arkhineo WS call status: " + lastStatus + " and error: " + lastMessage;
			//ValidationScriptAllowRetry = true
			Data.SetValue("State", 200);
			Data.SetValue("StatusCode", 200);
			Data.SetValue("ShortStatusTranslated", lastMessage);
			Data.SetValue("ShortStatus", lastMessage);
			Data.SetValue("LongStatus", errorMessage);
			Data.SetValue("CompletionDateTime", new Date());
		}
		else
		{
			var xmlContent = httpResult.responseData;
			var dom = Process.CreateXMLDOMElement(xmlContent);
			var arkhineoIdentifier = dom.getAttribute("archive-id");
			Data.SetValue("ArchiveUniqueIdentifier__", arkhineoIdentifier);
			Data.SetValue("LinkToArchive__", "https://pwd.arkhineo.fr/cfes/archives/" + arkhineoIdentifier);
		}
	}
	else
	{
		Log.Info("No custom arkhineo vault: Old method.");
	}
}
/** ******** **/
/** RUN PART **/
/** ******** **/
function executeRequestedAction()
{
	if (!Data.GetValue("ArchiveUniqueIdentifier__"))
	{
		var currentName = Process.AutoValidatingOnExpiration() ? "onexpiration" : Data.GetActionName();
		var currentAction = Data.GetActionType();
		Log.Info("Action: " + (currentAction ? currentAction : "<empty>") + ", Name: " + (currentName ? currentName : "<empty>") + ", Device: " + Data.GetActionDevice());
		/* Action map to specify what to do according to the Action Name. The validation, if needed, will always be executed synchronously.
		    The action in itself can be executed asynchronously.
		key: action name given by Data.GetActionName(). The empty string key means that no user action was ever performed on this form (submission)
		value : object
		- execute: function to call
		- requireValidation: set to true if you want the action to validate data
		- isAsync: set to true if the action should be executing asynchronously
		- leaveForm (only if isAsync is false): set to true if the form should be closed at the end of the action
		- keepScheduledActionParameters : set to true if you want to keep the values of the fields ScheduledActionDate and ScheduledAction after the validation action.
		*/
		var approveActionMap = {};
		approveActionMap[""] = {
			"execute": actionTryFiscalArchiving,
			"requireValidation": false,
			"leaveForm": false
		};
		var action = approveActionMap[currentName.toLowerCase()];
		if (!action)
		{
			action = approveActionMap[""];
		}
		// Execute action now
		action.execute();
		Process.LeaveForm();
	}
}
executeRequestedAction();