{
	"*": {
		"GDRData": {
			"type": "GDRData",
			"options": {
				"version": 0
			},
			"stamp": 1,
			"data": []
		},
		"OCRParams": {
			"type": "OCRParams",
			"options": {
				"version": 0
			},
			"stamp": 2,
			"data": []
		},
		"Screen": {
			"type": "FlexibleFormLayout",
			"options": {
				"splitterType": "complex",
				"version": 0,
				"maxwidth": 1024,
				"backColor": "text-backgroundcolor-default",
				"style": null
			},
			"*": {
				"form-header": {
					"type": "FlexibleFormHeader",
					"options": {
						"version": 0,
						"maxwidth": 1024
					},
					"stamp": 3,
					"data": []
				},
				"form-center": {
					"type": "FlexibleFormCenter",
					"options": {
						"version": 0,
						"maxwidth": 1024
					},
					"*": {
						"form-content": {
							"type": "FlexibleFormContent",
							"options": {
								"version": 0
							},
							"*": {
								"form-content-top": {
									"type": "FlexibleFormSplitter",
									"options": {
										"autosize": true,
										"version": 1,
										"box": {
											"top": 0,
											"left": 0,
											"width": 100,
											"height": 25
										},
										"hidden": true
									},
									"*": {
										"form-content-top-1": {
											"type": "FlexibleFormLine",
											"options": {
												"version": 0
											},
											"*": {
												"DocumentsPanel": {
													"type": "PanelDocument",
													"options": {
														"version": 0,
														"border": {},
														"labelLength": 0,
														"iconURL": "",
														"TitleFontSize": "S",
														"hideActionButtons": false,
														"hideTitleBar": false,
														"hideTitle": false,
														"label": "_Documents"
													},
													"*": {
														"Document": {
															"data": [
																"documentscontroldata"
															],
															"type": "Documents",
															"options": {
																"version": 0
															},
															"stamp": 4
														}
													},
													"stamp": 5,
													"data": []
												}
											},
											"stamp": 6,
											"data": []
										}
									},
									"stamp": 7,
									"data": []
								},
								"form-content-bottom": {
									"type": "FlexibleFormSplitter",
									"options": {
										"version": 1,
										"box": {
											"top": 0,
											"left": 0,
											"width": 100,
											"height": 100
										},
										"hidden": false,
										"hideSeparator": true
									},
									"*": {
										"form-content-left": {
											"type": "FlexibleFormSplitter",
											"options": {
												"version": 1,
												"box": {
													"top": "0%",
													"left": "0%",
													"width": "100%",
													"height": null
												},
												"hidden": false
											},
											"*": {
												"form-content-left-2": {
													"type": "FlexibleFormLine",
													"options": {
														"version": 0
													},
													"*": {
														"SystemData": {
															"type": "PanelSystemData",
															"options": {
																"multicolumn": false,
																"version": 0,
																"border": {},
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": false,
																"hideTitleBar": false,
																"hideTitle": false,
																"label": "_System data",
																"hidden": true
															},
															"stamp": 8,
															"data": []
														}
													},
													"stamp": 9,
													"data": []
												},
												"form-content-left-3": {
													"type": "FlexibleFormLine",
													"options": {
														"version": 0
													},
													"*": {
														"NextProcess": {
															"type": "PanelNextProcess",
															"options": {
																"hidden": true,
																"version": 0,
																"border": {},
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": false,
																"hideTitleBar": false,
																"hideTitle": false,
																"label": "_Next Process"
															},
															"stamp": 10,
															"data": []
														}
													},
													"stamp": 11,
													"data": []
												},
												"form-content-left-1": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 12,
													"*": {
														"TitlePane": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": true,
																"hideTitleBar": true,
																"hideTitle": true,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": " ",
																"leftImageURL": "",
																"removeMargins": false,
																"version": 0,
																"hidden": false
															},
															"stamp": 13,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {},
																		"version": 0
																	},
																	"stamp": 14,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {},
																				"lines": 0,
																				"columns": 2,
																				"colspans": [],
																				"version": 0
																			},
																			"stamp": 15,
																			"*": {}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-6": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 16,
													"*": {
														"Consignment_Details": {
															"type": "PanelData",
															"options": {
																"border": {},
																"version": 0,
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": true,
																"hideTitleBar": false,
																"hideTitle": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "_Invoice Details",
																"hidden": false,
																"leftImageURL": "",
																"removeMargins": false,
																"elementsAlignment": "left"
															},
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"options": {
																		"controlsAssociation": {
																			"InvoiceCurrency__": "LabelCurrency__",
																			"LabelCurrency__": "InvoiceCurrency__",
																			"InvoiceAmount__": "LabelAmount__",
																			"LabelAmount__": "InvoiceAmount__",
																			"VendorNumber__": "LabelVendorNumber__",
																			"LabelVendorNumber__": "VendorNumber__",
																			"CompanyCode__": "LabelCompanyCode__",
																			"LabelCompanyCode__": "CompanyCode__",
																			"InvoiceDate__": "LabelInvoiceDate__",
																			"LabelInvoiceDate__": "InvoiceDate__",
																			"PostingDate__": "LabelPostingDate__",
																			"LabelPostingDate__": "PostingDate__",
																			"Balance__": "LabelBalance__",
																			"LabelBalance__": "Balance__",
																			"InvoiceNetAmount__": "LabelInvoiceNetAmount__",
																			"LabelInvoiceNetAmount__": "InvoiceNetAmount__"
																		},
																		"version": 0
																	},
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"options": {
																				"lines": 8,
																				"columns": 2,
																				"version": 0,
																				"ctrlsPos": {
																					"InvoiceCurrency__": {
																						"line": 3,
																						"column": 2
																					},
																					"LabelCurrency__": {
																						"line": 3,
																						"column": 1
																					},
																					"InvoiceAmount__": {
																						"line": 5,
																						"column": 2
																					},
																					"LabelAmount__": {
																						"line": 5,
																						"column": 1
																					},
																					"VendorNumber__": {
																						"line": 1,
																						"column": 2
																					},
																					"LabelVendorNumber__": {
																						"line": 1,
																						"column": 1
																					},
																					"CompanyCode__": {
																						"line": 2,
																						"column": 2
																					},
																					"LabelCompanyCode__": {
																						"line": 2,
																						"column": 1
																					},
																					"InvoiceDate__": {
																						"line": 6,
																						"column": 2
																					},
																					"LabelInvoiceDate__": {
																						"line": 6,
																						"column": 1
																					},
																					"PostingDate__": {
																						"line": 7,
																						"column": 2
																					},
																					"LabelPostingDate__": {
																						"line": 7,
																						"column": 1
																					},
																					"Balance__": {
																						"line": 8,
																						"column": 2
																					},
																					"LabelBalance__": {
																						"line": 8,
																						"column": 1
																					},
																					"InvoiceNetAmount__": {
																						"line": 4,
																						"column": 2
																					},
																					"LabelInvoiceNetAmount__": {
																						"line": 4,
																						"column": 1
																					}
																				},
																				"colspans": [
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[]
																				]
																			},
																			"data": [
																				"fields"
																			],
																			"*": {
																				"LabelVendorNumber__": {
																					"type": "Label",
																					"data": [
																						"VendorNumber__"
																					],
																					"options": {
																						"label": "_Vendor number",
																						"version": 0
																					},
																					"stamp": 17
																				},
																				"VendorNumber__": {
																					"type": "ShortText",
																					"data": [
																						"VendorNumber__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"label": "_Vendor number",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"readonly": true,
																						"browsable": false
																					},
																					"stamp": 18
																				},
																				"LabelCompanyCode__": {
																					"type": "Label",
																					"data": [
																						"CompanyCode__"
																					],
																					"options": {
																						"label": "_Company code",
																						"version": 0
																					},
																					"stamp": 19
																				},
																				"CompanyCode__": {
																					"type": "ShortText",
																					"data": [
																						"CompanyCode__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"label": "_Company code",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"readonly": true,
																						"browsable": false
																					},
																					"stamp": 20
																				},
																				"LabelCurrency__": {
																					"type": "Label",
																					"data": [
																						"InvoiceCurrency__"
																					],
																					"options": {
																						"label": "_Clearing document currency",
																						"version": 0
																					},
																					"stamp": 21
																				},
																				"InvoiceCurrency__": {
																					"type": "ShortText",
																					"data": [
																						"InvoiceCurrency__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"label": "_Clearing document currency",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"readonly": true,
																						"browsable": false
																					},
																					"stamp": 22
																				},
																				"LabelInvoiceNetAmount__": {
																					"type": "Label",
																					"data": [
																						"InvoiceNetAmount__"
																					],
																					"options": {
																						"label": "_InvoiceNetAmount",
																						"version": 0
																					},
																					"stamp": 145
																				},
																				"InvoiceNetAmount__": {
																					"type": "Decimal",
																					"data": [
																						"InvoiceNetAmount__"
																					],
																					"options": {
																						"version": 2,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"integer": false,
																						"label": "_InvoiceNetAmount",
																						"precision_internal": 2,
																						"precision_current": 2,
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"enablePlusMinus": false,
																						"readonly": true
																					},
																					"stamp": 146
																				},
																				"LabelAmount__": {
																					"type": "Label",
																					"data": [
																						"InvoiceAmount__"
																					],
																					"options": {
																						"label": "_Clearing document amount",
																						"version": 0
																					},
																					"stamp": 23
																				},
																				"InvoiceAmount__": {
																					"type": "Decimal",
																					"data": [
																						"InvoiceAmount__"
																					],
																					"options": {
																						"version": 2,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"integer": false,
																						"label": "_Clearing document amount",
																						"precision_internal": 2,
																						"precision_current": 2,
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"readonly": true
																					},
																					"stamp": 24
																				},
																				"LabelInvoiceDate__": {
																					"type": "Label",
																					"data": [
																						"InvoiceDate__"
																					],
																					"options": {
																						"label": "_Clearing document date",
																						"hidden": true,
																						"version": 0
																					},
																					"stamp": 25
																				},
																				"InvoiceDate__": {
																					"type": "DateTime",
																					"data": [
																						"InvoiceDate__"
																					],
																					"options": {
																						"displayLongFormat": false,
																						"label": "_Clearing document date",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"readonly": true,
																						"hidden": true,
																						"version": 0
																					},
																					"stamp": 26
																				},
																				"LabelPostingDate__": {
																					"type": "Label",
																					"data": [
																						"PostingDate__"
																					],
																					"options": {
																						"label": "_Posting date",
																						"version": 0
																					},
																					"stamp": 27
																				},
																				"PostingDate__": {
																					"type": "DateTime",
																					"data": [
																						"PostingDate__"
																					],
																					"options": {
																						"displayLongFormat": false,
																						"label": "_Posting date",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"readonly": true,
																						"version": 0
																					},
																					"stamp": 28
																				},
																				"LabelBalance__": {
																					"type": "Label",
																					"data": [
																						"Balance__"
																					],
																					"options": {
																						"label": "_Balance",
																						"version": 0
																					},
																					"stamp": 29
																				},
																				"Balance__": {
																					"type": "Decimal",
																					"data": [
																						"Balance__"
																					],
																					"options": {
																						"version": 2,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"integer": false,
																						"label": "_Balance",
																						"precision_internal": 2,
																						"precision_current": 2,
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"readonly": true
																					},
																					"stamp": 30
																				}
																			},
																			"stamp": 31
																		}
																	},
																	"stamp": 32,
																	"data": []
																}
															},
															"stamp": 33,
															"data": []
														},
														"ERP_Details": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": true,
																"hideTitleBar": false,
																"hideTitle": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "_ERP Details",
																"hidden": false,
																"leftImageURL": "",
																"removeMargins": false,
																"elementsAlignment": "left",
																"version": 0
															},
															"stamp": 34,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"ERPInvoiceNumber__": "LabelERPInvoiceNumber__",
																			"LabelERPInvoiceNumber__": "ERPInvoiceNumber__",
																			"LabelAssignedAmount__": "AssignedAmount__",
																			"AssignedAmount__": "LabelAssignedAmount__",
																			"ShortText__": "Label_ShortText",
																			"Label_ShortText": "ShortText__",
																			"SAPCurrency___label__": "SAPCurrency__",
																			"SAPCurrency__": "SAPCurrency___label__"
																		},
																		"version": 0
																	},
																	"stamp": 35,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"ERPInvoiceNumber__": {
																						"line": 1,
																						"column": 2
																					},
																					"LabelERPInvoiceNumber__": {
																						"line": 1,
																						"column": 1
																					},
																					"LabelAssignedAmount__": {
																						"line": 2,
																						"column": 1
																					},
																					"AssignedAmount__": {
																						"line": 2,
																						"column": 2
																					},
																					"SAPCurrency___label__": {
																						"line": 3,
																						"column": 1
																					},
																					"SAPCurrency__": {
																						"line": 3,
																						"column": 2
																					}
																				},
																				"lines": 3,
																				"columns": 2,
																				"colspans": [
																					[],
																					[],
																					[]
																				],
																				"version": 0
																			},
																			"stamp": 36,
																			"*": {
																				"SAPCurrency__": {
																					"type": "ShortText",
																					"data": [
																						"SAPCurrency__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"label": "_SAPCurrency",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"readonly": true,
																						"browsable": false
																					},
																					"stamp": 158
																				},
																				"SAPCurrency___label__": {
																					"type": "Label",
																					"data": [
																						"SAPCurrency__"
																					],
																					"options": {
																						"label": "_SAPCurrency"
																					},
																					"stamp": 157
																				},
																				"AssignedAmount__": {
																					"type": "Decimal",
																					"data": [
																						"AssignedAmount__"
																					],
																					"options": {
																						"version": 2,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"integer": false,
																						"label": "_AssignedAmount",
																						"precision_internal": 2,
																						"precision_current": 2,
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"readonly": true
																					},
																					"stamp": 37
																				},
																				"LabelAssignedAmount__": {
																					"type": "Label",
																					"data": [
																						"AssignedAmount__"
																					],
																					"options": {
																						"label": "_AssignedAmount",
																						"version": 0
																					},
																					"stamp": 38
																				},
																				"LabelERPInvoiceNumber__": {
																					"type": "Label",
																					"data": [
																						"ERPInvoiceNumber__"
																					],
																					"options": {
																						"label": "_ERP Clearing document number",
																						"version": 0
																					},
																					"stamp": 39
																				},
																				"ERPInvoiceNumber__": {
																					"type": "ShortText",
																					"data": [
																						"ERPInvoiceNumber__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"label": "_ERP Clearing document number",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"browsable": false,
																						"readonly": true
																					},
																					"stamp": 40
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-4": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 41,
													"*": {
														"ClearInvoicesPane": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": "400",
																"iconURL": "AP_WorkflowBacktoAP.png",
																"TitleFontSize": "S",
																"hideActionButtons": true,
																"hideTitleBar": false,
																"hideTitle": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "_ClearInvoicesPane",
																"leftImageURL": "",
																"removeMargins": false,
																"version": 0,
																"elementsAlignment": "left"
															},
															"stamp": 42,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {},
																		"version": 0
																	},
																	"stamp": 43,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"InvoicesToClearCount__": {
																						"line": 1,
																						"column": 1
																					},
																					"Spacer_line__": {
																						"line": 3,
																						"column": 1
																					},
																					"Spacer_line2__": {
																						"line": 4,
																						"column": 1
																					},
																					"LineItems__": {
																						"line": 2,
																						"column": 1
																					}
																				},
																				"lines": 4,
																				"columns": 2,
																				"colspans": [
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					]
																				],
																				"version": 0
																			},
																			"stamp": 44,
																			"*": {
																				"InvoicesToClearCount__": {
																					"type": "Text",
																					"data": false,
																					"options": {
																						"marge": 0,
																						"textSize": "S",
																						"textAlignment": "center",
																						"textStyle": "bold",
																						"textColor": "default",
																						"backgroundcolor": "default",
																						"label": "No invoice selected",
																						"version": 0,
																						"iconClass": ""
																					},
																					"stamp": 45
																				},
																				"LineItems__": {
																					"type": "Table",
																					"data": [
																						"LineItems__"
																					],
																					"options": {
																						"version": 5,
																						"lines": 10,
																						"columns": 10,
																						"displayEmptyLines": false,
																						"alwaysOneLine": true,
																						"menuOptions": {
																							"disableInsertLine": true,
																							"disableAddLine": true,
																							"hideTopNavigation": true,
																							"hideBottomNavigation": false,
																							"hideTableRowMenu": true,
																							"hideTableRowDelete": true,
																							"hideTableRowAdd": true,
																							"hideTableRowDuplicate": true,
																							"addButtonShouldInsert": true
																						},
																						"item": {
																							"display": null
																						},
																						"group": {
																							"name": "GDR\nPage"
																						},
																						"label": "_Line items",
																						"subsection": null,
																						"readonly": true
																					},
																					"stamp": 46,
																					"*": {
																						"Actions": {
																							"type": "$TableActions",
																							"subtype": "Actions",
																							"options": {
																								"version": 0
																							},
																							"stamp": 47,
																							"*": {
																								"Top": {
																									"type": "$TableActions",
																									"subtype": "Top",
																									"options": {
																										"version": 0
																									},
																									"stamp": 48
																								},
																								"Bottom": {
																									"type": "$TableActions",
																									"subtype": "Top",
																									"options": {
																										"version": 0
																									},
																									"stamp": 49
																								}
																							}
																						},
																						"Header": {
																							"type": "$TableRow",
																							"subtype": "Header",
																							"options": {
																								"version": 0
																							},
																							"stamp": 50,
																							"data": [
																								null
																							],
																							"*": {
																								"Cell1": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 51,
																									"data": [],
																									"*": {
																										"ValidationURL__": {
																											"type": "Label",
																											"data": [
																												"ValidationURL__"
																											],
																											"options": {
																												"label": "_ValidationURL",
																												"version": 0
																											},
																											"stamp": 52,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell2": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 53,
																									"data": [],
																									"*": {
																										"LineType__": {
																											"type": "Label",
																											"data": [
																												"LineType__"
																											],
																											"options": {
																												"label": "_Line item type",
																												"version": 0
																											},
																											"stamp": 54,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell3": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"minwidth": "220"
																									},
																									"stamp": 55,
																									"data": [],
																									"*": {
																										"DocumentNumber__": {
																											"type": "Label",
																											"data": [
																												"DocumentNumber__"
																											],
																											"options": {
																												"label": "_Line item document number",
																												"version": 0,
																												"minwidth": "220"
																											},
																											"stamp": 56,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell4": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"minwidth": "120"
																									},
																									"stamp": 148,
																									"data": [],
																									"*": {
																										"NetAmount__": {
																											"type": "Label",
																											"data": [
																												"NetAmount__"
																											],
																											"options": {
																												"label": "_Line item amount excluding tax",
																												"version": 0,
																												"minwidth": "120"
																											},
																											"stamp": 149,
																											"position": [
																												"_Decimal"
																											]
																										}
																									}
																								},
																								"Cell5": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"minwidth": "120"
																									},
																									"stamp": 57,
																									"data": [],
																									"*": {
																										"Amount__": {
																											"type": "Label",
																											"data": [
																												"Amount__"
																											],
																											"options": {
																												"label": "_Line item amount",
																												"version": 0,
																												"minwidth": "120"
																											},
																											"stamp": 58,
																											"position": [
																												"Decimal number"
																											]
																										}
																									}
																								},
																								"Cell6": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"minwidth": "100"
																									},
																									"stamp": 59,
																									"data": [],
																									"*": {
																										"TaxAmount__": {
																											"type": "Label",
																											"data": [
																												"TaxAmount__"
																											],
																											"options": {
																												"label": "_Line item tax amount",
																												"version": 0,
																												"minwidth": "100"
																											},
																											"stamp": 60,
																											"position": [
																												"Decimal number"
																											]
																										}
																									}
																								},
																								"Cell7": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"minwidth": "80"
																									},
																									"stamp": 152,
																									"data": [],
																									"*": {
																										"Currency__": {
																											"type": "Label",
																											"data": [
																												"Currency__"
																											],
																											"options": {
																												"label": "_Line item currency",
																												"version": 0,
																												"minwidth": "80"
																											},
																											"stamp": 153,
																											"position": [
																												"_ShortText"
																											]
																										}
																									}
																								},
																								"Cell8": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": "80"
																									},
																									"stamp": 164,
																									"data": [],
																									"*": {
																										"InvoiceDate__": {
																											"type": "Label",
																											"data": [
																												"InvoiceDate__"
																											],
																											"options": {
																												"label": "_InvoiceDate",
																												"minwidth": "80"
																											},
																											"stamp": 165,
																											"position": [
																												"_DateTime"
																											]
																										}
																									}
																								},
																								"Cell9": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": "80"
																									},
																									"stamp": 172,
																									"data": [],
																									"*": {
																										"PostingDate__": {
																											"type": "Label",
																											"data": [
																												"PostingDate__"
																											],
																											"options": {
																												"label": "_PostingDate",
																												"minwidth": "80"
																											},
																											"stamp": 173,
																											"position": [
																												"_DateTime"
																											]
																										}
																									}
																								},
																								"Cell10": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": 140
																									},
																									"stamp": 168,
																									"data": [],
																									"*": {
																										"InvoiceDescription__": {
																											"type": "Label",
																											"data": [
																												"InvoiceDescription__"
																											],
																											"options": {
																												"label": "_InvoiceDescription",
																												"minwidth": 140
																											},
																											"stamp": 169,
																											"position": [
																												"_ShortText"
																											]
																										}
																									}
																								}
																							}
																						},
																						"Clipboard": {
																							"type": "$TableRow",
																							"subtype": "Clipboard",
																							"options": {
																								"version": 0
																							},
																							"stamp": 61,
																							"data": [
																								null
																							],
																							"*": {
																								"Cell1": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 62,
																									"data": [],
																									"*": {
																										"ValidationURL__": {
																											"type": "ShortText",
																											"data": [
																												"ValidationURL__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"label": "_ValidationURL",
																												"activable": true,
																												"width": "300",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"readonly": true,
																												"browsable": false,
																												"autocompletable": false
																											},
																											"stamp": 63,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell2": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 64,
																									"data": [],
																									"*": {
																										"LineType__": {
																											"type": "ShortText",
																											"data": [
																												"LineType__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"label": "_Line item type",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"browsable": false,
																												"readonly": true
																											},
																											"stamp": 65,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell3": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 66,
																									"data": [],
																									"*": {
																										"DocumentNumber__": {
																											"type": "ShortText",
																											"data": [
																												"DocumentNumber__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"label": "_Line item document number",
																												"activable": true,
																												"width": "220",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"readonly": true,
																												"browsable": false
																											},
																											"stamp": 67,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell4": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 147,
																									"data": [],
																									"*": {
																										"NetAmount__": {
																											"type": "Decimal",
																											"data": [
																												"NetAmount__"
																											],
																											"options": {
																												"version": 2,
																												"textSize": "S",
																												"textAlignment": "right",
																												"textStyle": "default",
																												"textColor": "default",
																												"integer": false,
																												"label": "_Line item amount excluding tax",
																												"precision_internal": 2,
																												"precision_current": 2,
																												"activable": true,
																												"width": "110",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"enablePlusMinus": false,
																												"readonly": true
																											},
																											"stamp": 150,
																											"position": [
																												"_Decimal"
																											]
																										}
																									}
																								},
																								"Cell5": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 68,
																									"data": [],
																									"*": {
																										"Amount__": {
																											"type": "Decimal",
																											"data": [
																												"Amount__"
																											],
																											"options": {
																												"version": 2,
																												"textSize": "S",
																												"textAlignment": "right",
																												"textStyle": "default",
																												"textColor": "default",
																												"integer": false,
																												"label": "_Line item amount",
																												"precision_internal": 2,
																												"precision_current": 2,
																												"activable": true,
																												"width": "110",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"readonly": true
																											},
																											"stamp": 69,
																											"position": [
																												"Decimal number"
																											]
																										}
																									}
																								},
																								"Cell6": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 70,
																									"data": [],
																									"*": {
																										"TaxAmount__": {
																											"type": "Decimal",
																											"data": [
																												"TaxAmount__"
																											],
																											"options": {
																												"version": 2,
																												"textSize": "S",
																												"textAlignment": "right",
																												"textStyle": "default",
																												"textColor": "default",
																												"integer": false,
																												"label": "_Line item tax amount",
																												"precision_internal": 2,
																												"precision_current": 2,
																												"activable": true,
																												"width": "100",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"readonly": true
																											},
																											"stamp": 71,
																											"position": [
																												"Decimal number"
																											]
																										}
																									}
																								},
																								"Cell7": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 151,
																									"data": [],
																									"*": {
																										"Currency__": {
																											"type": "ShortText",
																											"data": [
																												"Currency__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"label": "_Line item currency",
																												"activable": true,
																												"width": "80",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"readonly": true,
																												"browsable": false
																											},
																											"stamp": 154,
																											"position": [
																												"_ShortText"
																											]
																										}
																									}
																								},
																								"Cell8": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {},
																									"stamp": 163,
																									"data": [],
																									"*": {
																										"InvoiceDate__": {
																											"type": "DateTime",
																											"data": [
																												"InvoiceDate__"
																											],
																											"options": {
																												"displayLongFormat": false,
																												"label": "_InvoiceDate",
																												"activable": true,
																												"width": "80",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"readonly": true
																											},
																											"stamp": 166,
																											"position": [
																												"_DateTime"
																											]
																										}
																									}
																								},
																								"Cell9": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {},
																									"stamp": 171,
																									"data": [],
																									"*": {
																										"PostingDate__": {
																											"type": "DateTime",
																											"data": [
																												"PostingDate__"
																											],
																											"options": {
																												"displayLongFormat": false,
																												"label": "_PostingDate",
																												"activable": true,
																												"width": "80",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"readonly": true
																											},
																											"stamp": 174,
																											"position": [
																												"_DateTime"
																											]
																										}
																									}
																								},
																								"Cell10": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {},
																									"stamp": 167,
																									"data": [],
																									"*": {
																										"InvoiceDescription__": {
																											"type": "ShortText",
																											"data": [
																												"InvoiceDescription__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"readonly": true,
																												"label": "_InvoiceDescription",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"browsable": false
																											},
																											"stamp": 170,
																											"position": [
																												"_ShortText"
																											]
																										}
																									}
																								}
																							}
																						}
																					}
																				},
																				"Spacer_line__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "",
																						"width": "",
																						"color": "default",
																						"label": "Spacer line",
																						"version": 0
																					},
																					"stamp": 72
																				},
																				"Spacer_line2__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "",
																						"width": "",
																						"color": "default",
																						"label": "Spacer line2",
																						"version": 0
																					},
																					"stamp": 73
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-10": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 75,
													"*": {
														"GL_lines_adjustment_pane": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {
																	"collapsed": false
																},
																"labelLength": 0,
																"iconURL": "AP_WorkflowBacktoAP.png",
																"TitleFontSize": "S",
																"hideActionButtons": true,
																"hideTitleBar": false,
																"hideTitle": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "_GL lines adjustment pane",
																"hidden": false,
																"leftImageURL": "",
																"removeMargins": false,
																"elementsAlignment": "left",
																"version": 0
															},
															"stamp": 76,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {},
																		"version": 0
																	},
																	"stamp": 77,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"BalancingLineItems__": {
																						"line": 1,
																						"column": 1
																					}
																				},
																				"lines": 1,
																				"columns": 2,
																				"colspans": [
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					]
																				],
																				"version": 0
																			},
																			"stamp": 78,
																			"*": {
																				"BalancingLineItems__": {
																					"type": "Table",
																					"data": [
																						"BalancingLineItems__"
																					],
																					"options": {
																						"version": 5,
																						"lines": 10,
																						"columns": 10,
																						"displayEmptyLines": false,
																						"alwaysOneLine": true,
																						"menuOptions": {
																							"disableInsertLine": true,
																							"disableAddLine": true,
																							"hideTopNavigation": true,
																							"hideBottomNavigation": false,
																							"hideTableRowMenu": true,
																							"hideTableRowDelete": false,
																							"hideTableRowAdd": false,
																							"hideTableRowDuplicate": true,
																							"addButtonShouldInsert": false
																						},
																						"item": {
																							"display": null
																						},
																						"group": {
																							"name": "GDR\nPage"
																						},
																						"label": "_BalancingLineItems",
																						"subsection": null
																					},
																					"stamp": 79,
																					"*": {
																						"Actions": {
																							"type": "$TableActions",
																							"subtype": "Actions",
																							"options": {
																								"version": 0
																							},
																							"stamp": 80,
																							"*": {
																								"Top": {
																									"type": "$TableActions",
																									"subtype": "Top",
																									"options": {
																										"version": 0
																									},
																									"stamp": 81
																								},
																								"Bottom": {
																									"type": "$TableActions",
																									"subtype": "Top",
																									"options": {
																										"version": 0
																									},
																									"stamp": 82
																								}
																							}
																						},
																						"Header": {
																							"type": "$TableRow",
																							"subtype": "Header",
																							"options": {
																								"version": 0
																							},
																							"stamp": 83,
																							"data": [
																								null
																							],
																							"*": {
																								"Cell1": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 84,
																									"data": [],
																									"*": {
																										"LineType__": {
																											"type": "Label",
																											"data": [
																												"LineType__"
																											],
																											"options": {
																												"label": "_Adjustment line type",
																												"version": 0
																											},
																											"stamp": 85,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell2": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 86,
																									"data": [],
																									"*": {
																										"Description__": {
																											"type": "Label",
																											"data": [
																												"Description__"
																											],
																											"options": {
																												"label": "_Adjustment line description",
																												"version": 0
																											},
																											"stamp": 87,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell3": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 88,
																									"data": [],
																									"*": {
																										"Amount__": {
																											"type": "Label",
																											"data": [
																												"Amount__"
																											],
																											"options": {
																												"label": "_Adjustment line amount excluding tax",
																												"version": 0
																											},
																											"stamp": 89,
																											"position": [
																												"Decimal number"
																											]
																										}
																									}
																								},
																								"Cell4": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 90,
																									"data": [],
																									"*": {
																										"GLAccount__": {
																											"type": "Label",
																											"data": [
																												"GLAccount__"
																											],
																											"options": {
																												"label": "_Adjustment line G/L account",
																												"version": 0
																											},
																											"stamp": 91,
																											"position": [
																												"Select from a table"
																											]
																										}
																									}
																								},
																								"Cell5": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 92,
																									"data": [],
																									"*": {
																										"GLDescription__": {
																											"type": "Label",
																											"data": [
																												"GLDescription__"
																											],
																											"options": {
																												"label": "_Adjustment line G/L account description",
																												"version": 0
																											},
																											"stamp": 93,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell6": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 94,
																									"data": [],
																									"*": {
																										"CostCenter__": {
																											"type": "Label",
																											"data": [
																												"CostCenter__"
																											],
																											"options": {
																												"label": "_Adjustment line cost center",
																												"version": 0
																											},
																											"stamp": 95,
																											"position": [
																												"Select from a table"
																											]
																										}
																									}
																								},
																								"Cell7": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 96,
																									"data": [],
																									"*": {
																										"CCDescription__": {
																											"type": "Label",
																											"data": [
																												"CCDescription__"
																											],
																											"options": {
																												"label": "_Adjustment line cost center description",
																												"version": 0
																											},
																											"stamp": 97,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell8": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 98,
																									"data": [],
																									"*": {
																										"TaxCode__": {
																											"type": "Label",
																											"data": [
																												"TaxCode__"
																											],
																											"options": {
																												"label": "_Adjustment line tax code",
																												"version": 0
																											},
																											"stamp": 99,
																											"position": [
																												"Select from a table"
																											]
																										}
																									}
																								},
																								"Cell9": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 100,
																									"data": [],
																									"*": {
																										"TaxAmount__": {
																											"type": "Label",
																											"data": [
																												"TaxAmount__"
																											],
																											"options": {
																												"label": "_Adjustment line tax amount",
																												"version": 0
																											},
																											"stamp": 101,
																											"position": [
																												"Decimal number"
																											]
																										}
																									}
																								},
																								"Cell10": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 102,
																									"data": [],
																									"*": {
																										"TaxRate__": {
																											"type": "Label",
																											"data": [
																												"TaxRate__"
																											],
																											"options": {
																												"label": "_Adjustment line tax rate",
																												"version": 0
																											},
																											"stamp": 103
																										}
																									}
																								}
																							}
																						},
																						"Clipboard": {
																							"type": "$TableRow",
																							"subtype": "Clipboard",
																							"options": {
																								"version": 0
																							},
																							"stamp": 104,
																							"data": [
																								null
																							],
																							"*": {
																								"Cell1": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 105,
																									"data": [],
																									"*": {
																										"LineType__": {
																											"type": "ShortText",
																											"data": [
																												"LineType__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"label": "_Adjustment line type",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"browsable": false,
																												"readonly": true
																											},
																											"stamp": 106,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell2": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 107,
																									"data": [],
																									"*": {
																										"Description__": {
																											"type": "ShortText",
																											"data": [
																												"Description__"
																											],
																											"options": {
																												"label": "_Adjustment line description",
																												"activable": true,
																												"width": 140,
																												"version": 0,
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"browsable": false
																											},
																											"stamp": 108,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell3": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 109,
																									"data": [],
																									"*": {
																										"Amount__": {
																											"type": "Decimal",
																											"data": [
																												"Amount__"
																											],
																											"options": {
																												"version": 2,
																												"textSize": "S",
																												"textAlignment": "right",
																												"textStyle": "default",
																												"textColor": "default",
																												"integer": false,
																												"label": "_Adjustment line amount excluding tax",
																												"precision_internal": 2,
																												"precision_current": 2,
																												"activable": true,
																												"width": "80",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"readonly": false
																											},
																											"stamp": 110,
																											"position": [
																												"Decimal number"
																											]
																										}
																									}
																								},
																								"Cell4": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 111,
																									"data": [],
																									"*": {
																										"GLAccount__": {
																											"type": "DatabaseComboBox",
																											"data": [
																												"GLAccount__"
																											],
																											"options": {
																												"label": "_Adjustment line G/L account",
																												"activable": true,
																												"width": "80",
																												"fTSmaxRecords": "20",
																												"version": 0,
																												"PreFillFTS": true,
																												"browsable": true,
																												"RestrictSearch": true,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"minNbLines": 1,
																												"maxNbLines": 1,
																												"autoCompleteSearchMode": "startswith",
																												"searchMode": "contains"
																											},
																											"stamp": 112,
																											"position": [
																												"Select from a table"
																											]
																										}
																									}
																								},
																								"Cell5": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 113,
																									"data": [],
																									"*": {
																										"GLDescription__": {
																											"type": "ShortText",
																											"data": [
																												"GLDescription__"
																											],
																											"options": {
																												"label": "_Adjustment line G/L account description",
																												"activable": true,
																												"width": 140,
																												"readonly": true,
																												"browsable": false,
																												"version": 0,
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format"
																											},
																											"stamp": 114,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell6": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 115,
																									"data": [],
																									"*": {
																										"CostCenter__": {
																											"type": "DatabaseComboBox",
																											"data": [
																												"CostCenter__"
																											],
																											"options": {
																												"label": "_Adjustment line cost center",
																												"activable": true,
																												"width": "100",
																												"PreFillFTS": true,
																												"fTSmaxRecords": "20",
																												"version": 0,
																												"browsable": true,
																												"RestrictSearch": true,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"minNbLines": 1,
																												"maxNbLines": 1,
																												"autoCompleteSearchMode": "startswith",
																												"searchMode": "contains"
																											},
																											"stamp": 116,
																											"position": [
																												"Select from a table"
																											]
																										}
																									}
																								},
																								"Cell7": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 117,
																									"data": [],
																									"*": {
																										"CCDescription__": {
																											"type": "ShortText",
																											"data": [
																												"CCDescription__"
																											],
																											"options": {
																												"label": "_Adjustment line cost center description",
																												"activable": true,
																												"width": 140,
																												"readonly": true,
																												"browsable": false,
																												"version": 0,
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format"
																											},
																											"stamp": 118,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell8": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 119,
																									"data": [],
																									"*": {
																										"TaxCode__": {
																											"type": "DatabaseComboBox",
																											"data": [
																												"TaxCode__"
																											],
																											"options": {
																												"label": "_Adjustment line tax code",
																												"activable": true,
																												"width": "65",
																												"PreFillFTS": true,
																												"fTSmaxRecords": "20",
																												"version": 0,
																												"browsable": true,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"minNbLines": 1,
																												"maxNbLines": 1,
																												"autoCompleteSearchMode": "startswith",
																												"searchMode": "contains"
																											},
																											"stamp": 120,
																											"position": [
																												"Select from a table"
																											]
																										}
																									}
																								},
																								"Cell9": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 121,
																									"data": [],
																									"*": {
																										"TaxAmount__": {
																											"type": "Decimal",
																											"data": [
																												"TaxAmount__"
																											],
																											"options": {
																												"version": 2,
																												"textSize": "S",
																												"textAlignment": "right",
																												"textStyle": "default",
																												"textColor": "default",
																												"integer": false,
																												"label": "_Adjustment line tax amount",
																												"precision_internal": 2,
																												"precision_current": 2,
																												"activable": true,
																												"width": "80",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"readonly": true
																											},
																											"stamp": 122,
																											"position": [
																												"Decimal number"
																											]
																										}
																									}
																								},
																								"Cell10": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 123,
																									"data": [],
																									"*": {
																										"TaxRate__": {
																											"type": "Decimal",
																											"data": [
																												"TaxRate__"
																											],
																											"options": {
																												"integer": false,
																												"precision_internal": 2,
																												"label": "_Adjustment line tax rate",
																												"activable": true,
																												"width": 140,
																												"readonly": true,
																												"version": 2,
																												"textSize": "S",
																												"textAlignment": "right",
																												"textStyle": "default",
																												"textColor": "default",
																												"precision_current": 2,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format"
																											},
																											"stamp": 124
																										}
																									}
																								}
																							}
																						}
																					}
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-5": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 141,
													"*": {
														"Workflow_pane": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": false,
																"hideTitleBar": false,
																"hideTitle": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "_Workflow pane",
																"hidden": true,
																"leftImageURL": "",
																"removeMargins": false,
																"elementsAlignment": "left",
																"version": 0
															},
															"stamp": 142,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {},
																		"version": 0
																	},
																	"stamp": 143,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"ClearInvoicesComment__": {
																						"line": 1,
																						"column": 1
																					}
																				},
																				"lines": 1,
																				"columns": 2,
																				"colspans": [
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					]
																				],
																				"version": 0
																			},
																			"stamp": 144,
																			"*": {
																				"ClearInvoicesComment__": {
																					"type": "LongText",
																					"data": [
																						"ClearInvoicesComment__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"label": "_ClearInvoicesComment_report",
																						"activable": true,
																						"width": "700",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"browsable": false,
																						"version": 0,
																						"minNbLines": 10,
																						"numberOfLines": 10
																					},
																					"stamp": 74
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												}
											},
											"stamp": 125,
											"data": []
										},
										"form-content-right": {
											"type": "FlexibleFormSplitter",
											"options": {
												"version": 1,
												"box": {
													"top": 0,
													"left": 45,
													"width": 55,
													"height": 100
												},
												"hidden": true,
												"hideSeparator": true
											},
											"*": {
												"form-content-right-1": {
													"type": "FlexibleFormLine",
													"options": {
														"version": 0
													},
													"*": {
														"PreviewPanel": {
															"type": "PanelPreview",
															"options": {
																"border": {},
																"version": 0,
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": false,
																"hideTitleBar": false,
																"hideTitle": false,
																"label": "_Document Preview"
															},
															"*": {
																"Preview": {
																	"data": [
																		"previewdata"
																	],
																	"type": "Preview",
																	"options": {
																		"version": 0
																	},
																	"stamp": 126
																}
															},
															"stamp": 127,
															"data": []
														}
													},
													"stamp": 128,
													"data": []
												}
											},
											"stamp": 129,
											"data": []
										}
									},
									"stamp": 130,
									"data": []
								}
							},
							"stamp": 131,
							"data": []
						}
					},
					"stamp": 132,
					"data": []
				},
				"form-footer": {
					"type": "FlexibleFormFooter",
					"options": {
						"splitterType": "simple-h",
						"version": 0,
						"maxwidth": 1024
					},
					"*": {
						"ClearInvoices": {
							"type": "SubmitButton",
							"options": {
								"label": "_ClearInvoices",
								"action": "approve",
								"submit": true,
								"version": 0,
								"textPosition": "text-right",
								"textSize": "MS",
								"textColor": "default",
								"nextprocess": {
									"processName": "",
									"attachmentsMode": "all",
									"willBeChild": null
								},
								"url": "",
								"textStyle": "default",
								"urlImageOverlay": "",
								"position": null
							},
							"stamp": 133,
							"data": []
						},
						"Close": {
							"type": "SubmitButton",
							"options": {
								"label": "_Close",
								"action": "cancel",
								"submit": true,
								"version": 0
							},
							"stamp": 134,
							"data": []
						}
					},
					"stamp": 135,
					"data": []
				}
			},
			"stamp": 136,
			"data": []
		}
	},
	"stamps": 174,
	"data": []
}