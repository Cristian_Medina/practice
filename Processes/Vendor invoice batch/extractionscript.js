///#GLOBALS Lib
var run = function ()
{
	Sys.Helpers.TryCallFunction("Lib.AP.Customization.Extraction.OnBatchExtractionScriptBegin");
	// Set the subject of the form
	Data.SetValue("Subject", Language.Translate("_Received file: {0}", false, Document.GetName()));
	// Set the splitting recipient
	var nextProcessid = Process.GetNextProcessID();
	if (nextProcessid)
	{
		// Set the split tool to generate Vendor invoices directly
		Data.SetValue("SplitToProcessId", nextProcessid);
		if (Data.GetValue("OriginalJobID"))
		{
			// Generate the split vendor invoice processes directly for the user that owns the previous Job, AKA the owner of this one
			setSplitToOwner(Users.GetUser(Data.GetValue("OwnerId")));
		}
		else
		{
			var specificVendorInvoiceOwner = Variable.GetValueAsString("VendorInvoiceOwner");
			if (specificVendorInvoiceOwner)
			{
				specificVendorInvoiceOwner = Lib.P2P.ResolveDemoLogin(specificVendorInvoiceOwner);
				Log.Info("'VendorInvoiceOwner' variable set to '" + specificVendorInvoiceOwner + "', forwarding process and setting 'SplitToOwner' to this owner.");
				// assigns the batch process to the specified user
				Process.Forward(specificVendorInvoiceOwner);
				// assigns the split documents/processes to the specified user
				setSplitToOwner(Users.GetUser(specificVendorInvoiceOwner));
			}
			else
			{
				// Generate the vendor invoices directly for the user set as the next process user
				var nextProcessUser = Process.GetNextProcessUser();
				if (nextProcessUser)
				{
					setSplitToOwner(nextProcessUser);
				}
			}
		}
	}
	// Serialize the configuration on the record
	Sys.Parameters.GetInstance("P2P").Serialize();
	Sys.Parameters.GetInstance("AP").Serialize();
	// Check if a split is required
	var nPages = Document.GetPageCount();
	if (nPages <= 1)
	{
		Log.Info("Single page - auto approve (pages=" + nPages + ")");
		// No split required
		// Keep the current value for NeedValidation.
		// It is either the one designed on the process or 1 if the user submitted the file from the Web interface
	}
	else
	{
		Log.Info("Multiple pages - splitting required (pages=" + nPages + ")");
		// Show the split tool automatically when opening the form
		Data.SetValue("Split", 1);
		// Validation is required
		Data.SetValue("NeedValidation", 1);
	}

	function setSplitToOwner(user)
	{
		var fullDn = user.GetVars().GetValue_String("FullDn", 0);
		Data.SetValue("SplitToOwnerID", fullDn);
		Data.SetValue("SplitToOwnerPB", user.GetValue("OwnerPB"));
	}
	Sys.Helpers.TryCallFunction("Lib.AP.Customization.Extraction.OnBatchExtractionScriptEnd");
};
run();