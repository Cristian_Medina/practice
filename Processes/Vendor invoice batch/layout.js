{
	"*": {
		"GDRData": {
			"type": "GDRData",
			"options": {
				"version": 0
			},
			"stamp": 1,
			"data": []
		},
		"OCRParams": {
			"type": "OCRParams",
			"options": {
				"version": 0
			},
			"stamp": 2,
			"data": []
		},
		"Screen": {
			"type": "FlexibleFormLayout",
			"options": {
				"splitterType": "complex",
				"version": 0
			},
			"*": {
				"form-header": {
					"type": "FlexibleFormHeader",
					"options": {
						"version": 0
					},
					"stamp": 3,
					"data": []
				},
				"form-center": {
					"type": "FlexibleFormCenter",
					"options": {
						"version": 0
					},
					"*": {
						"form-content": {
							"type": "FlexibleFormContent",
							"options": {
								"version": 0
							},
							"*": {
								"form-content-top": {
									"type": "FlexibleFormSplitter",
									"options": {
										"autosize": true,
										"version": 1,
										"box": {
											"top": "0%",
											"left": "0%",
											"width": null,
											"height": "13%"
										},
										"hidden": true
									},
									"*": {},
									"stamp": 4,
									"data": []
								},
								"form-content-bottom": {
									"type": "FlexibleFormSplitter",
									"options": {
										"version": 1,
										"box": {
											"top": "0%",
											"left": "0%",
											"width": null,
											"height": "100%"
										},
										"hidden": false,
										"hideSeparator": true
									},
									"*": {
										"form-content-left": {
											"type": "FlexibleFormSplitter",
											"options": {
												"version": 1,
												"box": {
													"top": "0%",
													"left": "0%",
													"width": "45%",
													"height": null
												},
												"hidden": false
											},
											"*": {
												"form-content-left-4": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 5,
													"*": {
														"DocumentsPanel": {
															"type": "PanelDocument",
															"options": {
																"version": 0,
																"border": {},
																"labelLength": 0,
																"label": "_Documents"
															},
															"*": {
																"Document": {
																	"data": [
																		"documentscontroldata"
																	],
																	"type": "Documents",
																	"options": {
																		"version": 1,
																		"previewButton": true,
																		"downloadButton": true
																	},
																	"stamp": 6
																}
															},
															"stamp": 7,
															"data": []
														}
													}
												},
												"form-content-left-5": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 8,
													"*": {
														"NextProcess": {
															"type": "PanelNextProcess",
															"options": {
																"hidden": false,
																"version": 0,
																"border": {},
																"labelLength": 0,
																"label": "_Next Process"
															},
															"stamp": 9,
															"data": []
														}
													}
												},
												"form-content-left-2": {
													"type": "FlexibleFormLine",
													"options": {
														"version": 0
													},
													"*": {
														"SystemData": {
															"type": "PanelSystemData",
															"options": {
																"multicolumn": false,
																"version": 0,
																"border": {
																	"collapsed": true
																},
																"labelLength": 0,
																"label": "_System data"
															},
															"stamp": 10,
															"data": []
														}
													},
													"stamp": 11,
													"data": []
												},
												"form-content-left-8": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 12,
													"*": {
														"DataPanel": {
															"type": "PanelData",
															"options": {
																"border": {
																	"collapsed": true
																},
																"version": 0,
																"labelLength": 0,
																"label": "_Document data",
																"hidden": true
															},
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"options": {
																		"controlsAssociation": {},
																		"version": 0
																	},
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"options": {
																				"lines": 0,
																				"columns": 2,
																				"version": 0,
																				"ctrlsPos": {},
																				"colspans": []
																			},
																			"data": [
																				"fields"
																			],
																			"*": {},
																			"stamp": 13
																		}
																	},
																	"stamp": 14,
																	"data": []
																}
															},
															"stamp": 15,
															"data": []
														}
													}
												}
											},
											"stamp": 16,
											"data": []
										},
										"form-content-right": {
											"type": "FlexibleFormSplitter",
											"options": {
												"version": 1,
												"box": {
													"top": "0%",
													"left": "45%",
													"width": "55%",
													"height": null
												},
												"hidden": false
											},
											"*": {
												"form-content-right-1": {
													"type": "FlexibleFormLine",
													"options": {
														"version": 0
													},
													"*": {
														"PreviewPanel": {
															"type": "PanelPreview",
															"options": {
																"border": {},
																"version": 0,
																"labelLength": 0,
																"label": "_Document Preview"
															},
															"*": {
																"Preview": {
																	"data": [
																		"previewdata"
																	],
																	"type": "Preview",
																	"options": {
																		"version": 0
																	},
																	"stamp": 17
																}
															},
															"stamp": 18,
															"data": []
														}
													},
													"stamp": 19,
													"data": []
												}
											},
											"stamp": 20,
											"data": []
										}
									},
									"stamp": 21,
									"data": []
								}
							},
							"stamp": 22,
							"data": []
						}
					},
					"stamp": 23,
					"data": []
				},
				"form-footer": {
					"type": "FlexibleFormFooter",
					"options": {
						"splitterType": "simple-h",
						"version": 0
					},
					"*": {
						"Save": {
							"type": "SubmitButton",
							"data": false,
							"options": {
								"label": "_Save",
								"action": "save",
								"version": 0
							},
							"stamp": 24
						},
						"Reject": {
							"type": "SubmitButton",
							"data": false,
							"options": {
								"label": "_Reject",
								"action": "reject",
								"version": 0
							},
							"stamp": 25
						},
						"Quit": {
							"type": "SubmitButton",
							"data": false,
							"options": {
								"label": "_Close",
								"action": "cancel",
								"version": 0
							},
							"stamp": 26
						},
						"Approve": {
							"type": "SubmitButton",
							"data": false,
							"options": {
								"label": "_Approve",
								"action": "approve",
								"version": 0
							},
							"stamp": 27
						},
						"Split": {
							"type": "SubmitButton",
							"options": {
								"label": "_Split",
								"action": "split",
								"submit": true,
								"version": 0,
								"disabled": true
							},
							"stamp": 28,
							"data": []
						}
					},
					"stamp": 29,
					"data": []
				}
			},
			"stamp": 30,
			"data": []
		}
	},
	"stamps": 30,
	"data": []
}