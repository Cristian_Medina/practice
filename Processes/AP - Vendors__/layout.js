{
	"*": {
		"Screen": {
			"type": "FlexibleFormLayout",
			"options": {
				"splitterType": "simple-v",
				"backColor": "text-backgroundcolor-color7",
				"version": 0,
				"maxwidth": 1500
			},
			"*": {
				"form-header": {
					"type": "FlexibleFormHeader",
					"options": {
						"hidePanelsMenu": false,
						"version": 0,
						"maxwidth": 1500
					},
					"stamp": 1,
					"data": []
				},
				"form-center": {
					"type": "FlexibleFormCenter",
					"options": {
						"version": 0,
						"maxwidth": 1500
					},
					"*": {
						"form-content": {
							"type": "FlexibleFormContent",
							"options": {
								"version": 0
							},
							"*": {
								"form-content-center": {
									"type": "FlexibleFormSplitter",
									"options": {
										"version": 1,
										"box": {
											"top": "0%",
											"left": "0%",
											"width": "100%"
										},
										"hidden": false
									},
									"*": {
										"form-content-center-1": {
											"type": "FlexibleFormLine",
											"data": [],
											"options": {
												"version": 0
											},
											"stamp": 2,
											"*": {
												"DataPanel": {
													"type": "PanelData",
													"options": {
														"border": {},
														"version": 0,
														"labelLength": 0,
														"label": "_VendorInfo",
														"iconURL": "",
														"TitleFontSize": "M",
														"hideActionButtons": true,
														"hideTitleBar": false,
														"backgroundcolor": "default",
														"labelsAlignment": "right",
														"hideTitle": false,
														"leftImageURL": "",
														"removeMargins": false,
														"elementsAlignment": "left",
														"panelStyle": "FlexibleFormPanelLight",
														"hidden": false
													},
													"*": {
														"FieldsManager": {
															"type": "FieldsManager",
															"options": {
																"controlsAssociation": {
																	"Name__": "LabelName__",
																	"LabelName__": "Name__",
																	"Number__": "LabelNumber__",
																	"LabelNumber__": "Number__",
																	"VATNumber__": "LabelVATNumber__",
																	"LabelVATNumber__": "VATNumber__",
																	"FaxNumber__": "LabelFaxNumber__",
																	"LabelFaxNumber__": "FaxNumber__",
																	"PhoneNumber__": "LabelPhoneNumber__",
																	"LabelPhoneNumber__": "PhoneNumber__",
																	"PreferredInvoiceType__": "LabelPreferredInvoiceType__",
																	"LabelPreferredInvoiceType__": "PreferredInvoiceType__",
																	"CompanyCode__": "LabelCompanyCode__",
																	"LabelCompanyCode__": "CompanyCode__",
																	"Email__": "LabelEmail__",
																	"LabelEmail__": "Email__",
																	"PaymentTermCode__": "LabelPaymentTermCode__",
																	"LabelPaymentTermCode__": "PaymentTermCode__",
																	"GeneralAccount__": "LabelGeneralAccount__",
																	"LabelGeneralAccount__": "GeneralAccount__",
																	"TaxSystem__": "LabelTaxSystem__",
																	"LabelTaxSystem__": "TaxSystem__",
																	"Currency__": "LabelCurrency__",
																	"LabelCurrency__": "Currency__",
																	"ParafiscalTax__": "LabelParafiscalTax__",
																	"LabelParafiscalTax__": "ParafiscalTax__",
																	"SupplierDue__": "LabelSupplierDue__",
																	"LabelSupplierDue__": "SupplierDue__",
																	"DUNSNumber__": "LabelDUNSNumber__",
																	"LabelDUNSNumber__": "DUNSNumber__"
																},
																"version": 0
															},
															"*": {
																"Grid": {
																	"type": "GridLayout",
																	"options": {
																		"lines": 16,
																		"columns": 2,
																		"version": 0,
																		"ctrlsPos": {
																			"Name__": {
																				"line": 3,
																				"column": 2
																			},
																			"LabelName__": {
																				"line": 3,
																				"column": 1
																			},
																			"Number__": {
																				"line": 2,
																				"column": 2
																			},
																			"LabelNumber__": {
																				"line": 2,
																				"column": 1
																			},
																			"VATNumber__": {
																				"line": 6,
																				"column": 2
																			},
																			"LabelVATNumber__": {
																				"line": 6,
																				"column": 1
																			},
																			"FaxNumber__": {
																				"line": 5,
																				"column": 2
																			},
																			"LabelFaxNumber__": {
																				"line": 5,
																				"column": 1
																			},
																			"PhoneNumber__": {
																				"line": 4,
																				"column": 2
																			},
																			"LabelPhoneNumber__": {
																				"line": 4,
																				"column": 1
																			},
																			"PreferredInvoiceType__": {
																				"line": 8,
																				"column": 2
																			},
																			"LabelPreferredInvoiceType__": {
																				"line": 8,
																				"column": 1
																			},
																			"CompanyCode__": {
																				"line": 1,
																				"column": 2
																			},
																			"LabelCompanyCode__": {
																				"line": 1,
																				"column": 1
																			},
																			"Email__": {
																				"line": 10,
																				"column": 2
																			},
																			"LabelEmail__": {
																				"line": 10,
																				"column": 1
																			},
																			"PaymentTermCode__": {
																				"line": 9,
																				"column": 2
																			},
																			"LabelPaymentTermCode__": {
																				"line": 9,
																				"column": 1
																			},
																			"GeneralAccount__": {
																				"line": 11,
																				"column": 2
																			},
																			"LabelGeneralAccount__": {
																				"line": 11,
																				"column": 1
																			},
																			"TaxSystem__": {
																				"line": 12,
																				"column": 2
																			},
																			"LabelTaxSystem__": {
																				"line": 12,
																				"column": 1
																			},
																			"Currency__": {
																				"line": 13,
																				"column": 2
																			},
																			"LabelCurrency__": {
																				"line": 13,
																				"column": 1
																			},
																			"ParafiscalTax__": {
																				"line": 14,
																				"column": 2
																			},
																			"LabelParafiscalTax__": {
																				"line": 14,
																				"column": 1
																			},
																			"SupplierDue__": {
																				"line": 15,
																				"column": 2
																			},
																			"LabelSupplierDue__": {
																				"line": 15,
																				"column": 1
																			},
																			"Spacer_line__": {
																				"line": 16,
																				"column": 1
																			},
																			"DUNSNumber__": {
																				"line": 7,
																				"column": 2
																			},
																			"LabelDUNSNumber__": {
																				"line": 7,
																				"column": 1
																			}
																		},
																		"colspans": [
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			]
																		]
																	},
																	"data": [
																		"fields"
																	],
																	"*": {
																		"LabelCompanyCode__": {
																			"type": "Label",
																			"data": [
																				"CompanyCode__"
																			],
																			"options": {
																				"label": "Company code",
																				"version": 0
																			},
																			"stamp": 3
																		},
																		"CompanyCode__": {
																			"type": "DatabaseComboBox",
																			"data": [
																				"CompanyCode__"
																			],
																			"options": {
																				"browsable": true,
																				"label": "Company code",
																				"activable": true,
																				"width": 500,
																				"fTSmaxRecords": "20",
																				"version": 0,
																				"PreFillFTS": true
																			},
																			"stamp": 4
																		},
																		"LabelNumber__": {
																			"type": "Label",
																			"data": [
																				"Number__"
																			],
																			"options": {
																				"label": "Number",
																				"version": 0
																			},
																			"stamp": 5
																		},
																		"Number__": {
																			"type": "ShortText",
																			"data": [
																				"Number__"
																			],
																			"options": {
																				"label": "Number",
																				"activable": true,
																				"width": "500",
																				"length": 50,
																				"version": 0,
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default"
																			},
																			"stamp": 6
																		},
																		"LabelName__": {
																			"type": "Label",
																			"data": [
																				"Name__"
																			],
																			"options": {
																				"label": "Name",
																				"version": 0
																			},
																			"stamp": 7
																		},
																		"Name__": {
																			"type": "ShortText",
																			"data": [
																				"Name__"
																			],
																			"options": {
																				"label": "Name",
																				"activable": true,
																				"width": "500",
																				"length": 35,
																				"version": 0,
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default"
																			},
																			"stamp": 8
																		},
																		"LabelPhoneNumber__": {
																			"type": "Label",
																			"data": [
																				"PhoneNumber__"
																			],
																			"options": {
																				"label": "Phone number",
																				"version": 0
																			},
																			"stamp": 19
																		},
																		"PhoneNumber__": {
																			"type": "ShortText",
																			"data": [
																				"PhoneNumber__"
																			],
																			"options": {
																				"label": "Phone number",
																				"activable": true,
																				"width": "500",
																				"length": 32,
																				"version": 0,
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default"
																			},
																			"stamp": 20
																		},
																		"LabelFaxNumber__": {
																			"type": "Label",
																			"data": [
																				"FaxNumber__"
																			],
																			"options": {
																				"label": "Fax number",
																				"version": 0
																			},
																			"stamp": 21
																		},
																		"FaxNumber__": {
																			"type": "ShortText",
																			"data": [
																				"FaxNumber__"
																			],
																			"options": {
																				"label": "Fax number",
																				"activable": true,
																				"width": "500",
																				"length": 32,
																				"version": 0,
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default"
																			},
																			"stamp": 22
																		},
																		"LabelVATNumber__": {
																			"type": "Label",
																			"data": [
																				"VATNumber__"
																			],
																			"options": {
																				"label": "VAT number",
																				"version": 0
																			},
																			"stamp": 23
																		},
																		"VATNumber__": {
																			"type": "ShortText",
																			"data": [
																				"VATNumber__"
																			],
																			"options": {
																				"label": "VAT number",
																				"activable": true,
																				"width": "500",
																				"length": 20,
																				"version": 0,
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default"
																			},
																			"stamp": 24
																		},
																		"LabelDUNSNumber__": {
																			"type": "Label",
																			"data": [
																				"DUNSNumber__"
																			],
																			"options": {
																				"label": "_DUNSNumber"
																			},
																			"stamp": 70
																		},
																		"DUNSNumber__": {
																			"type": "ShortText",
																			"data": [
																				"DUNSNumber__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"version": 1,
																				"label": "_DUNSNumber",
																				"activable": true,
																				"width": "500",
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"helpIconPosition": "Right",
																				"browsable": false,
																				"length": 9
																			},
																			"stamp": 71
																		},
																		"LabelPreferredInvoiceType__": {
																			"type": "Label",
																			"data": [
																				"PreferredInvoiceType__"
																			],
																			"options": {
																				"label": "Preferred invoice type",
																				"version": 0
																			},
																			"stamp": 25
																		},
																		"PreferredInvoiceType__": {
																			"type": "ShortText",
																			"data": [
																				"PreferredInvoiceType__"
																			],
																			"options": {
																				"label": "Preferred invoice type",
																				"activable": true,
																				"width": "500",
																				"version": 0,
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default"
																			},
																			"stamp": 26
																		},
																		"LabelPaymentTermCode__": {
																			"type": "Label",
																			"data": [
																				"PaymentTermCode__"
																			],
																			"options": {
																				"label": "Payment terms",
																				"version": 0
																			},
																			"stamp": 27
																		},
																		"PaymentTermCode__": {
																			"type": "DatabaseComboBox",
																			"data": [
																				"PaymentTermCode__"
																			],
																			"options": {
																				"browsable": true,
																				"label": "Payment terms",
																				"activable": true,
																				"width": "500",
																				"fTSmaxRecords": "20",
																				"PreFillFTS": true,
																				"version": 0
																			},
																			"stamp": 28
																		},
																		"LabelEmail__": {
																			"type": "Label",
																			"data": [
																				"Email__"
																			],
																			"options": {
																				"label": "Email",
																				"version": 0
																			},
																			"stamp": 29
																		},
																		"Email__": {
																			"type": "ShortText",
																			"data": [
																				"Email__"
																			],
																			"options": {
																				"label": "Email",
																				"activable": true,
																				"width": "500",
																				"version": 0,
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default"
																			},
																			"stamp": 30
																		},
																		"LabelGeneralAccount__": {
																			"type": "Label",
																			"data": [
																				"GeneralAccount__"
																			],
																			"options": {
																				"label": "General account",
																				"version": 0
																			},
																			"stamp": 31
																		},
																		"GeneralAccount__": {
																			"type": "ShortText",
																			"data": [
																				"GeneralAccount__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "General account",
																				"activable": true,
																				"width": "500",
																				"browsable": false,
																				"version": 0
																			},
																			"stamp": 32
																		},
																		"LabelTaxSystem__": {
																			"type": "Label",
																			"data": [
																				"TaxSystem__"
																			],
																			"options": {
																				"label": "Tax system",
																				"version": 0
																			},
																			"stamp": 33
																		},
																		"TaxSystem__": {
																			"type": "ShortText",
																			"data": [
																				"TaxSystem__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "Tax system",
																				"activable": true,
																				"width": "500",
																				"length": 10,
																				"browsable": false,
																				"version": 0
																			},
																			"stamp": 34
																		},
																		"LabelCurrency__": {
																			"type": "Label",
																			"data": [
																				"Currency__"
																			],
																			"options": {
																				"label": "Currency",
																				"version": 0
																			},
																			"stamp": 35
																		},
																		"Currency__": {
																			"type": "ShortText",
																			"data": [
																				"Currency__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "Currency",
																				"activable": true,
																				"width": "500",
																				"length": 5,
																				"browsable": false,
																				"version": 0
																			},
																			"stamp": 36
																		},
																		"LabelParafiscalTax__": {
																			"type": "Label",
																			"data": [
																				"ParafiscalTax__"
																			],
																			"options": {
																				"label": "Parafiscal tax",
																				"version": 0
																			},
																			"stamp": 37
																		},
																		"ParafiscalTax__": {
																			"type": "CheckBox",
																			"data": [
																				"ParafiscalTax__"
																			],
																			"options": {
																				"label": "Parafiscal tax",
																				"activable": true,
																				"version": 0
																			},
																			"stamp": 38
																		},
																		"LabelSupplierDue__": {
																			"type": "Label",
																			"data": [
																				"SupplierDue__"
																			],
																			"options": {
																				"label": "Supplier due",
																				"version": 0
																			},
																			"stamp": 39
																		},
																		"SupplierDue__": {
																			"type": "DatabaseComboBox",
																			"data": [
																				"SupplierDue__"
																			],
																			"options": {
																				"browsable": true,
																				"label": "Supplier due",
																				"activable": true,
																				"width": "500",
																				"fTSmaxRecords": "20",
																				"version": 0,
																				"PreFillFTS": true
																			},
																			"stamp": 40
																		},
																		"Spacer_line__": {
																			"type": "Spacer",
																			"data": false,
																			"options": {
																				"height": "",
																				"width": "",
																				"color": "default",
																				"label": "Spacer line",
																				"version": 0
																			},
																			"stamp": 64
																		}
																	},
																	"stamp": 41
																}
															},
															"stamp": 42,
															"data": []
														}
													},
													"stamp": 43,
													"data": []
												},
												"VendorAddress": {
													"type": "PanelData",
													"data": [],
													"options": {
														"border": {},
														"labelLength": 0,
														"iconURL": "",
														"TitleFontSize": "M",
														"hideActionButtons": true,
														"hideTitleBar": false,
														"hideTitle": false,
														"backgroundcolor": "default",
														"labelsAlignment": "right",
														"label": "_VendorAddress",
														"hidden": false,
														"leftImageURL": "",
														"removeMargins": false,
														"elementsAlignment": "left",
														"panelStyle": "FlexibleFormPanelLight",
														"version": 0
													},
													"stamp": 55,
													"*": {
														"FieldsManager": {
															"type": "FieldsManager",
															"data": [],
															"options": {
																"controlsAssociation": {
																	"LabelStreet__": "Street__",
																	"Street__": "LabelStreet__",
																	"LabelPostOfficeBox__": "PostOfficeBox__",
																	"PostOfficeBox__": "LabelPostOfficeBox__",
																	"LabelCity__": "City__",
																	"City__": "LabelCity__",
																	"LabelPostalCode__": "PostalCode__",
																	"PostalCode__": "LabelPostalCode__",
																	"LabelRegion__": "Region__",
																	"Region__": "LabelRegion__",
																	"LabelCountry__": "Country__",
																	"Country__": "LabelCountry__",
																	"VendorAddress__": "LabelVendorAddress__",
																	"LabelVendorAddress__": "VendorAddress__",
																	"Sub__": "LabelSub__",
																	"LabelSub__": "Sub__"
																},
																"version": 0
															},
															"stamp": 56,
															"*": {
																"Grid": {
																	"type": "GridLayout",
																	"data": [
																		"fields"
																	],
																	"options": {
																		"ctrlsPos": {
																			"LabelStreet__": {
																				"line": 2,
																				"column": 1
																			},
																			"Street__": {
																				"line": 2,
																				"column": 2
																			},
																			"LabelPostOfficeBox__": {
																				"line": 3,
																				"column": 1
																			},
																			"PostOfficeBox__": {
																				"line": 3,
																				"column": 2
																			},
																			"LabelCity__": {
																				"line": 4,
																				"column": 1
																			},
																			"City__": {
																				"line": 4,
																				"column": 2
																			},
																			"LabelPostalCode__": {
																				"line": 5,
																				"column": 1
																			},
																			"PostalCode__": {
																				"line": 5,
																				"column": 2
																			},
																			"LabelRegion__": {
																				"line": 6,
																				"column": 1
																			},
																			"Region__": {
																				"line": 6,
																				"column": 2
																			},
																			"LabelCountry__": {
																				"line": 7,
																				"column": 1
																			},
																			"Country__": {
																				"line": 7,
																				"column": 2
																			},
																			"VendorAddress__": {
																				"line": 8,
																				"column": 2
																			},
																			"LabelVendorAddress__": {
																				"line": 8,
																				"column": 1
																			},
																			"Spacer_line2__": {
																				"line": 9,
																				"column": 1
																			},
																			"Sub__": {
																				"line": 1,
																				"column": 2
																			},
																			"LabelSub__": {
																				"line": 1,
																				"column": 1
																			}
																		},
																		"lines": 9,
																		"columns": 2,
																		"colspans": [
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			]
																		],
																		"version": 0
																	},
																	"stamp": 57,
																	"*": {
																		"LabelSub__": {
																			"type": "Label",
																			"data": [
																				"Sub__"
																			],
																			"options": {
																				"label": "_Sub"
																			},
																			"stamp": 68
																		},
																		"Sub__": {
																			"type": "ShortText",
																			"data": [
																				"Sub__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"version": 1,
																				"label": "_Sub",
																				"activable": true,
																				"width": "500",
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"browsable": false
																			},
																			"stamp": 69
																		},
																		"Country__": {
																			"type": "ShortText",
																			"data": [
																				"Country__"
																			],
																			"options": {
																				"label": "Country",
																				"activable": true,
																				"width": "500",
																				"length": 3,
																				"version": 0,
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default"
																			},
																			"stamp": 18
																		},
																		"LabelCountry__": {
																			"type": "Label",
																			"data": [
																				"Country__"
																			],
																			"options": {
																				"label": "Country",
																				"version": 0
																			},
																			"stamp": 17
																		},
																		"Region__": {
																			"type": "ShortText",
																			"data": [
																				"Region__"
																			],
																			"options": {
																				"label": "Region",
																				"activable": true,
																				"width": "500",
																				"length": 3,
																				"version": 0,
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default"
																			},
																			"stamp": 16
																		},
																		"LabelRegion__": {
																			"type": "Label",
																			"data": [
																				"Region__"
																			],
																			"options": {
																				"label": "Region",
																				"version": 0
																			},
																			"stamp": 15
																		},
																		"PostalCode__": {
																			"type": "ShortText",
																			"data": [
																				"PostalCode__"
																			],
																			"options": {
																				"label": "Postal code",
																				"activable": true,
																				"width": "500",
																				"length": 10,
																				"version": 0,
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default"
																			},
																			"stamp": 14
																		},
																		"LabelPostalCode__": {
																			"type": "Label",
																			"data": [
																				"PostalCode__"
																			],
																			"options": {
																				"label": "Postal code",
																				"version": 0
																			},
																			"stamp": 13
																		},
																		"City__": {
																			"type": "ShortText",
																			"data": [
																				"City__"
																			],
																			"options": {
																				"label": "City",
																				"activable": true,
																				"width": "500",
																				"length": 35,
																				"version": 0,
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default"
																			},
																			"stamp": 12
																		},
																		"LabelCity__": {
																			"type": "Label",
																			"data": [
																				"City__"
																			],
																			"options": {
																				"label": "City",
																				"version": 0
																			},
																			"stamp": 11
																		},
																		"PostOfficeBox__": {
																			"type": "ShortText",
																			"data": [
																				"PostOfficeBox__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "PostOfficeBox",
																				"activable": true,
																				"width": "500",
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"browsable": false,
																				"version": 0,
																				"length": 20
																			},
																			"stamp": 53
																		},
																		"LabelPostOfficeBox__": {
																			"type": "Label",
																			"data": [
																				"PostOfficeBox__"
																			],
																			"options": {
																				"label": "PostOfficeBox",
																				"version": 0
																			},
																			"stamp": 52
																		},
																		"Street__": {
																			"type": "ShortText",
																			"data": [
																				"Street__"
																			],
																			"options": {
																				"label": "Street",
																				"activable": true,
																				"width": "500",
																				"length": 50,
																				"version": 0,
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default"
																			},
																			"stamp": 10
																		},
																		"LabelStreet__": {
																			"type": "Label",
																			"data": [
																				"Street__"
																			],
																			"options": {
																				"label": "Street",
																				"version": 0
																			},
																			"stamp": 9
																		},
																		"LabelVendorAddress__": {
																			"type": "Label",
																			"data": [],
																			"options": {
																				"label": "_Vendor Address",
																				"version": 0
																			},
																			"stamp": 62
																		},
																		"VendorAddress__": {
																			"type": "LongText",
																			"data": [],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"version": 1,
																				"minNbLines": 5,
																				"label": "_Vendor Address",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"numberOfLines": 5,
																				"readonly": true,
																				"notInDB": true,
																				"dataType": "String",
																				"browsable": false
																			},
																			"stamp": 63
																		},
																		"Spacer_line2__": {
																			"type": "Spacer",
																			"data": false,
																			"options": {
																				"height": "",
																				"width": "",
																				"color": "default",
																				"label": "Spacer line2",
																				"version": 0
																			},
																			"stamp": 65
																		}
																	}
																}
															}
														}
													}
												}
											}
										}
									},
									"stamp": 44,
									"data": []
								}
							},
							"stamp": 45,
							"data": []
						}
					},
					"stamp": 46,
					"data": []
				},
				"form-footer": {
					"type": "FlexibleFormFooter",
					"options": {
						"splitterType": "simple-h",
						"version": 0,
						"maxwidth": 1500
					},
					"*": {
						"Save": {
							"type": "SubmitButton",
							"options": {
								"label": "_Save",
								"action": "savequit",
								"submit": true,
								"version": 0,
								"shortCut": "S"
							},
							"stamp": 47,
							"data": []
						},
						"Close": {
							"type": "SubmitButton",
							"options": {
								"label": "_Close",
								"action": "cancel",
								"submit": true,
								"version": 0,
								"shortCut": "Q"
							},
							"stamp": 48,
							"data": []
						},
						"Delete": {
							"type": "SubmitButton",
							"options": {
								"label": "_Delete",
								"action": "delete",
								"submit": true,
								"version": 0,
								"disabled": true
							},
							"stamp": 49,
							"data": []
						}
					},
					"stamp": 50,
					"data": []
				}
			},
			"stamp": 51,
			"data": []
		}
	},
	"stamps": 71,
	"data": []
}