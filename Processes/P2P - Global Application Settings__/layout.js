{
	"*": {
		"Screen": {
			"type": "FlexibleFormLayout",
			"options": {
				"splitterType": "simple-v",
				"version": 0,
				"backColor": "text-backgroundcolor-default",
				"style": null
			},
			"*": {
				"form-header": {
					"type": "FlexibleFormHeader",
					"options": {
						"hidePanelsMenu": false,
						"version": 0
					},
					"stamp": 1,
					"data": []
				},
				"form-center": {
					"type": "FlexibleFormCenter",
					"options": {
						"version": 0
					},
					"*": {
						"form-content": {
							"type": "FlexibleFormContent",
							"options": {
								"version": 0
							},
							"*": {
								"form-content-center": {
									"type": "FlexibleFormSplitter",
									"options": {
										"version": 1,
										"box": {
											"top": "0%",
											"left": "0%",
											"width": "100%"
										},
										"hidden": false,
										"hideSeparator": true
									},
									"*": {
										"form-content-center-1": {
											"type": "FlexibleFormLine",
											"data": [],
											"options": {
												"version": 0
											},
											"stamp": 2,
											"*": {
												"Global_Parameters_Pane": {
													"type": "PanelData",
													"options": {
														"border": {},
														"version": 0,
														"labelLength": 0,
														"iconURL": "",
														"TitleFontSize": "S",
														"hideActionButtons": false,
														"hideTitleBar": false,
														"hideTitle": false,
														"backgroundcolor": "default",
														"labelsAlignment": "right",
														"label": "_Global parameters pane",
														"leftImageURL": "",
														"removeMargins": false,
														"elementsAlignment": "left",
														"sameHeightAsSiblings": false
													},
													"*": {
														"FieldsManager": {
															"type": "FieldsManager",
															"options": {
																"controlsAssociation": {
																	"EnableAccountPayableGlobalSetting__": "LabelEnableAccountPayableGlobalSetting__",
																	"LabelEnableAccountPayableGlobalSetting__": "EnableAccountPayableGlobalSetting__",
																	"EnablePurchasingGlobalSetting__": "LabelEnablePurchasingGlobalSetting__",
																	"LabelEnablePurchasingGlobalSetting__": "EnablePurchasingGlobalSetting__",
																	"SendNotificationsToEachGroupMembers__": "LabelSendNotificationsToEachGroupMembers__",
																	"LabelSendNotificationsToEachGroupMembers__": "SendNotificationsToEachGroupMembers__",
																	"EnableExpenseClaimsGlobalSetting__": "LabelEnableExpenseClaimsGlobalSetting__",
																	"LabelEnableExpenseClaimsGlobalSetting__": "EnableExpenseClaimsGlobalSetting__",
																	"EnableFlipPOGlobalSetting__": "LabelEnableFlipPOGlobalSetting__",
																	"LabelEnableFlipPOGlobalSetting__": "EnableFlipPOGlobalSetting__",
																	"EnableVendorManagementGlobalSetting__": "LabelEnableVendorManagementGlobalSetting__",
																	"LabelEnableVendorManagementGlobalSetting__": "EnableVendorManagementGlobalSetting__",
																	"SuffixAccount__": "LabelSuffixAccount__",
																	"LabelSuffixAccount__": "SuffixAccount__",
																	"EnableAIForExpenses__": "LabelEnableAIForExpenses__",
																	"LabelEnableAIForExpenses__": "EnableAIForExpenses__",
																	"EnableSynergyNeuralNetworkGlobalSetting__": "LabelEnableSynergyNeuralNetworkGlobalSetting__",
																	"LabelEnableSynergyNeuralNetworkGlobalSetting__": "EnableSynergyNeuralNetworkGlobalSetting__",
																	"EnableContractGlobalSetting__": "LabelEnableContractGlobalSetting__",
																	"LabelEnableContractGlobalSetting__": "EnableContractGlobalSetting__",
																	"EnableLowBudgetNotification__": "LabelEnableLowBudgetNotification__",
																	"LabelEnableLowBudgetNotification__": "EnableLowBudgetNotification__",
																	"EnablePunchoutV2__": "LabelEnablePunchoutV2__",
																	"LabelEnablePunchoutV2__": "EnablePunchoutV2__",
																	"EnableServiceBasedItem__": "LabelEnableServiceBasedItem__",
																	"LabelEnableServiceBasedItem__": "EnableServiceBasedItem__",
																	"EnableInventoryManagement__": "LabelEnableInventoryManagement__",
																	"LabelEnableInventoryManagement__": "EnableInventoryManagement__"
																},
																"version": 0
															},
															"*": {
																"Grid": {
																	"type": "GridLayout",
																	"options": {
																		"lines": 15,
																		"columns": 2,
																		"version": 0,
																		"ctrlsPos": {
																			"EnableAccountPayableGlobalSetting__": {
																				"line": 1,
																				"column": 2
																			},
																			"LabelEnableAccountPayableGlobalSetting__": {
																				"line": 1,
																				"column": 1
																			},
																			"EnablePurchasingGlobalSetting__": {
																				"line": 2,
																				"column": 2
																			},
																			"LabelEnablePurchasingGlobalSetting__": {
																				"line": 2,
																				"column": 1
																			},
																			"SendNotificationsToEachGroupMembers__": {
																				"line": 5,
																				"column": 2
																			},
																			"LabelSendNotificationsToEachGroupMembers__": {
																				"line": 5,
																				"column": 1
																			},
																			"EnableExpenseClaimsGlobalSetting__": {
																				"line": 4,
																				"column": 2
																			},
																			"LabelEnableExpenseClaimsGlobalSetting__": {
																				"line": 4,
																				"column": 1
																			},
																			"EnableFlipPOGlobalSetting__": {
																				"line": 6,
																				"column": 2
																			},
																			"LabelEnableFlipPOGlobalSetting__": {
																				"line": 6,
																				"column": 1
																			},
																			"EnableVendorManagementGlobalSetting__": {
																				"line": 7,
																				"column": 2
																			},
																			"LabelEnableVendorManagementGlobalSetting__": {
																				"line": 7,
																				"column": 1
																			},
																			"SuffixAccount__": {
																				"line": 9,
																				"column": 2
																			},
																			"LabeSuffixAccount__": {
																				"line": 9,
																				"column": 1
																			},
																			"LabelSuffixAccount__": {
																				"line": 9,
																				"column": 1
																			},
																			"EnableAIForExpenses__": {
																				"line": 8,
																				"column": 2
																			},
																			"LabelEnableAIForExpenses__": {
																				"line": 8,
																				"column": 1
																			},
																			"EnableSynergyNeuralNetworkGlobalSetting__": {
																				"line": 10,
																				"column": 2
																			},
																			"LabelEnableSynergyNeuralNetworkGlobalSetting__": {
																				"line": 10,
																				"column": 1
																			},
																			"ocrEngine__": {
																				"line": 11,
																				"column": 2
																			},
																			"LabelocrEngine__": {
																				"line": 11,
																				"column": 1
																			},
																			"EnableContractGlobalSetting__": {
																				"line": 3,
																				"column": 2
																			},
																			"LabelEnableContractGlobalSetting__": {
																				"line": 3,
																				"column": 1
																			},
																			"EnableLowBudgetNotification__": {
																				"line": 12,
																				"column": 2
																			},
																			"LabelEnableLowBudgetNotification__": {
																				"line": 12,
																				"column": 1
																			},
																			"EnablePunchoutV2__": {
																				"line": 13,
																				"column": 2
																			},
																			"LabelEnablePunchoutV2__": {
																				"line": 13,
																				"column": 1
																			},
																			"EnableServiceBasedItem__": {
																				"line": 14,
																				"column": 2
																			},
																			"LabelEnableServiceBasedItem__": {
																				"line": 14,
																				"column": 1
																			},
																			"EnableInventoryManagement__": {
																				"line": 15,
																				"column": 2
																			},
																			"LabelEnableInventoryManagement__": {
																				"line": 15,
																				"column": 1
																			}
																		},
																		"colspans": [
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[]
																		]
																	},
																	"data": [
																		"fields"
																	],
																	"*": {
																		"LabelEnableAccountPayableGlobalSetting__": {
																			"type": "Label",
																			"data": [
																				"EnableAccountPayableGlobalSetting__"
																			],
																			"options": {
																				"label": "_Enable Account Payable",
																				"version": 0
																			},
																			"stamp": 3
																		},
																		"EnableAccountPayableGlobalSetting__": {
																			"type": "CheckBox",
																			"data": [
																				"EnableAccountPayableGlobalSetting__"
																			],
																			"options": {
																				"label": "_Enable Account Payable",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"version": 0
																			},
																			"stamp": 4
																		},
																		"LabelEnablePurchasingGlobalSetting__": {
																			"type": "Label",
																			"data": [
																				"EnablePurchasingGlobalSetting__"
																			],
																			"options": {
																				"label": "_Enable Purchasing_V2",
																				"version": 0
																			},
																			"stamp": 5
																		},
																		"EnablePurchasingGlobalSetting__": {
																			"type": "CheckBox",
																			"data": [
																				"EnablePurchasingGlobalSetting__"
																			],
																			"options": {
																				"label": "_Enable Purchasing_V2",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"version": 0
																			},
																			"stamp": 6
																		},
																		"LabelEnableContractGlobalSetting__": {
																			"type": "Label",
																			"data": [
																				"EnableContractGlobalSetting__"
																			],
																			"options": {
																				"label": "_EnableContractGlobalSetting",
																				"version": 0
																			},
																			"stamp": 7
																		},
																		"EnableContractGlobalSetting__": {
																			"type": "CheckBox",
																			"data": [
																				"EnableContractGlobalSetting__"
																			],
																			"options": {
																				"label": "_EnableContractGlobalSetting",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"version": 0
																			},
																			"stamp": 8
																		},
																		"LabelEnableExpenseClaimsGlobalSetting__": {
																			"type": "Label",
																			"data": [
																				"EnableExpenseClaimsGlobalSetting__"
																			],
																			"options": {
																				"label": "_Enable Expense Claims",
																				"version": 0
																			},
																			"stamp": 9
																		},
																		"EnableExpenseClaimsGlobalSetting__": {
																			"type": "CheckBox",
																			"data": [
																				"EnableExpenseClaimsGlobalSetting__"
																			],
																			"options": {
																				"label": "_Enable Expense Claims",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"version": 0
																			},
																			"stamp": 10
																		},
																		"LabelSendNotificationsToEachGroupMembers__": {
																			"type": "Label",
																			"data": [
																				"SendNotificationsToEachGroupMembers__"
																			],
																			"options": {
																				"label": "_SendNotificationsToEachGroupMembers",
																				"version": 0
																			},
																			"stamp": 11
																		},
																		"SendNotificationsToEachGroupMembers__": {
																			"type": "CheckBox",
																			"data": [
																				"SendNotificationsToEachGroupMembers__"
																			],
																			"options": {
																				"label": "_SendNotificationsToEachGroupMembers",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"version": 0
																			},
																			"stamp": 12
																		},
																		"LabelEnableFlipPOGlobalSetting__": {
																			"type": "Label",
																			"data": [
																				"EnableFlipPOGlobalSetting__"
																			],
																			"options": {
																				"label": "_Enable Flip PO",
																				"version": 0
																			},
																			"stamp": 13
																		},
																		"EnableFlipPOGlobalSetting__": {
																			"type": "CheckBox",
																			"data": [
																				"EnableFlipPOGlobalSetting__"
																			],
																			"options": {
																				"label": "_Enable Flip PO",
																				"activable": true,
																				"width": 230,
																				"version": 0
																			},
																			"stamp": 14
																		},
																		"LabelEnableVendorManagementGlobalSetting__": {
																			"type": "Label",
																			"data": [
																				"EnableVendorManagementGlobalSetting__"
																			],
																			"options": {
																				"label": "_Enable Vendor Management",
																				"version": 0
																			},
																			"stamp": 15
																		},
																		"EnableVendorManagementGlobalSetting__": {
																			"type": "CheckBox",
																			"data": [
																				"EnableVendorManagementGlobalSetting__"
																			],
																			"options": {
																				"label": "_Enable Vendor Management",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"version": 0
																			},
																			"stamp": 16
																		},
																		"LabelEnableAIForExpenses__": {
																			"type": "Label",
																			"data": [
																				"EnableAIForExpenses__"
																			],
																			"options": {
																				"label": "_Enable AI for Expenses",
																				"version": 0
																			},
																			"stamp": 17
																		},
																		"EnableAIForExpenses__": {
																			"type": "CheckBox",
																			"data": [
																				"EnableAIForExpenses__"
																			],
																			"options": {
																				"label": "_Enable AI for Expenses",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"version": 0
																			},
																			"stamp": 18
																		},
																		"LabelSuffixAccount__": {
																			"type": "Label",
																			"data": [
																				"SuffixAccount__"
																			],
																			"options": {
																				"label": "_SuffixAccount",
																				"version": 0
																			},
																			"stamp": 19
																		},
																		"SuffixAccount__": {
																			"type": "ShortText",
																			"data": [
																				"SuffixAccount__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"version": 1,
																				"label": "_SuffixAccount",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"browsable": false
																			},
																			"stamp": 20
																		},
																		"LabelEnableSynergyNeuralNetworkGlobalSetting__": {
																			"type": "Label",
																			"data": [
																				"EnableSynergyNeuralNetworkGlobalSetting__"
																			],
																			"options": {
																				"label": "_Enable Synergy Neural Network",
																				"version": 0
																			},
																			"stamp": 21
																		},
																		"EnableSynergyNeuralNetworkGlobalSetting__": {
																			"type": "CheckBox",
																			"data": [
																				"EnableSynergyNeuralNetworkGlobalSetting__"
																			],
																			"options": {
																				"label": "_Enable Synergy Neural Network",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"version": 0
																			},
																			"stamp": 22
																		},
																		"LabelocrEngine__": {
																			"type": "Label",
																			"data": [
																				"ocrEngine__"
																			],
																			"options": {
																				"label": "_ocrEngine",
																				"version": 0
																			},
																			"stamp": 23
																		},
																		"ocrEngine__": {
																			"type": "ShortText",
																			"data": [
																				"ocrEngine__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"version": 1,
																				"label": "_ocrEngine",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"browsable": false
																			},
																			"stamp": 24
																		},
																		"LabelEnableLowBudgetNotification__": {
																			"type": "Label",
																			"data": [
																				"EnableLowBudgetNotification__"
																			],
																			"options": {
																				"label": "_EnableLowBudgetNotification",
																				"version": 0
																			},
																			"stamp": 25
																		},
																		"EnableLowBudgetNotification__": {
																			"type": "CheckBox",
																			"data": [
																				"EnableLowBudgetNotification__"
																			],
																			"options": {
																				"label": "_EnableLowBudgetNotification",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"helpIconPosition": "Right",
																				"version": 0
																			},
																			"stamp": 26
																		},
																		"LabelEnablePunchoutV2__": {
																			"type": "Label",
																			"data": [
																				"EnablePunchoutV2__"
																			],
																			"options": {
																				"label": "_EnablePunchoutV2",
																				"version": 0
																			},
																			"stamp": 27
																		},
																		"EnablePunchoutV2__": {
																			"type": "CheckBox",
																			"data": [
																				"EnablePunchoutV2__"
																			],
																			"options": {
																				"label": "_EnablePunchoutV2",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"helpIconPosition": "Right",
																				"version": 0
																			},
																			"stamp": 28
																		},
																		"LabelEnableServiceBasedItem__": {
																			"type": "Label",
																			"data": [
																				"EnableServiceBasedItem__"
																			],
																			"options": {
																				"label": "_EnableServiceBasedItem",
																				"version": 0
																			},
																			"stamp": 29
																		},
																		"EnableServiceBasedItem__": {
																			"type": "CheckBox",
																			"data": [
																				"EnableServiceBasedItem__"
																			],
																			"options": {
																				"label": "_EnableServiceBasedItem",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"helpIconPosition": "Right",
																				"version": 0
																			},
																			"stamp": 30
																		},
																		"LabelEnableInventoryManagement__": {
																			"type": "Label",
																			"data": [
																				"EnableInventoryManagement__"
																			],
																			"options": {
																				"label": "_EnableInventoryManagement"
																			},
																			"stamp": 42
																		},
																		"EnableInventoryManagement__": {
																			"type": "CheckBox",
																			"data": [
																				"EnableInventoryManagement__"
																			],
																			"options": {
																				"label": "_EnableInventoryManagement",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"helpIconPosition": "Right",
																				"possibleValues": {},
																				"possibleKeys": {}
																			},
																			"stamp": 43
																		}
																	},
																	"stamp": 31
																}
															},
															"stamp": 32,
															"data": []
														}
													},
													"stamp": 33,
													"data": []
												}
											}
										}
									},
									"stamp": 34,
									"data": []
								}
							},
							"stamp": 35,
							"data": []
						}
					},
					"stamp": 36,
					"data": []
				},
				"form-footer": {
					"type": "FlexibleFormFooter",
					"options": {
						"splitterType": "simple-h",
						"version": 0
					},
					"*": {
						"Save": {
							"type": "SubmitButton",
							"options": {
								"label": "_Save",
								"action": "savequit",
								"submit": true,
								"version": 0,
								"shortCut": "S"
							},
							"stamp": 37,
							"data": []
						},
						"Close": {
							"type": "SubmitButton",
							"options": {
								"label": "_Close",
								"action": "cancel",
								"submit": true,
								"version": 0,
								"shortCut": "Q"
							},
							"stamp": 38,
							"data": []
						},
						"Delete": {
							"type": "SubmitButton",
							"options": {
								"label": "_Delete",
								"action": "delete",
								"submit": true,
								"version": 0,
								"disabled": true
							},
							"stamp": 39,
							"data": []
						}
					},
					"stamp": 40,
					"data": []
				}
			},
			"stamp": 41,
			"data": []
		}
	},
	"stamps": 43,
	"data": []
}