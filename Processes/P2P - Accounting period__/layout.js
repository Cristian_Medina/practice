{
	"*": {
		"Screen": {
			"type": "FlexibleFormLayout",
			"options": {
				"splitterType": "simple-v",
				"version": 0
			},
			"*": {
				"form-header": {
					"type": "FlexibleFormHeader",
					"options": {
						"hidePanelsMenu": false,
						"version": 0
					},
					"stamp": 2,
					"data": []
				},
				"form-center": {
					"type": "FlexibleFormCenter",
					"options": {
						"version": 0
					},
					"*": {
						"form-content": {
							"type": "FlexibleFormContent",
							"options": {
								"version": 0
							},
							"*": {
								"form-content-center": {
									"type": "FlexibleFormSplitter",
									"options": {
										"version": 1,
										"box": {
											"top": "0%",
											"left": "0%",
											"width": "100%"
										},
										"hidden": false
									},
									"*": {
										"form-content-center-1": {
											"type": "FlexibleFormLine",
											"data": [],
											"options": {},
											"stamp": 9,
											"*": {
												"DataPanel": {
													"type": "PanelData",
													"options": {
														"border": {},
														"version": 0,
														"labelLength": 0,
														"iconURL": "",
														"TitleFontSize": "S",
														"hideActionButtons": false,
														"hideTitleBar": false,
														"backgroundcolor": "default",
														"labelsAlignment": "right",
														"label": "_Document data"
													},
													"*": {
														"FieldsManager": {
															"type": "FieldsManager",
															"options": {
																"controlsAssociation": {
																	"CompanyCode__": "LabelCompany_code__",
																	"LabelCompany_code__": "CompanyCode__",
																	"PeriodName__": "LabelPeriod_name__",
																	"LabelPeriod_name__": "PeriodName__",
																	"PeriodCode__": "LabelPeriod_code__",
																	"LabelPeriod_code__": "PeriodCode__",
																	"PeriodStart__": "LabelPeriod_start__",
																	"LabelPeriod_start__": "PeriodStart__",
																	"PeriodEnd__": "LabelPeriod_end__",
																	"LabelPeriod_end__": "PeriodEnd__"
																},
																"version": 0
															},
															"*": {
																"Grid": {
																	"type": "GridLayout",
																	"options": {
																		"lines": 5,
																		"columns": 2,
																		"version": 0,
																		"ctrlsPos": {
																			"CompanyCode__": {
																				"line": 1,
																				"column": 2
																			},
																			"LabelCompany_code__": {
																				"line": 1,
																				"column": 1
																			},
																			"PeriodName__": {
																				"line": 3,
																				"column": 2
																			},
																			"LabelPeriod_name__": {
																				"line": 3,
																				"column": 1
																			},
																			"PeriodCode__": {
																				"line": 2,
																				"column": 2
																			},
																			"LabelPeriod_code__": {
																				"line": 2,
																				"column": 1
																			},
																			"PeriodStart__": {
																				"line": 4,
																				"column": 2
																			},
																			"LabelPeriod_start__": {
																				"line": 4,
																				"column": 1
																			},
																			"PeriodEnd__": {
																				"line": 5,
																				"column": 2
																			},
																			"LabelPeriod_end__": {
																				"line": 5,
																				"column": 1
																			}
																		},
																		"colspans": [
																			[],
																			[],
																			[],
																			[],
																			[]
																		]
																	},
																	"data": [
																		"fields"
																	],
																	"*": {
																		"LabelCompany_code__": {
																			"type": "Label",
																			"data": [
																				"CompanyCode__"
																			],
																			"options": {
																				"label": "_Company code"
																			},
																			"stamp": 16
																		},
																		"CompanyCode__": {
																			"type": "DatabaseComboBox",
																			"data": [
																				"CompanyCode__"
																			],
																			"options": {
																				"browsable": true,
																				"label": "_Company code",
																				"activable": true,
																				"width": 230,
																				"PreFillFTS": true,
																				"fTSmaxRecords": "20",
																				"length": 20
																			},
																			"stamp": 17
																		},
																		"LabelPeriod_code__": {
																			"type": "Label",
																			"data": [
																				"PeriodCode__"
																			],
																			"options": {
																				"label": "_Period code"
																			},
																			"stamp": 18
																		},
																		"PeriodCode__": {
																			"type": "ShortText",
																			"data": [
																				"PeriodCode__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "_Period code",
																				"activable": true,
																				"width": 230,
																				"browsable": false
																			},
																			"stamp": 19
																		},
																		"LabelPeriod_name__": {
																			"type": "Label",
																			"data": [
																				"PeriodName__"
																			],
																			"options": {
																				"label": "_Period name"
																			},
																			"stamp": 20
																		},
																		"PeriodName__": {
																			"type": "ShortText",
																			"data": [
																				"PeriodName__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "_Period name",
																				"activable": true,
																				"width": 230,
																				"browsable": false
																			},
																			"stamp": 21
																		},
																		"LabelPeriod_start__": {
																			"type": "Label",
																			"data": [
																				"PeriodStart__"
																			],
																			"options": {
																				"label": "_Period start"
																			},
																			"stamp": 22
																		},
																		"PeriodStart__": {
																			"type": "DateTime",
																			"data": [
																				"PeriodStart__"
																			],
																			"options": {
																				"displayLongFormat": false,
																				"label": "_Period start",
																				"activable": true,
																				"width": 230
																			},
																			"stamp": 23
																		},
																		"LabelPeriod_end__": {
																			"type": "Label",
																			"data": [
																				"PeriodEnd__"
																			],
																			"options": {
																				"label": "_Period end"
																			},
																			"stamp": 24
																		},
																		"PeriodEnd__": {
																			"type": "DateTime",
																			"data": [
																				"PeriodEnd__"
																			],
																			"options": {
																				"displayLongFormat": false,
																				"label": "_Period end",
																				"activable": true,
																				"width": 230
																			},
																			"stamp": 25
																		}
																	},
																	"stamp": 8
																}
															},
															"stamp": 7,
															"data": []
														}
													},
													"stamp": 6,
													"data": []
												}
											}
										}
									},
									"stamp": 5,
									"data": []
								}
							},
							"stamp": 4,
							"data": []
						}
					},
					"stamp": 3,
					"data": []
				},
				"form-footer": {
					"type": "FlexibleFormFooter",
					"options": {
						"splitterType": "simple-h",
						"version": 0
					},
					"*": {
						"Save": {
							"type": "SubmitButton",
							"options": {
								"label": "_Save",
								"action": "savequit",
								"submit": true,
								"version": 0
							},
							"stamp": 11,
							"data": []
						},
						"Close": {
							"type": "SubmitButton",
							"options": {
								"label": "_Close",
								"action": "cancel",
								"submit": true,
								"version": 0
							},
							"stamp": 12,
							"data": []
						},
						"Delete": {
							"type": "SubmitButton",
							"options": {
								"label": "_Delete",
								"action": "delete",
								"submit": true,
								"version": 0,
								"disabled": true
							},
							"stamp": 13,
							"data": []
						}
					},
					"stamp": 10,
					"data": []
				}
			},
			"stamp": 1,
			"data": []
		}
	},
	"stamps": 25,
	"data": []
}