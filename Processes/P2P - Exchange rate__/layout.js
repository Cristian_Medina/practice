{
	"*": {
		"Screen": {
			"type": "FlexibleFormLayout",
			"options": {
				"splitterType": "simple-v",
				"version": 0
			},
			"*": {
				"form-header": {
					"type": "FlexibleFormHeader",
					"options": {
						"hidePanelsMenu": false,
						"version": 0
					},
					"stamp": 2,
					"data": []
				},
				"form-center": {
					"type": "FlexibleFormCenter",
					"options": {
						"version": 0
					},
					"*": {
						"form-content": {
							"type": "FlexibleFormContent",
							"options": {
								"version": 0
							},
							"*": {
								"form-content-center": {
									"type": "FlexibleFormSplitter",
									"options": {
										"version": 1,
										"box": {
											"top": "0%",
											"left": "0%",
											"width": "100%"
										},
										"hidden": false
									},
									"*": {
										"form-content-center-1": {
											"type": "FlexibleFormLine",
											"data": [],
											"options": {
												"version": 0
											},
											"stamp": 9,
											"*": {
												"DataPanel": {
													"type": "PanelData",
													"options": {
														"border": {},
														"version": 0,
														"labelLength": 0,
														"iconURL": "",
														"TitleFontSize": "S",
														"hideActionButtons": false,
														"hideTitleBar": false,
														"backgroundcolor": "default",
														"labelsAlignment": "right",
														"label": "_Document data"
													},
													"*": {
														"FieldsManager": {
															"type": "FieldsManager",
															"options": {
																"controlsAssociation": {
																	"CompanyCode__": "LabelCompany_code__",
																	"LabelCompany_code__": "CompanyCode__",
																	"RatioFrom__": "LabelRatio_from__",
																	"LabelRatio_from__": "RatioFrom__",
																	"RatioTo__": "LabelRatio_to__",
																	"LabelRatio_to__": "RatioTo__",
																	"Rate__": "LabelRate__",
																	"LabelRate__": "Rate__",
																	"CurrencyFrom__": "LabelCurrency_from__",
																	"LabelCurrency_from__": "CurrencyFrom__"
																},
																"version": 0
															},
															"*": {
																"Grid": {
																	"type": "GridLayout",
																	"options": {
																		"lines": 5,
																		"columns": 2,
																		"version": 0,
																		"ctrlsPos": {
																			"CompanyCode__": {
																				"line": 1,
																				"column": 2
																			},
																			"LabelCompany_code__": {
																				"line": 1,
																				"column": 1
																			},
																			"RatioFrom__": {
																				"line": 3,
																				"column": 2
																			},
																			"LabelRatio_from__": {
																				"line": 3,
																				"column": 1
																			},
																			"RatioTo__": {
																				"line": 4,
																				"column": 2
																			},
																			"LabelRatio_to__": {
																				"line": 4,
																				"column": 1
																			},
																			"Rate__": {
																				"line": 5,
																				"column": 2
																			},
																			"LabelRate__": {
																				"line": 5,
																				"column": 1
																			},
																			"CurrencyFrom__": {
																				"line": 2,
																				"column": 2
																			},
																			"LabelCurrency_from__": {
																				"line": 2,
																				"column": 1
																			}
																		},
																		"colspans": [
																			[],
																			[],
																			[],
																			[],
																			[]
																		]
																	},
																	"data": [
																		"fields"
																	],
																	"*": {
																		"LabelCompany_code__": {
																			"type": "Label",
																			"data": [
																				"Select_from_a_table__"
																			],
																			"options": {
																				"label": "Company code",
																				"version": 0
																			},
																			"stamp": 16
																		},
																		"CompanyCode__": {
																			"type": "DatabaseComboBox",
																			"data": [
																				"CompanyCode__"
																			],
																			"options": {
																				"browsable": true,
																				"label": "Company code",
																				"activable": true,
																				"width": 230,
																				"PreFillFTS": true,
																				"fTSmaxRecords": "20",
																				"version": 0
																			},
																			"stamp": 17
																		},
																		"LabelCurrency_from__": {
																			"type": "Label",
																			"data": [
																				"CurrencyFrom__"
																			],
																			"options": {
																				"label": "Currency from",
																				"version": 0
																			},
																			"stamp": 18
																		},
																		"CurrencyFrom__": {
																			"type": "ShortText",
																			"data": [
																				"CurrencyFrom__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "Currency from",
																				"activable": true,
																				"width": 230,
																				"browsable": false,
																				"version": 0
																			},
																			"stamp": 19
																		},
																		"LabelRatio_from__": {
																			"type": "Label",
																			"data": [
																				"RatioFrom__"
																			],
																			"options": {
																				"label": "Ratio from",
																				"version": 0
																			},
																			"stamp": 24
																		},
																		"RatioFrom__": {
																			"type": "Integer",
																			"data": [
																				"RatioFrom__"
																			],
																			"options": {
																				"version": 1,
																				"textSize": "S",
																				"textAlignment": "right",
																				"textStyle": "default",
																				"textColor": "default",
																				"integer": true,
																				"label": "Ratio from",
																				"precision_internal": 0,
																				"activable": true,
																				"width": 230
																			},
																			"stamp": 25
																		},
																		"LabelRatio_to__": {
																			"type": "Label",
																			"data": [
																				"RatioTo__"
																			],
																			"options": {
																				"label": "Ratio to",
																				"version": 0
																			},
																			"stamp": 26
																		},
																		"RatioTo__": {
																			"type": "Integer",
																			"data": [
																				"RatioTo__"
																			],
																			"options": {
																				"version": 1,
																				"textSize": "S",
																				"textAlignment": "right",
																				"textStyle": "default",
																				"textColor": "default",
																				"integer": true,
																				"label": "Ratio to",
																				"precision_internal": 0,
																				"activable": true,
																				"width": 230
																			},
																			"stamp": 27
																		},
																		"LabelRate__": {
																			"type": "Label",
																			"data": [
																				"Rate__"
																			],
																			"options": {
																				"label": "Rate",
																				"version": 0
																			},
																			"stamp": 28
																		},
																		"Rate__": {
																			"type": "Decimal",
																			"data": [
																				"Rate__"
																			],
																			"options": {
																				"version": 1,
																				"textSize": "S",
																				"textAlignment": "right",
																				"textStyle": "default",
																				"textColor": "default",
																				"integer": false,
																				"label": "Rate",
																				"precision_internal": 5,
																				"activable": true,
																				"width": 230
																			},
																			"stamp": 29
																		}
																	},
																	"stamp": 8
																}
															},
															"stamp": 7,
															"data": []
														}
													},
													"stamp": 6,
													"data": []
												}
											}
										}
									},
									"stamp": 5,
									"data": []
								}
							},
							"stamp": 4,
							"data": []
						}
					},
					"stamp": 3,
					"data": []
				},
				"form-footer": {
					"type": "FlexibleFormFooter",
					"options": {
						"splitterType": "simple-h",
						"version": 0
					},
					"*": {
						"Save": {
							"type": "SubmitButton",
							"options": {
								"label": "_Save",
								"action": "savequit",
								"submit": true,
								"version": 0
							},
							"stamp": 11,
							"data": []
						},
						"Close": {
							"type": "SubmitButton",
							"options": {
								"label": "_Close",
								"action": "cancel",
								"submit": true,
								"version": 0
							},
							"stamp": 12,
							"data": []
						},
						"Delete": {
							"type": "SubmitButton",
							"options": {
								"label": "_Delete",
								"action": "delete",
								"submit": true,
								"version": 0,
								"disabled": true
							},
							"stamp": 13,
							"data": []
						}
					},
					"stamp": 10,
					"data": []
				}
			},
			"stamp": 1,
			"data": []
		}
	},
	"stamps": 29,
	"data": []
}