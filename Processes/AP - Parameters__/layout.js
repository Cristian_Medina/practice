{
	"*": {
		"Screen": {
			"type": "FlexibleFormLayout",
			"options": {
				"splitterType": "simple-v",
				"version": 0
			},
			"*": {
				"form-header": {
					"type": "FlexibleFormHeader",
					"options": {
						"hidePanelsMenu": false,
						"version": 0
					},
					"stamp": 1,
					"data": []
				},
				"form-center": {
					"type": "FlexibleFormCenter",
					"options": {
						"version": 0
					},
					"*": {
						"form-content": {
							"type": "FlexibleFormContent",
							"options": {
								"version": 0
							},
							"*": {
								"form-content-center": {
									"type": "FlexibleFormSplitter",
									"options": {
										"version": 1,
										"box": {
											"top": "0%",
											"left": "0%",
											"width": "100%"
										},
										"hidden": false
									},
									"*": {
										"form-content-center-1": {
											"type": "FlexibleFormLine",
											"data": [],
											"options": {
												"version": 0
											},
											"stamp": 2,
											"*": {
												"DataPanel": {
													"type": "PanelData",
													"options": {
														"border": {},
														"version": 0,
														"labelLength": 0,
														"label": "_Document data",
														"iconURL": "",
														"TitleFontSize": "S",
														"hideActionButtons": true,
														"hideTitleBar": true,
														"hideTitle": true,
														"backgroundcolor": "default",
														"labelsAlignment": "right",
														"leftImageURL": ""
													},
													"*": {
														"FieldsManager": {
															"type": "FieldsManager",
															"options": {
																"controlsAssociation": {
																	"DuplicateCheckAlertLevel__": "LabelDuplicateCheckAlertLevel__",
																	"LabelDuplicateCheckAlertLevel__": "DuplicateCheckAlertLevel__",
																	"TouchlessEnabled__": "LabelTouchlessEnabled__",
																	"LabelTouchlessEnabled__": "TouchlessEnabled__",
																	"Template__": "LabelTemplate__",
																	"LabelTemplate__": "Template__",
																	"CompanyCode__": "LabelCompanyCode__",
																	"LabelCompanyCode__": "CompanyCode__",
																	"VendorNumber__": "LabelVendorNumber__",
																	"LabelVendorNumber__": "VendorNumber__",
																	"LineItemsImporterMapping__": "LabelLineItemsImporterMapping__",
																	"LabelLineItemsImporterMapping__": "LineItemsImporterMapping__"
																},
																"version": 0
															},
															"*": {
																"Grid": {
																	"type": "GridLayout",
																	"options": {
																		"lines": 6,
																		"columns": 2,
																		"version": 0,
																		"ctrlsPos": {
																			"DuplicateCheckAlertLevel__": {
																				"line": 3,
																				"column": 2
																			},
																			"LabelDuplicateCheckAlertLevel__": {
																				"line": 3,
																				"column": 1
																			},
																			"TouchlessEnabled__": {
																				"line": 5,
																				"column": 2
																			},
																			"LabelTouchlessEnabled__": {
																				"line": 5,
																				"column": 1
																			},
																			"Template__": {
																				"line": 4,
																				"column": 2
																			},
																			"LabelTemplate__": {
																				"line": 4,
																				"column": 1
																			},
																			"CompanyCode__": {
																				"line": 1,
																				"column": 2
																			},
																			"LabelCompanyCode__": {
																				"line": 1,
																				"column": 1
																			},
																			"VendorNumber__": {
																				"line": 2,
																				"column": 2
																			},
																			"LabelVendorNumber__": {
																				"line": 2,
																				"column": 1
																			},
																			"LineItemsImporterMapping__": {
																				"line": 6,
																				"column": 2
																			},
																			"LabelLineItemsImporterMapping__": {
																				"line": 6,
																				"column": 1
																			}
																		},
																		"colspans": [
																			[],
																			[],
																			[],
																			[],
																			[],
																			[]
																		]
																	},
																	"data": [
																		"fields"
																	],
																	"*": {
																		"LabelCompanyCode__": {
																			"type": "Label",
																			"data": [
																				"CompanyCode__"
																			],
																			"options": {
																				"label": "_CompanyCode",
																				"version": 0
																			},
																			"stamp": 3
																		},
																		"CompanyCode__": {
																			"type": "DatabaseComboBox",
																			"data": [
																				"CompanyCode__"
																			],
																			"options": {
																				"label": "_CompanyCode",
																				"activable": true,
																				"width": 500,
																				"fTSmaxRecords": "20",
																				"browsable": true,
																				"version": 0
																			},
																			"stamp": 4
																		},
																		"LabelVendorNumber__": {
																			"type": "Label",
																			"data": [
																				"VendorNumber__"
																			],
																			"options": {
																				"label": "_VendorNumber",
																				"version": 0
																			},
																			"stamp": 5
																		},
																		"VendorNumber__": {
																			"type": "DatabaseComboBox",
																			"data": [
																				"VendorNumber__"
																			],
																			"options": {
																				"label": "_VendorNumber",
																				"activable": true,
																				"width": "500",
																				"fTSmaxRecords": "20",
																				"browsable": true,
																				"version": 0
																			},
																			"stamp": 6
																		},
																		"LabelDuplicateCheckAlertLevel__": {
																			"type": "Label",
																			"data": [
																				"DuplicateCheckAlertLevel__"
																			],
																			"options": {
																				"label": "Alert on duplicates",
																				"version": 0
																			},
																			"stamp": 7
																		},
																		"DuplicateCheckAlertLevel__": {
																			"type": "ComboBox",
																			"data": [
																				"DuplicateCheckAlertLevel__"
																			],
																			"options": {
																				"possibleValues": {
																					"0": "If at least 1 value is identical",
																					"1": "If at least 2 values are identical",
																					"2": "If at least 3 values are identical",
																					"3": "Do not alert on duplicates"
																				},
																				"keyValueMode": true,
																				"possibleKeys": {
																					"0": "1",
																					"1": "2",
																					"2": "3",
																					"3": "0"
																				},
																				"label": "Alert on duplicates",
																				"activable": true,
																				"width": "500",
																				"version": 0
																			},
																			"stamp": 8
																		},
																		"LabelTemplate__": {
																			"type": "Label",
																			"data": [
																				"Template__"
																			],
																			"options": {
																				"label": "_Template",
																				"version": 0
																			},
																			"stamp": 9
																		},
																		"Template__": {
																			"type": "DatabaseComboBox",
																			"data": [
																				"Template__"
																			],
																			"options": {
																				"label": "_Template",
																				"activable": true,
																				"width": 500,
																				"fTSmaxRecords": "20",
																				"browsable": true,
																				"version": 0
																			},
																			"stamp": 10
																		},
																		"LabelTouchlessEnabled__": {
																			"type": "Label",
																			"data": [
																				"TouchlessEnabled__"
																			],
																			"options": {
																				"label": "Enable touchless",
																				"version": 0
																			},
																			"stamp": 11
																		},
																		"TouchlessEnabled__": {
																			"type": "CheckBox",
																			"data": [
																				"TouchlessEnabled__"
																			],
																			"options": {
																				"label": "Enable touchless",
																				"activable": true,
																				"version": 0
																			},
																			"stamp": 12
																		},
																		"LabelLineItemsImporterMapping__": {
																			"type": "Label",
																			"data": [
																				"LineItemsImporterMapping__"
																			],
																			"options": {
																				"label": "_LineItemsImporterMapping",
																				"version": 0
																			},
																			"stamp": 26
																		},
																		"LineItemsImporterMapping__": {
																			"type": "ShortText",
																			"data": [
																				"LineItemsImporterMapping__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "_LineItemsImporterMapping",
																				"activable": true,
																				"width": "600",
																				"version": 0,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"browsable": false
																			},
																			"stamp": 27
																		}
																	},
																	"stamp": 15
																}
															},
															"stamp": 16,
															"data": []
														}
													},
													"stamp": 17,
													"data": []
												}
											}
										}
									},
									"stamp": 18,
									"data": []
								}
							},
							"stamp": 19,
							"data": []
						}
					},
					"stamp": 20,
					"data": []
				},
				"form-footer": {
					"type": "FlexibleFormFooter",
					"options": {
						"splitterType": "simple-h",
						"version": 0
					},
					"*": {
						"Save": {
							"type": "SubmitButton",
							"options": {
								"label": "_Save",
								"action": "savequit",
								"submit": true,
								"version": 0
							},
							"stamp": 21,
							"data": []
						},
						"Close": {
							"type": "SubmitButton",
							"options": {
								"label": "_Close",
								"action": "cancel",
								"submit": true,
								"version": 0
							},
							"stamp": 22,
							"data": []
						},
						"Delete": {
							"type": "SubmitButton",
							"options": {
								"label": "_Delete",
								"action": "delete",
								"submit": true,
								"version": 0,
								"disabled": true
							},
							"stamp": 23,
							"data": []
						}
					},
					"stamp": 24,
					"data": []
				}
			},
			"stamp": 25,
			"data": []
		}
	},
	"stamps": 27,
	"data": []
}