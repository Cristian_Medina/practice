///#GLOBALS Lib
var currentActionName = Data.GetActionName();
var currentActionType = Data.GetActionType();
Log.Info("-- CO Validation Script -- Name: '" + (currentActionName ? currentActionName : "<empty>") + "', Action: '" + (currentActionType ? currentActionType : "<empty>") + "'");
if (!currentActionName) {
    var login = Variable.GetValueAsString("VendorLogin");
    Log.Info("First validation - forward to vendor '" + login + "'");
    Process.Forward(login);
    Process.LeaveForm();
}
else if (currentActionType === "approve" && (currentActionName === "Confirm" || currentActionName === "Reject")) {
    // Update the PO confirmation datetime
    Query.Reset();
    var confirmationDateTime = Sys.Helpers.Date.Date2DBDateTime(new Date());
    var _query = Process.CreateQueryAsProcessAdmin();
    var queryFilter = "(OrderNumber__=" + Data.GetValue("Sales_Order_Number__") + ")";
    _query.SetSpecificTable("CDNAME#" + Lib.P2P.GetPOProcessName());
    _query.SetFilter(queryFilter);
    Log.Info("Looking for Purchase Order matching filter:" + queryFilter);
    if (_query.MoveFirst()) {
        var purchaseOrder = _query.MoveNext();
        if (purchaseOrder) {
            var vars = purchaseOrder.GetUninheritedVars();
            vars.AddValue_String("ConfirmationDatetime__", confirmationDateTime, true);
            try {
                purchaseOrder.Process();
                Log.Info("Purchase order updated successfully.");
                // Publish confirmation on the current conversation
                Lib.Purchasing.AddConversationItem(currentActionName === "Confirm" ? Language.Translate("_CustomerOrderAccepted") : Language.Translate("_CustomerOrderRejected"), Lib.Purchasing.ConversationTypes.GeneralInformation, true, Data.GetValue("Sales_Order_Number__"));
                if (currentActionName === "Reject") {
                    Data.SetValue("State", 400);
                    // Update the rejection datetime
                    Log.Info("Setting 'RejectionDatetime__' to '" + confirmationDateTime + "'");
                    Data.SetValue("RejectionDatetime__", confirmationDateTime);
                }
                else {
                    // Update the confirmation datetime
                    Log.Info("Setting 'ConfirmationDatetime__' to '" + confirmationDateTime + "'");
                    Data.SetValue("ConfirmationDatetime__", confirmationDateTime);
                }
            }
            catch (e) {
                Log.Info("Failed to update purchase order: " + e);
                Lib.CommonDialog.NextAlert.Define("_PO update error", "_UpdatePOfailed");
                Process.PreventApproval();
            }
        }
        else {
            Log.Info("Purchase order not found !");
        }
    }
    else {
        Log.Info("MoveFirst failed: " + _query.GetLastErrorMessage());
        Process.PreventApproval();
    }
}
