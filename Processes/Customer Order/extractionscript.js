///#GLOBALS Lib
// Load Parameter in extractionScript to avoid access issue from HTML customerScript.
Lib.P2P.CompanyCodesValue.QueryValues(Data.GetValue("CompanyCode__")).Then(function (CCValues) {
    if (Object.keys(CCValues).length > 0) {
        Sys.Parameters.GetInstance("PAC").Reload(CCValues.DefaultConfiguration__);
    }
    else {
        Log.Error("The requested company code is not in the company code table.");
    }
});
Variable.SetValueAsString("containsQuantityBasedItem", "0");
Lib.Purchasing.POItems.CheckContainsQuantityBasedItem(Data.GetValue("OrderNumber__")).Then(function (containsQuantityBasedItem) {
    Variable.SetValueAsString("containsQuantityBasedItem", containsQuantityBasedItem ? "1" : "0");
});
if (Variable.GetValueAsString("SendNotification") === "1") {
    var currUser = Users.GetUser(Variable.GetValueAsString("VendorLogin"));
    var vendorLogin = currUser.GetValue("Login");
    var vendorEmail = currUser.GetValue("emailaddress");
    var buyerComment = Variable.GetValueAsString("BuyerComment__");
    var customTag = null;
    if (buyerComment.length > 0) {
        customTag = {
            "AdditionalMessage_START__": "",
            "AdditionalMessage_END__": "",
            "PortalUrl": "www.esker.fr"
        };
        customTag.AdditionalMessage = buyerComment;
    }
    else {
        customTag = {
            "AdditionalMessage_START__": "<!--",
            "AdditionalMessage_END__": "-->"
        };
    }
    var email = null;
    var curProcessRuid = Data.GetValue("RuidEx");
    customTag.PortalUrl = currUser.GetProcessURL(curProcessRuid, true);
    email = Sys.EmailNotification.CreateEmail(vendorLogin, vendorEmail, null, "Purchasing_Email_NotifVendorPortal.htm", customTag, Sys.Parameters.GetInstance("AP").GetParameter("SendNotificationsToEachGroupMembers__") === "1");
    var CcEmailAddress = Variable.GetValueAsString("EmailCarbonCopy__");
    if (!Sys.Helpers.IsEmpty(CcEmailAddress)) {
        CcEmailAddress = CcEmailAddress.split("\n").join(";");
        Sys.EmailNotification.AddCC(email, CcEmailAddress);
    }
    if (Sys.Parameters.GetInstance("PAC").GetParameter("AlwaysAttachPurchaseOrder") === "1") {
        var sendAttachments = Sys.Parameters.GetInstance("PAC").GetParameter("SendPOAttachments") === true;
        var nbAttach = Attach.GetNbAttach();
        for (var i = 0; i < nbAttach; i++) {
            var isQuote = Attach.GetValue(i, "Purchasing_IsQuote");
            var isPO = Attach.GetValue(i, "Purchasing_IsPO");
            var attachFile = Attach.GetConvertedFile(i) || Attach.GetInputFile(i);
            var name = Attach.GetName(i);
            if (isQuote === "True") {
                Log.Info("Attaching quote " + i + " to vendor notif: " + name);
                Sys.EmailNotification.AddAttach(email, attachFile, name);
            }
            else if (isPO === "True") {
                Log.Info("Attaching PO " + i + " to vendor notif: " + name);
                Sys.EmailNotification.AddAttach(email, attachFile, "PO" + "_" + Data.GetValue("OrderNumber__"));
            }
            else if (sendAttachments) {
                Log.Info("Attaching attachment " + i + " to vendor notif: " + name);
                Sys.EmailNotification.AddAttach(email, attachFile, name);
            }
        }
    }
    Sys.EmailNotification.SendEmail(email);
}
