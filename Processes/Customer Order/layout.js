{
	"*": {
		"GDRData": {
			"type": "GDRData",
			"options": {
				"version": 0
			},
			"stamp": 1,
			"data": []
		},
		"OCRParams": {
			"type": "OCRParams",
			"options": {
				"version": 0
			},
			"stamp": 2,
			"data": []
		},
		"Screen": {
			"type": "FlexibleFormLayout",
			"options": {
				"splitterType": "complex",
				"version": 0,
				"maxwidth": "",
				"backColor": "text-backgroundcolor-color7",
				"style": "FlexibleFormLight"
			},
			"*": {
				"form-header": {
					"type": "FlexibleFormHeader",
					"options": {
						"version": 0,
						"maxwidth": ""
					},
					"stamp": 3,
					"data": []
				},
				"form-center": {
					"type": "FlexibleFormCenter",
					"options": {
						"version": 0,
						"maxwidth": ""
					},
					"*": {
						"form-content": {
							"type": "FlexibleFormContent",
							"options": {
								"version": 0
							},
							"*": {
								"form-content-top": {
									"type": "FlexibleFormSplitter",
									"options": {
										"autosize": true,
										"version": 1,
										"box": {
											"top": "0%",
											"left": "0%",
											"width": null,
											"height": "0%"
										},
										"hidden": true
									},
									"*": {},
									"stamp": 4,
									"data": []
								},
								"form-content-bottom": {
									"type": "FlexibleFormSplitter",
									"options": {
										"version": 1,
										"box": {
											"top": "0%",
											"left": "0%",
											"width": null,
											"height": "100%"
										},
										"hidden": false,
										"hideSeparator": true
									},
									"*": {
										"form-content-left": {
											"type": "FlexibleFormSplitter",
											"options": {
												"version": 1,
												"box": {
													"top": "0%",
													"left": "0%",
													"width": "44%",
													"height": null
												},
												"hidden": false,
												"hideSeparator": true
											},
											"*": {
												"form-content-left-3": {
													"type": "FlexibleFormLine",
													"options": {
														"version": 0
													},
													"*": {
														"NextProcess": {
															"type": "PanelNextProcess",
															"options": {
																"hidden": true,
																"version": 0,
																"border": {},
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": false,
																"hideTitleBar": false,
																"label": "_Next Process",
																"hideTitle": false,
																"sameHeightAsSiblings": false
															},
															"stamp": 5,
															"data": []
														}
													},
													"stamp": 6,
													"data": []
												},
												"form-content-left-1": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 7,
													"*": {
														"DocumentsPanel": {
															"type": "PanelDocument",
															"options": {
																"version": 0,
																"border": {},
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "L",
																"hideActionButtons": true,
																"hideTitleBar": false,
																"label": "_Documents",
																"hidden": false,
																"hideTitle": false,
																"sameHeightAsSiblings": false
															},
															"*": {
																"Document": {
																	"data": [
																		"documentscontroldata"
																	],
																	"type": "Documents",
																	"options": {
																		"version": 0,
																		"downloadButton": true
																	},
																	"stamp": 8
																}
															},
															"stamp": 9,
															"data": []
														}
													}
												},
												"form-content-left-2": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 10,
													"*": {
														"PreviewPanel": {
															"type": "PanelPreview",
															"options": {
																"border": {},
																"version": 0,
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": false,
																"hideTitleBar": false,
																"label": "_Document Preview",
																"hideTitle": false,
																"sameHeightAsSiblings": false
															},
															"*": {
																"Preview": {
																	"data": [
																		"previewdata"
																	],
																	"type": "Preview",
																	"options": {
																		"version": 0
																	},
																	"stamp": 11
																}
															},
															"stamp": 12,
															"data": []
														}
													}
												}
											},
											"stamp": 13,
											"data": []
										},
										"form-content-right": {
											"type": "FlexibleFormSplitter",
											"options": {
												"version": 1,
												"box": {
													"top": "0%",
													"left": "44%",
													"width": "56%",
													"height": null
												},
												"hidden": false
											},
											"*": {
												"form-content-right-2": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 14,
													"*": {
														"Banner": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"iconURL": "",
																"TitleFontSize": "L",
																"hideActionButtons": false,
																"hideTitleBar": true,
																"backgroundcolor": "color7",
																"labelsAlignment": "right",
																"label": "_Banner",
																"leftImageURL": "",
																"version": 0,
																"hideTitle": false,
																"sameHeightAsSiblings": false,
																"removeMargins": false,
																"elementsAlignment": "left",
																"labelLength": 0
															},
															"stamp": 15,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {},
																		"version": 0
																	},
																	"stamp": 16,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"HTMLBanner__": {
																						"line": 1,
																						"column": 1
																					}
																				},
																				"lines": 1,
																				"columns": 2,
																				"colspans": [
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					]
																				],
																				"version": 0
																			},
																			"stamp": 17,
																			"*": {
																				"HTMLBanner__": {
																					"type": "HTML",
																					"data": false,
																					"options": {
																						"label": "",
																						"width": 683,
																						"htmlContent": "Customer order",
																						"version": 0,
																						"css": "@ span {line-height: 44px;}@ "
																					},
																					"stamp": 18
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-right-7": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 19,
													"*": {
														"Action": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": true,
																"hideTitleBar": true,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "_Action",
																"leftImageURL": "",
																"version": 0,
																"hideTitle": false,
																"sameHeightAsSiblings": false,
																"removeMargins": true,
																"elementsAlignment": "left",
																"labelLength": 0
															},
															"stamp": 20,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"stamp": 21,
																	"*": {
																		"Grid": {
																			"type": "HorizontalLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"displayAsFlex": true,
																				"ctrlsPos": {
																					"Confirm__": {
																						"line": 2,
																						"column": 1
																					},
																					"Share__": {
																						"line": 5,
																						"column": 1
																					},
																					"Quit__": {
																						"line": 9,
																						"column": 1
																					},
																					"InvoiceThisOrder__": {
																						"line": 8,
																						"column": 1
																					},
																					"Change__": {
																						"line": 3,
																						"column": 1
																					},
																					"Reject__": {
																						"line": 4,
																						"column": 1
																					},
																					"ServiceEntrySheet__": {
																						"line": 6,
																						"column": 1
																					},
																					"CreateAdvancedShippingNotice__": {
																						"line": 7,
																						"column": 1
																					},
																					"Spacer3__": {
																						"line": 1,
																						"column": 1
																					}
																				},
																				"lines": 9,
																				"version": 0,
																				"colspans": [
																					[
																						[
																							{
																								"index": 0,
																								"length": 2
																							}
																						]
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					]
																				]
																			},
																			"stamp": 22,
																			"*": {
																				"Spacer3__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "10px",
																						"width": "100%",
																						"color": "default",
																						"label": "_Spacer3",
																						"version": 0
																					},
																					"stamp": 147
																				},
																				"Confirm__": {
																					"type": "FormButton",
																					"data": false,
																					"options": {
																						"buttonLabel": "_Confirm",
																						"label": "Button",
																						"urlImage": "",
																						"style": 5,
																						"width": "190",
																						"version": 0,
																						"textPosition": "text-below",
																						"textSize": "MS",
																						"textStyle": "default",
																						"textColor": "color1",
																						"nextprocess": {
																							"processName": "AP - Application Settings Wizard",
																							"attachmentsMode": "all",
																							"willBeChild": true,
																							"returnToOriginalUrl": false
																						},
																						"iconColor": "color1",
																						"urlImageOverlay": "",
																						"awesomeClasses": "esk-ifont-approve-check-circle fa-4x ",
																						"action": "none",
																						"url": ""
																					},
																					"stamp": 23
																				},
																				"Change__": {
																					"type": "FormButton",
																					"data": false,
																					"options": {
																						"buttonLabel": "_Change",
																						"label": "_Button",
																						"textPosition": "text-below",
																						"textSize": "MS",
																						"textStyle": "default",
																						"textColor": "color1",
																						"nextprocess": {
																							"processName": "EditPORequest",
																							"attachmentsMode": "all",
																							"willBeChild": false,
																							"returnToOriginalUrl": true
																						},
																						"urlImageOverlay": "",
																						"width": "190",
																						"action": "openprocess",
																						"url": "",
																						"urlImage": "",
																						"style": 5,
																						"version": 0,
																						"iconColor": "color1",
																						"awesomeClasses": "esk-ifont-edit-document-circle fa-4x"
																					},
																					"stamp": 24
																				},
																				"Reject__": {
																					"type": "FormButton",
																					"data": false,
																					"options": {
																						"buttonLabel": "_Reject",
																						"label": "_Button",
																						"textPosition": "text-below",
																						"textSize": "MS",
																						"textStyle": "default",
																						"textColor": "color1",
																						"nextprocess": {
																							"processName": "AP - Application Settings Wizard",
																							"attachmentsMode": "all",
																							"willBeChild": true,
																							"returnToOriginalUrl": false
																						},
																						"urlImage": "",
																						"urlImageOverlay": "",
																						"style": 5,
																						"width": "190",
																						"action": "none",
																						"url": "",
																						"version": 0,
																						"iconColor": "color1",
																						"awesomeClasses": "esk-ifont-reject-document-circle fa-4x"
																					},
																					"stamp": 25
																				},
																				"Share__": {
																					"type": "FormButton",
																					"data": false,
																					"options": {
																						"buttonLabel": "_Share",
																						"label": "Button",
																						"urlImage": "",
																						"style": 5,
																						"width": "190px",
																						"version": 0,
																						"textPosition": "text-below",
																						"textSize": "MS",
																						"textStyle": "default",
																						"textColor": "color1",
																						"iconColor": "color1",
																						"nextprocess": {
																							"processName": "AP - Application Settings Wizard",
																							"attachmentsMode": "all",
																							"willBeChild": true,
																							"returnToOriginalUrl": false
																						},
																						"urlImageOverlay": "",
																						"hidden": true,
																						"awesomeClasses": "esk-ifont-share-circle fa-4x",
																						"action": "none",
																						"url": ""
																					},
																					"stamp": 26
																				},
																				"ServiceEntrySheet__": {
																					"type": "FormButton",
																					"data": false,
																					"options": {
																						"buttonLabel": "_New Service Entry Sheet",
																						"label": "_New Service Entry Sheet",
																						"urlImage": "",
																						"style": 5,
																						"textPosition": "text-below",
																						"textSize": "MS",
																						"textStyle": "default",
																						"textColor": "color1",
																						"iconColor": "color1",
																						"nextprocess": {
																							"processName": "AP - Application Settings Wizard",
																							"attachmentsMode": "all",
																							"willBeChild": true,
																							"returnToOriginalUrl": false
																						},
																						"urlImageOverlay": "",
																						"hidden": true,
																						"awesomeClasses": "esk-ifont-new-ses-circle fa-4x",
																						"action": "none",
																						"url": "",
																						"version": 0,
																						"width": "190px"
																					},
																					"stamp": 28
																				},
																				"Quit__": {
																					"type": "FormButton",
																					"data": false,
																					"options": {
																						"buttonLabel": "_Quit",
																						"label": "Button2",
																						"urlImage": "",
																						"style": 5,
																						"width": "190px",
																						"version": 0,
																						"textPosition": "text-below",
																						"textSize": "MS",
																						"textStyle": "default",
																						"textColor": "color1",
																						"nextprocess": {
																							"processName": {
																								"processName": "",
																								"attachmentsMode": "all",
																								"willBeChild": true,
																								"returnToOriginalUrl": false
																							},
																							"attachmentsMode": "all",
																							"willBeChild": true,
																							"returnToOriginalUrl": false
																						},
																						"iconColor": "color1",
																						"urlImageOverlay": "",
																						"awesomeClasses": "esk-ifont-quit-circle fa-4x",
																						"action": "none",
																						"url": ""
																					},
																					"stamp": 29
																				},
																				"CreateAdvancedShippingNotice__": {
																					"type": "FormButton",
																					"data": false,
																					"options": {
																						"buttonLabel": "_CreateAdvancedShippingNotice",
																						"label": "_CreateAdvancedShippingNotice",
																						"textPosition": "text-below",
																						"textSize": "MS",
																						"textStyle": "default",
																						"textColor": "color1",
																						"iconColor": "color1",
																						"nextprocess": {
																							"processName": "Advanced Shipping Notice",
																							"attachmentsMode": "all",
																							"willBeChild": true,
																							"returnToOriginalUrl": false
																						},
																						"urlImageOverlay": "",
																						"width": "190",
																						"hidden": true,
																						"action": "openprocess",
																						"url": "",
																						"urlImage": "",
																						"style": 5,
																						"version": 0,
																						"awesomeClasses": "esk-ifont-adv-shipping-notice-circle fa-4x"
																					},
																					"stamp": 85
																				},
																				"InvoiceThisOrder__": {
																					"type": "FormButton",
																					"data": false,
																					"options": {
																						"buttonLabel": "_Invoice this order",
																						"label": "_Invoice this order",
																						"textPosition": "text-below",
																						"textSize": "MS",
																						"textStyle": "default",
																						"textColor": "color1",
																						"nextprocess": {
																							"processName": "AP - Application Settings Wizard",
																							"attachmentsMode": "all",
																							"willBeChild": true,
																							"returnToOriginalUrl": false
																						},
																						"urlImageOverlay": "",
																						"width": "190px",
																						"action": "none",
																						"url": "",
																						"urlImage": "",
																						"style": 5,
																						"version": 0,
																						"iconColor": "color1",
																						"awesomeClasses": "esk-ifont-convert-to-invoice-circle fa-4x"
																					},
																					"stamp": 31
																				}
																			}
																		}
																	},
																	"options": {
																		"version": 0,
																		"controlsAssociation": {}
																	}
																}
															}
														}
													}
												},
												"form-content-right-1": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 32,
													"*": {
														"Order_information": {
															"type": "PanelData",
															"options": {
																"border": {
																	"collapsed": false
																},
																"version": 0,
																"labelLength": "200",
																"iconURL": "",
																"TitleFontSize": "L",
																"hideActionButtons": true,
																"hideTitleBar": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "_Order information",
																"hidden": false,
																"leftImageURL": "",
																"hideTitle": false,
																"sameHeightAsSiblings": false
															},
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"options": {
																		"controlsAssociation": {
																			"Sales_Order_Number__": "LabelSales_Order_Number__",
																			"LabelSales_Order_Number__": "Sales_Order_Number__",
																			"Sales_Order_Date__": "LabelSales_Order_Date__",
																			"LabelSales_Order_Date__": "Sales_Order_Date__",
																			"Total__": "LabelTotal__",
																			"LabelTotal__": "Total__",
																			"ConfirmationDatetime__": "LabelConfirmation_datetime__",
																			"LabelConfirmation_datetime__": "ConfirmationDatetime__",
																			"CanceledDatetime__": "LabelCanceledDatetime__",
																			"LabelCanceledDatetime__": "CanceledDatetime__",
																			"RevisionDateTime__": "LabelRevisionDateTime__",
																			"LabelRevisionDateTime__": "RevisionDateTime__",
																			"CompanyCode__": "LabelCompanyCode__",
																			"LabelCompanyCode__": "CompanyCode__",
																			"VendorNumber__": "LabelVendorNumber__",
																			"LabelVendorNumber__": "VendorNumber__",
																			"Invoice_RuidEx__": "LabelInvoice_RuidEx__",
																			"LabelInvoice_RuidEx__": "Invoice_RuidEx__",
																			"RejectionDatetime__": "LabelRejectionDatetime__",
																			"LabelRejectionDatetime__": "RejectionDatetime__"
																		},
																		"version": 0
																	},
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"options": {
																				"lines": 11,
																				"columns": 2,
																				"version": 0,
																				"ctrlsPos": {
																					"Sales_Order_Number__": {
																						"line": 1,
																						"column": 2
																					},
																					"LabelSales_Order_Number__": {
																						"line": 1,
																						"column": 1
																					},
																					"Sales_Order_Date__": {
																						"line": 2,
																						"column": 2
																					},
																					"LabelSales_Order_Date__": {
																						"line": 2,
																						"column": 1
																					},
																					"Total__": {
																						"line": 3,
																						"column": 2
																					},
																					"LabelTotal__": {
																						"line": 3,
																						"column": 1
																					},
																					"Ligne_d_espacement__": {
																						"line": 8,
																						"column": 1
																					},
																					"ConfirmationDatetime__": {
																						"line": 4,
																						"column": 2
																					},
																					"LabelConfirmation_datetime__": {
																						"line": 4,
																						"column": 1
																					},
																					"CanceledDatetime__": {
																						"line": 7,
																						"column": 2
																					},
																					"LabelCanceledDatetime__": {
																						"line": 7,
																						"column": 1
																					},
																					"RevisionDateTime__": {
																						"line": 6,
																						"column": 2
																					},
																					"LabelRevisionDateTime__": {
																						"line": 6,
																						"column": 1
																					},
																					"CompanyCode__": {
																						"line": 9,
																						"column": 2
																					},
																					"LabelCompanyCode__": {
																						"line": 9,
																						"column": 1
																					},
																					"VendorNumber__": {
																						"line": 10,
																						"column": 2
																					},
																					"LabelVendorNumber__": {
																						"line": 10,
																						"column": 1
																					},
																					"Invoice_RuidEx__": {
																						"line": 11,
																						"column": 2
																					},
																					"LabelInvoice_RuidEx__": {
																						"line": 11,
																						"column": 1
																					},
																					"RejectionDatetime__": {
																						"line": 5,
																						"column": 2
																					},
																					"LabelRejectionDatetime__": {
																						"line": 5,
																						"column": 1
																					}
																				},
																				"colspans": [
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[],
																					[],
																					[]
																				]
																			},
																			"data": [
																				"fields"
																			],
																			"*": {
																				"LabelSales_Order_Number__": {
																					"type": "Label",
																					"data": [
																						"Sales_Order_Number__"
																					],
																					"options": {
																						"label": "_Sales Order Number",
																						"version": 0
																					},
																					"stamp": 33
																				},
																				"Sales_Order_Number__": {
																					"type": "ShortText",
																					"data": [
																						"Sales_Order_Number__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"label": "_Sales Order Number",
																						"activable": true,
																						"width": 230,
																						"version": 0,
																						"browsable": false,
																						"readonly": true
																					},
																					"stamp": 34
																				},
																				"LabelSales_Order_Date__": {
																					"type": "Label",
																					"data": [
																						"Sales_Order_Date__"
																					],
																					"options": {
																						"label": "_Sales Order Date",
																						"version": 0
																					},
																					"stamp": 35
																				},
																				"Sales_Order_Date__": {
																					"type": "DateTime",
																					"data": [
																						"Sales_Order_Date__"
																					],
																					"options": {
																						"displayLongFormat": false,
																						"label": "_Sales Order Date",
																						"activable": true,
																						"width": 230,
																						"version": 0,
																						"readonly": true
																					},
																					"stamp": 36
																				},
																				"LabelTotal__": {
																					"type": "Label",
																					"data": [
																						"Total__"
																					],
																					"options": {
																						"label": "_Total",
																						"version": 0
																					},
																					"stamp": 37
																				},
																				"Total__": {
																					"type": "Decimal",
																					"data": [
																						"Total__"
																					],
																					"options": {
																						"version": 1,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"integer": false,
																						"label": "_Total",
																						"precision_internal": 2,
																						"activable": true,
																						"width": 230,
																						"readonly": true,
																						"precision_current": 2
																					},
																					"stamp": 38
																				},
																				"LabelConfirmation_datetime__": {
																					"type": "Label",
																					"data": [
																						"ConfirmationDatetime__"
																					],
																					"options": {
																						"label": "_Confirmation datetime",
																						"version": 0
																					},
																					"stamp": 39
																				},
																				"ConfirmationDatetime__": {
																					"type": "RealDateTime",
																					"data": [
																						"ConfirmationDatetime__"
																					],
																					"options": {
																						"readonly": true,
																						"displayLongFormat": false,
																						"label": "_Confirmation datetime",
																						"activable": true,
																						"width": 230,
																						"version": 0
																					},
																					"stamp": 40
																				},
																				"LabelRejectionDatetime__": {
																					"type": "Label",
																					"data": [
																						"RejectionDatetime__"
																					],
																					"options": {
																						"label": "_RejectionDatetime",
																						"hidden": true,
																						"version": 0
																					},
																					"stamp": 41
																				},
																				"RejectionDatetime__": {
																					"type": "RealDateTime",
																					"data": [
																						"RejectionDatetime__"
																					],
																					"options": {
																						"readonly": true,
																						"displayLongFormat": false,
																						"label": "_RejectionDatetime",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"helpIconPosition": "Right",
																						"hidden": true,
																						"version": 0
																					},
																					"stamp": 42
																				},
																				"LabelRevisionDateTime__": {
																					"type": "Label",
																					"data": [
																						"RevisionDateTime__"
																					],
																					"options": {
																						"label": "_RevisionDateTime",
																						"version": 0
																					},
																					"stamp": 43
																				},
																				"RevisionDateTime__": {
																					"type": "RealDateTime",
																					"data": [
																						"RevisionDateTime__"
																					],
																					"options": {
																						"readonly": true,
																						"displayLongFormat": false,
																						"label": "_RevisionDateTime",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"version": 0
																					},
																					"stamp": 44
																				},
																				"LabelCanceledDatetime__": {
																					"type": "Label",
																					"data": [
																						"CanceledDatetime__"
																					],
																					"options": {
																						"label": "_CanceledDatetime",
																						"version": 0
																					},
																					"stamp": 45
																				},
																				"CanceledDatetime__": {
																					"type": "RealDateTime",
																					"data": [
																						"CanceledDatetime__"
																					],
																					"options": {
																						"displayLongFormat": false,
																						"label": "_CanceledDatetime",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"readonly": true,
																						"version": 0
																					},
																					"stamp": 46
																				},
																				"Ligne_d_espacement__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "",
																						"width": "",
																						"color": "default",
																						"label": "Ligne d'espacement",
																						"version": 0
																					},
																					"stamp": 47
																				},
																				"LabelCompanyCode__": {
																					"type": "Label",
																					"data": [
																						"CompanyCode__"
																					],
																					"options": {
																						"label": "_Company code",
																						"version": 0
																					},
																					"stamp": 48
																				},
																				"CompanyCode__": {
																					"type": "ShortText",
																					"data": [
																						"CompanyCode__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"label": "_Company code",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"readonly": true,
																						"browsable": false
																					},
																					"stamp": 49
																				},
																				"LabelVendorNumber__": {
																					"type": "Label",
																					"data": [
																						"VendorNumber__"
																					],
																					"options": {
																						"label": "_Vendor number",
																						"version": 0
																					},
																					"stamp": 50
																				},
																				"VendorNumber__": {
																					"type": "ShortText",
																					"data": [
																						"VendorNumber__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"label": "_Vendor number",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"readonly": true,
																						"browsable": false
																					},
																					"stamp": 51
																				},
																				"LabelInvoice_RuidEx__": {
																					"type": "Label",
																					"data": [
																						"Invoice_RuidEx__"
																					],
																					"options": {
																						"label": "_Invoice_RuidEx",
																						"version": 0
																					},
																					"stamp": 52
																				},
																				"Invoice_RuidEx__": {
																					"type": "ShortText",
																					"data": [
																						"Invoice_RuidEx__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"label": "_Invoice_RuidEx",
																						"activable": true,
																						"width": "",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"browsable": false,
																						"readonly": true,
																						"hidden": true
																					},
																					"stamp": 53
																				}
																			},
																			"stamp": 54
																		}
																	},
																	"stamp": 55,
																	"data": []
																}
															},
															"stamp": 56,
															"data": []
														}
													}
												},
												"form-content-right-3": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 57,
													"*": {
														"SystemData": {
															"type": "PanelSystemData",
															"options": {
																"multicolumn": false,
																"version": 0,
																"border": {},
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": false,
																"hideTitleBar": false,
																"label": "_System data",
																"hidden": true,
																"hideTitle": false,
																"sameHeightAsSiblings": false
															},
															"stamp": 58,
															"data": []
														}
													}
												},
												"form-content-right-10": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 141,
													"*": {
														"RelatedShippingNoticePane": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"iconURL": "",
																"TitleFontSize": "L",
																"hideActionButtons": true,
																"hideTitleBar": false,
																"hideTitle": false,
																"sameHeightAsSiblings": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "_RelatedShippingNoticePane",
																"hidden": true,
																"leftImageURL": "",
																"removeMargins": false,
																"elementsAlignment": "left",
																"version": 0,
																"labelLength": 0
															},
															"stamp": 87,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {},
																		"version": 0
																	},
																	"stamp": 88,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"RelatedShippingNoticeTable__": {
																						"line": 2,
																						"column": 1
																					},
																					"Spacer__": {
																						"line": 3,
																						"column": 1
																					},
																					"Spacer2__": {
																						"line": 1,
																						"column": 1
																					}
																				},
																				"lines": 3,
																				"columns": 2,
																				"colspans": [
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					]
																				],
																				"version": 0
																			},
																			"stamp": 89,
																			"*": {
																				"Spacer2__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "",
																						"width": "",
																						"color": "default",
																						"label": "_Spacer2",
																						"version": 0
																					},
																					"stamp": 126
																				},
																				"RelatedShippingNoticeTable__": {
																					"type": "Table",
																					"data": [
																						"RelatedShippingNoticeTable__"
																					],
																					"options": {
																						"version": 5,
																						"lines": 10,
																						"columns": 5,
																						"displayEmptyLines": false,
																						"alwaysOneLine": true,
																						"menuOptions": {
																							"disableInsertLine": true,
																							"disableAddLine": true,
																							"hideTopNavigation": true,
																							"hideBottomNavigation": true,
																							"hideTableRowMenu": true,
																							"hideTableRowDelete": true,
																							"hideTableRowAdd": true,
																							"hideTableRowDuplicate": true,
																							"hideTableRowButtonDuplicate": true,
																							"addButtonShouldInsert": false
																						},
																						"item": {
																							"display": null
																						},
																						"group": {
																							"name": "GDR\nPage"
																						},
																						"label": "_RelatedShippingNoticeTable",
																						"readonly": true,
																						"subsection": null
																					},
																					"stamp": 99,
																					"*": {
																						"Actions": {
																							"type": "$TableActions",
																							"subtype": "Actions",
																							"options": {
																								"version": 0
																							},
																							"stamp": 100,
																							"*": {
																								"Top": {
																									"type": "$TableActions",
																									"subtype": "Top",
																									"options": {
																										"version": 0
																									},
																									"stamp": 101
																								},
																								"Bottom": {
																									"type": "$TableActions",
																									"subtype": "Top",
																									"options": {
																										"version": 0
																									},
																									"stamp": 102
																								}
																							}
																						},
																						"Header": {
																							"type": "$TableRow",
																							"subtype": "Header",
																							"options": {
																								"version": 0
																							},
																							"stamp": 103,
																							"data": [
																								null
																							],
																							"*": {
																								"Cell1": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": 140,
																										"version": 0
																									},
																									"stamp": 144,
																									"data": [],
																									"*": {
																										"ASNNumber__": {
																											"type": "Label",
																											"data": [
																												"ASNNumber__"
																											],
																											"options": {
																												"label": "_ASNNumber",
																												"minwidth": 140,
																												"version": 0
																											},
																											"stamp": 145,
																											"position": [
																												"_LongText"
																											]
																										}
																									}
																								},
																								"Cell2": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": 140,
																										"version": 0
																									},
																									"stamp": 110,
																									"data": [],
																									"*": {
																										"ASNShippingDate__": {
																											"type": "Label",
																											"data": [
																												"ASNShippingDate__"
																											],
																											"options": {
																												"label": "_ASNShippingDate",
																												"minwidth": 140,
																												"version": 0
																											},
																											"stamp": 111,
																											"position": [
																												"_DateTime"
																											]
																										}
																									}
																								},
																								"Cell3": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": 140,
																										"version": 0
																									},
																									"stamp": 114,
																									"data": [],
																									"*": {
																										"ASNExpectedDeliveryDate__": {
																											"type": "Label",
																											"data": [
																												"ASNExpectedDeliveryDate__"
																											],
																											"options": {
																												"label": "_ASNExpectedDeliveryDate",
																												"minwidth": 140,
																												"version": 0
																											},
																											"stamp": 115,
																											"position": [
																												"_DateTime"
																											]
																										}
																									}
																								},
																								"Cell4": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": 140,
																										"version": 0
																									},
																									"stamp": 118,
																									"data": [],
																									"*": {
																										"ASNCarrier__": {
																											"type": "Label",
																											"data": [
																												"ASNCarrier__"
																											],
																											"options": {
																												"label": "_ASNCarrier",
																												"minwidth": 140,
																												"version": 0
																											},
																											"stamp": 119,
																											"position": [
																												"_ShortText"
																											]
																										}
																									}
																								},
																								"Cell5": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": 140
																									},
																									"stamp": 148,
																									"data": [],
																									"*": {
																										"ASNStatus__": {
																											"type": "Label",
																											"data": [
																												"ASNStatus__"
																											],
																											"options": {
																												"label": "_ASNStatus",
																												"type": "LongText",
																												"minwidth": 140
																											},
																											"stamp": 149,
																											"position": [
																												"_LongText",
																												false,
																												false,
																												"LongText"
																											]
																										}
																									}
																								}
																							}
																						},
																						"Clipboard": {
																							"type": "$TableRow",
																							"subtype": "Clipboard",
																							"options": {
																								"version": 0
																							},
																							"stamp": 104,
																							"data": [
																								null
																							],
																							"*": {
																								"Cell1": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 143,
																									"data": [],
																									"*": {
																										"ASNNumber__": {
																											"type": "LongText",
																											"data": [
																												"ASNNumber__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"minNbLines": 1,
																												"label": "_ASNNumber",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"helpIconPosition": "Right",
																												"browsable": false,
																												"numberOfLines": 1
																											},
																											"stamp": 146,
																											"position": [
																												"_LongText"
																											]
																										}
																									}
																								},
																								"Cell2": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 109,
																									"data": [],
																									"*": {
																										"ASNShippingDate__": {
																											"type": "DateTime",
																											"data": [
																												"ASNShippingDate__"
																											],
																											"options": {
																												"displayLongFormat": false,
																												"label": "_ASNShippingDate",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"helpIconPosition": "Right",
																												"version": 0
																											},
																											"stamp": 112,
																											"position": [
																												"_DateTime"
																											]
																										}
																									}
																								},
																								"Cell3": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 113,
																									"data": [],
																									"*": {
																										"ASNExpectedDeliveryDate__": {
																											"type": "DateTime",
																											"data": [
																												"ASNExpectedDeliveryDate__"
																											],
																											"options": {
																												"displayLongFormat": false,
																												"label": "_ASNExpectedDeliveryDate",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"helpIconPosition": "Right",
																												"version": 0
																											},
																											"stamp": 116,
																											"position": [
																												"_DateTime"
																											]
																										}
																									}
																								},
																								"Cell4": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 117,
																									"data": [],
																									"*": {
																										"ASNCarrier__": {
																											"type": "ShortText",
																											"data": [
																												"ASNCarrier__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"label": "_ASNCarrier",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"helpIconPosition": "Right",
																												"browsable": false
																											},
																											"stamp": 120,
																											"position": [
																												"_ShortText"
																											]
																										}
																									}
																								},
																								"Cell5": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {},
																									"stamp": 147,
																									"data": [],
																									"*": {
																										"ASNStatus__": {
																											"type": "LongText",
																											"data": [
																												"ASNStatus__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"minNbLines": 1,
																												"label": "_ASNStatus",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"helpIconPosition": "Right",
																												"numberOfLines": 1,
																												"browsable": false
																											},
																											"stamp": 150,
																											"position": [
																												"_LongText"
																											]
																										}
																									}
																								}
																							}
																						}
																					}
																				},
																				"Spacer__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "",
																						"width": "",
																						"color": "default",
																						"label": "_Spacer",
																						"version": 0
																					},
																					"stamp": 125
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-right-9": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 59,
													"*": {
														"Event_history": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": "200",
																"iconURL": "",
																"TitleFontSize": "L",
																"hideActionButtons": true,
																"hideTitleBar": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "_Events",
																"leftImageURL": "",
																"version": 0,
																"hideTitle": false,
																"sameHeightAsSiblings": false
															},
															"stamp": 60,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {},
																		"version": 0
																	},
																	"stamp": 61,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"ConversationUI__": {
																						"line": 1,
																						"column": 1
																					}
																				},
																				"lines": 1,
																				"columns": 2,
																				"colspans": [
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					]
																				],
																				"version": 0
																			},
																			"stamp": 62,
																			"*": {
																				"ConversationUI__": {
																					"type": "ConversationUI",
																					"data": false,
																					"options": {
																						"label": "_ConversationUI",
																						"activable": true,
																						"tableName": "Conversation__",
																						"volatile": true,
																						"width": "100%",
																						"version": 0,
																						"conversationOptions": {
																							"ignoreIfExists": false,
																							"notifyByEmail": true,
																							"emailTemplate": "Conversation_MissedPurchasingItem.htm",
																							"emailCustomTags": {
																								"OrderNumber__": null
																							}
																						}
																					},
																					"stamp": 63
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-right-11": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 64,
													"*": {
														"PO_Data": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {
																	"collapsed": false
																},
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": true,
																"hideTitleBar": true,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "_PO Data",
																"leftImageURL": "",
																"version": 0,
																"hidden": true,
																"hideTitle": false,
																"sameHeightAsSiblings": false
															},
															"stamp": 65,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"ShipToCompany__": "LabelShip_to_company__",
																			"LabelShip_to_company__": "ShipToCompany__",
																			"QuoteNumber__": "LabelQuote_number__",
																			"LabelQuote_number__": "QuoteNumber__",
																			"Currency__": "LabelCurrency__",
																			"LabelCurrency__": "Currency__",
																			"OrderNumber__": "LabelOrder_number__",
																			"LabelOrder_number__": "OrderNumber__",
																			"TotalNetAmount__": "LabelTotal_net_amount__",
																			"LabelTotal_net_amount__": "TotalNetAmount__"
																		},
																		"version": 0
																	},
																	"stamp": 66,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"ShipToCompany__": {
																						"line": 2,
																						"column": 2
																					},
																					"LabelShip_to_company__": {
																						"line": 2,
																						"column": 1
																					},
																					"QuoteNumber__": {
																						"line": 3,
																						"column": 2
																					},
																					"LabelQuote_number__": {
																						"line": 3,
																						"column": 1
																					},
																					"Currency__": {
																						"line": 4,
																						"column": 2
																					},
																					"LabelCurrency__": {
																						"line": 4,
																						"column": 1
																					},
																					"OrderNumber__": {
																						"line": 1,
																						"column": 2
																					},
																					"LabelOrder_number__": {
																						"line": 1,
																						"column": 1
																					},
																					"TotalNetAmount__": {
																						"line": 5,
																						"column": 2
																					},
																					"LabelTotal_net_amount__": {
																						"line": 5,
																						"column": 1
																					}
																				},
																				"lines": 5,
																				"columns": 2,
																				"colspans": [
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[]
																				],
																				"version": 0
																			},
																			"stamp": 67,
																			"*": {
																				"LabelOrder_number__": {
																					"type": "Label",
																					"data": [
																						"OrderNumber__"
																					],
																					"options": {
																						"label": "OrderNumber__",
																						"version": 0
																					},
																					"stamp": 68
																				},
																				"OrderNumber__": {
																					"type": "ShortText",
																					"data": [
																						"OrderNumber__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"label": "OrderNumber__",
																						"activable": true,
																						"width": 230,
																						"version": 0
																					},
																					"stamp": 69
																				},
																				"LabelShip_to_company__": {
																					"type": "Label",
																					"data": [
																						"ShipToCompany__"
																					],
																					"options": {
																						"label": "ShipToCompany__",
																						"version": 0
																					},
																					"stamp": 70
																				},
																				"ShipToCompany__": {
																					"type": "ShortText",
																					"data": [
																						"ShipToCompany__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"label": "ShipToCompany__",
																						"activable": true,
																						"width": 230,
																						"version": 0
																					},
																					"stamp": 71
																				},
																				"LabelQuote_number__": {
																					"type": "Label",
																					"data": [
																						"QuoteNumber__"
																					],
																					"options": {
																						"label": "QuoteNumber__",
																						"version": 0
																					},
																					"stamp": 72
																				},
																				"QuoteNumber__": {
																					"type": "ShortText",
																					"data": [
																						"QuoteNumber__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"label": "QuoteNumber__",
																						"activable": true,
																						"width": 230,
																						"version": 0
																					},
																					"stamp": 73
																				},
																				"LabelCurrency__": {
																					"type": "Label",
																					"data": [
																						"Currency__"
																					],
																					"options": {
																						"label": "Currency__",
																						"version": 0
																					},
																					"stamp": 74
																				},
																				"Currency__": {
																					"type": "ComboBox",
																					"data": [
																						"Currency__"
																					],
																					"options": {
																						"possibleValues": {
																							"0": "Euro",
																							"1": "USD"
																						},
																						"keyValueMode": true,
																						"possibleKeys": {
																							"0": "EUR",
																							"1": "USD"
																						},
																						"label": "Currency__",
																						"activable": true,
																						"width": 230,
																						"version": 1,
																						"maxNbLinesToDisplay": 10
																					},
																					"stamp": 75
																				},
																				"LabelTotal_net_amount__": {
																					"type": "Label",
																					"data": [
																						"TotalNetAmount__"
																					],
																					"options": {
																						"label": "TotalNetAmount__",
																						"version": 0
																					},
																					"stamp": 76
																				},
																				"TotalNetAmount__": {
																					"type": "Decimal",
																					"data": [
																						"TotalNetAmount__"
																					],
																					"options": {
																						"version": 1,
																						"textSize": "S",
																						"textAlignment": "right",
																						"textStyle": "default",
																						"textColor": "default",
																						"integer": false,
																						"label": "TotalNetAmount__",
																						"precision_internal": 2,
																						"activable": true,
																						"width": 230,
																						"precision_current": 2
																					},
																					"stamp": 77
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												}
											},
											"stamp": 78,
											"data": []
										}
									},
									"stamp": 79,
									"data": []
								}
							},
							"stamp": 80,
							"data": []
						}
					},
					"stamp": 81,
					"data": []
				},
				"form-footer": {
					"type": "FlexibleFormFooter",
					"options": {
						"splitterType": "simple-h",
						"version": 0,
						"maxwidth": ""
					},
					"*": {},
					"stamp": 82,
					"data": []
				}
			},
			"stamp": 83,
			"data": []
		}
	},
	"stamps": 150,
	"data": []
}