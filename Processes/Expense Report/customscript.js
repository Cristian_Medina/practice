var CustomScript;
(function (CustomScript) {
    ///#GLOBALS Lib Sys
    var topMessageWarning = Lib.P2P.TopMessageWarning(Controls.TopPaneWarning, Controls.TopMessageWarning__);
    var layout = {
        panes: ["Banner", "ExpenseReportInformation", "Expenses", "ApprovalWorkflow"].map(function (name) { return Controls[name]; }),
        actionButtons: ["Save", "SubmitExpenses", "BackToUser", "DeleteReport", "Retry", "Close", "DownloadCrystalReportsDataFile", "PreviewExpenseReport"].map(function (name) { return Controls[name]; }),
        HideWaitScreen: function (hide) {
            // async call just after boot
            setTimeout(function () {
                Controls.TotalAmount__.Wait(!hide);
            });
        }
    };
    var statusInitFunctions = {
        Draft: function (isReadOnly) {
            if (!isReadOnly) {
                // Buttons
                Controls.SubmitExpenses.Hide(false);
                Lib.Expense.Report.SubmitButtonDisabler.SetDisabled(true);
                Lib.Expense.Report.DeleteButtonDisabler.SetDisabled(Sys.Helpers.IsEmpty(Data.GetValue("RUIDEX")));
                Controls.DeleteReport.Hide(Sys.Helpers.IsEmpty(Data.GetValue("RUIDEX")));
                Controls.Save.Hide(false);
                // Hide Requester name
                Controls.UserName__.Hide(true);
            }
            else {
                Controls.AddExpense__.Hide(true);
            }
        },
        Toapprove: function (isReadOnly) {
            if (!isReadOnly) {
                // Buttons
                Controls.SubmitExpenses.Hide(false);
                Controls.SubmitExpenses.SetText(Language.Translate("_Approve"));
                Controls.BackToUser.Hide(false);
            }
            // Fields
            Controls.UserName__.Hide(false);
            // Prevent adding/removing lines
            Controls.ExpensesTable__.SetReadOnly(true);
            Controls.ExpensesTable__.SetRowToolsHidden(true);
            Controls.ExpenseReportInformation.SetReadOnly(true);
            Controls.ApprovalWorkflow.SetReadOnly(isReadOnly);
            Controls.AddExpense__.Hide(true);
        },
        Tocontrol: function (isReadOnly) {
            if (!isReadOnly) {
                // Buttons
                Controls.SubmitExpenses.Hide(false);
                Controls.SubmitExpenses.SetText(Language.Translate(Lib.Expense.Report.Workflow.IsLastController() && Lib.Expense.Report.IsRefundable() ? "_Approve and create an invoice" : "_Approve"));
                Controls.BackToUser.Hide(false);
            }
            // Fields
            Controls.UserName__.Hide(false);
            // Prevent adding/removing lines
            Controls.ExpensesTable__.SetReadOnly(true);
            Controls.ExpensesTable__.SetRowToolsHidden(true);
            Controls.ExpenseReportInformation.SetReadOnly(true);
            Controls.ApprovalWorkflow.SetReadOnly(isReadOnly);
            Controls.AddExpense__.Hide(true);
        },
        Validated: function (isReadOnly) {
            Controls.AddExpense__.Hide(true);
        },
        Deleted: function (isReadOnly) {
            // Prevent adding/removing lines
            Controls.ExpensesTable__.SetReadOnly(true);
            Controls.ExpensesTable__.SetRowToolsHidden(true);
            Controls.AddExpense__.Hide(true);
        }
    };
    Lib.Expense.ControlsWatcher.Register({
        name: "Label For Currency",
        controls: [{ name: "CC_Currency__" }],
        checkApplyingConditionFunc: function () {
            return true;
        },
        applyFunc: function () {
            var currency = Controls.CC_Currency__.GetValue();
            Controls.TotalAmount__.SetLabel(currency ? Language.Translate("_TotalAmount") + " (" + currency + ")" : Language.Translate("_TotalAmount"));
            Controls.RefundableAmount__.SetLabel(currency ? Language.Translate("_RefundableAmount") + " (" + currency + ")" : Language.Translate("_RefundableAmount"));
        }
    });
    function IsExpensesTableRefreshNeeded() {
        return (ProcessInstance.selectedRuidFromView || Controls.ExpensesTable__.GetItemCount() > 0) && Data.GetValue("ExpenseReportStatus__") === "Draft";
    }
    var gSaveOnQuit = false;
    function HighlightUpdatedLines(MsnEx) {
        Sys.Helpers.Controls.ForEachTableRow(Controls.ExpensesTable__, function (row) {
            if (row.ExpenseNumber__.GetError()) {
                row.AddRowStyle("grouped");
            }
            else if (row.ExpenseNumber__.GetWarning()) {
                if (row.ExpenseMSNEX__.GetValue() === MsnEx && row.ExpenseNumber__.GetWarning() != Language.Translate("_Warning tax amount expense report")) {
                    row.ExpenseNumber__.SetWarning(null);
                    // The line item has been updated, if user clicks on Quit, the updated line item will not be saved,
                    // as result, when reopen the expense report, the line item will have the warning "_Item has been updated".
                    // --> memorize this state and do Save on Quit
                    gSaveOnQuit = true;
                }
                else {
                    row.AddRowStyle("grouped");
                }
            }
        });
    }
    function SetExpensesFilter() {
        var filter = "(&(ExpenseStatus__=To submit)(|(OwnerID=%[User:dn])(CreatorOwnerId=%[User:dn]))";
        if (Controls.ExpensesTable__.GetItemCount() > 0) {
            // coherence with already selected items
            var msnexs_1 = [];
            Sys.Helpers.Data.ForEachTableItem("ExpensesTable__", function (item) {
                // do not select items already selected
                msnexs_1.push(item.GetValue("ExpenseMSNEX__"));
            });
            filter += "(MSNEX[!=](" + msnexs_1.join(",") + "))";
        }
        if (Data.GetValue("OriginalOwnerId") && Data.GetValue("User__")) {
            filter += "(|(OriginalOwnerId=cn=" + Data.GetValue("User__") + "*)(OriginalOwnerId=)(OriginalOwnerId!=*))";
        }
        else if (Data.GetValue("state")) {
            filter += "(|(OriginalOwnerId=cn=" + Data.GetValue("OriginalOwnerId") + "*)(OriginalOwnerId=)(OriginalOwnerId!=*))";
        }
        filter += ")";
        Controls.BrowseExpense__.SetFilter(filter);
    }
    function InitLayoutBeforeStarting() {
        Log.Info("InitLayoutBeforeStarting");
        //keepIt for backward compatibility
        if (Controls.Approve_Forward) {
            Controls.Approve_Forward.Hide(true);
        }
        Process.ShowFirstErrorAfterBoot(false);
        layout.panes.forEach(function (pane) {
            pane.Hide(true);
        });
        layout.actionButtons.forEach(function (button) {
            button.Hide(true);
        });
        layout.HideWaitScreen(false);
    }
    function InitPreviewPanel() {
        Controls.DocumentsPanel.AllowChangeOrUploadReferenceDocument(false);
        if (Attach.GetNbAttach() > 0 && Data.GetValue("ExpenseReportStatus__") != "Draft") {
            Log.Info("Form has a document");
            Log.Info("Document shown");
            // Workaround boot/customscript race condition to display and hide panels
            Controls.PreviewPanel.Hide(true);
            Controls.form_content_right.Hide(true);
            // End of workaround
            ProcessInstance.SetFormWidth();
            // Workaround boot/customscript race condition to display and hide panels
            setTimeout(function () {
                Controls.PreviewPanel.Hide(false);
                Controls.form_content_right.Hide(false);
                Controls.form_content_right.SetSize(45);
            }, 1000);
        }
        else {
            Log.Info("Form has NO document");
            Log.Info("Document hidden");
            // Workaround boot/customscript race condition to display and hide panels
            Controls.PreviewPanel.Hide(false);
            Controls.form_content_right.Hide(false);
            ProcessInstance.SetFormWidth("default");
            Controls.PreviewPanel.Hide(true);
            Controls.form_content_right.Hide(true);
        }
    }
    function InitInformationPanel() {
        Controls.UserName__.Hide(false);
        Controls.ExpenseReportNumber__.Hide(Sys.Helpers.IsEmpty(Controls.ExpenseReportNumber__.GetValue()));
        Controls.SubmissionDate__.Hide(Sys.Helpers.IsEmpty(Controls.SubmissionDate__.GetValue()));
        Controls.ExpenseReportTypeName__.SetAttributes("ID__");
        var timeoutID = null;
        Controls.ExpenseReportTypeName__.OnSelectItem = function (item) {
            timeoutID && clearTimeout(timeoutID);
            timeoutID = setTimeout(function () {
                Controls.ExpenseReportTypeID__.SetValue(item.GetValue("ID__"));
                Lib.Expense.Report.Workflow.DelayRebuildWorkflow();
                timeoutID = null;
            });
        };
        Controls.ExpenseReportTypeName__.OnChange = function () {
            if (!timeoutID) {
                timeoutID = setTimeout(function () {
                    if (Sys.Helpers.IsEmpty(Controls.ExpenseReportTypeName__.GetValue())) {
                        Controls.ExpenseReportTypeID__.SetValue("");
                    }
                    Lib.Expense.Report.Workflow.DelayRebuildWorkflow();
                    timeoutID = null;
                });
            }
        };
    }
    // Override Common function in order to update specific client component
    Lib.Expense.Report.OnDeleteItem = Sys.Helpers.Wrap(Lib.Expense.Report.OnDeleteItem, function (originalFn) {
        return originalFn
            .apply(this, Array.prototype.slice.call(arguments, 1))
            .Then(function () {
            RefreshWorkflow();
            Lib.Expense.Report.DuplicateCheck.UpdateDuplicateWarning(topMessageWarning);
        });
    });
    function InitExpensesTable() {
        Controls.ExpensesTable__.SetAtLeastOneLine(false);
        Controls.ExpensesTable__.SetWidth("100%");
        Controls.ExpensesTable__.SetExtendableColumn("ExpenseDescription__");
        Controls.BrowseExpense__.Hide(true);
        Controls.ExpensesTable__.OnDeleteItem = Lib.Expense.Report.OnDeleteItem;
        Controls.ExpensesTable__.OnRefreshRow = function (index) {
            var row = Controls.ExpensesTable__.GetRow(index);
            row.ExpenseNumber__.DisplayAs({ type: "Link" });
            if (row.ExpenseNumber__.GetError() || row.ExpenseNumber__.GetWarning()) {
                row.AddRowStyle("grouped");
            }
            else {
                row.RemoveRowStyle("grouped");
            }
            var status = Data.GetValue("ExpenseReportStatus__");
            if (status === "Draft" || status === "To approve" || status === "To control") {
                Lib.Expense.Report.DuplicateCheck.QueryDuplicate(row).Then(function (results) {
                    Lib.Expense.Report.DuplicateCheck.DisplayDuplicateIndicator(row, results.length > 0);
                });
            }
        };
        Controls.ExpensesTable__.Duplicate__.OnClick = function () {
            var row = this.GetRow();
            Lib.Expense.Report.DuplicateCheck.QueryDuplicate(row).Then(function (results) {
                if (results.length > 0) {
                    Lib.Expense.Report.DuplicateCheck.PopupDuplicate(row.ExpenseNumber__.GetValue(), results, null, { requesterDisplayed: Data.GetValue("ExpenseReportStatus__") === "To control"
                            || Data.GetValue("ExpenseReportStatus__") === "To approve" });
                }
                else {
                    Lib.Expense.Report.DuplicateCheck.DisplayDuplicateIndicator(row, false);
                    Lib.Expense.Report.DuplicateCheck.UpdateDuplicateWarning(topMessageWarning);
                    Popup.Alert(["_Expense {0} is no longer duplicated", row.ExpenseNumber__.GetValue()], false, null, "_Duplicate check");
                }
            });
        };
        Controls.ExpensesTable__.ExpenseNumber__.OnClick = function () {
            Controls.ExpensesTable__.ExpenseNumber__.Wait(true);
            var attributes = ["MsnEx", "ValidationURL", "ExpenseStatus__"];
            var filter = "ExpenseNumber__=" + this.GetValue();
            Sys.GenericAPI.Query("CDNAME#Expense", filter, attributes, function (results, error) {
                Controls.ExpensesTable__.ExpenseNumber__.Wait(false);
                if (!error && results && results.length > 0) {
                    var result_1 = results[0];
                    if (!Lib.Expense.IsExpenseSubmittable(result_1.ExpenseStatus__) && !Lib.Expense.IsDraftExpense(result_1.ExpenseStatus__)) {
                        Process.OpenLink({ url: result_1.ValidationURL + "&OnQuit=Close" });
                    }
                    else if (!ProcessInstance.IsModified()) {
                        Process.OpenLink({ url: result_1.ValidationURL + "&OnQuit=Back", inCurrentTab: true, returnUrlParams: "backmsnex=" + result_1.MsnEx });
                    }
                    else {
                        Lib.CommonDialog.PopupYesCancel(function (action) {
                            if (action === "Yes") {
                                Process.OpenLink({
                                    url: result_1.ValidationURL + "&OnQuit=Back",
                                    inCurrentTab: true,
                                    returnUrlParams: "backmsnex=" + result_1.MsnEx,
                                    ignoreModif: true
                                });
                            }
                        }, "_Edit expense", "_All modifications in the current form will be lost. Are you sure that you want to continue?");
                    }
                }
                else {
                    Popup.Alert("_Expense not found or access denied", false, null, "_Expense not found title");
                }
            });
        };
        Controls.PreviewExpenseReport.OnClick = function () {
            var expenseReportTemplateInfos = Lib.Expense.Report.Export.GetExpenseReportTemplateInfos();
            Lib.P2P.Export.IsAvailableTemplate(Sys.Helpers.Globals.User, expenseReportTemplateInfos.template, expenseReportTemplateInfos.escapedCompanyCode)
                .Then(function (result) {
                if (result.exist) {
                    var context = { expenseNumbers: Lib.Expense.Report.GetExpenseNumbersInForm() };
                    Lib.Expense.Report.QueryExpenses(context).Then(function (contextEx) {
                        if (expenseReportTemplateInfos.fileFormat === "RPT") {
                            Process.OpenPreview({
                                "conversionType": "crystal",
                                "templateName": expenseReportTemplateInfos.template,
                                "language": expenseReportTemplateInfos.escapedCompanyCode,
                                "data": function (fnDataBuildDone) {
                                    Lib.Expense.Report.Export.CreateExpenseReportJsonString(contextEx, expenseReportTemplateInfos, function (jsonString) {
                                        fnDataBuildDone(jsonString);
                                    });
                                }
                            });
                        }
                        else {
                            throw "Unknown file extension: " + expenseReportTemplateInfos.fileFormat;
                        }
                    });
                }
                else {
                    throw "Template not found: " + expenseReportTemplateInfos.template;
                }
            })
                .Catch(function (e) {
                Log.Error("Failed in IsAvailableTemplate: " + e);
                Popup.Alert("_An error occured when trying to open the preview", true, null, "_PreviewError");
            });
            return false;
        };
        Controls.DownloadCrystalReportsDataFile.OnClick = function () {
            var expenseReportTemplateInfos = Lib.Expense.Report.Export.GetExpenseReportTemplateInfos();
            if (expenseReportTemplateInfos.fileFormat === "RPT") {
                var context = { expenseNumbers: Lib.Expense.Report.GetExpenseNumbersInForm() };
                Lib.Expense.Report.QueryExpenses(context).Then(function (contextEx) {
                    Process.OpenPreview({
                        "conversionType": "crystal",
                        "templateName": expenseReportTemplateInfos.template,
                        "language": expenseReportTemplateInfos.escapedCompanyCode,
                        "outputFormat": "mdb",
                        "data": function (fnDataBuildDone) {
                            Lib.Expense.Report.Export.CreateExpenseReportJsonString(contextEx, expenseReportTemplateInfos, function (jsonString) {
                                fnDataBuildDone(jsonString);
                            });
                        }
                    });
                });
            }
            return false;
        };
        Sys.Helpers.Controls.ForEachTableRow(Controls.ExpensesTable__, function (row) { row.ExpenseNumber__.DisplayAs({ type: "Link" }); });
    }
    function InitBanner() {
        Log.Info("InitBanner");
        Sys.Helpers.Banner.SetStatusCombo(Controls.ExpenseReportStatus__);
        Sys.Helpers.Banner.SetHTMLBanner(Controls.HTMLBanner__);
        Sys.Helpers.Banner.SetMainTitle("Expense report");
        var status = Data.GetValue("ExpenseReportStatus__");
        Sys.Helpers.Banner.SetSubTitle("_Banner expense report " + status.toLowerCase());
    }
    function InitAddExpenseButton() {
        var attributesExpense = "OwnerId|ExpenseStatus__|InputTaxRate__|TaxAmount1__|TaxRate1__|TaxCode1__|TaxAmount2__|TaxRate2__|TaxCode2__|TaxAmount3__|TaxRate3__|TaxCode3__|TaxAmount4__"
            + "|TaxRate4__|TaxCode4__|TaxAmount5__|TaxRate5__|TaxCode5__|ExpenseNumber__";
        var additionalAttributesExpense = Lib.Expense.Report.GetExpenseFieldsName().join("|");
        attributesExpense = additionalAttributesExpense.concat("|" + attributesExpense);
        Controls.BrowseExpense__.SetAttributes(attributesExpense);
        Controls.BrowseExpense__.OnSelectItem = function (item) {
            if (Controls.ExpensesTable__.GetItemCount() === 0 && Sys.Helpers.IsEmpty(Data.GetValue("OriginalOwnerId"))) {
                //when creating an empty Expense report, the first line added should determine both OriginalOwnerId and User__
                Data.SetValue("User__", "");
                Data.SetValue("UserName__", "");
            }
            Lib.Expense.Report.DuplicateCheck.CheckDuplicateBeforeAdd(item, function (duplicate) {
                var rowItem = Controls.ExpensesTable__.AddItem(false);
                Lib.Expense.Report.SetExpenseTableLine(rowItem, item)
                    .Then(Lib.Expense.Report.SetHeaderFields)
                    .Then(Lib.Expense.Report.SetHeaderDependentTableLineFields)
                    .Then(function () {
                    Lib.Expense.Report.UpdateAndCheckLines();
                    InitWarningBanner();
                    Lib.Expense.Report.SubmitButtonDisabler.SetDisabled(false);
                    RefreshWorkflow();
                    SetExpensesFilter();
                    if (Sys.Helpers.IsString(duplicate)) {
                        Log.Error("Error during loading duplicate");
                    }
                    else if (duplicate) {
                        Lib.Expense.Report.DuplicateCheck.UpdateDuplicateWarning(topMessageWarning, true);
                    }
                });
            });
        };
        Controls.BrowseExpense__.OnColumnFormating = function (attributeName, value) {
            if (attributeName === "Date__") {
                value = Language.FormatDate(Sys.Helpers.Date.ISOSTringToDate(value));
            }
            return value;
        };
        Controls.AddExpense__.OnClick = function () {
            SetExpensesFilter();
            Controls.BrowseExpense__.DoBrowse();
        };
    }
    function InitWorkflow() {
        var wkf = Lib.Expense.Report.Workflow;
        wkf.controller.Define(wkf.parameters);
        RefreshWorkflow();
    }
    function RefreshWorkflow() {
        var wkf = Lib.Expense.Report.Workflow;
        if (!wkf.IsReadOnly()) {
            // only once
            if (wkf.controller.GetTableIndex() === 0) {
                wkf.controller.SetRolesSequence([
                    wkf.enums.roles.user,
                    wkf.enums.roles.manager,
                    wkf.enums.roles.controller
                ]);
            }
            else if (wkf.controller.RebuildAllowed()) {
                // The workflow can be changed before BackToUser,
                wkf.controller.Rebuild();
            }
        }
    }
    function InitOnClickButton() {
        function OnConfirmDelete() {
            ProcessInstance.ApproveAsynchronous("DeleteReport");
        }
        function CheckExpenseReport() {
            // Check that the Expense Report Type is set
            var ok = !Sys.Helpers.IsEmpty(Controls.ExpenseReportTypeName__.GetValue());
            if (!ok) {
                Controls.ExpenseReportTypeName__.SetError("This field is required!");
                return false;
            }
            // Check that the currency is set
            if (Sys.Helpers.IsEmpty(Controls.CC_Currency__.GetValue())) {
                Controls.TotalAmount__.SetError("_No currency has been defined for this CompanyCode.");
                return false;
            }
            return true;
        }
        Controls.BackToUser.OnClick = function () {
            DialogComment("BackToUser");
        };
        Controls.DeleteReport.OnClick = function () {
            Popup.Confirm("_Delete_ExpenseReport_explanation", false, OnConfirmDelete, null, "_Delete expense report confirmation");
            return false;
        };
        Controls.Retry.OnClick = function () {
            var lastActionName = Variable.GetValueAsString("LastActionName");
            if (lastActionName) {
                if (lastActionName === "SubmitExpenses") {
                    ProcessInstance.SetUrlParametersForPreventApproval("backmsnex=");
                }
                ProcessInstance.Approve(lastActionName);
            }
            else {
                Popup.Alert("_No action to retry");
            }
        };
        Controls.SubmitExpenses.OnClick = function () {
            var _a, _b;
            if (Process.ShowFirstError() === null && ((_b = (_a = Controls.ExpenseReportTypeName__).IsRequired) === null || _b === void 0 ? void 0 : _b.call(_a)) && CheckExpenseReport()) {
                if (Object.keys(Lib.Expense.Report.Workflow.additionalContributorsCache).length === 0 && !Variable.GetValueAsString("AdditionalContributors")) {
                    ProcessInstance.SetUrlParametersForPreventApproval("backmsnex=");
                    ProcessInstance.Approve("SubmitExpenses");
                }
                else {
                    Variable.SetValueAsString("AdditionalContributors", JSON.stringify(Lib.Expense.Report.Workflow.additionalContributorsCache));
                    if (Data.GetValue("ExpenseReportStatus__") == "To control") {
                        ProcessInstance.Approve("Approve_RequestFurtherApproval");
                    }
                    else {
                        ProcessInstance.Approve("Approve_Forward");
                    }
                }
            }
        };
        Controls.Close.OnClick = function () {
            if (gSaveOnQuit) {
                ProcessInstance.SaveAndQuit("Save");
                return false;
            }
            return null;
        };
    }
    function InitButtonBar() {
        InitOnClickButton();
        Controls.Close.Hide(false);
        Controls.Retry.Hide(true);
    }
    function InitConditionalLayout() {
        if (Process.isDesignActive) {
            var expenseReportTemplateInfos = Lib.Expense.Report.Export.GetExpenseReportTemplateInfos();
            if (expenseReportTemplateInfos.fileFormat === "RPT") {
                Controls.DownloadCrystalReportsDataFile.Hide(false);
            }
        }
    }
    function InitLayout() {
        Log.Info("InitLayout");
        layout.panes.forEach(function (pane) {
            pane.Hide(false);
        });
        layout.HideWaitScreen(true);
        InitPreviewPanel();
        InitInformationPanel();
        InitExpensesTable();
        InitBanner();
        InitWarningBanner();
        InitAddExpenseButton();
        InitButtonBar();
        InitConditionalLayout();
        Lib.Expense.Report.Workflow.InitPanel();
        var func = Sys.Helpers.TryGetFunction("Lib.Expense.Report.Customization.Client.CustomizeLayout");
        if (func) {
            func();
        }
        else {
            Sys.Helpers.TryCallFunction("Lib.Expense.Report.Customization.Client.CustomiseLayout");
        }
        Lib.P2P.DisplayBackupUserWarning("Expense", function (displayName) {
            topMessageWarning.Add(Language.Translate("_View on bealf of {0}", false, displayName), 0);
        }, function () {
            if ((Data.GetValue("State") == 70 || Data.GetValue("State") == 90) && User.IsBackupUserOf(Data.GetValue("OwnerId"))) {
                return Data.GetValue("OwnerID");
            }
        });
        var nextAlert = Lib.CommonDialog.NextAlert.GetNextAlert();
        if (!nextAlert || nextAlert.behaviorName !== "onUnexpectedError") {
            /* Init depending expense report status
             * /!\ BECAREFUL IF THE STATUS NAME CHANGES THE FUNCTION POINTER WILL NOT WORK ANYMORE /!\
             * Because the name of the property (statusKey) in statusInitFunctions correspond to the status name without the spaces.
             */
            var statusKey = Controls.ExpenseReportStatus__.GetValue().replace(/\s/g, "");
            if (statusInitFunctions.hasOwnProperty(statusKey)) {
                statusInitFunctions[statusKey](Lib.Expense.Report.Workflow.IsReadOnly());
            }
            Lib.Expense.ControlsWatcher.CheckAll();
        }
    }
    function InitWarningBanner() {
        Lib.P2P.DisplayOnBehalfWarning(User.loginId, function (nameToDisplay, isCreatedBy) {
            if (isCreatedBy) {
                topMessageWarning.Add(Language.Translate("_ExpenseReportCreatedBy", true, nameToDisplay), 0);
            }
            else {
                topMessageWarning.Add(Language.Translate("_ExpenseReportCreatedFor", true, nameToDisplay), 0);
            }
        });
    }
    function FillDialogCallback(dialogTexts) {
        return function (dialog /*, tabId, event, control*/) {
            var ctrl = dialog.AddDescription("ctrlDesc", null, 466);
            var commentValue = Controls.Comments__.GetValue();
            if (Sys.Helpers.IsEmpty(commentValue)) {
                ctrl.SetText(Language.Translate(dialogTexts.descriptionRequired));
            }
            else {
                ctrl.SetText(Language.Translate(dialogTexts.descriptionConfirmation));
            }
            var commentCtrl = dialog.AddMultilineText("ctrlComments", "_Comment - Comment Dialog", 400);
            dialog.RequireControl(commentCtrl);
            commentCtrl.SetValue(commentValue);
        };
    }
    function CommitDialogCallback(action) {
        return function (dialog) {
            Controls.Comments__.SetValue(dialog.GetControl("ctrlComments").GetValue());
            ProcessInstance.ApproveAsynchronous(action);
        };
    }
    function DialogComment(action, dialogTexts, onCommitted) {
        var defaultDialogTexts = {
            titleConfirmation: "_Approval comments confirmation",
            titleRequired: "_Approval comments required",
            descriptionConfirmation: "_Please confirm your comment",
            descriptionRequired: "_Please write your comment"
        }, att;
        if (!dialogTexts) {
            dialogTexts = defaultDialogTexts;
        }
        else {
            for (att in defaultDialogTexts) {
                if (!(att in dialogTexts)) {
                    dialogTexts[att] = defaultDialogTexts[att];
                }
            }
        }
        var title = Controls.Comments__.GetValue() ? dialogTexts.titleConfirmation : dialogTexts.titleRequired;
        Popup.Dialog(title, null, FillDialogCallback(dialogTexts), CommitDialogCallback(action), null);
        return false;
    }
    function FilledTechnicalDataWithExpenseDuplicate(result) {
        var technicalData = JSON.parse(Controls.TechnicalData__.GetText());
        var allExpenseMSNex = [];
        result.forEach(function (expense) {
            allExpenseMSNex.push(expense.GetValue("ExpenseMSNEX__"));
        });
        technicalData.expenseAlreadyViewAsDuplicate = allExpenseMSNex;
        Sys.Helpers.SilentChange(function () { return Controls.TechnicalData__.SetText(JSON.stringify(technicalData)); });
    }
    function Start() {
        InitWorkflow();
        InitLayout();
        if (!Process.GetURLParameter("backmsnex")) {
            Lib.CommonDialog.NextAlert.Show({
                // special behavior for Unexpected error
                "onUnexpectedError": {
                    IsShowable: function () {
                        Controls.Retry.Hide(false);
                        return true;
                    }
                },
                "MissingExpensesError": {
                    Popup: function (nextAlert) {
                        Lib.CommonDialog.NextAlert.PopupYesCancel(function (action) {
                            switch (action) {
                                case "Yes":
                                    ProcessInstance.Approve("PostValidation_Post");
                                    break;
                                case "Cancel":
                                    Lib.Purchasing.SetDocumentReadonlyAndHideAllButtonsExceptQuit();
                                    break;
                                default:
                                    break;
                            }
                        }, nextAlert, "_Retry now", "_Retry later");
                    }
                }
            });
        }
        if (!ProcessInstance.state) {
            ProcessInstance.DisableExtractionScript();
        }
    }
    /** *************** **/
    /** BEFORE STARTING **/
    /** *************** **/
    Sys.Helpers.EnableSmartSilentChange();
    // START - ignore all changes on form during the initialization processing
    ProcessInstance.SetSilentChange(true);
    function LoadUserProperties() {
        return Sys.Helpers.Promise.Create(function (resolve) {
            if (Sys.Helpers.IsEmpty(Data.GetValue("User__"))) {
                //FOR UPGDRADE : OwnerName__ can be null on Expenses, and therefor be empty here
                Data.SetValue("User__", User.loginId);
                Data.SetValue("UserName__", User.fullName);
            }
            Lib.P2P.UserProperties.QueryValues(Data.GetValue("User__"))
                .Then(function (UserPropertiesValues) {
                var allowedCompanyCodes = UserPropertiesValues.GetAllowedCompanyCodes().split("\n");
                var companyCode = Data.GetValue("CompanyCode__");
                var ccCurrency = Data.GetValue("CC_Currency__");
                if (!ProcessInstance.state || ((Sys.Helpers.IsEmpty(companyCode) || Sys.Helpers.IsEmpty(ccCurrency)) && Data.GetValue("ExpenseReportStatus__") === "Draft") && !Lib.CommonDialog.NextAlert.GetNextAlert()) {
                    Data.SetValue("UserNumber__", UserPropertiesValues.UserNumber__);
                    if (!companyCode) {
                        companyCode = UserPropertiesValues.CompanyCode__;
                        if (!companyCode && allowedCompanyCodes.length) {
                            companyCode = allowedCompanyCodes[0];
                        }
                    }
                    else {
                        // Reset CompanyCode__ in order to let the framework to check allowed table value in next CompanyCode__.SetValue(companyCode)
                        Data.SetValue("CompanyCode__", "");
                    }
                    Data.SetValue("CompanyCode__", companyCode);
                }
                resolve();
            });
        });
    }
    function RefreshExpensesTable() {
        return Sys.Helpers.Promise.Create(function (resolve) {
            Lib.Expense.Report.DuplicateCheck.ResetDuplicateIndicators();
            if (IsExpensesTableRefreshNeeded()) {
                var MsnEx_1 = Process.GetURLParameter("backmsnex");
                Lib.Expense.Report.FillExpensesTable()
                    .Then(Lib.Expense.Report.DuplicateCheck.CheckDuplicate)
                    .Then(function (duplicate) {
                    Lib.Expense.Report.DuplicateCheck.DisplayDuplicateIndicators(duplicate);
                    Lib.Expense.Report.DuplicateCheck.UpdateDuplicateWarning(topMessageWarning, duplicate.length > 0 || undefined);
                    HighlightUpdatedLines(MsnEx_1);
                    resolve();
                })
                    .Catch(function () {
                    Log.Error("Error during loading duplicate");
                });
            }
            else {
                if (Data.GetValue("ExpenseReportStatus__") === "To approve" || Data.GetValue("ExpenseReportStatus__") === "To control") {
                    Lib.Expense.Report.DuplicateCheck.CheckAllDuplicate()
                        .Then(function (duplicate) {
                        Lib.Expense.Report.DuplicateCheck.DisplayDuplicateIndicators(duplicate);
                        Lib.Expense.Report.DuplicateCheck.UpdateDuplicateWarning(topMessageWarning, duplicate.length > 0);
                        FilledTechnicalDataWithExpenseDuplicate(duplicate);
                    })
                        .Catch(function () {
                        Log.Error("Error during loading duplicate");
                    });
                }
                resolve();
            }
        });
    }
    function LoadExpenseReportTypeTable() {
        return Sys.Helpers.Promise.Create(function (resolve) {
            if (Controls.ExpenseReportStatus__.GetValue() === "Draft") {
                Lib.Expense.Report.QueryExpenseReportType(Data.GetValue("CompanyCode__"))
                    .Then(function (records) {
                    // When the expense report type is not found and submitted from mobile fill expenses table
                    var expenseReportTypeExist = Controls.ExpenseReportTypeName__.GetValue() === (records[0] && records[0].ExpenseReportTypeName__);
                    if (!expenseReportTypeExist && Variable.GetValueAsString("SubmittedFromMobileApp")) {
                        var MsnEx_2 = Process.GetURLParameter("backmsnex");
                        Lib.Expense.Report.FillExpensesTable(JSON.parse(Variable.GetValueAsString("SelectedExpenseIDs"))).Then(function () {
                            HighlightUpdatedLines(MsnEx_2);
                            Lib.Expense.Report.SubmitButtonDisabler.SetDisabled(false);
                            resolve();
                        });
                    }
                    else if (records.length === 1) {
                        Controls.ExpenseReportTypeName__.SetValue(records[0].ExpenseReportTypeName__);
                        Controls.ExpenseReportTypeID__.SetValue(records[0].ID__);
                    }
                    else if (records.length > 1) {
                        // > 1 in case no ERT is defined
                        var defaultID_1 = Controls.ExpenseReportTypeID__.GetValue();
                        var defaultType = Sys.Helpers.Array.Find(records, function (type) {
                            return type.ID__ == defaultID_1;
                        });
                        if (defaultType !== undefined) {
                            Controls.ExpenseReportTypeName__.SetValue(defaultType.ExpenseReportTypeName__);
                        }
                        else {
                            // No need to clear the ExpenseReportTypeID__, since ExpenseReportTypeName__ required
                            Log.Warn("The default Expense Report type specified by the admin doesn't exist OR none is specified.");
                        }
                    }
                    resolve();
                });
            }
            else {
                resolve();
            }
        });
    }
    var syncBeforeStarting = Sys.Helpers.Synchronizer.Create(function () {
        Start();
        // END - ignore all changes on form during the initialization processing
        ProcessInstance.SetSilentChange(false);
    }, null, { progressDelay: 15000, OnProgress: Sys.Helpers.Synchronizer.OnProgress });
    var onLoadPromise = Sys.Helpers.TryCallFunction("Lib.Expense.Report.Customization.Client.OnLoad");
    if (onLoadPromise) {
        syncBeforeStarting.Register(onLoadPromise);
    }
    if (IsExpensesTableRefreshNeeded()) {
        Lib.Expense.Report.SubmitButtonDisabler.SetDisabled(false);
    }
    function assynchStartFunction() {
        InitLayoutBeforeStarting();
        return RefreshExpensesTable()
            .Then(LoadUserProperties)
            .Then(Lib.Expense.Report.LoadCompanyCodesValue)
            .Then(function () {
            if (Sys.Helpers.IsEmpty(Data.GetValue("CC_Currency__"))) {
                Lib.Expense.Report.SubmitButtonDisabler.SetDisabled(true);
            }
        })
            .Then(LoadExpenseReportTypeTable);
    }
    syncBeforeStarting.Register(assynchStartFunction, null, { returnsPromise: true });
    syncBeforeStarting.Start();
})(CustomScript || (CustomScript = {}));
