///#GLOBALS Lib
/* globals ChildProcessType */
var requiredFields = new Lib.Purchasing.CheckPO.RequiredFields(Data);
var workflow = Sys.WorkflowController.Create(Data, Variable, Language);
var g_newComment = Data.GetValue("Comments__");
//
// Some variables left to detect errors in notifications
//
function SetVariables() {
    Variable.SetValueAsString("RequesterFullName__", "/!\\ MULTIPLE PURCHASE ORDERS");
    Variable.SetValueAsString("PurchaseRequestViewUrl__", "/!\\ MULTIPLE PURCHASE ORDERS");
}
var Treasurer = (function () {
    var exists = null;
    var instance = {
        login: Lib.P2P.ResolveDemoLogin(Sys.Parameters.GetInstance("PAC").GetParameter("TreasurerLogin")),
        user: null,
        Exists: function (needUser) {
            if (exists === null || (needUser && this.user === null)) {
                if (!Variable.GetValueAsString("TreasurerExists__") || needUser) {
                    this.user = Users.GetUser(this.login);
                    exists = this.user !== null;
                    Variable.SetValueAsString("TreasurerExists__", exists ? "true" : "false");
                }
                else {
                    exists = Variable.GetValueAsString("TreasurerExists__") === "true";
                }
                if (!exists) {
                    Log.Info("No AP Treasurer found.");
                }
            }
            return exists;
        },
        GiveReadRight: function () {
            if (this.Exists()) {
                Log.Info("Grant read right to treasurer: " + this.login);
                Process.SetRight(this.login, "read");
            }
        },
        StoreSomeInfo: function () {
            if (this.Exists(true)) {
                Log.Info("Store some information about Treasurer");
                var userVars = this.user.GetVars();
                Variable.SetValueAsString("TreasurerName__", userVars.GetValue_String("DisplayName", 0));
            }
        }
    };
    return instance;
})();
function CanCancel() {
    var grItemsFilter = "(&(!(Status__=Canceled))(OrderNumber__=" + Data.GetValue("OrderNumber__") + "))";
    var query = Process.CreateQuery();
    query.SetSpecificTable(Lib.Purchasing.Items.GRItemsDBInfo.table);
    query.SetFilter(grItemsFilter);
    return query.GetRecordCount() == 0;
}
function CreatePOInERP(callback) {
    Log.Time("CreatePOInERP");
    var ERPName = Lib.ERP.GetERPName();
    var createDocInERP = Sys.Parameters.GetInstance("P2P_" + ERPName).GetParameter("CreateDocInERP");
    Log.Info("Create purchase order in ERP " + ERPName);
    var poData = { number: Data.GetValue("OrderNumber__") };
    if (!poData.number) {
        var ret_1 = Lib.ERP.ExecuteERPFunc("Create", "PO");
        if (!ret_1 || ret_1.error) {
            // Unit of measure not available in SAP
            if (ERPName === "SAP" && ret_1.error.indexOf("E/ME/057") !== -1) {
                Log.Error(ret_1.error);
                ret_1.error = "_Error unit of measure unavailable";
            }
            Lib.CommonDialog.NextAlert.Define("_Purchase order creation error", ret_1.error);
            Log.TimeEnd("CreatePOInERP");
            return false;
        }
        poData = ret_1.ret;
        // allocate a number if no document created in ERP
        if (!createDocInERP) {
            // Fetch previous PO number if it already exists (crash protection)
            poData.number = Transaction.Read("SafePAC_PONUMBER");
            if (!poData.number) {
                poData.number = Lib.Purchasing.NextNumber("PO", "OrderNumber__");
                Transaction.Write("SafePAC_PONUMBER", poData.number);
            }
        }
        Data.SetValue("OrderNumber__", poData.number);
        Log.Info("Created with number " + poData.number);
    }
    else {
        Log.Info("Already created with number " + poData.number);
    }
    Lib.ERP.ExecuteERPFunc("AttachURL", "PO");
    var ret = callback(createDocInERP, poData);
    Log.TimeEnd("CreatePOInERP");
    return ret;
}
function AttachPO() {
    Log.Time("AttachPO");
    if (!Lib.Purchasing.POValidation.AttachPO(Data.GetValue("OrderNumber__"))) {
        var popup = Lib.CommonDialog.NextAlert.GetNextAlert();
        if (!popup || !popup.isError) {
            Lib.CommonDialog.NextAlert.Define("_Purchase order attaching error", "_Error attaching purchase order", { isError: true });
        }
        Log.TimeEnd("AttachPO");
        return false;
    }
    Log.TimeEnd("AttachPO");
    return true;
}
function CheckErrorPO() {
    return Lib.ERP.ExecuteERPFunc("CheckError", "PO").ret;
}
function RetryPO(continueMode) {
    return Lib.ERP.ExecuteERPFunc("Retry", "PO", continueMode).ret;
}
function CreateNewVendor() {
    // Build variables for AP Clerk notif
    if (Variable.GetValueAsString("NewVendor__") == "true") {
        Log.Info("New vendor detected: " + Data.GetValue("VendorName__"));
        Variable.SetValueAsString("NewVendorName__", Data.GetValue("VendorName__"));
        Variable.SetValueAsString("NewVendorAddress__", [Data.GetValue("VendorAddress__")]);
        Variable.SetValueAsString("NewVendorEmail__", Data.GetValue("VendorEmail__"));
        var apClerk = Lib.Purchasing.GetAPClerk();
        apClerk.GiveReadRight();
        apClerk.StoreSomeInfo();
    }
}
function SendErrorNotifToMe(template) {
    var currentUserLogin = Lib.P2P.GetValidatorOrOwner().GetValue("login");
    var email = Sys.EmailNotification.CreateEmail({
        userId: currentUserLogin,
        customTags: {
            BuyerName__: Data.GetValue("BuyerName__"),
            OrderNumber__: Data.GetValue("OrderNumber__")
        },
        template: template || "Purchasing_Email_NotifPOError.htm"
    });
    Sys.EmailNotification.SendEmail(email);
}
function OrderOnPunchout() {
    var vendorID = Data.GetTable("LineItems__").GetItem(0).GetValue("RequestedVendor__");
    var ret = Lib.Purchasing.Punchout.Order(vendorID);
    // If an error occured
    if (!ret.ret) {
        var errorMessage = void 0;
        if (ret.status == "401") {
            errorMessage = "_OpenPunchout wrong credentials";
        }
        else if (typeof ret.status == "string" && ret.status.charAt(0) == "5") {
            errorMessage = "_OpenPunchout error 5xx message";
        }
        else if (typeof ret.status == "string" && ret.status.charAt(0) == "4") {
            errorMessage = "_OpenPunchout error 4xx message";
        }
        else {
            errorMessage = "_OpenPunchout wrong network configuration";
        }
        Lib.CommonDialog.NextAlert.Define("_Purchase order creation error", errorMessage);
    }
    return ret.ret;
}
function GetCustomerCompany() {
    var companyName = "";
    // If doesn't, query the company code info
    var companyCode = Data.GetValue("CompanyCode__");
    Log.Info("GetCustomerCompany: Retrieve the company code information of " + companyCode + ".");
    Lib.P2P.CompanyCodesValue.QueryValues(companyCode).Then(function (CCValues) {
        if (Object.keys(CCValues).length > 0) {
            companyName = CCValues.CompanyName__;
        }
    });
    // Last fallback, returns the ShipToCompany...
    if (!companyName) {
        Log.Info("GetCustomerCompany: no CompanyName__ in CompanyCode table. Returns ShipToCompany field.");
        companyName = Data.GetValue("ShipToCompany__");
    }
    Log.Info("GetCustomerCompany returns '" + companyName + "'.");
    return companyName;
}
//
// Sends a purchase order via e-mail to the vendor, using the English template taken from the current account
//
function PublishCustomerOrder(vendor) {
    if (vendor && Data.GetValue("EmailNotificationOptions__") != "PunchoutMode") {
        try {
            Log.Info("Generating Customer order");
            var childCD = Process.CreateProcessInstance("Customer Order", false, false, false);
            if (childCD) {
                var vars = childCD.GetUninheritedVars();
                vars.AddValue_String("Sales_Order_Number__", Data.GetValue("OrderNumber__"), true);
                vars.AddValue_Date("Sales_Order_Date__", new Date(), true);
                vars.AddValue_Double("Total__", Data.GetValue("TotalNetAmount__"), true);
                vars.AddValue_String("CompanyCode__", Data.GetValue("CompanyCode__"), true);
                vars.AddValue_String("VendorNumber__", Data.GetValue("VendorNumber__"), true);
                var externalVars = childCD.GetExternalVars();
                externalVars.AddValue_String("VendorLogin", vendor.GetVars().GetValue_String("login", 0), true);
                externalVars.AddValue_String("CustomerCompany", GetCustomerCompany(), true);
                //Add attachments for CO
                externalVars.AddValue_String("BuyerComment__", Data.GetValue("BuyerComment__"), true);
                var currentUser = Users.GetUser(Data.GetValue("OwnerId"));
                var CcEmailAddress = currentUser.GetValue("EmailAddress");
                CcEmailAddress += "\n";
                CcEmailAddress += Data.GetValue("EmailCarbonCopy__");
                externalVars.AddValue_String("EmailCarbonCopy__", CcEmailAddress, true);
                externalVars.AddValue_String("AlwaysAttachPurchaseOrder", Sys.Parameters.GetInstance("PAC").GetParameter("AlwaysAttachPurchaseOrder"), true);
                if (Data.GetValue("EmailNotificationOptions__") == "SendToVendor") {
                    externalVars.AddValue_String("SendNotification", "1", true);
                }
                vars.AddValue_String("OrderNumber__", Data.GetValue("OrderNumber__"), true);
                vars.AddValue_String("ShipToCompany__", Data.GetValue("ShipToCompany__"), true);
                vars.AddValue_String("Currency__", Data.GetValue("Currency__"), true);
                vars.AddValue_Double("TotalNetAmount__", Data.GetValue("TotalNetAmount__"), true);
                var nbAttach = Attach.GetNbAttach();
                // Loops on all attachments.
                Log.Info("Nb Attach: " + nbAttach);
                var sendAttachments = Sys.Parameters.GetInstance("PAC").GetParameter("SendPOAttachments") === true;
                for (var idx = 0; idx < nbAttach; idx++) {
                    var type = Attach.GetValue(idx, "Purchasing_DocumentType");
                    Log.Info("Purchasing document type: " + type);
                    if (type === "PO" || type === "QUOTE" || (type !== "CSV" && sendAttachments)) {
                        var attachFile = Attach.GetConvertedFile(idx) || Attach.GetInputFile(idx);
                        if (attachFile != null) {
                            var name = Attach.GetName(idx);
                            Log.Info("Attach name: " + name);
                            var newAttach = childCD.AddAttachEx(attachFile);
                            var newAttachVars = newAttach.GetVars();
                            newAttachVars.AddValue_String("AttachOutputName", name, true);
                            newAttachVars.AddValue_String("Purchasing_IsPO", type === "PO", true);
                            newAttachVars.AddValue_String("Purchasing_IsQuote", type === "QUOTE", true);
                        }
                    }
                }
                // Process it
                childCD.Process();
                return vars.GetValue_String("RUIDEX", 0);
            }
            Log.Error("Could not create the next process");
            throw "Could not create the next process";
        }
        catch (e) {
            Log.Info("Customer Order does not exist");
        }
    }
    return "";
}
//
// Set CanceledDateTime on customer Order
// If state = 70 --> successfully terminate the record.
//
function CancelCustomerOrderOnPortal() {
    var ret = true;
    Log.Info("Cancel Customer order");
    // Try to fill other attributes from "AP - Vendors" table
    var query = Process.CreateQueryAsProcessAdmin();
    query.SetSpecificTable("CDNAME#Customer Order");
    var queryFilter = "(&(CanceledDateTime__!=*)(Sales_Order_Number__=" + Data.GetValue("OrderNumber__") + "))";
    query.SetFilter(queryFilter);
    if (query.MoveFirst()) {
        var transport = query.MoveNext();
        if (transport) {
            var vars = transport.GetUninheritedVars();
            vars.AddValue_Date("CanceledDateTime__", new Date(), true);
            vars.AddValue_Long("State", 100, true);
            transport.Process();
            if (transport.GetLastError()) {
                Log.Info("Cancel Customer order failed : " + transport.GetLastErrorMessage() + "(" + transport.GetLastError() + ")");
                ret = false;
            }
        }
    }
    return ret;
}
//
// Send e-mail notification to ap clerk to advise "NewVendor"
//
function SendNotifNewVendorToAPClerk() {
    var email = Sys.EmailNotification.CreateEmail({
        userId: Lib.Purchasing.GetAPClerk().login,
        subject: "_New vendor requested",
        template: "Purchasing_Email_NotifNewVendor_APClerk.htm",
        backupUserAsCC: false,
        sendToAllMembersIfGroup: Sys.Parameters.GetInstance("P2P").GetParameter("SendNotificationsToEachGroupMembers") === "1"
    });
    if (email) {
        Sys.EmailNotification.SendEmail(email);
    }
}
//
// Send e-mail notification to ap clerk to advise down payment pending
//
function SendNotifWaitingForDownPaymentToTreasurer() {
    var email = Sys.EmailNotification.CreateEmail({
        userId: Treasurer.login,
        subject: "_Purchase order waiting for payment",
        template: "Purchasing_Email_NotifWaitingForDownPayment.htm",
        backupUserAsCC: false,
        sendToAllMembersIfGroup: Sys.Parameters.GetInstance("P2P").GetParameter("SendNotificationsToEachGroupMembers") === "1"
    });
    if (email) {
        Sys.EmailNotification.SendEmail(email);
    }
}
//
// Send e-mail notification to buyer to advise down payment done
//
function SendNotifDownPaymentDoneToBuyer() {
    var email = Sys.EmailNotification.CreateEmail({
        userId: Data.GetValue("BuyerLogin__"),
        subject: "_Purchase order payment done",
        template: "Purchasing_Email_NotifDownPaymentDone.htm",
        backupUserAsCC: true,
        customTags: {
            "DestinationFullName": Data.GetValue("BuyerName__")
        },
        sendToAllMembersIfGroup: Sys.Parameters.GetInstance("P2P").GetParameter("SendNotificationsToEachGroupMembers") === "1"
    });
    if (email) {
        Sys.EmailNotification.SendEmail(email);
    }
    var nval = Lib.Purchasing.GetNumberAsString;
    var culture = Lib.P2P.GetValidatorOrOwner().GetValue("Culture");
    var decimalValue = nval(Data.GetValue("PaymentAmount__"), culture);
    var msg = Language.Translate("_Down payment done") + " - " + Data.GetValue("Currency__") + " " + decimalValue;
    Lib.Purchasing.AddConversationItem(msg, Lib.Purchasing.ConversationTypes.GeneralInformation, true);
}
//
// Send e-mail notification to requester to advise order
//
function SendEmailToRequesters() {
    var itemsIndex = [];
    var PRitems = Data.GetTable("LineItems__");
    var itemCount = PRitems.GetItemCount();
    for (var i = 0; i < itemCount; i++) {
        itemsIndex.push(i);
    }
    Lib.Purchasing.POValidation.SendEmailToRequesters(itemsIndex);
}
function SendOrderToVendor() {
    Log.Time("SendOrderToVendor");
    var vendor = Lib.Purchasing.Vendor.GetVendorContact();
    if (Sys.Parameters.GetInstance("PAC").GetParameter("DemoEnableInvoiceCreation") == "2") {
        Lib.Purchasing.Demo.GenerateVendorInvoice(Data.GetValue("OrderNumber__"), true);
    }
    // Publish Customer Order
    var ruidex = PublishCustomerOrder(vendor);
    // Publish event
    if (ruidex) {
        Variable.SetValueAsString("CustomerOrderNumber", ruidex);
        Conversation.Join("Conversation__", ruidex);
    }
    var ok = true;
    if (Data.GetValue("EmailNotificationOptions__") == "SendToVendor") {
        ok = Lib.Purchasing.POValidation.SendNotifToVendor(vendor, "Purchasing_Email_NotifSupplier.htm", false);
    }
    else if (Data.GetValue("EmailNotificationOptions__") == "DontSend") {
        Lib.Purchasing.POValidation.SendNotifToMe("Purchasing_Email_NotifBuyer_PODoNotSend.htm", false);
    }
    else if (Data.GetValue("EmailNotificationOptions__") == "PunchoutMode") {
        ok = OrderOnPunchout();
    }
    // We advise APClerk for new vendor
    if (ok && Variable.GetValueAsString("NewVendor__") === "true" && Lib.Purchasing.GetAPClerk().Exists()) {
        SendNotifNewVendorToAPClerk();
    }
    Log.TimeEnd("SendOrderToVendor");
    return ok;
}
/**
 * Calculate Undelivered_amount__ and OpenOrderLocalCurrency__ (aka Undelivered_amount__ in local currency)
 */
function WaitForDeliver(autoReceiveOrderData) {
    // Get all unique recipients
    var allRecipientLogins = [];
    Sys.Helpers.Data.ForEachTableItem("LineItems__", function (line) {
        allRecipientLogins.push(line.GetValue("RecipientDN__"));
    });
    var allRecipientLoginsDisctinct = Sys.Helpers.Array.GetDistinctArray(allRecipientLogins);
    // Give read right on PO to all recipients
    var ownerLogin = Lib.P2P.GetOwner().GetValue("login");
    Sys.Helpers.Array.ForEach(allRecipientLoginsDisctinct, function (recipientLogin) {
        // Give right to recipient if different from buyer/treasurer
        if (ownerLogin !== recipientLogin) {
            Log.Info("Grant all right on purchase order to recipient: " + recipientLogin);
            // we need the "all" right in order to be able to resubmit PO if recipient cancels a GR and reopens PO
            Process.SetRight(recipientLogin, "all");
        }
    });
    if (!autoReceiveOrderData) {
        Log.Info("Purchase order ready to be delivered");
    }
    else {
        Log.Info("Generating one GR for auto reception");
        var grInstance = void 0;
        var recipientIsGroup = Users.GetUser(allRecipientLoginsDisctinct[0]).GetVars().GetValue_String("IsGroup", 0);
        if (allRecipientLoginsDisctinct.length === 1 && !recipientIsGroup) {
            var recipient = allRecipientLoginsDisctinct[0];
            Log.Info("Only one recipient which is not a group => generating GR for " + recipient);
            grInstance = Process.CreateProcessInstanceForUser(Lib.P2P.GetGRProcessName(), recipient, 2 /* Notification */, true);
        }
        else { // We explicitely create the GR as the buyer to avoid creating it with the user or group who did the downpayment
            var buyer = Data.GetValue("BuyerLogin__");
            Log.Info("Multiple recipients or recipient is a group => generating GR for buyer " + buyer);
            grInstance = Process.CreateProcessInstanceForUser(Lib.P2P.GetGRProcessName(), buyer, 2 /* Notification */, true);
        }
        var extGRVars = grInstance.GetExternalVars();
        extGRVars.AddValue_String("OrderNumber__", Data.GetValue("OrderNumber__"), true);
        extGRVars.AddValue_String("CompanyCode__", Data.GetValue("CompanyCode__"), true);
        // Serialize "AutoReceiveOrderData" in GR & PO because it's needed in both side
        var jsonData = JSON.stringify(autoReceiveOrderData);
        extGRVars.AddValue_String("AutoReceiveOrderData", jsonData, true);
        Variable.SetValueAsString("AutoReceiveOrderData", jsonData);
        grInstance.Process();
    }
    // Add an expiration timeout
    var validityDate = new Date();
    validityDate.setFullYear(validityDate.getFullYear() + 10);
    Data.SetValue("ValidityDateTime", Sys.Helpers.Date.Date2DBDateTime(validityDate));
    Process.SetAutoValidateOnExpiration(true);
}
function GetContributionData(sequenceStep, newAction) {
    var currentContributor = workflow.GetContributorAt(sequenceStep);
    currentContributor.action = newAction;
    currentContributor.date = new Date();
    var newComment = Lib.P2P.AddOnBehalfOf(currentContributor, g_newComment) || g_newComment;
    if (newComment) {
        var previousComment = currentContributor.comment;
        var newCommentFormatted = newComment;
        if (previousComment && previousComment.length > 0) {
            newCommentFormatted += "\n" + previousComment;
        }
        Variable.SetValueAsString("Last_comments__", newCommentFormatted);
        Data.SetValue("Comments__", "");
        currentContributor.comment = newCommentFormatted;
    }
    return currentContributor;
}
function SynchronizeItems(justUpdateItemsParam, futureOrderStatus, resumeDocumentAction) {
    // Items synchronization
    Log.Time("SynchronizeItems");
    var justUpdateItems = justUpdateItemsParam || false;
    var askDownPayment = Data.GetValue("PaymentPercent__") > 0;
    var autoReceiveOrderData = Lib.Purchasing.GetAutoReceiveOrderData();
    var bok = Sys.Helpers.Data.RollbackableSection(function (rollbackFn) {
        var orderStatus;
        if (futureOrderStatus) {
            orderStatus = futureOrderStatus;
        }
        else if (askDownPayment) {
            orderStatus = "To pay";
        }
        else if (autoReceiveOrderData) {
            orderStatus = "Auto receive";
        }
        else {
            orderStatus = "To receive";
        }
        var options = {
            futureOrderStatus: orderStatus,
            justUpdateItems: null,
            allPRItems: null,
            resumeDocumentAction: resumeDocumentAction
        };
        if (justUpdateItems) {
            options.justUpdateItems = true;
        }
        else {
            var allPRItems = Lib.Purchasing.POItems.GetPRItemsInForm();
            options.allPRItems = allPRItems;
        }
        return Lib.Purchasing.POValidation.SynchronizeItems(options) || rollbackFn();
    });
    Log.TimeEnd("SynchronizeItems");
    return bok;
}
/**
 * Check the selected PR items are still orderable and not over ordererd.
 * Create order in ERP (date, number, status, etc.)
 * Update budget as ordered.
 * Synchronize PR & PO items.
 *
 * If any error, the next alert is defined and we prevent approval.
 * @return {boolean} true if succeeded, false otherwise.
 */
function Order() {
    // Get list of PR Numbers
    var allPRItems = Lib.Purchasing.POItems.GetPRItemsInForm();
    var lockByPRNames = Object.keys(allPRItems);
    var bok = true;
    // Protect concurrent access on PO items for each PR number
    var lockOk = Process.PreventConcurrentAccess(lockByPRNames, function () {
        if (Data.GetValue("AutomaticOrder__")) {
            Log.Info("AUTO doOrder");
            Data.SetValue("Comments__", Language.Translate("_Auto Send PO Message"));
        }
        Data.SetValue("OrderDate__", Helpers.Date.InDocumentTimezone(new Date()));
        Data.SetValue("OrderDateTime__", new Date());
        if (Sys.Parameters.GetInstance("PAC").GetParameter("MultiShipTo", false)) {
            if (!(Data.GetValue("EmailNotificationOptions__") == "PunchoutMode")) {
                Data.SetValue("ShipToCompany__", GetCustomerCompany());
            }
        }
        Lib.Purchasing.POItems.UpdatePOFromPRAndPO()
            .Catch(function (e) {
            Lib.Purchasing.OnUnexpectedError(e.toString());
            bok = false;
        });
        bok = bok && Lib.Purchasing.POValidation.CheckOrderableItems();
        bok = bok && Lib.Purchasing.POValidation.CheckOverOrderedItems();
        bok = bok && CreatePOInERP(Lib.Purchasing.POValidation.OnCreatePOInERP);
        bok = bok && SynchronizeItems(true);
    });
    if (!lockOk) {
        Lib.CommonDialog.NextAlert.Define("_Purchase order creation error", "_PreventConcurrentAccess on items error message");
    }
    if (!bok || !lockOk) {
        Process.PreventApproval();
        return false;
    }
    // Set next alert with order number
    var number = Data.GetValue("OrderNumber__");
    var msg = "_Advise buyer on PO creation";
    if (number) {
        var ERPName = Lib.ERP.GetERPName();
        var createDocInERP = Sys.Parameters.GetInstance("P2P_" + ERPName).GetParameter("CreateDocInERP");
        msg += createDocInERP ? " with number " + ERPName : " with number";
    }
    Lib.CommonDialog.NextAlert.Define("_PO creation popup", msg, { isError: false, behaviorName: "POCreationInfo" }, number);
    return true;
}
/*** Workflow part ***/
var parameters = {
    actions: {
        doReceipt: {
            OnDone: function ( /*sequenceStep*/) {
                var data = JSON.parse(Variable.GetValueAsString("resumeWithActionData") || "{}");
                if (data.actualRecipientDN) {
                    Lib.Purchasing.POValidation.GiveReadRightToActualRecipientIfNeeded(data.actualRecipientDN);
                }
                var bok = Lib.Purchasing.POValidation.SynchronizeItems({ fromAction: false });
                if (bok) {
                    if (Data.GetValue("OrderStatus__") == "Received") {
                        if (Lib.Purchasing.IsAutoReceiveOrderEnabled()) {
                            Log.Info("AUTO doReceipt");
                            Data.SetValue("Comments__", Language.Translate("_Auto PO Reception Message"));
                        }
                        else {
                            Log.Info("doReceipt");
                        }
                        // Once delivered, forward to buyer if not already me
                        var ownerLogin = Lib.P2P.GetOwner().GetValue("login");
                        var buyerLogin = Data.GetValue("BuyerLogin__");
                        if (buyerLogin && ownerLogin !== buyerLogin) {
                            Log.Info("Forwarding purchase order to buyer: " + buyerLogin);
                            Process.ChangeOwner(buyerLogin);
                            Process.LeaveForm();
                        }
                        if (Lib.Purchasing.POItems.HasDeliveredItems()) {
                            Lib.Purchasing.AddConversationItem(Language.Translate("_Item received"), Lib.Purchasing.ConversationTypes.ItemReception, true, undefined, Users.GetUser(buyerLogin));
                        }
                    }
                    else {
                        Process.WaitForUpdate();
                        Process.LeaveForm();
                    }
                }
                else {
                    Process.PreventApproval();
                }
            }
        },
        doDownPayment: {
            OnDone: function (sequenceStep) {
                var autoReceiveOrderData = Lib.Purchasing.GetAutoReceiveOrderData();
                var bok = Sys.Helpers.Data.RollbackableSection(function (rollbackFn) {
                    if (!autoReceiveOrderData) {
                        Data.SetValue("OrderStatus__", "To receive");
                    }
                    else {
                        Data.SetValue("OrderStatus__", "Auto receive");
                    }
                    return Lib.Purchasing.POValidation.SynchronizeItems() || rollbackFn();
                });
                if (bok) {
                    var contributionData = GetContributionData(sequenceStep, this.actions.downPaymentDone.GetName());
                    workflow.EndWorkflow(contributionData);
                    Log.Info("Down payment done");
                    Variable.SetValueAsString("DownPaymentDone__", "true");
                    SendNotifDownPaymentDoneToBuyer();
                    WaitForDeliver(autoReceiveOrderData); // forward to recipient
                    Process.WaitForUpdate();
                    Process.LeaveForm();
                }
                else {
                    Process.PreventApproval();
                }
            }
        },
        doOrder: {
            OnDone: function (sequenceStep) {
                try {
                    var askDownPayment = Data.GetValue("PaymentPercent__") > 0;
                    var autoReceiveOrderData = Lib.Purchasing.GetAutoReceiveOrderData();
                    var bok = AttachPO();
                    bok = bok && Lib.Purchasing.POBudget.AsOrdered();
                    // Re-synchronize items as they may have been modified when computing budget
                    bok = bok && SynchronizeItems();
                    bok = bok && SendOrderToVendor();
                    if (bok) {
                        Lib.Purchasing.POValidation.GiveReadRightToBuyers(Lib.P2P.GetValidatorOrOwnerLogin());
                        Lib.Purchasing.SetRightForP2PSupervisor();
                        var contributionData = GetContributionData(sequenceStep, parameters.actions.ordered.GetName());
                        if (askDownPayment) {
                            workflow.NextContributor(contributionData);
                        }
                        else {
                            workflow.EndWorkflow(contributionData);
                        }
                        // Update order status
                        if (askDownPayment) {
                            Data.SetValue("OrderStatus__", "To pay");
                        }
                        else if (autoReceiveOrderData) {
                            Data.SetValue("OrderStatus__", "Auto receive");
                        }
                        else {
                            Data.SetValue("OrderStatus__", "To receive");
                        }
                        SendEmailToRequesters();
                        if (askDownPayment) {
                            if (Treasurer.Exists()) {
                                SendNotifWaitingForDownPaymentToTreasurer();
                            }
                            Log.Info("Forwarding purchase order to Treasurer: " + Treasurer.login);
                            Process.Forward(Treasurer.login);
                        }
                        else {
                            WaitForDeliver(autoReceiveOrderData); // forward to recipient
                            Process.WaitForUpdate();
                        }
                    }
                    else {
                        SendErrorNotifToMe();
                        Process.PreventApproval();
                    }
                }
                catch (e) {
                    Log.Error(e.toString());
                    Lib.CommonDialog.NextAlert.Define("_Purchase order creation error", "_PO unhandled exception error message");
                    SendErrorNotifToMe("Purchasing_Email_NotifPOError.htm");
                    Process.PreventApproval();
                }
            }
        },
        received: {},
        downPaymentDone: {},
        ordered: {},
        canceled: {
            OnDone: function (sequenceStep) {
                var bok = true;
                var poStatus = Data.GetValue("OrderStatus__");
                var contributionData = GetContributionData(sequenceStep, this.actions.canceled.GetName());
                if (poStatus !== "To order") {
                    bok = bok && Lib.Purchasing.POBudget.AsCanceled();
                    bok = bok && SynchronizeItems(false, "Canceled", "Cancel_PO");
                    bok = bok && CancelCustomerOrderOnPortal();
                }
                if (bok) {
                    Data.SetValue("OrderStatus__", "Canceled");
                    workflow.EndWorkflow(contributionData);
                    Log.Info("Notify cancellation via the conversation");
                    var msg = Language.Translate("_PurchaseOrderCanceled", false);
                    Lib.Purchasing.AddConversationItem(msg, Lib.Purchasing.ConversationTypes.ItemReception, true, undefined, Users.GetUser(Data.GetValue("BuyerLogin__")));
                    // Cancel message
                    Data.SetValue("State", 300);
                    Process.LeaveForm();
                }
                else {
                    Lib.CommonDialog.NextAlert.Define("_PO cancel error", "_PO cancel error message", { isError: true, behaviorName: "POCancelError" });
                    SendErrorNotifToMe();
                    Process.PreventApproval();
                    /* State 60 does not work Requested action is deleted.
                    if (Data.GetValue("NTries") < 4) // After several tries send error.
                    {
                        Data.SetValue("State", 60);
                        Process.LeaveForm();
                    }
                    else
                    {
                        SendErrorNotifToMe(); // CUstomer should click on retry or contact is administrator.
                        Process.PreventApproval();
                    }
                    */
                }
                return bok;
            }
        }
    }
};
Sys.Helpers.Extend(true, parameters, Lib.Purchasing.WorkflowPO.Parameters);
workflow.AllowRebuild(false);
workflow.Define(parameters);
/*** END - Workflow part - END ***/
/*** Main ***/
Lib.Purchasing.InitTechnicalFields();
var currentName = Data.GetActionName();
var currentAction = Data.GetActionType();
Log.Info("-- PO Validation Script -- Name: '" + (currentName ? currentName : "<empty>") + "', Action: '" + (currentAction ? currentAction : "<empty>") + "'");
// Set variables for notifications
SetVariables();
// Index table to ES for better reporting
Lib.P2P.SetTablesToIndex(["LineItems__", "AdditionalFees__"]);
if (Lib.ERP.IsSAP()) {
    Sys.Helpers.Data.SetAllowTableValuesOnlyForFields(["VendorName__", "VendorNumber__", "PaymentTermCode__"], false);
    Sys.Helpers.Data.SetAllowTableValuesOnlyForColumns("LineItems__", ["ItemCostCenterName__", "ItemGLAccount__"], false);
}
var bContinue = true;
if (currentName === "Cancel_purchase_order") {
    if (CanCancel()) {
        var ret = null;
        if (Lib.ERP.IsSAP()) {
            Log.Info("CancelInERP");
            var PONumber = Data.GetTable("OrderNumber__");
            ret = Lib.ERP.ExecuteERPFunc("Cancel", "PO", PONumber);
            if (!ret || ret.error) {
                Log.Error("Error during cancel in ERP. Details: " + ret.error);
                Lib.CommonDialog.NextAlert.Define("_PO cancel error", "_PO cancel error message", { isError: true, behaviorName: "POCancelError" });
                SendErrorNotifToMe();
                Process.PreventApproval();
            }
            else {
                var poData = ret.ret;
                if (poData.cancelID) {
                    Data.SetValue("Cancellation_ID_in_ERP__", poData.cancelID);
                }
            }
        }
        if (!ret || !ret.error) {
            if (workflow.IsEnded()) {
                workflow.Restart(GetContributionData(0, parameters.actions.canceled.GetName()));
            }
            workflow.DoAction(parameters.actions.canceled.GetName());
        }
        bContinue = false;
    }
}
else if (currentName !== "Retry_"
    && currentName !== "Continue_" // Avoid loop in recovery due to recallscript
    && (CheckErrorPO() // Check ERP Error
        || Lib.Purchasing.POItems.CheckDataCoherency())) // Check all items have same curreny & company code
 {
    Process.PreventApproval();
    bContinue = false;
}
if (bContinue) {
    if (currentName === "Retry_") {
        RetryPO();
        currentName = "Submit_";
    }
    if (currentName === "Continue_") {
        RetryPO(true);
        currentName = "Submit_";
    }
    // First validation
    if (currentName === "" && currentAction === "") {
        if (Data.GetValue("State") == 50) {
            if (Data.GetValue("AutomaticOrder__")) {
                currentAction = "approve";
                currentName = "Submit_";
            }
            else {
                Process.PreventApproval();
            }
        }
    }
    if (currentName === "PostValidation_Post") {
        var wfAction = workflow.GetContributorAt(workflow.GetContributorIndex()).action;
        Log.Info("Workflow action " + wfAction);
        workflow.DoAction(wfAction);
    }
    else if (currentName === "PostValidation_DoReceipt") {
        parameters.actions.doReceipt.OnDone();
    }
    else if (currentName !== "" && currentAction !== "") {
        requiredFields.CheckAll();
        if (Data.IsFormInError()) {
            Log.Info("Form in error.");
        }
        else if (currentAction === "approve_asynchronous" || currentAction === "approve" || currentAction === "ResumeWithAction" || currentAction === "reseal") {
            Lib.CommonDialog.NextAlert.Reset();
            switch (currentName) {
                case "Generate_Invoice":
                    Lib.Purchasing.Demo.GenerateVendorInvoice(Data.GetValue("OrderNumber__"), false);
                    Process.PreventApproval();
                    break;
                case "Generate_CO":
                    PublishCustomerOrder(Lib.Purchasing.Vendor.GetVendorContact());
                    Process.PreventApproval();
                    break;
                case "Submit_":
                case "RequestPayment": // Order/send
                    CreateNewVendor();
                    if (Data.GetValue("PaymentPercent__") > 0) {
                        Treasurer.StoreSomeInfo();
                    }
                    if (Order()) {
                        Data.SetValue("KeepOpenAfterApproval", "WaitForAsyncRecall");
                        Process.RecallScript("PostValidation_Post", true);
                    }
                    break;
                case "SynchronizeItems":
                    Lib.Purchasing.POValidation.SynchronizeItemsFromAction();
                    break;
                case "OnCanceledReception":
                    Lib.Purchasing.POValidation.OnCanceledReception();
                    break;
                case "ConfirmPayment":
                    Process.RecallScript("PostValidation_Post");
                    break;
                case "SaveEditing_":
                case "RetryEditOrder":
                    Lib.Purchasing.POValidation.EditOrder();
                    break;
                case "FullBudgetRecovery":
                    Lib.Purchasing.POBudget.DoFullRecovery();
                    break;
                case "SendEmailNotifications":
                    Lib.Purchasing.POValidation.NotifyVendorByEmail();
                    break;
                default:
                    Lib.Purchasing.POValidation.OnUnknownAction(currentAction, currentName);
                    break;
            }
        }
        else if (currentAction === "save") {
            Lib.Purchasing.POValidation.OnSave();
        }
        else {
            Lib.Purchasing.POValidation.OnUnknownAction(currentAction, currentName);
        }
    }
    // Ignore the first validation action (<empty>/<empty>)
    else if (currentName !== "" || currentAction !== "") {
        Lib.Purchasing.POValidation.OnUnknownAction(currentAction, currentName);
    }
}
var isRecallScriptScheduled = Process.IsRecallScriptScheduled();
Sys.Helpers.TryCallFunction("Lib.PO.Customization.Server.OnValidationScriptEnd", currentAction, currentName, isRecallScriptScheduled);
