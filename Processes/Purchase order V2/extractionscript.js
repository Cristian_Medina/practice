///#GLOBALS Lib Sys
var requiredFields = new Lib.Purchasing.CheckPO.RequiredFields(Data);
function FillPurchaseOrder(isAutoSend) {
    Log.Info("Filling purchase order...");
    var prRuidEx;
    var filter = "";
    var options = { resetItems: true, orderByClause: null };
    // created automatically with the user exit
    if (Lib.Purchasing.IsAutoCreateOrderEnabled() || isAutoSend) {
        prRuidEx = Variable.GetValueAsString("PR_RuidEx__");
        filter = "(PRRUIDEX__=" + prRuidEx + ")";
        options.orderByClause = "LineNumber__ ASC";
        var orderedItemsSubFilter = Variable.GetValueAsString("PR_OrderedItemsSubFilter");
        if (orderedItemsSubFilter) {
            filter = "(&" + filter + orderedItemsSubFilter + ")";
        }
    }
    if (!filter) {
        Log.Info("No PR RuidEx found on record because this PO is created from scratch. Skip this step.");
        return;
    }
    // only no completely ordered items for this ruidex
    Log.Info("Filling PO Form with items from filter: " + filter);
    Lib.Purchasing.POItems.FillForm(filter, options)
        .Then(function () {
        Data.SetValue("NumberOfLines__", Data.GetTable("LineItems__").GetItemCount());
        requiredFields.CheckItemsDeliveryDates();
    })
        .Catch(function (e) {
        Lib.CommonDialog.NextAlert.Define("_Purchase order creation error", e, { isError: true, behaviorName: "POInitError" });
    });
}
//
// MAIN
//
var currentName = Data.GetActionName();
var currentAction = Data.GetActionType();
Log.Info("-- PO Extraction Script -- Name: '" + (currentName ? currentName : "<empty>") + "', Action: '" + (currentAction ? currentAction : "<empty>") + "'");
// set some values on record
Lib.Purchasing.InitTechnicalFields();
Lib.P2P.InitSAPConfiguration(Lib.ERP.GetERPName(), "PAC");
var autoSendOrderParamString = Variable.GetValueAsString("AutoSendOrderParam");
var isAutoSendOrder = autoSendOrderParamString && autoSendOrderParamString !== "{}";
// Fills in the current purchase order from the serialized purchase requisition
FillPurchaseOrder(isAutoSendOrder);
var autoSendOrder = Lib.Purchasing.IsAutoCreateOrderEnabled() && Sys.Helpers.TryCallFunction("Lib.PO.Customization.Server.IsAutoSendOrderEnabled");
var autoSendOrderParam = null;
if (isAutoSendOrder) {
    autoSendOrderParam = JSON.parse(autoSendOrderParamString);
    if (autoSendOrderParam.autoOrder) {
        autoSendOrder = autoSendOrderParam;
    }
}
Variable.SetValueAsString("wkfdata", "");
function DownPaymentAsked() {
    var val = Data.GetValue("PaymentAmount__");
    return val !== null && val !== 0;
}
function sendOrderAutomatically(emailNotificationOptionsParams) {
    // Fill data
    var inError = false;
    // If EmailNotificationOptions not set, use default value
    var emailNotificationOptions = emailNotificationOptionsParams || "";
    if (emailNotificationOptions !== "SendToVendor"
        && emailNotificationOptions !== "DontSend"
        && emailNotificationOptions !== "PunchoutMode"
        && emailNotificationOptions !== "") {
        Log.Info("AutoSendOrder: Invalid value [" + emailNotificationOptions + "] for EmailNotificationOptions__ field");
        inError = true;
    }
    else if (emailNotificationOptions !== "") {
        Data.SetValue("EmailNotificationOptions__", emailNotificationOptions);
    }
    // VendorEmail is required when in SendToVendor mode
    // We retrieve the first known address for the vendor
    if (emailNotificationOptions === "SendToVendor" && !autoSendOrder.VendorEmail) {
        //Create Vendor contact if empty
        var vendor = Lib.Purchasing.Vendor.GetVendorContact();
        if (vendor) {
            autoSendOrder.VendorEmail = vendor.GetValue("EmailAddress");
        }
        else {
            Data.SetError("VendorEmail__", "This field is required");
            Log.Error("No vendor found and vendor could not be created");
            inError = true;
        }
    }
    if (autoSendOrder.VendorEmail) {
        Data.SetValue("VendorEmail__", autoSendOrder.VendorEmail);
    }
    if (autoSendOrder.VendorLogin) {
        Variable.SetValueAsString("ContactLogin", autoSendOrder.VendorLogin);
    }
    if (autoSendOrder.BuyerComment) {
        Data.SetValue("BuyerComment__", autoSendOrder.BuyerComment);
    }
    // Calculate downpayment
    var res = Lib.Purchasing.CalculateDownPayment(Data.GetValue("TotalNetAmount__"), autoSendOrder.PaymentAmount, autoSendOrder.PaymentPercent);
    Data.SetValue("PaymentPercent__", res.PaymentPercent);
    Data.SetValue("PaymentAmount__", res.PaymentAmount);
    inError = requiredFields.CheckAll() && inError; // Always do the check to highlight wrong fields even if we already encountered an error
    if (!inError) {
        // Compute workflow
        var workflow_1 = Sys.WorkflowController.Create(Data, Variable, Language);
        workflow_1.Define(Lib.Purchasing.WorkflowPO.Parameters);
        Lib.Purchasing.WorkflowPO.UpdateRolesSequence(DownPaymentAsked(), workflow_1);
        Data.SetValue("AutomaticOrder__", true);
    }
    else {
        Log.Error("Order not autosent because of invalid data");
        sendNotificationPOInDraftMode("_A Purchase Order was not automatically sent", "Purchasing_Email_NotifAutoPOError.htm");
    }
}
function getOrderTransmissionMethod(companyCode, vendorNumber) {
    return Sys.Helpers.Promise.Create(function (resolve, reject) {
        if (autoSendOrder.EmailNotificationOptions === "DefaultOrderTransmissionMethod") {
            Lib.Purchasing.Vendor.QueryPOTransmissionPreferences(companyCode, vendorNumber)
                .Then(function (preferences) {
                // Prompt method should not autosend
                if ((preferences === null || preferences === void 0 ? void 0 : preferences.POMethod__) && (preferences === null || preferences === void 0 ? void 0 : preferences.POMethod__) !== "Prompt") {
                    resolve(preferences.POMethod__);
                }
                else {
                    reject("PO not sent : Preferred PO method not set for this vendor");
                }
            })
                .Catch(function (e) {
                reject("PO not sent : Error querying preferred PO method: " + e);
            });
        }
        else {
            resolve(autoSendOrder.EmailNotificationOptions);
        }
    });
}
function sendNotificationPOInDraftMode(subject, template) {
    var row = Data.GetTable("LineItems__").GetItem(0);
    var requesterDN = row.GetValue("RecipientDN__");
    var requesterMail = Sys.Helpers.String.ExtractLoginFromDN(requesterDN);
    var options = {
        userId: requesterMail,
        subject: subject,
        template: template,
        customTags: {
            RequesterName__: row.GetValue("ItemRequester__"),
            PRNumber__: row.GetValue("PRNumber__")
        },
        fromName: "Esker Purchase Order",
        backupUserAsCC: true,
        sendToAllMembersIfGroup: Sys.Parameters.GetInstance("P2P").GetParameter("SendNotificationsToEachGroupMembers") === "1"
    };
    Sys.EmailNotification.SendEmailNotification(options);
}
function setPOInDraftMode(error) {
    Log.Info(error);
    sendNotificationPOInDraftMode("_A purchase order was not automatically sent using the vendors default transmission method", "Purchasing_Email_NotifAutoPODraft.htm");
}
function callCustomizationOnLoad() {
    Sys.Helpers.TryCallFunction("Lib.PO.Customization.Server.OnLoad");
}
if (autoSendOrder) {
    getOrderTransmissionMethod(Data.GetValue("CompanyCode__"), Data.GetValue("VendorNumber__"))
        .Then(sendOrderAutomatically)
        .Catch(setPOInDraftMode)
        .Finally(callCustomizationOnLoad);
}
else {
    callCustomizationOnLoad();
}
