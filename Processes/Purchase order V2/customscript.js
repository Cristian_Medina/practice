///#GLOBALS Lib
Log.Time("CustomScript");
var requiredFields = new Lib.Purchasing.CheckPO.RequiredFields(Data);
var workflow = Sys.WorkflowController.Create({ version: 1 });
var topMessageWarning = Lib.P2P.TopMessageWarning(Controls.TopPaneWarning, Controls.TopMessageWarning__);
var g_hasValidGoodsReceipt = false;
var g_editButtonVisible = false;
var nextAlert = Lib.CommonDialog.NextAlert.GetNextAlert();
var g_isEditing = ProcessInstance.isEditing || (nextAlert && nextAlert.isError);
var banner = Sys.Helpers.Banner;
var showTax = false;
banner.SetStatusCombo(Controls.OrderStatus__);
banner.SetHTMLBanner(Controls.HTMLBanner__);
banner.SetMainTitle("Purchase order");
/** **************** **/
/** Global Helpers **/
/** **************** **/
function IsOwner() {
    return Lib.P2P.IsOwner();
}
function IsRecipient() {
    var isRecipient = false;
    var table = Data.GetTable("LineItems__");
    var count = table.GetItemCount();
    for (var i = 0; i < count; i++) {
        var row = table.GetItem(i);
        var recipientLogin = row.GetValue("RecipientDN__");
        isRecipient = isRecipient || (recipientLogin && (User.loginId.toUpperCase() === recipientLogin.toUpperCase() || User.IsMemberOf(recipientLogin) || User.IsBackupUserOf(recipientLogin)));
    }
    return isRecipient;
}
var DownPayment = {
    CurrentMasterControlName: Variable.GetValueAsString("PaymentCurrentMasterControlName"),
    UpdateGui: function (status, bDisplayError) {
        var isAdmin = Lib.P2P.IsAdminNotOwner();
        Controls.PaymentPercent__.SetReadOnly(isAdmin || status !== "To order" || this.IsDone());
        Controls.PaymentAmount__.SetReadOnly(isAdmin || status !== "To order" || this.IsDone());
        Controls.PaymentType__.SetRequired(status === "To pay");
        Controls.PaymentDate__.SetRequired(status === "To pay");
        if (!this.IsAsked() && status !== "To pay") // If no down payment
         {
            // Init values on first access (if they are null)
            ProcessInstance.SetSilentChange(true);
            Data.SetValue("PaymentPercent__", 0);
            Data.SetValue("PaymentAmount__", 0);
            ProcessInstance.SetSilentChange(false);
            Controls.Submit_.Hide(isAdmin || status === "To receive" || status === "Auto receive" || status === "Received" || status === "Canceled" || ProcessInstance.state === 50);
            Controls.RequestPayment.Hide(true);
            Controls.ConfirmPayment.Hide(true);
            // down payment fields
            Controls.PaymentType__.Hide(true);
            Controls.PaymentType__.SetReadOnly(true);
            Controls.PaymentDate__.Hide(true);
            Controls.PaymentReference__.Hide(true);
        }
        else {
            // down payment fields
            Controls.PaymentAmount__.Hide(false);
            if (status === "To order" && !this.IsDone()) {
                if (bDisplayError) {
                    var treasurer = workflow.GetContributorsByRole("treasurer");
                    if (!(treasurer && treasurer.length && treasurer[0].login)) {
                        Controls.PaymentPercent__.SetError("_undefined treasurer");
                        Controls.PaymentAmount__.SetError("_undefined treasurer");
                    }
                    else {
                        Controls.PaymentPercent__.SetError("");
                        Controls.PaymentAmount__.SetError("");
                    }
                }
                Controls.RequestPayment.Hide(false);
                Controls.Submit_.Hide(true);
                Controls.ConfirmPayment.Hide(true);
                Controls.PaymentType__.Hide(true);
                Controls.PaymentDate__.Hide(true);
                Controls.PaymentReference__.Hide(true);
            }
            else if (!isAdmin && status === "To pay") {
                Controls.Submit_.Hide(true);
                Controls.RequestPayment.Hide(true);
                Controls.ConfirmPayment.Hide(false);
                Controls.PaymentType__.SetReadOnly(false);
                Controls.PaymentDate__.Hide(false);
                Controls.PaymentDate__.SetReadOnly(false);
                Controls.PaymentReference__.Hide(false);
                Controls.PaymentReference__.SetReadOnly(false);
                Controls.PaymentType__.Hide(false);
            }
            else {
                Controls.Submit_.Hide(isAdmin || status === "To receive" || status === "Auto receive" || status === "Received" || status === "Canceled" || ProcessInstance.state === 50);
                Controls.RequestPayment.Hide(true);
                Controls.ConfirmPayment.Hide(true);
                Controls.PaymentType__.SetReadOnly(true);
                Controls.PaymentDate__.Hide(false);
                Controls.PaymentDate__.SetReadOnly(true);
                Controls.PaymentReference__.Hide(false);
                Controls.PaymentReference__.SetReadOnly(true);
            }
        }
    },
    IsAsked: function () {
        var val = Controls.PaymentAmount__.GetValue();
        var percent = Controls.PaymentPercent__.GetValue();
        return (val !== null && val !== 0) || (percent !== null && percent !== 0);
    },
    IsValid: function () {
        var okAmount = requiredFields.CheckDownPaymentAmount();
        var okPercent = requiredFields.CheckDownPaymentPercent();
        var ok = okAmount && okPercent;
        if (ok || Lib.Purchasing.IsLineItemsEmpty()) {
            requiredFields.RemoveAllDownPaymentInputError();
        }
        return ok;
    },
    IsDone: function () {
        return Variable.GetValueAsString("DownPaymentDone__") === "true";
    },
    Calculate: function () {
        DownPayment.CurrentMasterControlName = Variable.GetValueAsString("PaymentCurrentMasterControlName");
        // Do something only if user has set 'amount' or '%' downpayment
        if (DownPayment.CurrentMasterControlName) {
            var res = void 0;
            // Set correct input value according the value of Down Payment Master Input
            if (DownPayment.CurrentMasterControlName == "PaymentAmount__") {
                res = Lib.Purchasing.CalculateDownPayment(Controls.TotalNetAmount__.GetValue(), Controls.PaymentAmount__.GetValue(), null);
                Controls.PaymentPercent__.SetValue(isFinite(res.PaymentPercent) ? res.PaymentPercent : 0);
            }
            else if (DownPayment.CurrentMasterControlName == "PaymentPercent__") {
                res = Lib.Purchasing.CalculateDownPayment(Controls.TotalNetAmount__.GetValue(), null, Controls.PaymentPercent__.GetValue());
                Controls.PaymentAmount__.SetValue(res.PaymentAmount);
            }
        }
        if (DownPayment.IsValid()) {
            DownPayment.UpdateGui(Data.GetValue("OrderStatus__"));
            Lib.Purchasing.WorkflowPO.UpdateRolesSequence(DownPayment.IsAsked(), workflow);
        }
    },
    OnChange: function () {
        Variable.SetValueAsString("PaymentCurrentMasterControlName", this.GetName());
        DownPayment.Calculate();
    },
    Request: function () {
        requiredFields.CheckAll();
        if (Process.ShowFirstError() === null) {
            // We need to wait the result of PO creation when buyer requests payment
            ProcessInstance.Approve("RequestPayment");
        }
        return false;
    },
    Confirm: function () {
        requiredFields.CheckAll();
        if (Process.ShowFirstError() === null) {
            ProcessInstance.ApproveAsynchronous("ConfirmPayment");
        }
        return false;
    }
};
var TotalNetAmount = {
    Set: function () {
        Log.Info("Set a new PO total net amount...");
        // Save value of down payment amount before auto reset when setting total amount
        Lib.Purchasing.Items.ComputeTotalAmount();
        if (DownPayment.CurrentMasterControlName && !g_isEditing) {
            DownPayment.Calculate();
        }
    },
    OnChange: function () {
        DownPayment.Calculate();
    }
};
//#region Items
function DisableButtonsIfNeeded() {
    var disable = Lib.Purchasing.IsLineItemsEmpty();
    Controls.Preview_purchase_order.SetDisabled(disable);
    Controls.DownloadCrystalReportsDataFile.SetDisabled(disable);
    Controls.Submit_.SetDisabled(disable);
    Controls.RequestPayment.SetDisabled(disable);
    Controls.SaveEditOrder.SetDisabled(disable);
}
var Items = {
    InitLayout: function () {
        var buyerLogin = Controls.BuyerLogin__.GetValue() || "";
        var isAdmin = Lib.P2P.IsAdminNotOwner();
        var isBuyerOrBackUpOrAdmin = User.loginId.toUpperCase() === buyerLogin.toUpperCase()
            || User.IsMemberOf(buyerLogin)
            || User.IsBackupUserOf(buyerLogin)
            || isAdmin;
        showTax = Sys.Parameters.GetInstance("PAC").GetParameter("DisplayTaxCode") && isBuyerOrBackUpOrAdmin;
        var error = Lib.Purchasing.Items.IsLineItemsInError();
        Controls.LineItems__.SetAtLeastOneLine(false);
        Controls.LineItems__.SetWidth("100%");
        Controls.LineItems__.SetExtendableColumn("ItemDescription__");
        Controls.LineItems__.ItemCurrency__.Hide(!error);
        Controls.LineItems__.ItemCompanyCode__.Hide(!error);
        Controls.LineItems__.ItemTaxCode__.Hide(!showTax);
        Controls.LineItems__.ItemTaxCode__.SetRequired(showTax);
        Lib.P2P.InitItemTypeControl();
        Lib.Purchasing.POItems.InitAddItemsToOrderButton();
        var status = Data.GetValue("OrderStatus__");
        if (!isAdmin && status === "To order") {
            // PO in state "To Order": allow to remove items from the list
            Controls.LineItems__.SetReadOnly(false); //warning : this row reset all the column to their initial readOnly state.
            Controls.LineItems__.SetRowToolsHidden(false);
            Controls.LineItems__.ItemRequestedDeliveryDate__.SetRequired(true);
            Controls.LineItems__.ItemDescription__.SetRequired(true);
            Controls.LineItems__.ItemUnitPrice__.SetRequired(true);
            Controls.LineItems__.ItemNetAmount__.SetRequired(true);
            Controls.LineItems__.ItemNetAmount__.SetLabel("_ItemToOrderAmount");
        }
        else {
            Controls.LineItems__.SetReadOnly(status !== "To receive" || !g_isEditing);
            Controls.LineItems__.SetRowToolsHidden(status !== "To receive" || !g_isEditing);
            Controls.ItemsButtons.Hide(status !== "To receive" || !g_isEditing);
            Controls.AddItemsToOrderButton__.Hide(!g_isEditing || g_hasValidGoodsReceipt);
        }
        Items.UpdateLayout();
    },
    ItemTypeDependenciesVisibilities: {
        "ItemStartDate__": {
            "AmountBased": false, "QuantityBased": false, "ServiceBased": true
        },
        "ItemEndDate__": {
            "AmountBased": false, "QuantityBased": false, "ServiceBased": true
        },
        "ItemRequestedDeliveryDate__": {
            "AmountBased": true, "QuantityBased": true, "ServiceBased": false
        },
        "ItemQuantity__": {
            "AmountBased": false, "QuantityBased": true, "ServiceBased": true
        },
        "ItemUnitPrice__": {
            "AmountBased": false, "QuantityBased": true, "ServiceBased": true
        },
        "ItemUnit__": {
            VisibleCondition: function () {
                return Sys.Parameters.GetInstance("PAC").GetParameter("DisplayUnitOfMeasure");
            },
            "AmountBased": false, "QuantityBased": true, "ServiceBased": true
        },
        "ItemUndeliveredQuantity__": {
            VisibleCondition: function (status) {
                return (status === "To receive" || status === "Auto receive") && ProcessInstance.state < 100 && ProcessInstance.state !== 50;
            },
            "AmountBased": false, "QuantityBased": true, "ServiceBased": true
        },
        "ItemTotalDeliveredQuantity__": {
            VisibleCondition: function (status) {
                return status !== "To order" && status !== "To pay" && status !== "Canceled";
            },
            "AmountBased": false, "QuantityBased": true, "ServiceBased": true
        },
        "ItemOpenAmount__": {
            VisibleCondition: function (status) {
                return (status === "To receive" || status === "Auto receive") && ProcessInstance.state < 100 && ProcessInstance.state !== 50;
            },
            "AmountBased": true, "QuantityBased": false, "ServiceBased": false,
            ShowAllColumnCellsWhenOneCellIsVisible: true
        },
        "ItemReceivedAmount__": {
            VisibleCondition: function (status) {
                return status !== "To order" && status !== "To pay" && status !== "Canceled";
            },
            "AmountBased": true, "QuantityBased": false, "ServiceBased": false,
            ShowAllColumnCellsWhenOneCellIsVisible: true
        },
        "OrderableAmount__": {
            VisibleCondition: function (status) {
                return (g_isEditing && status === "To receive") || status === "To order";
            },
            "AmountBased": true, "QuantityBased": false, "ServiceBased": false
        }
    },
    UpdateLayout: function () {
        var isUnitOfMeasureEnabled = Sys.Parameters.GetInstance("PAC").GetParameter("DisplayUnitOfMeasure");
        Controls.LineItems__.ItemUnit__.SetRequired(isUnitOfMeasureEnabled);
        Sys.Helpers.Controls.ForEachTableRow(Controls.LineItems__, function (row) {
            Items.PRNumber.SetLink(row);
            if (Lib.Purchasing.Items.IsAmountBasedItem(row.GetItem())) {
                row.ItemUnit__.SetRequired(false);
            }
            else {
                row.ItemUnit__.SetRequired(isUnitOfMeasureEnabled);
            }
        });
        Lib.Purchasing.Items.UpdateItemTypeDependencies(Items.ItemTypeDependenciesVisibilities, Data.GetValue("OrderStatus__"));
        Controls.NumberOfLines__.SetValue(Controls.LineItems__.GetItemCount());
        Controls.OpenOrderLocalCurrency__.SetLabel(Language.Translate("_OpenOrderLocalCurrency", true, Data.GetValue("LocalCurrency__")));
    },
    OnRefreshRow: function (index) {
        var row = Controls.LineItems__.GetRow(index);
        Items.PRNumber.SetLink(row);
        var isPunchoutLine = Lib.Purchasing.Punchout.PO.IsPunchoutLineRow(row);
        var isLocked = row.Locked__.IsChecked();
        var isAmountBasedItem = Lib.Purchasing.Items.IsAmountBasedItem(row.GetItem());
        var serviceBased = Lib.Purchasing.Items.IsServiceBasedItem(row.GetItem());
        var status = Data.GetValue("OrderStatus__");
        if (status === "To receive" && g_isEditing) {
            var todayDate = new Date();
            todayDate.setUTCHours(12, 0, 0, 0);
            var lineWithReception = row.NoGoodsReceipt__.IsChecked() ? Sys.Helpers.Date.CompareDate(todayDate, row.ItemRequestedDeliveryDate__.GetValue()) > 0 : row.ItemDeliveryComplete__.IsChecked();
            row.ItemRequestedDeliveryDate__.SetReadOnly(lineWithReception);
            row.ItemQuantity__.SetReadOnly(isAmountBasedItem || g_hasValidGoodsReceipt);
            row.ItemUnitPrice__.SetReadOnly(isAmountBasedItem || g_hasValidGoodsReceipt || isLocked);
            row.ItemNetAmount__.SetReadOnly(!isAmountBasedItem || g_hasValidGoodsReceipt || isLocked);
            row.ItemDescription__.SetReadOnly(isPunchoutLine || g_hasValidGoodsReceipt || isLocked);
            row.ItemNumber__.SetReadOnly(isPunchoutLine || g_hasValidGoodsReceipt || isLocked);
            Controls.LineItems__.HideTableRowDelete(false);
        }
        else if (status === "To order" && !Lib.P2P.IsAdminNotOwner()) {
            row.ItemNumber__.SetReadOnly(isPunchoutLine || isLocked);
            row.ItemDescription__.SetReadOnly(isPunchoutLine || isLocked);
            row.ItemStartDate__.SetReadOnly(!serviceBased);
            row.ItemEndDate__.SetReadOnly(!serviceBased);
            row.ItemRequestedDeliveryDate__.SetReadOnly(isPunchoutLine);
            row.ItemUnitPrice__.SetReadOnly(isPunchoutLine || isAmountBasedItem || isLocked);
            row.ItemNetAmount__.SetReadOnly(isPunchoutLine || !isAmountBasedItem || isLocked);
            row.ItemUnit__.SetReadOnly(isPunchoutLine || isLocked);
            row.ItemTaxCode__.SetReadOnly(false);
        }
        row.ItemUnitPrice__.Hide(isAmountBasedItem);
        row.ItemQuantity__.Hide(isAmountBasedItem);
        row.ItemTotalDeliveredQuantity__.Hide(isAmountBasedItem);
        row.ItemUndeliveredQuantity__.Hide(isAmountBasedItem);
        row.ItemUnit__.Hide(isAmountBasedItem);
        row.OrderableAmount__.Hide(!isAmountBasedItem);
        row.ItemStartDate__.Hide(!serviceBased);
        row.ItemEndDate__.Hide(!serviceBased);
        row.ItemRequestedDeliveryDate__.Hide(serviceBased);
        if (Lib.Purchasing.Punchout.PO.IsEnabled()) {
            Lib.Purchasing.Punchout.PO.CheckPunchoutRowValidity(row);
            if (requiredFields.IsMultiShipTo) {
                var deliveryAddressID = Controls.LineItems__.GetRow(0).ItemDeliveryAddressID__.GetValue();
                Lib.Purchasing.Punchout.PO.CheckDeliveryAddressID(row, deliveryAddressID);
            }
        }
    },
    OnAddItem: function (item, index) {
        Lib.Purchasing.POItems.ReNumberLineItemAdd(item, g_isEditing);
        // Update total amount
        requiredFields.CheckAdditionalFeesAmounts()
            .Then(Lib.Purchasing.Items.ComputeTotalAmount)
            // Total amount has changed, so update downpayment values if the user has filled it
            .Then(DownPayment.Calculate)
            .Then(Lib.Purchasing.POItems.FillTaxSummary)
            .Then(function () {
            Lib.Purchasing.CheckPO.CheckLeadTime(item);
            requiredFields.CheckItemsDeliveryDates({
                specificItemIndex: index
            });
        })
            .Then(DisableButtonsIfNeeded)
            .Then(Items.UpdateLayout);
    },
    OnDeleteItem: function (item, index) {
        if (index == 0) {
            //First line is deleted, we need to check if the CompanyCode and Currency of this PO need to be changed
            if (Controls.LineItems__.GetRow(1) && Controls.LineItems__.GetRow(1).GetItem()) {
                var companyCode = Controls.LineItems__.GetRow(1).GetItem().GetValue("ItemCompanyCode__");
                var currency = Controls.LineItems__.GetRow(1).GetItem().GetValue("ItemCurrency__");
                Log.Info("First line CompanyCode : " + companyCode);
                Log.Info("First line Currency : " + currency);
                if (companyCode !== Controls.CompanyCode__.GetValue()) {
                    Data.SetValue("CompanyCode__", companyCode);
                    Data.SetValue("Currency__", currency);
                    Lib.Purchasing.POItems.SetLocalCurrency();
                    UpdateDownpaymentLabels(currency);
                    Lib.Purchasing.POItems.CheckDataCoherency();
                }
                else if (currency !== Controls.Currency__.GetValue()) {
                    Data.SetValue("Currency__", currency);
                    UpdateDownpaymentLabels(currency);
                    Lib.Purchasing.POItems.CheckDataCoherency();
                }
            }
            if (requiredFields.IsMultiShipTo && Lib.Purchasing.Punchout.PO.IsEnabled()) {
                requiredFields.CheckAllSameDeliveryAddress();
            }
        }
        Lib.Purchasing.POItems.ReNumberLineItemRemove(item, index, g_isEditing);
        // Update total amount
        requiredFields.CheckAdditionalFeesAmounts()
            .Then(Lib.Purchasing.Items.ComputeTotalAmount)
            // Total amount has changed, so update downpayment values if the user has filled it
            .Then(DownPayment.Calculate)
            .Then(Lib.Purchasing.POItems.FillTaxSummary)
            .Then(DisableButtonsIfNeeded)
            .Then(Items.UpdateLayout);
    },
    Description: {
        OnChange: function () {
            var CurrentLineRow = this.GetRow();
            if (Lib.Purchasing.Punchout.PO.IsEnabled()) {
                Lib.Purchasing.Punchout.PO.CheckPunchoutRowValidity(CurrentLineRow);
                if (requiredFields.IsMultiShipTo) {
                    var deliveryAddressID = Controls.LineItems__.GetRow(0).ItemDeliveryAddressID__.GetValue();
                    Lib.Purchasing.Punchout.PO.CheckDeliveryAddressID(CurrentLineRow, deliveryAddressID);
                }
            }
        }
    },
    ItemQuantity: {
        OnChange: function () {
            var currentLineItem = this.GetItem();
            if (Lib.Purchasing.CheckPO.CheckItemQuantity(currentLineItem)) {
                // This is correct as long as we don't allow quantity edition after a partial reception on the line
                currentLineItem.SetValue("ItemUndeliveredQuantity__", currentLineItem.GetValue("NoGoodsReceipt__") ? 0 : currentLineItem.GetValue("ItemQuantity__"));
                Items.Unit_Price.OnChange.call(this);
            }
            else {
                Controls.SaveEditOrder.SetDisabled(!!Process.ShowFirstError());
            }
        }
    },
    Unit_Price: {
        OnChange: function () {
            var item = this.GetItem();
            var orderPrice = item.GetValue("ItemUnitPrice__");
            if (Sys.Helpers.IsEmpty(item.GetValue("ItemUnitPrice__"))) {
                item.SetError("ItemUnitPrice__", "This field is required!");
            }
            else if (orderPrice < 0) {
                item.SetError("ItemUnitPrice__", "_The unit price can't be negative");
            }
            else {
                var requisitionPrice = item.GetValue("ItemRequestedUnitPrice__");
                var orderQty = item.GetValue("ItemQuantity__");
                item.SetValue("ItemNetAmount__", Sys.Helpers.Round(new Sys.Decimal(orderPrice).mul(orderQty), Controls.LineItems__.ItemNetAmount__.GetPrecision() || 2).toNumber());
                item.SetValue("ItemNetAmountLocalCurrency__", Sys.Helpers.Round(new Sys.Decimal(orderPrice).mul(orderQty).mul(item.GetValue("ItemExchangeRate__")), Controls.LineItems__.ItemNetAmount__.GetPrecision() || 2).toNumber());
                var customChangeAllowed = Sys.Helpers.TryCallFunction("Lib.PO.Customization.Common.IsItemUnitPriceChangeAllowed", item);
                var changeAllowed = customChangeAllowed || (customChangeAllowed === null && Lib.Purchasing.CheckPO.IsItemPriceTolerated(requisitionPrice, requisitionPrice, orderPrice));
                if (!changeAllowed) {
                    item.SetError("ItemUnitPrice__", "_The price variance between purchase order and purchase requisition exceeds the tolerance limit");
                }
                else {
                    item.SetError("ItemUnitPrice__");
                    TotalNetAmount.Set();
                    Items.CalculateTaxAmount(this.GetRow());
                }
            }
            Controls.SaveEditOrder.SetDisabled(!!Process.ShowFirstError());
        }
    },
    NetAmount: {
        OnChange: function () {
            var item = this.GetItem();
            var orderPrice = item.GetValue("ItemNetAmount__");
            item.SetValue("ItemQuantity__", orderPrice);
            item.SetValue("ItemUndeliveredQuantity__", orderPrice);
            item.SetValue("ItemNetAmountLocalCurrency__", Sys.Helpers.Round(new Sys.Decimal(orderPrice).mul(item.GetValue("ItemExchangeRate__")), Controls.LineItems__.ItemNetAmount__.GetPrecision() || 2).toNumber());
            if (Sys.Helpers.IsEmpty(orderPrice)) {
                item.SetError("ItemNetAmount__", "This field is required!");
            }
            else if (orderPrice < 0 || (orderPrice == 0 && Lib.Purchasing.Items.IsAmountBasedItem(item))) {
                item.SetError("ItemNetAmount__", "_The unit price can't be negative");
            }
            else {
                var requisitionPrice = item.GetValue("ItemRequestedAmount__");
                var orderablePrice = item.GetValue("OrderableAmount__");
                var customChangeAllowed = Sys.Helpers.TryCallFunction("Lib.PO.Customization.Common.IsItemUnitPriceChangeAllowed", item);
                var changeAllowed = customChangeAllowed || (customChangeAllowed === null && Lib.Purchasing.CheckPO.IsTotalPriceTolerated(requisitionPrice, orderablePrice, orderPrice));
                if (!changeAllowed) {
                    item.SetError("ItemNetAmount__", "_The price variance between purchase order and purchase requisition exceeds the tolerance limit");
                }
                else {
                    item.SetError("ItemNetAmount__", "");
                    item.SetError("PRNumber__", "");
                    item.SetValue("ItemOpenAmount__", orderPrice);
                    TotalNetAmount.Set();
                    Items.CalculateTaxAmount(this.GetRow());
                }
            }
            Controls.SaveEditOrder.SetDisabled(!!Process.ShowFirstError());
        }
    },
    PRNumber: {
        SetLink: function (row) {
            row.PRNumber__.DisplayAs({ type: "Link" });
        },
        OnClick: function () {
            Controls.LineItems__.PRNumber__.Wait(true);
            var options = {
                table: "CDNAME#Purchase requisition V2",
                filter: "RequisitionNumber__=" + this.GetValue(),
                attributes: ["MsnEx", "ValidationURL"],
                sortOrder: null,
                maxRecords: 1
            };
            Sys.GenericAPI.PromisedQuery(options)
                .Then(function (queryResult) {
                if (queryResult.length == 1) {
                    Process.OpenLink(queryResult[0].ValidationURL + "&OnQuit=Close");
                }
                else {
                    Popup.Alert("_Purchase requisition not found or access denied", false, null, "_Purchase requisition not found title");
                }
            })
                .Catch(function ( /*error: string*/) {
                Popup.Alert("_Purchase requisition not found or access denied", false, null, "_Purchase requisition not found title");
            })
                .Finally(function () {
                Controls.LineItems__.PRNumber__.Wait(false);
            });
        }
    },
    TaxCode: {
        OnSelectItem: function (item) {
            var row = this.GetRow();
            row.ItemTaxRate__.SetValue(item.GetValue("TaxRate__"));
            Items.CalculateTaxAmount(row);
        }
    },
    GLAccount: (function () {
        var timeoutID = null;
        return {
            OnSelectItem: function (item) {
                var self = this;
                if (timeoutID) {
                    clearTimeout(timeoutID);
                }
                timeoutID = setTimeout(function () {
                    var currentRow = self.GetRow();
                    if (Lib.Budget.IsEnabled() && Sys.Helpers.IsEmpty(item.GetValue("Group__"))) {
                        self.SetError("_No expense category associated with this GL Account");
                    }
                    currentRow.ItemGroup__.SetValue(item.GetValue("Group__"));
                    Controls.SaveEditOrder.SetDisabled(!!Process.ShowFirstError());
                    timeoutID = null;
                });
            },
            OnChange: function () {
                if (!timeoutID) {
                    var self_1 = this;
                    timeoutID = setTimeout(function () {
                        var currentRow = self_1.GetRow();
                        currentRow.ItemGroup__.SetValue(null);
                        timeoutID = null;
                    });
                }
            }
        };
    })(),
    CalculateTaxAmount: function (row) {
        if (row.ItemNetAmount__) {
            row.ItemTaxAmount__.SetValue(new Sys.Decimal(row.ItemNetAmount__.GetValue()).mul(row.ItemTaxRate__.GetValue()).div(100).toNumber());
        }
        else if (row.Price__) { // From additional fees
            row.ItemTaxAmount__.SetValue(new Sys.Decimal(row.Price__.GetValue() || 0).mul(row.ItemTaxRate__.GetValue() || 0).div(100).toNumber());
        }
        Lib.Purchasing.POItems.FillTaxSummary();
    }
};
//#endregion
//#region workflow definition
function InitWorkflowLayout() {
    var status = Data.GetValue("OrderStatus__");
    Controls.Comments__.Hide(Lib.P2P.IsAdminNotOwner() || ProcessInstance.isReadOnly);
    Controls.Ligne_d_espacement10__.Hide(Lib.P2P.IsAdminNotOwner() || ProcessInstance.isReadOnly);
    Controls.ApprovalWorkflow.Hide(status === "To receive" || status === "Auto receive" || status === "Received");
}
function UpdateWorkflowLayout() {
    Controls.Comments__.SetPlaceholder(Language.Translate("_Enter your comment ..."));
    Controls.POWorkflow__.SetWidth("100%");
    Controls.POWorkflow__.SetExtendableColumn("WRKFComment__");
    Controls.POWorkflow__.SetRowToolsHidden(true);
}
var parameters = {
    mappingTable: {
        OnRefreshRow: function (index) {
            var table = Controls[this.mappingTable.tableName];
            var row = table.GetRow(index);
            if (row.WRKFAction__.GetValue()) //test if the line is not empty
             {
                row.WRKFRole__.SetImageURL(this.actions[row.WRKFAction__.GetValue()].image, false);
                Lib.P2P.HighlightCurrentWorkflowStep(workflow, this.mappingTable.tableName, index);
            }
            row.WRKFUserName__.SetImageURL(Lib.P2P.GetP2PUserImage(row.WRKFIsGroup__.GetValue()), true);
        }
    },
    callbacks: {
        OnError: function (msg) {
            Log.Info(msg);
        },
        OnBuilding: function () {
            Controls.Submit_.SetDisabled(true);
            Controls.RequestPayment.SetDisabled(true);
            Controls.ComputingWorkflow__.Hide(false);
            ProcessInstance.SetSilentChange(true);
        },
        OnBuilt: function () {
            DownPayment.UpdateGui(Data.GetValue("OrderStatus__"), true);
            Controls.ComputingWorkflow__.Hide(true);
            if (!ProcessInstance.isReadOnly) // Re-enable "Order" button only if form is read/write
             {
                DisableButtonsIfNeeded();
            }
            Lib.P2P.HighlightCurrentWorkflowStep(workflow, this.mappingTable.tableName);
            ProcessInstance.SetSilentChange(false);
        }
    }
};
Sys.Helpers.Extend(true, parameters, Lib.Purchasing.WorkflowPO.Parameters);
workflow.Define(parameters);
//#endregion
function UpdateVendorEmailLayout() {
    if (Data.GetValue("EmailNotificationOptions__") == "SendToVendor") {
        Controls.VendorEmail__.Hide(false);
        Controls.VendorEmail__.SetRequired(true);
    }
    else {
        Controls.VendorEmail__.Hide(true);
        Controls.VendorEmail__.SetRequired(false);
    }
}
function UpdateProgressBar() {
    var isEnableOrderProgress = Sys.Parameters.GetInstance("PAC").GetParameter("EnableOrderProgress", false);
    if (isEnableOrderProgress) {
        var table = Data.GetTable("LineItems__");
        var amountOrdered = 0;
        var amountReceived = 0;
        for (var i = 0; i < table.GetItemCount(); i++) {
            var item = table.GetItem(i);
            amountReceived += item.GetValue("ItemTotalDeliveredQuantity__") * item.GetValue("ItemUnitPrice__");
            amountOrdered += item.GetValue("ItemNetAmount__");
        }
        Controls.ProgressBar__.FireEvent("onLoadProgressBar", { total: amountOrdered, done: amountReceived, currency: Data.GetValue("LocalCurrency__") });
    }
}
function UpdateDownpaymentLabels(currency) {
    Controls.TotalNetAmount__.SetLabel(Language.Translate("_Total net amount", false, currency));
    Controls.InfoTotalNetAmount__.SetLabel(Language.Translate("_Info total net amount", false, currency));
    Controls.PaymentAmount__.SetLabel(Language.Translate("_Payment amount") + " (" + currency + ")");
    Controls.TotalAmountIncludingVAT__.SetLabel(Language.Translate("_TotalAmountIncludingVAT") + " (" + currency + ")");
    Controls.TotalAmountIncludingVAT__.Hide(true);
}
//#region PO in punchout mode
function UpdatePOLayoutAsPunchoutMode() {
    if (Data.GetValue("EmailNotificationOptions__") == "PunchoutMode") {
        Lib.Purchasing.Punchout.PO.UpdatePane();
        Controls.AddFee__.Hide(true);
        Controls.AdditionalFees_pane.Hide(true);
        Controls.AdditionalFees__.SetItemCount(0);
        var isMultiShipTo = Sys.Parameters.GetInstance("PAC").GetParameter("MultiShipTo", false);
        if (isMultiShipTo) {
            requiredFields.CheckAllSameDeliveryAddress();
        }
    }
    else {
        Lib.Purchasing.Punchout.PO.ResetPane();
        Controls.AddFee__.Hide(Lib.ERP.IsSAP());
    }
}
function SetPunchoutLineItemsToReadOnly() {
    var nbItem = Math.min(Controls.LineItems__.GetItemCount(), Controls.LineItems__.GetLineCount());
    for (var lineIdx = 0; lineIdx < nbItem; lineIdx++) {
        var row = Controls.LineItems__.GetRow(lineIdx);
        if (row.IsVisible()) {
            Items.OnRefreshRow(lineIdx);
        }
    }
}
//#endregion
function OnClickCancelPO() {
    function OnConfirmCancel(comment) {
        if (Data.GetValue("state") != 90) {
            ProcessInstance.ApproveAsynchronous("Cancel_purchase_order");
        }
        else {
            ProcessInstance.ResumeWithActionAsynchronous("Cancel_purchase_order", { "Comments__": comment });
        }
    }
    var CancelConfirmationPopup = (function () {
        var $comment = null;
        var onCommitted = null;
        function Fill(dialog /*, tabId, event, control*/) {
            var ctrl = dialog.AddDescription("ctrlDesc", null, 466);
            ctrl.SetText(Language.Translate("_Cancel_PO_explanation"));
            var commentCtrl = dialog.AddMultilineText("ctrlComments", "_Comment", 400);
            if (Data.GetValue("OrderStatus__") == "To receive") {
                dialog.RequireControl(commentCtrl);
            }
        }
        function Commit(dialog /*, tabId, event, control*/) {
            $comment = dialog.GetControl("ctrlComments").GetValue();
            Controls.Comments__.SetValue($comment);
            if (Sys.Helpers.IsFunction(onCommitted)) {
                onCommitted($comment);
            }
        }
        function Validate(dialog /*, tabId, event, control*/) {
            if (!dialog.GetControl("ctrlComments").GetValue()) {
                dialog.GetControl("ctrlComments").SetError("This field is required!");
                return false;
            }
            return true;
        }
        function Display(_onCommitted) {
            $comment = null;
            onCommitted = _onCommitted;
            Popup.Dialog("_Cancel purchase order confirmation", null, Fill, Commit, Validate);
        }
        return {
            get comment() { return $comment; },
            Display: Display
        };
    })();
    CancelConfirmationPopup.Display(OnConfirmCancel);
    return false;
}
//DeclareEvent
(function () {
    Sys.Helpers.Controls.OnDatabaseComboboxEvents(Controls.LineItems__.FreeDimension1__, Lib.P2P.FreeDimension.DefineOnSelectItem("FreeDimension1ID__", "Code__"), Lib.P2P.FreeDimension.DefineOnUnknownOrEmptyValue("FreeDimension1ID__"), Lib.P2P.FreeDimension.DefineOnUnknownOrEmptyValue("FreeDimension1ID__"));
    Sys.Helpers.Controls.OnDatabaseComboboxEvents(Controls.LineItems__.FreeDimension1ID__, Lib.P2P.FreeDimension.DefineOnSelectItem("FreeDimension1__", "Description__"), Lib.P2P.FreeDimension.DefineOnUnknownOrEmptyValue("FreeDimension1__"), Lib.P2P.FreeDimension.DefineOnUnknownOrEmptyValue("FreeDimension1__"));
    /* Buttons */
    Controls.PaymentPercent__.OnChange = DownPayment.OnChange;
    Controls.PaymentAmount__.OnChange = DownPayment.OnChange;
    Controls.RequestPayment.OnClick = DownPayment.Request;
    Controls.ConfirmPayment.OnClick = DownPayment.Confirm;
    Controls.Submit_.OnClick = function () {
        // Remove potential budget errors, server side will check for them again
        var table = Data.GetTable("LineItems__");
        var tableSize = table.GetItemCount();
        for (var i = 0; i < tableSize; i++) {
            var item = table.GetItem(i);
            if (item.GetError("ItemDescription__") === Language.Translate("_No budget allocated following user changes", false)) {
                table.GetItem(i).SetError("ItemDescription__", "");
            }
            table.GetItem(i).SetWarning("ItemRequestedDeliveryDate__", "");
        }
        if (requiredFields.CheckAll()) {
            var isMultiShipTo = Sys.Parameters.GetInstance("PAC").GetParameter("MultiShipTo", false);
            if (isMultiShipTo && tableSize > 0) {
                var firstItem = table.GetItem(0);
                //Filling header ship to with first line ship to, useful in punchout mode (Electronical Order).
                Data.SetValue("DeliveryAddressID__", firstItem.GetValue("ItemDeliveryAddressID__"));
                Data.SetValue("ShipToCompany__", firstItem.GetValue("ItemShipToCompany__"));
                Data.SetValue("ShipToAddress__", firstItem.GetValue("ItemShipToAddress__"));
            }
        }
        if (Process.ShowFirstError() !== null) {
            return false;
        }
    };
    Controls.NewPR.OnClick = function () {
        ProcessInstance.OpenInProcess({
            processName: "Purchase requisition V2",
            attachmentsMode: "none",
            willBeChild: false,
            startWithoutProcessing: true,
            doNotCopyExternalVarsFromAncestor: true
        });
    };
    Controls.DownloadCrystalReportsDataFile.OnClick = function () {
        var poTemplateInfos = Lib.Purchasing.POExport.GetPOTemplateInfos();
        if (poTemplateInfos.fileFormat === "RPT") {
            Process.OpenPreview({
                "conversionType": "crystal",
                "templateName": poTemplateInfos.template,
                "language": poTemplateInfos.escapedCompanyCode,
                "outputFormat": "mdb",
                "data": function (fnDataBuildDone) {
                    Lib.Purchasing.POExport.CreatePOJsonString(poTemplateInfos, function (jsonString) {
                        fnDataBuildDone(jsonString);
                    });
                }
            });
        }
        return false;
    };
    Controls.Preview_purchase_order.OnClick = function () {
        var poTemplateInfos = Lib.Purchasing.POExport.GetPOTemplateInfos();
        Lib.P2P.Export.IsAvailableTemplate(Sys.Helpers.Globals.User, "PO_template_V2.docx", poTemplateInfos.escapedCompanyCode)
            .Then(function (result) {
            var preview = poTemplateInfos.termsConditions ?
                {
                    "converter": "PDF Modifier",
                    "mergeKey": "merge",
                    "mergeValue": poTemplateInfos.termsConditions
                } : null;
            if (result.exist && poTemplateInfos.template == "PurchaseOrder.rpt") {
                Log.Info("Old .docx purchase order template detected.");
                poTemplateInfos.fileFormat = "DOCX";
                poTemplateInfos.template = "PO_template_V2.docx";
            }
            if (poTemplateInfos.fileFormat === "RPT") {
                Process.OpenPreview({
                    "conversionType": "crystal",
                    "templateName": poTemplateInfos.template,
                    "language": poTemplateInfos.escapedCompanyCode,
                    "data": function (fnDataBuildDone) {
                        Lib.Purchasing.POExport.CreatePOJsonString(poTemplateInfos, function (jsonString) {
                            fnDataBuildDone(jsonString);
                        });
                    },
                    "preview": preview
                });
            }
            else {
                Process.OpenPreview({
                    "conversionType": "mailMerge",
                    "templateName": poTemplateInfos.template,
                    "language": poTemplateInfos.escapedCompanyCode,
                    "csv": Lib.Purchasing.CreatePOCsv(poTemplateInfos.poNumber),
                    "preview": preview
                });
            }
        })
            .Catch(function (e) {
            Log.Error("Failed to IsAvailableTemplate: " + e);
        });
        return false;
    };
    Controls.Cancel_purchase_order.OnClick = OnClickCancelPO;
    Controls.EmailNotificationOptions__.OnChange = function () {
        UpdateVendorEmailLayout();
        UpdatePOLayoutAsPunchoutMode();
    };
    /* Line Items */
    Controls.LineItems__.ItemRequestedDeliveryDate__.OnChange = function () {
        if (!g_isEditing) {
            Lib.Purchasing.CheckPO.CheckLeadTime(this.GetItem());
        }
        requiredFields.CheckItemsDeliveryDates({
            // return item index - call with inWholeTable (1-based API)
            specificItemIndex: this.GetRow().GetLineNumber(/*inWholeTable*/ true) - 1
        });
    };
    Controls.LineItems__.ItemQuantity__.OnChange = Items.ItemQuantity.OnChange;
    Controls.LineItems__.ItemUnitPrice__.OnChange = Items.Unit_Price.OnChange;
    Controls.LineItems__.ItemDescription__.OnChange = Items.Description.OnChange;
    Controls.LineItems__.ItemTaxCode__.OnSelectItem = Items.TaxCode.OnSelectItem;
    Controls.LineItems__.OnDeleteItem = Items.OnDeleteItem;
    Controls.LineItems__.OnAddItem = Items.OnAddItem;
    Controls.LineItems__.OnRefreshRow = Items.OnRefreshRow;
    Controls.LineItems__.PRNumber__.OnClick = Items.PRNumber.OnClick;
    Controls.LineItems__.ItemGLAccount__.OnSelectItem = Items.GLAccount.OnSelectItem;
    Controls.LineItems__.ItemGLAccount__.OnChange = Items.GLAccount.OnChange;
    Controls.LineItems__.ItemNetAmount__.OnChange = Items.NetAmount.OnChange;
    /* Ship to */
    Lib.Purchasing.ShipTo.InitControls();
    /* Vendor */
    Controls.VendorNumber__.SetAttributes("Email__|VATNumber__|FaxNumber__|PaymentTermCode__|PaymentTermCode__.Description__");
    Controls.VendorName__.SetAttributes("Email__|VATNumber__|FaxNumber__|PaymentTermCode__|PaymentTermCode__.Description__");
    Controls.VendorNumber__.OnSelectItem = Lib.Purchasing.Vendor.OnSelectItem;
    Controls.VendorName__.OnSelectItem = Lib.Purchasing.Vendor.OnSelectItem;
    Controls.VendorName__.OnChange = Lib.Purchasing.Vendor.OnChange;
    var contactEmail = "";
    Controls.VendorEmail__.SetAttributes("login");
    Controls.VendorEmail__.OnSelectItem = function (item) {
        var login = item.GetValue("Login");
        contactEmail = item.GetValue("EmailAddress");
        Variable.SetValueAsString("ContactLogin", login);
    };
    Controls.VendorEmail__.OnChange = function () {
        if (contactEmail !== Controls.VendorEmail__.GetValue()) {
            Variable.SetValueAsString("ContactLogin", "");
        }
    };
    Controls.NewVendorRequest__.OnClick = Lib.Purchasing.Vendor.OpenNewVendorRequestForm;
    var vendorsExtraFilter = Sys.Helpers.TryCallFunction("Lib.P2P.Customization.Common.GetVendorsExtraFilter");
    if (vendorsExtraFilter && vendorsExtraFilter.length > 0) {
        var vendorFilter = Sys.Helpers.Browse.BuildLDAPFilter(vendorsExtraFilter);
        Log.Info("Extra filter defined for vendors: " + vendorFilter);
        var vendorsCustomFilter = Controls.VendorName__.GetCustomFilter(false);
        if (vendorsCustomFilter) {
            if (vendorFilter.charAt(0) !== "(") {
                vendorFilter = "(" + vendorFilter + ")";
            }
            if (vendorsCustomFilter.charAt(0) !== "(") {
                vendorsCustomFilter = "(" + vendorsCustomFilter + ")";
            }
            vendorFilter = "(&" + vendorFilter + vendorsCustomFilter + ")";
        }
        Log.Info("Setting filter for vendors: " + vendorFilter);
        Controls.VendorName__.SetFilter(vendorFilter);
    }
    var lastSelectedPaymentTermCode;
    /* Payment terms */
    Controls.PaymentTermCode__.OnSelectItem = function (item) {
        lastSelectedPaymentTermCode = item.GetValue("PaymentTermCode__");
        this.SetValue(lastSelectedPaymentTermCode); // In SAP, the payment term is always uppercase.
        Controls.PaymentTermDescription__.SetValue(item.GetValue("Description__"));
    };
    Controls.PaymentTermCode__.OnChange = function () {
        // Reset PaymentTermDescription__ only if we haven't selected an existing item
        if (lastSelectedPaymentTermCode !== Controls.PaymentTermCode__.GetValue()) {
            Controls.PaymentTermDescription__.SetValue("");
        }
    };
    /* Payment method */
    Controls.PaymentMethodCode__.OnSelectItem = function (item) {
        Controls.PaymentMethodDescription__.SetValue(item.GetValue("Description__"));
    };
    Controls.PaymentMethodCode__.OnChange = function () {
        if (!Controls.PaymentMethodCode__.GetValue()) {
            Controls.PaymentMethodDescription__.SetValue("");
        }
    };
    Controls.ValidityStart__.OnChange = function () {
        requiredFields.CheckValidityPeriod("ValidityStart__");
    };
    Controls.ValidityEnd__.OnChange = function () {
        requiredFields.CheckValidityPeriod("ValidityEnd__");
    };
    /* Additional fees */
    Controls.AdditionalFees__.ItemTaxCode__.OnSelectItem = Items.TaxCode.OnSelectItem;
    Controls.AddFee__.OnClick = function () {
        var filter = "&";
        if (!Sys.Helpers.IsEmpty(Data.GetValue("CompanyCode__"))) {
            // same company code as already selected Items
            filter += "(CompanyCode__=" + Data.GetValue("CompanyCode__") + ")";
        }
        Controls.BrowseAddFees__.SetFilter(filter);
        Controls.BrowseAddFees__.DoBrowse();
    };
    Controls.BrowseAddFees__.OnSelectItem = function (item) {
        var table = Data.GetTable("AdditionalFees__");
        var index = table.GetItemCount();
        table.AddItem();
        var row = table.GetItem(index);
        var newFee = {
            AdditionalFeeID__: item.GetValue("AdditionalFeeID__"),
            Description__: item.GetValue("Description__"),
            MaxAmount__: parseInt(item.GetValue("MaxAmount__"), 10)
        };
        row.SetValue("AdditionalFeeID__", newFee.AdditionalFeeID__);
        row.SetValue("AdditionalFeeDescription__", newFee.Description__);
        // Reset the hidden browse to avoid a hidden error
        Controls.BrowseAddFees__.SetValue("");
        Controls.AdditionalFees_pane.Hide(false);
        Lib.Purchasing.AdditionalFees.PushValues(Data.GetValue("CompanyCode__"), newFee);
    };
    Controls.AdditionalFees__.Price__.OnChange = function () {
        var _this = this;
        requiredFields.CheckAdditionalFeesAmounts()
            .Then(function () { return Items.CalculateTaxAmount(_this.GetRow()); })
            .Then(Lib.Purchasing.POItems.FillTaxSummary)
            .Then(Lib.Purchasing.Items.ComputeTotalAmount);
    };
    Controls.AdditionalFees__.ItemTaxCode__.OnChange = function () {
        Lib.Purchasing.POItems.FillTaxSummary();
        Lib.Purchasing.Items.ComputeTotalAmount();
    };
    Controls.AdditionalFees__.OnDeleteItem = function ( /*item, index*/) {
        requiredFields.CheckAdditionalFeesAmounts()
            .Then(Lib.Purchasing.POItems.FillTaxSummary)
            .Then(Lib.Purchasing.Items.ComputeTotalAmount);
    };
    Controls.ProgressBar__.BindEvent("OnLoadProgressBar", UpdateProgressBar);
})();
/** ******************* **/
/** Form initialization **/
/** ******************* **/
var GlobalLayout = {
    panes: ["TopPaneWarning",
        "Banner",
        "Requisition_information",
        "AdditionalDataPane",
        "Vendor_details",
        "Ship_to",
        "Vendor_notification_pane",
        "Down_payment_request",
        "Line_items",
        "TaxSummaryPane",
        "DocumentsPanel",
        "ApprovalWorkflow",
        "Delivery_history",
        "Event_history",
        "AdditionalFees_pane",
        "ItemsButtons",
        "LocalActionPane",
        "RelatedInvoicesPane"].map(function (name) { return Controls[name]; }),
    actionButtons: ["Save",
        "Close",
        "Cancel_purchase_order",
        "Submit_",
        "NewGR2",
        "RequestPayment",
        "Generate_Invoice",
        "ConfirmPayment",
        "Generate_CO",
        "Finalize_PO",
        "Preview_purchase_order",
        "DownloadCrystalReportsDataFile",
        "SynchronizeItems",
        "Edit_",
        "SaveEditing_",
        "SaveEditOrder",
        "RollbackEditionAndQuit",
        "NewPR"].map(function (name) { return Controls[name]; }),
    HideWaitScreen: function (hide) {
        // async call just after boot
        setTimeout(function () {
            Controls.TotalNetAmount__.Wait(!hide);
        });
    },
    Hide: function (hide) {
        GlobalLayout.panes.forEach(function (pane) {
            pane.Hide(hide);
        });
        GlobalLayout.actionButtons.forEach(function (button) {
            button.Hide(hide);
        });
        Log.TimeStamp("HideWaitScreen : " + !hide);
        GlobalLayout.HideWaitScreen(!hide);
    }
};
function InitRelatedInvoice() {
    Controls.RelatedInvoicesTable__.SetItemCount(0);
    var options = {
        table: "CDNAME#Vendor invoice",
        filter: "(OrderNumber__=*" + Data.GetValue("OrderNumber__") + "*)",
        attributes: ["InvoiceNumber__", "InvoiceDate__", "InvoiceAmount__", "InvoiceCurrency__", "InvoiceStatus__", "ValidationURL", "MSN"],
        sortOrder: "InvoiceNumber__ ASC",
        maxRecords: null,
        additionalOptions: {
            bigQuery: true,
            queryOptions: "FastSearch=1"
        }
    };
    Sys.GenericAPI.PromisedQuery(options)
        .Then(function (queryResults) {
        if (queryResults.length > 0) {
            queryResults.forEach(function (r) {
                var newItem = Controls.RelatedInvoicesTable__.AddItem();
                newItem.SetValue("InvoiceNumber__", r.InvoiceNumber__);
                newItem.SetValue("InvoiceDate__", r.InvoiceDate__);
                newItem.SetValue("InvoiceAmount__", r.InvoiceAmount__);
                newItem.SetValue("InvoiceCurrency__", r.InvoiceCurrency__);
                newItem.SetValue("InvoiceStatus__", Language.Translate("_VIPStatus_" + r.InvoiceStatus__));
                newItem.SetValue("InvoiceValidationURL__", r.ValidationURL);
            });
            setTimeout(function () {
                var lineItemsCount = Math.min(Controls.RelatedInvoicesTable__.GetItemCount(), Controls.RelatedInvoicesTable__.GetLineCount());
                for (var lineIdx = 0; lineIdx < lineItemsCount; lineIdx++) {
                    var row = Controls.RelatedInvoicesTable__.GetRow(lineIdx);
                    row.InvoiceNumber__.DisplayAs({ type: "Link" });
                }
                Controls.DisplayInvoices__.Hide(false);
            }, 100);
        }
    })
        .Catch(function () {
        //nothing to do
    });
    var relatedInvoiceShown = false;
    Controls.DisplayInvoices__.SetText("_DisplayInvoices_show");
    Controls.DisplayInvoices__.OnClick = function () {
        Controls.RelatedInvoicesPane.Hide(relatedInvoiceShown);
        Controls.DisplayInvoices__.SetText(relatedInvoiceShown ? "_DisplayInvoices_show" : "_DisplayInvoices_hide");
        relatedInvoiceShown = !relatedInvoiceShown;
    };
    Controls.RelatedInvoicesTable__.InvoiceNumber__.OnClick = function () {
        var currentItem = this.GetRow().GetItem();
        Process.OpenLink(currentItem.GetValue("InvoiceValidationURL__") + "&OnQuit=Close");
    };
    Controls.RelatedInvoicesTable__.OnRefreshRow = function (index) {
        var row = Controls.RelatedInvoicesTable__.GetRow(index);
        row.InvoiceNumber__.DisplayAs({ type: "Link" });
    };
}
function InitRelatedGoodsReceipt() {
    Controls.RelatedGoodsReceiptTable__.SetItemCount(0);
    var options = {
        table: "CDNAME#Goods receipt V2",
        filter: "(OrderNumber__=" + Data.GetValue("OrderNumber__") + ")",
        attributes: ["GRNumber__", "DeliveryDate__", "DeliveryNote__", "GRStatus__", "ShortOwnerID", "ValidationURL", "MSN"],
        sortOrder: "GRNumber__ DESC",
        maxRecords: null,
        additionalOptions: {
            bigQuery: true
        }
    };
    Sys.GenericAPI.PromisedQuery(options)
        .Then(function (queryResults) {
        if (queryResults.length > 0) {
            queryResults.forEach(function (r) {
                //AddItem(before = true) => we need to have he last GR at the start of the table.
                //Since we use the BigQuery option, wa can't get the records in the desire order
                var newItem = Controls.RelatedGoodsReceiptTable__.AddItem(true);
                newItem.SetValue("GoodsReceiptDate__", r.DeliveryDate__);
                newItem.SetValue("GoodsReceiptNote__", r.DeliveryNote__);
                newItem.SetValue("GoodsReceiptNumber__", r.GRNumber__);
                newItem.SetValue("GoodsReceiptStatus__", Language.Translate("_GRStatus_" + r.GRStatus__));
                var receiverLogin = r.ShortOwnerID;
                Sys.OnDemand.Users.CacheByLogin.Get(receiverLogin, Lib.P2P.attributesForUserCache).Then(function (result) {
                    var user = result[receiverLogin];
                    if (!user.$error) {
                        Sys.Helpers.SilentChange(function () {
                            newItem.SetValue("GoodsReceiptOwner__", user.displayname ? user.displayname : user.login);
                        });
                    }
                });
                newItem.SetValue("GoodsReceiptValidationURL__", r.ValidationURL);
            });
            setTimeout(function () {
                var lineItemsCount = Math.min(Controls.RelatedGoodsReceiptTable__.GetItemCount(), Controls.RelatedGoodsReceiptTable__.GetLineCount());
                for (var lineIdx = 0; lineIdx < lineItemsCount; lineIdx++) {
                    var row = Controls.RelatedGoodsReceiptTable__.GetRow(lineIdx);
                    row.GoodsReceiptNumber__.DisplayAs({ type: "Link" });
                    row.GoodsReceiptStatus__.DisplayAs({ type: "Link" });
                }
                Controls.DisplayGoodsReceipt__.Hide(false);
            }, 100);
        }
    })
        .Catch(function () {
        //nothing to do
    });
    var relatedGoodsReceiptShown = false;
    Controls.DisplayGoodsReceipt__.SetText("_DisplayGoodsReceipt_show");
    Controls.DisplayGoodsReceipt__.OnClick = function () {
        Controls.Delivery_history.Hide(relatedGoodsReceiptShown);
        Controls.DisplayGoodsReceipt__.SetText(relatedGoodsReceiptShown ? "_DisplayGoodsReceipt_show" : "_DisplayGoodsReceipt_hide");
        relatedGoodsReceiptShown = !relatedGoodsReceiptShown;
    };
    Controls.RelatedGoodsReceiptTable__.GoodsReceiptNumber__.OnClick = function () {
        var currentItem = this.GetRow().GetItem();
        Process.OpenLink(currentItem.GetValue("GoodsReceiptValidationURL__") + "&OnQuit=Close");
    };
    Controls.RelatedGoodsReceiptTable__.GoodsReceiptStatus__.OnClick = Controls.RelatedGoodsReceiptTable__.GoodsReceiptNumber__.OnClick;
    Controls.RelatedGoodsReceiptTable__.OnRefreshRow = function (index) {
        var row = Controls.RelatedGoodsReceiptTable__.GetRow(index);
        row.GoodsReceiptNumber__.DisplayAs({ type: "Link" });
        row.GoodsReceiptStatus__.DisplayAs({ type: "Link" });
    };
}
function InitRelatedASN() {
    var isAdvancedShippingNoticeEnabled = Sys.Parameters.GetInstance("PAC").GetParameter("EnableAdvancedShippingNotice", false);
    var relatedShippingNoticePaneShown;
    if (isAdvancedShippingNoticeEnabled) {
        Lib.Purchasing.ASN.InitRelatedASNPane(Controls.RelatedShippingNoticeTable__, Controls.RelatedShippingNoticePane, true).Then(function (itemCount) {
            Controls.RelatedShippingNoticePane.Hide(true);
            Controls.DisplayAdvancedShippingNotices__.Hide(itemCount === 0);
        });
        setRelatedASNVisibility(false);
        Controls.DisplayAdvancedShippingNotices__.OnClick = function () { return setRelatedASNVisibility(!relatedShippingNoticePaneShown); };
    }
    function setRelatedASNVisibility(isVisible) {
        relatedShippingNoticePaneShown = isVisible;
        Controls.RelatedShippingNoticePane.Hide(!isVisible);
        Controls.DisplayAdvancedShippingNotices__.SetText(isVisible ? "_DisplayAdvancedShippingNotices_hide" : "_DisplayAdvancedShippingNotices_show");
    }
}
function InitRelatedDocumentPane() {
    Controls.DisplayGoodsReceipt__.Hide(true);
    Controls.DisplayInvoices__.Hide(true);
    Controls.RelatedInvoicesPane.Hide(true);
    Controls.Delivery_history.Hide(true);
    if (Data.GetValue("OrderStatus__") !== "To order") {
        InitRelatedInvoice();
        InitRelatedGoodsReceipt();
        InitRelatedASN();
    }
}
function InitAdditionalFees() {
    var isToOrderOrEditing = Data.GetValue("OrderStatus__") === "To order" || (Data.GetValue("OrderStatus__") === "To receive" && g_isEditing);
    var isAdmin = Lib.P2P.IsAdminNotOwner();
    Controls.AddFee__.Hide(!isToOrderOrEditing || Lib.ERP.IsSAP());
    Controls.AdditionalFees__.Price__.SetReadOnly(isAdmin || !isToOrderOrEditing);
    Controls.AdditionalFees__.ItemTaxCode__.Hide(!showTax);
    Controls.AdditionalFees__.ItemTaxCode__.SetReadOnly(!showTax);
    Controls.AdditionalFees__.ItemTaxCode__.SetRequired(showTax);
    Controls.AdditionalFees__.SetAtLeastOneLine(false);
    Controls.BrowseAddFees__.SetAttributes("AdditionalFeeID__");
    // Hide AdditionalFees table by default if no value has been set in it
    if (Data.GetTable("AdditionalFees__").GetItemCount() == 0) {
        Controls.AdditionalFees_pane.Hide(true);
    }
}
function InitTechnicalFields() {
    Controls.TaxSummaryPane.Hide(true);
    Controls.TaxSummary__.SetAtLeastOneLine(false);
    //Keep for upgrade purpose : Old fields that are not deleted after updrage
    if (Controls.NewGR) {
        Controls.NewGR.Hide(true);
    }
    if (Controls.RelatedGoodsReceipt__) {
        Controls.RelatedGoodsReceipt__.Hide(true);
    }
    /**
     * Button for debug purpose, in order to enable them, use Lib.PO.Customization.Client.CustomizeLayout
     */
    Controls.Generate_CO.Hide();
    Controls.Finalize_PO.Hide();
}
function InitConversationPane() {
    // Conversation options
    var options = {
        ignoreIfExists: false,
        notifyByEmail: true,
        emailTemplate: "Conversation_MissedPurchasingItem.htm",
        emailCustomTags: {
            OrderNumber__: Controls.OrderNumber__.GetValue()
        }
    };
    Controls.ConversationUI__.SetOptions(options);
    // Show event history
    if (Variable.GetValueAsString("CustomerOrderNumber")) {
        // Add vendor name into Event history panel title
        var title = Language.Translate("_Event_history", false, Controls.VendorName__.GetValue());
        Controls.Event_history.SetText(title);
        Controls.Event_history.Hide(false);
    }
    else {
        Controls.Event_history.Hide(true);
    }
}
function InitGeneralInformations() {
    var status = Data.GetValue("OrderStatus__");
    Controls.Requisition_information.Hide(status !== "To receive" && status !== "Auto receive" && status !== "Received");
    Controls.RevisionDateTime__.Hide(Sys.Helpers.IsEmpty(Data.GetValue("RevisionDateTime__")));
    Controls.OrderNumber__.Hide(Sys.Helpers.IsEmpty(Data.GetValue("OrderNumber__")));
    Controls.BuyerName__.Hide(status === "Canceled" || status === "To order");
    Controls.OrderDate__.Hide(status === "Canceled" || status === "To order");
    Controls.ConfirmationDatetime__.Hide(true);
    Controls.OrderStatus__.Hide(true);
}
function InitAdditionalDataPane() {
    var isEditable = Data.GetValue("OrderStatus__") === "To order" && !Lib.P2P.IsAdminNotOwner();
    Controls.ValidityStart__.SetReadOnly(!isEditable);
    Controls.ValidityEnd__.SetReadOnly(!isEditable);
    Controls.AdditionalDataPane.Hide(true);
}
function InitButton() {
    var status = Data.GetValue("OrderStatus__");
    var buyerLogin = Controls.BuyerLogin__.GetValue() || "";
    var isBuyerOrBackUp = User.loginId.toUpperCase() === buyerLogin.toUpperCase()
        || User.IsMemberOf(buyerLogin)
        || User.IsBackupUserOf(buyerLogin);
    var isAdmin = Lib.P2P.IsAdminNotOwner();
    var isBuyerOrBackUpOrAdmin = isBuyerOrBackUp || isAdmin;
    var hideNewPRButton = g_isEditing
        || Sys.Helpers.IsEmpty(Controls.OrderNumber__.GetValue())
        || (Controls.EmailNotificationOptions__.GetValue() == "PunchoutMode")
        || (!IsOwner() && !User.IsBackupUserOf(Data.GetValue("OwnerId")) && !IsRecipient());
    Controls.NewPR.Hide(hideNewPRButton);
    Controls.NewGR2.Hide(true);
    Controls.Preview_purchase_order.Hide(isAdmin || !(status === "To order" || (Data.GetValue("OrderStatus__") === "To receive" && g_isEditing)));
    Controls.DownloadCrystalReportsDataFile.Hide(true);
    var hideCancelPO = status === "To order" && (Sys.Helpers.IsEmpty(Data.GetValue("RUIDEX")) || !Sys.Helpers.IsEmpty(Data.GetValue("OrderNumber__")));
    Controls.Cancel_purchase_order.Hide(hideCancelPO || !isBuyerOrBackUpOrAdmin || g_hasValidGoodsReceipt || ProcessInstance.state >= 100 || ProcessInstance.state === 50 || ProcessInstance.isEditing);
    Controls.SynchronizeItems.Hide(true);
    Controls.SaveEditing_.Hide(true);
    Controls.SaveEditOrder.Hide(true);
    Controls.RollbackEditionAndQuit.Hide(true);
    g_editButtonVisible = Controls.Edit_.IsVisible() && isBuyerOrBackUp;
    Controls.Edit_.Hide(true);
    Controls.Submit_.Hide(isAdmin || status === "To receive" || status === "Auto receive" || status === "Received");
    Controls.Save.Hide(isAdmin || status === "To receive" || status === "Auto receive" || status === "Received" || status == "Canceled");
    /*
    * DemoEnableInvoiceCreation :
    * 0 = Nothing to do
    * 1 = User can generate his invoice when he want
    * 2 = Global invoice is generated when the P.O is sent to the vendor
    * 3 = Partial invoices are generated whenever items are delivered
    */
    if (Sys.Parameters.GetInstance("PAC").GetParameter("DemoEnableInvoiceCreation") != "1") {
        Controls.Generate_Invoice.Hide();
    }
}
function InitVendorPanes() {
    var status = Data.GetValue("OrderStatus__");
    var isReadOnly = Lib.P2P.IsAdminNotOwner() || (status !== "To order");
    Controls.VendorName__.SetReadOnly(isReadOnly);
    // To avoid error "This field is required" in the following 3 fields when creating PO from Items, the fields are defined as not required in process template and reset here to required.
    Controls.VendorName__.SetRequired(true);
    Controls.VendorEmail__.SetRequired(true);
    Controls.EmailCarbonCopy__.Hide(true);
    Controls.Vendor_notification_pane.Hide(Data.GetValue("OrderStatus__") !== "To order");
    Controls.Vendor_notification_pane.SetReadOnly(isReadOnly);
    Controls.NewVendorRequest__.Hide(isReadOnly);
    UpdateVendorEmailLayout();
}
function InitBrowsePRItems() {
    var attributes = "";
    Sys.Helpers.Object.ForEach(Lib.Purchasing.Items.PRItemsDBInfo.fields, function (field) {
        attributes += Sys.Helpers.IsPlainObject(field) ? field.name : field;
        attributes += "|";
    });
    //remove trailing | character
    attributes = attributes.slice(0, -1);
    Controls.BrowsePRItems__.SetAttributes(attributes);
}
function InitProgressOrderPane() {
    var status = Data.GetValue("OrderStatus__");
    if (status === "To receive" || status === "Auto receive" || status === "Received" || status == "Canceled") {
        var isEnableOrderProgress = Sys.Parameters.GetInstance("PAC").GetParameter("EnableOrderProgress", false);
        Controls.progressBarPanel.Hide(!isEnableOrderProgress);
    }
}
function FixLayout() {
    GlobalLayout.Hide(false);
    var buyerLogin = Controls.BuyerLogin__.GetValue() || "";
    var isBuyerOrBackUp = User.loginId.toUpperCase() === buyerLogin.toUpperCase()
        || User.IsMemberOf(buyerLogin)
        || User.IsBackupUserOf(buyerLogin);
    if (isBuyerOrBackUp) {
        Process.SetHelpId("5031");
    }
    InitTechnicalFields();
    Items.InitLayout();
    InitAdditionalFees();
    InitConversationPane();
    Lib.Purchasing.ShipTo.SetReadOnly(Data.GetValue("OrderStatus__") !== "To order" || Lib.P2P.IsAdminNotOwner());
    InitGeneralInformations();
    InitAdditionalDataPane();
    InitButton();
    InitWorkflowLayout();
    InitRelatedDocumentPane();
    InitVendorPanes();
    InitBrowsePRItems();
    InitProgressOrderPane();
    DownPayment.UpdateGui(Data.GetValue("OrderStatus__"));
    UpdateDownpaymentLabels(Controls.Currency__.GetValue());
}
function ConditionalLayout() {
    function SetBannerLabel(poBanner, poStatus) {
        switch (poStatus) {
            case "To order":
                poBanner.SetSubTitle("_Banner to order");
                break;
            case "To pay":
                poBanner.SetSubTitle("_Banner to pay");
                break;
            case "Canceled":
                poBanner.SetSubTitle("_Banner order canceled");
                break;
            case "To receive":
                poBanner.SetSubTitle("_Banner waiting for delivery");
                break;
            case "Under construction":
                poBanner.SetSubTitle("_Banner under construction");
                break;
            default:
                poBanner.SetSubTitle("_Banner delivered");
        }
    }
    var status = Data.GetValue("OrderStatus__");
    var isAdmin = Lib.P2P.IsAdminNotOwner();
    Controls.PaymentTermCode__.SetReadOnly(isAdmin || status !== "To order");
    Controls.PaymentMethodCode__.SetReadOnly(isAdmin || status !== "To order");
    if (status !== "To order" || isAdmin) {
        if (isAdmin) {
            Controls.DocumentsPanel.SetReadOnly(true);
        }
        var nbAttach = Attach.GetNbAttach();
        Controls.DocumentsPanel.SetNumberOfNonRemovableAttachments(nbAttach);
    }
    else if (Process.isDesignActive) {
        var poTemplateInfos = Lib.Purchasing.POExport.GetPOTemplateInfos();
        if (poTemplateInfos.fileFormat === "RPT") {
            Controls.DownloadCrystalReportsDataFile.Hide(false);
        }
    }
    Controls.Down_payment_request.Hide(status !== "To order" && status !== "To pay" && status !== "Canceled" && !DownPayment.IsAsked());
    // reception pending and completed
    if (status === "To receive" || status === "Auto receive" || status === "Received") {
        // computing the current status...
        banner.SetSubTitle("");
        if (status !== "Received") {
            // enable editable all modifiable fields such as delivery date
            if (g_isEditing) {
                var nextAlert_1 = Lib.CommonDialog.NextAlert.GetNextAlert();
                if (!nextAlert_1 || !nextAlert_1.isError) {
                    EditingLayout();
                }
            }
            else {
                Controls.NewGR2.Hide((!IsRecipient() && !Lib.P2P.IsAdminNotOwner()) || !Lib.Purchasing.POItems.GoodsReceiptNeeded());
                Controls.Edit_.Hide(!g_editButtonVisible);
            }
        }
    }
    SetBannerLabel(banner, status);
    SetPunchoutLineItemsToReadOnly();
    if (Lib.Purchasing.HasOnlyPunchoutItems()) {
        if (status === "To order") {
            // Set punchout mode by default if all items are punchout (and only in draft)
            Data.SetValue("EmailNotificationOptions__", "PunchoutMode");
        }
        UpdateVendorEmailLayout();
        UpdatePOLayoutAsPunchoutMode();
    }
    else if (status === "To order") {
        Lib.Purchasing.Vendor.QueryPOTransmissionPreferences(Data.GetValue("CompanyCode__"), Data.GetValue("VendorNumber__"))
            .Then(function (preferences) {
            var POMethod = preferences === null || preferences === void 0 ? void 0 : preferences.POMethod__;
            if (POMethod) {
                if (POMethod == "SendToVendor" || POMethod == "DontSend") {
                    Data.SetValue("EmailNotificationOptions__", POMethod);
                    UpdateVendorEmailLayout();
                }
            }
        })
            .Catch(function (e) {
            Log.Error("Error querying preferred PO method: " + e);
        });
    }
}
function OnClickSaveEditing() {
    var SendOptions = ["POEdit_EmailToVendor=_POEdit_EmailToVendor", "POEdit_DoNotSend=_POEdit_DoNotSend"];
    var defaultVendorEmail = Data.GetValue("VendorEmail__");
    var defaultSendOption = "POEdit_EmailToVendor";
    Variable.SetValueAsString("SendOption", JSON.stringify({
        sendOption: defaultSendOption,
        email: defaultVendorEmail
    }));
    function OnConfirmRegeneration(action) {
        switch (action) {
            case "Yes":
                Variable.SetValueAsString("regeneratePO", "1");
                Controls.SaveEditing_.Click();
                break;
            case "Cancel":
            default:
        }
    }
    var PORegenerationPopup = (function () {
        var onCommitted = null;
        var fillPopup = function (dialog) {
            var ctrl1 = dialog.AddDescription("ctrlDesc", null, 466);
            ctrl1.SetText(Language.Translate("_Inform about PO regeneration"));
            var ctrl2 = dialog.AddComboBox("POEdit_SendOptions__", "_POEdit_SendOptions", 300);
            ctrl2.SetAvailableValues(SendOptions);
            ctrl2.SetValue(defaultSendOption);
            var ctrl3 = dialog.AddText("POEdit_Email__", "_POEdit_Email", 300);
            ctrl3.SetValue(defaultVendorEmail);
            dialog.HideDefaultButtons();
            var ctrl4 = dialog.AddButton("Yes__", Language.Translate("_Regenerate PO"));
            ctrl4.SetSubmitStyle();
            dialog.AddButton("Cancel__", Language.Translate("_Cancel save PO edition"));
        };
        var commitPopup = function () {
            if (Sys.Helpers.IsFunction(onCommitted)) {
                onCommitted("Yes");
            }
        };
        var handlePopup = function (dialog, tabId, event, control) {
            if (event === "OnClick") {
                switch (control.GetName()) {
                    case "Yes__":
                        dialog.Commit();
                        break;
                    case "Cancel__":
                    default:
                        dialog.Cancel();
                        break;
                }
            }
            else if (event === "OnChange") {
                switch (control.GetName()) {
                    case "POEdit_SendOptions__":
                    case "POEdit_Email__":
                        {
                            var sendOption = dialog.GetControl("POEdit_SendOptions__").GetValue();
                            dialog.HideControl("POEdit_Email__", sendOption !== "POEdit_EmailToVendor");
                            Variable.SetValueAsString("SendOption", JSON.stringify({
                                sendOption: sendOption,
                                email: dialog.GetControl("POEdit_Email__").GetValue()
                            }));
                            break;
                        }
                    default:
                        break;
                }
            }
        };
        var cancelPopup = function () {
            if (Sys.Helpers.IsFunction(onCommitted)) {
                onCommitted("Cancel");
            }
        };
        function Display(_onCommitted) {
            Variable.SetValueAsString("regeneratePO", "0");
            onCommitted = _onCommitted;
            Popup.Dialog("_Regenerate PO after edition", null, fillPopup, commitPopup, null, handlePopup, cancelPopup);
        }
        return {
            Display: Display
        };
    })();
    Lib.Purchasing.POEdition.ChangesManager.Serialize();
    // Display the popup only if their are changes that require PO regeneration
    var changedFields = Lib.Purchasing.POEdition.ChangesManager.GetChangedFieldsList();
    if (Object.keys(changedFields).length !== 0 && Lib.Purchasing.POEdition.ChangesManager.NeedToRegeneratePO(changedFields)) {
        PORegenerationPopup.Display(OnConfirmRegeneration);
    }
    else {
        Controls.SaveEditing_.Click();
    }
    return false;
}
function EditingLayout() {
    Controls.LineItems__.ItemNetAmount__.SetLabel("_ItemToOrderAmount");
    Controls.SaveEditOrder.Hide(false);
    Controls.LineItems__.ItemOpenAmount__.Hide(true);
    Controls.SaveEditOrder.OnClick = function () {
        if (!Process.ShowFirstError()) {
            OnClickSaveEditing();
        }
    };
    Lib.CommonDialog.NextAlert.Reset();
    var focusCtrl;
    var table = Controls.LineItems__;
    var lineItemsCount = Math.min(table.GetItemCount(), table.GetLineCount());
    for (var i = lineItemsCount - 1; i >= 0; i--) {
        var row = table.GetRow(i);
        if (!row.ItemDeliveryComplete__.IsChecked()) {
            focusCtrl = row.ItemRequestedDeliveryDate__;
            focusCtrl.SetReadOnly(false);
        }
    }
    if (focusCtrl) {
        focusCtrl.Focus();
    }
    Lib.Purchasing.POItems.StockItemsBeforeEdit();
    Lib.Purchasing.POEdition.ChangesManager.Watch("ItemNumber__", "LineItems__");
    Lib.Purchasing.POEdition.ChangesManager.Watch("ItemDescription__", "LineItems__");
    Lib.Purchasing.POEdition.ChangesManager.Watch("ItemRequestedDeliveryDate__", "LineItems__");
    Lib.Purchasing.POEdition.ChangesManager.Watch("ItemQuantity__", "LineItems__", function (item) { return Lib.Purchasing.Items.IsAmountBasedItem(item); });
    Lib.Purchasing.POEdition.ChangesManager.Watch("ItemUnitPrice__", "LineItems__");
    Lib.Purchasing.POEdition.ChangesManager.Watch("ItemNetAmount__", "LineItems__");
    Lib.Purchasing.POEdition.ChangesManager.Watch("Price__", "AdditionalFees__");
}
/** ***** **/
/** START **/
/** ***** **/
function Start() {
    Log.Time("Start");
    Lib.Purchasing.InitTechnicalFields();
    /** ************************** **/
    /** ERP Specific initialization */
    /** ************************** **/
    Lib.Purchasing.Browse.Init();
    FixLayout();
    ConditionalLayout();
    if (workflow.GetTableIndex() === 0) {
        Lib.Purchasing.WorkflowPO.UpdateRolesSequence(DownPayment.IsAsked(), workflow);
    }
    DisableButtonsIfNeeded();
    UpdateWorkflowLayout();
    /* All the layout is fixed, try to call for PS customization */
    var func = Sys.Helpers.TryGetFunction("Lib.PO.Customization.Client.CustomizeLayout");
    if (func) {
        func();
    }
    else {
        Sys.Helpers.TryCallFunction("Lib.PO.Customization.Client.CustomiseLayout");
    }
    Lib.CommonDialog.NextAlert.Show({
        // special behavior for Synchronize Items error
        "syncItemsFromActionError": {
            IsShowable: function () {
                // just show the button and Popup
                Controls.SynchronizeItems.Hide(false);
                return true;
            }
        },
        // special behavior for Unexpected error
        "onUnexpectedError": {
            IsShowable: function () {
                // In PO workflow we just advise user to check logs and retry last action.
                // In post PO workflow we show the "SynchronizeItems" button (we change label) and advise user to check logs and retry
                var status = Data.GetValue("OrderStatus__");
                var inPOWorkflow = Lib.Purchasing.POStatus.ForPOWorkflow.indexOf(status) !== -1;
                if (!inPOWorkflow) {
                    Controls.SynchronizeItems.SetText(Language.Translate("_Retry"));
                    Controls.SynchronizeItems.Hide(false);
                }
                return true;
            }
        },
        // special behaviors for PO creation
        "POCreationInfo": {
            IsShowable: function () {
                var showable = !!Data.GetActionName();
                // Avoid when popup is shown the "PO changed" warning
                if (showable) {
                    Controls.PaymentType__.Hide(true);
                    Controls.PaymentDate__.Hide(true);
                    Controls.PaymentReference__.Hide(true);
                }
                return showable;
            },
            OnOK: function () {
                // Close or go to the next document
                if (!ProcessInstance.Next("next")) {
                    ProcessInstance.Quit("quit");
                }
            }
        },
        "POCreationError": {
            Popup: function (nextAlert) {
                Lib.CommonDialog.NextAlert.PopupYesCancel(function (action) {
                    switch (action) {
                        case "Yes":
                            ProcessInstance.Approve("Retry_");
                            break;
                        case "Cancel":
                            Lib.Purchasing.SetDocumentReadonlyAndHideAllButtonsExceptQuit();
                            break;
                        default:
                            break;
                    }
                }, nextAlert, "_PO not found in ERP");
            }
        },
        "SendToVendorError": {
            Popup: function (nextAlert) {
                Lib.CommonDialog.NextAlert.PopupYesCancel(function (action) {
                    switch (action) {
                        case "Yes":
                            ProcessInstance.Approve("PostValidation_Post");
                            break;
                        case "Cancel":
                            Lib.Purchasing.SetDocumentReadonlyAndHideAllButtonsExceptQuit();
                            break;
                        default:
                            break;
                    }
                }, nextAlert, "_Retry now", "_Retry later");
            }
        },
        "SendEmailNotifications": {
            Popup: function (nextAlert) {
                Lib.CommonDialog.NextAlert.PopupYesCancel(function (action) {
                    switch (action) {
                        case "Yes":
                            ProcessInstance.Approve("SendEmailNotifications");
                            break;
                        case "Cancel":
                            Lib.Purchasing.SetDocumentReadonlyAndHideAllButtonsExceptQuit();
                            break;
                        default:
                            break;
                    }
                }, nextAlert, "_Retry now", "_Retry later");
            }
        },
        "ClearSequence": {
            IsShowable: function () {
                Data.SetValue("OrderNumber__", "");
                return true;
            }
        },
        "POEditionError": {
            Popup: function (nextAlert) {
                // first time, we are still in editing mode. We cannot do any action in this mode.
                EditingLayout();
                var firstSaveReturn = !!Data.GetActionName();
                var rollbackNeeded = Lib.Purchasing.POEdition.Rollback.IsRollbackNeeded();
                if (rollbackNeeded) {
                    Lib.Purchasing.SetDocumentReadonlyAndHideAllButtonsExceptQuit("RollbackEditionAndQuit");
                    Popup.Alert(nextAlert.message, true, function () {
                        Controls.RollbackEditionAndQuit.OnClick = function () {
                            Lib.Purchasing.ResetAllFieldsInError();
                            Lib.Purchasing.POEdition.ChangesManager.Revert()
                                .Then(function () {
                                if (firstSaveReturn) {
                                    Controls.SaveEditing_.Click();
                                }
                                else {
                                    ProcessInstance.Approve("RetryEditOrder");
                                }
                            });
                        };
                    }, nextAlert.title);
                }
                else {
                    Lib.CommonDialog.NextAlert.PopupYesCancel(function (action) {
                        switch (action) {
                            case "Yes":
                                if (firstSaveReturn) {
                                    Controls.SaveEditing_.Click();
                                }
                                else {
                                    ProcessInstance.Approve("RetryEditOrder");
                                }
                                break;
                            default:
                                break;
                        }
                    }, nextAlert, "_Retry now", "_Retry later");
                }
            }
        },
        "POCancelError": {
            Popup: function (nextAlert) {
                Lib.CommonDialog.NextAlert.PopupYesCancel(function (action) {
                    switch (action) {
                        case "Yes":
                            OnClickCancelPO();
                            break;
                        case "Cancel":
                            Lib.Purchasing.SetDocumentReadonlyAndHideAllButtonsExceptQuit();
                            break;
                        default:
                            break;
                    }
                }, nextAlert, "_Retry PO cancel");
            }
        }
    });
    Controls.TopPaneWarning.Hide(true);
    Lib.P2P.DisplayArchiveDurationWarning("PAC", function () {
        topMessageWarning.Add(Language.Translate("_Archive duration warning"));
    });
    Lib.P2P.DisplayAdminWarning("PAC", function (displayName) {
        topMessageWarning.Add(Language.Translate("_View as admin on bealf of {0}", false, displayName));
    });
    Lib.P2P.DisplayBackupUserWarning("PAC", function (displayName) {
        topMessageWarning.Add(Language.Translate("_View on bealf of {0}", false, displayName));
    }, function () {
        if (Data.GetValue("OwnerID") == Sys.Helpers.Globals.User.loginId) {
            return null;
        }
        var buyerLogin = Controls.BuyerLogin__.GetValue() || "";
        var login = null;
        if (Data.GetValue("State") == 50 || Data.GetValue("State") == 70 || Data.GetValue("State") == 90) {
            // Display banner for the backup of the buyer
            if (User.IsBackupUserOf(buyerLogin)) {
                login = buyerLogin;
            }
            else {
                // Display banner for the backup of the recipient
                var g = Sys.Helpers.Globals;
                var recipientDn = void 0;
                var i = 0;
                var table = Data.GetTable("LineItems__");
                var count = table.GetItemCount();
                while (!recipientDn && i < count) {
                    var row = table.GetItem(i);
                    recipientDn = row.GetValue("RecipientDN__");
                    if (!g.User.IsBackupUserOf(recipientDn)) {
                        recipientDn = null;
                    }
                    ++i;
                }
                // Display warning on state = 90 and 70
                var state = Data.GetValue("State");
                if ((state === "70" || state === "90") && User.IsBackupUserOf(Data.GetValue("OwnerId"))) {
                    recipientDn = Data.GetValue("OwnerId");
                }
                var recipientLogin = recipientDn ? Sys.Helpers.String.ExtractLoginFromDN(recipientDn) : null;
                login = recipientLogin;
            }
        }
        return login;
    });
    if (ProcessInstance.state === 50) {
        var status = Data.GetValue("OrderStatus__");
        if (status === "Under construction" || status === "To order") {
            Lib.Purchasing.SetDocumentReadonlyAndHideAllButtonsExceptQuit();
        }
    }
    if (!ProcessInstance.state) {
        ProcessInstance.DisableExtractionScript();
    }
    Process.ShowFirstError();
    Log.TimeEnd("Start");
}
/** *************** **/
/** BEFORE STARTING **/
/** *************** **/
function FixLayoutBeforeStarting() {
    Log.Info("FixLayoutBeforeStarting");
    Process.ShowFirstErrorAfterBoot(false);
    GlobalLayout.Hide(true);
}
function FillPurchaseOrder(token) {
    Log.Time("FillPurchaseOrder");
    Log.Info("Filling purchase order...");
    var prRuidEx;
    var filter = "";
    var options = { resetItems: true, orderByMsn: null, orderByClause: null };
    var ancestorsids = ProcessInstance.selectedRuidFromView;
    if (ancestorsids) {
        var i = 0;
        for (i = 0; i < ancestorsids.length; i++) {
            if (ancestorsids[i].startsWith("CT#")) {
                // It's a PR item
                var msn = ancestorsids[i].split(".")[1];
                filter += "(MSN=" + msn + ")";
                if (!options.orderByMsn) {
                    options.orderByMsn = [];
                }
                options.orderByMsn.push(msn);
            }
            else {
                // It's a PR
                prRuidEx = ancestorsids[i];
                filter += "(PRRUIDEX__=" + prRuidEx + ")";
                Variable.SetValueAsString("PR_RuidEx__", prRuidEx);
                options.orderByClause = "LineNumber__ ASC";
            }
        }
        if (i > 1) {
            filter = "(|" + filter + ")";
        }
    }
    if (!filter) {
        Log.Info("No PR RuidEx found on record because this PO is created from scratch. Skip this step.");
        Log.TimeEnd("FillPurchaseOrder");
        token.Use();
        return;
    }
    // Since the PR can be multi-buyers and the PO mono-buyer => pick only current buyer's items
    filter = "(&(BuyerDN__=$submit-owner)" + filter + ")";
    // only no completely ordered items for this ruidex
    Log.Info("Filling PO Form with items from filter: " + filter);
    Lib.Purchasing.POItems.FillForm(filter, options)
        .Then(function () {
        Log.TimeEnd("FillPurchaseOrder");
        Lib.Purchasing.CheckPO.CheckAllLeadTimes();
        requiredFields.CheckItemsDeliveryDates();
        token.Use();
    })
        .Catch(function (e) {
        Lib.CommonDialog.NextAlert.Define("_Purchase order creation error", e, { isError: true, behaviorName: "POInitError" });
        Log.TimeEnd("FillPurchaseOrder");
        token.Use();
    }).Finally(function () {
        Controls.OpenOrderLocalCurrency__.SetLabel(Language.Translate("_OpenOrderLocalCurrency", true, Data.GetValue("LocalCurrency__")));
    });
}
function UpdatePOFromGR(token) {
    var actionNeeded = 1;
    function done() {
        if (--actionNeeded <= 0) {
            token.Use();
        }
    }
    if (g_isEditing) {
        Lib.Purchasing.POEdition.ChangesManager.Watch("OrderStatus__");
        Lib.Purchasing.POEdition.ChangesManager.Watch("ItemUndeliveredQuantity__", "LineItems__");
        Lib.Purchasing.POEdition.ChangesManager.Watch("ItemDeliveryComplete__", "LineItems__");
        Lib.Purchasing.POEdition.ChangesManager.Watch("ItemNetAmount__", "LineItems__");
        actionNeeded++;
        Lib.Purchasing.POItems.UpdatePOFromPRAndPO(true).Then(done);
    }
    Lib.Purchasing.POItems.UpdatePOFromGR(function (hasGoodsReceipt, hasValidGoodsReceipt) {
        g_hasValidGoodsReceipt = hasValidGoodsReceipt;
        UpdateProgressBar();
        done();
    });
}
Sys.Helpers.EnableSmartSilentChange();
// START - ignore all changes on form during the initialization processing
Log.Info({
    "ReadOnly": ProcessInstance.isReadOnly,
    "State": ProcessInstance.state
});
ProcessInstance.SetSilentChange(true);
FixLayoutBeforeStarting();
var syncBeforeStarting = Sys.Helpers.Synchronizer.Create(function () {
    Start();
    // END - ignore all changes on form during the initialization processing
    ProcessInstance.SetSilentChange(false);
}, null, { progressDelay: 15000, OnProgress: Sys.Helpers.Synchronizer.OnProgress });
if (!Data.GetValue("State")) {
    syncBeforeStarting.Register(FillPurchaseOrder);
}
var statusBeforeUpdate = Data.GetValue("OrderStatus__");
if (statusBeforeUpdate === "Auto receive" || statusBeforeUpdate === "To receive") {
    syncBeforeStarting.Register(UpdatePOFromGR);
}
var onLoadPromise = Sys.Helpers.TryCallFunction("Lib.PO.Customization.Client.OnLoad");
if (onLoadPromise) {
    syncBeforeStarting.Register(onLoadPromise);
}
Log.Time("LoadParameters");
Lib.Purchasing.LoadParameters().Then(function () {
    Log.TimeEnd("LoadParameters");
    syncBeforeStarting.Synchronize(function () {
        Log.TimeEnd("Synchronizer");
    });
    Log.Time("Synchronizer");
    syncBeforeStarting.Start();
});
Log.TimeEnd("CustomScript");
