///#GLOBALS Lib
var currentName = Data.GetActionName();
if (currentName == "Finalize_PO") {
    Lib.Purchasing.POItems.UpdatePOFromGR(function ( /*hasGoodsReceipt*/) {
        //nothing to do
    });
    Data.SetValue("OpenOrderLocalCurrency__", 0);
}
