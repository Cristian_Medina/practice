var Validation;
(function (Validation) {
    ///#GLOBALS Lib Sys
    var currentName = Data.GetActionName();
    var currentAction = Data.GetActionType();
    Log.Info("-- Budget Management Validation Script -- Name: '" + (currentName ? currentName : "<empty>") + "', Action: '" + (currentAction ? currentAction : "<empty>") + "' , Device: '" + Data.GetActionDevice());
    /** ********* **/
    /** FUNCTIONS **/
    /** ********* **/
    function ExecuteAction(action, template) {
        Sys.Helpers.TryCallFunction("Lib.Budget.Management.Customization.Server.OnExecuteAction", currentName);
        var completedTime = "";
        var startTime = Data.GetValue("ValidationDateTime");
        Data.SetValue("StartedTime__", startTime);
        Process.EnableScriptMode(false);
        action()
            .Then(function (actionResult) {
            completedTime = new Date();
            Data.SetValue("CompletedTime__", completedTime);
            return actionResult.msg;
        })
            .Catch(function (errorResult) {
            return errorResult instanceof Error ? errorResult.toString() : errorResult.msg;
        })
            .Then(function (msg) {
            Data.SetValue("Output__", msg);
            if (template) {
                var user = Users.GetUser(Data.GetValue("User__"));
                var formatOptions = {
                    dateFormat: "ShortDate",
                    timeZone: "Local"
                };
                var customTags = {
                    "Operation__": Language.Translate("_Operation" + Data.GetValue("Operation__") + "Step"),
                    "ImportedFileName": Attach.GetName(0) + Attach.GetExtension(0),
                    "StartedTime__": user.GetFormattedDate(startTime, formatOptions),
                    "CompletedTime__": completedTime ? user.GetFormattedDate(completedTime, formatOptions) : "",
                    "Output__": msg
                };
                SendNotificationEmail(customTags, template);
            }
            else {
                Data.SetValue("KeepOpenAfterApproval", "WaitForApproval");
            }
        });
    }
    function SendNotificationEmail(customTags, template) {
        var email = Sys.EmailNotification.CreateEmail({
            userId: Data.GetValue("User__"),
            template: template,
            backupUserAsCC: false,
            sendToAllMembersIfGroup: Sys.Parameters.GetInstance("P2P").GetParameter("SendNotificationsToEachGroupMembers") === "1",
            customTags: customTags
        });
        if (email) {
            Sys.EmailNotification.SendEmail(email);
        }
    }
    if (Data.GetValue("State") >= 50) {
        if (currentAction === "approve_asynchronous" || currentAction === "approve" || currentAction === "ResumeWithAction") {
            switch (currentName) {
                case "ImportCSV":
                    if (currentAction === "approve_asynchronous") {
                        ExecuteAction(Lib.Budget.Management.ImportCSV, "Budget_Management_Email_ImportSummary.htm");
                    }
                    else {
                        var maxLines = 500;
                        if (Lib.Budget.Management.GetImportCSVLineCount(maxLines + 1) <= maxLines) {
                            ExecuteAction(Lib.Budget.Management.ImportCSV);
                        }
                        else {
                            Lib.CommonDialog.NextAlert.Define("_Import large CSV", "_Confirm import of large CSV asynchronously", { isError: false, behaviorName: "LargeCSV" }, maxLines);
                            Process.PreventApproval();
                        }
                    }
                    break;
                case "Delete":
                    ExecuteAction(Lib.Budget.Management.Delete);
                    break;
                case "Export":
                    ExecuteAction(Lib.Budget.Management.ExportCSV);
                    break;
                case "Revise":
                    ExecuteAction(Lib.Budget.Management.Revise);
                    break;
                case "Close":
                    ExecuteAction(Lib.Budget.Management.Close);
                    break;
                default:
                    break;
            }
        }
        else if (currentAction !== "reprocess") {
            ExecuteAction(Lib.Budget.Management.ImportCSV, "Budget_Management_Email_OperationSummary.htm");
        }
    }
    Sys.Helpers.TryCallFunction("Lib.Budget.Management.Customization.Server.OnValidationScriptEnd", currentAction, currentName);
})(Validation || (Validation = {}));
